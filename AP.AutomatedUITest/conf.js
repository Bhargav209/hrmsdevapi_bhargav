﻿// An example configuration file. 
exports.config = {
    // The address of a running selenium server. 
    seleniumAddress: 'http://localhost:4444/wd/hub',

    // Capabilities to be passed to the webdriver instance. 

    capabilities: {
        'browserName': 'chrome'
    },

    //multiCapabilities: [{
    //        'browserName': 'chrome'
    //    }, {
    //        'browserName': 'firefox'
    //    }],

    // Spec patterns are relative to the current working directly when 
    // protractor is called. 

    useAllAngular2AppRoots: true,
    //specs: ['test/spec/protractor/*_test.js'],
    specs: ['MasterScreensTest.js'],

    // Options to be passed to Jasmine-node. 
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    }
    //onPrepare: function () {
    //    browser.manage().timeouts().implicitlyWait(30000);
    //}
};

