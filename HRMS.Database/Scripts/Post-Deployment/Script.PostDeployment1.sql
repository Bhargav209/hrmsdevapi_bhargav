﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

---------------------------START-----------------------------
--Project Life cycle DML scripts

--Resizing and making not null to Projectname column of project table
ALTER TABLE Projects
ALTER COLUMN ProjectName VARCHAR(100) NOT NULL

--applying unique key constraint to projectname column of project table
ALTER TABLE Projects
ADD CONSTRAINT unq_projects_projectname UNIQUE (ProjectName)

--applying unique key constraint to ProjectCode column of project table
ALTER TABLE Projects
ADD CONSTRAINT uq_projects_projectcode UNIQUE (ProjectCode)

--making not null to actual start date column of project table
ALTER TABLE Projects
ALTER COLUMN ActualStartDate DateTime NOT NULL

--making not null to clientid column of project table
ALTER TABLE Projects
ALTER COLUMN ClientId INT NOT NULL

--making not null to DepartmentId column of project table
ALTER TABLE Projects
ALTER COLUMN DepartmentId INT NOT NULL

--making not null to DomainId column of project table
ALTER TABLE Projects
ALTER COLUMN DomainId INT NOT NULL

--making not null to ProjectStateId column of project table
ALTER TABLE Projects
ALTER COLUMN ProjectStateId INT NOT NULL

--Deleting PlannedStartDate column from project table
ALTER TABLE Projects
DROP COLUMN PlannedStartDate

--Deleting PlannedEndDate column from project table
ALTER TABLE Projects
DROP COLUMN PlannedEndDate

--Deleting IsActive column from project table
ALTER TABLE Projects
DROP COLUMN IsActive

--Deleting StatusId column from project table
ALTER TABLE Projects
DROP COLUMN StatusId

--Deleting IsSOWSigned column from project table
ALTER TABLE Projects
DROP COLUMN IsSOWSigned

--Deleting LeadID column from project table
ALTER TABLE Projects
DROP COLUMN LeadID

-- update rejectedbyDH value to halted in status table
--UPDATE status SET StatusCode='Halted', StatusDescription='Halted' where StatusId = 23 AND CategoryID = 9

--update projectstateid as closed where isactive is 0 in project table
--UPDATE Projects SET ProjectStateId = 26 WHERE IsActive = 0

--update domainid where domain id is null in project table
--UPDATE Projects SET DomainId = 2 WHERE DomainId IS NULL

--update ProjectStateId where ProjectStateId is null in project table
--UPDATE Projects SET ProjectStateId = 24 WHERE ProjectStateId IS NULL

--renaming projectstateid column to projectstatus column in project table
--EXEC sp_rename 'project.ProjectStateId', 'ProjectStatus', 'COLUMN';


--inserting project states into status table
INSERT INTO STATUS (StatusId,StatusCode,StatusDescription,IsActive,CreatedUser,CreatedDate,SystemInfo,CategoryID)
            Values(21,'SubmittedForApproval','Submitted For Approval',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),			
                  (22,'ApprovedByDH','Approved By Delivery Head',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
			      (23,'RejectedByDH','RejectedBy Delivery Head',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (24,'Drafted','Drafted',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (25,'Created','Created',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (26,'Closed','Closed',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (27,'Execution','Execution',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9)

--inserting a new category ppc to categorymaster table for project life cycle
INSERT INTO CategoryMaster(CategoryName,IsActive,CreatedUser,CreatedDate,SystemInfo) values('PPC',1,'mithun.pradhan@senecaglobal.com','2019-02-05 20:15:20.890','192.168.11.2')

--inserting a new notification type with newly created category for project life cycle into notification type table
INSERT INTO NotificationType(NotificationTypeID,NotificationCode,NotificationDesc,CreatedUser,CreatedDate,SystemInfo,CategoryId) 
values(12,'PPC','Project Profile Creation','mithun.pradhan@senecaglobal.com','2019-02-05 20:15:20.890','192.168.11.2',9)


---------------------------END----------------------------------
---------------------------START-----------------------------
--applied composite unique constraint to sowid and projectid in sow table

ALTER TABLE SOW ADD CONSTRAINT
 uk_SOW_2 UNIQUE NONCLUSTERED 
 (
 SOWId,
 ProjectId
 )
---------------------------END----------------------------------
---------------------------START-----------------------------
--Applied check constraint to client billing role

ALTER TABLE clientbillingroles
Add Constraint CK_ClientBillingRoles CHECK(noofpositions>=1)

---------------------------END----------------------------------

---------------------------START-----------------------------
-- KRApdfconfiguration
DELETE FROM [dbo].[KRApdfconfiguration]
GO
SET IDENTITY_INSERT [dbo].[KRApdfconfiguration] ON 

INSERT [dbo].[KRApdfconfiguration] ([KRApdfconfigurationID], [Section1], [Section2], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (1, N'<p> I thank you for your commitment and contributions for successful delivery of products and services to our clients during the year 2016-17. In our pursuit for delivery excellence, meaningful goals and objectives have been set for our projects to institute high performance culture and they are aligned to the key result areas of associates. Our associate recognition and reward mechanisms has been designed to promote high-performance, innovation and improvement, and entrepreneurship.</p><br/>
<p> We are focusing on implementation of various organization initiatives to improve productivity and quality of delivery to our clients:</p>
<ul style="list-style-type: circle;">
<li>Teaming and cross-functional collaboration</li>
<li>Competency development of associates</li>
<li>Innovation and continuous improvement</li>
<li>Process automation and tool usage</li>
</ul><br/>
<p> You would have received valuable feedback during your development cycle reviews and also during the annual associate development review with regard to your achievement against the KRAs set for the year 2016-17. You are required to implement suitable developmental action plan during the year 2017-18 to address the identified deficiencies and improvements needs.</p><br/>
<p> The following KRAs are assigned to you for the year 2017-18 to demonstrate your work performance and competency development:</p>', N'<p>You need to collect and record necessary evidence (data and information) to track your progress against KRAs. You are required to report the same to your Reporting Manager and to the members of your development review committee for their analysis and review. Please use the Associate Development Record template (available in SGMS) for the same. Your work performance and competency development progress against these KRAs will be reviewed during associate development reviews.</p><br/>
<p>I strongly urge you to maintain high performance attitude and focus on continuous learning to stay competitive, and be able to perform multiple roles to generate superior outcomes.</p><br/>', N'SENECAGLOBAL\santosh.bolisetty', CAST(N'2019-04-18T13:01:19.450' AS DateTime), NULL, NULL, N'192.168.2.208  ')
SET IDENTITY_INSERT [dbo].[KRApdfconfiguration] OFF
Go

---------------------------END----------------------------------
---------------------------START-----------------------------
--KRAMeasurementType data configuration
DELETE FROM [dbo].[KRAMeasurementType]
GO
SET IDENTITY_INSERT [dbo].[KRAMeasurementType] ON 

INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (1, N'Scale')
INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (2, N'Percentage')
INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (3, N'Number')
INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (4, N'Date')
SET IDENTITY_INSERT [dbo].[KRAMeasurementType] OFF

GO

---------------------------END----------------------------------
---------------------------START-----------------------------
-- Alter AssociateKRAMapper 

ALTER TABLE [dbo].[AssociateKRAMapper] DROP CONSTRAINT [FK_KRAAssociateMapper_KRAGroup]
GO

ALTER TABLE [dbo].[AssociateKRAMapper] DROP CONSTRAINT [FK_KRAAssociateMapper_FinancialYear]
GO

ALTER TABLE [dbo].[AssociateKRAMapper] DROP CONSTRAINT [FK_KRAAssociateMapper_Employee]
GO

/****** Object:  Table [dbo].[AssociateKRAMapper]    Script Date: 4/24/2019 7:14:08 PM ******/
DROP TABLE [dbo].[AssociateKRAMapper]
GO

/****** Object:  Table [dbo].[AssociateKRAMapper]    Script Date: 4/24/2019 7:14:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AssociateKRAMapper](
	[EmployeeID] [int] NOT NULL,
	[KRARoleCategoryID] [int] NULL,
	[KRARoleID] [int] NULL,
	[FinancialYearID] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[KRAGroupID] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_Employee]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_FinancialYear] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_FinancialYear]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_KRAGroup] FOREIGN KEY([KRAGroupID])
REFERENCES [dbo].[KRAGroup] ([KRAGroupId])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_KRAGroup]
GO

---------------------------END----------------------------------
---------------------------START-----------------------------

DELETE FROM [dbo].[KRATargetPeriod]
GO
DELETE FROM [dbo].[KRAOperator]
GO
SET IDENTITY_INSERT [dbo].[KRAOperator] ON 

INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (1, N'>')
INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (2, N'>=')
INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (3, N'<')
INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (4, N'<=')
INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (5, N'=')
SET IDENTITY_INSERT [dbo].[KRAOperator] OFF
SET IDENTITY_INSERT [dbo].[KRATargetPeriod] ON 

INSERT [dbo].[KRATargetPeriod] ([KRATargetPeriodID], [KRATargetPeriod]) VALUES (1, N'Quarterly')
INSERT [dbo].[KRATargetPeriod] ([KRATargetPeriodID], [KRATargetPeriod]) VALUES (2, N'Half yearly')
INSERT [dbo].[KRATargetPeriod] ([KRATargetPeriodID], [KRATargetPeriod]) VALUES (3, N'Yearly')
SET IDENTITY_INSERT [dbo].[KRATargetPeriod] OFF

GO
---------------------------END----------------------------------

---------------------------START-----------------------------
DELETE FROM [dbo].[RoleCategory]
GO
SET IDENTITY_INSERT [dbo].[RoleCategory] ON 

INSERT [dbo].[RoleCategory] ([RoleCategoryID], [RoleCategoryName], [RoleCategoryDescription], [DateCreated], [CreatedUser], [DateModified], [ModifiedUser], [SystemInfo]) VALUES (1, N'Individual Contributor', N'Individual Contributor', CAST(N'2018-04-13T09:54:12.103' AS DateTime), N'Kalyan Penumutchu', CAST(N'2018-04-13T09:54:12.103' AS DateTime), N'Sushmitha', N'192.168.2.27   ')
INSERT [dbo].[RoleCategory] ([RoleCategoryID], [RoleCategoryName], [RoleCategoryDescription], [DateCreated], [CreatedUser], [DateModified], [ModifiedUser], [SystemInfo]) VALUES (2, N'Independent Contributor', N'Independent Contributor', CAST(N'2018-04-18T14:06:26.710' AS DateTime), N'kalyan Penumutchu', CAST(N'2018-04-18T14:06:26.710' AS DateTime), N'sa', N'192.168.2.174  ')
INSERT [dbo].[RoleCategory] ([RoleCategoryID], [RoleCategoryName], [RoleCategoryDescription], [DateCreated], [CreatedUser], [DateModified], [ModifiedUser], [SystemInfo]) VALUES (3, N'Leader', N'Leader', CAST(N'2018-04-18T14:06:46.037' AS DateTime), N'kalyan Penumutchu', CAST(N'2018-04-18T14:06:46.037' AS DateTime), N'sa', N'192.168.2.174  ')
SET IDENTITY_INSERT [dbo].[RoleCategory] OFF
---------------------------END----------------------------------
---------------------------START-----------------------------
--SGL15009-3993  Admin-Roles: Unable to create roles in admin screen

ALTER TABLE RoleMaster
ADD CONSTRAINT UC_RoleMaster UNIQUE (SGRoleID,PrefixID,SuffixID);
---------------------------START-----------------------------
---------------------------END----------------------------------
---------------------------START-----------------------------

ALTER TABLE ADRCycle
ADD FinancialYearId INT NOT NULL
GO
ALTER TABLE [dbo].[ADRCycle]  WITH CHECK ADD  CONSTRAINT [FK_ADRCycle_FinancialYear] FOREIGN KEY([FinancialYearId])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[ADRCycle] CHECK CONSTRAINT [FK_ADRCycle_FinancialYear]
GO
---------------------------END----------------------------------
---------------------------START-----------------------------
--Added parentid to Categorymaster table
ALTER TABLE CategoryMaster
ADD ParentId INT NULL
---------------------------END----------------------------------
---------------------------START-----------------------------
--Applied Unique key constriant to ADROrganisationDevelopmentMaster and ADROrganisationValueMaster tables
ALTER TABLE ADROrganisationDevelopmentMaster
ADD CONSTRAINT UC_ADROrganisationDevelopment UNIQUE (FinancialYearId,ADROrganisationDevelopmentActivity);

ALTER TABLE ADROrganisationValueMaster
ADD CONSTRAINT UC_ADROrganisationValue UNIQUE (FinancialYearId,ADROrganisationValue);
---------------------------END----------------------------------