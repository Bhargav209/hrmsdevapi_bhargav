﻿
-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	16-11-2017
-- Modified date	:	16-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Calculates Notice Period  
-- ======================================================= 
   
CREATE FUNCTION [dbo].[udf_NoticePeriodCalculation]
(@DateOfResignation DATETIME)
RETURNS DATETIME
AS
BEGIN

	DECLARE @NoticePeriod INT
	DECLARE @LastWorkingDay DATETIME

	SELECT @NoticePeriod = ValueID FROM [dbo].[lkValue] WHERE ValueName = 'Notice Period'
	SELECT @LastWorkingDay = DATEADD(MONTH,@NoticePeriod,@DateOfResignation)
	RETURN @LastWorkingDay;
END
