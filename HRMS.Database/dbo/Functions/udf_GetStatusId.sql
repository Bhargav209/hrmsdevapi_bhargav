﻿CREATE FUNCTION [dbo].[udf_GetStatusId](@StatusCode NVARCHAR(50), @CategoryName NVARCHAR(50))  
RETURNS INT   
AS   
BEGIN  
    RETURN
	(
		SELECT              
			[status].StatusId
		FROM [dbo].[Status] [status]       
		INNER JOIN [dbo].[CategoryMaster] categoryMaster        
		ON [status].CategoryID = categoryMaster.CategoryID      
		WHERE 
			[status].StatusCode = @StatusCode AND categoryMaster.CategoryName = @CategoryName   
	)
END;

