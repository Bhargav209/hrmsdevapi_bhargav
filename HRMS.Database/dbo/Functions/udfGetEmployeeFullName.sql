﻿CREATE FUNCTION [dbo].[udfGetEmployeeFullName](@EmployeeId int)    
RETURNS varchar(300)     
AS     
BEGIN    
    RETURN  
 (  
  SELECT   
  CASE WHEN LEN(ISNULL(MiddleName,''))>=1 THEN  CONCAT (FirstName, ' ', MiddleName, ' ', LastName) 
       ELSE CONCAT (FirstName, ' ', LastName) END AS EmployeeName 
  FROM   
   [dbo].[Employee] employee   
  WHERE   
   employee.employeeid=@EmployeeId   
 )  
END;

