﻿CREATE FUNCTION [dbo].[udf_GetEmployeeProjects](@EmployeeId int)      
RETURNS varchar(300)       
AS       
    
BEGIN      
    
DECLARE @ProjectCount INT     
DECLARE @Projects VARCHAR(MAX) = NULL, @project VARCHAR(MAX)     
    
    
SELECT @ProjectCount = COUNT(*) FROM AssociateAllocation aa WHERE aa.EmployeeId = @EmployeeId 
    
IF(@ProjectCount > 1)     
BEGIN    
 DECLARE db_cursor CURSOR FOR     

 SELECT project.projectName FROM 
  [dbo].[Projects] project  INNER JOIN [AssociateAllocation] allocation                    
   ON allocation.ProjectId = project.ProjectId 
   where allocation.EmployeeId = @EmployeeId and allocation.isactive=1  
    
OPEN db_cursor      
FETCH NEXT FROM db_cursor INTO @project      
    
WHILE @@FETCH_STATUS = 0      
BEGIN      
 IF(@Projects IS NULL)    
  SET @Projects = @project    
 ELSE    
  SET @Projects = @Projects + ', ' + @project    
      
      FETCH NEXT FROM db_cursor INTO @project     
END     
CLOSE db_cursor      
DEALLOCATE db_cursor     
END    
    
ELSE    
BEGIN    
 SELECT @Projects =  project.ProjectName    
 FROM [dbo].[Projects] project  INNER JOIN [AssociateAllocation] allocation                    
   ON allocation.ProjectId = project.ProjectId 
   where allocation.EmployeeId = @EmployeeId  
END    
    
    RETURN    
 (    
  SELECT @Projects    
 )    
END;
