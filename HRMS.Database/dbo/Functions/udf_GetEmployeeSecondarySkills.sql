﻿CREATE FUNCTION [dbo].[udf_GetEmployeeSecondarySkills](@EmployeeId int)    
RETURNS varchar(300)     
AS     
  
BEGIN    
  
DECLARE @skillCount INT   
DECLARE @skills VARCHAR(MAX) = NULL, @skill VARCHAR(MAX)   
  
  
SELECT @skillCount = COUNT(*) FROM employeeskills es WHERE es.EmployeeId = @EmployeeId 
  
IF(@skillCount > 1)   
BEGIN  
 DECLARE db_cursor CURSOR FOR   
SELECT  skill.SkillName  
 FROM EmployeeSkills  employeeSkill  
 JOIN  Skills skill ON employeeSkill.SkillId = skill.SkillId  
 WHERE employeeSkill.EmployeeId = @EmployeeId  AND employeeSkill.IsPrimary=0 
  
OPEN db_cursor    
FETCH NEXT FROM db_cursor INTO @skill    
  
WHILE @@FETCH_STATUS = 0    
BEGIN    
 IF(@skills IS NULL)  
  SET @skills = @skill  
 ELSE  
  SET @skills = @skills + ', ' + @skill  
    
      FETCH NEXT FROM db_cursor INTO @skill   
END   
CLOSE db_cursor    
DEALLOCATE db_cursor   
END  
  
ELSE  
BEGIN  
 SELECT @skills =  skill.SkillName  
 FROM EmployeeSkills  employeeSkill  
 JOIN  Skills skill ON employeeSkill.SkillId = skill.SkillId  
 WHERE employeeSkill.EmployeeId = @EmployeeId  
END  
  
    RETURN  
 (  
  SELECT @skills  
 )  
END;   
