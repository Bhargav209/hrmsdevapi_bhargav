﻿CREATE TRIGGER [dbo].[trg_addProjectDataToHistory]   
ON [dbo].[Projects]
AFTER UPDATE AS
IF (UPDATE([ProjectName]) OR UPDATE([ClientId]) OR UPDATE([ProjectTypeId]) OR UPDATE([ProjectStateId]) OR UPDATE([ActualEndDate]) OR UPDATE([PracticeAreaId]) OR UPDATE([DomainId])) 
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[ProjectsHistory] 
	( 	
	 [ProjectId]
	,[ProjectCode]
	,[ProjectName]
	,[ClientId]
	,[ProjectTypeId]
	,[ProjectStateId]
	,[ActualStartDate]	
	,[ActualEndDate]
	,[CreatedUser]
	,[CreatedDate]
	,[SystemInfo]
	,[DepartmentId]
	,[PracticeAreaId]
	,[DomainId]
	 )  
     SELECT
	    d.ProjectId,
		d.ProjectCode,
		d.ProjectName,
        d.ClientId,
		d.ProjectTypeId,
		d.ProjectStateId,
		d.ActualStartDate,
		d.ActualEndDate,        
		d.CreatedUser,
		d.CreatedDate,		
        d.SystemInfo ,
		d.DepartmentId,
		d.PracticeAreaId,
		d.DomainId 	   
	FROM Inserted project
	INNER JOIN Deleted d ON project.ProjectId = d.ProjectId
END;
