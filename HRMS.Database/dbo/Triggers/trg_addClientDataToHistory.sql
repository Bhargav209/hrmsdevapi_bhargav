﻿CREATE TRIGGER [dbo].[trg_addClientDataToHistory]     
ON [dbo].[Clients]  
AFTER UPDATE AS  
IF (UPDATE([ClientCode]) OR UPDATE([ClientName]) OR UPDATE([ClientRegisterName]) OR UPDATE([IsActive]))   
BEGIN    
  --Audit OLD record.    
  INSERT INTO [dbo].[ClientsHistory]   
 (    
  [ClientId]  
 ,[ClientCode]  
 ,[ClientName]  
 ,[ClientRegisterName]  
 ,[IsActive]  
 ,[CreatedBy]  
 ,[CreatedDate]   
 ,[SystemInfo]  
  )    
     SELECT  
        d.ClientId,    
        d.ClientCode,    
        d.ClientName,   
  d.ClientRegisterName,  
  d.IsActive,  
  d.CreatedUser,  
  d.CreatedDate,    
        d.SystemInfo        
 FROM Inserted client  
 INNER JOIN Deleted d ON client.ClientId = d.ClientId  
END;  