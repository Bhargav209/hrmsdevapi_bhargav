﻿CREATE TABLE [dbo].[ADRMeasurementAreas](
	[ADRMeasurementAreaId] [int] IDENTITY(1,1) NOT NULL,
	[ADRMeasurementAreaName] [varchar](50) NOT NULL,
	[ADRMeasurementAreaDescription] [varchar](100) NULL,
	[FinancialYearId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ADRMeasurementArea] PRIMARY KEY CLUSTERED 
(
	[ADRMeasurementAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ADRMeasurementAreas]  WITH CHECK ADD  CONSTRAINT [FK_ADRMeasurementAreas_FinancialYear] FOREIGN KEY([FinancialYearId])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[ADRMeasurementAreas] CHECK CONSTRAINT [FK_ADRMeasurementAreas_FinancialYear]
GO