﻿CREATE TABLE [dbo].[Projects](
	[ProjectId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectCode] [varchar](30) NOT NULL,
	[ProjectName] [varchar](100) NOT NULL,
	[ClientId] [int] NOT NULL,
	[ProjectTypeId] [int] NULL,
	[ProjectStateId] [int] NOT NULL,
	[ActualStartDate] [datetime] NOT NULL,
	[ActualEndDate] [datetime] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[DepartmentId] [int] NOT NULL,
	[PracticeAreaId] [int] NOT NULL,
	[DomainId] [int] NOT NULL,
 CONSTRAINT [PK__Projects__761ABEF07F13ACCB] PRIMARY KEY CLUSTERED 
(
	[ProjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [unq_projects_projectname] UNIQUE NONCLUSTERED 
(
	[ProjectName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uq_projects_projectcode] UNIQUE NONCLUSTERED 
(
	[ProjectCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Projects] ADD  CONSTRAINT [DF_Projects_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[Projects] ADD  CONSTRAINT [DF_Projects_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Projects] ADD  CONSTRAINT [DF_Projects_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Clients1] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([ClientId])
GO

ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Clients1]
GO

ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO

ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Departments]
GO

ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Domains] FOREIGN KEY([DomainId])
REFERENCES [dbo].[Domain] ([DomainID])
GO

ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Domains]
GO

ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_PracticeArea] FOREIGN KEY([PracticeAreaId])
REFERENCES [dbo].[PracticeArea] ([PracticeAreaId])
GO

ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_PracticeArea]
GO

ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_ProjectTypeId] FOREIGN KEY([ProjectTypeId])
REFERENCES [dbo].[ProjectType] ([ProjectTypeId])
GO

ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_ProjectTypeId]
GO


