﻿CREATE TABLE [dbo].[ClientBillingRoleHistory] (
    [Id]                      INT           IDENTITY (1, 1) NOT NULL,
    [ClientBillingRoleId]     INT           NOT NULL,
    [ClientBillingRoleCode]   VARCHAR (50)  NULL,
    [ClientBillingRoleName]   VARCHAR (50)  NOT NULL,
    [ProjectId]               INT           NOT NULL,
    [NoOfPositions]           INT           NOT NULL,
    [StartDate]               DATE          NULL,
    [EndDate]                 DATE          NULL,
    [CreatedDate]             DATETIME      NULL,
    [CreatedUser]             VARCHAR (100) NULL,
    [SystemInfo]              VARCHAR (50)  NULL,
    [ClientBillingPercentage] INT           NULL
);

