﻿CREATE TABLE [dbo].[RequisitionRoleSkills] (
    [RoleSkillMappingID] INT           NOT NULL,
    [CompetencyAreaID]   INT           NOT NULL,
    [SkillGroupID]       INT           NOT NULL,
    [SkillID]            INT           NOT NULL,
    [ProficiencyLevelID] INT           NOT NULL,
    [CreatedUser]        VARCHAR (100) CONSTRAINT [DF_RequisitionRoleSkills_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]       VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      CONSTRAINT [DF_RequisitionRoleSkills_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]       DATETIME      NULL,
    [SystemInfo]         VARCHAR (50)  CONSTRAINT [DF_RequisitionRoleSkills_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [FK_RequisitionRoleSkills_CompetencyArea] FOREIGN KEY ([CompetencyAreaID]) REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId]),
    CONSTRAINT [FK_RequisitionRoleSkills_ProficiencyLevel] FOREIGN KEY ([ProficiencyLevelID]) REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId]),
    CONSTRAINT [FK_RequisitionRoleSkills_RequisitionRoleSkillsMapping] FOREIGN KEY ([RoleSkillMappingID]) REFERENCES [dbo].[RequisitionRoleSkillMapping] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_RequisitionRoleSkills_SkillGroup] FOREIGN KEY ([SkillGroupID]) REFERENCES [dbo].[SkillGroup] ([SkillGroupId]),
    CONSTRAINT [FK_RequisitionRoleSkills_Skills] FOREIGN KEY ([SkillID]) REFERENCES [dbo].[Skills] ([SkillId])
);

