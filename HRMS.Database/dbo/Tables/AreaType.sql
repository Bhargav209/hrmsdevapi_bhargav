﻿CREATE TABLE [dbo].[AreaType] (
    [ID]   INT           IDENTITY (1, 1) NOT NULL,
    [Type] VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_AreaType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

