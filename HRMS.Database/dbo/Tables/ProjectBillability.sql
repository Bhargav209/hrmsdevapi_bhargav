﻿CREATE TABLE [dbo].[ProjectBillability] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [ProjectId]          INT           NOT NULL,
    [NoOfResources]      INT           NULL,
    [BillablePercentage] DECIMAL (18)  NULL,
    [EffectiveDate]      DATE          NULL,
    [IsActive]           BIT           NULL,
    [Remarks]            VARCHAR (250) NULL,
    [CreatedBy]          VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      NULL,
    [ModifiedBy]         VARCHAR (100) NULL,
    [ModifiedDate]       DATETIME      NULL,
    [SystemInfo]         VARCHAR (100) NULL,
    CONSTRAINT [PK_ProjectBillability] PRIMARY KEY CLUSTERED ([Id] ASC)
);

