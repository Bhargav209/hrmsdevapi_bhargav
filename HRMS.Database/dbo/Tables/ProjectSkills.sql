﻿CREATE TABLE [dbo].[ProjectSkills] (
    [ProjectSkillId]   INT           IDENTITY (1, 1) NOT NULL,
    [ProjectId]        INT           NULL,
    [SkillId]          INT           NULL,
    [IsActive]         BIT           NULL,
    [CreatedUser]      VARCHAR (100) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  NULL,
    [ProficiencyId]    INT           NULL,
    [CompetencyAreaId] INT           NULL,
    [ProjectRoleId]    INT           NULL,
    PRIMARY KEY CLUSTERED ([ProjectSkillId] ASC),
    CONSTRAINT [FK_ProjectSkills_ProjectRoles] FOREIGN KEY ([ProjectRoleId]) REFERENCES [dbo].[ProjectRoles] ([ProjectRoleId])
);

