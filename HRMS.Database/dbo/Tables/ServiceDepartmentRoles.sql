﻿CREATE TABLE [dbo].[ServiceDepartmentRoles] (
    [ServiceDepartmentRoleID] INT           IDENTITY (1, 1) NOT NULL,
    [RoleMasterID]            INT           NOT NULL,
    [EmployeeID]              INT           NOT NULL,
    [IsActive]                BIT           NOT NULL,
    [CreatedUser]             VARCHAR (150) CONSTRAINT [DF_ServiceDepartmentRoles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [CreatedDate]             DATETIME      CONSTRAINT [DF_ServiceDepartmentRoles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedUser]            VARCHAR (150) CONSTRAINT [DF_ServiceDepartmentRoles_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedDate]            DATETIME      NULL,
    [SystemInfo]              VARCHAR (50)  CONSTRAINT [DF_ServiceDepartmentRoles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [DepartmentID]            INT           NOT NULL,
    CONSTRAINT [PK_ServiceDepartmentRoles] PRIMARY KEY CLUSTERED ([ServiceDepartmentRoleID] ASC)
);

