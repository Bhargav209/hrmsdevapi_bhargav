﻿CREATE TABLE [dbo].[AssociateADRDetail](
    [AssociateADRDetailId] [int] IDENTITY(1,1) NOT NULL,
	[AssociateADRMasterId] [int] NOT NULL,
	[AspectId] [int] NOT NULL,
	[KRADefinitionId] [int] NOT NULL,
	[Contribution] [varchar](MAX) NOT NULL,	
	[SelfRating] decimal(3,1) NULL,
	[ManagerComments] [varchar](MAX) NULL,
	[Rating] decimal(3,1) NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,	
	
	CONSTRAINT [PK_AssociateADRDetail] PRIMARY KEY CLUSTERED 
(
	[AssociateADRDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AssociateADRDetail]  WITH CHECK ADD  CONSTRAINT [FK_AssociateADRDetail_AssociateADRMaster] FOREIGN KEY([AssociateADRMasterId])
REFERENCES [dbo].[AssociateADRMaster] ([AssociateADRMasterId])
GO

ALTER TABLE [dbo].[AssociateADRDetail] CHECK CONSTRAINT [FK_AssociateADRDetail_AssociateADRMaster]
GO

ALTER TABLE [dbo].[AssociateADRDetail]  WITH CHECK ADD  CONSTRAINT [FK_AssociateADRDetail_AspectMaster] FOREIGN KEY([AspectId])
REFERENCES [dbo].[AspectMaster] ([AspectId])
GO

ALTER TABLE [dbo].[AssociateADRDetail] CHECK CONSTRAINT [FK_AssociateADRDetail_AspectMaster]
GO

ALTER TABLE [dbo].[AssociateADRDetail]  WITH CHECK ADD  CONSTRAINT [FK_AssociateADRDetail_KRADefinition] FOREIGN KEY([KRADefinitionId])
REFERENCES [dbo].[KRADefinition] ([KRADefinitionId])
GO

ALTER TABLE [dbo].[AssociateADRDetail] CHECK CONSTRAINT [FK_AssociateADRDetail_KRADefinition]
GO