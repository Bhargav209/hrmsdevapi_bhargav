﻿CREATE TABLE [dbo].[MenuMaster] (
    [MenuId]       INT            IDENTITY (1, 1) NOT NULL,
    [Title]        NVARCHAR (50)  NOT NULL,
    [IsActive]     BIT            NULL,
    [Path]         NVARCHAR (250) NULL,
    [DisplayOrder] INT            NOT NULL,
    [ParentId]     INT            NOT NULL,
    [CreatedBy]    VARCHAR (100)  DEFAULT (suser_sname()) NULL,
    [ModifiedBy]   VARCHAR (100)  NULL,
    [CreatedDate]  DATETIME       DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME       DEFAULT (getdate()) NULL,
    [SystemInfo]   VARCHAR (50)   DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [Parameter]    NVARCHAR (50)  NULL,
    [NodeId]       NVARCHAR (50)  NULL,
    [Style]        NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([MenuId] ASC)
);

