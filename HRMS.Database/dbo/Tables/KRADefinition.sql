﻿CREATE TABLE [dbo].[KRADefinition](
	[KRADefinitionId] [int] IDENTITY(1,1) NOT NULL,
	[KRAGroupId] [int] NOT NULL,
	[KRAAspectId] [int] NOT NULL,
	[Metric] [varchar](500) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [varchar](50) NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ModifiedUser] [varchar](50) NULL,
	[SystemInfo] [varchar](50) NULL,
	[FinancialYearId] [int] NOT NULL,
	[KRAOperatorID] [int] NOT NULL,
	[KRAMeasurementTypeID] [int] NOT NULL,
	[KRAScaleMasterID] [int] NULL,
	[TargetValue] [decimal](5, 2) NULL,
	[KRATargetText] [varchar](100) NULL,
	[KRATargetPeriodID] [int] NOT NULL,
	[TargetValueAsDate] [date] NULL,
	[KRAAspectOrder] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[KRADefinitionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[KRADefinition] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[KRADefinition] ADD  DEFAULT (getdate()) FOR [ModifiedDate]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_FinancialYear] FOREIGN KEY([FinancialYearId])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_FinancialYear]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_KRAAspect] FOREIGN KEY([KRAAspectId])
REFERENCES [dbo].[KRAAspectMaster] ([KRAAspectID])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_KRAAspect]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_KRACombination] FOREIGN KEY([KRAGroupId])
REFERENCES [dbo].[KRAGroup] ([KRAGroupId])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_KRACombination]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_KRAMeasurementType] FOREIGN KEY([KRAMeasurementTypeID])
REFERENCES [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_KRAMeasurementType]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_KRAOperator] FOREIGN KEY([KRAOperatorID])
REFERENCES [dbo].[KRAOperator] ([KRAOperatorID])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_KRAOperator]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_KRAScaleMaster] FOREIGN KEY([KRAScaleMasterID])
REFERENCES [dbo].[KRAScaleMaster] ([KRAScaleMasterID])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_KRAScaleMaster]
GO

ALTER TABLE [dbo].[KRADefinition]  WITH CHECK ADD  CONSTRAINT [FK_KRADefinition_KRATargetPeriod] FOREIGN KEY([KRATargetPeriodID])
REFERENCES [dbo].[KRATargetPeriod] ([KRATargetPeriodID])
GO

ALTER TABLE [dbo].[KRADefinition] CHECK CONSTRAINT [FK_KRADefinition_KRATargetPeriod]
GO


