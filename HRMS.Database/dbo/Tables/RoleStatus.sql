﻿CREATE TABLE [dbo].[RoleStatus] (
    [RoleStatusId] INT           IDENTITY (1, 1) NOT NULL,
    [RoleId]       INT           NOT NULL,
    [StatusId]     INT           NOT NULL,
    [CategoryId]   INT           NOT NULL,
    [CreatedUser]  VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  NULL,
    CONSTRAINT [PK_RoleStatus] PRIMARY KEY CLUSTERED ([RoleStatusId] ASC)
);

