﻿CREATE TABLE [dbo].[RoleMaster] (
    [RoleMasterID]           INT            IDENTITY (1, 1) NOT NULL,
    [SGRoleID]               INT            NOT NULL,
    [PrefixID]               INT            NULL,
    [SuffixID]               INT            NULL,
    [DepartmentID]           INT            NULL,
    [RoleDescription]        NVARCHAR (500) NULL,
    [KeyResponsibilities]    NVARCHAR (MAX) NULL,
    [EducationQualification] NVARCHAR (MAX) NULL,
    [IsActive]               BIT            CONSTRAINT [DF_RoleMaster_IsActive] DEFAULT ((1)) NULL,
    [CreatedUser]            VARCHAR (150)  CONSTRAINT [DF_RoleMaster_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]           VARCHAR (150)  NULL,
    [CreatedDate]            DATETIME       CONSTRAINT [DF_RoleMaster_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]           DATETIME       NULL,
    [SystemInfo]             VARCHAR (50)   CONSTRAINT [DF_RoleMaster_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [KRAGroupID]             INT            NULL,
    CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED ([RoleMasterID] ASC),
    CONSTRAINT [FK_RoleMaster_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentId]),
    CONSTRAINT [FK_RoleMaster_SGRolePrefix] FOREIGN KEY ([PrefixID]) REFERENCES [dbo].[SGRolePrefix] ([PrefixID]),
    CONSTRAINT [FK_RoleMaster_SGRoles] FOREIGN KEY ([SGRoleID]) REFERENCES [dbo].[SGRole] ([SGRoleID]),
    CONSTRAINT [FK_RoleMaster_SGRoleSuffix] FOREIGN KEY ([SuffixID]) REFERENCES [dbo].[SGRoleSuffix] ([SuffixID])
);


GO
CREATE NONCLUSTERED INDEX [IX_RoleMaster_DepartmentId]
    ON [dbo].[RoleMaster]([DepartmentID] ASC);

