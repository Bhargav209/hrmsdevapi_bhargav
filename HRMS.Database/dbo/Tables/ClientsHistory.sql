﻿CREATE TABLE [dbo].[ClientsHistory](     
	[ClientHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int]  NOT NULL,
	[ClientCode] [varchar](6) NOT NULL,	
	[ClientRegisterName] [varchar](150) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,	
	[SystemInfo] [varchar](50) NULL,	
 [ClientName] VARCHAR(50) NOT NULL, 
    CONSTRAINT [PK__ClientsHistory] PRIMARY KEY CLUSTERED 
(
	[ClientHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])

