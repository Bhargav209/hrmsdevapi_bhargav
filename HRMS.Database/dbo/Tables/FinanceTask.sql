﻿CREATE TABLE [dbo].[FinanceTask] (
    [FinanceNotificationID] INT          IDENTITY (1, 1) NOT NULL,
    [EmployeeCode]          VARCHAR (50) NOT NULL,
    [TaskID]                INT          NOT NULL,
    [StatusId]              INT          NOT NULL,
    CONSTRAINT [PK_FinanceTask] PRIMARY KEY CLUSTERED ([FinanceNotificationID] ASC),
    CONSTRAINT [FK_FinanceTask_Status] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[FinanceTaskMaster] ([FinanceTaskID])
);

