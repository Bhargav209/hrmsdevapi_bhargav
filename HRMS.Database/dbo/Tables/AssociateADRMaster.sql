﻿CREATE TABLE [dbo].[AssociateADRMaster](
    [AssociateADRMasterId] [int] IDENTITY(1,1) NOT NULL,
	[AssociateKRAMapperId] [int] NOT NULL,
	[ADRCycleID] [int] NOT NULL,
	[StatusId] [int] NOT NULL,	
	[CreatedUser] [varchar](100) NOT NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,	
	
	CONSTRAINT [PK_AssociateADRMaster] PRIMARY KEY CLUSTERED 
(
	[AssociateADRMasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AssociateADRMaster]  WITH CHECK ADD  CONSTRAINT [FK_AssociateADRMaster_AssociateKRAMapper] FOREIGN KEY([AssociateKRAMapperId])
REFERENCES [dbo].[AssociateKRAMapper] ([AssociateKRAMapperId])
GO

ALTER TABLE [dbo].[AssociateADRMaster] CHECK CONSTRAINT [FK_AssociateADRMaster_AssociateKRAMapper]
GO

ALTER TABLE [dbo].[AssociateADRMaster]  WITH CHECK ADD  CONSTRAINT [FK_AssociateADRMaster_ADRCycle] FOREIGN KEY([ADRCycleID])
REFERENCES [dbo].[ADRCycle] ([ADRCycleID])
GO

ALTER TABLE [dbo].[AssociateADRMaster] CHECK CONSTRAINT [FK_AssociateADRMaster_ADRCycle]
GO




