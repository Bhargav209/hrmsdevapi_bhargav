﻿CREATE TABLE [dbo].[KRAAspectMaster] (
    [KRAAspectID]  INT           IDENTITY (1, 1) NOT NULL,
    [AspectId]     INT           NOT NULL,
    [DateCreated]  DATETIME      CONSTRAINT [DF_KRAAspectMaster_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_KRAAspectMaster_CreatedUser] DEFAULT (suser_sname()) NOT NULL,
    [DateModified] DATETIME      CONSTRAINT [DF_KRAAspectMaster_DateModified] DEFAULT (getdate()) NULL,
    [ModifiedUser] VARCHAR (100) CONSTRAINT [DF_KRAAspectMaster_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_KRAAspectMaster_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    [DepartmentId] INT           NOT NULL,
    CONSTRAINT [PK_KRAAspectMaster] PRIMARY KEY CLUSTERED ([KRAAspectID] ASC),
    CONSTRAINT [FK_KRAAspectMaster_Departments] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId])
);


GO
CREATE NONCLUSTERED INDEX [IX_KRAAspectMaster_AspectId]
    ON [dbo].[KRAAspectMaster]([AspectId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_KRAAspectMaster_DepartmentId]
    ON [dbo].[KRAAspectMaster]([DepartmentId] ASC);

