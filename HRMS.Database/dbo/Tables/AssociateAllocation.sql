﻿CREATE TABLE [dbo].[AssociateAllocation] (
    [AssociateAllocationId]     INT           IDENTITY (1, 1) NOT NULL,
    [TRId]                      INT           NULL,
    [ProjectId]                 INT           NULL,
    [EmployeeId]                INT           NULL,
    [RoleMasterId]              INT           NULL,
    [IsActive]                  BIT           NULL,
    [AllocationPercentage]      INT           NULL,
    [InternalBillingPercentage] DECIMAL (18)  NULL,
    [IsCritical]                BIT           NULL,
    [EffectiveDate]             DATE          NULL,
    [AllocationDate]            DATE          NULL,
    [CreatedBy]                 VARCHAR (200) NULL,
    [ModifiedBy]                VARCHAR (200) NULL,
    [CreateDate]                DATETIME      NULL,
    [ModifiedDate]              DATETIME      NULL,
    [SystemInfo]                VARCHAR (100) NULL,
    [ReportingManagerId]        INT           NULL,
    [IsPrimary]                 BIT           CONSTRAINT [DF__Associate__IsPri__37A5467C] DEFAULT ((0)) NULL,
    [IsBillable]                BIT           NULL,
    [InternalBillingRoleId]     INT           NULL,
    [ClientBillingRoleId]       INT           NULL,
    [ReleaseDate]               DATE          NULL,
    [ClientBillingPercentage]   DECIMAL (18)  CONSTRAINT [DF__Associate__Clien__38996AB5] DEFAULT ((0)) NULL,
    [ProgramManagerID]          INT           NULL,
    [LeadID]                    INT           NULL,
    CONSTRAINT [PK_AssociateAllocation] PRIMARY KEY CLUSTERED ([AssociateAllocationId] ASC),
    CONSTRAINT [FK_AssociateAllocation_AllocationPercentage] FOREIGN KEY ([AllocationPercentage]) REFERENCES [dbo].[AllocationPercentage] ([AllocationPercentageID]),
    CONSTRAINT [FK_AssociateAllocation_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociateAllocation_LeadID] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociateAllocation_ProgramManagerID] FOREIGN KEY ([ProgramManagerID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociateAllocation_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([ProjectId]),
    CONSTRAINT [FK_AssociateAllocation_ReportingManagerID] FOREIGN KEY ([ReportingManagerId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociateAllocation_RoleMaster] FOREIGN KEY ([RoleMasterId]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID]),
    CONSTRAINT [FK_AssociateAllocation_TalentRequisition] FOREIGN KEY ([TRId]) REFERENCES [dbo].[TalentRequisition] ([TRId])
);


GO
CREATE NONCLUSTERED INDEX [IX_AssociateAllocation_ProjectId]
    ON [dbo].[AssociateAllocation]([ProjectId] ASC);

