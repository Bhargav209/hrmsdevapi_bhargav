﻿CREATE TABLE [dbo].[RequisitionExpertiseRequirements] (
    [RequisitionExpertiseId] INT           IDENTITY (1, 1) NOT NULL,
    [TRId]                   INT           NOT NULL,
    [ExpertiseAreaId]        INT           NOT NULL,
    [ExpertiseDescription]   VARCHAR (MAX) NULL,
    [CreatedBy]              VARCHAR (50)  NULL,
    [CreatedDate]            DATETIME      NULL,
    [ModifiedBy]             VARCHAR (50)  NULL,
    [ModifiedDate]           DATETIME      NULL,
    [SystemInfo]             VARCHAR (50)  NULL,
    [RoleMasterId]           INT           NULL,
    CONSTRAINT [PK_RequisitionExpertiseRequirements] PRIMARY KEY CLUSTERED ([RequisitionExpertiseId] ASC),
    CONSTRAINT [FK_RequisitionExpertiseRequirements_ExpertiseArea] FOREIGN KEY ([ExpertiseAreaId]) REFERENCES [dbo].[ExpertiseArea] ([ExpertiseId]),
    CONSTRAINT [FK_RequisitionExpertiseRequirements_TalentRequisition] FOREIGN KEY ([TRId]) REFERENCES [dbo].[TalentRequisition] ([TRId])
);

