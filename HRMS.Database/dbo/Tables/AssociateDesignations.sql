﻿CREATE TABLE [dbo].[AssociateDesignations] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]    INT           NULL,
    [DesignationId] INT           NULL,
    [GradeId]       INT           NULL,
    [CreatedBy]     VARCHAR (100) NULL,
    [CreatedDate]   DATETIME      NULL,
    [SystemInfo]    VARCHAR (50)  CONSTRAINT [DF_AssociateDesignations_SystemInfo] DEFAULT (getdate()) NULL,
    [FromDate]      DATETIME      NULL,
    [ToDate]        DATETIME      NULL,
    CONSTRAINT [PK_AssociateDesignations] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AssociateDesignations_Designation] FOREIGN KEY ([ID]) REFERENCES [dbo].[AssociateDesignations] ([ID]),
    CONSTRAINT [FK_AssociateDesignations_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

