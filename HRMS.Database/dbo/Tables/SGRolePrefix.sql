﻿CREATE TABLE [dbo].[SGRolePrefix] (
    [PrefixID]    INT            IDENTITY (1, 1) NOT NULL,
    [PrefixName]  VARCHAR (50)   NOT NULL,
    [CreatedBy]   NVARCHAR (150) NULL,
    [CreatedDate] DATETIME       CONSTRAINT [DF_SGRolePrefix_CreatedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]  VARCHAR (50)   NULL,
    CONSTRAINT [PK_SGRolePrefix] PRIMARY KEY CLUSTERED ([PrefixID] ASC)
);

