﻿CREATE TABLE [dbo].[JDTemplateTitleMaster] (
    [TemplateTitleId]     INT           IDENTITY (1, 1) NOT NULL,
    [TemplateTitle]       NVARCHAR (50) NOT NULL,
    [TemplateDescription] NVARCHAR (50) NOT NULL,
    [CreatedUser]         VARCHAR (100) CONSTRAINT [DF_JDTemplateTitleMaster_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]        VARCHAR (100) NULL,
    [CreatedDate]         DATETIME      CONSTRAINT [DF_JDTemplateTitleMaster_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]        DATETIME      NULL,
    [SystemInfo]          VARCHAR (50)  CONSTRAINT [DF_JDTemplateTitleMaster_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    PRIMARY KEY CLUSTERED ([TemplateTitleId] ASC)
);

