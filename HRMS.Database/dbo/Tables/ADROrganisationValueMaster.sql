﻿CREATE TABLE [dbo].[ADROrganisationValueMaster](
	[ADROrganisationValueID] [int] IDENTITY(1,1) NOT NULL,
	[ADROrganisationValue] [varchar](100) NOT NULL,
	[FinancialYearId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ADROrganisationValue] PRIMARY KEY CLUSTERED 
(
	[ADROrganisationValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ADROrganisationValueMaster]  WITH CHECK ADD  CONSTRAINT [FK_ADRADROrganisationValueMaster_FinancialYear] FOREIGN KEY([FinancialYearId])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[ADROrganisationValueMaster] CHECK CONSTRAINT [FK_ADRADROrganisationValueMaster_FinancialYear]
GO