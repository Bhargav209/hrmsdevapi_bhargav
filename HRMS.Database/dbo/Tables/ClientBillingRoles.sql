﻿CREATE TABLE [dbo].[ClientBillingRoles] (
    [ClientBillingRoleId]     INT           IDENTITY (1, 1) NOT NULL,
    [ClientBillingRoleCode]   VARCHAR (50)  NULL,
    [ClientBillingRoleName]   VARCHAR (60)  NULL,
    [IsActive]                BIT           NULL,
    [CreatedUser]             VARCHAR (100) CONSTRAINT [DF_ClientBillingRoles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]            VARCHAR (100) NULL,
    [CreatedDate]             DATETIME      CONSTRAINT [DF_ClientBillingRoles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]            DATETIME      NULL,
    [SystemInfo]              VARCHAR (50)  CONSTRAINT [DF_ClientBillingRoles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [ProjectId]               INT           NOT NULL,
    [NoOfPositions]           INT           NULL,
    [StartDate]               DATE          NULL,
    [EndDate]                 DATE          NULL,
    [ClientBillingPercentage] INT           NULL,
    CONSTRAINT [PK__ClientBillingRoles] PRIMARY KEY CLUSTERED ([ClientBillingRoleId] ASC), 
    CONSTRAINT [FK_ClientBillingRoles_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [Projects]([ProjectId]) ON DELETE CASCADE
);



