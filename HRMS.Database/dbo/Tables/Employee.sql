﻿CREATE TABLE [dbo].[Employee] (
    [EmployeeId]                     INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeCode]                   VARCHAR (50)    NOT NULL,
    [FirstName]                      VARCHAR (100)   NULL,
    [MiddleName]                     VARCHAR (100)   NULL,
    [LastName]                       VARCHAR (100)   NULL,
    [Photograph]                     VARBINARY (MAX) NULL,
    [AccessCardNo]                   VARCHAR (100)   NULL,
    [Gender]                         VARCHAR (10)    NULL,
    [GradeId]                        INT             NULL,
    [Designation]                    INT             NULL,
    [MaritalStatus]                  VARCHAR (50)    NULL,
    [Qualification]                  VARCHAR (100)   NULL,
    [TelephoneNo]                    VARCHAR (30)    NULL,
    [MobileNo]                       VARCHAR (30)    NULL,
    [WorkEmailAddress]               VARCHAR (100)   NULL,
    [PersonalEmailAddress]           VARCHAR (100)   NULL,
    [DateofBirth]                    DATETIME        NULL,
    [JoinDate]                       DATETIME        NULL,
    [ConfirmationDate]               DATETIME        NULL,
    [RelievingDate]                  DATETIME        NULL,
    [BloogGroup]                     VARCHAR (50)    NULL,
    [Nationality]                    VARCHAR (50)    NULL,
    [PANNumber]                      VARCHAR (50)    NULL,
    [PassportNumber]                 VARCHAR (50)    NULL,
    [PassportIssuingOffice]          VARCHAR (50)    NULL,
    [PassportDateValidUpto]          VARCHAR (50)    NULL,
    [ReportingManager]               INT             NULL,
    [ProgramManager]                 INT             NULL,
    [DepartmentId]                   INT             NULL,
    [SystemInfo]                     VARCHAR (50)    CONSTRAINT [DF_Employee_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsActive]                       BIT             NULL,
    [CreatedUser]                    VARCHAR (100)   CONSTRAINT [DF_Employee_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]                   VARCHAR (100)   NULL,
    [CreatedDate]                    DATETIME        CONSTRAINT [DF_Employee_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]                   DATETIME        NULL,
    [DocumentsUploadFlag]            BIT             NULL,
    [CubicalNumber]                  NVARCHAR (50)   NULL,
    [AlternateMobileNo]              VARCHAR (15)    NULL,
    [StatusId]                       INT             NULL,
    [BGVInitiatedDate]               DATETIME        NULL,
    [BGVCompletionDate]              DATETIME        NULL,
    [BGVStatusId]                    INT             NULL,
    [Experience]                     DECIMAL (18, 2) NULL,
    [CompetencyGroup]                INT             NULL,
    [BGVTargetDate]                  DATETIME        NULL,
    [EmployeeTypeId]                 INT             NULL,
    [UserId]                         INT             NULL,
    [ResignationDate]                DATETIME        NULL,
    [BGVStatus]                      VARCHAR (50)    NULL,
    [PAID]                           INT             NULL,
    [HRAdvisor]                      VARCHAR (100)   NULL,
    [UANNumber]                      VARCHAR (50)    NULL,
    [AadharNumber]                   VARCHAR (50)    NULL,
    [PFNumber]                       VARCHAR (50)    NULL,
    [Remarks]                        NVARCHAR (MAX)  NULL,
    [EmploymentStartDate]            DATE            NULL,
    [CareerBreak]                    INT             NULL,
    [TotalExperience]                DECIMAL (18, 2) NULL,
    [ExperienceExcludingCareerBreak] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_EmployeeId] PRIMARY KEY CLUSTERED ([EmployeeId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EMployee_UserId_IsActive]
    ON [dbo].[Employee]([UserId] ASC, [IsActive] ASC)
    INCLUDE([EmployeeId]);


GO
CREATE TRIGGER [dbo].[updateEmployeeData]   
ON dbo.Employee
AFTER UPDATE AS
IF (UPDATE([Designation]) OR UPDATE([GradeId]) OR UPDATE([DepartmentId]) OR UPDATE([CompetencyGroup])) 
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[AssociateHistory] 
	( 
	 [EmployeeID]
	,[DesignationId]
	,[GradeId]
	,[Remarks]
	,[DepartmentId]
	,[PracticeAreaId]
	,[CreatedBy]
	,[CreatedDate]
	,[ModifiedDate]
	,[ModifiedUser]
	,[SystemInfo]
	 )  
     SELECT
        d.EmployeeId,  
        d.Designation,  
        d.GradeId, 
		'History Has been recorded using trigger - updateEmployeeData ',
		d.DepartmentId,
		d.CompetencyGroup,
		d.CreatedUser,
		d.CreatedDate,
		d.ModifiedDate,
		d.ModifiedUser, 
        d.SystemInfo
  	   
	FROM Inserted i
	INNER JOIN Deleted d ON i.EmployeeID = d.EmployeeID
END;

GO
-- =============================================
-- Author:		Bhavani
-- Create date: 14-11-2018 15:51:30
-- Description:	Trigger to update talent pool allocation when ever Technology or Competency is changed
-- =============================================
CREATE TRIGGER [dbo].[trg_updateEmployeePoolAllocation] 
   ON  [dbo].[Employee]
   AFTER UPDATE AS

DECLARE @EmployeeId INT, @AssociateAllocationId INT, @AllocationPercentageId INT, @ReleaseDate Date, @RoleMasterId INT, @PracticeAreaId INT,
@TalentpoolProjectId INT, @ReportingManagerId INT, @ModifiedUser varchar(100), @SystemInfo varchar(100), @ModifiedDate Date, @ProjectTypeId INT

IF (UPDATE([CompetencyGroup])) 
BEGIN  

SELECT @EmployeeId = I.EmployeeId
,@PracticeAreaId = I.CompetencyGroup
,@ModifiedUser = d.ModifiedUser
,@SystemInfo = d.SystemInfo
,@ModifiedDate = I.ModifiedDate
FROM inserted I INNER JOIN deleted d on d.EmployeeId = i.EmployeeId

SELECT @ProjectTypeId= ProjectTypeId FROM ProjectType WHERE ProjectTypeCode = 'Talent Pool' 

UPDATE allocation
SET 
 allocation.IsActive = 0
,allocation.ReleaseDate = CASE WHEN @ModifiedDate >= allocation.EffectiveDate THEN  @ModifiedDate ELSE allocation.EffectiveDate END
,allocation.ModifiedDate = GETDATE()
,allocation.ModifiedBy = @ModifiedUser
,allocation.SystemInfo = @SystemInfo
,@AssociateAllocationId = allocation.AssociateAllocationId
FROM
AssociateAllocation allocation
INNER JOIN Projects project on project.ProjectId = allocation.ProjectId
INNER JOIN ProjectType pt on pt.ProjectTypeId = project.ProjectTypeId
WHERE allocation.EmployeeId = @EmployeeId AND allocation.IsActive = 1 AND pt.ProjectTypeId = @ProjectTypeId

IF(@AssociateAllocationId > 0)
BEGIN
SELECT @RoleMasterId = RoleMasterId, @AllocationPercentageId = AllocationPercentage, @ReleaseDate = ReleaseDate FROM AssociateAllocation WHERE AssociateAllocationId = @AssociateAllocationId

SELECT @TalentpoolProjectId = talentpool.ProjectId, @ReportingManagerId = manager.ReportingManagerID from TalentPool talentpool
INNER JOIN ProjectManagers manager on manager.ProjectID = talentpool.ProjectId 
WHERE talentpool.PracticeAreaId = @PracticeAreaId AND manager.IsActive=1

INSERT INTO [dbo].[AssociateAllocation] 
	( 
	  [TRId]
	 ,[ProjectId]
	 ,[EmployeeId]
	 ,[RoleMasterId]
	 ,[IsActive]
	 ,[AllocationPercentage]
	 ,[InternalBillingPercentage]
	 ,[IsCritical]
	 ,[EffectiveDate]
	 ,[AllocationDate]
	 ,[CreatedBy]
	 ,[CreateDate]
	 ,[SystemInfo]
	 ,[ReportingManagerId]
	 ,[IsPrimary]
	 ,[IsBillable]
	 ,[ClientBillingPercentage]
	 )  
     values
	 (
		 1
		,@TalentpoolProjectId
		,@EmployeeId
		,@RoleMasterId
		,1
		,@AllocationPercentageId
		,0
		,0
		,CASE WHEN @ModifiedDate >= @ReleaseDate THEN  @ModifiedDate ELSE @ReleaseDate END
		,GETDATE()
		,@ModifiedUser
		,GETDATE()
		,@SystemInfo
		,@ReportingManagerID
		,0
		,0
		,0
  	 )  
	
END
END;
