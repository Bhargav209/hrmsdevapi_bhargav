﻿CREATE TABLE [dbo].[EmployeeProjects] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]       INT           NULL,
    [OrganizationName] VARCHAR (100) NULL,
    [ProjectName]      VARCHAR (MAX) NULL,
    [DomainID]         INT           NULL,
    [Duration]         INT           NULL,
    [RoleMasterId]     INT           NULL,
    [KeyAchievements]  VARCHAR (MAX) NULL,
    [IsActive]         BIT           NULL,
    [CreatedUser]      VARCHAR (100) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_EmployeeProject] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_EmployeeProjects_Domain] FOREIGN KEY ([DomainID]) REFERENCES [dbo].[Domain] ([DomainID]),
    CONSTRAINT [FK_EmployeeProjects_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeProjects_RoleMaster] FOREIGN KEY ([RoleMasterId]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
);

