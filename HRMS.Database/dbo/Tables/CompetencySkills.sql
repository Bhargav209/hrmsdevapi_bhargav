﻿CREATE TABLE [dbo].[CompetencySkills] (
    [CompetencySkillsId] INT           IDENTITY (1, 1) NOT NULL,
    [RoleMasterID]       INT           NULL,
    [CompetencyAreaId]   INT           NULL,
    [SkillId]            INT           NULL,
    [SkillGroupID]       INT           NULL,
    [IsActive]           BIT           NULL,
    [CreatedUser]        VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      NULL,
    [ModifiedUser]       VARCHAR (100) NULL,
    [ModifiedDate]       DATETIME      NULL,
    [SystemInfo]         VARCHAR (50)  NULL,
    [IsPrimary]          BIT           NULL,
    [ProficiencyLevelId] INT           NOT NULL,
    CONSTRAINT [PK_CompetencySkills] PRIMARY KEY CLUSTERED ([CompetencySkillsId] ASC),
    CONSTRAINT [FK_CompetencySkills_CompetencyArea] FOREIGN KEY ([CompetencyAreaId]) REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId]),
    CONSTRAINT [FK_CompetencySkills_CompetencySkills] FOREIGN KEY ([CompetencySkillsId]) REFERENCES [dbo].[CompetencySkills] ([CompetencySkillsId]),
    CONSTRAINT [FK_CompetencySkills_RoleMaster] FOREIGN KEY ([RoleMasterID]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID]),
    CONSTRAINT [FK_CompetencySkills_SkillGroupID] FOREIGN KEY ([SkillGroupID]) REFERENCES [dbo].[SkillGroup] ([SkillGroupId]),
    CONSTRAINT [FK_CompetencySkills_Skills] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Skills] ([SkillId]),
    CONSTRAINT [FK_RoleCompSkill_ProficiencyLevel] FOREIGN KEY ([ProficiencyLevelId]) REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId]),
    CONSTRAINT [uc_RoleCompSkill] UNIQUE NONCLUSTERED ([RoleMasterID] ASC, [CompetencyAreaId] ASC, [SkillId] ASC)
);

