﻿CREATE TABLE [dbo].[ProjectRoleDetails] (
    [RoleAssignmentId] INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]       INT           NULL,
    [ProjectId]        INT           NULL,
    [RoleMasterId]     INT           NULL,
    [IsPrimaryRole]    BIT           NULL,
    [StatusId]         INT           NULL,
    [FromDate]         DATETIME      NULL,
    [ToDate]           DATETIME      NULL,
    [IsActive]         BIT           NULL,
    [CreatedBy]        VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      NULL,
    [ModifiedBy]       VARCHAR (100) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  NULL,
    [RejectReason]     VARCHAR (MAX) NULL,
    CONSTRAINT [PK_EmployeeRoleDetails] PRIMARY KEY CLUSTERED ([RoleAssignmentId] ASC),
    CONSTRAINT [FK_EmployeeRoleDetails_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProjectRoleDetails_RoleMaster] FOREIGN KEY ([RoleMasterId]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
);

