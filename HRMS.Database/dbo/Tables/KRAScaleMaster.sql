﻿CREATE TABLE [dbo].[KRAScaleMaster] (
    [KRAScaleMasterID] INT           IDENTITY (1, 1) NOT NULL,
    [MinimumScale]     INT           NOT NULL,
    [MaximumScale]     INT           NOT NULL,
    [KRAScaleTitle]    NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([KRAScaleMasterID] ASC)
);

