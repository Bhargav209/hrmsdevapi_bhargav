﻿CREATE TABLE [dbo].[Addendum] (
    [AddendumId]    INT           IDENTITY (1, 1) NOT NULL,
    [ProjectId]     INT           NOT NULL,
    [SOWId]         VARCHAR (15)  NOT NULL,
    [AddendumNo]    VARCHAR(50)           NULL,
    [RecipientName] VARCHAR (30)  NULL,
    [AddendumDate]  DATETIME      NOT NULL,
    [Note]          VARCHAR (250) NULL,
    [CreatedUser]   VARCHAR (100) NULL,
    [ModifiedUser]  VARCHAR (100) NULL,
    [CreatedDate]   DATETIME      NULL,
    [ModifiedDate]  DATETIME      NULL,
    [SystemInfo]    VARCHAR (50)  NULL,
    [Id]            INT           NOT NULL,
    CONSTRAINT [PK_AddendumId] PRIMARY KEY CLUSTERED ([AddendumId] ASC),
    CONSTRAINT [FK_Addendum_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([ProjectId]),
    CONSTRAINT [FK_Addendum_SOW] FOREIGN KEY ([Id]) REFERENCES [dbo].[SOW] ([Id])
);

