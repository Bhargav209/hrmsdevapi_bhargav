﻿CREATE TABLE [dbo].[KRAGroup] (
    [KRAGroupId]     INT           IDENTITY (1, 1) NOT NULL,
    [DepartmentId]   INT           NOT NULL,
    [RoleCategoryId] INT           NOT NULL,
    [ProjectTypeId]  INT           NULL,
    [KRATitle]       VARCHAR (100) NOT NULL,
    [CreatedDate]    DATETIME      DEFAULT (getdate()) NOT NULL,
    [CreatedUser]    VARCHAR (50)  NULL,
    [ModifiedDate]   DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedUser]   VARCHAR (50)  NULL,
    [SystemInfo]     VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([KRAGroupId] ASC),
    CONSTRAINT [FK_KRACombination_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId]),
    CONSTRAINT [FK_KRACombination_ProjectType] FOREIGN KEY ([ProjectTypeId]) REFERENCES [dbo].[ProjectType] ([ProjectTypeId]),
    CONSTRAINT [FK_KRACombination_RoleCategory] FOREIGN KEY ([RoleCategoryId]) REFERENCES [dbo].[RoleCategory] ([RoleCategoryID])
);


GO
CREATE NONCLUSTERED INDEX [IX_KRAGroup_DepartmentId]
    ON [dbo].[KRAGroup]([DepartmentId] ASC);

