﻿CREATE TABLE [dbo].[KRApdfconfiguration] (
    [KRApdfconfigurationID] INT           IDENTITY (1, 1) NOT NULL,
    [Section1]              VARCHAR (MAX) NULL,
    [Section2]              VARCHAR (MAX) NULL,
    [CreatedBy]             VARCHAR (150) DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]           DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            VARCHAR (150) NULL,
    [ModifiedDate]          DATETIME      NULL,
    [SystemInfo]            VARCHAR (50)  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    PRIMARY KEY CLUSTERED ([KRApdfconfigurationID] ASC)
);

