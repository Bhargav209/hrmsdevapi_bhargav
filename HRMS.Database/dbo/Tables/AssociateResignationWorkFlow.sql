﻿CREATE TABLE [dbo].[AssociateResignationWorkFlow] (
    [WorkFlowID]     INT           IDENTITY (1, 1) NOT NULL,
    [ResignationID]  INT           NOT NULL,
    [FromEmployeeID] INT           NOT NULL,
    [ToEmployeeID]   INT           NULL,
    [StatusID]       INT           NOT NULL,
    [CreatedBy]      VARCHAR (100) NOT NULL,
    [CreatedDate]    DATETIME      NOT NULL,
    CONSTRAINT [PK_AssociateResignationWorkFlow] PRIMARY KEY CLUSTERED ([WorkFlowID] ASC),
    CONSTRAINT [FK_AssociateResignationWorkFlow_AssociateResignation] FOREIGN KEY ([ResignationID]) REFERENCES [dbo].[AssociateResignation] ([ResignationID]),
    CONSTRAINT [FK_AssociateResignationWorkFlow_FromEmployeeID] FOREIGN KEY ([FromEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

