﻿CREATE TABLE [dbo].[AssociatesCertifications] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT           NOT NULL,
    [CertificationID] INT           NOT NULL,
    [ValidFrom]       VARCHAR (4)   NOT NULL,
    [Institution]     VARCHAR (150) NULL,
    [Specialization]  VARCHAR (150) NULL,
    [ValidUpto]       VARCHAR (4)   NULL,
    [CreatedUser]     VARCHAR (100) NULL,
    [ModifiedUser]    VARCHAR (100) NULL,
    [CreatedDate]     DATETIME      NULL,
    [ModifiedDate]    DATETIME      NULL,
    [SystemInfo]      VARCHAR (50)  NULL,
    [SkillGroupID]    INT           NOT NULL,
    CONSTRAINT [PK_AssociatesCertifications] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AssociatesCertifications_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociatesCertifications_SkillGroup] FOREIGN KEY ([SkillGroupID]) REFERENCES [dbo].[SkillGroup] ([SkillGroupId]),
    CONSTRAINT [FK_AssociatesCertifications_Skills] FOREIGN KEY ([CertificationID]) REFERENCES [dbo].[Skills] ([SkillId])
);


GO

CREATE TRIGGER [dbo].[trg_DeleteAssociateCertificateHistory]   
ON [dbo].[AssociatesCertifications]
FOR DELETE AS
--Audit OLD record.  
	INSERT INTO [dbo].[AssociatesCertificationsHistory]
		( 
			 ID
			,EmployeeID
			,CertificationID
			,ValidFrom
			,Institution
			,Specialization
			,ValidUpto
			,SkillGroupID
		)  
	SELECT
			 d.ID
			,d.EmployeeId
			,d.CertificationID
			,d.ValidFrom
			,d.Institution
			,d.Specialization
 			,d.ValidUpto
			,d.SkillGroupId
	FROM Deleted d

GO

CREATE TRIGGER [dbo].[trg_UpdateAssociateCertificates]   
ON [dbo].[AssociatesCertifications]
AFTER UPDATE AS
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[AssociatesCertificationsHistory] 
	(
	 ID
	,EmployeeID
	,CertificationID
	,ValidFrom
	,Institution
	,Specialization
	,ValidUpto
	,SkillGroupID
	)  
     SELECT
		d.ID,
        d.EmployeeId,  
        d.CertificationID,  
        d.ValidFrom,  
        d.Institution,
		d.Specialization,
		d.ValidUpto,
		d.SkillGroupID
	FROM Inserted i
	INNER JOIN Deleted d ON i.ID = d.ID
END;
