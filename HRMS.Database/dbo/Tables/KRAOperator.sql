﻿CREATE TABLE [dbo].[KRAOperator] (
    [KRAOperatorID] INT           IDENTITY (1, 1) NOT NULL,
    [Operator]      NVARCHAR (10) NOT NULL,
    PRIMARY KEY CLUSTERED ([KRAOperatorID] ASC)
);

