﻿CREATE TABLE [dbo].[Award] (
    [AwardId]          INT           IDENTITY (1, 1) NOT NULL,
    [AwardTitle]       VARCHAR (50)  NOT NULL,
    [AwardSubTitle]    VARCHAR (50)  NOT NULL,
    [AwardDescription] VARCHAR (500) NOT NULL,
    [NumberOfAwards]   INT           NULL,
    [PriceAmount]      INT           NULL,
    [CreatedBy]        VARCHAR (100) CONSTRAINT [DF_Award_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]      DATETIME      CONSTRAINT [DF_Award_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       VARCHAR (100) CONSTRAINT [DF_Award_ModifiedBy] DEFAULT (suser_sname()) NULL,
    [ModifiedDate]     DATETIME      CONSTRAINT [DF_Award_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]       VARCHAR (50)  CONSTRAINT [DF_Award_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_Award] PRIMARY KEY CLUSTERED ([AwardId] ASC)
);

