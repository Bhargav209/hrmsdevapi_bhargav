﻿CREATE TABLE [dbo].[ADRAssociateKRAReview] (
    [ID]                           BIGINT        IDENTITY (1, 1) NOT NULL,
    [EmployeeID]                   INT           NOT NULL,
    [KRASetID]                     INT           NOT NULL,
    [ADRCycleID]                   INT           NOT NULL,
    [FinancialYearID]              INT           NOT NULL,
    [CriticalTasksPerformed]       VARCHAR (MAX) NULL,
    [PerformanceAchievement]       VARCHAR (MAX) NULL,
    [PerformanceAchievementAnnual] VARCHAR (MAX) NULL,
    [KRAFulfillmentAnnualID]       INT           NULL,
    [CreatedDate]                  DATETIME      CONSTRAINT [DF_ADRAssociateKRAReview_CreatedDate1] DEFAULT (getdate()) NULL,
    [CreatedUser]                  VARCHAR (150) CONSTRAINT [DF_ADRAssociateKRAReview_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedDate]                 DATETIME      CONSTRAINT [DF_ADRAssociateKRAAspect_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedUser]                 VARCHAR (150) CONSTRAINT [DF_ADRAssociateKRAReview_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]                   VARCHAR (50)  CONSTRAINT [DF_ADRAssociateKRAAspect_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_ADRAssocaiteKRAAspect] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ADRAssociateKRAReview_ADRCycle] FOREIGN KEY ([ADRCycleID]) REFERENCES [dbo].[ADRCycle] ([ADRCycleID]),
    CONSTRAINT [FK_ADRAssociateKRAReview_ADRKRAFulfillement] FOREIGN KEY ([KRAFulfillmentAnnualID]) REFERENCES [dbo].[ADRKRAFulfillement] ([ID]),
    CONSTRAINT [FK_ADRAssociateKRAReview_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ADRAssociateKRAReview_FinancialYear] FOREIGN KEY ([FinancialYearID]) REFERENCES [dbo].[FinancialYear] ([ID])
);

