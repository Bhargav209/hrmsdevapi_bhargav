﻿CREATE TABLE [dbo].[MenuRoles] (
    [MenuRoleId]   INT           IDENTITY (1, 1) NOT NULL,
    [MenuId]       INT           NOT NULL,
    [RoleId]       INT           NULL,
    [CreatedBy]    VARCHAR (100) CONSTRAINT [DF_MenuRoles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedBy]   VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_MenuRoles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      CONSTRAINT [DF_MenuRoles_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_MenuRoles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsActive]     BIT           NULL,
    CONSTRAINT [PK_MenuRoles] PRIMARY KEY CLUSTERED ([MenuRoleId] ASC)
);

