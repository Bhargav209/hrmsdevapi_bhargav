﻿CREATE TABLE [dbo].[KRAWorkFlow] (
    [WorkFlowID]      INT            IDENTITY (1, 1) NOT NULL,
    [FinancialYearID] INT            NOT NULL,
    [StatusID]        INT            NOT NULL,
    [FromEmployeeID]  INT            NOT NULL,
    [ToEmployeeID]    INT            NULL,
    [DepartmentID]    INT            NULL,
    [Comments]        VARCHAR (1500) NULL,
    [CreatedBy]       VARCHAR (100)  CONSTRAINT [DF_KRAWorkFlow_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]     DATETIME       CONSTRAINT [DF_KRAWorkFlow_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_KRAWorkFlow] PRIMARY KEY CLUSTERED ([WorkFlowID] ASC),
    CONSTRAINT [FK_KRAWorkFlow_Employee] FOREIGN KEY ([FromEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

