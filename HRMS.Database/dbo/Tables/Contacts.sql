﻿CREATE TABLE [dbo].[Contacts] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]   INT           NULL,
    [AddressType]  VARCHAR (100) NULL,
    [AddressLine1] VARCHAR (256) NULL,
    [AddressLine2] VARCHAR (256) NULL,
    [City]         VARCHAR (100) NULL,
    [State]        VARCHAR (100) NULL,
    [PostalCode]   VARCHAR (20)  NULL,
    [Country]      VARCHAR (100) NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_Address_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_Address_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_Address_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsActive]     BIT           NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Address_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

