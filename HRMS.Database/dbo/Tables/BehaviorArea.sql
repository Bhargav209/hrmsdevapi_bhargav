﻿CREATE TABLE [dbo].[BehaviorArea] (
    [BehaviorAreaId]          INT           IDENTITY (1, 1) NOT NULL,
    [BehaviorAreaDescription] VARCHAR (100) NOT NULL,
    [IsActive]                BIT           NULL,
    [CreatedBy]               VARCHAR (100) NULL,
    [ModifiedBy]              VARCHAR (100) NULL,
    [CreatedDate]             DATETIME      CONSTRAINT [DF_BehaviorArea_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]            DATETIME      CONSTRAINT [DF_BehaviorArea_ModifiedDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_BehaviorArea] PRIMARY KEY CLUSTERED ([BehaviorAreaId] ASC)
);

