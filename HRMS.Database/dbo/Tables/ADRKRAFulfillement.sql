﻿CREATE TABLE [dbo].[ADRKRAFulfillement] (
    [ID]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50) NOT NULL,
    [IsActive] INT           NOT NULL,
    CONSTRAINT [PK_ADRKRAFulfillement] PRIMARY KEY CLUSTERED ([ID] ASC)
);

