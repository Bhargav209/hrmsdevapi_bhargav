﻿CREATE TABLE [dbo].[KRATargetPeriod] (
    [KRATargetPeriodID] INT           IDENTITY (1, 1) NOT NULL,
    [KRATargetPeriod]   NVARCHAR (30) NOT NULL,
    PRIMARY KEY CLUSTERED ([KRATargetPeriodID] ASC)
);

