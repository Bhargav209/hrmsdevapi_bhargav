﻿CREATE TABLE [dbo].[PreviousEmploymentDetails] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]   INT           NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Address]      VARCHAR (256) NULL,
    [Designation]  VARCHAR (100) NULL,
    [ServiceFrom]  DATETIME      NOT NULL,
    [ServiceTo]    DATETIME      NOT NULL,
    [LeavingReson] VARCHAR (MAX) NULL,
    [IsActive]     BIT           NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_EmploymentDetails_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_EmploymentDetails_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      CONSTRAINT [DF_PreviousEmploymentDetails_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_EmploymentDetails_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_PreviousEmploymentDetails_1] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PreviousEmploymentDetails_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

