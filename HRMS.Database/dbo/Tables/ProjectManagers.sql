﻿CREATE TABLE [dbo].[ProjectManagers] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [ProjectID]          INT           NULL,
    [ReportingManagerID] INT           NULL,
    [ProgramManagerID]   INT           NULL,
    [IsActive]           BIT           NULL,
    [CreatedBy]          VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      CONSTRAINT [DF_ProjectManagers_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedBy]         VARCHAR (100) NULL,
    [ModifiedDate]       DATETIME      CONSTRAINT [DF_ProjectManagers_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]         VARCHAR (50)  NULL,
    [LeadID]             INT           NULL,
    CONSTRAINT [PK_ProjectManagers] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ProjectManagers_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProjectManagers_ProgramManager] FOREIGN KEY ([ProgramManagerID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProjectManagers_Projects] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Projects] ([ProjectId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ProjectManagers_ReportingManager] FOREIGN KEY ([ReportingManagerID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

