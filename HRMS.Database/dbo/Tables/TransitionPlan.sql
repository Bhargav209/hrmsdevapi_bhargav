﻿CREATE TABLE [dbo].[TransitionPlan] (
    [TransitionId] INT           IDENTITY (1, 1) NOT NULL,
    [ProjectId]    INT           NOT NULL,
    [EmployeeId]   INT           NOT NULL,
    [StartDate]    DATE          NOT NULL,
    [EndDate]      DATE          NOT NULL,
    [StatusId]     INT           NOT NULL,
    [CreatedBy]    VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      NULL,
    [ModifiedBy]   VARCHAR (100) NULL,
    [ModifiedDate] DATETIME      NULL,
    [RoleMasterId] INT           NOT NULL,
    CONSTRAINT [PK_TransitionPlan_1] PRIMARY KEY CLUSTERED ([TransitionId] ASC),
    CONSTRAINT [FK_TransitionPlan_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TransitionPlan_RoleMaster] FOREIGN KEY ([RoleMasterId]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
);

