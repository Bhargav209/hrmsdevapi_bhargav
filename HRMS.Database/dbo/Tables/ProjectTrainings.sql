﻿CREATE TABLE [dbo].[ProjectTrainings] (
    [ProjectTrainingId]   INT            IDENTITY (1, 1) NOT NULL,
    [ProjectTrainingCode] NVARCHAR (256) NULL,
    [ProjectTrainingName] NVARCHAR (256) NULL,
    [ProjectTrainingType] NVARCHAR (256) NULL,
    [ProjectId]           INT            NULL,
    [IsActive]            BIT            NULL,
    [CreatedUser]         VARCHAR (100)  CONSTRAINT [DF_ProjectTraining_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]        VARCHAR (100)  NULL,
    [CreatedDate]         DATETIME       CONSTRAINT [DF_ProjectTraining_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]        DATETIME       NULL,
    [SystemInfo]          VARCHAR (50)   CONSTRAINT [DF_ProjectTraining_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_ProjectTraining] PRIMARY KEY CLUSTERED ([ProjectTrainingId] ASC)
);

