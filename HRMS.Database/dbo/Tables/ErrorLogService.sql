﻿CREATE TABLE [dbo].[ErrorLogService] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [FileName]     VARCHAR (50)   NULL,
    [ErrorMessage] NVARCHAR (MAX) NULL,
    [CreatedDate]  NCHAR (10)     NULL
);

