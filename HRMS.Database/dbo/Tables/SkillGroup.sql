﻿CREATE TABLE [dbo].[SkillGroup] (
    [SkillGroupId]     INT           IDENTITY (1, 1) NOT NULL,
    [SkillGroupName]   VARCHAR (100) NOT NULL,
    [Description]      VARCHAR (MAX) NULL,
    [IsActive]         BIT           NULL,
    [CreatedUser]      VARCHAR (100) CONSTRAINT [DF_SkillGroup_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      CONSTRAINT [DF_SkillGroup_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  CONSTRAINT [DF_SkillGroup_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [CompetencyAreaId] INT           NOT NULL,
    CONSTRAINT [PK_SkillGroup] PRIMARY KEY CLUSTERED ([SkillGroupId] ASC),
    CONSTRAINT [FK_SkillGroup_CompetencyArea] FOREIGN KEY ([CompetencyAreaId]) REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId]),
    CONSTRAINT [UC_SkillGroup_SkillGroupName_CompetencyAreaId] UNIQUE NONCLUSTERED ([SkillGroupName] ASC, [CompetencyAreaId] ASC)
);


GO

