﻿CREATE TABLE [dbo].[RequisitionRoleSkillMapping] (
    [ID]                      INT IDENTITY (1, 1) NOT NULL,
    [TRID]                    INT NOT NULL,
    [RequistiionRoleDetailID] INT NOT NULL,
    CONSTRAINT [PK_RequisitionRoleSkillMapping] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RequisitionRoleSkillMapping_RequisitionRoleDetails] FOREIGN KEY ([RequistiionRoleDetailID]) REFERENCES [dbo].[RequisitionRoleDetails] ([RequisitionRoleDetailID])
);

