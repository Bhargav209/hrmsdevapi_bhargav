﻿CREATE TABLE [dbo].[LearningAptitude] (
    [LearningAptitudeId]          INT           IDENTITY (1, 1) NOT NULL,
    [LearningAptitudeDescription] VARCHAR (250) NOT NULL,
    [IsActive]                    BIT           NULL,
    [CreatedBy]                   VARCHAR (100) NULL,
    [ModifiedBy]                  VARCHAR (100) NULL,
    [CreatedDate]                 DATETIME      CONSTRAINT [DF_LearningAptitude_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]                DATETIME      CONSTRAINT [DF_LearningAptitude_ModifiedDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_LearningAptitude] PRIMARY KEY CLUSTERED ([LearningAptitudeId] ASC)
);

