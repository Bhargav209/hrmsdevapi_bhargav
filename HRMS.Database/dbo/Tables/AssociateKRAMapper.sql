﻿CREATE TABLE [dbo].[AssociateKRAMapper](
    [AssociateKRAMapperId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[KRARoleCategoryID] [int] NULL,
	[KRARoleID] [int] NULL,
	[FinancialYearID] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[KRAGroupID] [int] NOT NULL, 
    [IsPDFGenerated] BIT NULL,
	CONSTRAINT [PK_AssociateKRAMapper] PRIMARY KEY CLUSTERED 
(
	[AssociateKRAMapperId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_Employee]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_FinancialYear] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_FinancialYear]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_KRAGroup] FOREIGN KEY([KRAGroupID])
REFERENCES [dbo].[KRAGroup] ([KRAGroupId])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_KRAGroup]
GO