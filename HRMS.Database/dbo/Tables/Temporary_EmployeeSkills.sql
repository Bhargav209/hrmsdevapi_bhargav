﻿CREATE TABLE [dbo].[Temporary_EmployeeSkills] (
    [ID]                   INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]           INT           NOT NULL,
    [CompetencyAreaId]     INT           NOT NULL,
    [SkillGroupId]         INT           NOT NULL,
    [SkillId]              INT           NOT NULL,
    [ProficiencyLevelId]   INT           NOT NULL,
    [IsPrimary]            BIT           NULL,
    [IsActive]             BIT           NOT NULL,
    [Experience]           INT           NULL,
    [LastUsed]             INT           NULL,
    [EmployeeName]         VARCHAR (200) NULL,
    [CompetencyAreaCode]   VARCHAR (150) NULL,
    [SkillGroupName]       VARCHAR (200) NULL,
    [SkillCode]            VARCHAR (200) NULL,
    [ProficiencyLevelCode] VARCHAR (50)  NULL,
    [CreatedUser]          VARCHAR (100) NULL,
    [CreatedDate]          DATETIME      NULL,
    [SystemInfo]           VARCHAR (50)  NULL,
    [EmployeeCode]         VARCHAR (50)  NULL,
    CONSTRAINT [PK_Temporary_EmployeeSkills] PRIMARY KEY CLUSTERED ([EmployeeId] ASC, [SkillId] ASC)
);

