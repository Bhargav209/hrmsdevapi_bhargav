﻿CREATE TABLE [dbo].[TagAssociate] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [TagAssociateListName] VARCHAR (100) NOT NULL,
    [EmployeeID]   INT           NOT NULL,
    [ManagerId]    INT           NOT NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_wishlist_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_wishlist_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  NULL,
    CONSTRAINT [PK_wishlist] PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    FOREIGN KEY ([ManagerId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

