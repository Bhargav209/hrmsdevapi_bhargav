﻿CREATE TABLE [dbo].[AssociateAppreciation] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [ADRCycleID]          INT           NOT NULL,
    [FinancialYearID]     INT           NOT NULL,
    [FromEmployeeID]      INT           NULL,
    [ToEmployeeID]        INT           NULL,
    [AppreciationTypeID]  INT           NOT NULL,
    [AppreciationMessage] VARCHAR (MAX) NULL,
    [AppreciationDate]    DATETIME      CONSTRAINT [DF_AssociateAppreciation_AppreciationDate] DEFAULT (getdate()) NOT NULL,
    [DeleteReason]        VARCHAR (MAX) NULL,
    [IsActive]            BIT           NOT NULL,
    [DateCreated]         DATETIME      CONSTRAINT [DF_AssociateAppreciation_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedUser]         VARCHAR (150) NOT NULL,
    [DateModified]        DATETIME      CONSTRAINT [DF_AssociateAppreciation_DateModified] DEFAULT (getdate()) NULL,
    [ModifiedUser]        VARCHAR (150) NULL,
    [SystemInfo]          VARCHAR (50)  CONSTRAINT [DF_AssociateAppreciation_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_AssociateAppreciation] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AssociateAppreciation_ADRCycle] FOREIGN KEY ([ADRCycleID]) REFERENCES [dbo].[ADRCycle] ([ADRCycleID]),
    CONSTRAINT [FK_AssociateAppreciation_AppreciationType] FOREIGN KEY ([AppreciationTypeID]) REFERENCES [dbo].[AppreciationType] ([ID]),
    CONSTRAINT [FK_AssociateAppreciation_FinancialYear] FOREIGN KEY ([FinancialYearID]) REFERENCES [dbo].[FinancialYear] ([ID]),
    CONSTRAINT [FK_AssociateAppreciation_FromEmployee] FOREIGN KEY ([FromEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociateAppreciation_ToEmployee] FOREIGN KEY ([ToEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

