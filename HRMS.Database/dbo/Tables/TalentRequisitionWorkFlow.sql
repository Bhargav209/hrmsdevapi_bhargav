﻿CREATE TABLE [dbo].[TalentRequisitionWorkFlow] (
    [WorkFlowID]          INT            IDENTITY (1, 1) NOT NULL,
    [TalentRequisitionID] INT            NOT NULL,
    [StatusID]            INT            NOT NULL,
    [FromEmployeeID]      INT            NOT NULL,
    [ToEmployeeID]        VARCHAR (50)   NULL,
    [Comments]            VARCHAR (1500) NULL,
    [CreatedBy]           VARCHAR (100)  CONSTRAINT [DF_TalentRequisitionWorkFlow_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]         DATETIME       CONSTRAINT [DF_TalentRequisitionWorkFlow_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TalentRequisitionWorkFlow] PRIMARY KEY CLUSTERED ([WorkFlowID] ASC),
    CONSTRAINT [FK_TalentRequisitionWorkFlow_Employee] FOREIGN KEY ([FromEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TalentRequisitionWorkFlow_TalentRequisition] FOREIGN KEY ([TalentRequisitionID]) REFERENCES [dbo].[TalentRequisition] ([TRId])
);

