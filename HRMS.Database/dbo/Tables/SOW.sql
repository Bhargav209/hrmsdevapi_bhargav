﻿CREATE TABLE [dbo].[SOW](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SOWId] [varchar](50) NOT NULL,
	[SOWFileName] [varchar](50) NULL,
	[ProjectId] [int] NOT NULL,
	[SOWSignedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK__SOW__3214EC07C0156878] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uk_SOW_Project] UNIQUE NONCLUSTERED 
(
	[SOWId] ASC,
	[ProjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SOW] ADD  CONSTRAINT [DF__SOW__CreatedUser__23942C2E]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[SOW] ADD  CONSTRAINT [DF_SOW_CreatedDate]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[SOW] ADD  CONSTRAINT [DF__SOW__CreatedDate__257C74A0]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[SOW] ADD  CONSTRAINT [DF__SOW__SystemInfo__267098D9]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[SOW]  WITH CHECK ADD  CONSTRAINT [FK_SOW_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[SOW] CHECK CONSTRAINT [FK_SOW_Project]
GO