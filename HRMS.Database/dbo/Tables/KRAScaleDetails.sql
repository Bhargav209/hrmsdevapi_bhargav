﻿CREATE TABLE [dbo].[KRAScaleDetails] (
    [KRAScaleDetailID] INT          IDENTITY (1, 1) NOT NULL,
    [KRAScaleMasterID] INT          NULL,
    [KRAScale]         INT          NOT NULL,
    [ScaleDescription] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([KRAScaleDetailID] ASC),
    CONSTRAINT [FK_KRAScaleDetails_KRAScaleMaster] FOREIGN KEY ([KRAScaleMasterID]) REFERENCES [dbo].[KRAScaleMaster] ([KRAScaleMasterID])
);

