﻿CREATE TABLE [dbo].[SGRole] (
    [SGRoleID]     INT            IDENTITY (1, 1) NOT NULL,
    [SGRoleName]   VARCHAR (100)  NOT NULL,
    [CreatedBy]    NVARCHAR (150) NULL,
    [CreatedDate]  DATETIME       CONSTRAINT [DF_KRARoleMaster_CreatedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]   VARCHAR (50)   NULL,
    [DepartmentId] INT            NOT NULL,
    CONSTRAINT [PK_KRARoleMaster] PRIMARY KEY CLUSTERED ([SGRoleID] ASC),
    CONSTRAINT [IX_KRARoleMaster] UNIQUE NONCLUSTERED ([SGRoleName] ASC)
);

