﻿CREATE TABLE [dbo].[Clients] (
    [ClientId]            INT            IDENTITY (1, 1) NOT NULL,
    [ClientCode]          VARCHAR (6)   NOT NULL,
    [ClientName]          VARCHAR (50)  NOT NULL,
    [ClientRegisterName]     VARCHAR (150)  NULL,
    [IsActive]            BIT            NULL,
    [CreatedUser]         VARCHAR (100)  CONSTRAINT [DF_Clients_CreatedUser] DEFAULT (suser_sname()) NULL,
    [CreatedDate]         DATETIME       CONSTRAINT [DF_Clients_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedUser]        VARCHAR (100)  NULL,
    [ModifiedDate]        DATETIME       NULL,
    [SystemInfo]          VARCHAR (50)   CONSTRAINT [DF_Clients_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [ClientNameHash] NVARCHAR (450) NULL,
    CONSTRAINT [PK__Clients] PRIMARY KEY CLUSTERED ([ClientId] ASC),
    CONSTRAINT [UC_ClientShortNameHash] UNIQUE NONCLUSTERED ([ClientNameHash] ASC),
	CONSTRAINT [UC_ClientCode] UNIQUE NONCLUSTERED ([ClientCode] ASC)
);



