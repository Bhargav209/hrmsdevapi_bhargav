﻿CREATE TABLE [dbo].[DepartmentType] (
    [DepartmentTypeId]          INT          IDENTITY (1, 1) NOT NULL,
    [DepartmentTypeDescription] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([DepartmentTypeId] ASC)
);

