﻿CREATE TABLE [dbo].[AwardType] (
    [AwardTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [AwardType]   NVARCHAR (50) NULL,
    CONSTRAINT [PK_AwardType] PRIMARY KEY CLUSTERED ([AwardTypeId] ASC)
);

