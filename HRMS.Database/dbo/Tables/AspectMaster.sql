﻿CREATE TABLE [dbo].[AspectMaster] (
    [AspectId]     INT           IDENTITY (1, 1) NOT NULL,
    [AspectName]   VARCHAR (70)  NOT NULL,
    [CreatedDate]  DATETIME      DEFAULT (getdate()) NOT NULL,
    [CreatedUser]  VARCHAR (100) DEFAULT (suser_sname()) NOT NULL,
    [ModifiedDate] DATETIME      DEFAULT (getdate()) NULL,
    [ModifiedUser] VARCHAR (100) DEFAULT (suser_sname()) NULL,
    [SystemInfo]   VARCHAR (50)  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    PRIMARY KEY CLUSTERED ([AspectId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_AspectMaster_AspectName]
    ON [dbo].[AspectMaster]([AspectName] ASC);

