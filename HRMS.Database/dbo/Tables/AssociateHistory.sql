﻿CREATE TABLE [dbo].[AssociateHistory] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [EmployeeID]     INT            NOT NULL,
    [DesignationId]  NVARCHAR (100) NULL,
    [GradeId]        INT            NULL,
    [CreatedDate]    DATETIME       CONSTRAINT [DF_AssociateHistory_CreatedDate] DEFAULT (getdate()) NULL,
    [CreatedBy]      NVARCHAR (100) NULL,
    [Remarks]        NVARCHAR (100) NULL,
    [ModifiedDate]   DATETIME       NULL,
    [ModifiedUser]   NVARCHAR (100) NULL,
    [DepartmentId]   INT            NOT NULL,
    [PracticeAreaId] INT            NULL,
    [SystemInfo]     VARCHAR (100)  NULL,
    CONSTRAINT [PK_AssociateHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

