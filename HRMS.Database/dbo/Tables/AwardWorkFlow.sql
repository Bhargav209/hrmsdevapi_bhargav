﻿CREATE TABLE [dbo].[AwardWorkFlow] (
    [AwardWorkFlowId] INT            IDENTITY (1, 1) NOT NULL,
    [FinancialYearId] INT            NOT NULL,
    [StatusId]        INT            NOT NULL,
    [FromEmployeeId]  INT            NOT NULL,
    [ToEmployeeId]    INT            NOT NULL,
    [Comments]        VARCHAR (1500) NOT NULL,
    [CreatedBy]       VARCHAR (100)  CONSTRAINT [DF_AwardWorkFlow_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]     DATETIME       CONSTRAINT [DF_AwardWorkFlow_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AwardWorkFlow] PRIMARY KEY CLUSTERED ([AwardWorkFlowId] ASC),
    CONSTRAINT [FK_AwardWorkFlow_FinancialYear] FOREIGN KEY ([FinancialYearId]) REFERENCES [dbo].[FinancialYear] ([ID]),
    CONSTRAINT [FK_AwardWorkFlow_FromEmployee] FOREIGN KEY ([FromEmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AwardWorkFlow_ToEmployee] FOREIGN KEY ([ToEmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

