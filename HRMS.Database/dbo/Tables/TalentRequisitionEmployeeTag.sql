﻿CREATE TABLE [dbo].[TalentRequisitionEmployeeTag] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [TalentRequisitionID] INT           NULL,
    [EmployeeID]          INT           NULL,
    [RoleMasterID]        INT           NULL,
    [CreatedUser]         VARCHAR (100) CONSTRAINT [DF_TalentRequisitionEmployeeTag_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]        VARCHAR (100) NULL,
    [CreatedDate]         DATETIME      CONSTRAINT [DF_TalentRequisitionEmployeeTag_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]        DATETIME      NULL,
    [SystemInfo]          VARCHAR (50)  CONSTRAINT [DF_TalentRequisitionEmployeeTag_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsUntagged]          BIT           CONSTRAINT [DF_TalentRequisitionEmployeeTag_IsUntagged] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TalentRequisitionEmployeeTag] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TalentRequisitionEmployeeTag_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TalentRequisitionEmployeeTag_RoleMaster] FOREIGN KEY ([RoleMasterID]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID]),
    CONSTRAINT [FK_TalentRequisitionEmployeeTag_TalentRequisition] FOREIGN KEY ([TalentRequisitionID]) REFERENCES [dbo].[TalentRequisition] ([TRId])
);

