﻿CREATE TABLE [dbo].[AllocationPercentage] (
    [AllocationPercentageID] INT           IDENTITY (1, 1) NOT NULL,
    [Percentage]             DECIMAL (18)  NOT NULL,
    [CreatedDate]            DATETIME      CONSTRAINT [DF_AllocationPercentage_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedUser]            VARCHAR (100) CONSTRAINT [DF_AllocationPercentage_CreatedUser] DEFAULT (suser_sname()) NOT NULL,
    [ModifiedDate]           DATETIME      CONSTRAINT [DF_AllocationPercentage_ModifiedDate] DEFAULT (getdate()) NULL,
    [ModifiedUser]           VARCHAR (100) CONSTRAINT [DF_AllocationPercentage_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]             VARCHAR (50)  CONSTRAINT [DF_AllocationPercentage_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_AllocationPercentage] PRIMARY KEY CLUSTERED ([AllocationPercentageID] ASC)
);

