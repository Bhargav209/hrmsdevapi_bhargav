﻿CREATE TABLE [dbo].[KRASubmittedGroup] (
    [KRASubmittedGroupId] INT           IDENTITY (1, 1) NOT NULL,
    [WorkFlowId]          INT           NOT NULL,
    [KRAGroupId]          INT           NOT NULL,
    [CreatedBy]           VARCHAR (100) DEFAULT (suser_sname()) NULL,
    [CreatedDate]         DATETIME      DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([KRASubmittedGroupId] ASC)
);

