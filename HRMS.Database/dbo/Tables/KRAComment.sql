﻿CREATE TABLE [dbo].[KRAComment] (
    [KRACommentId]    INT            IDENTITY (1, 1) NOT NULL,
    [KRAGroupId]      INT            NOT NULL,
    [FinancialYearId] INT            NOT NULL,
    [EmployeeId]      INT            NOT NULL,
    [Comments]        VARCHAR (1500) NULL,
    [CommentedDate]   DATETIME       NOT NULL,
    CONSTRAINT [PK_KRAComment] PRIMARY KEY CLUSTERED ([KRACommentId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRAComments_FinancialYearId]
    ON [dbo].[KRAComment]([FinancialYearId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_KRAComments_GroupId]
    ON [dbo].[KRAComment]([KRAGroupId] ASC);

