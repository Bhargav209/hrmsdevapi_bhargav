﻿CREATE TABLE [dbo].[Roles] (
    [RoleId]                 INT            IDENTITY (1, 1) NOT NULL,
    [RoleName]               NVARCHAR (256) NULL,
    [RoleDescription]        NVARCHAR (256) NULL,
    [IsActive]               BIT            NULL,
    [CreatedUser]            VARCHAR (100)  CONSTRAINT [DF_Roles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]           VARCHAR (100)  NULL,
    [CreatedDate]            DATETIME       CONSTRAINT [DF_Roles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]           DATETIME       NULL,
    [SystemInfo]             VARCHAR (50)   CONSTRAINT [DF_Roles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [DepartmentId]           INT            NULL,
    [KeyResponsibilities]    NVARCHAR (MAX) NULL,
    [EducationQualification] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_RoleId] PRIMARY KEY CLUSTERED ([RoleId] ASC),
    CONSTRAINT [FK_Roles_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId])
);

