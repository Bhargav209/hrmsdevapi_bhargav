﻿CREATE TABLE [dbo].[AssociatesMemberShips] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeID]     INT           NOT NULL,
    [ProgramTitle]   VARCHAR (150) NOT NULL,
    [ValidFrom]      VARCHAR (4)   NOT NULL,
    [Institution]    VARCHAR (150) NULL,
    [Specialization] VARCHAR (150) NULL,
    [ValidUpto]      VARCHAR (4)   NULL,
    [CreatedUser]    VARCHAR (100) NULL,
    [ModifiedUser]   VARCHAR (100) NULL,
    [CreatedDate]    DATETIME      NULL,
    [ModifiedDate]   DATETIME      NULL,
    [SystemInfo]     VARCHAR (50)  NULL,
    CONSTRAINT [PK_AssociatesMemberShips] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AssociatesMemberShips_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);


GO
CREATE TRIGGER [dbo].[trg_DeleteAssociateMembershipHistory]   
ON [dbo].[AssociatesMemberShips]
FOR DELETE AS
--Audit OLD record.  
	INSERT INTO [dbo].[AssociatesMemberShipsHistory]
		( 
			 ID
			,EmployeeID
			,ProgramTitle
			,ValidFrom
			,Institution
			,Specialization
			,ValidUpto
		)  
	SELECT
			 d.ID
			,d.EmployeeId
			,d.ProgramTitle
			,d.ValidFrom
			,d.Institution
			,d.Specialization
 			,d.ValidUpto
	FROM Deleted d

GO

CREATE TRIGGER [dbo].[trg_UpdateAssociateMemberships]   
ON [dbo].[AssociatesMemberShips]
AFTER UPDATE AS
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[AssociatesMemberShipsHistory]
	(
	 ID 
	,EmployeeID
	,ProgramTitle
	,ValidFrom
	,Institution
	,Specialization
	,ValidUpto
	)  
     SELECT
		d.ID,
        d.EmployeeId,  
        d.ProgramTitle,  
        d.ValidFrom,  
        d.Institution,
		d.Specialization,
		d.ValidUpto
	FROM Inserted i
	INNER JOIN Deleted d ON i.ID = d.ID
END;
