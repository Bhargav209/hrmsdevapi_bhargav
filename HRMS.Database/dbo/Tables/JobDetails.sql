﻿CREATE TABLE [dbo].[JobDetails] (
    [JobID]        INT            IDENTITY (1, 1) NOT NULL,
    [JobCode]      NVARCHAR (50)  NOT NULL,
    [GroupID]      NVARCHAR (MAX) NOT NULL,
    [JobInterval]  INT            NULL,
    [Status]       NVARCHAR (50)  NULL,
    [ModifiedUser] NVARCHAR (50)  NULL,
    [CreatedUser]  NVARCHAR (50)  NULL,
    [CreatedDate]  DATETIME       NULL,
    [ModifiedDate] DATETIME       NULL,
    [SystemInfo]   NVARCHAR (50)  NULL,
    CONSTRAINT [PK_JobDetails] PRIMARY KEY CLUSTERED ([JobID] ASC)
);

