﻿CREATE TABLE [dbo].[SkillData] (
    [CompetencyArea] NVARCHAR (MAX)  NULL,
    [SkillGroup]     NVARCHAR (MAX)  NULL,
    [Skill]          VARCHAR (MAX)   NULL,
    [description]    VARBINARY (MAX) NULL
);

