﻿CREATE TABLE [dbo].[lkValue] (
    [ValueKey]         INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [IsActive]         BIT            NULL,
    [ValueID]          NVARCHAR (255) NOT NULL,
    [ValueName]        NVARCHAR (255) NOT NULL,
    [ValueTypeKey]     INT            NOT NULL,
    [CreatedBy]        NVARCHAR (50)  NULL,
    [CreatedDate]      DATETIME       NULL,
    [LastModifiedBy]   NVARCHAR (50)  NULL,
    [LastModifiedDate] DATETIME       NULL,
    CONSTRAINT [PK_lkValue] PRIMARY KEY CLUSTERED ([ValueKey] ASC),
    CONSTRAINT [FK_lkValueType_lkValue] FOREIGN KEY ([ValueTypeKey]) REFERENCES [dbo].[ValueType] ([ValueTypeKey]),
    CONSTRAINT [UNQ_lkValue] UNIQUE NONCLUSTERED ([ValueID] ASC, [ValueTypeKey] ASC)
);

