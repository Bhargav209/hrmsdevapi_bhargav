﻿CREATE TABLE [dbo].[FinancialYear] (
    [ID]       INT IDENTITY (1, 1) NOT NULL,
    [FromYear] INT NOT NULL,
    [ToYear]   INT NOT NULL,
    [IsActive] BIT NOT NULL,
    CONSTRAINT [PK_FinancialYear] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [CHK_FromYearAndToYear] CHECK ([FromYear]<[ToYear] AND [FromYear]>(2015) AND [ToYear]>(2015) AND ([FromYear]>=(2015) AND [FromYear]<=(9999)) AND ([ToYear]>=(2015) AND [ToYear]<=(9999)))
);

