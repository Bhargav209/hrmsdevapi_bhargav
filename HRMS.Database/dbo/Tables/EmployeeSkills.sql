﻿CREATE TABLE [dbo].[EmployeeSkills] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]         INT           NOT NULL,
    [CompetencyAreaId]   INT           NOT NULL,
    [SkillId]            INT           NULL,
    [ProficiencyLevelId] INT           NULL,
    [Experience]         INT           NULL,
    [LastUsed]           INT           NULL,
    [IsPrimary]          BIT           NULL,
    [IsActive]           BIT           NULL,
    [CreatedUser]        VARCHAR (100) CONSTRAINT [DF_EmployeeSkills_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]       VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      CONSTRAINT [DF_EmployeeSkills_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]       DATETIME      NULL,
    [SystemInfo]         VARCHAR (50)  CONSTRAINT [DF_EmployeeSkills_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [SkillGroupId]       INT           NULL,
    [RequisitionId]      INT           NULL,
    CONSTRAINT [PK_EmployeeSkills_1] PRIMARY KEY CLUSTERED ([ID] ASC)
);



