﻿CREATE TABLE [dbo].[AssociatesMemberShipsHistory] (
    [ID]             INT           NOT NULL,
    [EmployeeID]     INT           NULL,
    [ProgramTitle]   VARCHAR (150) NULL,
    [ValidFrom]      VARCHAR (4)   NULL,
    [Institution]    VARCHAR (150) NULL,
    [Specialization] VARCHAR (150) NULL,
    [ValidUpto]      VARCHAR (4)   NULL
);

