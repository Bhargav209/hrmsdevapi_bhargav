﻿CREATE TABLE [dbo].[UploadFiles] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeID]   INT           NOT NULL,
    [FileName]     VARCHAR (MAX) NULL,
    [IsActive]     BIT           NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_UploadFiles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_UploadFiles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_UploadFiles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_UploadFiles] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UploadFiles_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

