﻿CREATE TABLE [dbo].[AssociateUtilize] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [ProjectId]          INT           NOT NULL,
    [EmployeeId]         INT           NOT NULL,
    [IsActive]           BIT           NULL,
    [RoleMasterId]       INT           NULL,
    [PercentUtilization] INT           NOT NULL,
    [Billable]           BIT           NULL,
    [Critical]           BIT           NULL,
    [ManagerId]          INT           NOT NULL,
    [PrimaryManager]     BIT           NULL,
    [ActualStartDate]    DATETIME      NULL,
    [ActualEndDate]      DATETIME      NULL,
    [CreatedUser]        VARCHAR (100) DEFAULT (suser_sname()) NULL,
    [CreatedDate]        DATETIME      DEFAULT (getdate()) NULL,
    [SystemInfo]         VARCHAR (50)  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AssociateUtilize_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

