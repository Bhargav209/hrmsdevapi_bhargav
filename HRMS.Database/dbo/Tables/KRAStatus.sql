﻿CREATE TABLE [dbo].[KRAStatus] (
    [KRAStatusId]     INT IDENTITY (1, 1) NOT NULL,
    [KRAGroupId]      INT NOT NULL,
    [FinancialYearId] INT NOT NULL,
    [StatusId]        INT NOT NULL,
    PRIMARY KEY CLUSTERED ([KRAStatusId] ASC),
    CONSTRAINT [FK_KRAStatus_FinancialYear] FOREIGN KEY ([FinancialYearId]) REFERENCES [dbo].[FinancialYear] ([ID]),
    CONSTRAINT [FK_KRAStatus_KRACombination] FOREIGN KEY ([KRAGroupId]) REFERENCES [dbo].[KRAGroup] ([KRAGroupId])
);


GO
CREATE NONCLUSTERED INDEX [IX_KRAStatus_FinancialYearId]
    ON [dbo].[KRAStatus]([FinancialYearId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_KRAStatus_StatusId]
    ON [dbo].[KRAStatus]([StatusId] ASC);

