﻿CREATE TABLE [dbo].[Users] (
    [UserId]       INT            IDENTITY (1, 1) NOT NULL,
    [UserName]     NVARCHAR (256) NULL,
    [Password]     NVARCHAR (MAX) NULL,
    [EmailAddress] NVARCHAR (254) NOT NULL,
    [IsActive]     BIT            NULL,
    [CreatedUser]  VARCHAR (100)  CONSTRAINT [DF_Users_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100)  NULL,
    [CreatedDate]  DATETIME       CONSTRAINT [DF_Users_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME       NULL,
    [SystemInfo]   VARCHAR (50)   CONSTRAINT [DF_Users_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsSuperAdmin] BIT            NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

