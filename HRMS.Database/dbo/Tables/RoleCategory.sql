﻿CREATE TABLE [dbo].[RoleCategory] (
    [RoleCategoryID]          INT           IDENTITY (1, 1) NOT NULL,
    [RoleCategoryName]        VARCHAR (150) NOT NULL,
    [RoleCategoryDescription] VARCHAR (MAX) NOT NULL,
    [DateCreated]             DATETIME      CONSTRAINT [DF_RoleCategory_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedUser]             VARCHAR (100) CONSTRAINT [DF_RoleCategory_CreatedUser] DEFAULT (suser_sname()) NOT NULL,
    [DateModified]            DATETIME      CONSTRAINT [DF_RoleCategory_DateModified] DEFAULT (getdate()) NULL,
    [ModifiedUser]            VARCHAR (100) CONSTRAINT [DF_RoleCategory_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]              VARCHAR (50)  CONSTRAINT [DF_RoleCategory_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_RoleCategory] PRIMARY KEY CLUSTERED ([RoleCategoryID] ASC)
);

