﻿CREATE TABLE [dbo].[SkillSearch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NULL,
	[FirstName] [varchar](150) NULL,
	[LastName] [varchar](150) NULL,
	[Experience] [decimal](5, 2) NULL,
	[RoleMasterId] [int] NULL,
	[RoleDescription] [varchar](50) NULL,
	[DesignationID] [int] NULL,
	[DesignationCode] [varchar](50) NULL,
	[ProjectCode] [varchar](15) NULL,
	[ProjectName] [varchar](150) NULL,
	[IsPrimary] [bit] NULL,
	[IsCritical] [bit] NULL,
	[IsBillable] [bit] NULL,
	[CompetencyAreaID] [int] NULL,
	[CompetencyAreaCode] [varchar](150) NULL,
	[SkillIGroupID] [int] NULL,
	[SkillGroupName] [varchar](150) NULL,
	[SkillID] [int] NULL,
	[SkillName] [varchar](150) NULL,
	[ProficiencyLevelID] [int] NULL,
	[ProficiencyLevelCode] [varchar](150) NULL,
	[EmployeeCode] [varchar](10) NULL,
	[ProjectId] [int] NULL,
	[IsSkillPrimary] [bit] NULL,
	[DesignationName] [varchar](150) NULL,
	[LastUsed] [int] NULL,
	[SkillExperience] [int] NULL,
 CONSTRAINT [PK_SkillSearch] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SkillSearch] ADD  CONSTRAINT [DF__SkillSear__IsSki__717DADAD]  DEFAULT ((0)) FOR [IsSkillPrimary]
GO

ALTER TABLE [dbo].[SkillSearch]  WITH CHECK ADD  CONSTRAINT [FK__SkillSear__Proje__7271D1E6] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[SkillSearch] CHECK CONSTRAINT [FK__SkillSear__Proje__7271D1E6]
GO


