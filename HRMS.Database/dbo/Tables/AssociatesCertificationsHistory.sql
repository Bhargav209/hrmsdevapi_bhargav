﻿CREATE TABLE [dbo].[AssociatesCertificationsHistory] (
    [ID]              INT           NOT NULL,
    [EmployeeID]      INT           NULL,
    [CertificationID] INT           NULL,
    [ValidFrom]       VARCHAR (4)   NULL,
    [Institution]     VARCHAR (150) NULL,
    [Specialization]  VARCHAR (150) NULL,
    [ValidUpto]       VARCHAR (4)   NULL,
    [SkillGroupID]    INT           NULL
);

