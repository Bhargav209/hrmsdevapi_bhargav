﻿CREATE TABLE [dbo].[Skills] (
    [SkillId]          INT           IDENTITY (1, 1) NOT NULL,
    [SkillCode]        VARCHAR (100) NULL,
    [SkillName]        VARCHAR (256) NULL,
    [SkillDescription] VARCHAR (MAX) NULL,
    [CompetencyAreaId] INT           NULL,
    [IsActive]         BIT           CONSTRAINT [DF_Skills_IsActive] DEFAULT ((1)) NULL,
    [CreatedUser]      VARCHAR (100) CONSTRAINT [DF_Skills_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      CONSTRAINT [DF_Skills_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  CONSTRAINT [DF_Skills_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsApproved]       BIT           CONSTRAINT [DF_Skills_IsApproved] DEFAULT ((0)) NULL,
    [SkillGroupId]     INT           NULL,
    CONSTRAINT [PK__Skills__DFA09187D25470AE] PRIMARY KEY CLUSTERED ([SkillId] ASC),
    CONSTRAINT [FK_Skills_CompetencyArea] FOREIGN KEY ([CompetencyAreaId]) REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId]),
    CONSTRAINT [FK_Skills_SkillGroup] FOREIGN KEY ([SkillGroupId]) REFERENCES [dbo].[SkillGroup] ([SkillGroupId])
);

