﻿CREATE TABLE [dbo].[KRARole] (
    [KRARoleID]          INT           IDENTITY (1, 1) NOT NULL,
    [KRARoleCategoryID]  INT           NOT NULL,
    [KRARoleName]        VARCHAR (150) NOT NULL,
    [KRARoleDescription] VARCHAR (MAX) NOT NULL,
    [DateCreated]        DATETIME      CONSTRAINT [DF_KRARole_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedUser]        VARCHAR (100) CONSTRAINT [DF_KRARole_CreatedUser] DEFAULT (suser_sname()) NOT NULL,
    [DateModified]       DATETIME      CONSTRAINT [DF_KRARole_DateModified] DEFAULT (getdate()) NULL,
    [ModifiedUser]       VARCHAR (100) CONSTRAINT [DF_KRARole_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]         VARCHAR (50)  CONSTRAINT [DF_KRARole_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_KRARole] PRIMARY KEY CLUSTERED ([KRARoleID] ASC),
    CONSTRAINT [FK_KRARole_KRARoleCategory] FOREIGN KEY ([KRARoleCategoryID]) REFERENCES [dbo].[RoleCategory] ([RoleCategoryID])
);

