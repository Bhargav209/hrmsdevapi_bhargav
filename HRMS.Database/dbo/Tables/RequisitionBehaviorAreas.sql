﻿CREATE TABLE [dbo].[RequisitionBehaviorAreas] (
    [RequisitionBehaviorAreaId] INT           IDENTITY (1, 1) NOT NULL,
    [TRId]                      INT           NOT NULL,
    [BehaviorAreaId]            INT           NOT NULL,
    [BehaviorCharacteristics]   VARCHAR (MAX) NULL,
    [BehaviorRatingId]          INT           NULL,
    [CreatedBy]                 VARCHAR (50)  NULL,
    [CreatedDate]               DATETIME      NULL,
    [ModifiedBy]                VARCHAR (50)  NULL,
    [ModifiedDate]              DATETIME      NULL,
    [SystemInfo]                VARCHAR (50)  NULL,
    [RoleMasterId]              INT           NULL,
    CONSTRAINT [PK_RequisitionBehaviorAreas] PRIMARY KEY CLUSTERED ([RequisitionBehaviorAreaId] ASC),
    CONSTRAINT [FK_RequisitionBehaviorAreas_BehaviorArea] FOREIGN KEY ([BehaviorAreaId]) REFERENCES [dbo].[BehaviorArea] ([BehaviorAreaId]),
    CONSTRAINT [FK_RequisitionBehaviorAreas_BehaviorRatings] FOREIGN KEY ([BehaviorRatingId]) REFERENCES [dbo].[BehaviorRatings] ([RatingId]),
    CONSTRAINT [FK_RequisitionBehaviorAreas_TalentRequisition] FOREIGN KEY ([TRId]) REFERENCES [dbo].[TalentRequisition] ([TRId])
);

