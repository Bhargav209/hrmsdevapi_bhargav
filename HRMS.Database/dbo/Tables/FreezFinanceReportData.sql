﻿CREATE TABLE [dbo].[FreezFinanceReportData](
	[Id] [int] IDENTITY(1,1) NOT NULL,	
	[JsonData] [nvarchar](max) NULL,	
	[Month] [int] NULL,
	[Year] [int] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO