﻿CREATE TABLE [dbo].[ProjectRoles] (
    [ProjectRoleId]    INT            IDENTITY (1, 1) NOT NULL,
    [ProjectId]        INT            NOT NULL,
    [RoleMasterId]     INT            NOT NULL,
    [Responsibilities] NVARCHAR (MAX) NULL,
    [IsActive]         BIT            NOT NULL,
    [CreatedUser]      VARCHAR (100)  CONSTRAINT [DF_ProjectRoles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]     VARCHAR (100)  NULL,
    [CreatedDate]      DATETIME       CONSTRAINT [DF_ProjectRoles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]     DATETIME       NULL,
    [SystemInfo]       VARCHAR (50)   CONSTRAINT [DF_ProjectRoles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_ProjectRoles] PRIMARY KEY CLUSTERED ([ProjectRoleId] ASC),
    CONSTRAINT [FK_ProjectRoles_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([ProjectId]),
    CONSTRAINT [FK_ProjectRoles_RoleMaster] FOREIGN KEY ([RoleMasterId]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
);

