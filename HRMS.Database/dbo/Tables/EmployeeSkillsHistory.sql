﻿CREATE TABLE [dbo].[EmployeeSkillsHistory] (
    [ID]                 INT NOT NULL,
    [EmployeeId]         INT NULL,
    [CompetencyAreaId]   INT NULL,
    [SkillId]            INT NULL,
    [ProficiencyLevelId] INT NULL,
    [Experience]         INT NULL,
    [LastUsed]           INT NULL,
    [IsPrimary]          BIT NULL,
    [SkillGroupId]       INT NULL
);

