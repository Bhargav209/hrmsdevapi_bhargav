﻿CREATE TABLE [dbo].[ProficiencyLevel] (
    [ProficiencyLevelId]          INT            IDENTITY (1, 1) NOT NULL,
    [ProficiencyLevelCode]        NVARCHAR (256) NULL,
    [ProficiencyLevelDescription] NVARCHAR (256) NULL,
    [IsActive]                    BIT            NULL,
    [CreatedUser]                 VARCHAR (100)  NULL,
    [CreatedDate]                 DATETIME       NULL,
    [ModifiedUser]                VARCHAR (100)  NULL,
    [ModifiedDate]                DATETIME       NULL,
    [SystemInfo]                  VARCHAR (50)   NULL,
    CONSTRAINT [PK_ProficiencyLevel] PRIMARY KEY CLUSTERED ([ProficiencyLevelId] ASC)
);

