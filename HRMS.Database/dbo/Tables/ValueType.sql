﻿CREATE TABLE [dbo].[ValueType] (
    [ValueTypeKey]     INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [IsActive]         BIT            NULL,
    [ValueTypeID]      NVARCHAR (255) NOT NULL,
    [ValueTypeName]    NVARCHAR (255) NOT NULL,
    [CreatedBy]        NVARCHAR (50)  NULL,
    [CreatedDate]      DATETIME       NULL,
    [LastModifiedBy]   NVARCHAR (50)  NULL,
    [LastModifiedDate] DATETIME       NULL,
    CONSTRAINT [PK_lkValueType] PRIMARY KEY CLUSTERED ([ValueTypeKey] ASC),
    CONSTRAINT [UNQ_lkValueType] UNIQUE NONCLUSTERED ([ValueTypeID] ASC, [IsActive] ASC)
);

