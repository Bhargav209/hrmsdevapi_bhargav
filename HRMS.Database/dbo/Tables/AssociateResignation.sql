﻿CREATE TABLE [dbo].[AssociateResignation] (
    [ResignationID]     INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeID]        INT           NOT NULL,
    [ReasonID]          INT           NOT NULL,
    [ReasonDescription] VARCHAR (MAX) NOT NULL,
    [DateOfResignation] DATETIME      NOT NULL,
    [LastWorkingDate]   DATETIME      NOT NULL,
    [StatusID]          INT           NOT NULL,
    [CreatedBy]         VARCHAR (100) NOT NULL,
    [CreatedDate]       DATETIME      NOT NULL,
    [ModifiedBy]        VARCHAR (100) NULL,
    [ModifiedDate]      DATETIME      NULL,
    [IsActive]          BIT           NULL,
    CONSTRAINT [PK_AssociateResignation] PRIMARY KEY CLUSTERED ([ResignationID] ASC),
    CONSTRAINT [FK_AssociateResignation_AssociateResignation] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

