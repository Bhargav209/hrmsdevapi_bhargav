﻿CREATE TABLE [dbo].[BehaviorRatings] (
    [RatingId]                  INT           IDENTITY (1, 1) NOT NULL,
    [BehaviorRatingDescription] VARCHAR (100) NOT NULL,
    [IsActive]                  BIT           NULL,
    [CreatedBy]                 VARCHAR (100) NULL,
    [ModifiedBy]                VARCHAR (100) NULL,
    [CreatedDate]               DATETIME      CONSTRAINT [DF_BehaviorRatings_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]              DATETIME      CONSTRAINT [DF_BehaviorRatings_ModifiedDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Rating] PRIMARY KEY CLUSTERED ([RatingId] ASC)
);

