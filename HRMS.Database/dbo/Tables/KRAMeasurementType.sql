﻿CREATE TABLE [dbo].[KRAMeasurementType] (
    [KRAMeasurementTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [KRAMeasurementType]   NVARCHAR (70) NOT NULL,
    CONSTRAINT [PK__KRAMeasu__3AB60E6B48BE3E52] PRIMARY KEY CLUSTERED ([KRAMeasurementTypeID] ASC)
);

