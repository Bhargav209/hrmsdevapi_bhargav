﻿CREATE TABLE [dbo].[AllMenus] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [MenuId]       INT           NOT NULL,
    [CreatedBy]    VARCHAR (100) NULL,
    [ModifiedBy]   VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  NULL,
    CONSTRAINT [PK_AllMenus] PRIMARY KEY CLUSTERED ([ID] ASC)
);

