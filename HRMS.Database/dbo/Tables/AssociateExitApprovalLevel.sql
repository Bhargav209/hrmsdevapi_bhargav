﻿CREATE TABLE [dbo].[AssociateExitApprovalLevel] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [ApprovalLevel] INT           NOT NULL,
    [ApprovedBy]    NVARCHAR (50) NOT NULL,
    [isActive]      BIT           NULL,
    CONSTRAINT [PK_AssociateExitApprovalLevel] PRIMARY KEY CLUSTERED ([ID] ASC)
);

