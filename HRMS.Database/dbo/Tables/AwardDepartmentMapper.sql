﻿CREATE TABLE [dbo].[AwardDepartmentMapper] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [AwardId]      INT           NOT NULL,
    [AwardTypeId]  INT           NOT NULL,
    [DepartmentId] INT           NOT NULL,
    [CreatedBy]    VARCHAR (100) CONSTRAINT [DF_AwardDepartmentMapper_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_AwardDepartmentMapper_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   VARCHAR (100) CONSTRAINT [DF_AwardDepartmentMapper_ModifiedBy] DEFAULT (suser_sname()) NULL,
    [ModifiedDate] DATETIME      CONSTRAINT [DF_AwardDepartmentMapper_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_AwardDepartmentMapper_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_AwardDepartmentMapper] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AwardDepartmentMapper_Award] FOREIGN KEY ([AwardId]) REFERENCES [dbo].[Award] ([AwardId]),
    CONSTRAINT [FK_AwardDepartmentMapper_AwardType] FOREIGN KEY ([AwardTypeId]) REFERENCES [dbo].[AwardType] ([AwardTypeId]),
    CONSTRAINT [FK_AwardDepartmentMapper_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId])
);

