﻿CREATE TABLE [dbo].[NotificationType] (
    [NotificationTypeID] INT            NOT NULL,
    [NotificationCode]   NVARCHAR (50)  NOT NULL,
    [NotificationDesc]   NVARCHAR (150) NULL,
    [CreatedUser]        NVARCHAR (100) NULL,
    [CreatedDate]        DATETIME       DEFAULT (getdate()) NULL,
    [ModifiedUser]       NVARCHAR (100) NULL,
    [ModifiedDate]       DATETIME       DEFAULT (getdate()) NULL,
    [SystemInfo]         NVARCHAR (50)  NULL,
    [CategoryId]         INT            NOT NULL,
    CONSTRAINT [FK_NotificationType_CategoryMaster] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[CategoryMaster] ([CategoryID])
);

