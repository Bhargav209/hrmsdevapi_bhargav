﻿CREATE TABLE [dbo].[SGRoleSuffix] (
    [SuffixID]    INT            IDENTITY (1, 1) NOT NULL,
    [SuffixName]  VARCHAR (50)   NOT NULL,
    [CreatedBy]   NVARCHAR (150) NULL,
    [CreatedDate] DATETIME       CONSTRAINT [DF_SGRoleSuffix_CreatedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]  VARCHAR (50)   NULL,
    CONSTRAINT [PK_SGRoleSuffix] PRIMARY KEY CLUSTERED ([SuffixID] ASC)
);

