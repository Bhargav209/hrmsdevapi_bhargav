﻿CREATE TABLE [dbo].[UserRoles] (
    [UserRolesID]  INT           IDENTITY (1, 1) NOT NULL,
    [RoleId]       INT           NULL,
    [IsActive]     BIT           NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_UsersInRoles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_UsersInRoles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_UsersInRoles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [UserId]       INT           NULL,
    [IsPrimary]    BIT           DEFAULT ((0)) NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED ([UserRolesID] ASC),
    CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([RoleId]),
    CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
);

