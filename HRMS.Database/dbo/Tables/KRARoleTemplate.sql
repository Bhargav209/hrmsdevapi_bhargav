﻿CREATE TABLE [dbo].[KRARoleTemplate] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [RoleMasterID]    INT           NOT NULL,
    [FinancialYearID] INT           NOT NULL,
    [StatusID]        INT           NOT NULL,
    [KRAAspectID]     INT           NOT NULL,
    [KRAAspectMetric] VARCHAR (500) NOT NULL,
    [KRAAspectTarget] VARCHAR (250) NOT NULL,
    [DateCreated]     DATETIME      NOT NULL,
    [CreatedUser]     VARCHAR (100) NOT NULL,
    [DateModified]    DATETIME      NULL,
    [ModifiedUser]    VARCHAR (100) NULL,
    [SystemInfo]      VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_KRARoleTepmlate] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_KRARoleTepmlate_KRAAspectMaster] FOREIGN KEY ([FinancialYearID]) REFERENCES [dbo].[FinancialYear] ([ID]),
    CONSTRAINT [FK_KRARoleTepmlate_RoleMaster] FOREIGN KEY ([RoleMasterID]) REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
);


GO
CREATE NONCLUSTERED INDEX [IX_FinancialYearID]
    ON [dbo].[KRARoleTemplate]([FinancialYearID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleMasterID]
    ON [dbo].[KRARoleTemplate]([RoleMasterID] ASC);

