﻿CREATE TABLE [dbo].[ADRSection](
	[ADRSectionId] [int] IDENTITY(1,1) NOT NULL,
	[ADRSectionName] [varchar](150) NOT NULL,
	[ADRMeasurementAreaId] [int] NOT NULL,
	[FinancialYearId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
	[DepartmentId] [int] NULL,
 CONSTRAINT [PK_ADRSection] PRIMARY KEY CLUSTERED 
(
	[ADRSectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ADRSection]  WITH CHECK ADD  CONSTRAINT [FK_ADRSection_ADRMeasurementAreaId] FOREIGN KEY([ADRMeasurementAreaId])
REFERENCES [dbo].[ADRMeasurementAreas] ([ADRMeasurementAreaId])
GO

ALTER TABLE [dbo].[ADRSection] CHECK CONSTRAINT [FK_ADRSection_ADRMeasurementAreaId]
GO

ALTER TABLE [dbo].[ADRSection]  WITH CHECK ADD  CONSTRAINT [FK_ADRSections_FinancialYear] FOREIGN KEY([FinancialYearId])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[ADRSection] CHECK CONSTRAINT [FK_ADRSections_FinancialYear]
GO
ALTER TABLE [dbo].[ADRSection]  WITH CHECK ADD  CONSTRAINT [FK_ADRSection_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO

ALTER TABLE [dbo].[ADRSection] CHECK CONSTRAINT [FK_ADRSection_Departments]
GO