﻿CREATE TABLE [dbo].[ProjectsHistory](
    [ProjectHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[ProjectCode] [varchar](30) NOT NULL,
	[ProjectName] [varchar](100) NOT NULL,
	[ClientId] [int] NOT NULL,
	[ProjectTypeId] [int] NULL,
	[ProjectStateId] [int] NOT NULL,
	[ActualStartDate] [datetime] NOT NULL,
	[ActualEndDate] [datetime] NULL,
	[CreatedUser] [varchar](100) NULL,	
	[CreatedDate] [datetime] NULL,	
	[SystemInfo] [varchar](50) NULL,
	[DepartmentId] [int] NOT NULL,
	[PracticeAreaId] [int] NOT NULL,
	[DomainId] [int] NOT NULL,
 CONSTRAINT [PK__ProjectsHistory__761ABEF07F13ACCB] PRIMARY KEY CLUSTERED 
(
	[ProjectHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
 
