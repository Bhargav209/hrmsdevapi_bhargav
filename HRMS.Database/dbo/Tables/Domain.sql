﻿CREATE TABLE [dbo].[Domain] (
    [DomainID]     INT           IDENTITY (1, 1) NOT NULL,
    [DomainName]   VARCHAR (50)  NOT NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_Domain_CreatedDate] DEFAULT (suser_sname()) NOT NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_Domain_CreatedUser] DEFAULT (suser_sname()) NOT NULL,
    [ModifiedDate] DATETIME      CONSTRAINT [DF_Domain_ModifiedDate] DEFAULT (getdate()) NULL,
    [ModifiedUser] VARCHAR (100) CONSTRAINT [DF_Domain_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_Domain_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_Domain] PRIMARY KEY CLUSTERED ([DomainID] ASC)
);

