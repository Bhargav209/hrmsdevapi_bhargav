﻿CREATE TABLE [dbo].[AddendumDocuments] (
    [AddendumDocumentId] INT           IDENTITY (1, 1) NOT NULL,
    [AddendumId]         INT           NOT NULL,
    [SOWId]              INT           NOT NULL,
    [DocumentName]       VARCHAR (300) NOT NULL,
    [CreatedUser]        VARCHAR (100) NULL,
    [ModifiedUser]       VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      NULL,
    [ModifiedDate]       DATETIME      NULL,
    [SystemInfo]         VARCHAR (50)  NULL,
    CONSTRAINT [PK_AddendumDocumentId] PRIMARY KEY CLUSTERED ([AddendumDocumentId] ASC),
    CONSTRAINT [FK_AddendumDocument_Addendum] FOREIGN KEY ([AddendumId]) REFERENCES [dbo].[Addendum] ([AddendumId]),
    CONSTRAINT [FK_AddendumDocument_SOW] FOREIGN KEY ([SOWId]) REFERENCES [dbo].[SOW] ([Id])
);

