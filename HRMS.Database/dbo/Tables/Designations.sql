﻿CREATE TABLE [dbo].[Designations] (
    [DesignationId]   INT           IDENTITY (1, 1) NOT NULL,
    [DesignationCode] VARCHAR (100) NULL,
    [DesignationName] VARCHAR (256) NULL,
    [IsActive]        BIT           NULL,
    [CreatedUser]     VARCHAR (100) CONSTRAINT [DF_Designations_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]    VARCHAR (100) NULL,
    [CreatedDate]     DATETIME      CONSTRAINT [DF_Designations_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]    DATETIME      NULL,
    [SystemInfo]      VARCHAR (50)  CONSTRAINT [DF_Designations_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [GradeId]         INT           NULL,
    PRIMARY KEY CLUSTERED ([DesignationId] ASC),
    CONSTRAINT [FK_Designations_Grades] FOREIGN KEY ([GradeId]) REFERENCES [dbo].[Grades] ([GradeId])
);

