﻿CREATE TABLE [dbo].[EmployeeType] (
    [EmployeeTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeTypeCode] VARCHAR (100) NULL,
    [EmployeeType]     VARCHAR (100) NULL,
    [CreatedUser]      VARCHAR (100) CONSTRAINT [DF_EmployeeType_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      CONSTRAINT [DF_EmployeeType_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  CONSTRAINT [DF_EmployeeType_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [IsActive]         BIT           NULL,
    PRIMARY KEY CLUSTERED ([EmployeeTypeId] ASC)
);

