﻿CREATE TABLE [dbo].[CustomKRAs] (
    [CustomKRAID]     INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT           NOT NULL,
    [KRAAspectID]     INT           NOT NULL,
    [KRAAspectMetric] VARCHAR (500) NOT NULL,
    [KRAAspectTarget] VARCHAR (250) NOT NULL,
    [CreatedDate]     DATETIME      CONSTRAINT [DF_CustomKRAs_CreatedDate] DEFAULT (getdate()) NULL,
    [CreatedUser]     VARCHAR (100) CONSTRAINT [DF_CustomKRAs_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedDate]    DATETIME      CONSTRAINT [DF_CustomKRAs_ModifiedDate] DEFAULT (getdate()) NULL,
    [ModifiedUser]    VARCHAR (100) CONSTRAINT [DF_CustomKRAs_ModifiedUser] DEFAULT (suser_sname()) NULL,
    [SystemInfo]      VARCHAR (50)  NULL,
    [FinancialYearID] INT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_CustomKRAs] PRIMARY KEY CLUSTERED ([CustomKRAID] ASC),
    CONSTRAINT [FK_CustomKRAs_KRAAspectMaster] FOREIGN KEY ([KRAAspectID]) REFERENCES [dbo].[KRAAspectMaster] ([KRAAspectID])
);


GO
CREATE NONCLUSTERED INDEX [IX_EmployeeID]
    ON [dbo].[CustomKRAs]([EmployeeID] ASC);

