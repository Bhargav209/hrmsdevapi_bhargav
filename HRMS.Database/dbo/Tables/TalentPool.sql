﻿CREATE TABLE [dbo].[TalentPool] (
    [TalentPoolId]   INT           IDENTITY (1, 1) NOT NULL,
    [PracticeAreaId] INT           NULL,
    [ProjectId]      INT           NULL,
    [IsActive]       BIT           NULL,
    [CreatedBy]      NVARCHAR (50) NULL,
    [CreatedDate]    DATETIME      NULL,
    [ModifiedBy]     NVARCHAR (50) NULL,
    [ModifiedDate]   DATETIME      NULL,
    [SystemInfo]     NVARCHAR (50) NULL,
    CONSTRAINT [PK_TalentPool] PRIMARY KEY CLUSTERED ([TalentPoolId] ASC)
);

