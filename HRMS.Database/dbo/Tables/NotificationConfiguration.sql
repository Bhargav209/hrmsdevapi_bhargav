﻿CREATE TABLE [dbo].[NotificationConfiguration] (
    [NotificationConfigID] INT            IDENTITY (1, 1) NOT NULL,
    [NotificationTypeID]   INT            NOT NULL,
    [EmailFrom]            NVARCHAR (MAX) NULL,
    [EmailTo]              NVARCHAR (MAX) NULL,
    [EmailCC]              NVARCHAR (MAX) NULL,
    [EmailSubject]         NVARCHAR (150) NULL,
    [EmailContent]         NVARCHAR (MAX) NULL,
    [SLA]                  INT            NULL,
    [CreatedBy]            NVARCHAR (100) NULL,
    [CreatedDate]          DATETIME       DEFAULT (getdate()) NULL,
    [ModifiedBy]           NVARCHAR (100) NULL,
    [ModifiedDate]         DATETIME       DEFAULT (getdate()) NULL,
    [SystemInfo]           NVARCHAR (50)  NULL,
    [CategoryId]           INT            NOT NULL,
    CONSTRAINT [PK_NotificationConfiguration] PRIMARY KEY CLUSTERED ([NotificationConfigID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NotificationConfiguration]
    ON [dbo].[NotificationConfiguration]([NotificationTypeID] ASC);

