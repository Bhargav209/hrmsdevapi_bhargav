﻿CREATE TABLE [dbo].[Status] (
    [StatusId]          INT            NOT NULL,
    [StatusCode]        NVARCHAR (256) NULL,
    [StatusDescription] NVARCHAR (256) NULL,
    [IsActive]          BIT            NULL,
    [CreatedUser]       VARCHAR (100)  CONSTRAINT [DF_Status_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]      VARCHAR (100)  NULL,
    [CreatedDate]       DATETIME       CONSTRAINT [DF_Status_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]      DATETIME       NULL,
    [SystemInfo]        VARCHAR (50)   CONSTRAINT [DF_Status_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [Category]          VARCHAR (100)  NULL,
    [CategoryID]        INT            NULL,
    CONSTRAINT [FK_Status_CategoryMaster] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[CategoryMaster] ([CategoryID])
);

