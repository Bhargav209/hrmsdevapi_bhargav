﻿CREATE TABLE [dbo].[ExpertiseArea] (
    [ExpertiseId]              INT           IDENTITY (1, 1) NOT NULL,
    [ExpertiseAreaDescription] VARCHAR (MAX) NOT NULL,
    [IsActive]                 BIT           NULL,
    [CreatedBy]                VARCHAR (100) NULL,
    [ModifiedBy]               VARCHAR (100) NULL,
    [CreatedDate]              DATETIME      CONSTRAINT [DF_Expertise_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]             DATETIME      CONSTRAINT [DF_Expertise_ModifiedDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Expertise] PRIMARY KEY CLUSTERED ([ExpertiseId] ASC)
);

