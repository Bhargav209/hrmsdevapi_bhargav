﻿CREATE TABLE [dbo].[ProjectWorkFlow] (
    [WorkFlowId]     INT           IDENTITY (1, 1) NOT NULL,
    [SubmittedBy]    INT           NOT NULL,
    [SubmittedTo]    INT           NOT NULL,
    [SubmittedDate]  DATETIME      NULL,
    [WorkFlowStatus] INT           NOT NULL,
    [ProjectId]      INT           NOT NULL,
    [Comments]       VARCHAR (250) NULL,
    CONSTRAINT [PK_ProjectWorkFlow_1] PRIMARY KEY CLUSTERED ([WorkFlowId] ASC),
    CONSTRAINT [FK_Projects_ProjectId] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([ProjectId])
);

