﻿CREATE TABLE [dbo].[AwardEmployeeMapper] (
    [AwardEmployeeId]   INT           IDENTITY (1, 1) NOT NULL,
    [AwardNominationId] INT           NOT NULL,
    [EmployeeId]        INT           NOT NULL,
    [CreatedBy]         VARCHAR (100) CONSTRAINT [DF_AwardEmployeeMapper_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [CreatedDate]       DATETIME      CONSTRAINT [DF_AwardEmployeeMapper_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        VARCHAR (100) CONSTRAINT [DF_AwardEmployeeMapper_ModifiedBy] DEFAULT (suser_sname()) NULL,
    [ModifiedDate]      DATETIME      CONSTRAINT [DF_AwardEmployeeMapper_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]        VARCHAR (50)  CONSTRAINT [DF_AwardEmployeeMapper_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NOT NULL,
    CONSTRAINT [PK_AwardEmployeeMapper] PRIMARY KEY CLUSTERED ([AwardEmployeeId] ASC),
    CONSTRAINT [FK_AwardEmployeeMapper_AwardNomination] FOREIGN KEY ([AwardNominationId]) REFERENCES [dbo].[AwardNomination] ([AwardNominationId]),
    CONSTRAINT [FK_AwardEmployeeMapper_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

