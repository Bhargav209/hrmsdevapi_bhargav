﻿CREATE TABLE [dbo].[AppreciationType] (
    [ID]   INT           IDENTITY (1, 1) NOT NULL,
    [Type] VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_AppreciationType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

