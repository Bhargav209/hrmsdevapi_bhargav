﻿CREATE VIEW [dbo].[vw_AssociateReject]    
AS    
 SELECT     
   emp.EmployeeId AS empID  
  ,emp.EmployeeCode AS empCode  
  ,emp.FirstName  + ' ' + emp.LastName AS empName    
  ,dept.DepartmentCode AS Department  
  ,desg.DesignationName AS Designation  
 FROM EMPLOYEE emp    
 INNER JOIN Designations desg    
 ON emp.Designation = desg.DesignationId    
 INNER JOIN Departments dept    
 ON emp.DepartmentId = dept.DepartmentId    
 WHERE emp.StatusId = 3 AND emp.IsActive = 1
