﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 10-01-2019    
-- Modified date : 10-01-2019    
-- Modified By  : Mithun    
-- Description  : Get addendums By id for Grid    
-- =================================================    
CREATE PROCEDURE [dbo].[usp_GetAddendums]    
(    
 @Id INT,
 @ProjectId INT 
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON; 
               
 SELECT 
   [AddendumId] AS AddendumId
  ,[ProjectId]  AS ProjectId   
  ,[SOWId] AS SOWId
  ,[AddendumNo] AS AddendumNo
  ,[RecipientName] AS RecipientName
  ,[AddendumDate] AS AddendumDate
  ,[Note] AS Note  
  ,[Id] AS Id
 FROM     
  [dbo].[Addendum]         
WHERE     
  Id=@Id AND ProjectId=@ProjectId
         
END