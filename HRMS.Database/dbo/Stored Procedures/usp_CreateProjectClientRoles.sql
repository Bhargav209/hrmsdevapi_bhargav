﻿-- ========================================================
-- Author			:	Basha
-- Create date		:	12-12-2018
-- Modified date	:	12-12-2018
-- Modified By		:	Basha
-- Description		:	Create project client roles.
-- ======================================================== 

CREATE  PROCEDURE [dbo].[usp_CreateProjectClientRoles]
(  
@ProjectId INT,
@ClientBillingRoleId INT,
@CreatedUser VARCHAR(150),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON;  

 INSERT INTO 
	[dbo].[ProjectClientBillingRoles]  
	(ProjectId,ClientBillingRoleId,CreatedUser,SystemInfo)  
 VALUES  
	(@ProjectId,@ClientBillingRoleId,@CreatedUser,@SystemInfo)  
  
 SELECT @@ROWCOUNT
    
END