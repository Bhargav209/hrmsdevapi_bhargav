﻿  
  
-- =======================================         
-- Author   : Kalyan.Penumutchu              
-- Create date  : 01-05-2019        
-- Modified date :                   
-- Modified By  :               
-- Description  : Returns all employees who has no defined KRAs in a given financial year .              
-- =======================================    
CREATE  PROCEDURE usp_GetEmployeesByKRARoleByFinancialYear    
(    
 @KRARoleID INT,    
 @FinancialYearId INT    
) AS    
BEGIN    
 --Get all employees based on KRARole and Financial Year    
 SELECT  
 emp.EmployeeId AS EmployeeId  
  ,emp.FirstName + ' ' + emp.LastName AS [EmployeeName]    
 FROM     
  AssociateKRARoleMapper akrm JOIN Employee emp ON emp.EmployeeId = akrm.EmployeeId    
 WHERE    
  akrm.KRARoleId = @KRARoleID    
  AND financialyearid = @FinancialYearId    
  AND emp.IsActive = 1    
    
 EXCEPT    
 --Get all employees who has already KRAs in a given Financial Year    
 SELECT  
 emp.EmployeeId,   
  emp.FirstName + ' ' + emp.LastName AS [EmployeeName]    
 FROM     
  AssociateKRAMapper akm JOIN Employee emp ON emp.EmployeeId = akm.EmployeeId    
 WHERE    
  akm.FinancialYearID =  @FinancialYearId    
  AND emp.IsActive = 1    
END