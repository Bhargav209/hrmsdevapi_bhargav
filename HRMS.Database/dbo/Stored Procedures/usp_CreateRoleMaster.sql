﻿-- ========================================================  
-- Author   : Sushmitha  
-- Create date  : 12-02-2018  
-- Modified date : 12-02-2018,09-05-2019  
-- Modified By  : Sushmitha ,Mithun 
-- Description  : Adds roles.  
-- ========================================================   
CREATE PROCEDURE [dbo].[usp_CreateRoleMaster]     
(  
  @SGRoleID   INT  
 ,@PrefixID   INT NULL  
 ,@SuffixID   INT NULL  
 ,@DepartmentID  INT  
 ,@RoleDescription NVARCHAR(500) NULL  
 ,@KeyResponsibilities NVARCHAR(MAX)      
 ,@EducationQualification NVARCHAR(MAX)  
 ,@CreatedDate   DATETIME  
 ,@CreatedUser   VARCHAR(150)  
 ,@SystemInfo   VARCHAR(50)    
 ,@KRAGroupID  INT NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  

   BEGIN TRY  
  INSERT INTO [dbo].[RoleMaster]  
   (  
   SGRoleID    
  ,PrefixID    
  ,SuffixID    
  ,DepartmentID    
  ,RoleDescription    
  ,KeyResponsibilities    
  ,EducationQualification   
  ,CreatedDate    
  ,CreatedUser    
  ,SystemInfo  
  ,KRAGroupID    
   )    
  VALUES    
  (    
   @SGRoleID    
  ,CASE @PrefixID WHEN 0 THEN NULL ELSE @PrefixID END    
  ,CASE @SuffixID WHEN 0 THEN NULL ELSE @SuffixID END   
  ,@DepartmentID    
  ,@RoleDescription    
  ,@KeyResponsibilities    
  ,@EducationQualification  
  ,@CreatedDate    
  ,@CreatedUser    
  ,@SystemInfo   
  ,@KRAGroupID   
   )  
 SELECT @@ROWCOUNT 
 END TRY  
  BEGIN CATCH  
   SELECT ERROR_NUMBER( ) --2627 is Violation in unique index 
  END CATCH 
  
END  
  