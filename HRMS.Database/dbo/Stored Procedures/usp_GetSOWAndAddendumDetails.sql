﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 11-01-2019    
-- Modified date : 11-01-2019    
-- Modified By  : Mithun    
-- Description  : Get SOW And Addendum Details    
-- =================================================    
CREATE PROCEDURE [dbo].[usp_GetSOWAndAddendumDetails]    
(  
  @Id INT
 ,@RoleName VARCHAR(50)    
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON;
 DECLARE @RoleId INT

 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)
 IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head             
 SELECT   
   sow.Id AS Id 
  ,sow.ProjectId AS ProjectId   
  ,sow.SOWId AS SOWId
  ,sow.SOWFileName AS SOWFileName
  ,sow.SOWSignedDate AS SOWSignedDate  
  ,addendum.AddendumId AS AddendumId   
  ,addendum.AddendumNo AS AddendumNo
  ,addendum.RecipientName AS RecipientName
  ,addendum.AddendumDate AS AddendumDate
  ,addendum.Note AS Note  
    
 FROM SOW sow
 INNER JOIN Addendum addendum on sow.Id= addendum.Id     
 WHERE sow.Id=2
 ELSE
 SELECT -1   
              
END