﻿-- ========================================================            
-- Author   : Basha            
-- Create date  : 24-12-2018            
-- Modified date : 24-04-2019      
-- Modified By  : Sabiha            
-- Description  : Add Client            
-- ========================================================             
CREATE PROCEDURE [dbo].[usp_CreateClient]  
(            
 @ClientCode VARCHAR(6)            
 ,@ClientName VARCHAR(50)            
 ,@ClientRegisterName VARCHAR(150)            
 ,@IsActive bit            
 ,@CreatedUser VARCHAR(100)            
 ,@CreatedDate   DATETIME            
 ,@SystemInfo VARCHAR(50)            
)            
AS            
BEGIN            
 SET NOCOUNT ON;            
            
 DECLARE @IsCategoryShortNameExists INT, @ClientShortNameHash varchar(450) ,@ClientId INT          
 SELECT @ClientName= RTRIM(LTRIM(@ClientName))            
        
SELECT @ClientShortNameHash = CONVERT(varchar(50),UPPER(@ClientName));        
SET @ClientShortNameHash  = HashBytes('SHA1', @ClientShortNameHash);    
	
	 BEGIN TRY   
     INSERT INTO Clients            
   (            
      ClientCode              
     ,ClientName              
     ,ClientRegisterName            
     ,IsActive            
     ,ClientNameHash        
     ,CreatedUser              
     ,CreatedDate            
     ,SystemInfo               
   )              
     VALUES              
     (              
     @ClientCode              
     ,@ClientName              
     ,@ClientRegisterName           
     ,@IsActive             
     ,@ClientShortNameHash       
     ,@CreatedUser              
     ,@CreatedDate            
     ,@SystemInfo             
   ) 
          
     SELECT @@ROWCOUNT        
  END TRY          
 BEGIN CATCH         
 SELECT ERROR_NUMBER( )--2627 is Violation in unique index  ;        
 END CATCH          
END 