﻿-- ======================================
-- Author			:	Basha
-- Create date		:	22-11-2018
-- Modified date	:	22-11-2018
-- Modified By		:	Basha
-- Description		:	Update a Actual Experience and Exclu.CareerBreak Experience

-- ======================================
CREATE PROCEDURE [dbo].[usp_UpdateActualExperienceExcluCarrerBreakExperience]
	AS          
BEGIN        
 SET NOCOUNT ON;  
 
 UPDATE Employee SET CareerBreak = 0 WHERE CareerBreak IS NULL


UPDATE Employee set TotalExperience=

			   CASE WHEN  CONVERT(int,(DATEDIFF(MM, EmploymentStartDate,GETDATE()))%12)<10
                                  THEN 
                                  (CONVERT(varchar,  Floor(CAST(DATEDIFF(MM, EmploymentStartDate,GETDATE()) as decimal(18,1))/12))+'.0'+
									 CONVERT(varchar,  DATEDIFF(MM, EmploymentStartDate,GETDATE())%12))
								  ELSE
								   ( CONVERT(varchar,  Floor(CAST(DATEDIFF(MM, EmploymentStartDate,GETDATE()) as decimal(18,1))/12))+'.'+
									CONVERT(varchar,  DATEDIFF(MM, EmploymentStartDate,GETDATE())%12)) END
UPDATE Employee set  ExperienceExcludingCareerBreak=  
    
 CONVERT(varchar, floor((((FLOOR(TotalExperience)*12) +  convert(int,PARSENAME((TotalExperience - Convert(int,FLOOR( TotalExperience))),1)))  
  -floor(case WHEN CareerBreak = 0 then 0 else CareerBreak/30 end))/12)) +    
  (case WHEN convert(int,(((FLOOR(TotalExperience)*12)   +  convert(int,PARSENAME((TotalExperience - Convert(int,FLOOR( TotalExperience))),1)))-  
  floor(case WHEN CareerBreak = 0 then 0 else CareerBreak/30 end)))%12 < 10 THEN  
  ('.0' + convert(varchar,convert(int,(((FLOOR(TotalExperience)*12)   +  convert(int,PARSENAME((TotalExperience - Convert(int,FLOOR( TotalExperience))),1)))-  
  floor(case WHEN CareerBreak = 0 then 0 else CareerBreak/30 end)))%12))  
     
  ELSE   
  ('.' + convert(varchar,convert(int,(((FLOOR(TotalExperience)*12)   +  convert(int,PARSENAME((TotalExperience - Convert(int,FLOOR( TotalExperience))),1)))-  
  floor(case WHEN CareerBreak = 0 then 0 else CareerBreak/30 end)))%12))  
  END)   
  
  FROM Employee       
END  
