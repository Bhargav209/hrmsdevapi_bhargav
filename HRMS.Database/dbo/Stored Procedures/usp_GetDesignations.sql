﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	13-06-2018
-- Modified date	:	13-06-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets list of designations by search string
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetDesignations]
(
@SearchString VARCHAR(20)
)
AS         
BEGIN
	SET NOCOUNT ON;    

	SELECT
		 DesignationId AS Id
	    ,DesignationName AS Name
	FROM
		[dbo].[Designations]
	WHERE
		DesignationName LIKE '%' + @SearchString + '%' AND IsActive = 1

END

