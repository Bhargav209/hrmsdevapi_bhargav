﻿-- ================================================                     
-- Author   : Santosh                    
-- Create date  : 17-04-2018                    
-- Modified date : 02-04-2019                        
-- Modified By  : Bhavani,Mithun                   
-- Description  : Get Utilization Report        
-- [dbo].[usp_rpt_FinanceReportCount] '01-Aug-2018', '31-Aug-2018', 0  
-- ================================================                  
CREATE PROCEDURE [dbo].[usp_rpt_FinanceReportCount]        
(          
 @FromDate DATETIME        
   ,@ToDate DATETIME        
   ,@ProjectID INT            
)               
AS                 
BEGIN    
    
 SET NOCOUNT ON; 
     DECLARE @CategoryId INT
     DECLARE @ProjectStateId INT

     SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'
     SELECT @ProjectStateId = StatusId FROM Status WHERE StatusCode= 'Closed' AND CategoryID = @CategoryId
    
 --TotalCount    
 SELECT      
  COUNT(*) AS [TotalCount]    
 FROM [dbo].[AssociateAllocation] allocation        
 LEFT JOIN [dbo].[ClientBillingRoles] clientrole        
 ON allocation.ClientBillingRoleId = clientrole.ClientBillingRoleId        
 LEFT JOIN [dbo].[InternalBillingRoles] internalrole        
 ON allocation.InternalBillingRoleId = internalrole.InternalBillingRoleId        
 --INNER JOIN [dbo].[RoleMaster] rolemaster        
 --ON allocation.RoleMasterID = rolemaster.RoleMasterID        
 INNER JOIN [dbo].[Projects] project        
 ON allocation.ProjectId = project.ProjectId      
 INNER JOIN ProjectManagers managers ON managers.ProjectID = project.ProjectId AND managers.IsActive=1  
 INNER JOIN [dbo].[Clients] client        
 ON project.ClientId = client.ClientId        
 INNER JOIN [dbo].[Employee] employee        
 ON allocation.EmployeeId = employee.EmployeeId   
 INNER JOIN [dbo].[EmployeeSkills] employeeSkills                      
 ON employee.EmployeeId = employeeSkills.EmployeeId   
 INNER JOIN [dbo].[Skills] skills                      
 ON employeeSkills.SkillId = skills.SkillId           
 INNER JOIN [dbo].[Grades] grade        
 ON employee.GradeId = grade.GradeId        
 INNER JOIN [dbo].[Departments] department        
 ON employee.DepartmentId = department.DepartmentId        
 INNER JOIN [dbo].[Designations] designation        
 ON employee.Designation = designation.DesignationId        
 INNER JOIN [dbo].[AllocationPercentage] allocationpercentage        
 ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID        
 WHERE  project.ProjectStateId <> @ProjectStateId --and  rolemaster.IsActive = 1     
 AND((allocation.EffectiveDate <= @ToDate) AND allocation.EffectiveDate <> ISNULL(allocation.ReleaseDate,''))  
 AND ((allocation.ReleaseDate >= @FromDate AND allocation.ReleaseDate <= @ToDate)  
 OR allocation.ReleaseDate IS NULL OR allocation.IsActive = 1)     
 AND allocation.ProjectId = CASE @ProjectID WHEN 0 THEN allocation.ProjectId ELSE @ProjectID END     
 AND employeeSkills.IsPrimary=1     
END  