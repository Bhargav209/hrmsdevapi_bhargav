﻿-- ========================================================    
-- Author   : Praveen    
-- Create date  : 06-11-2018    
-- Modified date : 27-03-2019, 02-04-2019    
-- Modified By  : Sabiha,Mithun    
-- Description  : Update ClientBillingRole.    
-- [dbo].[usp_UpdateClientBillingRole] 118,'demo is required',153,3,'12-Jan-2019','15-Jan-2019','2019-01-11 19:41:52.900','BHAVANI','123'    
-- ========================================================       
      
  
CREATE PROCEDURE [dbo].[usp_UpdateClientBillingRole]              
(                
  @ClientBillingRoleId INT              
 ,@ClientBillingRoleName VARCHAR(70)               
 ,@ProjectId INT              
 ,@NoOfPositions INT              
 ,@StartDate DATE              
 ,@EndDate DATE              
 ,@ModifiedDate   DATETIME              
 ,@ModifiedUser   VARCHAR(100)              
 ,@SystemInfo   VARCHAR(50)              
 ,@ClientBillingPercentage INT            
)                
AS                
DECLARE @ExistingPositions INT            
 DECLARE @ProjectStateId INT           
 DECLARE @CategoryId INT          
 DECLARE @Drafted INT     
 DECLARE @Closed INT    
 DECLARE @Created INT    
 DECLARE @AllocationCount INT           
          
BEGIN              
              
 SET NOCOUNT ON;           
          
 SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'          
 SELECT @Drafted = StatusId FROM Status WHERE CategoryID=@CategoryId AND StatusCode='Drafted'    
 SELECT @Closed = StatusId FROM Status WHERE CategoryID=@CategoryId AND StatusCode='Closed'    
 SELECT @Created = StatusId FROM Status WHERE CategoryID=@CategoryId AND StatusCode='Created'             
          
 SELECT @ProjectStateId = ProjectStateId FROM Projects WHERE ProjectId=@ProjectId          
              
 SELECT @ExistingPositions = [NoOfPositions] FROM ClientBillingRoles WHERE ClientBillingRoleId = @ClientBillingRoleId AND IsActive=1              
          
 SELECT @AllocationCount = COUNT(*) FROM AssociateAllocation where ProjectId = @ProjectId AND IsBillable = 1 AND ClientBillingRoleId = @ClientBillingRoleId AND @ProjectStateId <> @Closed    
    
 IF(@AllocationCount >= @NoOfPositions AND (@ProjectStateId <> @Drafted OR @ProjectStateId <> @Created)) --    
    SELECT -2    
           
 --IF (@ExistingPositions > @NoOfPositions AND @ProjectStateId <> @Drafted)              
 --   SELECT 0            
              
 ELSE      
   BEGIN      
      IF EXISTS(SELECT 1 FROM ClientBillingRoles WHERE ClientBillingRoleName=@ClientBillingRoleName AND NoOfPositions=@NoOfPositions AND ClientBillingPercentage = @ClientBillingPercentage AND StartDate = @StartDate AND ProjectId = @ProjectId AND ClientBillingRoleId <> @ClientBillingRoleId)          
          SELECT -1           
      ELSE          
        BEGIN            
           INSERT INTO [dbo].[ClientBillingRoleHistory]              
                       ([ClientBillingRoleId]              
                      ,[ClientBillingRoleName]              
                      ,[ProjectId]                  
                      ,[NoOfPositions]              
                      ,[StartDate]              
                      ,[EndDate]            
                      ,[CreatedUser]                         
                      ,[CreatedDate]                         
                      ,[SystemInfo]            
                      ,[ClientBillingPercentage])              
              
           SELECT [ClientBillingRoleId]              
                 ,[ClientBillingRoleName]              
                 ,[ProjectId]                  
                 ,[NoOfPositions]              
                 ,[StartDate]              
                 ,[EndDate]              
                 ,@ModifiedUser                    
                 ,@ModifiedDate                         
                 ,@SystemInfo             
                 ,[ClientBillingPercentage]            
           FROM ClientBillingRoles WHERE ClientBillingRoleId = @ClientBillingRoleId               
            
           UPDATE [dbo].[ClientBillingRoles] SET             
                        [ClientBillingRoleName] =  @ClientBillingRoleName            
                       ,[NoOfPositions]   = @NoOfPositions            
                       ,[StartDate]    = @StartDate            
                       ,[ModifiedUser]    =   @ModifiedUser            
                       ,[EndDate]     = @EndDate          
                       ,[ModifiedDate]    =   @ModifiedDate            
                       ,[SystemInfo]    = @SystemInfo            
                       ,[ClientBillingPercentage]  =   @ClientBillingPercentage            
           WHERE ClientBillingRoleId = @ClientBillingRoleId             
         END           
       END      
            
 IF(@ExistingPositions < @NoOfPositions AND @ProjectStateId <> @Drafted)            
  SELECT 1            
 ELSE            
  --SELECT @@ROWCOUNT            
  SELECT @ClientBillingRoleId            
 END  