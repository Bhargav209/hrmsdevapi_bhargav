﻿-- ========================================    
-- Author   : Santosh       
-- Create date  : 08-01-2017          
-- Modified date : 08-01-2017    
-- Modified By  : Santosh    
-- Description  : Gets Financial Year      
-- ========================================    
CREATE PROCEDURE [dbo].[usp_GetFinancialYear]    
AS         
BEGIN    
    
 SET NOCOUNT ON;      
 SELECT    
  ID  
 ,CAST(FromYear AS VARCHAR(4)) + ' - ' + CAST(ToYear AS VARCHAR(4)) AS Name  
 FROM [dbo].[FinancialYear]    
END

