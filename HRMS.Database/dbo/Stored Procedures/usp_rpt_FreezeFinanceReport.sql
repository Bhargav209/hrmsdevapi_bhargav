﻿
-- ==========================================                   
-- Author   : Mithun                  
-- Create date  : 27-02-2019                  
-- Modified date : 27-02-2019                       
-- Modified By  : Mithun                  
-- Description  : Get Rmg Report Data By Month Year     
--[dbo].[usp_rpt_GetRmgReportDataByMonthYear]      
-- ==========================================                
CREATE PROCEDURE [dbo].[usp_rpt_FreezeFinanceReport]      
(        
  @Month INT      
 ,@Year INT      
 ,@JsonData NVARCHAR(max)  
 ,@CreatedUser  VARCHAR(100)
 ,@CreatedDate  DATETIME
 ,@SystemInfo VARCHAR(50)  
)             
AS               
BEGIN      
 SET NOCOUNT ON;        
       
 INSERT INTO FreezFinanceReportData(
	    Month
	   ,Year
	   ,JsonData
	   ,CreatedUser
	   ,CreatedDate
       ,SystemInfo  
	   )
    VALUES(
	    @Month
	   ,@Year
	   ,@JsonData
	   ,@CreatedUser
	   ,@CreatedDate
	   ,@SystemInfo
	  )
SELECT @@ROWCOUNT
END 
