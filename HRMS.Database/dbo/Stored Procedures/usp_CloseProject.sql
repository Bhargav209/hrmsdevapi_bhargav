﻿
-- ==================================     
-- Author			:  Hemamalini            
-- Create date		:  15-5-2019  
-- Description		:  Sets project state to closed and updates end date of project    
-- ================================== 
create Procedure [dbo].[usp_CloseProject]  
(  
 @ProjectId INT ,
 @EndDate DATETIME,
 @ModifiedUser  VARCHAR(100),            
 @ModifiedDate  DATETIME,            
 @SystemInfo VARCHAR(50)   
)  
AS              
BEGIN              
 SET NOCOUNT ON;
               
 DECLARE @CategoryId INT          
 DECLARE @StatusId INT           
           
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'          
 SELECT @StatusId=StatusId FROM Status WHERE StatusCode = 'Closed' and CategoryID = @CategoryId 

  UPDATE Projects SET                        
            ActualEndDate = @EndDate  
		   ,ProjectStateId = @StatusId                 
           ,ModifiedUser = @ModifiedUser            
           ,ModifiedDate = @ModifiedDate            
           ,SystemInfo = @SystemInfo                  
         WHERE ProjectId = @ProjectId   
		     
   SELECT @@ROWCOUNT          
  
END