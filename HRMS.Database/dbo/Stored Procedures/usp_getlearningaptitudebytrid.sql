﻿-- ================================================    
-- Author			:	Ramya Singamsetti        
-- Create date		:	24-11-2017        
-- Modified date	:	05-06-2018
-- Modified By		:	Santosh
-- Description		:	Get Learning Aptitudes by TalentrequisitionId
-- exec [dbo].[usp_GetLearningAptitudeByTrId] 989  
-- ================================================    
CREATE PROCEDURE [dbo].[usp_getlearningaptitudebytrid]  
(
	@TalentRequisitionId int   
   ,@RoleMasterId INT 
)
AS        
BEGIN    
    
 SET NOCOUNT ON;    
     
 SELECT  
  @TalentRequisitionId AS TalentRequisitionId,   
  CASE WHEN ISNULL(rla.RequisitionLearningAptitudeId,0) = 0 THEN 0 ELSE rla.RequisitionLearningAptitudeId END AS RequisitionLearningAptitudeId,  
  la.LearningAptitudeId,  
  la.LearningAptitudeDescription    
 FROM [dbo].[RequisitionLearningAptitudes] rla  
 RIGHT JOIN [dbo].LearningAptitude la ON rla.LearningAptitudeId=la.LearningAptitudeId AND rla.RoleMasterId = @RoleMasterId   
 AND rla.TRId=@TalentRequisitionId 
 WHERE la.IsActive = 1   
  
END
