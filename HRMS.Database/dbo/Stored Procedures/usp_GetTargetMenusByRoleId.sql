﻿--=======================================================
-- Author			:	Sunitha
-- Create date		:	12-03-2018
-- Modified date	:	12-03-2018
-- Modified By		:	Sunitha
-- Description		:	Get Menus by RoleId
-- ======================================================  
CREATE PROCEDURE [dbo].[usp_GetTargetMenusByRoleId] 
(
	@RoleId INT
)
AS       
BEGIN  
	SET NOCOUNT ON;    
		SELECT DISTINCT
		   menuroles.MenuId
		   ,menumaster.Title as MenuName
		FROM [dbo].[MenuRoles] menuroles
		INNER JOIN [dbo].[MenuMaster] menumaster
		ON menuroles.MenuId = menumaster.MenuId
		WHERE RoleId = @RoleId AND menumaster.IsActive = 1 AND menuroles.IsActive=1
END

