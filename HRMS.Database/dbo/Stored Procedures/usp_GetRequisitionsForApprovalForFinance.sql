﻿-- ========================================================
-- Author			:	Bhavani
-- Create date		:	08-02-2018
-- Modified date	:	08-02-2018
-- Modified By		:	
-- Description		:	Get finance head dashboard that displays all pending for approval requisitons.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRequisitionsForApprovalForFinance] --101
@EmployeeId INT
AS
BEGIN 

SET NOCOUNT ON;
       
BEGIN
	SELECT DISTINCT
		talentRequisiton.TRId  AS TalentrequisitionId
	   ,talentRequisiton.TRCode
	   ,talentRequisiton.RequestedDate
	   ,employee.FirstName + ' ' + employee.LastName AS RequestedBy
	   ,department.DepartmentCode
	   ,projects.ProjectName
	   ,talentRequisiton.RequisitionType
	FROM 
     [dbo].[TalentRequisition] talentRequisiton  
	 INNER JOIN [dbo].[TalentRequisitionWorkFlow] workFlow
	 ON talentRequisiton.TRId = workFlow.TalentRequisitionID
	 LEFT JOIN [dbo].[Projects] projects  
	 ON talentRequisiton.ProjectId = projects.ProjectId
	 INNER JOIN [dbo].[Employee] employee
	 ON talentRequisiton.RaisedBy = employee.EmployeeId
	 INNER JOIN [dbo].[Departments] department
	 ON talentRequisiton.DepartmentId = department.DepartmentId
	WHERE 
	 talentRequisiton.StatusId IN (17) --Pending for approval
	 AND workFlow.ToEmployeeID = @EmployeeId
	ORDER BY RequestedDate DESC
END 
END

