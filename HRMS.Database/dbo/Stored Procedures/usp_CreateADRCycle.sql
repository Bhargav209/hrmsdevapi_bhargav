﻿CREATE PROCEDURE [dbo].[usp_CreateADRCycle]           
(        
 @ADRCycle VARCHAR        
 ,@FinancialYearId INT      
 ,@IsActive BIT        
 ,@DateCreated DATETIME        
 ,@CreatedUser VARCHAR     
 ,@SystemInfo VARCHAR         

)        
AS        
BEGIN        
 SET NOCOUNT ON;         
         
   IF EXISTS(SELECT 1 FROM ADRCycle WHERE (ADRCycle=@ADRCycle AND FinancialYearId=@FinancialYearId ))      
      SELECT -1      
   ELSE      
     BEGIN      
   INSERT INTO [dbo].[ADRCycle]        
           ([ADRCycle]        
           ,[IsActive]
		   ,[DateCreated]        
           ,[CreatedUser]                   
           ,[SystemInfo]        
           ,[FinancialYearId])        
       VALUES        
           (@ADRCycle        
           ,@IsActive        
           ,@CreatedUser                   
           ,@DateCreated                  
           ,@SystemInfo        
           ,@FinancialYearId)          
         
       SELECT @@ROWCOUNT        
  END       
END 