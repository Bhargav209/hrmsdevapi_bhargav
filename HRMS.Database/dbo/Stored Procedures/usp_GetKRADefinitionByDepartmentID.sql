﻿-- =========================================================
-- Author			:	Santosh
-- Create date		:	18-04-2018  
-- Modified date	:	18-04-2018
-- Modified By		:	Santosh
-- Description		:	Get KRA Definitions By Department ID
-- =========================================================
CREATE PROCEDURE [dbo].[usp_GetKRADefinitionByDepartmentID]
(  
  @KRAGroupID INT
 ,@FinancialYearID INT  
 ,@DepartmentID INT  
)  
AS                    
BEGIN                
                
 SET NOCOUNT ON;      
                   
 SELECT  
        aspectMaster.AspectName AS KRAAspectName  
       ,kraDefinition.Metric  
      -- ,kraDefinition.[Target]  
FROM   
       [dbo].[KRADefinition] kraDefinition     
       INNER JOIN [dbo].[KRAGroup] kraGroup      
       ON kraDefinition.KRAGroupId = kraGroup.KRAGroupId   
       INNER JOIN [dbo].[KRAAspectMaster] kraAspect  
       ON kraDefinition.KRAAspectId = kraAspect.KRAAspectID   
       INNER JOIN [dbo].[KRAStatus] kraStatus  
       ON kraGroup.KRAGroupId = kraStatus.KRAGroupId  
       INNER JOIN [dbo].[FinancialYear] financialYear  
       ON kraStatus.FinancialYearId = financialYear.ID
	   INNER JOIN [dbo].[AspectMaster] aspectMaster
       ON kraAspect.AspectId = aspectMaster.AspectId 
WHERE  
       kraDefinition.KRAGroupId = @KRAGroupID 
	   AND kraStatus.FinancialYearId = @FinancialYearId
	   AND kraGroup.DepartmentId = @DepartmentID
              
 END  
