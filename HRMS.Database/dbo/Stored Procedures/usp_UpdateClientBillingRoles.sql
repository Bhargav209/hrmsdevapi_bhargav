﻿-- ======================================================      
-- Author   : Bhavani        
-- Create date  : 02-01-2019    
-- Modified date : 
-- Modified By  :    
-- Description  : Update Project Client Billing Roles
-- [dbo].[usp_UpdateProjectClientBillingRoles]
-- ======================================================              

CREATE   PROCEDURE [dbo].[usp_UpdateClientBillingRoles]          
(            
@ProjectId INT,          
@ClientBillingRoleId VARCHAR(150),
@NoOfPositions INT,          
@CreatedUser VARCHAR(150),            
@SystemInfo VARCHAR(50),
--@ProjectClientBillingRoleId INT NULL,
@AddendumNumber VARCHAR(50) NULL,
@ReasonForUpdate VARCHAR(500) NULL
)            
AS          
BEGIN       
SET NOCOUNT ON;   

	--IF NOT EXISTS(SELECT 1 FROM ProjectClientBillingRoles WHERE ProjectClientBillingRoleId = @ProjectClientBillingRoleId AND ProjectId = @ProjectId AND ClientBillingRoleId = @ClientBillingRoleId AND NoOfPositions = @NoOfPositions AND IsActive = 1)
	
	--UPDATE ProjectClientBillingRoles SET IsActive = 0, ModifiedUser = @CreatedUser, ModifiedDate = GETDATE(), SystemInfo = @SystemInfo WHERE ProjectClientBillingRoleId = @ProjectClientBillingRoleId 

	--INSERT INTO ProjectClientBillingRoles (ProjectId,ClientBillingRoleId,NoOfPositions,CreatedUser,CreatedDate,SystemInfo,IsActive) VALUES 
	--	(@ProjectId,@ClientBillingRoleId,@NoOfPositions,@CreatedUser,GETDATE(),@SystemInfo,1)

END