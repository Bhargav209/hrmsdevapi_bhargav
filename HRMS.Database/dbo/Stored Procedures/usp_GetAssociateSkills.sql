﻿-- ==========================================
-- Author			:	Santosh
-- Create date		:	09-11-2017
-- Modified date	:	15-11-2017                
-- Modified By		:	Santosh      
-- Description		:	Gets associates skills
-- [dbo].[usp_GetAssociateSkills]  161
 -- ==========================================      
CREATE PROCEDURE [dbo].[usp_GetAssociateSkills] 
@EmployeeID INT  
 AS          
BEGIN      
      
 SET NOCOUNT ON;

 SELECT    
 es.ID    
 ,es.CompetencyAreaId    
 ,ca.CompetencyAreaCode AS CompetencyArea  
 ,es.IsActive  
 ,es.SkillGroupID    
 ,sg.SkillGroupName    
 ,es.SkillId    
 ,s.SkillName    
 ,es.Experience    
 ,es.LastUsed    
 ,es.IsPrimary    
 ,es.ProficiencyLevelId    
 ,pl.ProficiencyLevelCode    
 FROM EmployeeSkills es    
 INNER JOIN CompetencyArea ca  
 ON es.CompetencyAreaId = ca.CompetencyAreaId and ca.CompetencyAreaId<>28    
 INNER JOIN SkillGroup sg      
 ON es.SkillGroupID = sg.SkillGroupId      
 INNER JOIN Skills s    
 ON es.SkillId = s.SkillId    
 INNER JOIN ProficiencyLevel pl    
 ON es.ProficiencyLevelId = pl.ProficiencyLevelId    
 WHERE es.EmployeeId = @EmployeeID AND es.IsActive = 1 AND sg.IsActive=1 AND ca.IsActive = 1  
  
 UNION  
   
 SELECT DISTINCT  
 0 AS [Id]  
 ,ca.CompetencyAreaId  
 ,ca.CompetencyAreaCode  AS CompetencyArea  
 ,NULL IsActive  
 ,0 SkillGroupID  
 ,NULL SkillGroupName  
 ,0 AS SkillId  
 ,NULL AS SkillName  
 ,NULL AS Experience  
 ,NULL AS LastUsed  
 ,NULL AS IsPrimary  
 ,0 AS ProficiencyLevelId  
 ,NULL AS ProficiencyLevelCode  
 FROM CompetencyArea ca  
 INNER JOIN SkillGroup sg      
 ON ca.CompetencyAreaId = sg.CompetencyAreaId   
 WHERE ca.IsActive = 1 AND sg.IsActive = 1  and ca.CompetencyAreaId<>28
   
 UNION  
   
 SELECT   
 0 AS [Id]  
 ,sg.CompetencyAreaId  
 ,NULL CompetencyArea  
 ,NULL IsActive  
 ,sg.SkillGroupID  
 ,sg.SkillGroupName  
 ,0 AS SkillId  
 ,NULL AS SkillName  
 ,NULL AS Experience  
 ,NULL AS LastUsed  
 ,NULL AS IsPrimary  
 ,0 AS ProficiencyLevelId  
 ,NULL AS ProficiencyLevelCode  
 FROM SkillGroup sg   
 INNER JOIN CompetencyArea ca ON ca.CompetencyAreaId = sg.CompetencyAreaId 
 WHERE sg.IsActive = 1 AND ca.IsActive=1 and sg.CompetencyAreaId<>28      
END
