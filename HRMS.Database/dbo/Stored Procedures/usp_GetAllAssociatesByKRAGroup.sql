﻿-- ==================================================      
-- Author                  :  Chandra   
-- Create date             :  20-06-2018         
-- Modified date           :  17-5-2019        
-- Modified By             :  Hemamalini      
-- Description             :  GetAllAssociatesByKRAGroup     
-- ====================================================    
create PROCEDURE [dbo].[usp_GetAllAssociatesByKRAGroup]       
AS    
BEGIN    
    
SELECT DISTINCT     
   AssociateKRAMapper.EmployeeID AS EmployeeId    
  ,[emp].EmployeeCode    
  ,[dbo].[udfGetEmployeeFullName](AssociateKRAMapper.EmployeeId) AS AssociateName    
  ,kraGroup.KRAGroupId    
 FROM AssociateKRAMapper AssociateKRAMapper    
 INNER JOIN [Employee] AS [emp] ON [emp].EmployeeId=AssociateKRAMapper.EmployeeID and emp.IsActive=1    
 INNER JOIN [KRAGroup] AS [kraGroup] ON AssociateKRAMapper.KRAGroupId = [kraGroup].[KRAGroupId]   
 WHERE AssociateKRAMapper.IsActive=1 AND AssociateKRAMapper.IsPDFGenerated = 0
    
END    