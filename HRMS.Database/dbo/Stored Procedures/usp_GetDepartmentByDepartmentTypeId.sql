﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	05-06-2018            
-- Modified date	:	13-03-2018            
-- Modified By		:	05-06-2018            
-- Description		:	Gets all departments under department type     
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetDepartmentByDepartmentTypeId]
(
@DepartmentTypeId INT
)
AS
BEGIN

SET NOCOUNT ON;  
	
	SELECT
		 DepartmentId AS Id
		,Description AS Name
	FROM 
		[dbo].[Departments]
	WHERE DepartmentTypeId = @DepartmentTypeId AND IsActive = 1

END

