﻿-- =================================================        
-- AUTHOR  : PRASANNA        
-- CREATE DATE  : 28-01-2019        
-- MODIFIED DATE: 28-01-2019        
-- MODIFIED BY  : SABIHA        
-- DESCRIPTION  : GET WISH LIST NAMES TO POPULATE    
-- =================================================        
CREATE PROCEDURE [dbo].[USP_GetTagAssociateListNames]        
(      
  @MANAGERID INT      
)        
AS                      
BEGIN             
    
 SET NOCOUNT ON;    
 SELECT max(ID) as Id,TagAssociateListName as 'TagListName'    
 FROM TagAssociate     
 WHERE MANAGERID=@MANAGERID    
 group by TagAssociateListName    
    
END