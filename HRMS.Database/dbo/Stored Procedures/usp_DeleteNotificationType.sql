﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	28-03-2018            
-- Modified date	:	28-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Delete Notification type by notification type id
-- ============================================

CREATE PROCEDURE [dbo].[usp_DeleteNotificationType]
(  
  @NotificationTypeID INT,
  @CategoryId INT  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
 
 DECLARE @NotificationConfigurationExists INT=0;
 SELECT @NotificationConfigurationExists =COUNT(NotificationTypeID) FROM NotificationConfiguration WHERE NotificationTypeID=@NotificationTypeID and CategoryId=@CategoryId

 IF @NotificationConfigurationExists <> 0
	BEGIN
		 --If NotificationType with respect to Category exists in [NotificationConfiguration],Then delete record in NotificationConfiguration table
		 DELETE
			  FROM 
				[dbo].[NotificationConfiguration]    
			  WHERE 
				NotificationTypeID = @NotificationTypeID AND CategoryId = @CategoryId

	END
 --and Then ,Then delete record in NotificationType table
  DELETE
	  FROM 
		[dbo].[NotificationType]    
	  WHERE 
		NotificationTypeID = @NotificationTypeID AND CategoryId = @CategoryId

	 SELECT @@ROWCOUNT
END

