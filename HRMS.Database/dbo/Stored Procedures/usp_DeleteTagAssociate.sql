﻿-- ============================================    
-- Author   : Prasanna            
-- Create date  : 28-01-2019    
-- Modified date : 28-01-2019      
-- Description  : Delete wish list    
-- ============================================      
CREATE PROCEDURE [dbo].[usp_DeleteTagAssociate]    
@TagAssociateId INT  
  
AS    
BEGIN    
 SET NOCOUNT ON;      
 Delete from TagAssociate where Id=@TagAssociateId;  
  
 SELECT @@ROWCOUNT  
END