﻿CREATE PROCEDURE [dbo].[usp_UpdateFinancialYearAndStatus]
@ID INT,
@FromYear INT,
@ToYear INT,
@IsActive BIT

AS
 BEGIN
SET NOCOUNT ON;  

 DECLARE 
@currentMonth INT = MONTH(GETDATE()),
@CurrentYear INT = YEAR(GETDATE()),
@FinancialYear INT

--checking if from year already exists or not 

 SELECT @FinancialYear=COUNT(ID) FROM FinancialYear WHERE FromYear = @FromYear AND ToYear = @ToYear AND IsActive = @IsActive
 IF (@FinancialYear > 0)
 BEGIN
	SELECT -1
 END
 ELSE
 BEGIN
 --Checking if fromYear is equal to currentyear  and after march  and isactive =true, able to update
	IF ((@FromYear = @CurrentYear AND @currentMonth > 3) AND  (@IsActive=1))
		BEGIN
			UPDATE 
                [dbo].[FinancialYear]  
			SET 
				IsActive = 0
              
			UPDATE 
                [dbo].[FinancialYear] 
			SET
                IsActive  = @IsActive
			WHERE 
                ID = @ID
          
		  SELECT @@ROWCOUNT 
		END
	ELSE IF ((@FromYear = @CurrentYear-1 AND @currentMonth <= 3) AND  (@IsActive=1))
	BEGIN
		UPDATE 
			[dbo].[FinancialYear] 
		SET 
			IsActive = @IsActive 
		WHERE 
			ID = @ID
	END
	ELSE
		SELECT -11
 END
END
Go