﻿CREATE PROCEDURE [dbo].[usp_GetAllSkillDetails]            
(            
@EmployeeID INT            
)            
AS            
BEGIN            
            
SET NOCOUNT ON;             
        
SELECT  DISTINCT        
  skillsearch.EmployeeId,        
  (skillsearch.FirstName + ' ' + skillsearch.lastname  )as EmployeeName,      
  skillsearch.SkillId,            
  skillsearch.SkillIGroupID,       
  skillsearch.SkillGroupName,             
  skillsearch.SkillName ,       
  skillsearch.IsSkillPrimary AS isPrimary,              
  skillsearch.LastUsed,                
  skillsearch.CompetencyAreaId,         
  skillsearch.CompetencyAreaCode,          
  skillsearch.proficiencyLevelId,      
  skillsearch.ProficiencyLevelCode,
  skillsearch.SkillExperience       
            
FROM  SkillSearch skillsearch   
WHERE             
 skillsearch.EmployeeId = @EmployeeID      
END   
  