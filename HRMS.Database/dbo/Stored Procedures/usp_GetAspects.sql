﻿-- ======================================      
-- Author   : Sushmitha  
-- Create date  : 20-07-2018  
-- Modified date : 06-05-2019  
-- Modified By  : Chandra  
-- Description  : Gets Aspects Master.   
-- ======================================      
CREATE PROCEDURE [dbo].[usp_GetAspects]    
AS           
BEGIN  
 SET NOCOUNT ON;     
    SELECT DISTINCT  
    AspectMaster.AspectId  
   ,AspectMaster.AspectName as KRAAspectName
   ,CASE WHEN (KRAAspectMaster.AspectId is not null) THEN 'true' ELSE 'false' END AS IsMappedAspect
  FROM [dbo].[AspectMaster]  AspectMaster 
  LEFT JOIN   dbo.KRAAspectMaster KRAAspectMaster 
  ON KRAAspectMaster.AspectId=AspectMaster.AspectId
  
END  


