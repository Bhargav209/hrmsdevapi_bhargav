﻿-- ====================================  
-- Author		:	Hemamalini     
-- Create date  :	13-5-2019           
-- Modified date:	13-5-2019   
-- Modified By  :	Hemamalini    
-- Description  :	Checks whether project has any active client billing roles     
-- ====================================  

create Procedure [dbo].[usp_HasActiveClientBillingRoles]  
(  
 @ProjectId INT    
)  
AS              
BEGIN              
 SET NOCOUNT ON;              
              
 SELECT count(*) from ClientBillingRoles where IsActive = 1 and ProjectId = @ProjectId  
  
END