﻿-- ============================================================================  
-- Author         : Basha              
-- Create date    : 19-06-2018              
-- Modified date  : 19-06-2018              
-- Modified By    : Basha              
-- Description    : Gets notification types by CategoryID  
-- ============================================================================  
CREATE PROCEDURE [dbo].[usp_GetNotificationTypeByCategoryId]
@CategoryId INT  
AS                
BEGIN            
        
 SET NOCOUNT ON;  
         
 SELECT    
   notificationType.NotificationTypeID
			 ,notificationType.NotificationCode AS NotificationType
		FROM [dbo].[NotificationType] notificationType
		INNER JOIN [dbo].[CategoryMaster] categoryMaster
		ON notificationType.CategoryId = categoryMaster.CategoryID WHERE notificationType.CategoryId = @CategoryId
END

