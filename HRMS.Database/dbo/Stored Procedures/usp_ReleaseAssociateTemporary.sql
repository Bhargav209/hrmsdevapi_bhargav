﻿-- ======================================================      
-- Author   : Bhavani        
-- Create date  : 24-08-2018    
-- Modified date : 09-05-2019  
-- Modified By  : Mithun    
-- Description  : Release an associate from project  
-- [dbo].[usp_ReleaseAssociate] 25,75,30,4,43,'3-Mar-2018','3-Mar-2018','ramya', '192.168.2.208'  
-- ======================================================              
CREATE PROCEDURE [dbo].[usp_ReleaseAssociateTemporary]      
(       
 @ProjectId INT ,      
 @EmployeeId INT,      
 @TalentPoolProjectId INT,    
 --@ReportingManagerId INT,      
 @ReleaseDate DateTime,      
 @TalentRequisitionId int,
 @MakePrimaryProjectId INT,
 @IsPrimary BIT,      
 @ModifiedDate Datetime,      
 @ModifiedBy varchar(150),      
 @SystemInfo varchar(50)      
)        
AS        
BEGIN        
 SET NOCOUNT ON;    
     
 DECLARE @EffectiveDate DATETIME      
 DECLARE @RoleMasterID INT    
 DECLARE @InternalBillingRoleId INT    
 DECLARE @ExitingProjectAllocationPercentage INT    
 DECLARE @ExitingPoolPercentage INT    
 DECLARE @AssociateAllocationId INT      
 DECLARE @AllocationPercentage INT    
 DECLARE @ProjectTypeId INT    
 DECLARE @PracticeAreaId INT    
 DECLARE @DepartmentId INT    
 DECLARE @ReportingManagerId INT    
    SET @ExitingPoolPercentage = 0    
  
 DECLARE @CategoryId INT  
 DECLARE @ProjectStateId INT  
  
 SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'  
 SELECT @ProjectStateId = StatusId FROM Status WHERE StatusCode= 'Closed' AND CategoryID = @CategoryId  
    
 SELECT @ReportingManagerId = ProgramManagerID FROM ProjectManagers WHERE ProjectID = @TalentPoolProjectId AND IsActive = 1    
 SELECT @ProjectTypeId= ProjectTypeId FROM ProjectType WHERE ProjectTypeCode='Training'    
 IF EXISTS(SELECT 1 FROM Projects project WHERE project.ProjectId = @ProjectId AND project.ProjectTypeId = @ProjectTypeId   
 AND [project].ProjectStateId <> @ProjectStateId  
 )    
 BEGIN    
  SELECT @DepartmentId = DepartmentId FROM Departments WHERE DepartmentCode = 'Delivery'    
  SELECT @PracticeAreaId = PracticeAreaId FROM TalentPool WHERE ProjectId = @TalentPoolProjectId    
      
  UPDATE Employee SET CompetencyGroup = @PracticeAreaId, DepartmentId = @DepartmentId, ModifiedUser = @ModifiedBy, ModifiedDate = GETDATE() WHERE EmployeeId = @EmployeeId     
 END    
     
    IF EXISTS(SELECT 1 FROM AssociateAllocation allocation WHERE allocation.ProjectId = @ProjectId AND allocation.EmployeeId = @EmployeeId AND allocation.IsActive = 1)    
 BEGIN      
    
  SELECT     
    @EffectiveDate       = allocation.EffectiveDate    
   ,@RoleMasterID       = RoleMasterId    
   ,@InternalBillingRoleId     = InternalBillingRoleId      
   ,@ExitingProjectAllocationPercentage = allocationPercent.[Percentage]     
   ,@AssociateAllocationId     = AssociateAllocationId      
  FROM AssociateAllocation allocation    
  INNER JOIN [AllocationPercentage] allocationPercent    
  ON allocation.AllocationPercentage = allocationPercent.AllocationPercentageID    
  WHERE allocation.ProjectId = @ProjectId     
  AND allocation.EmployeeId = @EmployeeId     
  AND allocation.IsActive = 1           
      
  UPDATE AssociateAllocation      
   SET ReleaseDate =      
   CASE     
   WHEN @ReleaseDate < @EffectiveDate     
   THEN @EffectiveDate       
   ELSE @ReleaseDate      
   END,      
   IsActive = 0,      
   ModifiedDate = GETDATE(),      
   ModifiedBy = @ModifiedBy,      
   SystemInfo = @SystemInfo      
  WHERE AssociateAllocationId = @AssociateAllocationId   

    END    
    
    IF EXISTS(SELECT * FROM AssociateAllocation allocation WHERE allocation.ProjectId = @TalentPoolProjectId AND allocation.EmployeeId = @EmployeeId AND allocation.IsActive = 1)      
    BEGIN    
    
  SET @AssociateAllocationId = 0      
  SELECT     
    @EffectiveDate = allocation.EffectiveDate    
   ,@ExitingPoolPercentage = allocationPercent.[Percentage]     
   ,@AssociateAllocationId = AssociateAllocationId       
  FROM AssociateAllocation allocation    
  INNER JOIN [AllocationPercentage] allocationPercent    
  ON allocation.AllocationPercentage = allocationPercent.AllocationPercentageID    
  WHERE allocation.ProjectId = @TalentPoolProjectId     
  AND allocation.EmployeeId = @EmployeeId     
  AND allocation.IsActive = 1      
      
  UPDATE AssociateAllocation      
   SET ReleaseDate =      
   CASE WHEN @ReleaseDate<@EffectiveDate then @EffectiveDate       
   ELSE @ReleaseDate      
   END,      
  IsActive = 0,      
  ModifiedDate = GETDATE(),      
  ModifiedBy = @ModifiedBy,      
  SystemInfo = @SystemInfo      
  WHERE      
  AssociateAllocationId = @AssociateAllocationId      
    END       
    --

 SET @AllocationPercentage =  @ExitingProjectAllocationPercentage + @ExitingPoolPercentage      
     
 IF (@AllocationPercentage >0)    
 BEGIN    
 SELECT @AllocationPercentage = AllocationPercentageID FROM AllocationPercentage WHERE Percentage = @AllocationPercentage     
    
 BEGIN    
    INSERT INTO [dbo].[AssociateAllocation]      
           ([TRId]    
           ,[ProjectId]      
           ,[EmployeeId]      
           ,[RoleMasterId]      
           ,[IsActive]      
           ,[AllocationPercentage]      
           ,[InternalBillingPercentage]      
           ,[IsCritical]      
           ,[EffectiveDate]      
           ,[AllocationDate]      
           ,[CreatedBy]                 
           ,[CreateDate]               
           ,[SystemInfo]      
           ,[ReportingManagerId]                
           ,[IsBillable]      
           ,[InternalBillingRoleId]                 
     )               
                
     VALUES      
       (    @TalentRequisitionId    
           ,@TalentPoolProjectId      
           ,@EmployeeId      
           ,@RoleMasterID      
           ,1      
           ,@AllocationPercentage    
           ,0      
           ,0      
           ,DATEADD(day, 1, @ReleaseDate)      
           ,Getdate()      
           ,@ModifiedBy                 
           ,Getdate()                 
           ,@SystemInfo      
           ,@ReportingManagerId                 
           ,0      
           ,@InternalBillingRoleId            
           )      
   END      
   END 
   
  --from release screen assigning primary project
  IF(@MakePrimaryProjectId <> 0) 
      
    BEGIN  
	   
   DECLARE @InternalBillingPercentage DECIMAL(18,0) 
		  ,@IsCritical BIT    
		  ,@ExistingReportingManagerId INT 
		  ,@IsBillable BIT
		  ,@ClientBillingRoleId INT
		  ,@ClientBillingPercentage DECIMAL(18,0)
	   SET @AssociateAllocationId = 0      
       SELECT     
           @EffectiveDate       = allocation.EffectiveDate    
          ,@RoleMasterID       = RoleMasterId    
          ,@InternalBillingRoleId     = InternalBillingRoleId      
          ,@ExitingProjectAllocationPercentage = AllocationPercentage     
          ,@InternalBillingPercentage     = InternalBillingPercentage 
		  ,@IsCritical     = IsCritical 
		  ,@ExistingReportingManagerId     = ReportingManagerId 
		  ,@IsBillable = IsBillable
		  ,@ClientBillingRoleId = ClientBillingRoleId
		  ,@ClientBillingPercentage = ClientBillingPercentage
		  ,@AssociateAllocationId = AssociateAllocationId
		      
       FROM AssociateAllocation allocation    
       INNER JOIN [AllocationPercentage] allocationPercent  
	   ON allocation.AllocationPercentage = allocationPercent.AllocationPercentageID    
       WHERE allocation.ProjectId = @MakePrimaryProjectId     
       AND allocation.EmployeeId = @EmployeeId     
       AND allocation.IsActive = 1

	   --first releasing a person from the project that is going to make primary
	   UPDATE AssociateAllocation      
       SET ReleaseDate = CASE WHEN @ReleaseDate<@EffectiveDate then @EffectiveDate ELSE @ReleaseDate END,      
           IsActive = 0,      
           ModifiedDate = GETDATE(),      
           ModifiedBy = @ModifiedBy,      
           SystemInfo = @SystemInfo      
       WHERE AssociateAllocationId = @AssociateAllocationId 

        --making the project as primary
       INSERT INTO AssociateAllocation
	   (
	        [TRId]    
           ,[ProjectId]      
           ,[EmployeeId]      
           ,[RoleMasterId]      
           ,[IsActive]      
           ,[AllocationPercentage]      
           ,[InternalBillingPercentage]      
           ,[IsCritical]
		   ,[IsPrimary]      
           ,[EffectiveDate]      
           ,[AllocationDate]      
           ,[ModifiedBy]                 
           ,[ModifiedDate]               
           ,[SystemInfo]      
           ,[ReportingManagerId]                
           ,[IsBillable]      
           ,[InternalBillingRoleId]  
		   ,[ClientBillingRoleId]
		   ,[ClientBillingPercentage]
	   ) 
	   Values
	   (
	        @TalentRequisitionId    
           ,@MakePrimaryProjectId      
           ,@EmployeeId      
           ,@RoleMasterID      
           ,1      
           ,@ExitingProjectAllocationPercentage    
           ,@InternalBillingPercentage      
           ,@IsCritical
		   ,@IsPrimary     
           ,DATEADD(day, 1, @ReleaseDate)		   
           ,Getdate()      
           ,@ModifiedBy                 
           ,Getdate()                 
           ,@SystemInfo      
           ,@ExistingReportingManagerId                 
           ,@IsBillable     
           ,@InternalBillingRoleId
		   ,@ClientBillingRoleId 
		   ,@ClientBillingPercentage  
	   )   
    END   
  SELECT @@ROWCOUNT      
END