﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-04-2018
-- Modified date	:	12-04-2018
-- Modified By		:	Sushmitha
-- Description		:	Updates KRA definition By kra definition Id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_UpdateKRADefinition]
(
@KRADefinitionId INT,
@Metric VARCHAR(500), 
@KRAMeasurementTypeID INT,
@KRAOperatorID INT,
@KRAScaleMasterID INT,
@KRATargetValue DECIMAL(5,2),
@KRATargetValeAsDate Datetime NULL,
@KRATargetText VARCHAR(100) NULL,
@KRATargetPeriodID INT,
@RoleName VARCHAR(100),
@StatusId INT,
@ModifiedUser VARCHAR(50),      
@SystemInfo VARCHAR(50)    
)
AS          
BEGIN      
      
 SET NOCOUNT ON; 
 DECLARE @DraftStatusId INT
 DECLARE @SendBackStatusId INT
 DECLARE @SubmittedForDH INT
 DECLARE @SubmittedForHRHead INT
 DECLARE @SendBacktoDepartmentHead INT 

 SET @DraftStatusId = (SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA'))
 SET @SendBackStatusId = (SELECT [dbo].[udf_GetStatusId]('SendBackForHRMReview', 'KRA'))
 SET @SubmittedForDH = (SELECT [dbo].[udf_GetStatusId]('SubmittedForDepartmentHeadReview', 'KRA'))
 SET @SubmittedForHRHead = (SELECT [dbo].[udf_GetStatusId]('SubmittedForHRHeadReview','KRA'))
 SET @SendBacktoDepartmentHead = (SELECT [dbo].[udf_GetStatusId]('SendBackForDepartmentHeadReview','KRA'))

 IF ((@RoleName = 'HRM' AND (@StatusId =  @DraftStatusId OR @StatusId = @SendBackStatusId) OR (@RoleName = 'Department Head' AND @StatusId = @SubmittedForDH OR @StatusId = @SendBacktoDepartmentHead) OR (@RoleName = 'HR Head' AND @StatusId = @SubmittedForHRHead)))
 BEGIN
	UPDATE [dbo].[KRADefinition]          
	SET  
		Metric					=	@Metric
		,KRAMeasurementTypeID   =   @KRAMeasurementTypeID
		,KRAOperatorID			=	@KRAOperatorID
		,KRAScaleMasterID		=	@KRAScaleMasterID
		,TargetValue			=	@KRATargetValue
		,TargetValueAsDate		=	@KRATargetValeAsDate
		,KRATargetText			=	@KRATargetText
		,KRATargetPeriodID		=	@KRATargetPeriodID
		,ModifiedUser			=	@ModifiedUser   
		,SystemInfo				=	@SystemInfo  
	WHERE KRADefinitionId		=	@KRADefinitionId  
            
	SELECT @@ROWCOUNT    

 END
 ELSE
 BEGIN
	SELECT -2 --unauthorized access
 END  
     
END
Go
