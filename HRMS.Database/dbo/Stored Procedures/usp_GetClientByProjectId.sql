﻿-- =================================================     
-- Author   : Praveen              
-- Create date  : 15-11-2018             
-- Modified date : 02-04-2019               
-- Modified By  : Praveen,Mithun              
-- Description  : Gets Client by projectid      
-- =================================================     

CREATE PROCEDURE [dbo].[usp_GetClientByProjectId]  
@ProjectId INT  
AS  
BEGIN  
 SET NOCOUNT ON;  
     DECLARE @CategoryId INT
     DECLARE @ProjectStateId INT

     SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'
     SELECT @ProjectStateId = StatusId FROM Status WHERE StatusCode= 'Closed' AND CategoryID = @CategoryId
 SELECT   
  clients.ClientId AS ClientId  
 ,clients.ClientCode AS ClientCode  
 ,clients.ClientName AS ClientName  
 FROM  
 [dbo].[Projects] projects  
 INNER JOIN [dbo].[Clients] clients  
 ON projects.ClientId = clients.ClientId  
 WHERE  
 projects.ProjectId = @ProjectId AND projects.ProjectStateId <> @ProjectStateId AND clients.IsActive = 1  
 END  
