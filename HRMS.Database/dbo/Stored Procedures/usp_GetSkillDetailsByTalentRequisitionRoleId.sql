﻿
-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	06-12-2017            
-- Modified date	:	11-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Get Skill Details by Talent requisition and RoleId..     
-- ==========================================
CREATE PROCEDURE [dbo].[usp_GetSkillDetailsByTalentRequisitionRoleId]
@TalentRequisitionId INT,
@RoleMasterId INT      
AS            
BEGIN        
        
 SET NOCOUNT ON;   
         
 SELECT
	 requisitionRoleSkills.SkillId AS Id
	,skills.SkillName AS Name
 FROM
	[dbo].[RequisitionRoleDetails] requisitionRoleDetails
	INNER JOIN [dbo].[TalentRequisition] talentRequisition
	ON requisitionRoleDetails.TRId = talentRequisition.TRId
	INNER JOIN [dbo].[RequisitionRoleSkillMapping] skillMapping
	ON requisitionRoleDetails.TRId = skillMapping.TRID AND requisitionRoleDetails.RequisitionRoleDetailID = skillMapping.RequistiionRoleDetailID
	INNER JOIN [dbo].[RequisitionRoleSkills] requisitionRoleSkills
	ON skillMapping.ID = requisitionRoleSkills.RoleSkillMappingID
	INNER JOIN [dbo].[Skills] skills
	ON requisitionRoleSkills.SkillId = skills.SkillId
 WHERE 
	requisitionRoleDetails.TRId = @TalentRequisitionId AND requisitionRoleDetails.RoleMasterId = @RoleMasterId
END
