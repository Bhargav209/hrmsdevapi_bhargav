﻿-- ======================================================      
-- Author   : Bhavani        
-- Create date  : 27-12-2018    
-- Modified date :   
-- Modified By  :     
-- Description  : Get Project Client Billing Roles by ProjectId
-- [dbo].[usp_GetProjectClientBillingRolesByProjectId]
-- ======================================================              

CREATE   PROCEDURE [dbo].[usp_GetProjectClientBillingRolesByProjectId]          
(            
@ProjectId INT         
)            
AS          
BEGIN       
SET NOCOUNT ON;   

	SELECT 
		ProjectClientBillingRoleId, 
		pcbr.ClientBillingRoleId, 
		cbr.ClientBillingRoleName, 
		NoOfPositions,
		AddendumNo, 
		ReasonForUpdate 
	FROM ProjectClientBillingRoles pcbr
	JOIN ClientBillingRoles cbr ON pcbr.ClientBillingRoleId = cbr.ClientBillingRoleId
	WHERE ProjectId = @ProjectId AND IsActive = 1
END