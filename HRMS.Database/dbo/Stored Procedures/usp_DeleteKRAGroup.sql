﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-04-2018
-- Modified date	:	12-04-2018
-- Modified By		:	Sushmitha
-- Description		:	Deletes KRA definition By kra definition Id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_DeleteKRAGroup]
(
@KRAGroupId INT,
@FinancialYearId INT,
@RoleName VARCHAR(50)
)
AS
BEGIN  
  
 SET NOCOUNT ON;

 DECLARE @DraftStatusId INT
 DECLARE @SendBackStatusId INT
 DECLARE @StatusId INT
 DECLARE @WorkFlowId INT
 DECLARE @DepartmentId INT

 SET @DraftStatusId = (SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA'))
 SET @SendBackStatusId = (SELECT [dbo].[udf_GetStatusId]('SendBackForHRMReview', 'KRA'))

 SET @StatusId = (SELECT StatusId FROM [dbo].[KRAStatus] WHERE KRAGroupId = @KRAGroupId AND FinancialYearId = @FinancialYearId)

 IF (@RoleName != 'HRM')
 BEGIN
	SELECT StatusId = -2  -- Assigning -2 indicates unauthorized access.
 END
	--While after deletion of those groups that have send back status, a notification has to be sent to Department head.
	--For notification to be sent the following data has to be returned.
 ELSE
 BEGIN
	SELECT 
		 kraGroup.KRATitle
		,department.[Description]
		,CAST(FromYear AS VARCHAR(4)) + ' - ' + CAST(ToYear AS VARCHAR(4)) AS FinancialYear
		,employee.EmployeeId AS DepartmentHeadId
		,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS ToEmployeeName
		,users.EmailAddress
	FROM [dbo].[KRAGroup] kraGroup
	INNER JOIN [dbo].[Departments] department
	ON kraGroup.DepartmentId = department.DepartmentId
	INNER JOIN [dbo].[KRAStatus] kraStatus
	ON kraGroup.KRAGroupId = kraStatus.KRAGroupId
	INNER JOIN [dbo].[FinancialYear] financialYear
	ON kraStatus.FinancialYearId = financialYear.ID
	INNER JOIN [dbo].[Employee] employee
	ON department.DepartmentHeadID = employee.EmployeeId
	INNER JOIN [dbo].[Users] users
	ON users.UserId = employee.UserId
	WHERE kraGroup.KRAGroupId = @KRAGroupId AND kraStatus.StatusId = @SendBackStatusId AND kraStatus.FinancialYearId = @FinancialYearId

	--If the status of input kragroups are draft OR Send back, delete records fron kra status, kra definition and kra comment tables.
	--If the status of input kragroups are draft, then the workflow table has to be ignored as there will be no record.

	IF ((@StatusId = @DraftStatusId) OR (@StatusId = @SendBackStatusId))  
	BEGIN
	
		DELETE FROM [dbo].[KRAStatus]
		WHERE
		KRAGroupId = @KRAGroupId AND FinancialYearId = @FinancialYearId 

		DELETE FROM [dbo].[KRADefinition]
		WHERE
		KRAGroupId = @KRAGroupId AND FinancialYearId = @FinancialYearId 

		DELETE FROM [dbo].[KRAComment]
		WHERE
		KRAGroupId = @KRAGroupId AND FinancialYearId = @FinancialYearId  

	--If the status of input kragroups are send back, then data from kraworkflow should also be deleted.

		IF (@StatusId = @SendBackStatusId) 
		BEGIN
			SELECT  @WorkFlowId = WorkFlowId FROM [dbo].[KRASubmittedGroup] 
			WHERE KRAGroupId = @KRAGroupId

			DELETE FROM [dbo].[KRAWorkFlow]
			WHERE
			WorkFlowID = @WorkFlowId
		END
	END
	END
 END 
 GO