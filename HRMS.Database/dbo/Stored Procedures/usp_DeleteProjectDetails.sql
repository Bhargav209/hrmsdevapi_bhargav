﻿CREATE  PROCEDURE [dbo].[usp_DeleteProjectDetails]      
 (      
  @ProjectId INT  
)                   
AS                      
BEGIN                  
                 
 SET NOCOUNT ON;  
DECLARE @CategoryId INT  
DECLARE @StatusId INT  
  
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'    
 SELECT @StatusId=StatusId FROM status WHERE StatusCode='Drafted' AND CategoryID=@CategoryId    
  
    DELETE projects WHERE ProjectId = @ProjectId AND ProjectStateId=@StatusId   
     
SELECT @@ROWCOUNT  
 END 