﻿-- ==============================================    
-- Author   : Mithun    
-- Create date  : 04-09-2019    
-- Modified date : 04-09-2019
-- Modified By  : Mithun    
-- Description  : Get Project Detail By EmployeeId    
-- ==============================================     
CREATE PROCEDURE [dbo].[usp_GetProjectDetailByEmployeeId]  
 @EmployeeId INT   
AS    
 SET NOCOUNT ON;
 DECLARE @CategoryId INT
 DECLARE @ProjectStateId INT
     
   SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'
   SELECT @ProjectStateId=StatusId FROM Status WHERE CategoryID = @CategoryId AND StatusCode = 'Closed'
 SELECT     
   allocation.EmployeeID AS EmployeeId
  ,allocation.ProjectId AS ProjectId
  ,project.ProjectName AS Project  
  ,allocationpercentage.[Percentage] AS AllocationPercentage  
  ,allocation.IsBillable AS Billable 
  ,allocation.IsCritical AS isCritical
  ,allocation.IsPrimary AS IsPrimary
  ,allocation.EffectiveDate AS EffectiveDate  
  ,employee.FirstName + ' '  + employee.LastName AS AssociateName 
  ,reportingmanager.FirstName + ' '  + reportingmanager.LastName AS ReportingManager
  ,Lead.FirstName + ' '  + Lead.LastName AS Lead
  
 FROM AssociateAllocation allocation    
 INNER JOIN Employee employee ON allocation.EmployeeId = employee.EmployeeId 
 INNER JOIN Projects project ON allocation.ProjectId = project.ProjectId AND project.ProjectStateId <> @ProjectStateId
 INNER JOIN ProjectManagers projectmanager ON allocation.ProjectId = projectmanager.ProjectId AND projectmanager.IsActive = 1
 INNER JOIN Employee reportingmanager ON reportingmanager.EmployeeId = projectmanager.ReportingManagerID
 INNER JOIN Employee Lead ON Lead.EmployeeId = projectmanager.LeadID     
 INNER JOIN AllocationPercentage allocationpercentage ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID   
    
 WHERE allocation.EmployeeId = @EmployeeId AND allocation.ReleaseDate IS NULL
