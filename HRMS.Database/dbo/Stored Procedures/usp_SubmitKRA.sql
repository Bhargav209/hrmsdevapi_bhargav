﻿-- ==================================     
-- Author			:  Sushmitha            
-- Create date		:  23-04-2018    
-- Modified date	:  21-05-2018    
-- Modified By		:  Santosh    
-- Description		:  Submit KRA    
-- ==================================        
CREATE PROCEDURE [dbo].[usp_SubmitKRA]            
 @FinancialYearID INT  
,@StatusID INT     
,@FromEmployeeID INT  
,@ToEmployeeID INT  
,@DepartmentID INT  
,@KRAGroupID VARCHAR(150)  
,@CreatedBy VARCHAR(100)  
,@CreatedDate DATETIME         
,@Systeminfo VARCHAR(50)  
,@Comments VARCHAR(MAX)         
AS          
BEGIN    
     
 SET NOCOUNT ON;          
      
 DECLARE @Role VARCHAR(50)  
 DECLARE @Flag BIT  
 DECLARE @Count INT
 DECLARE @SubmittedForDH INT
 DECLARE @Approved INT
 DECLARE @SendBackStatusId INT
 DECLARE @SubmittedForHRHead INT
 DECLARE @SendBacktoDepartmentHead INT 
 DECLARE @HRHeadEmployeeId INT

 SET @SubmittedForDH = (SELECT [dbo].[udf_GetStatusId]('SubmittedForDepartmentHeadReview','KRA'))
 SET @Approved = (SELECT [dbo].[udf_GetStatusId]('Approved','KRA'))
 SET @SendBackStatusId = (SELECT [dbo].[udf_GetStatusId]('SendBackForHRMReview','KRA'))
 SET @SubmittedForHRHead = (SELECT [dbo].[udf_GetStatusId]('SubmittedForHRHeadReview','KRA'))
 SET @SendBacktoDepartmentHead = (SELECT [dbo].[udf_GetStatusId]('SendBackForDepartmentHeadReview','KRA'))
    
 SET @Flag = 1      
 SELECT @ToEmployeeID = DepartmentHeadID FROM [dbo].[Departments] WHERE DepartmentId = @DepartmentID 
    
 SELECT @HRHeadEmployeeId = DepartmentHeadID FROM [dbo].[Departments] WHERE [Description]='Human Resources'

     
 IF(@ToEmployeeID IS NULL)      
 BEGIN      
  SELECT 2 --Department Head Not Exists      
  SET @Flag = 0      
 END    

 IF(IsNull(@HRHeadEmployeeId,0)=0)      
 BEGIN      
  SELECT 2 --HR Head Not Exists      
  SET @Flag = 0      
 END 
    
 -- Validations    
    
 -- Submitted For DepartmentHead Review    
 IF(@StatusID = @SubmittedForDH)    
 BEGIN    
  SELECT @Count = COUNT(1) FROM KRAStatus    
  WHERE FinancialYearID = @FinancialYearID     
  AND KRAGroupId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@KRAGroupId,','))    
  AND StatusId IN (SELECT StatusId FROM [Status]     
       WHERE (StatusCode = 'SubmittedForDepartmentHeadReview' OR StatusCode = 'Approved')    
                      AND CategoryID = (SELECT CategoryID FROM CategoryMaster WHERE CategoryName = 'KRA'))    
      
  IF(@Count > 0)    
  BEGIN    
   SELECT 5 -- Some of the KRA Groups already submitted for action.    
   SET @Flag = 0    
  END    
 END    
 -- Approved    
 ELSE IF(@StatusID = @Approved)    
 BEGIN    
  SELECT @Count = COUNT(1) FROM KRAStatus    
  WHERE FinancialYearID = @FinancialYearID     
  AND KRAGroupId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@KRAGroupId,','))    
  AND StatusId IN (SELECT StatusId FROM [Status]     
       WHERE (StatusCode = 'SendBackForHRMReview' OR StatusCode = 'Draft' OR StatusCode = 'Approved')    
       AND CategoryID = (SELECT CategoryID FROM CategoryMaster WHERE CategoryName = 'KRA'))    
      
  IF(@Count > 0)    
  BEGIN    
   SELECT 5 -- Some of the KRA Groups already submitted for action.    
   SET @Flag = 0    
  END    
 END    
 -- Send Back For HRM Review    
 ELSE IF(@StatusID = @SendBackStatusId)    
 BEGIN    
  SELECT @Count = COUNT(1) FROM KRAStatus    
  WHERE FinancialYearID = @FinancialYearID     
  AND KRAGroupId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@KRAGroupId,','))    
  AND StatusId IN (SELECT StatusId FROM [Status]     
       WHERE (StatusCode = 'SendBackForHRMReview' OR StatusCode = 'Draft' OR StatusCode = 'Approved')    
       AND CategoryID = (SELECT CategoryID FROM CategoryMaster WHERE CategoryName = 'KRA'))    
      
  IF(@Count > 0)    
  BEGIN    
   SELECT 5 -- Some of the KRA Groups already submitted for action.    
   SET @Flag = 0    
  END    
 END   
    
-- Submitted For HR Head Review
 ELSE IF(@StatusID = @SubmittedForHRHead)    
 BEGIN
 SET @ToEmployeeID=@HRHeadEmployeeId   
  SELECT @Count = COUNT(1) FROM KRAStatus    
  WHERE FinancialYearID = @FinancialYearID     
  AND KRAGroupId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@KRAGroupId,','))    
  AND StatusId IN (SELECT StatusId FROM [Status]     
       WHERE (StatusCode = 'SubmittedForHRHeadReview'OR StatusCode = 'Approved')    
       AND CategoryID = (SELECT CategoryID FROM CategoryMaster WHERE CategoryName = 'KRA'))    
      
  IF(@Count > 0)    
  BEGIN    
   SELECT 5 -- Some of the KRA Groups already submitted for action.    
   SET @Flag = 0    
  END    
 END  

 -- Send Back For Department Head Review    
 ELSE IF(@StatusID = @SendBackStatusId)    
 BEGIN    
  SELECT @Count = COUNT(1) FROM KRAStatus    
  WHERE FinancialYearID = @FinancialYearID     
  AND KRAGroupId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@KRAGroupId,','))    
  AND StatusId IN (SELECT StatusId FROM [Status]     
       WHERE (StatusCode = 'SendBackForDepartmentHeadReview' OR StatusCode = 'Draft' OR StatusCode = 'Approved')    
       AND CategoryID = (SELECT CategoryID FROM CategoryMaster WHERE CategoryName = 'KRA'))    
      
  IF(@Count > 0)    
  BEGIN    
   SELECT 5 -- Some of the KRA Groups already submitted for action.    
   SET @Flag = 0    
  END    
 END  

 -- Validations    
    
 -- Pick @ToEmployeeID in case of status = 'SendBackForHRMReview'    
 IF (@StatusID = @SendBackStatusId)    
 BEGIN    
  SELECT TOP 1 @ToEmployeeID = workFlow.FromEmployeeID    
  FROM KRAWorkFlow workFlow                  
  WHERE workFlow.DepartmentID = @DepartmentID    
  AND workFlow.FinancialYearID = @FinancialYearID    
  ORDER BY WorkFlowID DESC    
 END  
 
 -- Pick @ToEmployeeID in case of status = 'SendBackForDepartmentHeadReview'    
 IF (@StatusID = @SendBacktoDepartmentHead)    
 BEGIN    
  SELECT @ToEmployeeID = DepartmentHeadID FROM [dbo].[Departments] WHERE DepartmentId = @DepartmentID     
 END   
      
 IF (@Flag = 1)      
 BEGIN      
  UPDATE [dbo].[KRAStatus]          
  SET StatusID  =  @StatusID             
  WHERE FinancialYearID = @FinancialYearID     
  AND KRAGroupId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@KRAGroupId,','))      

  EXEC [dbo].[usp_CreateKRAWorkFlow] @FinancialYearID, @StatusID, @FromEmployeeID, @ToEmployeeID          
             ,@DepartmentID, @KRAGroupID, @CreatedBy, @CreatedDate, @Comments         
	     
 END      
END
Go