﻿-- ====================================================  
-- Author   : Santosh    
-- Create date  : 22-01-2018    
-- Modified date : 22-01-2018        
-- Modified By  : Santosh    
-- Description  : Fill ADR review data.    
-- ====================================================  
CREATE PROCEDURE [dbo].[usp_FillADRAssociateReviewData] --214  
(@EmployeeID INT)  
AS        
BEGIN        
 SET NOCOUNT ON;  
          
 /*INSERT INTO [ADRAssociateKRAReview]  
  (EmployeeID, KRASetID, ADRCycleID, FinancialYearID)  
 SELECT   
   mapper.EmployeeID  
  ,kraSet.KRASetID  
  ,(SELECT ADRCycleID FROM [ADRCycle] WHERE IsActive = 1) AS ADRCycleID  
  ,kraSet.FinancialYearID  
 FROM [KRASet] kraSet  
 INNER JOIN [AssociateKRAMapper] mapper  
 ON kraSet.KRARoleCategoryID = mapper.KRARoleCategoryID  
 AND kraSet.KRARoleID = mapper.KRARoleID   
 AND kraSet.FinancialYearID = mapper.FinancialYearID AND mapper.IsActive = 1  
 ,(SELECT ADRCycleID FROM [ADRCycle] WHERE IsActive = 1) AS ADRCycleID  
 WHERE mapper.EmployeeID = @EmployeeID  
   
 SELECT @@ROWCOUNT  
 */
END

