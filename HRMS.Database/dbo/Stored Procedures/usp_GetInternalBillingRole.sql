﻿-- ========================================================
-- Author			:	Prasanna 
-- Create date		:	02-11-2018
-- Modified date	:	02-11-2018
-- Modified By		:	Prasanna
-- Description		:	Update internal Billing Role.
-- ========================================================   

Create PROCEDURE [dbo].[usp_GetInternalBillingRole]    
AS       
BEGIN   
 SET NOCOUNT ON;
  SELECT
	InternalBillingRoleId
    ,InternalBillingRoleCode
	,InternalBillingRoleName
	,IsActive
  FROM [dbo].InternalBillingRoles
END
