﻿-- =========================================  
-- Author			:	Sushmitha       
-- Create date		:	13-06-2018  
-- Modified date	:	13-06-2018  
-- Modified By		:	Sushmitha  
-- Description		:	Get grade by designation 
-- =========================================  

CREATE PROCEDURE [dbo].[usp_GetGradeByDesignation]
(  
 @DesignationId INT  
)  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
      
  SELECT        
    grade.GradeId        
   ,grade.GradeName          
   ,grade.GradeCode     
   FROM [dbo].[Grades] grade    
   INNER JOIN [dbo].[Designations] designation        
   ON grade.GradeId = designation.GradeId  
   WHERE designation.DesignationId = @DesignationId       
  
END

