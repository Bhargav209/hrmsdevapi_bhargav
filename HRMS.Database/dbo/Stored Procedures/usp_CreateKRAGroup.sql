﻿-- ========================================  
-- Author                  :      Sushmitha  
-- Create date             :      12-04-2018  
-- Modified date     :      26-04-2018  
-- Modified By             :      Santosh
-- Description             :      Creates a KRA Group  
-- ========================================
CREATE PROCEDURE [dbo].[usp_CreateKRAGroup]  
(    
       @DepartmentId INT,  
       @RoleCategoryId INT,   
       @ProjectTypeId INT,  
       @KRATitle VARCHAR(100),  
       @FinancialYearId INT,  
       @CreatedUser VARCHAR(50),    
       @SystemInfo VARCHAR(50)    
)    
AS  
BEGIN
       
       SET NOCOUNT ON;    
       
       DECLARE @KRAGroupID INT;

       SELECT @KRAGroupID=0 
  
       SELECT @KRAGroupID=KRAGroupId 
       FROM KRAGroup 
       WHERE DepartmentId = @DepartmentId AND RoleCategoryId = @RoleCategoryId
              AND ISNULL(ProjectTypeId, 0) = ISNULL(@ProjectTypeId, 0) AND [dbo].[udf_RemoveAllSpaces](KRATitle)=[dbo].[udf_RemoveAllSpaces](@KRATitle)
  
       IF(@KRAGroupID = 0)  
       BEGIN   
              INSERT INTO [dbo].[KRAGroup]    
              (  
                      DepartmentId  
                     ,RoleCategoryId  
                     ,ProjectTypeId  
                     ,KRATitle  
                     ,CreatedUser  
                     ,SystemInfo  
              )    
              VALUES  
              (  
                      @DepartmentId  
                     ,@RoleCategoryId  
                     ,@ProjectTypeId  
                     ,@KRATitle  
                     ,@CreatedUser  
                     ,@SystemInfo  
              )    
          
              SELECT @KRAGroupID = @@IDENTITY;   
       END
              
       IF NOT EXISTS(SELECT KRAStatusId FROM KRAStatus WHERE KRAGroupId=@KRAGroupID AND FinancialYearId=@FinancialYearId)
       BEGIN  

                     INSERT INTO [dbo].[KRAStatus]  
                     (        
                            KRAGroupId        
                           ,FinancialYearId       
                           ,StatusId     
                     )   
                     VALUES  
                     (  
                            @KRAGroupID  
                           ,@FinancialYearId  
                           ,(SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA')) 
                     )  
              
                     SELECT @@ROWCOUNT  
       END 
       ELSE
              SELECT -1  
END
GO