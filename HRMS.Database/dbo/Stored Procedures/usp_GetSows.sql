﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 11-01-2019    
-- Modified date : 11-01-2019    
-- Modified By  : Mithun    
-- Description  : Get all SOW     
-- =================================================    
CREATE PROCEDURE [dbo].[usp_GetSows]    
   
AS                  
BEGIN         
    
 SET NOCOUNT ON;               
 SELECT 
   [Id] AS Id   
  ,[SOWId] AS SOWId     
  ,[SOWSignedDate] AS SOWSignedDate
  ,[SOWFileName] AS SOWFileName     
  ,[IsActive] AS IsActive 
 FROM     
  [dbo].[SOW]         
 WHERE     
  IsActive = 1   
              
END