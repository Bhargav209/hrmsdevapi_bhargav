﻿-- ====================================================================================================
-- Author			:	Sushmitha
-- Create date		:	08-01-2018
-- Modified date	:	08-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Get delivery head dashboard that displays all pending for approval requisitons.
-- ==================================================================================================== 
CREATE PROCEDURE [dbo].[usp_GetPendingRequisitionsForApproval] 
@EmployeeId INT
AS
BEGIN 

SET NOCOUNT ON;
       
DECLARE @EmailId VARCHAR(100)

DECLARE @StatusId INT

SELECT @StatusId=[dbo].[udf_GetStatusId]('SubmittedForApproval', 'TalentRequisition')

--Get email Id based on employeeId
SELECT @EmailId = [user].EmailAddress FROM [dbo].[Users] [user]
INNER JOIN [dbo].[Employee] employee
ON [user].UserId = employee.UserId AND employee.EmployeeId = @EmployeeId

--Check emailId is authorized or not
SELECT notificationConfig.EmailTo FROM [dbo].[NotificationConfiguration] notificationConfig
INNER JOIN [dbo].[NotificationType] notificationType
ON notificationConfig.NotificationTypeID = notificationType.NotificationTypeID
WHERE  notificationConfig.EmailTo LIKE + '%' + @EmailId + '%' and notificationConfig.NotificationTypeID = (SELECT NotificationTypeID 
											 FROM NotificationType WHERE NotificationCode = 'TRSubmitForApproval')

IF (@@ROWCOUNT = 0)
BEGIN
	SELECT 'N' AS Authorized
END
ELSE 
BEGIN

	SELECT DISTINCT 
		talentRequisiton.TRId  AS TalentRequisitionId
	   ,talentRequisiton.TRCode
	   ,talentRequisiton.RequestedDate
	   ,employee.FirstName + ' ' + employee.LastName AS RequestedBy
	   ,department.DepartmentCode
	   ,projects.ProjectName
	   ,talentRequisiton.RequisitionType
	FROM 
		[dbo].[TalentRequisition] talentRequisiton  
	 INNER JOIN [dbo].[TalentRequisitionWorkFlow] workFlow
	 ON talentRequisiton.TRId = workFlow.TalentRequisitionID
	 LEFT JOIN [dbo].[Projects] projects  
	 ON talentRequisiton.ProjectId = projects.ProjectId
	 INNER JOIN [dbo].[Employee] employee
	 ON talentRequisiton.RaisedBy = employee.EmployeeId
	 INNER JOIN [dbo].[Departments] department
	 ON talentRequisiton.DepartmentId = department.DepartmentId
	WHERE 
	 talentRequisiton.StatusId = @StatusId --Pending for approval
	 AND workFlow.ToEmployeeID LIKE + '%' + CONVERT(VARCHAR(10),@EmployeeId) + '%'
	ORDER BY RequestedDate DESC
END 
END
