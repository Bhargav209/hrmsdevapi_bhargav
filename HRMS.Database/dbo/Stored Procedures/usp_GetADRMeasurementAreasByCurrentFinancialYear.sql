﻿-- ======================================        
-- Author   : Sabiha        
-- Create date  : 29-05-2019        
-- Modified date :         
-- Modified By  :         
-- Description  : Get ADR Measurement Areas        
-- ======================================        
CREATE PROCEDURE [dbo].[usp_GetADRMeasurementAreasByCurrentFinancialYear]        
AS        
BEGIN        
 SET NOCOUNT ON;        
  SELECT        
    ADRMeasurementAreaId       
   ,ADRMeasurementAreaName    
  FROM ADRMeasurementAreas am    
  INNER JOIN FinancialYear fy    
ON am.FinancialYearId = fy.ID AND fy.IsActive = 1     
END 