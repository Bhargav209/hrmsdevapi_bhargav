﻿CREATE PROCEDURE [dbo].[usp_GetUserDepartmentDetails]
              
AS                
BEGIN            
           
 SET NOCOUNT ON; 
 SELECT 
  dept.DepartmentId AS DepartmentId
 ,dept.DepartmentCode AS DepartmentCode
 ,dept.Description AS Description
 ,dept.IsActive AS IsActive
 ,dept.DepartmentHeadID AS DepartmentHeadID
 ,emp.FirstName + ' ' + emp.LastName AS EmployeeName
 FROM Departments dept
 INNER JOIN Employee emp ON dept.DepartmentHeadID = emp.EmployeeId
 WHERE dept.IsActive = 1 AND emp.IsActive = 1

END
Go
