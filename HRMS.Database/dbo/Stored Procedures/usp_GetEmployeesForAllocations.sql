﻿-- ========================================================  
-- Author   : Mithun  
-- Create date  : 09-05-2019  
-- Modified date : 09-05-2019  
-- Modified By  : Mithun  
-- Description  : Get Employees For Allocations.  
-- ========================================================   
CREATE PROCEDURE [dbo].[usp_GetEmployeesForAllocations]     
  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
  SELECT 
  DISTINCT
   allocation.EmployeeId AS  EmployeeId
   ,employee.FirstName + ' ' + employee.LastName AS EmployeeName
   
  FROM AssociateAllocation allocation
  JOIN Employee employee ON allocation.EmployeeId = employee.EmployeeId
  JOIN Projects project ON allocation.ProjectId = project.ProjectId 
  
  WHERE allocation.IsActive = 1 AND employee.IsActive = 1 AND project.ProjectTypeId = 6  
END  


  