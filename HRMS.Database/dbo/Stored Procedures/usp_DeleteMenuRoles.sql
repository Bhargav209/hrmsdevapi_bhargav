﻿-- =======================================     
-- Author			:	Sunitha          
-- Create date		:	12-03-2018    
-- Modified date	:	12-03-2018              
-- Modified By		:	Sunitha          
-- Description		:	Delete TargetMenus         
-- =======================================
CREATE PROCEDURE [dbo].[usp_DeleteMenuRoles]        
@RoleId INT
AS          
BEGIN      
      
 SET NOCOUNT ON;  
     
DELETE FROM [dbo].[MenuRoles] WHERE RoleId=@RoleId


 SELECT @@ROWCOUNT          
END
