﻿-- ========================================================
-- Author			:	Ramya Singamsetti
-- Create date		:	11-01-2018
-- Modified date	:	11-01-2018
-- Modified By		:	Ramya
-- Description		:	Get tagged employees for requisition.
-- [dbo].[usp_GetTaggedEmployeesForTalentRequisition]
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetTaggedEmployeesForTalentRequisition] 
AS
BEGIN

DECLARE @CategoryId INT

SELECT @CategoryId = CategoryID FROM [dbo].[CategoryMaster] WHERE CategoryName = 'TalentRequisition'

SELECT
	employee.EmployeeId    
   ,employee.EmployeeCode
   ,employee.FirstName + ' ' + employee.LastName AS EmployeeName
   ,talentRequisition.TRId AS TalentRequisitionId    
   ,talentRequisition.TRCode
   ,rolemaster.RoleMasterID
   ,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
   ,projects.ProjectId
   ,projects.ProjectName
   ,roleDetails.RequisitionRoleDetailID
   ,cllients.ClientName
FROM 
	[dbo].[TalentRequisition] talentRequisition  
 INNER JOIN [dbo].[TalentRequisitionEmployeeTag] employeeTag  
 ON talentRequisition.TRId = employeeTag.TalentRequisitionID
 INNER JOIN [dbo].[Departments] dept
 on talentRequisition.DepartmentId=dept.DepartmentId
 INNER JOIN [dbo].[RequisitionRoleDetails] roleDetails
 ON employeeTag.RoleMasterID = roleDetails.RoleMasterId AND employeeTag.TalentRequisitionID = roleDetails.TRId
 INNER JOIN [dbo].[RoleMaster] rolemaster
 ON roleDetails.RoleMasterId= rolemaster.RoleMasterID
 INNER JOIN [dbo].[Employee] employee
 ON employeeTag.EmployeeId = employee.EmployeeId
 INNER JOIN [dbo].[Projects] projects
 ON talentRequisition.ProjectId = projects.ProjectId
 INNER JOIN [dbo].[Clients] cllients
 ON projects.ClientId=cllients.ClientId
 INNER JOIN [dbo].[Status] status
 ON status.StatusId=talentRequisition.StatusId and status.CategoryID=@CategoryId
Where 
	status.StatusCode='Approved' and dept.DepartmentTypeId=1 AND dept.DepartmentId=1
	and employeeTag.EmployeeID not in 
	(Select EmployeeId from AssociateAllocation 
	   where TRId=employeeTag.TalentRequisitionID 
			 and EmployeeID=employeeTag.EmployeeID and RoleMasterID=employeeTag.RoleMasterID)
END
