﻿-- ==========================================================    
-- Author   : Basha                
-- Create date  : 05-09-2018                
-- Modified date : 05-09-2018                
-- Modified By  : Basha                
-- Description  : Gets associates  approaval    
-- ==========================================================    
CREATE PROCEDURE [dbo].[usp_GetAssociateApproval]    
@EmployeeID INT    
AS                
BEGIN            
        
 SET NOCOUNT ON;    
          
 SELECT * FROM [dbo].[vw_AssociateApproval]     
 WHERE empID = @EmployeeID    
END
