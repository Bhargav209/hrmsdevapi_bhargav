﻿-- ========================================================
-- Author			:	Praveen
-- Create date		:	06-11-2018
-- Modified date	:	01-03-2019,30-04-2019,10-05-2019
-- Modified By		:	Sabiha,Mithun
-- Description		:	Get ClientBillingRoles by Project
-- [dbo].[usp_GetClientBillingRolesByProjectId] 153
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_GetClientBillingRolesByProjectId]          
(          
  @ProjectId INT          
)              
AS              
BEGIN              
 SET NOCOUNT ON;              
                
 SELECT              
  clientBillingRoles.ClientBillingRoleId           
 ,clientBillingRoles.ClientBillingRoleName          
 ,clientBillingRoles.NoOfPositions          
 ,clientBillingRoles.StartDate            
 ,clientBillingRoles.EndDate          
 ,clientBillingRoles.ClientBillingPercentage     
 ,clientBillingRoles.IsActive          
 ,projects.ProjectId          
 ,projects.ProjectName          
 ,allocationpercentage.Percentage       
 ,(SELECT COUNT(*) FROM AssociateAllocation WHERE ClientBillingRoleId = clientBillingRoles.ClientBillingRoleId AND ProjectId = @ProjectId AND ReleaseDate IS NULL) AS AllocationCount         
           
 FROM [dbo].[ClientBillingRoles] clientBillingRoles                
 INNER JOIN [dbo].[Projects] projects ON projects.ProjectId = clientBillingRoles.ProjectId           
 INNER JOIN [dbo].[AllocationPercentage] allocationpercentage  ON            
 clientBillingRoles.ClientBillingPercentage= allocationpercentage.AllocationPercentageID          
           
 WHERE           
   clientBillingRoles.ProjectId = @ProjectId           
          
END