﻿-- ===============================================  
-- Author			:	Santosh          
-- Create date		:	31-10-2017          
-- Modified date	:	31-10-2017          
-- Modified By		:  Santosh          
-- Description		:  Sets a Category to inactive         
-- ===============================================  
CREATE PROCEDURE [dbo].[usp_DeleteCategory]         
@CategoryID INT,  
@ModifiedUser VARCHAR(100),      
@SystemInfo VARCHAR(50),      
@ModifiedDate DATETIME     
AS          
BEGIN      
      
 SET NOCOUNT ON;
       
 UPDATE [dbo].[CategoryMaster]          
 SET  
	IsActive  = 0   
   ,ModifiedUser = @ModifiedUser   
   ,ModifiedDate = @ModifiedDate  
   ,SystemInfo = @SystemInfo  
 WHERE CategoryID = @CategoryID  
            
 SELECT @@ROWCOUNT          
END

