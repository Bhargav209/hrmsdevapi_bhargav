﻿-- ============================================                   
-- Author   : Hemamalini                          
-- Create date  : 22-05-2018                          
-- Modified date:                       
-- Modified By  :                          
-- Description  : Updates IsPDFGenerated field to 1              
-- ============================================              
CREATE PROCEDURE [dbo].[usp_UpdateIsPDFGeneratedForEmployee]               
(                  
 @EmployeeID INT                            
)                       
AS                         
BEGIN               
              
SET NOCOUNT ON;                   

   update AssociateKRAMapper set IsPDFGenerated = 1 where EmployeeID = @EmployeeID
         
END   
  