﻿--=======================================================
-- Author			:	Santosh
-- Create date		:	30-01-2018
-- Modified date	:	30-01-2018  
-- Modified By		:	Santosh
-- Description		:	Get projects details by ProjectID
-- ======================================================  
CREATE PROCEDURE [dbo].[usp_GetEmployeeDetailsByProjectID]
(
	@ProjectID INT
)
AS       
BEGIN  
	SET NOCOUNT ON;    
		SELECT DISTINCT
		   allocation.EmployeeId AS ID
		   ,[dbo].[udfGetEmployeeFullName](allocation.EmployeeId) AS Name
		FROM [dbo].[AssociateAllocation] allocation
		INNER JOIN [dbo].[Employee] employee
		ON allocation.EmployeeId = employee.EmployeeId
		WHERE ProjectID = @ProjectId AND employee.IsActive = 1 and allocation.IsActive=1
END
