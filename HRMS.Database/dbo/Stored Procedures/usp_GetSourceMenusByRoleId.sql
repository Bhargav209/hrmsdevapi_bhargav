﻿--=======================================================
-- Author			:	Sunitha
-- Create date		:	12-03-2018
-- Modified date	:	12-03-2018
-- Modified By		:	Sunitha
-- Description		:	Get Menus by RoleId
-- ======================================================  
CREATE PROCEDURE [dbo].[usp_GetSourceMenusByRoleId] 
(
	@RoleId INT
)
AS       
BEGIN  
	SET NOCOUNT ON;    
		SELECT
		    MenuId
		   ,Title as MenuName
		FROM [dbo].[MenuMaster] 
		WHERE MenuId NOT IN (SELECT MenuId FROM MenuRoles WHERE RoleId = @RoleId AND IsActive=1) AND IsActive = 1 AND ParentId !=0
END

