﻿-- ========================================================          
-- Author       : Chandra          
-- Create date  : 12-02-2018          
-- Modified date: 12-02-2018          
-- Modified By  : Chandra          
-- Description  : Get KraGroups by DepartmentId.          
-- ========================================================           
          
CREATE PROCEDURE [dbo].[usp_GetKraGroupsByFinancialYear]              
(              
 @FinancialYear INT              
)              
AS              
BEGIN              
 SET NOCOUNT ON;       
              
 DECLARE @CategoryId INT  
 DECLARE @StatusID INT      
      
 SELECT @CategoryId = [dbo].[udf_GetCategoryId]('KRA')      
        
SELECT         
    @StatusID=StatusID        
FROM         
 [dbo].[Status]         
WHERE         
 StatusCode = 'Approved' AND CategoryID = @CategoryId     
              
SELECT distinct         
   kraGroup.KRAGroupId              
  ,kraGroup.KRATitle   
  ,kraGroup.DepartmentId  
  ,KRADefinition.FinancialYearId  
 FROM [dbo].[KRAGroup] KRAGroup      
 INNER JOIN [dbo].[KRADefinition] KRADefinition ON kraGroup.KRAGroupId=KRADefinition.KRAGroupId    
 INNER JOIN [dbo].[KRAStatus] kraStatus ON kraGroup.KRAGroupId=kraStatus.KRAGroupId               
 WHERE  kraStatus.StatusID = @StatusID AND KRADefinition.FinancialYearId= @FinancialYear     
 ORDER BY kraGroup.KRATitle              
END 