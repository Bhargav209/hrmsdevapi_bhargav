﻿-- =======================================================================  
-- Author   :  Santosh        
-- Create date  :  31-10-2017        
-- Modified date :  31-10-2017        
-- Modified By  :  Santosh        
-- Description  :  Updates Skill Group master based upon  SkillGroupId     
-- =======================================================================    
  
-- =======================================================================  
-- Author   :  Kalyan.Penumutchu        
-- Create date  :  13-11-2017        
-- Modified date :  13-11-2017        
-- Modified By  :  Kalyan.Penumutchu        
-- Description  :  Updates Skill Group master based upon  SkillGroupId     
--Remarks - Removed IsActive we are not considering IsActive  
-- =======================================================================    
CREATE PROCEDURE [dbo].[usp_UpdateSkillGroup]        
@SkillGroupId INT,    
@SkillGroupName VARCHAR(100),       
@Description VARCHAR(MAX),    
@ModifiedUser VARCHAR(100),    
@SystemInfo VARCHAR(50),    
@ModifiedDate DATETIME,  
@CompetencyAreaId INT  
  
AS        
BEGIN    
    
SET NOCOUNT ON; 
     
 UPDATE [dbo].[SkillGroup]     
 SET    
 SkillGroupName  = @SkillGroupName    
 ,[Description]  = @Description    
 ,ModifiedUser  = @ModifiedUser    
 ,ModifiedDate  = @ModifiedDate    
 ,SystemInfo  = @SystemInfo    
 ,CompetencyAreaId = @CompetencyAreaId    
 WHERE SkillGroupId = @SkillGroupId    
    
 SELECT @@ROWCOUNT        
END
