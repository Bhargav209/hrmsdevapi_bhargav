﻿CREATE PROCEDURE [dbo].[usp_GetProjectDetail]      
 (      
 @RoleName VARCHAR(100),      
 @EmployeeId INT      
 )                   
AS                      
BEGIN                  
                 
 SET NOCOUNT ON;      
       
 DECLARE @RoleId INT   
 DECLARE @StatusId INT 
 DECLARE @DraftStatusId INT  
 DECLARE @CategoryId INT 
 DECLARE @EmailAddress varchar(100)    
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'  
 SELECT @StatusId=Statusid FROM Status WHERE StatusCode='Closed' AND CategoryID = @CategoryId   
 SELECT @DraftStatusId=Statusid FROM Status WHERE StatusCode='Drafted' AND CategoryID = @CategoryId      
 SELECT @EmailAddress=MAX(us.EmailAddress) FROM [Users] us INNER JOIN Employee emp ON emp.UserId = us.UserId WHERE emp.EmployeeId = @EmployeeId
     
 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)      
  IF(@RoleId = 5) -- 5 is program manager.ensure that roles table have always 5 as program manager      
  SELECT       
   project.ProjectId AS ProjectId      
  ,project.ProjectCode AS ProjectCode      
  ,project.ProjectName AS ProjectName      
  ,project.ActualStartDate AS ActualStartDate      
  ,project.ActualEndDate AS ActualEndDate      
  ,client.ClientName AS ClientName      
  ,practicearea.PracticeAreaCode AS PracticeAreaCode      
  ,projecttype.Description AS ProjectTypeDescription                                            
  ,state.StatusCode AS ProjectState    
  ,(SELECT TOP 1 w.WorkFlowStatus FROM ProjectWorkFlow w WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc) AS WorkFlowStatusId    
  ,(SELECT TOP 1 s.StatusCode FROM ProjectWorkFlow w INNER JOIN status s ON s.StatusId = w.WorkFlowStatus WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc ) AS WorkFlowStatus               
  ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName       
 FROM Projects project      
 INNER JOIN ProjectManagers projmanager ON project.ProjectId = projmanager.ProjectId     
 INNER JOIN  Status state ON project.ProjectStateId = state.StatusId     
 INNER JOIN Clients client ON project.ClientId = client.ClientId      
 INNER JOIN ProjectType projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId      
 INNER JOIN PracticeArea practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId      
 INNER JOIN Employee programmanager ON projmanager.ProgramManagerID = programmanager.EmployeeId       
      
 WHERE (projmanager.IsActive = 1 AND projmanager.ProgramManagerID=@EmployeeId) AND  ((project.ProjectStateId <> @StatusId AND project.ProjectStateId <> @DraftStatusId)  OR (project.CreatedUser = @EmailAddress AND project.ProjectStateId = @DraftStatusId))
 ORDER BY  project.ProjectName
      
 ELSE IF(@RoleId = 3)-- 3 is department head.ensure that roles table have always 3 as department head      
 SELECT       
   project.ProjectId AS ProjectId      
  ,project.ProjectCode AS ProjectCode      
  ,project.ProjectName AS ProjectName      
  ,project.ActualStartDate AS ActualStartDate      
  ,project.ActualEndDate AS ActualEndDate        
  ,client.ClientName AS ClientName      
  ,practicearea.PracticeAreaCode AS PracticeAreaCode      
  ,projecttype.Description AS ProjectTypeDescription                                            
  ,status.StatusCode AS ProjectState       
  ,(SELECT TOP 1 w.WorkFlowStatus FROM ProjectWorkFlow w WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc) AS WorkFlowStatusId    
  ,(SELECT TOP 1 s.StatusCode FROM ProjectWorkFlow w INNER JOIN status s ON s.StatusId = w.WorkFlowStatus WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc ) AS WorkFlowStatus                  
  ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName       
 FROM Projects project      
 LEFT OUTER JOIN ProjectManagers projmanager ON project.ProjectId = projmanager.ProjectId AND projmanager.IsActive = 1     
 LEFT OUTER  JOIN  Status status ON project.ProjectStateId = status.StatusId        
 INNER JOIN Clients client ON project.ClientId = client.ClientId      
 INNER  JOIN ProjectType projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId      
 INNER JOIN PracticeArea practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId      
 LEFT OUTER JOIN Employee programmanager ON projmanager.ProgramManagerID = programmanager.EmployeeId     
 WHERE  ((project.ProjectStateId <> @StatusId AND project.ProjectStateId <> @DraftStatusId)  OR (project.CreatedUser = @EmailAddress AND project.ProjectStateId = @DraftStatusId))  
 ORDER BY  project.ProjectName 
END