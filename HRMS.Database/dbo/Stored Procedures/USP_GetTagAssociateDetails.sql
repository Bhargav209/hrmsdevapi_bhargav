﻿-- =================================================      
-- AUTHOR  : PRASANNA      
-- CREATE DATE  : 28-01-2019      
-- MODIFIED DATE: 04-04-2019      
-- MODIFIED BY  : SABIHA      
-- DESCRIPTION  : GET WISH LIST Details TO POPULATE  
-- =================================================      
CREATE PROCEDURE [dbo].[USP_GetTagAssociateDetails]        
(      
  @MANAGERID INT      
)        
AS                      
BEGIN             
    
 SET NOCOUNT ON;    
    
 SELECT     
 taglist.ID,    
 taglist.TagAssociateListName as 'TagListName',taglist.EmployeeID,designation.DesignationName as 'Designation',    
 employee.FIRSTNAME+' '+employee.LASTNAME AS 'EmployeeName',    
 ISNULL((    
 SELECT allocationpercentage.percentage    
  FROM AssociateAllocation allocation JOIN AllocationPercentage allocationpercentage ON allocationpercentage.AllocationPercentageID=allocation.AllocationPercentage    
  JOIN Projects project ON allocation.ProjectId = project.ProjectId    
  JOIN ProjectType projecttype ON projecttype.ProjectTypeId = project.ProjectTypeId     
  WHERE allocation.EmployeeId = taglist.EmployeeID    
  AND projecttype.ProjectTypeId=6 AND ReleaseDate IS NULL    
 ) ,0)AS Allocation    
    
 FROM TagAssociate taglist    
 INNER JOIN EMPLOYEE employee ON employee.EMPLOYEEID=taglist.EMPLOYEEID    
 left join Designations designation on designation.DesignationId=employee.Designation    
     
 WHERE taglist.MANAGERID=@MANAGERID    
    
END