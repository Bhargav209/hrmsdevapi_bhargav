﻿CREATE PROCEDURE [dbo].[usp_GetClients]      
                    
AS                      
BEGIN                  
                 
 SET NOCOUNT ON;       
 SELECT       
  ClientId AS ClientId 
 ,ClientCode AS ClientCode      
 ,ClientRegisterName AS ClientRegisterName      
 ,ClientName AS ClientName      
 FROM   
 Clients      
 WHERE   
 IsActive = 1  
END 
