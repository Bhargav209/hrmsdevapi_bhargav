﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-04-2018
-- Modified date	:	12-04-2018
-- Modified By		:	Sushmitha
-- Description		:	Deletes KRA definition By kra definition Id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_DeleteKRAAspectByDefinitionId]
(
@KRAGroupId INT,
@FinancialYearId INT,
@KRAAspectId INT,
@StatusId INT,
@RoleName VARCHAR(100)
)
AS
BEGIN  
  
 SET NOCOUNT ON;

 DECLARE @CategoryId INT
 DECLARE @DraftStatusId INT
 DECLARE @SendBackStatusId INT
 DECLARE @SubmittedForDH INT
 DECLARE @SubmittedForHRHead INT
 DECLARE @SendBacktoDepartmentHead INT 

 SET @DraftStatusId = (SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA'))
 SET @SendBackStatusId = (SELECT [dbo].[udf_GetStatusId]('SendBackForHRMReview', 'KRA'))
 SET @SubmittedForDH = (SELECT [dbo].[udf_GetStatusId]('SubmittedForDepartmentHeadReview', 'KRA'))
 SET @SubmittedForHRHead = (SELECT [dbo].[udf_GetStatusId]('SubmittedForHRHeadReview','KRA'))
 SET @SendBacktoDepartmentHead = (SELECT [dbo].[udf_GetStatusId]('SendBackForDepartmentHeadReview','KRA'))

 IF ((@RoleName = 'HRM' AND (@StatusId = @DraftStatusId) OR @StatusId = @SendBackStatusId) OR (@RoleName = 'Department Head' AND (@StatusId = @SubmittedForDH OR @StatusId = @SendBacktoDepartmentHead)) OR (@RoleName = 'HR Head' AND @StatusId = @SubmittedForHRHead))
 BEGIN
	DELETE FROM [dbo].[KRADefinition]
	WHERE
		KRAAspectId= @KRAAspectId AND KRAGroupId = @KRAGroupId AND FinancialYearId=@FinancialYearId  

	SELECT @@ROWCOUNT    

 END
 ELSE
 BEGIN
	SELECT -1
 END        
END
GO
