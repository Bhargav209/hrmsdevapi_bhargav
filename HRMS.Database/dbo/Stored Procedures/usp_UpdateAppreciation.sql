﻿-- ============================================
-- Author			:	Santosh   
-- Create date		:	30-01-2018      
-- Modified date	:	06-02-2018
-- Modified By		:	Santosh
-- Description		:	Update Appreciation
-- ============================================  
CREATE PROCEDURE [dbo].[usp_UpdateAppreciation]
(  
	 @ID					INT
	,@AppreciationMessage	VARCHAR(MAX)
	,@DateModified			DATETIME
	,@ModifiedUser			VARCHAR(150)
	,@SystemInfo			VARCHAR(50)
)
AS
BEGIN
 SET NOCOUNT ON;
	UPDATE [dbo].[AssociateAppreciation]
	SET
		 AppreciationMessage	=	@AppreciationMessage
		,DateModified			=	@DateModified
		,ModifiedUser			=	@ModifiedUser
		,SystemInfo				=	@SystemInfo
	WHERE ID = @ID

	SELECT @@ROWCOUNT
END

