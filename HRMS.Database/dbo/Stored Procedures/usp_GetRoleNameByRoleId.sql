﻿-- =================================================   
-- Author			:	Sushmitha            
-- Create date		:	28-02-2018           
-- Modified date	:	28-02-2018             
-- Modified By		:	Sushmitha            
-- Description		:	Gets roles name by role Id    
-- ================================================= 

CREATE PROCEDURE [dbo].[usp_GetRoleNameByRoleId]
@RoleMasterID INT
AS
BEGIN
 SET NOCOUNT ON;

 SELECT
	[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
 FROM
	[dbo].[RoleMaster] rolemaster     
 WHERE 
	roleMaster.RoleMasterID = @RoleMasterID 
 END

