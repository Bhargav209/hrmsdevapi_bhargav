﻿-- ======================================      
-- Author   : Sabiha      
-- Create date  : 23-05-2019      
-- Modified date :       
-- Modified By  :       
-- Description  : Update a ADROrganisationDevelopment       
-- ======================================      
      
CREATE PROCEDURE [dbo].[usp_UpdateADROrganisationDevelopment]           
(          
@ADROrganisationDevelopmentID INT,   
@ADROrganisationDevelopmentActivity VARCHAR(1000),             
@DateModified DATETIME,              
@ModifiedUser VARCHAR(150),              
@SystemInfo VARCHAR(50)          
)          
AS          
BEGIN              
              
 SET NOCOUNT ON;            
           
 BEGIN TRY
 BEGIN          
          
 UPDATE           
 [dbo].[ADROrganisationDevelopmentMaster]           
 SET          
 ADROrganisationDevelopmentActivity  = @ADROrganisationDevelopmentActivity          
 ,ModifiedDate    = @DateModified          
 ,ModifiedUser    = @ModifiedUser          
 ,SystemInfo           = @SystemInfo          
 WHERE           
 ADROrganisationDevelopmentID = @ADROrganisationDevelopmentID          
              
  SELECT @@ROWCOUNT           
          
END          
END TRY
  BEGIN CATCH
   SELECT ERROR_NUMBER( ) --2627 is Violation in unique index  
  END CATCH        
END 