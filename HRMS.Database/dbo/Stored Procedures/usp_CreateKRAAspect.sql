﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	05-02-2018
-- Modified date	:	05-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Adds KRA Aspects.
-- ======================================================== 

create PROCEDURE [dbo].[usp_CreateKRAAspect]
(  
@AspectId INT, 
@DepartmentId INT, 
@CreatedDate DATETIME,  
@CreatedUser VARCHAR(150),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON;  

 UPDATE 
	[dbo].[KRAAspectMaster]
 SET
	DateModified = GETDATE()
 WHERE 
	AspectId = @AspectId AND DepartmentId = @DepartmentId

 IF (@@ROWCOUNT = 0)
 BEGIN
 
 INSERT INTO 
	[dbo].[KRAAspectMaster]  
	(AspectId, DepartmentId, DateCreated, CreatedUser, SystemInfo)  
 VALUES  
	(@AspectId, @DepartmentId, @CreatedDate, @CreatedUser, @SystemInfo)  
  
 SELECT @@ROWCOUNT
    
END
ELSE

SELECT -1 --duplicate

END
GO
