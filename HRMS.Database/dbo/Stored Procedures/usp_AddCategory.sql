﻿-- ====================================  
-- Author		:	Santosh     
-- Create date  :	31-10-2017           
-- Modified date:	31-10-2017  
-- Modified By  :	Santosh    
-- Description  :	Adds a new Category      
-- ====================================  
CREATE PROCEDURE [dbo].[usp_AddCategory]            
@CategoryName VARCHAR(50),           
@IsActive BIT,        
@CreatedUser VARCHAR(100),        
@SystemInfo VARCHAR(50),        
@CreatedDate DATETIME       
AS            
BEGIN        
        
 SET NOCOUNT ON;        
         
 INSERT INTO [dbo].[CategoryMaster]            
 (      
   CategoryName      
  ,IsActive      
  ,CreatedUser      
  ,ModifiedUser      
  ,CreatedDate      
  ,ModifiedDate      
  ,SystemInfo      
 )            
 VALUES            
 (      
   @CategoryName       
  ,@IsActive      
  ,@CreatedUser      
  ,@CreatedUser      
  ,@CreatedDate      
  ,@CreatedDate      
  ,@SystemInfo    
 )            
              
 SELECT @@ROWCOUNT            
END
