﻿-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	04-12-2017            
-- Modified date	:	04-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Add to requisition role skill mapping table.     
-- ==========================================

CREATE PROCEDURE [dbo].[usp_AddRequisitionRoleSkillMapping]            
 @TRID INT,
 @RequistiionRoleDetailID INT,
 @SkillGroupID INT
AS            
BEGIN        
        
 SET NOCOUNT ON;
/*         
 DECLARE @ID INT

 IF NOT EXISTS(SELECT 1 FROM [dbo].[RequisitionRoleSkillMapping] WHERE TRID = @TRID AND RequistiionRoleDetailID = @RequistiionRoleDetailID)
 BEGIN
 INSERT INTO [dbo].[RequisitionRoleSkillMapping]            
 (      
     TRId
	,RequistiionRoleDetailID
 )            
 VALUES            
 (      
     @TRID
	,@RequistiionRoleDetailID
 )     
 END
	
 SELECT @ID = ID 
 FROM 
	[dbo].[RequisitionRoleSkillMapping] 
 WHERE 
	TRID = @TRID AND RequistiionRoleDetailID = @RequistiionRoleDetailID

 DELETE 
 FROM 
	[dbo].[RequisitionRoleSkills] 
 WHERE  
	RoleSkillMappingID = @ID AND SkillGroupID = @SkillGroupID
 SELECT @ID
 */
END

