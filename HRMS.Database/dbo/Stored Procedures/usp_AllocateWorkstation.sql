﻿-- ========================================================      
-- Author   : Mithun      
-- Create date  : 29-05-2019      
-- Modified date : 29-05-2019      
-- Modified By  : Mithun      
-- Description  : Allocate Workstation.      
-- ========================================================       
      
CREATE PROCEDURE [dbo].[usp_AllocateWorkstation]      
(    
@WorkStationId INT,    
@EmployeeId INT,        
@CreatedDate DATETIME,        
@CreatedUser VARCHAR(150),        
@SystemInfo VARCHAR(50)        
)        
AS      
BEGIN      
      
 SET NOCOUNT ON;        
 DECLARE @IsOccupied INT ,@AllocatedWorkstationId INT ,@BayId INT  
 DECLARE @Id INT   
 DECLARE @Bayname VARCHAR(10)   
    
SELECT @AllocatedWorkstationId=WorkstationId,@IsOccupied = IsOccupied,@BayId=BayId FROM WorkStation WHERE id in(    
SELECT WorkstationId FROM WorkStationAllocation WHERE employeeid = @EmployeeId)  
  
--get bay name  
SELECT @Bayname = Name FROM BayInformation WHERE BayId = @BayId   
  
IF(@IsOccupied  = 0)    
BEGIN    
SELECT @Id=Id FROM WorkStation WHERE WorkstationId = @WorkStationId    
       
 INSERT INTO       
 [dbo].[WorkStationAllocation]        
 (WorkStationId,EmployeeId, CreatedDate, CreatedUser, SystemInfo)        
 VALUES        
 (@Id,@EmployeeId, @CreatedDate, @CreatedUser, @SystemInfo)     
    
 --updating workstation    
 UPDATE WorkStation     
 SET IsOccupied = 1,    
 ModifiedDate = @CreatedDate,    
 ModifiedUser = @CreatedUser,    
 SystemInfo = @SystemInfo    
 WHERE Id = @Id      
        
 SELECT @@ROWCOUNT    
 END    
 ELSE    
 BEGIN    
  SELECT CONVERT(VARCHAR(10),@AllocatedWorkstationId) +'$'+  @Bayname  
 END      
      
END  
  
