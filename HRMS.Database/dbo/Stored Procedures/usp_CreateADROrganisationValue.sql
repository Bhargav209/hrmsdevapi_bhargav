﻿-- ======================================        
-- Author   : Sabiha        
-- Create date  : 24-05-2019        
-- Modified date :04-06-2019         
-- Modified By  : Mithun         
-- Description  : Create new ADR Organisation Value        
-- ======================================        
        
CREATE PROCEDURE [dbo].[usp_CreateADROrganisationValue]          
(            
@ADROrganisationValue VARCHAR(100),   
@FinancialYearId INT,           
@CreatedDate DATETIME,            
@CreatedUser VARCHAR(150),            
@SystemInfo VARCHAR(50)            
)            
AS          
BEGIN          
          
 SET NOCOUNT ON;         
          
 BEGIN TRY        
           
 INSERT INTO           
 [dbo].[ADROrganisationValueMaster]            
 (ADROrganisationValue, FinancialYearId, CreatedDate, CreatedUser, SystemInfo)            
 VALUES            
 (@ADROrganisationValue, @FinancialYearId, @CreatedDate, @CreatedUser, @SystemInfo)            
            
 SELECT @@ROWCOUNT          
              
END TRY
  BEGIN CATCH
   SELECT ERROR_NUMBER( ) --2627 is Violation in unique index  
  END CATCH         
          
END