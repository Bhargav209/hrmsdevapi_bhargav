﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 11-01-2019    
-- Modified date : 11-01-2019    
-- Modified By  : Mithun    
-- Description  : update Addendum Details    
-- =================================================

--exec usp_UpdateAddendumDetails 5,22,'Department Head',23,'grt','2019/01/16','for testing','ger','2019/01/17','fgghh'
CREATE PROCEDURE [dbo].[usp_UpdateAddendumDetails]    
(   
 @Id INT
 ,@ProjectId INT
 ,@RoleName VARCHAR(50) 
 ,@AddendumNo VARCHAR(50)
 ,@RecipientName VARCHAR(50) 
 ,@AddendumDate DATETIME
 ,@Note VARCHAR(250)
 ,@ModifiedUser VARCHAR(100)
 ,@ModifiedDate DATETIME
 ,@SystemInfo VARCHAR(100)
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON;
 DECLARE @RoleId INT

 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)
 IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head  
  BEGIN           
  IF EXISTS(SELECT 1 FROM Addendum WHERE AddendumId=@Id AND ProjectId=@ProjectId)
  BEGIN
     UPDATE Addendum SET     
	    ProjectId = @ProjectId
	   ,AddendumNo= @AddendumNo
	   ,RecipientName = @RecipientName
	   ,AddendumDate = @AddendumDate 
	   ,Note = @Note
	   ,ModifiedDate = @ModifiedDate
	   ,ModifiedUser = @ModifiedUser
	   ,SystemInfo = @SystemInfo 
       WHERE AddendumId=@Id AND ProjectId=@ProjectId 
	  SELECT @@ROWCOUNT;
	   
  END
 ELSE
  SELECT -1
   
 END
ELSE
  SELECT -2      
END