﻿-- ===================================================      
-- Author   : Santosh      
-- Create date  : 26-04-2018      
-- Modified date : 26-04-2018                  
-- Modified By  : Santosh      
-- Description  : Get Utilization Report Count      
--[dbo].[usp_rpt_UtilizationReportCount] 0, 0, 0, 0, 0, 0, 0, 0, 1, 1      
-- ===================================================      
CREATE PROCEDURE [dbo].[usp_rpt_UtilizationReportCount]      
(      
    @EmployeeID INT      
   ,@ProjectID INT      
   ,@GradeID INT      
   ,@DesignationID INT      
   ,@ClientID INT      
   ,@AllocationPercentageID INT      
   ,@ProgramManagerID INT      
   ,@MinExperience INT     
   ,@MaxExperience INT         
   ,@IsBillable INT      
   ,@IsCritical INT   
   ,@practiceAreaID INT      
)      
AS      
BEGIN      
      
 SET NOCOUNT ON;      
         
 SELECT          
  COUNT(*) AS TotalCount          
 FROM [dbo].[AssociateAllocation] allocation          
 LEFT JOIN [dbo].[ClientBillingRoles] clientrole          
 ON allocation.ClientBillingRoleId = clientrole.ClientBillingRoleId          
 LEFT JOIN [dbo].[InternalBillingRoles] internalrole          
 ON allocation.InternalBillingRoleId = internalrole.InternalBillingRoleId          
 --INNER JOIN [dbo].[RoleMaster] rolemaster          
 --ON allocation.RoleMasterID = rolemaster.RoleMasterID          
 INNER JOIN [dbo].[Projects] project          
 ON allocation.ProjectId = project.ProjectId          
 INNER JOIN [dbo].[Clients] client          
 ON project.ClientId = client.ClientId          
 INNER JOIN [dbo].[Employee] employee          
 ON allocation.EmployeeId = employee.EmployeeId    
  LEFT JOIN [dbo].[EmployeeType] employeeType 
   on  employee.EmployeeTypeId=employeeType.EmployeeTypeId
 INNER JOIN [dbo].[Grades] grade          
 ON employee.GradeId = grade.GradeId          
 INNER JOIN [dbo].[Departments] department          
 ON employee.DepartmentId = department.DepartmentId          
 INNER JOIN [dbo].[Designations] designation          
 ON employee.Designation = designation.DesignationId          
 INNER JOIN [dbo].[AllocationPercentage] allocationpercentage          
 ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID 
 INNER JOIN ProjectManagers PM ON project.ProjectId = PM.ProjectID   AND PM.IsActive=1
 LEFT JOIN PracticeArea PA ON employee.CompetencyGroup=PA.PracticeAreaId 
 WHERE allocation.EmployeeId = CASE @EmployeeID WHEN 0 THEN allocation.EmployeeId ELSE @EmployeeID END          
 AND allocation.ProjectId = CASE @ProjectID WHEN 0 THEN allocation.ProjectId ELSE @ProjectID END          
 AND Employee.GradeId = CASE @GradeID WHEN 0 THEN Employee.GradeId ELSE @GradeID END          
 AND Employee.Designation = CASE @DesignationID WHEN 0 THEN Employee.Designation ELSE @DesignationID END          
 AND project.ClientId = CASE @ClientID WHEN 0 THEN project.ClientId ELSE @ClientID END          
 AND allocation.AllocationPercentage = CASE @AllocationPercentageID WHEN 0 THEN allocation.AllocationPercentage ELSE @AllocationPercentageID END          
 AND ISNULL(PM.ProgramManagerID, 0) = CASE @ProgramManagerID WHEN 0 THEN ISNULL(PM.ProgramManagerID, 0) ELSE @ProgramManagerID END  
 
 AND ISNULL(PA.PracticeAreaId, 0) = CASE  @practiceAreaID WHEN 0 THEN ISNULL(PA.PracticeAreaId, 0) ELSE @practiceAreaID END         
     
 AND (( @MinExperience = -1 and @MaxExperience = -1 AND ISNULL(employee.TotalExperience,0)=ISNULL(employee.TotalExperience,0 ))      
   OR (@MinExperience <> -1 and @MaxExperience <> -1 AND ISNULL(employee.TotalExperience,0) between @MinExperience and @MaxExperience) )     
    
 AND allocation.IsBillable = CASE @IsBillable WHEN -1 THEN allocation.IsBillable ELSE @IsBillable END          
 AND allocation.IsCritical = CASE @IsCritical WHEN -1 THEN allocation.IsCritical ELSE @IsCritical END          
 AND allocation.IsActive = 1   
    
END 