﻿-- =====================================================
-- Author			:	Sushmitha
-- Create date		:	16-05-2018
-- Modified date	:	22-05-2018
-- Modified By		:	Santosh
-- Description		:	Clone KRAs by financial Year id.
-- ===================================================== 
CREATE PROCEDURE [dbo].[usp_CloneKRAByFinancialYearId]
(  
	 @FromFinancialYearId VARCHAR(50)
	,@ToFinancialYearId INT
	,@CreatedUser VARCHAR(150)
	,@CreatedDate DATETIME
	,@SystemInfo VARCHAR(50)  
)  
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @ApprovedStatusId INT
	DECLARE @DraftStatusId INT
	DECLARE @CategoryId INT
	DECLARE @Count INT
	DECLARE @Flag INT = -8 --Nothing to clone


	SELECT @CategoryId = CategoryID FROM [dbo].[CategoryMaster] WHERE CategoryName = 'KRA'
	SELECT @ApprovedStatusId = StatusId FROM [dbo].[Status] WHERE StatusCode = 'Approved' AND CategoryID = @CategoryId
	SELECT @DraftStatusId = StatusId FROM [dbo].[Status] WHERE StatusCode = 'Draft' AND CategoryID = @CategoryId

	DECLARE @KRAGroup TABLE
	(
		 SNo INT IDENTITY(1,1)
		,KRAGroupId INT
	)

	INSERT INTO @KRAGroup (KRAGroupID)
	SELECT KRAGroupId
	FROM KRAStatus 
	WHERE FinancialYearId = @FromFinancialYearId AND StatusId = @ApprovedStatusId

	SELECT @Count = COUNT(1) FROM @KRAGroup

	IF @Count = 0
	SET @Flag = -5

	DECLARE @RowNum INT = 0
	DECLARE @KRAGroupId INT
	
	WHILE(@RowNum < @Count)
	BEGIN
		SET @RowNum = @RowNum + 1

		SELECT @KRAGroupId = KRAGroupId FROM @KRAGroup WHERE SNo = @RowNum
		
	    --Check if KRADefinition exists for a KRAGroupId
		IF NOT EXISTS(SELECT 'Y' FROM KRADefinition WHERE KRAGroupId = @KRAGroupId AND FinancialYearId = @ToFinancialYearId)
		BEGIN
			--Check if KRAGroupId exists
			IF NOT EXISTS(SELECT 'Y' FROM KRAStatus WHERE KRAGroupId = @KRAGroupId AND FinancialYearId = @ToFinancialYearId)
			BEGIN
				INSERT INTO [dbo].[KRAStatus]
				(
					 KRAGroupId
					,FinancialYearId
					,StatusId
				)  
				SELECT
					 KRAGroupId
					,@ToFinancialYearId
					,@DraftStatusId
			   FROM @KRAGroup WHERE KRAGroupId = @KRAGroupId

			   SET @Flag = 1
			END

		    INSERT INTO [dbo].[KRADefinition]
			(
				 KRAGroupId
				,KRAAspectId
				,Metric
				,FinancialYearId
				,KRAOperatorID
				,KRAMeasurementTypeID
				,KRAScaleMasterID
				,TargetValue
				,KRATargetText
				,KRATargetPeriodID
				,CreatedDate
				,CreatedUser
				,SystemInfo
			)
			SELECT
				 kraDefinition.KRAGroupId
				,kraDefinition.KRAAspectId
				,kraDefinition.Metric
				,@ToFinancialYearId
				,kraDefinition.KRAOperatorID
				,kraDefinition.KRAMeasurementTypeID
				,kraDefinition.KRAScaleMasterID
				,kraDefinition.TargetValue
				,kraDefinition.KRATargetText
				,kraDefinition.KRATargetPeriodID
				,@CreatedDate
				,@CreatedUser
				,@SystemInfo
			    FROM [dbo].[KRADefinition] kraDefinition
		    WHERE KRADefinition.KRAGroupId = @KRAGroupId AND kraDefinition.FinancialYearId = @FromFinancialYearId

			SET @Flag = 1
		END
   END
   SELECT @Flag
END
GO
