﻿
-- ========================================================    
-- Author   : Sabiha    
-- Create date  : 05-03-2019    
-- Modified date : 05-03-2019
-- Modified By  : Sabiha    
-- Description  : Delete Client Billing Role
-- ========================================================     
CREATE PROCEDURE [dbo].[usp_DeleteClientBillingRole] 
(    
 @ClientBillingRoleId INT    
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
    IF EXISTS (SELECT 1 FROM ClientBillingRoles WHERE ClientBillingRoleId = @ClientBillingRoleId)
	BEGIN
		DELETE FROM ClientBillingRoles WHERE ClientBillingRoleId = @ClientBillingRoleId 
		SELECT @@ROWCOUNT
	END
	ELSE 
		SELECT -1 
END