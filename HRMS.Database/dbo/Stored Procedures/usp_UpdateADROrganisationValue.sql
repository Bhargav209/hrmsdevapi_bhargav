﻿-- ======================================      
-- Author   : Sabiha      
-- Create date  : 24-05-2019      
-- Modified date :       
-- Modified By  :       
-- Description  : Update a ADR Organisation Value      
-- ======================================      
      
CREATE PROCEDURE [dbo].[usp_UpdateADROrganisationValue]         
(        
@ADROrganisationValueID INT,               
@ADROrganisationValue VARCHAR(100),           
@DateModified DATETIME,            
@ModifiedUser VARCHAR(150),            
@SystemInfo VARCHAR(50)        
)        
AS        
BEGIN            
            
 SET NOCOUNT ON;          
         
 BEGIN TRY  
 BEGIN        
        
 UPDATE         
 [dbo].[ADROrganisationValueMaster]         
 SET        
 ADROrganisationValue  = @ADROrganisationValue       
 ,ModifiedDate    = @DateModified        
 ,ModifiedUser    = @ModifiedUser        
 ,SystemInfo           = @SystemInfo        
 WHERE         
 ADROrganisationValueID = @ADROrganisationValueID        
            
  SELECT @@ROWCOUNT         
        
END        
END TRY
  BEGIN CATCH
   SELECT ERROR_NUMBER( ) --2627 is Violation in unique index  
  END CATCH         
END 