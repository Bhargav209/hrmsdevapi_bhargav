﻿-- =====================================     
-- Author   : Santosh    
-- Create date  : 24-01-2018    
-- Modified date : 24-01-2018        
-- Modified By  : Santosh    
-- Description  : Get ADR cycle    
-- ===================================== 
CREATE PROCEDURE [dbo].[usp_GetADRCycle]
AS
BEGIN
SET NOCOUNT ON;
 SELECT
  ADRCycleID
 ,ADRCycle
 ,IsActive
 FROM [dbo].[ADRCycle]
END

