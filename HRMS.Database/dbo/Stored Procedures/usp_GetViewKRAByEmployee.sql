﻿  -- ============================================           
-- Author   : Chandra                  
-- Create date  : 23-03-2018                  
-- Modified date :                  
-- Modified By  :                  
-- Description  : Gets View KRA By Employee for Associates      
-- ============================================      
CREATE PROCEDURE [dbo].[usp_GetViewKRAByEmployee]       
(          
 @EmployeeID INT,      
 @FinancialYearID INT      
)               
AS                 
BEGIN       
      
SET NOCOUNT ON;           
           
  SELECT      
    aspectMaster.AspectName AS KRAAspectName           
   ,kraDefinition.Metric as KRAAspectMetric           
   ,kraDefinition.TargetValue AS KRATargetValue
   ,kraOperator.Operator      
   ,kraMeasurement.KRAMeasurementType
   ,kraTargetPeriod.KRATargetPeriod
   FROM [dbo].[AssociateKraMapper] AssociateKraMapper
   INNER JOIN [dbo].[kraGroup] kraGroup ON kraGroup.KRAGroupId = AssociateKraMapper.KRAGroupId 
   INNER JOIN [dbo].[KRADefinition] kraDefinition ON kraGroup.KRAGroupId = kraDefinition.KRAGroupId       
   INNER JOIN [dbo].[KRAAspectMaster] kraAspect ON kraDefinition.KRAAspectId = kraAspect.KRAAspectID
   INNER JOIN [dbo].[KRAOperator] kraOperator ON kraDefinition.KRAOperatorID = kraOperator.KRAOperatorID  
   INNER JOIN [dbo].[KRAMeasurementType] kraMeasurement ON kraDefinition.KRAMeasurementTypeID = kraMeasurement.KRAMeasurementTypeID 
   INNER JOIN [dbo].[KRATargetPeriod] kraTargetPeriod ON kraDefinition.KRATargetPeriodID = kraTargetPeriod.KRATargetPeriodID
   INNER JOIN [dbo].[AspectMaster] aspectMaster ON kraAspect.AspectId = aspectMaster.AspectId  
   WHERE AssociateKraMapper.FinancialYearId= @FinancialYearID AND kraDefinition.FinancialYearId= @FinancialYearID 
   AND associateKraMapper.EmployeeID = @EmployeeID  
END 
