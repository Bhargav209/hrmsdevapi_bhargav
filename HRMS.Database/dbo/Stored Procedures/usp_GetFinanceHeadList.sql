﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	11-01-2018            
-- Modified date	:	11-01-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all finance heads list      
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetFinanceHeadList]
AS              
BEGIN          
          
 SET NOCOUNT ON;          
             
  SELECT   
		employee.EmployeeId AS Id,  
		[dbo].udfGetEmployeeFullName(employee.EmployeeId) AS [Name]  
  FROM   
	[dbo].[Employee] employee  
	INNER JOIN [dbo].[Users] [user] ON employee.UserId = [user].UserId   
	INNER JOIN [dbo].[UserRoles] userrole ON [user].UserId = userrole.UserId   
 WHERE   
	employee.IsActive = 1   
	AND userrole.IsActive = 1   
	AND userrole.RoleId = (SELECT RoleId FROM [dbo].[Roles] WHERE RoleName = 'FinanceHead')
 END

