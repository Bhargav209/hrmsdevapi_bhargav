﻿-- =======================================================================================  
-- Author					:		Sushmitha  
-- Create date				:		12-04-2018  
-- Modified date			:		14-06-2018
-- Modified By				:		Santosh  
-- Modified Description		:		To validate kra definition for groups included KRADefinitionId  
-- Description				:		Gets list of KRA Groups.  
-- =======================================================================================     
CREATE PROCEDURE [dbo].[usp_GetKRAGroups]   
(
	@FinancialYearId INT  
)
AS                      
BEGIN  
  
 SET NOCOUNT ON;        
      
 DECLARE @CategoryId INT  
  
 SELECT @CategoryId = [dbo].[udf_GetCategoryId]('KRA')  
                
 SELECT         
   kraGroup.KRAGroupId    
  ,kraStatus.FinancialYearId    
  ,kraGroup.KRATitle    
  ,department.[Description] AS DepartmentName    
  ,department.DepartmentId   
  ,roleCategory.RoleCategoryID AS RoleCategoryId
  ,roleCategory.RoleCategoryName
  ,projectType.ProjectTypeId    
  ,projectType.ProjectTypeCode
  ,[status].StatusDescription AS StatusDescription    
  ,[status].StatusCode AS StatusCode      
  ,[status].StatusId   
  ,(SELECT COUNT(1) FROM [dbo].[KRADefinition] WHERE KRAGroupId = kraGroup.KRAGroupId AND FinancialYearId = @FinancialYearId) AS KRACount     
 FROM     
 [dbo].[KRAGroup] kraGroup        
 INNER JOIN [dbo].[KRAStatus] kraStatus        
 ON kraGroup.KRAGroupId = kraStatus.KRAGroupId     
 INNER JOIN [dbo].[Departments] department    
 ON kraGroup.DepartmentId = department.DepartmentId    
 INNER JOIN [dbo].[RoleCategory] roleCategory    
 ON kraGroup.RoleCategoryId = roleCategory.RoleCategoryID    
 LEFT JOIN [dbo].[ProjectType] projectType    
 ON kraGroup.ProjectTypeId = projectType.ProjectTypeId    
 INNER JOIN [dbo].[Status] [status]    
 ON kraStatus.StatusId = [status].StatusId  and [status].CategoryID=@CategoryId  
END  
Go