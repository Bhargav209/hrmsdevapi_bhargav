﻿-- ======================================================      
-- Author   : Ramya        
-- Create date  : 01-03-2018    
-- Modified date :   
-- Modified By  : Ramya    
-- Description  : Release an associate from project  
-- [dbo].[usp_ReleaseAssociate] 25,75,30,4,43,'3-Mar-2018','3-Mar-2018','ramya', '192.168.2.208'  
-- ======================================================              
CREATE PROCEDURE [dbo].[usp_ReleaseAssociate]  
(   
 @ProjectId Int ,  
 @EmployeeId INT,  
 @TalentPoolProjectId int,  
 @ReleasingPercentage int,  
 @ReportingManagerId int,  
 @ReleaseDate DateTime,  
 @ModifiedDate Datetime,  
 @ModifiedBy varchar(150),  
 @SystemInfo varchar(50)  
)    
AS    
BEGIN    
 SET NOCOUNT ON;
	
	DECLARE @EffectiveDate DATETIME  
	DECLARE @RoleMasterID INT
	DECLARE @InternalBillingRoleId INT
	DECLARE @ExitingProjectAllocationPercentage INT
	DECLARE @ExitingPoolPercentage INT
	DECLARE @AssociateAllocationId INT  
	DECLARE @AllocationPercentage INT
    SET @ExitingPoolPercentage = 0
	
	SELECT @ReleasingPercentage = Percentage FROM AllocationPercentage WHERE AllocationPercentageID = @ReleasingPercentage
  
    IF EXISTS(SELECT 1 FROM AssociateAllocation allocation WHERE allocation.ProjectId = @ProjectId AND allocation.EmployeeId = @EmployeeId AND allocation.IsActive = 1)
	BEGIN  

		SELECT 
			 @EffectiveDate							=	allocation.EffectiveDate
			,@RoleMasterID							=	RoleMasterId
			,@InternalBillingRoleId					=	InternalBillingRoleId  
			,@ExitingProjectAllocationPercentage	=	allocationPercent.[Percentage] 
			,@AssociateAllocationId					=	AssociateAllocationId  
		FROM AssociateAllocation allocation
		INNER JOIN [AllocationPercentage] allocationPercent
		ON allocation.AllocationPercentage = allocationPercent.AllocationPercentageID
		WHERE allocation.ProjectId = @ProjectId 
		AND allocation.EmployeeId = @EmployeeId 
		AND allocation.IsActive = 1       
  
		UPDATE AssociateAllocation  
			SET ReleaseDate =  
			CASE 
			WHEN @ReleaseDate < @EffectiveDate 
			THEN @EffectiveDate   
			ELSE @ReleaseDate  
			END,  
			IsActive = 0,  
			ModifiedDate = GETDATE(),  
			ModifiedBy = @ModifiedBy,  
			SystemInfo = @SystemInfo  
		WHERE AssociateAllocationId = @AssociateAllocationId
    END

    IF EXISTS(SELECT * FROM AssociateAllocation allocation WHERE allocation.ProjectId = @TalentPoolProjectId AND allocation.EmployeeId = @EmployeeId AND allocation.IsActive = 1)  
    BEGIN

		SET @AssociateAllocationId = 0  
		SELECT 
			 @EffectiveDate = allocation.EffectiveDate
			,@ExitingPoolPercentage = allocationPercent.[Percentage] 
			,@AssociateAllocationId = AssociateAllocationId   
		FROM AssociateAllocation allocation
		INNER JOIN [AllocationPercentage] allocationPercent
		ON allocation.AllocationPercentage = allocationPercent.AllocationPercentageID
		WHERE allocation.ProjectId = @TalentPoolProjectId 
		AND allocation.EmployeeId = @EmployeeId 
		AND allocation.IsActive = 1  
		
		IF @ReleaseDate < @EffectiveDate  
			SET @ReleaseDate = @EffectiveDate  
  
		UPDATE AssociateAllocation  
			SET ReleaseDate =  
			CASE WHEN @ReleaseDate<@EffectiveDate then @EffectiveDate   
			ELSE @ReleaseDate  
			END,  
		IsActive = 0,  
		ModifiedDate = GETDATE(),  
		ModifiedBy = @ModifiedBy,  
		SystemInfo = @SystemInfo  
		WHERE  
		AssociateAllocationId = @AssociateAllocationId  
    END  

    IF (@ReleasingPercentage = @ExitingProjectAllocationPercentage)    
		SET @AllocationPercentage =  @ExitingProjectAllocationPercentage + @ExitingPoolPercentage  
	ELSE
		 SET @AllocationPercentage =  ((@ExitingProjectAllocationPercentage + @ExitingPoolPercentage) - @ReleasingPercentage)  

	SELECT @AllocationPercentage = AllocationPercentageID FROM AllocationPercentage WHERE Percentage = @AllocationPercentage 

	    
	IF (@ReleasingPercentage = @ExitingProjectAllocationPercentage) 
	BEGIN
    INSERT INTO [dbo].[AssociateAllocation]  
           ([ProjectId]  
           ,[EmployeeId]  
           ,[RoleMasterId]  
           ,[IsActive]  
           ,[AllocationPercentage]  
           ,[InternalBillingPercentage]  
           ,[IsCritical]  
           ,[EffectiveDate]  
           ,[AllocationDate]  
           ,[CreatedBy]             
           ,[CreateDate]           
           ,[SystemInfo]  
           ,[ReportingManagerId]            
           ,[IsBillable]  
           ,[InternalBillingRoleId]             
     )           
            
     VALUES  
       (	@TalentPoolProjectId  
           ,@EmployeeId  
           ,@RoleMasterID  
           ,1  
           ,@AllocationPercentage
           ,0  
           ,0  
           ,DATEADD(day, 1, @ReleaseDate)  
           ,Getdate()  
           ,@ModifiedBy             
           ,Getdate()             
           ,@SystemInfo  
           ,@ReportingManagerId             
           ,0  
           ,@InternalBillingRoleId        
           )  
   END  
   ELSE  
    BEGIN  
      
	INSERT INTO [dbo].[AssociateAllocation]  
           ([ProjectId]  
           ,[EmployeeId]  
           ,[RoleMasterId]  
           ,[IsActive]  
           ,[AllocationPercentage]  
           ,[InternalBillingPercentage]  
           ,[IsCritical]  
           ,[EffectiveDate]  
           ,[AllocationDate]  
           ,[CreatedBy]             
           ,[CreateDate]           
           ,[SystemInfo]  
           ,[ReportingManagerId]            
           ,[IsBillable]  
           ,[InternalBillingRoleId]             
     )           
            
     VALUES  
           (@ProjectId  
           ,@EmployeeId  
           ,@RoleMasterID  
           ,1  
           ,@AllocationPercentage 
           ,0  
           ,0  
           ,DATEADD(day, 1, @ReleaseDate)  
           ,Getdate()  
           ,@ModifiedBy             
           ,Getdate()             
           ,@SystemInfo  
           ,@ReportingManagerId             
           ,0  
           ,@InternalBillingRoleId        
            )  
  
  
	SET @AllocationPercentage =  (@ExitingProjectAllocationPercentage+@ExitingPoolPercentage) - (Select Percentage from AllocationPercentage where AllocationPercentageID=@AllocationPercentage)
	SELECT @AllocationPercentage = AllocationPercentageID FROM AllocationPercentage WHERE Percentage = @AllocationPercentage	 
 
    INSERT INTO [dbo].[AssociateAllocation]  
           ([ProjectId]  
           ,[EmployeeId]  
           ,[RoleMasterId]  
           ,[IsActive]  
           ,[AllocationPercentage]  
           ,[InternalBillingPercentage]  
           ,[IsCritical]  
           ,[EffectiveDate]  
           ,[AllocationDate]  
           ,[CreatedBy]             
           ,[CreateDate]           
           ,[SystemInfo]  
           ,[ReportingManagerId]            
           ,[IsBillable]  
           ,[InternalBillingRoleId]             
     )           
            
     VALUES  
           (@TalentPoolProjectId  
           ,@EmployeeId  
           ,@RoleMasterID  
           ,1  
           ,@AllocationPercentage
           ,0  
           ,0  
           ,DATEADD(day, 1, @ReleaseDate)  
           ,Getdate()  
           ,@ModifiedBy             
           ,Getdate()             
           ,@SystemInfo  
           ,@ReportingManagerId             
           ,0  
           ,@InternalBillingRoleId        
           )
	END	                      
  SELECT @@ROWCOUNT  
END
