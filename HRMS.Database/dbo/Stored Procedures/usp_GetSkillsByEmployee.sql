﻿CREATE PROCEDURE [dbo].[usp_GetSkillsByEmployee]  
(  
@EmployeeID INT  
)  
AS  
BEGIN  
  
SET NOCOUNT ON;    
SELECT  DISTINCT  
  sa.ID,  
  sa.EmployeeId, 
  sa.SkillId,
  sa.SkillGroupId,
  sw.SubmittedRequisitionId,  
  sk.SkillName ,  
  sa.Experience,  
  sa.IsPrimary,  
  sa.LastUsed,  
  st.StatusCode,
  sa.CompetencyAreaId, 
  sa.proficiencyLevelId,
  sk.SkillCode
  
FROM   
 SkillsSubmittedForApproval sa  
 INNER JOIN SkillsWorkFlow sw on sa.EmployeeId=sw.SubmittedBy  
 INNER JOIN EmployeeSkills es on sa.EmployeeId=es.EmployeeId  
 INNER JOIN skills sk on sa.skillId=sk.skillId  
 INNER JOIN Status st on sw.Status=st.StatusId  
WHERE   
 --Taking only Draft and Approved states  
 sa.EmployeeId=@EmployeeID   and st.CategoryID=3 and sw.Status in (19,20) and sa.ID=sw.SubmittedRequisitionId   
   
END