﻿-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	30-11-2017
-- Modified date	:	30-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Gets skills by skillGroupID
-- ======================================================= 

CREATE PROCEDURE [dbo].[usp_GetSkillsbySkillGroupID]
@SkillGroupID INT
AS      
BEGIN  
  
 SET NOCOUNT ON;

 SELECT  
	SkillId
   ,SkillName 
 FROM 
	[dbo].[Skills]
 WHERE 
	SkillGroupId = @SkillGroupID AND Isnull(IsActive,1) = 1 AND IsApproved = 1
END

