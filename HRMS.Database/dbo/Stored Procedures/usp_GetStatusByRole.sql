﻿-- ==========================================     
-- Author                  :      Basha            
-- Create date             :      28-06-2018            
-- Modified date     :      28-06-2018            
-- Modified By             :      Basha            
-- Description             :      Gets Status By Role      
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetStatusByRole] 
@RoleName varchar(50),
@CategoryId INT            
AS              
BEGIN          
          
 SET NOCOUNT ON;    
 
 --Declaring SQL Statement  for selecting statusid and statuscode for particular categoryId
DECLARE @SQL NVARCHAR(MAX)
SET @SQL = 'SELECT  distinct
                                  rolestatus.statusid As Id
                                  ,status.StatusCode As Name
                                  from Rolestatus rolestatus
                                  JOIN Status status
                                  ON status.StatusId = roleStatus.statusId 
                  WHERE status.CategoryId = ''' +  CAST(@categoryId AS VARCHAR) + ''''
      

         IF(@RoleName = 'Delivery Head')
              SET @SQL = @SQL + ' AND status.StatusId in (3,4)'

       EXEC sp_executesql @SQL

END

