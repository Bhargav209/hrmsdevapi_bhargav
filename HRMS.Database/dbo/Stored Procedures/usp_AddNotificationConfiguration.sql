﻿-- ======================================================  
-- Author		:	Santosh     
-- Create date  :	16-11-2017           
-- Modified date:	16-11-2017  
-- Modified By  :	Santosh    
-- Description  :	Adds a new notification category      
-- ====================================================== 
CREATE PROCEDURE [dbo].[usp_AddNotificationConfiguration]            
@NotificationTypeID INT,           
@EmailFrom VARCHAR(MAX),        
@EmailTo VARCHAR(MAX),        
@EmailCC VARCHAR(MAX),        
@EmailSubject VARCHAR(150),        
@CreatedBy VARCHAR(150),        
@CreatedDate DATETIME,
@SystemInfo VARCHAR(50)
AS            
BEGIN        
        
 SET NOCOUNT ON;        
         
	INSERT INTO [dbo].[NotificationConfiguration]
	(
		 NotificationTypeID   
		,EmailFrom		
		,EmailTo		
		,EmailCC		
		,EmailSubject			
		,CreatedBy		
		,CreatedDate
		,SystemInfo
	)
	VALUES
	(
		 @NotificationTypeID   
		,@EmailFrom		
		,@EmailTo		
		,@EmailCC		
		,@EmailSubject			
		,@CreatedBy		
		,@CreatedDate	
		,@SystemInfo
	)
                   
 SELECT @@ROWCOUNT            
END
