﻿
-- ==========================================                   
-- Author   : Mithun                  
-- Create date  : 26-02-2019                  
-- Modified date : 26-02-2019                       
-- Modified By  : Mithun                  
-- Description  : Freeze Finance Report Data     
--[dbo].[usp_rpt_FreezeFinanceReport]      
-- ==========================================                
CREATE PROCEDURE [dbo].[usp_rpt_CheckFinanceReportExists]      
(        
  @Month INT      
 ,@Year INT      
 
)             
AS               
BEGIN      
 SET NOCOUNT ON;        
       
SELECT DISTINCT 1 FROM FreezFinanceReportData where Month=@Month AND Year=@Year

END