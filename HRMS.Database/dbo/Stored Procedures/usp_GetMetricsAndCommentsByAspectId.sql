﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-02-2018
-- Modified date	:	08-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets KRA Metrics and Comments by AspectId, EmployeeId, Financial year and ADRCycleID.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetMetricsAndCommentsByAspectId]
(
	@EmployeeID INT,
	@ADRCycleID INT,
	@FinancialYearID INT,
	@KRAAspectID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	/*
	SELECT
		kraSet.KRAAspectMetric AS KRAMetric
		,review.CriticalTasksPerformed
	FROM [ADRAssociateKRAReview] review
	INNER JOIN [KRASet] kraSet
	ON review.KRASetID = kraSet.KRASetID
	INNER JOIN [KRAAspectMaster] aspect
	ON kraSet.KRAAspectID = aspect.KRAAspectID
	WHERE review.EmployeeID = @EmployeeID
	AND review.ADRCycleID = @ADRCycleID
	AND review.FinancialYearID = @FinancialYearID
	AND aspect.KRAAspectID = @KRAAspectID
	*/
END

