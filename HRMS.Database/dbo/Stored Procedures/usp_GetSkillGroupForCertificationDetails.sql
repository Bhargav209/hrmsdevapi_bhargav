﻿
-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	27-11-2017
-- Modified date	:	27-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Gets skill groups for competency 'Certification'
-- ======================================================= 

CREATE PROCEDURE [dbo].[usp_GetSkillGroupForCertificationDetails]
   
AS      
BEGIN  
  
 SET NOCOUNT ON;

 SELECT 
	 skillGroup.SkillGroupId 
	,skillGroup.SkillGroupName
 FROM 
	[dbo].[SkillGroup] skillGroup
 INNER JOIN 
	[dbo].[CompetencyArea] competency ON skillGroup.CompetencyAreaId = competency.CompetencyAreaId
 WHERE 
	competency.CompetencyAreaCode = 'Certification'
END

