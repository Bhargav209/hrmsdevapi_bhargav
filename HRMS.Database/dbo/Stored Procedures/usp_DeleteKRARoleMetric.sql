﻿-- ========================================================
-- Author			:	Ramya
-- Create date		:	17-04-2018
-- Modified date	:	
-- Modified By		:	Ramya
-- Description		:	Deletes KRA Role metric. By kra definition Id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_DeleteKRARoleMetric]
(
@KRADefinitionId INT,
@StatusId INT,
@RoleName VARCHAR(100)
)
AS
BEGIN  
  
 SET NOCOUNT ON;
 DECLARE @DraftStatusId INT
 DECLARE @SendBackStatusId INT
 DECLARE @SubmittedForDH INT
 DECLARE @SubmittedForHRHead INT
 DECLARE @SendBacktoDepartmentHead INT 

 SET @DraftStatusId = (SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA'))
 SET @SendBackStatusId = (SELECT [dbo].[udf_GetStatusId]('SendBackForHRMReview', 'KRA'))
 SET @SubmittedForDH = (SELECT [dbo].[udf_GetStatusId]('SubmittedForDepartmentHeadReview', 'KRA'))
 SET @SubmittedForHRHead = (SELECT [dbo].[udf_GetStatusId]('SubmittedForHRHeadReview','KRA'))
 SET @SendBacktoDepartmentHead = (SELECT [dbo].[udf_GetStatusId]('SendBackForDepartmentHeadReview','KRA'))

 IF ((@RoleName = 'HRM' AND (@StatusId =  @DraftStatusId OR @StatusId = @SendBackStatusId)) OR (@RoleName = 'Department Head' AND @StatusId = @SubmittedForDH OR @StatusId = @SendBacktoDepartmentHead) OR (@RoleName = 'HR Head' AND @StatusId = @SubmittedForHRHead))
 BEGIN
	DELETE FROM [dbo].[KRADefinition]
	WHERE
		KRADefinitionId = @KRADefinitionId 

	SELECT @@ROWCOUNT    

 END
 ELSE
 BEGIN
	SELECT -1
 END        
END
GO
