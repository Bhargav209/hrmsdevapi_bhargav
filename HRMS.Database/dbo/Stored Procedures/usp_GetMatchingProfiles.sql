﻿--===============================================================
 --Author			:	Santosh
 --Create date		:	15-12-2017
 --Modified date	:	29-01-2018
 --Modified By		:	Santosh
 --Description		:	Get matching profiles by search criteria.
 --[usp_GetMatchingProfiles] 0, '', '', 0, 0
 --==============================================================  
CREATE PROCEDURE [dbo].[usp_GetMatchingProfiles]
@RoleId INT,
@SkillId VARCHAR(250),
@ProficiencyLevelId VARCHAR(15),
@MinimumExperience INT,
@MaximumExperience INT,
@TalentRequisitionId INT NULL
AS        
BEGIN
	SET NOCOUNT ON;  
 
	--DECLARE @StatusId INT

	--SELECT @StatusId=[dbo].[udf_GetStatusId]('Close', 'TalentRequisition')

	--IF @MaximumExperience = 0
	--SELECT @MaximumExperience = MAX(Experience) FROM [dbo].[SkillSearch]  

	--DECLARE @ProjectId INT
	--SELECT @ProjectId = projectId FROM TalentRequisition WHERE TRId = @TalentRequisitionId
      
	--SELECT DISTINCT
	--  skillSearch.EmployeeID 
	-- ,skillSearch.EmployeeCode  
	-- ,skillSearch.FirstName + ' ' + skillSearch.LastName AS EmployeeName
	-- ,skillSearch.Experience
	-- ,skillSearch.DesignationCode 
	-- --,SkillSearch.RoleMasterID
	--FROM
	--[dbo].[SkillSearch]
	--WHERE     
	--RoleMasterID = CASE @RoleId WHEN  0 THEN skillSearch.RoleMasterID ELSE @RoleId END
	--AND Experience >= @MinimumExperience AND Experience <= @MaximumExperience   
	--AND
	--( @SkillId != '' AND skillSearch.SkillID IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@SkillId,','))
	-- OR (@SkillId = '' AND (skillSearch.SkillID IN (SELECT DISTINCT SkillId FROM [dbo].[SkillSearch]) OR skillSearch.SkillID IS NULL))
	--)    
	--AND
	--( @ProficiencyLevelId != '' AND skillSearch.ProficiencyLevelID IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@ProficiencyLevelId,','))
	-- OR (@ProficiencyLevelId = '' AND (skillSearch.ProficiencyLevelID IN (SELECT ProficiencyLevelId AS ProficiencyLevelId FROM [dbo].[ProficiencyLevel]) OR skillSearch.ProficiencyLevelID IS NULL))
	--)
	--AND skillSearch.EmployeeID NOT IN (SELECT EmployeeID FROM TalentRequisitionEmployeeTag WHERE TalentRequisitionID IN (SELECT TRId FROM TalentRequisition WHERE StatusId <> @StatusId )) -- Ignore Closed TR
	--AND SkillSearch.EmployeeID NOT IN (SELECT EmployeeID FROM AssociateAllocation WHERE ProjectId = @ProjectId AND IsActive = 1)

	--SELECT DISTINCT
	--  skillSearch.EmployeeID
	-- ,skillSearch.ProjectName
	-- ,Isnull(skillSearch.IsPrimary,0) as IsPrimary
	-- ,skillSearch.IsCritical 
	-- ,skillSearch.IsBillable     
	--FROM
	--[dbo].[SkillSearch]     
	--WHERE       
	--RoleMasterID = CASE @RoleId WHEN  0 THEN skillSearch.RoleMasterID ELSE @RoleId END
	--AND Experience >= @MinimumExperience AND Experience <= @MaximumExperience     
	--AND 
	--( @SkillId != '' AND skillSearch.SkillID IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@SkillId,','))
	-- OR (@SkillId = '' AND (skillSearch.SkillID IN (SELECT DISTINCT SkillId FROM [dbo].[SkillSearch]) OR skillSearch.SkillID IS NULL))
	--)    
	--AND
	--( @ProficiencyLevelId != '' AND skillSearch.ProficiencyLevelID IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@ProficiencyLevelId,','))
	-- OR (@ProficiencyLevelId = '' AND (skillSearch.ProficiencyLevelID IN (SELECT ProficiencyLevelId AS ProficiencyLevelId FROM [dbo].[ProficiencyLevel]) OR skillSearch.ProficiencyLevelID IS NULL))
	--)
	--AND skillSearch.EmployeeID NOT IN (SELECT EmployeeID FROM TalentRequisitionEmployeeTag WHERE TalentRequisitionID IN (SELECT TRId FROM TalentRequisition WHERE StatusId <> @StatusId)) -- Ignore Closed TR
	--AND SkillSearch.EmployeeID NOT IN (SELECT EmployeeID FROM AssociateAllocation WHERE ProjectId = @ProjectId AND IsActive = 1)
	  
	--SELECT DISTINCT
	--  skillSearch.EmployeeID
	-- ,skillSearch.CompetencyAreaCode
	-- ,skillSearch.SkillGroupName  
	-- ,skillSearch.SkillName   
	-- ,skillSearch.ProficiencyLevelCode    
	--FROM     
	--[dbo].[SkillSearch]       
	--WHERE
	--RoleMasterID = CASE @RoleId WHEN  0 THEN skillSearch.RoleMasterID ELSE @RoleId END
	--AND Experience >= @MinimumExperience AND Experience <= @MaximumExperience
	--AND
	--( @SkillId != '' AND skillSearch.SkillID IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@SkillId,','))
	-- OR (@SkillId = '' AND (skillSearch.SkillID IN (SELECT DISTINCT SkillId FROM [dbo].[SkillSearch]) OR skillSearch.SkillID IS NULL))
	--)
	--AND
	--( @ProficiencyLevelId != '' AND skillSearch.ProficiencyLevelID IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@ProficiencyLevelId,','))
	-- OR (@ProficiencyLevelId = '' AND (skillSearch.ProficiencyLevelID IN (SELECT ProficiencyLevelId AS ProficiencyLevelId FROM [dbo].[ProficiencyLevel]) OR skillSearch.ProficiencyLevelID IS NULL))
	--)
	--AND skillSearch.EmployeeID NOT IN (SELECT EmployeeID FROM TalentRequisitionEmployeeTag WHERE TalentRequisitionID IN (SELECT TRId FROM TalentRequisition WHERE StatusId <> @StatusId)) -- Ignore Closed TR
	--AND SkillSearch.EmployeeID NOT IN (SELECT EmployeeID FROM AssociateAllocation WHERE ProjectId = @ProjectId AND IsActive = 1)
	SELECT 1;

 END

