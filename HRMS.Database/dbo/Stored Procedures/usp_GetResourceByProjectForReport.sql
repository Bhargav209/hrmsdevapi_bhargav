﻿-- ================================================  
-- Author			:  Kalyan Penumutchu      
-- Create date		:  13-04-2018      
-- Modified date	:  18-04-2018      
-- Modified By		:  Kalyan Penumutchu
-- Description		:  This SP is used Reports of HRMS. Accepts Project id and returns count of billable and non billable per project.     
-- ================================================  
CREATE PROCEDURE [dbo].[usp_GetResourceByProjectForReport]
(	
	@projectId INT = 0 
)
AS
SELECT  
	project.projectname AS [ProjectName],
	count(*) AS [ResourceCount],
	ISNULL((
				SELECT COUNT(*) FROM AssociateAllocation bb join projects pp ON bb.ProjectId = pp.ProjectId
				WHERE associateAllocation.projectid=bb.projectid and bb.IsActive=1 and releasedate is null and IsBillable=1
				GROUP BY bb.ProjectId
				),0) 
				As BillableCount,
	ISNULL((
				SELECT COUNT(*) FROM AssociateAllocation bb join projects pp ON bb.ProjectId = pp.ProjectId
				WHERE associateAllocation.projectid=bb.projectid and bb.IsActive=1 and releasedate is null and IsBillable=0
				GROUP BY bb.ProjectId
				) ,0)
				As [NonBillableCount]
FROM AssociateAllocation associateAllocation 
INNER JOIN Projects project ON associateAllocation.ProjectId = project.ProjectId
WHERE associateAllocation.projectid=@projectId and associateAllocation.IsActive=1 and associateAllocation.releasedate is null
GROUP BY associateAllocation.projectid,project.projectname 
ORDER BY project.projectname

