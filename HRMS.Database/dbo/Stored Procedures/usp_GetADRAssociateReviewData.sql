﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-02-2018
-- Modified date	:	08-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Get(s) ADR review data.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetADRAssociateReviewData] --214, 3, 2
(
	@EmployeeID INT,
	@ADRCycleID INT,
	@FinancialYearID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	/*SELECT
		 review.ID AS ReviewID
		,aspect.KRAAspectName
		,kraSet.KRAAspectMetric
		,kraSet.KRAAspectTarget
		,review.CriticalTasksPerformed
	FROM [ADRAssociateKRAReview] review
	INNER JOIN [KRASet] kraSet
	ON review.KRASetID = kraSet.KRASetID
	INNER JOIN [KRAAspectMaster] aspect
	ON kraSet.KRAAspectID = aspect.KRAAspectID
	WHERE review.EmployeeID = @EmployeeID
	AND review.ADRCycleID = @ADRCycleID
	AND review.FinancialYearID = @FinancialYearID
	ORDER BY aspect.KRAAspectName
	*/
END

