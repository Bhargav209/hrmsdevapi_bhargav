﻿-- ============================================================================  
-- Author   : Santosh              
-- Create date  : 20-11-2017              
-- Modified date : 20-11-2017              
-- Modified By  : Santosh              
-- Description  : Gets notification configuration by NotificationTypeID  
-- ============================================================================  
CREATE PROCEDURE [dbo].[usp_GetNotificationConfigurationByNotificationTypeID]
@NotificationTypeID INT,
@CategoryId INT  
AS                
BEGIN            
        
 SET NOCOUNT ON;  
         
 SELECT    
   notificationType.NotificationDesc AS [Description]    
  ,[notificationType].NotificationTypeID
  ,[notificationType].CategoryId      
  ,[configuration].EmailFrom      
  ,[configuration].EmailTo      
  ,[configuration].EmailCC      
  ,[configuration].EmailSubject   
  ,[configuration].EmailContent   
 FROM [dbo].[NotificationConfiguration] [configuration]      
 RIGHT JOIN [dbo].[NotificationType] notificationType    
 ON [configuration].NotificationTypeID = notificationType.NotificationTypeID and [configuration].CategoryId= @CategoryId   
 WHERE notificationType.NotificationTypeID = @NotificationTypeID AND notificationType.CategoryId = @CategoryId
   
END

