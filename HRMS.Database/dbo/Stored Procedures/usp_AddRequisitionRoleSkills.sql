﻿
-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	30-11-2017            
-- Modified date	:	04-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Add requisition role skills     
-- ==========================================  

CREATE PROCEDURE [dbo].[usp_AddRequisitionRoleSkills]            
 @RoleSkillMappingID INT,
 @CompetencyAreaID INT,
 @SkillGroupID INT,
 @SkillID INT,
 @ProficiencyLevelID INT,
 @CreatedUser VARCHAR(100),
 @CreatedDate DATETIME,
 @SystemInfo VARCHAR(50) 
AS            
BEGIN        
        
 SET NOCOUNT ON;

 INSERT INTO [dbo].[RequisitionRoleSkills]            
 (      
     RoleSkillMappingID
	,CompetencyAreaID
	,SkillGroupID
	,SkillID
	,ProficiencyLevelID
	,CreatedUser
	,CreatedDate
	,SystemInfo
 )            
 VALUES            
 (      
     @RoleSkillMappingID
	,@CompetencyAreaID
	,@SkillGroupID
	,@SkillID
	,@ProficiencyLevelID
	,@CreatedUser
	,@CreatedDate
	,@SystemInfo
 )       
              
 SELECT @@ROWCOUNT            
END
