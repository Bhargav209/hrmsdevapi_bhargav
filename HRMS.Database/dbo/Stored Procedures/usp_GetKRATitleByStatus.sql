﻿-- ====================================
-- Author			: Sushmitha    
-- Create date		: 16-07-2018      
-- Modified date	: 16-07-2018      
-- Modified By		: Sushmitha      
-- Description		: Get KRA Title by status
-- ====================================      
CREATE PROCEDURE [dbo].[usp_GetKRATitleByStatus]
(
	@DepartmentID INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @StatusId INT
	DECLARE @FinancialYearId INT

	SET @StatusId = (SELECT [dbo].[udf_GetStatusId]('Approved', 'KRA'))

	SELECT @FinancialYearId = ID FROM [dbo].[FinancialYear] WHERE IsActive = 1
	
	SELECT
		 kraGroup.KRAGroupID AS ID
		,kraGroup.KRATitle AS Name
	FROM  [dbo].[KRAGroup] kraGroup
	INNER JOIN [dbo].[KRAStatus] kraStatus
	ON kraGroup.KRAGroupId = kraStatus.KRAGroupId
	WHERE kraStatus.FinancialYearId = @FinancialYearId
	AND kraGroup.DepartmentID = @DepartmentID	
	AND kraStatus.StatusId = @StatusId 

END
Go
