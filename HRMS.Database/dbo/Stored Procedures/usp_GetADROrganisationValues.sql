﻿-- ======================================      
-- Author   : Sabiha      
-- Create date  : 24-05-2019      
-- Modified date : 04-06-2019      
-- Modified By  : Mithun      
-- Description  : Get ADR Organisation Values      
-- ======================================      
CREATE PROCEDURE [dbo].[usp_GetADROrganisationValues]  
(@FinancialYearId INT)    
AS      
BEGIN      
 SET NOCOUNT ON;      
  SELECT      
    ADROrganisationValueID      
   ,ADROrganisationValue  
   ,fy.FromYear as FromYear  
 ,fy.ToYear as ToYear  
  FROM ADROrganisationValueMaster orgValue  
  INNER JOIN FinancialYear fy   
  ON orgValue.FinancialYearId = fy.ID  
   WHERE orgValue.FinancialYearId=@FinancialYearId    
  ORDER BY CreatedDate DESC  
END