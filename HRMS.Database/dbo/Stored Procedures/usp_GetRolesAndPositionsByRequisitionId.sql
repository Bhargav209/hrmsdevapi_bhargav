﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-01-2018
-- Modified date	:	08-01-2018
-- Modified By		:	Sunitha
-- Description		:	Get requisition details by talent requisition ID.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRolesAndPositionsByRequisitionId]
@TalentRequisitionId INT
AS
BEGIN

SET NOCOUNT ON;

SELECT
	roles.RoleMasterID As RoleID,   
    [dbo].[udf_GetRoleName](roles.RoleMasterID) AS RoleName
   ,ISNULL(requisitionRoleDetails.NoOfBillablePositions, 0) AS NoOfBillablePositions
   ,ISNULL(requisitionRoleDetails.NoOfNonBillablePositions, 0) AS NoOfNonBillablePositions
   ,(ISNULL(requisitionRoleDetails.NoOfBillablePositions, 0) + ISNULL(requisitionRoleDetails.NoOfNonBillablePositions, 0)) AS TotalPositions
   ,(SELECT COUNT(EmployeeID) FROM TalentRequisitionEmployeeTag WHERE TalentRequisitionID = @TalentRequisitionId AND RoleMasterId = requisitionRoleDetails.RoleMasterId  ) AS TaggedEmployees
FROM 
	[dbo].[RequisitionRoleDetails] requisitionRoleDetails  
 INNER JOIN [dbo].[TalentRequisition] requisition
 ON requisitionRoleDetails.TRId = requisition.TRId
 INNER JOIN [dbo].[RoleMaster] roles   
 ON requisitionRoleDetails.RoleMasterId = roles.RoleMasterID 

WHERE 
	requisitionRoleDetails.TRId = @TalentRequisitionId

END

