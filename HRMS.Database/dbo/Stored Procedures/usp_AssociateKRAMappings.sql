﻿--sp_helptext usp_AssociateKRAMappings    
    
          
-- ========================================================          
-- Author   : Chandra          
-- Create date  : 04-18-2018          
-- Modified date : 04-18-2018 , 17-5-2019 , 20-5-2019        
-- Modified By  : Chandra, Hemamalini          
-- Description  : Associate KRA Mappings.          
-- ========================================================           
          
CREATE PROCEDURE [dbo].[usp_AssociateKRAMappings]          
(            
@EmployeeID INT,           
@KRAGroupID INT,        
@FinancialYearID INT,        
@CreatedDate DATETIME,            
@IsActive bit          
)            
AS          
BEGIN          
          
 SET NOCOUNT ON;        
           
 IF EXISTS (SELECT * FROM [dbo].[AssociateKRAMapper]  WHERE EmployeeID = @EmployeeID AND FinancialYearID=@FinancialYearID )      
 BEGIN      
 UPDATE            
 [dbo].[AssociateKRAMapper] SET KRAGroupID=@KRAGroupID, IsPDFGenerated = 0   WHERE EmployeeID = @EmployeeID AND FinancialYearID=@FinancialYearID      
 END      
 ELSE      
 BEGIN      
  INSERT INTO           
 [dbo].[AssociateKRAMapper]            
 (EmployeeID, KRAGroupID, FinancialYearID, CreatedDate,IsActive,IsPDFGenerated)            
 VALUES            
 (@EmployeeID, @KRAGroupID, @FinancialYearID, @CreatedDate,@IsActive, 0)        
 END      
  SELECT @@ROWCOUNT           
      
END 