﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	18-01-2018
-- Modified date	:	18-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Delete Requisition Role Details.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_DeleteRequisitionRoleDetails] --1036
@RequisitionRoleDetailId INT,
@TalentRequisitionId INT,
@RoleMasterId INT
AS
BEGIN

 SET NOCOUNT ON;

 --Deletes record from RequisitionRoleSkillMapping and RequisitionRoleSkills as on delete cascade has been used.
 DELETE FROM 
	[dbo].[RequisitionRoleSkillMapping] 
 WHERE 
	RequistiionRoleDetailID = @RequisitionRoleDetailId 
	AND TRID = @TalentRequisitionId

 DELETE FROM
	[dbo].[TalentRequisitionEmployeeTag]
 WHERE
	TalentRequisitionID = @TalentRequisitionId
	AND RoleMasterID = @RoleMasterId

 DELETE FROM 
	[dbo].[RequisitionRoleDetails] 
 WHERE 
	RequisitionRoleDetailID = @RequisitionRoleDetailId 
	AND TRId = @TalentRequisitionId 

 Select @@ROWCOUNT

END

