﻿CREATE PROCEDURE [dbo].[usp_GetProjectStatuses]    
               
AS                    
BEGIN                
               
 SET NOCOUNT ON; 
 DECLARE @CategoryId INT 
   
  SELECT @CategoryId=CategoryId FROM CategoryMaster WHERE CategoryName = 'PPC'
  SELECT 
   status.StatusId AS StatusId
  ,status.StatusCode AS StatusCode  
 FROM Status status    
 WHERE status.CategoryID = @CategoryId  
END  
