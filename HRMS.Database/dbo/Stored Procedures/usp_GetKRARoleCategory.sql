﻿-- ============================================        
-- Author        : Sushmitha            
-- Create date   : 12-04-2018            
-- Modified date : 12-04-2018            
-- Modified By   : Sushmitha            
-- Description   : Gets KRARoleCategory         
-- ============================================      
CREATE PROCEDURE [dbo].[usp_GetKRARoleCategory]            
AS            
BEGIN        
        
 SET NOCOUNT ON;
         
 SELECT      
	 RoleCategoryID AS ID    
	,RoleCategoryName AS Name    
 FROM 
	[dbo].[RoleCategory]   
	   
END
Go
