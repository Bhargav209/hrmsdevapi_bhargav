﻿-- ========================================================
-- Author			:	Prasanna 
-- Create date		:	02-11-2018
-- Modified date	:	02-11-2018
-- Modified By		:	Prasanna
-- Description		:	Update internal Billing Role.
-- ========================================================   
  
CREATE PROCEDURE [dbo].[usp_UpdateInternalBillingRole]
(  
	@InternalBillingRoleID INT  
	,@InternalBillingRoleCode VARCHAR(50)
	,@InternalBillingRoleName VARCHAR(70)
	,@ModifiedDate   DATETIME
	,@ModifiedUser VARCHAR(100)
	,@SystemInfo VARCHAR(50)
)  
AS  
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS(Select InternalBillingRoleId from InternalBillingRoles where InternalBillingRoleCode=@InternalBillingRoleCode and InternalBillingRoleId!=@InternalBillingRoleID)
		BEGIN
			UPDATE InternalBillingRoles
			SET  
				InternalBillingRoleCode		=   @InternalBillingRoleCode        
				,InternalBillingRoleName    =   @InternalBillingRoleName    
				,ModifiedDate    			=   @ModifiedDate  
				,ModifiedUser	      		=   @ModifiedUser    
				,SystemInfo    				=   @SystemInfo 			
			WHERE InternalBillingRoleId		=	@InternalBillingRoleID

			SELECT @@ROWCOUNT    
		END
	ELSE
		SELECT -1

END