﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	28-03-2018
-- Modified date	:	28-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Adds Notification type.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateNotificationType]
(  
@NotificationCode NVARCHAR(50),
@NotificationDescription NVARCHAR(150),
@CategoryId INT, 
@CreatedUser VARCHAR(150),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON;  

  UPDATE 
	[dbo].[NotificationType]
 SET
	ModifiedDate = GETDATE()
 WHERE 
	NotificationCode = @NotificationCode

 IF (@@ROWCOUNT = 0)
 BEGIN
 --Select  Max Number for  combination of NotificationTypeID  and Category 
  DECLARE @MaNotificationTypeID INT = 0;
  SELECT @MaNotificationTypeID = Max(NotificationTypeID) FROM NotificationType WHERE CategoryId = @CategoryId

 --Incrementing next value  to NotificationTypeID
  SET @MaNotificationTypeID = IsNull(@MaNotificationTypeID,0) + 1

 INSERT INTO 
	[dbo].[NotificationType]  
	(NotificationTypeID,NotificationCode, NotificationDesc, CategoryId, CreatedUser, SystemInfo)  
 VALUES  
	(@MaNotificationTypeID,@NotificationCode, @NotificationDescription, @CategoryId, @CreatedUser, @SystemInfo)  
  
 SELECT @@ROWCOUNT
    
END
ELSE

SELECT -1

END
