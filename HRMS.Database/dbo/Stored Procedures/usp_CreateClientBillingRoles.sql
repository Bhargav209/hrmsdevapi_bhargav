﻿-- ======================================================      
-- Author   : Basha        
-- Create date  : 12-12-2018    
-- Modified date : 02-01-2019    
-- Modified By  : Bhavani    
-- Description  : Create Project Client Billing Roles
-- [dbo].[usp_CreateClientBillingRoles]
-- ======================================================              

CREATE   PROCEDURE [dbo].[usp_CreateClientBillingRoles]          
(            
@ProjectId INT,          
@ClientBillingRoleId VARCHAR(150),
@NoOfPositions INT,          
@CreatedUser VARCHAR(150),            
@SystemInfo VARCHAR(50),
@ProjectClientBillingRoleId INT NULL,
@AddendumNumber VARCHAR(100) NULL,
@ReasonForUpdate VARCHAR(MAX) NULL
)            
AS          
BEGIN       
SET NOCOUNT ON;   

	IF (@ProjectClientBillingRoleId = NULL OR @ProjectClientBillingRoleId = 0)
	BEGIN
	INSERT INTO ProjectClientBillingRoles (ProjectId,ClientBillingRoleId,NoOfPositions,CreatedUser,CreatedDate,SystemInfo,IsActive) VALUES 
		(@ProjectId,@ClientBillingRoleId,@NoOfPositions,@CreatedUser,GETDATE(),@SystemInfo,1)
	END
END