﻿-- ========================================================
-- Author			:	Prasanna
-- Create date		:	02-11-2018
-- Modified date	:	02-11-2018
-- Modified By		:	Prasanna
-- Description		:	Add internal Billing Roles
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_CreateInternalBillingRole]
(
	@InternalBillingRoleCode VARCHAR(50)
	,@InternalBillingRoleName VARCHAR(70)
	,@IsActive bit = 1 
	,@CreatedUser VARCHAR(100)
	,@CreatedDate   DATETIME
	,@SystemInfo VARCHAR(50)
)
AS
BEGIN
 SET NOCOUNT ON;
	IF not exists (Select InternalBillingRoleId from InternalBillingRoles where InternalBillingRoleCode=@InternalBillingRoleCode)
	   BEGIN
		 INSERT INTO InternalBillingRoles
		  (
			 InternalBillingRoleCode  
			,InternalBillingRoleName  
			,IsActive  
			,CreatedUser  
			,CreatedDate  
			,SystemInfo  		
		  )  
		 VALUES  
		 (  
			@InternalBillingRoleCode
			,@InternalBillingRoleName
			,@IsActive
			,@CreatedUser  
			,@CreatedDate  
			,@SystemInfo 
		  )
		SELECT @@ROWCOUNT
	   END
	ELSE
		SELECT -1
END