﻿
-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	16-11-2017
-- Modified date	:	16-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Gets Associates Resignations Lists  
-- ======================================================= 

CREATE PROCEDURE [dbo].[usp_GetAssociateResignationsList]
@ProgramManagerID INT
AS
BEGIN
 SET NOCOUNT ON;

 DECLARE @StatusID INT
 
 SELECT @StatusID = StatusID FROM [dbo].[Status] WHERE StatusCode = 'SubmittedForResignation'

 SELECT
  resignation.ResignationID
 ,resignation.EmployeeID
 ,resignation.ReasonDescription
 ,resignation.DateOfResignation
 ,[dbo].[udf_NoticePeriodCalculation] (resignation.DateOfResignation) AS LastWorkingDate
 ,employee.EmployeeCode
 ,employee.FirstName
 ,employee.LastName 
 ,employee.JoinDate
 ,department.DepartmentCode
 ,designation.DesignationName
 FROM [dbo].[AssociateResignation] resignation
 INNER JOIN [dbo].[Employee] employee
 ON resignation.EmployeeId = employee.EmployeeId
 INNER JOIN [dbo].[Departments] department
 ON employee.DepartmentId = department.DepartmentId
 INNER JOIN [dbo].[Designations] designation 
 ON employee.Designation = designation.DesignationID
 WHERE employee.ProgramManager = @ProgramManagerID AND employee.StatusId = @StatusID
END
