﻿--================================================  
-- Author			:  Kalyan Penumutchu      
-- Create date		:  13-04-2018      
-- Modified date	:  13-04-2018      
-- Modified By		:  Kalyan Penumutchu
-- Description		:  This SP is used Reports of HRMS. Accepts Project id and returns list of billable resource details.     
-- ================================================  
CREATE PROCEDURE [dbo].[usp_GetBillableResourcesByProjectForReport]
(	
	@projectId INT = 0 
)
AS
SELECT
	emp.employeecode AS [AssociateCode],
	[dbo].[udfGetEmployeeFullName](emp.EmployeeId) AS [AssociateName],
	allocPercentage.Percentage AS [AllocationPercentage],
	ibr.[InternalBillingRoleName] AS [InternalBillingRoleName],
	cbr.[ClientBillingRoleName] AS [ClientBillingRoleName],

	CASE alloc.isprimary
		WHEN 0 THEN 'False'
		ELSE 'True'
	END as [IsPrimaryProject],
	
	CASE alloc.iscritical
		WHEN 0 THEN 'False'
		ELSE 'True'
	END as [IsCriticalResource]

FROM associateallocation alloc 
JOIN Employee emp ON alloc.employeeid=emp.employeeid  
JOIN AllocationPercentage allocPercentage ON alloc.AllocationPercentage = allocPercentage.AllocationPercentageID                
LEFT JOIN InternalBillingRoles ibr ON ibr.[InternalBillingRoleId] = alloc.[InternalBillingRoleId]
LEFT JOIN ClientBillingRoles cbr ON cbr.[ClientBillingRoleId] = alloc.[ClientBillingRoleId]
WHERE
	alloc.projectid=@projectId AND
	alloc.releasedate is null AND
	isbillable=1 AND
	emp.isactive=1 AND
	alloc.isactive=1
ORDER BY [AssociateName]
