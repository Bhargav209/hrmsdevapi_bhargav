﻿    
CREATE PROCEDURE [dbo].[usp_rpt_FinanceReport]    
(    
 @FromDate DATETIME    
 , @ToDate DATETIME    
 , @ProjectID INT    
 --, @RowsPerPage INT    
 --, @PageNumber INT    
)    
AS    
BEGIN    
  SET NOCOUNT ON;    
    
  ;WITH FinanceReport AS    
  (SELECT     
    DENSE_RANK() OVER(PARTITION BY employee.EmployeeId, managers.ProjectId ORDER BY managers.CreatedDate DESC) AS RowNum,    
 project.ProjectId,    
 allocation.EffectiveDate,    
 allocation.ReleaseDate,    
    allocation.AssociateAllocationId    
 , employee.EmployeeCode    
 , dbo.udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName    
 , dbo.udfGetEmployeeFullName(managers.ProgramManagerID) AS ProgramManagerName    
 , dbo.udfGetEmployeeFullName(allocation.ReportingManagerId) AS ReportingManagerName    
 , dbo.udfGetEmployeeFullName(managers.LeadID) AS LeadName    
 --,dbo.udf_GetRoleName(rolemaster.RoleMasterID) AS RoleName      
 , designation.DesignationName    
 , department.[Description]    
 , grade.GradeName    
 , client.ClientName
 ,client.ClientCode  
 --,skills.SkillCode  As SkillCode       
 , dbo.udf_GetEmployeePrimarySkills(employee.EmployeeId) AS SkillCode    
 , project.ProjectName    
 , ISNULL(allocation.IsBillable,0) AS [IsBillable]    
 , ISNULL(allocation.IsCritical,0) AS [IsCritical]    
 , allocation.ClientBillingPercentage    
 , allocationpercentage.[Percentage] AS Allocationpercentage    
 , allocation.InternalBillingPercentage    
 , ISNULL(internalrole.InternalBillingRoleCode,'') AS InternalBillingRoleCode    
 , ISNULL(clientrole.ClientBillingRoleCode,'') AS ClientBillingRoleCode    
 , CASE WHEN @FromDate > allocation.EffectiveDate THEN @FromDate ELSE allocation.EffectiveDate END AS FromDate    
 , CASE WHEN @ToDate > allocation.ReleaseDate THEN allocation.ReleaseDate  ELSE @ToDate END AS ToDate    
 FROM [dbo].[AssociateAllocation] allocation    
 LEFT JOIN [dbo].[ClientBillingRoles] clientrole ON allocation.ClientBillingRoleId = clientrole.ClientBillingRoleId    
 LEFT JOIN [dbo].[InternalBillingRoles] internalrole ON allocation.InternalBillingRoleId = internalrole.InternalBillingRoleId      
 --INNER JOIN [dbo].[RoleMaster] rolemaster      
 --ON allocation.RoleMasterID = rolemaster.RoleMasterID      
 INNER JOIN [dbo].[Projects] project ON allocation.ProjectId = project.ProjectId      
 INNER JOIN ProjectManagers managers ON managers.ProjectID = project.ProjectId --AND managers.IsActive = 1    
 INNER JOIN [dbo].[Clients] client ON project.ClientId = client.ClientId      
 INNER JOIN [dbo].[Employee] employee ON allocation.EmployeeId = employee.EmployeeId     
 INNER JOIN [dbo].[EmployeeSkills] employeeSkills ON employee.EmployeeId = employeeSkills.EmployeeId     
 INNER JOIN [dbo].[Skills] skills ON employeeSkills.SkillId = skills.SkillId        
 INNER JOIN [dbo].[Grades] grade ON employee.GradeId = grade.GradeId      
 INNER JOIN [dbo].[Departments] department ON employee.DepartmentId = department.DepartmentId      
 INNER JOIN [dbo].[Designations] designation ON employee.Designation = designation.DesignationId      
 INNER JOIN [dbo].[AllocationPercentage] allocationpercentage ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID      
 WHERE (@ToDate BETWEEN allocation.EffectiveDate AND allocation.ReleaseDate OR @FromDate BETWEEN allocation.EffectiveDate AND ISNULL(allocation.ReleaseDate, @FromDate)    
 OR allocation.EffectiveDate BETWEEN @FromDate AND @ToDate OR allocation.ReleaseDate BETWEEN @FromDate AND @ToDate)    
 AND allocation.ProjectId = CASE @ProjectID WHEN 0 THEN allocation.ProjectId ELSE @ProjectID END    
 --ORDER BY employee.EmployeeCode, allocation.AssociateAllocationId    
 --OFFSET (@PageNumber - 1) ROWS    
 --FETCH NEXT @RowsPerPage ROWS ONLY      
 )    
 SELECT DISTINCT * FROM FinanceReport A    
 WHERE A.RowNum = 1    
 ORDER BY A.EmployeeCode, A.AssociateAllocationId    
END 