﻿-- ========================================================  
-- Author   : Mithun  
-- Create date  : 28-05-2019  
-- Modified date : 30-05-2019,13-06-2019  
-- Modified By  : Mithun, Sabiha  
-- Description      :   Update ADR Section      
-- ========================================================   
  
CREATE PROCEDURE [dbo].[usp_UpdateADRSection]  
(  
@ADRSectionId INT,         
@ADRSectionName VARCHAR(150),    
@ADRMeasurementAreaId INT,
@DepartmentId INT,  
@ModifiedDate DATETIME,      
@ModifiedUser VARCHAR(150),      
@SystemInfo VARCHAR(50)  
)  
AS  
BEGIN      
      
 SET NOCOUNT ON;    
  
 UPDATE [dbo].[ADRSection]   
     SET  
  ADRSectionName         = @ADRSectionName  
 ,ADRMeasurementAreaId = @ADRMeasurementAreaId 
 ,DepartmentId = @DepartmentId  
 ,ModifiedDate    = @ModifiedDate  
 ,ModifiedUser    = @ModifiedUser  
 ,SystemInfo           = @SystemInfo  
 WHERE   
 ADRSectionId = @ADRSectionId  
      
  SELECT @@ROWCOUNT   
  
END  