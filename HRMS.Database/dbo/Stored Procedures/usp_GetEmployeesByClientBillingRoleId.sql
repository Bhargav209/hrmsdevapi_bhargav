﻿CREATE PROCEDURE [dbo].[usp_GetEmployeesByClientBillingRoleId] 
(
 @ClientBillingRoleId INT,
 @ProjectId INT
)   
AS    
BEGIN    
 SET NOCOUNT ON;    
      
 SELECT  
    Employee.EmployeeId AS EmployeeId, 
    employee.FirstName + ' ' + employee.LastName AS EmployeeName  
 FROM AssociateAllocation allocation    
 INNER JOIN Employee employee ON allocation.EmployeeId = employee.EmployeeId   
 WHERE allocation.ClientBillingRoleId = @ClientBillingRoleId AND allocation.ProjectId = @ProjectId AND employee.IsActive = 1
 
 END
