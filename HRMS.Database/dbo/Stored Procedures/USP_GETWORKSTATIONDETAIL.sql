﻿-- ========================================================       
-- Author   : Prasanna      
-- Create date  : 23-11-2018      
-- Modified date : 23-11-2018      
-- Modified By  : Prasanna      
-- Description  : Gets Work Station Information.      
-- ========================================================       
      
CREATE PROCEDURE [dbo].[USP_GETWORKSTATIONDETAIL]         
@WORKSTATIONID INT,@WORKSTATIONSUFFIX VARCHAR(10)=null          
AS             
BEGIN         
 SET NOCOUNT ON;     
 DECLARE @CategoryId INT    
 DECLARE @ProjectStateId INT    
    
 SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'    
 SELECT @ProjectStateId = StatusId FROM status Where StatusCode = 'Closed' AND CategoryID = @CategoryId    
      
   SELECT      
 WS.WORKSTATIONID      
    ,EMP.FIRSTNAME+' '+EMP.LASTNAME AS ASSOCIATENAME      
 ,PROJ.PROJECTNAME AS PROJECTNAME      
    ,LEAD.FIRSTNAME+' '+LEAD.LASTNAME AS LEADNAME      
 ,WSA.EmployeeId      
  FROM [DBO].WORKSTATION WS      
  INNER JOIN [DBO].WORKSTATIONALLOCATION WSA ON WSA.WORKSTATIONID=WS.ID      
  INNER JOIN [DBO].EMPLOYEE EMP ON EMP.EMPLOYEEID=WSA.EMPLOYEEID      
  LEFT JOIN [DBO].ASSOCIATEALLOCATION ASS ON ASS.EMPLOYEEID=EMP.EMPLOYEEID      
  LEFT JOIN [DBO].PROJECTS PROJ ON ASS.PROJECTID=PROJ.PROJECTID      
  LEFT JOIN [DBO].EMPLOYEE LEAD ON EMP.REPORTINGMANAGER=LEAD.EMPLOYEEID      
      
  WHERE PROJ.ProjectStateId <> @ProjectStateId  AND ASS.ISACTIVE=1 AND WS.WORKSTATIONID=@WORKSTATIONID      
      
  AND ISNULL(WS.WORKSTATIONSUFFIX,'')= ISNULL(@WORKSTATIONSUFFIX,'')      
END      