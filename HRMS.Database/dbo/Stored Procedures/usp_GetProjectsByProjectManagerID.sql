﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	26-03-2018            
-- Modified date	:	02-04-2019            
-- Modified By		:	Sushmitha ,Mithun           
-- Description		:	Gets all the projects under a reporting manager.
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetProjectsByProjectManagerID]
(    
 @ProjectManagerID INT 
)         
AS           
BEGIN 
	SET NOCOUNT ON;  
	 DECLARE @CategoryId INT
     DECLARE @ProjectStateId INT

     SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'
     SELECT @ProjectStateId = StatusId FROM Status WHERE StatusCode= 'Closed' AND CategoryID = @CategoryId

	SELECT
		 project.ProjectId AS Id
	    ,project.ProjectName AS Name
	FROM [dbo].[Projects] project
	INNER JOIN [dbo].[ProjectManagers] projectManagers
	ON project.ProjectId = projectManagers.ProjectId
	WHERE 
		projectManagers.ProgramManagerID = @ProjectManagerID and projectManagers.IsActive=1 AND project.ProjectStateId <> @ProjectStateId
END
Go

