﻿--=======================================================  
-- Author   : Santosh  
-- Create date  : 30-01-2018  
-- Modified date : 26-03-2019 ,14-05-2019   
-- Modified By  : Santosh,Mithun  
-- Description  : Get projects   
-- =====================================  
CREATE PROCEDURE [dbo].[usp_GetProjects]                
AS                       
BEGIN                  
 SET NOCOUNT ON;               
 
 DECLARE @CategoryId INT
 DECLARE @ExecutedProjectStatusId INT  
 DECLARE @CreatedProjectStatusId INT      
 
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'  
 SELECT @CreatedProjectStatusId=Statusid FROM Status WHERE StatusCode='Created' AND CategoryID = @CategoryId 
 SELECT @ExecutedProjectStatusId=Statusid FROM Status WHERE StatusCode='Execution' AND CategoryID = @CategoryId             
                    
  SELECT                
    project.ProjectID                
   ,project.ProjectCode                
   ,project.ProjectName            
   ,project.ActualStartDate             
   ,project.ActualEndDate             
   ,client.ClientName             
 ,projecttype.[ProjectTypeId]             
 ,projecttype.[Description]             
 ,practicearea.[PracticeAreaId]             
 ,practicearea.[PracticeAreaCode]             
 ,project.[ModifiedDate]             
 ,department.[DepartmentCode]             
 ,employee.FirstName + ' ' + employee.LastName                   
  FROM Projects project               
  LEFT OUTER JOIN ProjectManagers AS projectmanager ON projectmanager.IsActive = 1 AND project.ProjectId = projectmanager.ProjectID            
  INNER JOIN Clients AS client ON client.ClientId = project.ClientId            
  INNER JOIN ProjectType AS projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId            
  INNER JOIN PracticeArea AS practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId            
  LEFT OUTER JOIN Employee AS employee ON projectmanager.ProgramManagerID = employee.EmployeeId            
  LEFT OUTER JOIN Departments AS department ON department.DepartmentId = project.DepartmentId            
  WHERE   project.ProjectStateId = @ExecutedProjectStatusId OR project.ProjectStateId = @CreatedProjectStatusId 
  ORDER BY  project.ProjectName                  
END 