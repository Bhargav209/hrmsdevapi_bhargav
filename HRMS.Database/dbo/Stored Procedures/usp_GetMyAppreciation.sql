﻿-- ===========================================           
-- Author   : Santosh          
-- Create date  : 30-01-2018          
-- Modified date : 30-01-2018              
-- Modified By  : Santosh          
-- Description  : Get My Appreciation(s)      
-- ===========================================        
CREATE PROCEDURE [dbo].[usp_GetMyAppreciation]
(
 @EmployeeID INT    
)     
AS       
BEGIN   
 SET NOCOUNT ON;
  SELECT  
    appreciation.ID
   ,[dbo].[udfGetEmployeeFullName](appreciation.FromEmployeeID) AS FromEmployeeName
   ,appType.[Type] AS AppreciationType
   ,AppreciationMessage
   ,AppreciationDate
   ,adrCycle.ADRCycle
   ,CONCAT(financialYear.FromYear, ' - ', financialYear.ToYear) AS FinancialYear
  FROM [dbo].[AssociateAppreciation] appreciation
  INNER JOIN [dbo].[AppreciationType] appType
  ON appreciation.AppreciationTypeID = appType.ID
  INNER JOIN [dbo].[ADRCycle] adrCycle
  ON appreciation.ADRCycleID = adrCycle.ADRCycleID
  INNER JOIN [dbo].[FinancialYear] financialYear
  ON appreciation.FinancialYearID = financialYear.ID
  WHERE ToEmployeeID = @EmployeeID AND appreciation.IsActive = 1
END

