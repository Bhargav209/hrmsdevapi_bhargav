﻿
-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	27-11-2017
-- Modified date	:	27-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Gets professional Details By EmployeeID
-- ======================================================= 

CREATE PROCEDURE [dbo].[usp_GetProfessionalDetailsByEmployeeID]
@EmployeeID INT
AS
BEGIN
 SET NOCOUNT ON;

SELECT
  certification.ID
 ,certification.CertificationID
 ,'' AS ProgramTitle
 ,certification.ValidFrom
 ,certification.Institution
 ,certification.Specialization
 ,certification.ValidUpto
 ,certification.SkillGroupID
 ,skillGroup.SkillGroupName
 ,skill.SkillName
 ,3 AS ProgramType
 FROM AssociatesCertifications certification
 inner join SkillGroup skillGroup
 on certification.SkillGroupID = skillGroup.SkillGroupId
 inner join Skills skill
 on certification.CertificationID = skill.SkillId
 WHERE certification.EmployeeID = @EmployeeID

 
 UNION
 
 SELECT
  membership.ID
  ,0 AS CertificationID
 ,membership.ProgramTitle
 ,membership.ValidFrom
 ,membership.Institution
 ,membership.Specialization
 ,membership.ValidUpto
 ,0 AS SkillGroupID
 ,'' AS SkillGroupName
 ,'' AS SkillName
 ,4 AS ProgramType
 FROM [dbo].[AssociatesMemberShips] membership
 WHERE membership.EmployeeID = @EmployeeID
END
