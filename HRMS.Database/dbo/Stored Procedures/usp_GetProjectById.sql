﻿--sp_helptext usp_GetProjectDetail

CREATE PROCEDURE [dbo].[usp_GetProjectById]        
 (        
  @ProjectId INT        
 )                     
AS                        
BEGIN                    
                   
 SET NOCOUNT ON;     
 DECLARE @StatusId INT  
 DECLARE @CategoryId INT  
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'  
 SELECT @StatusId=Statusid FROM Status WHERE StatusCode='Closed' AND CategoryID = @CategoryId       
         
  SELECT DISTINCT        
   project.ProjectId AS ProjectId        
  ,project.ProjectCode AS ProjectCode        
  ,project.ProjectName AS ProjectName       
  ,project.ActualStartDate AS ActualStartDate        
  ,project.ActualEndDate AS ActualEndDate    
  ,project.ClientId AS ClientId     
  ,client.ClientName AS ClientName                                    
  ,projectmanager.ProgramManagerID AS ManagerId        
  ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName           
  ,practicearea.PracticeAreaId AS PracticeAreaId      
  ,practicearea.PracticeAreaCode AS PracticeAreaCode      
  ,projecttype.ProjectTypeId AS ProjectTypeId       
  ,projecttype.Description AS ProjectTypeDescription                                            
  ,dept.DepartmentId AS DepartmentId      
  ,dept.DepartmentCode AS DepartmentCode     
  ,domain.DomainId AS DomainId    
  ,domain.DomainName AS DomainName    
  ,state.StatusId AS ProjectStateId    
  ,state.StatusCode AS ProjectState      
  --,(SELECT TOP 1 s.StatusCode FROM ProjectWorkFlow w INNER JOIN status s ON s.StatusId = w.WorkFlowStatus WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc ) AS WorkFlowStatus         
 FROM Projects project        
 INNER JOIN ProjectManagers projectmanager ON project.ProjectId = projectmanager.ProjectId     
 INNER JOIN  Status state ON project.ProjectStateId = state.StatusId    
 LEFT JOIN Domain domain ON project.DomainId = domain.DomainId       
 INNER JOIN Clients client ON project.ClientId = client.ClientId      
 INNER JOIN ProjectType projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId      
 INNER JOIN PracticeArea practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId      
 INNER JOIN Departments dept ON project.DepartmentId = dept.DepartmentId      
 INNER JOIN Employee programmanager ON projectmanager.ProgramManagerID = programmanager.EmployeeId        
 WHERE project.ProjectStateId <> @StatusId AND  project.ProjectId=@ProjectId AND projectmanager.IsActive=1        
END 


