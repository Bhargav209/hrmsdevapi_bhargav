﻿CREATE PROCEDURE [dbo].[usp_CreateClientBillingRole]           
(        
 @ClientBillingRoleName VARCHAR(70)        
 ,@ProjectId INT        
 ,@IsActive BIT = 1        
 ,@NoOfPositions INT        
 ,@CreatedDate   DATETIME        
 ,@CreatedUser   VARCHAR(100)        
 ,@SystemInfo   VARCHAR(50)           
 ,@StartDate DATE NULL        
 ,@ClientBillingPercentage INT NULL        
)        
AS        
BEGIN        
 SET NOCOUNT ON;         
         
   IF EXISTS(SELECT 1 FROM ClientBillingRoles WHERE (ClientBillingRoleName=@ClientBillingRoleName AND NoOfPositions=@NoOfPositions AND ClientBillingPercentage=@ClientBillingPercentage AND StartDate = @StartDate AND ProjectId = @ProjectId))      
      SELECT -1      
   ELSE      
     BEGIN      
   INSERT INTO [dbo].[ClientBillingRoles]        
           ([ClientBillingRoleName]        
           ,[ProjectId]            
           ,[IsActive]        
           ,[NoOfPositions]        
           ,[StartDate]        
           ,[CreatedUser]                   
           ,[CreatedDate]                   
           ,[SystemInfo]        
           ,[ClientBillingPercentage])        
       VALUES        
           (@ClientBillingRoleName        
           ,@ProjectId        
           ,@IsActive        
           ,@NoOfPositions        
           ,@StartDate        
           ,@CreatedUser                   
           ,@CreatedDate                  
           ,@SystemInfo        
           ,@ClientBillingPercentage)          
         
       SELECT @@ROWCOUNT        
  END       
END 