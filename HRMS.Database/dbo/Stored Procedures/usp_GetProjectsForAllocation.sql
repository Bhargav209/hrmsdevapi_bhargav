﻿-- ========================================================        
-- Author   : Mithun        
-- Create date  : 05-10-2019        
-- Modified date : 04-30-2019        
-- Modified By  : Mithun        
-- Description  : Get Projects For Allocation        
-- [dbo].[usp_GetProjectsForAllocation]        
-- ========================================================      
CREATE PROCEDURE [dbo].[usp_GetProjectsForAllocation]              
AS                     
BEGIN                
 SET NOCOUNT ON;             
              
  DECLARE @CategoryId INT
 DECLARE @ExecutedProjectStatusId INT  
 DECLARE @CreatedProjectStatusId INT      
 
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'  
 SELECT @CreatedProjectStatusId=Statusid FROM Status WHERE StatusCode='Created' AND CategoryID = @CategoryId 
 SELECT @ExecutedProjectStatusId=Statusid FROM Status WHERE StatusCode='Execution' AND CategoryID = @CategoryId            
                  
  SELECT              
    project.ProjectID              
   ,project.ProjectCode              
   ,project.ProjectName 
   ,client.ClientName 
          
  FROM Projects project    
  INNER JOIN Clients AS client ON client.ClientId = project.ClientId          
  WHERE   project.ProjectStateId = @ExecutedProjectStatusId OR project.ProjectStateId = @CreatedProjectStatusId 
  ORDER BY  project.ProjectName     
END 


