﻿CREATE PROCEDURE [dbo].[usp_GetProjectDetailById]        
 (        
  @ProjectId INT        
 )                     
AS                        
BEGIN                    
                   
 SET NOCOUNT ON;     
         
  SELECT DISTINCT        
   project.ProjectId AS ProjectId        
  ,project.ProjectCode AS ProjectCode        
  ,project.ProjectName AS ProjectName     
 FROM Projects project        
 WHERE  project.ProjectId = @ProjectId    
END 
