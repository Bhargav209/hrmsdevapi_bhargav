﻿
-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	22-11-2017
-- Modified date	:	22-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Gets Associates Rejected Resignations List 
-- ======================================================= 

CREATE PROCEDURE [dbo].[usp_GetRejectedResignationsList]
@EmployeeID INT
AS
BEGIN
 SET NOCOUNT ON;

 SELECT
  resignation.ResignationID
 ,resignation.DateOfResignation AS ResignationDate
 ,resignation.EmployeeID
 ,employee.EmployeeCode
 ,employee.FirstName + ' ' + employee.LastName AS EmployeeName
 ,designation.DesignationName AS Designation
 FROM [dbo].[AssociateResignation] resignation
 INNER JOIN [dbo].[Employee] employee
 ON resignation.EmployeeId = employee.EmployeeId
 INNER JOIN [dbo].[Designations] designation 
 ON employee.Designation = designation.DesignationID
 INNER JOIN [dbo].[AssociateResignationWorkFlow] EmployeeWorkFlow
 ON resignation.ResignationID = EmployeeWorkFlow.ResignationID
 WHERE EmployeeWorkFlow.FromEmployeeID = @EmployeeID AND EmployeeWorkFlow.StatusId = 11 -- Rejected
END
