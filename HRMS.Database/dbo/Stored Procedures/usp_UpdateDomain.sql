﻿--sp_helptext usp_UpdateDomain

-- ======================================  
-- Author   : Ramya  
-- Create date  : 05-03-2018  
-- Modified date :   
-- Modified By  :   
-- Description  : Update a Domain  
-- usp_UpdateDomain 1,'DataTest02','05-02-2018','SA','192.168.2.1'  
-- ======================================  
  
CREATE PROCEDURE [dbo].[usp_UpdateDomain]   
(  
@DomainID INT,         
@DomainName VARCHAR(50),     
@DateModified DATETIME,      
@ModifiedUser VARCHAR(150),      
@SystemInfo VARCHAR(50)  
)  
AS  
BEGIN      
      
 SET NOCOUNT ON;    
   
 UPDATE   
 [dbo].[Domain]  
 SET  
 ModifiedDate = GETDATE()  
 WHERE   
 DomainName = @DomainName AND DomainID <> @DomainID  
  
  
 IF (@@ROWCOUNT = 0)  
   
 BEGIN  
  
 UPDATE   
 [dbo].[Domain]   
 SET  
 DomainName         = @DomainName  
 ,ModifiedDate    = @DateModified  
 ,ModifiedUser    = @ModifiedUser  
 ,SystemInfo           = @SystemInfo  
 WHERE   
 DomainID = @DomainID  
      
  SELECT @@ROWCOUNT   
  
END  
ELSE  
SELECT -1 
END

