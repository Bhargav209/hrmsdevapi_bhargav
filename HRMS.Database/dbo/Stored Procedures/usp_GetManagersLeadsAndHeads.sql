﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	19-03-2018            
-- Modified date	:	20-03-2019            
-- Modified By		:	Sushmitha,Mithun            
-- Description		:	Gets reporting managers, program managers and competency leads list
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetManagersLeadsAndHeads]  
AS  
BEGIN  
 SET NOCOUNT ON;  

 DECLARE @StatusId INT
 DECLARE @CategoryId INT
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'
 SELECT @StatusId=Statusid FROM Status WHERE StatusCode='Closed' AND CategoryID = @CategoryId
      
 --To get program Managers  
 SELECT DISTINCT  
   projectManagers.ProgramManagerID AS Id  
  ,[dbo].[udfGetEmployeeFullName](projectManagers.ProgramManagerID) AS Name  
 FROM  
  [dbo].[Projects] projects  
  INNER JOIN [dbo].[ProjectManagers] projectManagers  
  ON projects.ProjectId = projectManagers.ProjectID  
 WHERE   
  projectManagers.IsActive = 1 AND projects.ProjectStateId <> @StatusId AND projectManagers.ProgramManagerID IS NOT NULL  
  
 UNION   
  
 --To get reporting managers  
 SELECT DISTINCT  
   projectManagers.ReportingManagerID AS Id  
  ,[dbo].[udfGetEmployeeFullName](projectManagers.ReportingManagerID) AS Name  
 FROM  
  [dbo].[Projects] projects  
  INNER JOIN [dbo].[ProjectManagers] projectManagers  
  ON projects.ProjectId = projectManagers.ProjectID  
 WHERE   
  projectManagers.IsActive = 1 AND projects.ProjectStateId <> @StatusId AND projectManagers.ReportingManagerID IS NOT NULL  
  
 UNION   
  
 --To get competency leads  
 SELECT DISTINCT  
   employee.EmployeeId AS Id  
  ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS Name  
 FROM   
  [dbo].[Employee] employee  
  INNER JOIN [dbo].[UserRoles] userroles  
  ON employee.UserId = userroles.UserId  
 WHERE   
  employee.IsActive = 1 AND userroles.IsActive = 1 AND userroles.RoleId = (SELECT RoleId FROM Roles WHERE RoleName = 'Competency Leader' AND IsActive=1)  
  
 UNION  
  
 --To get department heads  
 SELECT   
   employee.EmployeeId  
  ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS Name  
 FROM  
  [dbo].[Departments] department  
  INNER JOIN [dbo].[Employee] employee  
  ON department.DepartmentHeadID = employee.EmployeeId  
 WHERE department.IsActive = 1 AND employee.IsActive = 1 AND department.DepartmentHeadID IS NOT NULL  
   
 UNION  
  
 --To get leads  
 SELECT DISTINCT  
   projectManagers.LeadID AS Id  
  ,[dbo].[udfGetEmployeeFullName](projectManagers.LeadID) AS Name  
 FROM  
  [dbo].[Projects] projects  
  INNER JOIN [dbo].[ProjectManagers] projectManagers  
  ON projects.ProjectId = projectManagers.ProjectID  
 WHERE   
  projectManagers.IsActive = 1 AND projects.ProjectStateId <> @StatusId AND projectManagers.LeadID IS NOT NULL  
  
END  
  