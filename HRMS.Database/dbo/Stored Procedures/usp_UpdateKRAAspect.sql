﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	05-02-2018
-- Modified date	:	05-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Updates KRA Aspects.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_UpdateKRAAspect]
(
@KRAAspectID INT,       
@AspectId VARCHAR(50),
@DepartmentId INT,   
@ModifiedDate DATETIME,    
@ModifiedUser VARCHAR(150),    
@SystemInfo VARCHAR(50)
)
AS
BEGIN    
    
 SET NOCOUNT ON;  
 
 UPDATE 
	[dbo].[KRAAspectMaster]
 SET
	DateModified = GETDATE()
 WHERE 
	AspectId = @AspectId AND KRAAspectID <> @KRAAspectID AND DepartmentId = @DepartmentId


 IF (@@ROWCOUNT = 0)
 
 BEGIN

 UPDATE 
	[dbo].[KRAAspectMaster] 
 SET
	 AspectId         = @AspectId
	,DateModified		  = @ModifiedDate
	,ModifiedUser		  = @ModifiedUser
	,SystemInfo           = @SystemInfo
 WHERE 
	KRAAspectID = @KRAAspectID
    
  SELECT @@ROWCOUNT 

END
ELSE
SELECT -1 --duplicate
END
Go
