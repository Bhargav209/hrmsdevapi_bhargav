﻿-- ======================================
-- Author			:	Ramya
-- Create date		:	05-03-2018
-- Modified date	:	
-- Modified By		:	
-- Description		:	Create new Domain
-- ======================================

CREATE PROCEDURE [dbo].[usp_CreateDomain]
(  
@DomainName VARCHAR(50),  
@CreatedDate DATETIME,  
@CreatedUser VARCHAR(150),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON;  

 UPDATE 
	[dbo].[Domain]
 SET
	ModifiedDate = GETDATE()
 WHERE 
	DomainName = @DomainName

 IF (@@ROWCOUNT = 0)
 BEGIN
 
 INSERT INTO 
	[dbo].[Domain]  
	(DomainName, CreatedDate, CreatedUser, SystemInfo)  
 VALUES  
	(@DomainName, @CreatedDate, @CreatedUser, @SystemInfo)  
  
 SELECT @@ROWCOUNT
    
END
ELSE

SELECT -1

END

