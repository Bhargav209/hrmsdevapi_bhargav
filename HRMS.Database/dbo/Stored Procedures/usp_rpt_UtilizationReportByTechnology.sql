﻿-- ==========================================                     
-- Author   : Santosh                
-- Create date  : 17-04-2018                
-- Modified date : 17-04-2018 ,08-05-2019                   
-- Modified By  : Santosh ,Mithun             
-- Description  : Get Utilization Report              
--[dbo].[usp_rpt_UtilizationReport] 0, 53, 0, 0, 0, 0, 0, 0, -1, -1, 50, 1        
-- ==========================================               
CREATE PROCEDURE [dbo].[usp_rpt_UtilizationReportByTechnology]    
(                 
   @isExportToExcel BIT   
   ,@practiceAreaID  INT          
)               
AS                     
BEGIN              
 SET NOCOUNT ON;             
  IF (@isExportToExcel = 0 )    
  BEGIN            
   SELECT      Distinct       
    employee.EmployeeCode                     
   ,dbo.udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName     
                    
   ,dbo.udf_GetEmployeeProgramManager(employee.EmployeeId) AS ProgramManagerName    
     
     
                
   ,designation.DesignationName                        
   ,department.[Description]  AS [DepartmentName]                      
      
   ,ISNULL(employee.TotalExperience,0) AS [Experience]                        
   ,employee.JoinDate                        
   ,grade.GradeName                        
     
   ,PA.PracticeAreaCode  AS Technology                  
     
   ,dbo.udf_GetEmployeeProjects(employee.EmployeeId) AS ProjectName                            
    
  -- ,dbo.udf_GetEmployeeAllocationPercentage(employee.EmployeeId) AS Allocationpercentage                            
   --, CAST(dbo.udf_GetEmployeeAllocationPercentage(employee.EmployeeId) AS DECIMAL(18, 0)) as Allocationpercentage    
    
   ,dbo.udf_GetEmployeePrimarySkills(employee.EmployeeId) AS SkillCode    
   ,employee.AadharNumber  
   ,employeeType.EmployeeType          
   FROM [dbo].[AssociateAllocation] allocation                        
                       
   INNER JOIN [dbo].[Employee] employee                        
   ON allocation.EmployeeId = employee.EmployeeId     
   LEFT JOIN [dbo].[EmployeeType] employeeType   
   on  employee.EmployeeTypeId=employeeType.EmployeeTypeId  
                        
   INNER JOIN [dbo].[Grades] grade                        
   ON employee.GradeId = grade.GradeId                        
   INNER JOIN [dbo].[Departments] department                        
   ON employee.DepartmentId = department.DepartmentId                        
   INNER JOIN [dbo].[Designations] designation                        
   ON employee.Designation = designation.DesignationId                        
   INNER JOIN [dbo].[AllocationPercentage] allocationpercentage                        
   ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID                 
   --INNER JOIN ProjectManagers PM ON project.ProjectId = PM.ProjectID AND  PM.IsActive=1       
   LEFT JOIN PracticeArea PA ON employee.CompetencyGroup=PA.PracticeAreaId   
   WHERE  
    ISNULL(PA.PracticeAreaId, 0) = CASE  @practiceAreaID WHEN 0 THEN ISNULL(PA.PracticeAreaId, 0) ELSE @practiceAreaID END           
              
   AND allocation.IsActive = 1      
        
   ORDER BY  employee.EmployeeCode                         
        
 END    
 ELSE    
  BEGIN    
   SELECT   Distinct               
     employee.EmployeeCode                        
    ,dbo.udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName     
                    
   ,dbo.udf_GetEmployeeProgramManager(employee.EmployeeId) AS ProgramManagerName                      
                         
    ,designation.DesignationName                        
    ,department.[Description]  AS [DepartmentName]                      
                           
    ,ISNULL(employee.TotalExperience,0) AS [Experience]      
    ,employee.JoinDate                        
    ,grade.GradeName                        
    
 ,PA.PracticeAreaCode  AS Technology                         
   ,dbo.udf_GetEmployeeProjects(employee.EmployeeId) AS ProjectName   
                        
   --,dbo.udf_GetEmployeeAllocationPercentage(employee.EmployeeId) AS Allocationpercentage                        
     
 ,dbo.udf_GetEmployeePrimarySkills(employee.EmployeeId) AS SkillCode   
   ,employee.AadharNumber  
   ,employeeType.EmployeeType  
    FROM [dbo].[AssociateAllocation] allocation                        
                       
    INNER JOIN [dbo].[Employee] employee                        
    ON allocation.EmployeeId = employee.EmployeeId    
 LEFT JOIN [dbo].[EmployeeType] employeeType   
    on  employee.EmployeeTypeId=employeeType.EmployeeTypeId   
     
    INNER JOIN [dbo].[Grades] grade                        
    ON employee.GradeId = grade.GradeId                        
    INNER JOIN [dbo].[Departments] department                        
    ON employee.DepartmentId = department.DepartmentId                        
    INNER JOIN [dbo].[Designations] designation                        
    ON employee.Designation = designation.DesignationId                        
    INNER JOIN [dbo].[AllocationPercentage] allocationpercentage                        
    ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID                 
   
 LEFT JOIN PracticeArea PA ON employee.CompetencyGroup=PA.PracticeAreaId    
    WHERE   
   
  ISNULL(PA.PracticeAreaId, 0) = CASE  @practiceAreaID WHEN 0 THEN ISNULL(PA.PracticeAreaId, 0) ELSE @practiceAreaID END           
              
    AND allocation.IsActive = 1       
       
    ORDER BY  employee.EmployeeCode      
  END                      
END  