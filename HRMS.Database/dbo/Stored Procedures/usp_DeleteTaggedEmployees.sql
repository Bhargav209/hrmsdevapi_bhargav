﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	24-01-2018
-- Modified date	:	24-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Delete tagged employees.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_DeleteTaggedEmployees]
@Id INT
AS
BEGIN

 SET NOCOUNT ON;

 DELETE FROM 
	[dbo].[TalentRequisitionEmployeeTag] 
 WHERE 
	ID = @Id

 SELECT @@ROWCOUNT

END

