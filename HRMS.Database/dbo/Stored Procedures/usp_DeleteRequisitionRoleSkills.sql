﻿-- =====================================================
-- Author			:	Santosh          
-- Create date		:	13-12-2017            
-- Modified date	:	13-12-2017            
-- Modified By		:	Santosh            
-- Description		:	Deletes requisition role skills
-- ====================================================

CREATE PROCEDURE [dbo].[usp_DeleteRequisitionRoleSkills]            
 @TalentRequisitionID INT,
 @RequisitionRoleDetailID INT,
 @SkillGroupID INT
AS            
BEGIN
 SET NOCOUNT ON;         
	DECLARE @ID INT
	
	SELECT @ID = ID FROM [dbo].[RequisitionRoleSkillMapping] 
	WHERE TRID = @TalentRequisitionID AND RequistiionRoleDetailID = @RequisitionRoleDetailID

	DELETE FROM [dbo].[RequisitionRoleSkills] 
	WHERE RoleSkillMappingID = @ID AND SkillGroupID = @SkillGroupID

	SELECT @@ROWCOUNT
END
