﻿---- ========================================================
---- Author			:	Sushmitha
---- Create date		:	09-01-2018
---- Modified date	:	09-01-2018
---- Modified By		:	Sushmitha
---- Description		:	Approve requisition by delivery head.
---- ======================================================== 

CREATE PROCEDURE [dbo].[usp_ApproveRequisition]
@TalentrequisitionId INT,
@FromEmployeeID INT,
@ApprovedByID VARCHAR(50),
@StatusId INT,
@Comments VARCHAR(1500),
@Modifieduser VARCHAR(100),
@Modifieddate DATETIME,
@Createduser VARCHAR(100),
@Createddate DATETIME,
@Systeminfo VARCHAR(50)
AS      
BEGIN  
 SET NOCOUNT ON;

 UPDATE 
	[dbo].[TalentRequisition]
 SET 
	Statusid = @StatusId, 
	Modifieduser = @Modifieduser,
	Modifieddate = @Modifieddate,
	Systeminfo = @Systeminfo 
 WHERE 
	Trid=@TalentrequisitionId

 INSERT INTO [dbo].[TalentRequisitionWorkFlow]	            
 (      
	TalentRequisitionID
	,Comments 
	,StatusID
	,FromEmployeeID
	,ToEmployeeID    
	,CreatedBy       
	,CreatedDate 
 )            
 VALUES            
 (      
	@TalentrequisitionId 
	,@Comments     
	,@StatusId     
	,@FromEmployeeID      
	,@ApprovedByID     
	,@Createduser     
	,@Createddate    
 )        
  SELECT @@ROWCOUNT

END

