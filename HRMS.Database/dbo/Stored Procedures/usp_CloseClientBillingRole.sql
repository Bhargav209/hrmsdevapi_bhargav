﻿-- ========================================================      
-- Author   : Mithun      
-- Create date  : 04-30-2019      
-- Modified date : 04-30-2019      
-- Modified By  : Mithun      
-- Description  : Close Client Billing Role by client billing role id      
-- [dbo].[usp_CloseClientBillingRole]      
-- ========================================================       
CREATE PROCEDURE [dbo].[usp_CloseClientBillingRole]        
(      
  @ClientBillingRoleId INT  ,    
  @EndDate DATE,    
  @ModifiedUser VARCHAR(100),    
  @ModifiedDate DATE,    
  @SystemInfo VARCHAR(20)      
)            
AS            
BEGIN            
 SET NOCOUNT ON;    
             
 DECLARE @AllocationCount INT       
    
 --Get Allocation Count    
 SELECT @AllocationCount = COUNT(*) FROM AssociateAllocation WHERE ClientBillingRoleId = @ClientBillingRoleId AND ReleaseDate IS NULL    
 IF(@AllocationCount > 0)     
    SELECT -1    
 ELSE     
    BEGIN    
   UPDATE ClientBillingRoles    
   SET EndDate = @EndDate,
   IsActive = 0,    
       ModifiedDate = @ModifiedDate,    
    ModifiedUser = @ModifiedUser,    
    SystemInfo = @SystemInfo    
   WHERE ClientBillingRoleId = @ClientBillingRoleId           
 END     
 SELECT @@ROWCOUNT    
END 
  
