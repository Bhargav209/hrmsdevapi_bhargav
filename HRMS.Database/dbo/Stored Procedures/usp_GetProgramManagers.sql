﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	10-04-2018            
-- Modified date	:	10-04-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets program managers list
-- ============================================
CREATE PROCEDURE [dbo].[usp_GetProgramManagers]
AS
BEGIN
 SET NOCOUNT ON;

	--To get program Managers
	SELECT DISTINCT
		 employee.EmployeeId AS Id
		,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS Name
	FROM
		[dbo].[ProjectManagers] projectManagers
		INNER JOIN [dbo].[Employee] employee
		ON projectManagers.ProgramManagerID = employee.EmployeeId
	WHERE 
		projectManagers.IsActive = 1 AND employee.IsActive = 1 AND projectManagers.ProgramManagerID IS NOT NULL
		ORDER BY Name
END

