﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-02-2018
-- Modified date	:	08-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets all the comments of KRA Metrics by employeeId, Financial year and ADRCycleID.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetCommentsByEmployeeId] --214, 3, 2
(
	@EmployeeID INT,
	@ADRCycleID INT,
	@FinancialYearID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	/*
	SELECT
		review.CriticalTasksPerformed
	FROM [ADRAssociateKRAReview] review
	INNER JOIN [KRASet] kraSet
	ON review.KRASetID = kraSet.KRASetID
	INNER JOIN [KRAAspectMaster] aspect
	ON kraSet.KRAAspectID = aspect.KRAAspectID
	WHERE review.EmployeeID = @EmployeeID
	AND review.ADRCycleID = @ADRCycleID
	AND review.FinancialYearID = @FinancialYearID
	*/
END
