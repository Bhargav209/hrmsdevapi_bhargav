﻿-- ========================================================    
-- Author   : Chandra    
-- Create date  : 05-15-2019    
-- Modified date :     
-- Modified By  :   
-- Description  : Gets BayStation Information.    
-- ========================================================     
    
CREATE PROCEDURE [dbo].[usp_GetBays]    
AS    
BEGIN    
    
 SET NOCOUNT ON;     
 SELECT   [BayId],[Name],[Description] from BayInformation    
END 