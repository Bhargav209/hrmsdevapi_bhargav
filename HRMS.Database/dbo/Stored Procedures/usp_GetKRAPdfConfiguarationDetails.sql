﻿-- ==================================================  
-- Author                  :  Ramya.Singamsetti
-- Create date             :  28-03-2018     
-- Modified date     :      
-- Modified By             :      
-- Description             :  Method to get KRA pdf Configuration details  
-- ====================================================
CREATE PROCEDURE [dbo].[usp_GetKRAPdfConfiguarationDetails]   
AS
BEGIN

       SELECT KRApdfconfigurationID,
                 Section1,
                 Section2
       FROM [dbo].[KRApdfconfiguration]

END

