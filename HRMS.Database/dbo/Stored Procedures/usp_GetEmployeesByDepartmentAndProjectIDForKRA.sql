﻿-- ============================================           
-- Author   : Chandra                  
-- Create date  : 22-04-2018                  
-- Modified date : 22-04-2018                  
-- Modified By  : Chandra                  
-- Description  : Gets Employee details by project id        
-- ============================================      
      
CREATE PROCEDURE [dbo].[usp_GetEmployeesByDepartmentAndProjectIDForKRA]      
(      
  @ProjectID INT      
 ,@DepartmentID INT      
 ,@IsNew BIT      
)      
AS             
BEGIN        
 SET NOCOUNT ON;          
      
  IF (@IsNew = 1)      
  BEGIN      
   IF (@ProjectID > 0)      
   BEGIN      
    SELECT      
     allocation.EmployeeId AS Id      
     ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName      
    FROM       
     [dbo].[AssociateAllocation] allocation      
     INNER JOIN [dbo].[Employee] employee      
     ON allocation.EmployeeId = employee.EmployeeId  
  left Outer JOIN [dbo].[AssociateKRAMapper] AssociateKRAMapper      
     ON AssociateKRAMapper.EmployeeID = employee.EmployeeId      
    WHERE       
     ProjectID = @ProjectID AND DepartmentId = @DepartmentID AND allocation.IsActive = 1 AND AssociateKRAMapper.KRAGroupID IS NULL      
   END      
   ELSE      
   BEGIN      
    SELECT      
     employee.EmployeeId AS Id      
     ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName      
    FROM       
     [dbo].[Employee]  as employee  
  left Outer JOIN [dbo].[AssociateKRAMapper] AssociateKRAMapper      
     ON AssociateKRAMapper.EmployeeID = employee.EmployeeId    
    WHERE       
     DepartmentId = @DepartmentID AND employee.IsActive = 1 AND employee.EmployeeId NOT IN (SELECT EmployeeId FROM [dbo].[ServiceDepartmentRoles] WHERE DepartmentId = @DepartmentID AND IsActive = 1)  AND AssociateKRAMapper.KRAGroupID IS NULL    
   END      
  END      
  ELSE       
  BEGIN      
   IF (@ProjectID > 0)      
   BEGIN      
    SELECT      
     allocation.EmployeeId AS Id      
     ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName      
     ,(SELECT KRATitle FROM KRAGROUP WHERE KRAGroupID=AssociateKRAMapper.KRAGroupID) AS RoleName      
    FROM       
     [dbo].[AssociateAllocation] allocation      
     INNER JOIN [dbo].[Employee] employee      
     ON allocation.EmployeeId = employee.EmployeeId      
     INNER JOIN [dbo].[AssociateKRAMapper] AssociateKRAMapper      
     ON AssociateKRAMapper.EmployeeID = employee.EmployeeId      
    WHERE       
     allocation.ProjectID = @ProjectID AND employee.DepartmentId = @DepartmentID AND allocation.IsActive = 1 AND AssociateKRAMapper.KRAGroupID IS NOT NULL      
   END      
   ELSE      
   BEGIN      
    SELECT       
     employee.EmployeeId AS Id      
     ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName      
     ,(SELECT KRATitle FROM KRAGROUP WHERE KRAGroupID=AssociateKRAMapper.KRAGroupID) AS RoleName       
     FROM       
     [dbo].[Employee] employee      
     INNER JOIN [dbo].[ServiceDepartmentRoles] serviceDept      
     ON employee.EmployeeId = serviceDept.EmployeeID      
     INNER JOIN [dbo].[AssociateKRAMapper] AssociateKRAMapper      
     ON AssociateKRAMapper.EmployeeID = employee.EmployeeId    
                 WHERE       
     serviceDept.DepartmentID =@DepartmentID AND serviceDept.IsActive = 1 AND employee.IsActive = 1 AND AssociateKRAMapper.KRAGroupID IS NOT NULL      
      
   END      
  END      
END 