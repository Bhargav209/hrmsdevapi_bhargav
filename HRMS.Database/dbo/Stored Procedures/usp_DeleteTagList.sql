﻿-- ============================================        
-- Author   : Sabiha                
-- Create date  : 10-05-2019        
-- Modified date : 13-05-2019          
-- Description  : Delete Tag list        
-- ============================================          
CREATE PROCEDURE [dbo].[usp_DeleteTagList]        
@TagAssociateListName varchar(50),  
@ManagerId int    
      
AS        
BEGIN        
 SET NOCOUNT ON;          
 Delete from TagAssociate where TagAssociateListName=@TagAssociateListName AND ManagerId = @ManagerId;      
      
 SELECT @@ROWCOUNT      
END    