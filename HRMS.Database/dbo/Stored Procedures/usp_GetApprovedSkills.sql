﻿
CREATE PROCEDURE [dbo].[usp_GetApprovedSkills]            
(            
@EmployeeID INT            
)            
AS            
BEGIN            
            
SET NOCOUNT ON;             
        
SELECT  DISTINCT            
  es.ID,              
  es.EmployeeId,        
 (e.FirstName + ' ' + e.lastname  )as EmployeeName,      
  es.SkillId,            
  es.SkillGroupId,       
  sg.SkillGroupName,             
  sk.SkillName ,              
  es.Experience,              
  es.IsPrimary AS isPrimary,              
  es.LastUsed,                
  es.CompetencyAreaId,         
  ca.CompetencyAreaCode,          
  es.proficiencyLevelId,      
  pl.ProficiencyLevelCode,        
  sk.SkillCode            
            
FROM  EmployeeSkills es       
 Inner join Employee e on e.EmployeeId=es.EmployeeId       
 INNER JOIN skills sk on es.skillId=sk.skillId         
 Inner join ProficiencyLevel pl on es.ProficiencyLevelId=pl.ProficiencyLevelId        
 inner join CompetencyArea ca on es.CompetencyAreaId=ca.CompetencyAreaId        
 inner join SkillGroup sg on es.SkillGroupId=sg.SkillGroupId      
WHERE             
 es.EmployeeId=@EmployeeID        
END
