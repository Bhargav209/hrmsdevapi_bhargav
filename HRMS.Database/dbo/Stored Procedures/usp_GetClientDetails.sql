﻿-- ========================================================  
-- Author   : Basha  
-- Create date  : 24-12-2018  
-- Modified date : 24-04-2019  
-- Modified By  : Sabiha  
-- Description  : Get Client Details  
-- ========================================================   
CREATE PROCEDURE [dbo].[usp_GetClientDetails]  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SELECT ClientId,   
  ClientCode,  
  ClientName,  
  ClientRegisterName,   
  IsActive  
  FROM  
  Clients  
  where IsActive=1  
END  