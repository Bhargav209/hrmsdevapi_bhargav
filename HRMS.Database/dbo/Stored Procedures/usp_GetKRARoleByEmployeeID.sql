﻿--GET KRA Role while On-Boarding
CREATE PROCEDURE [dbo].[usp_GetKRARoleByEmployeeID]              
 (              
  @EmployeeId INT              
 )                           
AS                              
BEGIN                          
                         
 SET NOCOUNT ON;           
               
  SELECT DISTINCT              
   kraRole.KRARoleId AS KRARoleId                     
 FROM AssociateKRARoleMapper kraRole              
 WHERE  kraRole.EmployeeId = @EmployeeId          
END 