﻿--Update KRA Role while On-boarding
CREATE PROCEDURE [dbo].[usp_UpdateKRARole]            
 (            
  @EmployeeId int,
  @KRARoleId int         
 )                         
AS                            
BEGIN                        
                       
 SET NOCOUNT ON;                  
BEGIN TRY          
  UPDATE AssociateKRARoleMapper SET              
	 KRARoleId = @KRARoleId where EmployeeId = @EmployeeId
	                
SELECT @@ROWCOUNT                             
  END TRY          
  BEGIN CATCH         
   THROW    
  END CATCH        
 END 