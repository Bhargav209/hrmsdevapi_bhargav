﻿-- ===========================================================  
-- Author			:  Santosh      
-- Create date		:  31-10-2017      
-- Modified date	:  31-10-2017      
-- Modified By		:  Santosh      
-- Description		:  Set Skill Group master as inactive   
-- ===========================================================  

-- ================================================  
-- Author			:  Kalyan.Penumutchu      
-- Create date		:  13-11-2017      
-- Modified date	:  13-11-2017      
-- Modified By		:  Kalyan.Penumutchu      
-- Description		:  IsActive is no longer requiered from UI. So hard-coded inside SP     
-- ================================================ 
CREATE PROCEDURE [dbo].[usp_DeleteSkillGroup]      
@SkillGroupId INT,
@ModifiedUser VARCHAR(100),  
@SystemInfo VARCHAR(50),
@ModifiedDate DATETIME

AS      
BEGIN  
  
 SET NOCOUNT ON;  
   
 UPDATE  [dbo].[SkillGroup]   
 SET  
 IsActive			= 0  
 ,ModifiedUser		= @ModifiedUser  
 ,ModifiedDate		= @ModifiedDate  
 ,SystemInfo		= @SystemInfo  
 WHERE SkillGroupId = @SkillGroupId  
  
 SELECT @@ROWCOUNT      
END
