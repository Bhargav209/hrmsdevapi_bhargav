﻿-- =====================================       
-- Author   : Mithun  
-- Create date  : 28-05-2019  
-- Modified date : 30-05-2019 ,12-06-2019 
-- Modified By  : Mithun, Sabiha  
-- Description      : Get ADR Section      
-- =====================================   
CREATE PROCEDURE [dbo].[usp_GetADRSection]      
AS      
BEGIN      
SET NOCOUNT ON;      
 SELECT      
  ADRSectionId AS ADRSectionId      
 ,ADRSectionName AS ADRSectionName      
 ,adrsection.FinancialYearId AS FinancialYearId      
 ,adrsection.ADRMeasurementAreaId AS ADRMeasurementAreaId      
 ,CONVERT(VARCHAR(10),adrsection.DepartmentId) AS DepartmentId  
 ,department.Description AS DepartmentDescription  
 ,adrMeasurementAreas.ADRMeasurementAreaName AS ADRMeasurementAreaName      
       
 FROM ADRSection adrsection       
  INNER JOIN ADRMeasurementAreas adrMeasurementAreas ON adrsection.ADRMeasurementAreaId = adrMeasurementAreas.ADRMeasurementAreaId 
  INNER JOIN Departments department ON adrsection.DepartmentId = department.DepartmentId      
END 
  