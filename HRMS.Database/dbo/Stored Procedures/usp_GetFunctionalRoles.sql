﻿-- ========================================================    
-- Author       : Chandra    
-- Create date  : 12-02-2018    
-- Modified date: 12-02-2018    
-- Modified By  : Chandra    
-- Description  : Get KraGroups by DepartmentId.    
-- ========================================================     
    
CREATE PROCEDURE [dbo].[usp_GetFunctionalRoles]        
(        
 @DepartmentID INT        
)        
AS        
BEGIN        
 SET NOCOUNT ON; 
        
 DECLARE @StatusID INT      
      
SELECT       
    @StatusID=StatusID      
FROM       
 [dbo].[Status]       
WHERE       
 StatusCode = 'Approved' AND CategoryID = ( SELECT CategoryID FROM [dbo].[CategoryMaster] WHERE CategoryName = 'KRA')        
SELECT        
   kraGroup.KRAGroupId AS Id        
  ,kraGroup.KRATitle AS Name        
 FROM [dbo].[KRAGroup] KRAGroup
 INNER JOIN [dbo].[KRAStatus] kraStatus ON kraGroup.KRAGroupId=kraStatus.KRAGroupId         
 WHERE [KRAGroup].DepartmentID = @DepartmentID  AND kraStatus.StatusID = @StatusID      
 ORDER BY Name        
END 

