﻿CREATE PROCEDURE [dbo].[usp_UpdateSOWDetails]         
(             
@Id int,          
  @SOWId VARCHAR(50)           
 ,@ProjectId INT          
 ,@SOWFileName VARCHAR(50)          
 ,@SOWSignedDate DATETIME          
 ,@RoleName VARCHAR(50)            
 ,@ModifiedDate DATETIME          
 ,@ModifiedUser VARCHAR(100)          
 ,@SystemInfo VARCHAR(100)          
)              
AS                            
BEGIN                   
              
 SET NOCOUNT ON;          
 DECLARE @RoleId INT         
 DECLARE @ProjectStateId INT         
 DECLARE @CategoryId INT        
 DECLARE @Drafted INT         
 DECLARE @Created INT   
 DECLARE @SubmittedForApproval INT        
          
 SELECT @CategoryId = CategoryID FROM CategoryMaster WHERE CategoryName = 'PPC'        
 SELECT @Drafted = StatusId FROM Status WHERE CategoryID=@CategoryId AND StatusCode='Drafted'        
 SELECT @Created = StatusId FROM Status WHERE CategoryID=@CategoryId AND StatusCode='Created'   
 SELECT @SubmittedForApproval = StatusId FROM Status WHERE CategoryID=@CategoryId AND StatusCode='SubmittedForApproval'       
        
 SELECT @ProjectStateId = ProjectStateId FROM Projects WHERE ProjectId=@ProjectId        
        
 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)        
 BEGIN TRY  
    IF( @ProjectStateId = @Created OR @ProjectStateId = @SubmittedForApproval)        
      BEGIN        
         IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head            
           BEGIN         
              UPDATE SOW SET           
                     SOWId=@SOWId          
                                  ,SOWFileName=@SOWFileName          
                                  ,SOWSignedDate=@SOWSignedDate           
                                  ,ModifiedDate = @ModifiedDate          
                                  ,ModifiedUser = @ModifiedUser          
                                  ,SystemInfo = @SystemInfo            
             FROM SOW sow              
             WHERE sow.Id=@Id AND ProjectId=@ProjectId          
           
             SELECT @@ROWCOUNT          
           END          
               
      END        
    ELSE IF( @ProjectStateId = @Drafted)        
     BEGIN        
        IF(@RoleId = 5 OR @RoleId = 3)  -- 5 is Program Manager.ensure that roles table have always 5 as Program manager            
           BEGIN      
                            UPDATE SOW SET           
                                         SOWId=@SOWId          
                                     ,SOWFileName=@SOWFileName          
                                     ,SOWSignedDate=@SOWSignedDate           
                                     ,ModifiedDate = @ModifiedDate          
                                     ,ModifiedUser = @ModifiedUser          
                                     ,SystemInfo = @SystemInfo            
                 FROM SOW sow              
                 WHERE sow.Id=@Id AND ProjectId=@ProjectId          
           
                 SELECT @@ROWCOUNT           
           END        
        END        
 END TRY  
BEGIN CATCH   
    SELECT ERROR_NUMBER( ) --2627 is Violation in unique index
 END CATCH        
   
 END 
