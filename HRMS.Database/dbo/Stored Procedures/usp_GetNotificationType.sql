﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	28-03-2018
-- Modified date	:	28-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets list of notification types.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetNotificationType]
AS         
BEGIN
	SET NOCOUNT ON;    
		SELECT
			  notificationType.NotificationTypeID
			 ,notificationType.NotificationCode AS NotificationType
			 ,notificationType.NotificationDesc AS [Description]
			 ,categoryMaster.CategoryID
			 ,categoryMaster.CategoryName
		FROM [dbo].[NotificationType] notificationType
		INNER JOIN [dbo].[CategoryMaster] categoryMaster
		ON notificationType.CategoryId = categoryMaster.CategoryID
		ORDER BY NotificationCode DESC   
END

