﻿-- ===================================================        
-- Author   : Basha        
-- Create date  : 19-10-2018        
-- Modified date : 19-10-2018                    
-- Modified By  : Basha        
-- Description  : Get Utilization Report By Technology Count        
--[dbo].[usp_rpt_UtilizationReportCountByTechnology] 0, 0, 0, 0, 0, 0, 0, 0, 1, 1        
-- ===================================================    
CREATE PROCEDURE [dbo].[usp_rpt_UtilizationReportByTechnologyCount]      
(      
   @practiceAreaID INT      
)      
AS      
BEGIN      
      
 SET NOCOUNT ON;      
         
 SELECT         
  COUNT(Distinct employee.EmployeeId) AS TotalCount          
 FROM [dbo].[AssociateAllocation] allocation                      
                     
   INNER JOIN [dbo].[Employee] employee                      
   ON allocation.EmployeeId = employee.EmployeeId   
   LEFT JOIN [dbo].[EmployeeType] employeeType 
   on  employee.EmployeeTypeId=employeeType.EmployeeTypeId
   INNER JOIN [dbo].[Grades] grade                      
   ON employee.GradeId = grade.GradeId                      
   INNER JOIN [dbo].[Departments] department                      
   ON employee.DepartmentId = department.DepartmentId                      
   INNER JOIN [dbo].[Designations] designation                      
   ON employee.Designation = designation.DesignationId                      
   INNER JOIN [dbo].[AllocationPercentage] allocationpercentage                      
   ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID                   
   LEFT JOIN PracticeArea PA ON employee.CompetencyGroup=PA.PracticeAreaId 
   WHERE
    ISNULL(PA.PracticeAreaId, 0) = CASE  @practiceAreaID WHEN 0 THEN ISNULL(PA.PracticeAreaId, 0) ELSE @practiceAreaID END              
   AND allocation.IsActive = 1  
END
