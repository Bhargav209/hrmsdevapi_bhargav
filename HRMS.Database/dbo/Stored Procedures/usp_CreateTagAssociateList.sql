﻿-- ============================================  
-- Author			:	Prasanna          
-- Create date		:	28-01-2019  
-- Modified date	:	28-01-2019    
-- Description		:	Create Wish List For Manager  
-- ============================================    
CREATE PROCEDURE [dbo].[usp_CreateTagAssociateList]    
@ManagerId INT  
,@TagAssociateListName VARCHAR(150)  
,@EmployeeIds VARCHAR(1000)  
,@CreatedUser VARCHAR(150)           
,@SystemInfo VARCHAR(50)  
AS    
BEGIN    
 SET NOCOUNT ON;         
 -- check Tag list name existence for current manager  
 if not exists(select Id from TagAssociate where TagAssociateListName=@TagAssociateListName and ManagerId=@ManagerId)  
 Begin  
  --insert employee Ids into temp table  
  DECLARE @ListofEmpIDs TABLE(ID int IDENTITY(1,1),EmployeeId VARCHAR(100));  
  INSERT INTO @ListofEmpIDs SELECT VALUE FROM [UDF_SplitString] (@EmployeeIds, ',')  
  
  --loop throught the table and inser the records into wish list  
  DECLARE @Count INT  
  DECLARE @RowCount INT  
  DECLARE @employee_id NUMERIC(38,0)  
  SET @count=1  
  SELECT @RowCount=COUNT(1) FROM @ListofEmpIDs  
  WHILE(@count<=@RowCount)  
   BEGIN     
   --  
    SELECT @employee_id=EmployeeId FROM @ListofEmpIDs WHERE ID=@Count;  
    Insert into TagAssociate  
    (  
     TagAssociateListName  
     ,EmployeeID  
     ,ManagerId  
     ,CreatedUser  
     ,CreatedDate  
     ,SystemInfo  
    )  
    values  
    (  
     @TagAssociateListName  
     ,@employee_id  
     ,@ManagerId  
     ,@CreatedUser  
     ,GetDate()  
     ,@SystemInfo  
    )  
    SET @count=@count+1;  
   END  
  SELECT 1;  
 End  
 else  
  SELECT -1  
END 