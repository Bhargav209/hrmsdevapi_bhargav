﻿-- ================================================
-- Author			:	Santosh    
-- Create date		:	31-10-2017    
-- Modified date	:	31-10-2017    
-- Modified By		:	Santosh    
-- Description		:	Gets Skill Group master 
-- ================================================
CREATE PROCEDURE [dbo].[usp_GetSkillGroup]    
AS    
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
     sg.SkillGroupId
	,sg.SkillGroupName
	,sg.[Description]
	,sg.IsActive
	,comp.CompetencyAreaCode
	,sg.CompetencyAreaId
	FROM [dbo].[SkillGroup] sg
	INNER JOIN [dbo].[CompetencyArea] comp
	ON sg.CompetencyAreaId = comp.CompetencyAreaId
	WHERE sg.IsActive = 1  
END

