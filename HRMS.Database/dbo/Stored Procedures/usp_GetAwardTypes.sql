﻿-- =================================================
-- Author			:	Basha
-- Create date		:	15-05-2018
-- Modified date	:	15-05-2018
-- Modified By		:	Basha
-- Description		:	Get all the Award Types 
-- =================================================
CREATE PROCEDURE [dbo].[usp_GetAwardTypes]
AS              
BEGIN     

 SET NOCOUNT ON;           
 SELECT  
	AwardTypeId AS Id,
	AwardType AS Name
 FROM 
	[dbo].[AwardType]              
END

