﻿-- ============================================             
-- Author   : Mithun                    
-- Create date  : 18-06-2019                    
-- Modified date :                    
-- Modified By  :                    
-- Description  : Get Associate ADR Details        
-- ============================================        
CREATE PROCEDURE [dbo].[usp_GetAssociateADRDetail]           
(              
 @EmployeeID INT,          
 @FinancialYearID INT,  
 @AdrCycleId INT          
)                   
AS                     
BEGIN           
          
SET NOCOUNT ON;               
     
     DECLARE @KRAGroupId INT  
  
--SELECT @KRAGroupId=KRAGroupID FROM AssociateKRAMapper where EmployeeID=172 AND FinancialYearId=11  
  
SELECT   
 aspmaster.AspectName AS AspectName
,aspmaster.AspectId  AS AspectId
,kradefinition.Metric  AS Metric
,kradefinition.KRADefinitionId  AS KRADefinitionId
,kradefinition.TargetValue  AS TargetValue
,kradefinition.FinancialYearId  AS FinancialYearId
,kraOperator.Operator AS Operator         
,kraMeasurement.KRAMeasurementType AS KRAMeasurementType   
,kraTargetPeriod.KRATargetPeriod  AS KRATargetPeriod 
,associatekramapper.AssociateKRAMapperId  AS AssociateKRAMapperId
,associateadrmaster.AssociateADRMasterId  AS AssociateADRMasterId
,associateadrmaster.ADRCycleID  AS ADRCycleID
,associateadrmaster.StatusId  AS StatusId
,associateadrdetail.AssociateADRDetailId  AS AssociateADRDetailId
,associateadrdetail.Contribution  AS Contribution
,associateadrdetail.SelfRating  AS SelfRating
,associateadrdetail.ManagerComments  AS ManagerComments
,associateadrdetail.Rating   AS Rating
,state.StatusCode AS StatusCode
FROM KRADefinition kradefinition   
JOIN AssociateKRAMapper associatekramapper ON associatekramapper.KRAGroupID=kradefinition.KRAGroupId AND associatekramapper.EmployeeID=@EmployeeID AND associatekramapper.FinancialYearId=@FinancialYearID  
JOIN KRAAspectMaster kraasMaster ON kradefinition.KRAAspectId = kraasMaster.KRAAspectId  
JOIN AspectMaster aspmaster ON aspmaster.AspectId = kraasMaster.AspectId 
LEFT JOIN  AssociateADRMaster associateadrmaster ON associateadrmaster.ADRCycleID=@AdrCycleId 
INNER JOIN  Status state ON associateadrmaster.StatusId = state.StatusId  
LEFT JOIN  AssociateADRDetail associateadrdetail ON associateadrmaster.AssociateADRMasterId = associateadrdetail.AssociateADRMasterId and associateadrdetail.[KRADefinitionId] = kradefinition.KRADefinitionId   
INNER JOIN KRAOperator kraOperator ON kradefinition.KRAOperatorID = kraOperator.KRAOperatorID      
INNER JOIN KRAMeasurementType kraMeasurement ON kradefinition.KRAMeasurementTypeID = kraMeasurement.KRAMeasurementTypeID   
INNER JOIN KRATargetPeriod kraTargetPeriod ON kradefinition.KRATargetPeriodID = kraTargetPeriod.KRATargetPeriodID      
WHERE kradefinition.KRAGroupId=associatekramapper.KRAGroupId   
and kradefinition.FinancialYearId=@FinancialYearID  
END  
  