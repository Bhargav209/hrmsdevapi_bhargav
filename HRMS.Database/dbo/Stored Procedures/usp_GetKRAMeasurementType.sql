﻿-- =======================================================================================
-- Author				:	Sushmitha
-- Create date			:	10-05-2018
-- Modified date		:	10-05-2018
-- Modified By			:	Sushmitha
-- Description			:	Gets list of KRA measurement types.
-- =======================================================================================   

CREATE PROCEDURE [dbo].[usp_GetKRAMeasurementType]
AS
BEGIN
	SET NOCOUNT ON;      
                   
	SELECT    
		KRAMeasurementTypeID AS Id
	   ,KRAMeasurementType AS Name
	FROM [dbo].[KRAMeasurementType]  

END

