﻿-- ============================================                 
-- Author   : Chandra                        
-- Create date  : 23-03-2018                        
-- Modified date :                        
-- Modified By  :                        
-- Description  : Gets the organization and custom KRAs for Associates            
-- ============================================            
CREATE PROCEDURE [dbo].[usp_GetOrganizationAndCustomKRAsForEmployee]             
(                
 @EmployeeID INT,            
 @KRAGroupID INT,               
 @FinancialYearID INT            
)                     
AS                       
BEGIN             
            
SET NOCOUNT ON;                 
                
 DECLARE @KRARoleID INT            
 DECLARE @StatusID INT            
            
SELECT             
    @StatusID=StatusID            
FROM             
 [dbo].[Status]             
WHERE             
 StatusCode = 'Approved' AND CategoryID = ( SELECT CategoryID FROM [dbo].[CategoryMaster] WHERE CategoryName = 'KRA')            
            
  SELECT DISTINCT     
    KRADefinition.KRAAspectOrder
   ,KRAGroup.KRATitle            
   ,aspectMaster.AspectName AS KRAAspectName                 
   ,kraDefinition.Metric as KRAAspectMetric                 
   ,kraOperator.Operator            
   ,kraMeasurement.KRAMeasurementType            
   ,CAST(kraScaleMaster.MinimumScale AS VARCHAR(1)) + ' - ' + CAST(kraScaleMaster.MaximumScale AS VARCHAR(2)) as ScaleLevel            
   ,kraDefinition.TargetValue AS KRATargetValue            
   ,kraDefinition.KRATargetText            
   ,kraTargetPeriod.KRATargetPeriod            
   FROM [dbo].[KRAGroup] kraGroup            
   INNER JOIN [dbo].[KRADefinition] kraDefinition ON kraGroup.KRAGroupId = kraDefinition.KRAGroupId             
   INNER JOIN [dbo].[KRAAspectMaster] kraAspect ON kraDefinition.KRAAspectId = kraAspect.KRAAspectID            
   INNER JOIN [dbo].[KRAOperator] kraOperator ON kraDefinition.KRAOperatorID = kraOperator.KRAOperatorID              
   INNER JOIN [dbo].[KRAMeasurementType] kraMeasurement ON kraDefinition.KRAMeasurementTypeID = kraMeasurement.KRAMeasurementTypeID             
   LEFT JOIN [dbo].[KRAScaleMaster] kraScaleMaster ON kraDefinition.KRAScaleMasterID = kraScaleMaster.KRAScaleMasterID             
   INNER JOIN [dbo].[KRATargetPeriod] kraTargetPeriod ON kraDefinition.KRATargetPeriodID = kraTargetPeriod.KRATargetPeriodID            
   INNER JOIN [dbo].[KRAStatus] kraStatus ON kraGroup.KRAGroupId=kraStatus.KRAGroupId            
   INNER JOIN [dbo].AssociateKraMapper associateKraMapper ON kraGroup.KRAGroupId=associateKraMapper.KRAGroupID            
   INNER JOIN [dbo].[AspectMaster] aspectMaster ON kraAspect.AspectId = aspectMaster.AspectId            
   WHERE KRADefinition.KRAGroupId = @KRAGroupID AND kraDefinition.FinancialYearId= @FinancialYearID AND AssociateKraMapper.FinancialYearId=@FinancialYearID AND kraStatus.StatusID = @StatusID AND associateKraMapper.EmployeeID = @EmployeeID 
   ORDER BY KRADefinition.KRAAspectOrder       
END 

