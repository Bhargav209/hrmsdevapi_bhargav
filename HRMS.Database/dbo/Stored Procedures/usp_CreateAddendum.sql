﻿-- ========================================================      
-- Author   : Mithun      
-- Create date  : 10-01-2019      
-- Modified date : 10-01-2019      
-- Modified By  : Mithun      
-- Description  : Add Addendum .      
-- ========================================================       
      
CREATE PROCEDURE [dbo].[usp_CreateAddendum]      
(        
 @ProjectId INT
,@Id INT
,@AddendumNo VARCHAR(50)
,@RecipientName VARCHAR(30)
,@AddendumDate VARCHAR(150)
,@Note VARCHAR(250)
,@CreatedDate DATETIME
,@CreatedUser VARCHAR(150)
,@SystemInfo  VARCHAR(50)      
)        
AS      
BEGIN      
 SET NOCOUNT ON;
 DECLARE @SOWId VARCHAR(20)
 
 SELECT @SOWId=SOWId from sow where Id= @Id AND ProjectId = @ProjectId    
 INSERT INTO       
 [dbo].[Addendum]        
 (ProjectId, SOWId, AddendumNo, RecipientName, AddendumDate, Note,CreatedDate, CreatedUser, SystemInfo,Id)        
 VALUES        
 (@ProjectId, @SOWId,@AddendumNo,@RecipientName,Convert(DateTime,@AddendumDate),@Note,@CreatedDate, @CreatedUser, @SystemInfo,@Id)        
        
 SELECT @@ROWCOUNT      
END