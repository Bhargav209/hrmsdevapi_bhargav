﻿CREATE PROCEDURE [dbo].[usp_AssignProgramManagerToProject]
 (
  @UserRole VARCHAR(20),
  @EmployeeId INT
 )             
AS                
BEGIN            
           
 SET NOCOUNT ON;
 
 DECLARE @RoleId INT

 SELECT @RoleId=RoleId FROM Roles role WHERE RoleName = @UserRole
 IF EXISTS (SELECT 1 FROM UserRoles WHERE RoleId = @RoleId)
 BEGIN
    IF(@RoleId = 5) -- 5 is program manager.ensure that roles table have always 5 as program manager  
     SELECT     
       userrole.UserRolesID  
      ,programmanager.EmployeeId  
      ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName 
     
     FROM  UserRoles userrole   
     INNER JOIN  Employee programmanager ON userrole.UserId = programmanager.UserId  
     INNER JOIN  Roles roles  ON roles.RoleId = userrole.RoleId  
     WHERE roles.RoleName = @UserRole AND roles.IsActive = 1 AND userrole.IsActive=1 AND programmanager.EmployeeId =@EmployeeId
  
 ELSE IF(@RoleId = 3) -- 3 is department head.ensure that roles table have always 3 as department head  
     SELECT     
       userrole.UserRolesID  
      ,programmanager.EmployeeId  
      ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName   
     
     FROM  UserRoles userrole   
     INNER JOIN  Employee programmanager ON userrole.UserId = programmanager.UserId  
     INNER JOIN  Roles roles  ON roles.RoleId = userrole.RoleId  
     WHERE roles.RoleName = 'Program Manager' AND roles.IsActive = 1 AND userrole.IsActive=1  
 END  
END