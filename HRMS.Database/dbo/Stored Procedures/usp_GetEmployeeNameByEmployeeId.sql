﻿-- =================================================
-- Author			:	Sushmitha
-- Create date		:	03-04-2018
-- Modified date	:	03-04-2018
-- Modified By		:	Sushmitha
-- Description		:	Get all the active employees by employeeid
-- =================================================

CREATE PROCEDURE [dbo].[usp_GetEmployeeNameByEmployeeId]
(
@EmployeeId INT
)
AS
BEGIN
 SET NOCOUNT ON;

	SELECT
		[dbo].[udfGetEmployeeFullName](@EmployeeId) AS [Name]

END

