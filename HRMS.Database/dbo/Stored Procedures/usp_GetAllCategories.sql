﻿-- ==========================================     
-- Author			:	Santosh            
-- Create date		:	31-10-2017            
-- Modified date	:	31-10-2017            
-- Modified By		:	Santosh            
-- Description		:	Gets all the category       
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetAllCategories]             
AS              
BEGIN          
          
 SET NOCOUNT ON;          
           
 SELECT  
		CategoryID      
	   ,CategoryName      
	   ,CASE IsActive WHEN 1 THEN 'Yes' ELSE 'No' END AS Active
 FROM [dbo].[CategoryMaster]                   
END
