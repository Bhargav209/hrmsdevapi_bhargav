﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	26-03-2018            
-- Modified date	:	26-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all the employees under a department.
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetEmployeesForDepartment]
(    
 @EmployeeID INT 
)         
AS           
BEGIN 
	SET NOCOUNT ON;  

	DECLARE @DepartmentId INT

	SELECT @DepartmentId = DepartmentId FROM [dbo].[Departments] WHERE DepartmentHeadID = @EmployeeID

	SELECT
		 EmployeeId AS ID
	    ,[dbo].[udfGetEmployeeFullName](EmployeeId) AS Name
	FROM [dbo].[Employee] 
	WHERE 
		Employee.DepartmentId = @DepartmentId AND Employee.IsActive=1
END 
GO


