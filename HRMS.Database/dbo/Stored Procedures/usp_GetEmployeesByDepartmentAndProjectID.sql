﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	20-03-2018            
-- Modified date	:	20-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets Employee details by project id  
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetEmployeesByDepartmentAndProjectID]
(
	 @ProjectID INT
	,@DepartmentID INT
	,@IsNew BIT
)
AS       
BEGIN  
	SET NOCOUNT ON;    

		IF (@IsNew = 1)
		BEGIN
			IF (@ProjectID > 0)
			BEGIN
				SELECT
					allocation.AssociateAllocationId AS Id
					,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName
				FROM 
					[dbo].[AssociateAllocation] allocation
					INNER JOIN [dbo].[Employee] employee
					ON allocation.EmployeeId = employee.EmployeeId
				WHERE 
					ProjectID = @ProjectId AND DepartmentId = @DepartmentID AND allocation.IsActive = 1 AND allocation.RoleMasterId IS NULL
			END
			ELSE
			BEGIN
				SELECT
					EmployeeId AS Id
					,[dbo].[udfGetEmployeeFullName](EmployeeId) AS AssociateName
				FROM 
					[dbo].[Employee]
				WHERE 
					DepartmentId = @DepartmentID AND IsActive = 1 AND EmployeeId NOT IN (SELECT EmployeeId FROM [dbo].[ServiceDepartmentRoles] WHERE DepartmentId = @DepartmentID AND IsActive = 1)
			END
		END
		ELSE 
		BEGIN
			IF (@ProjectID > 0)
			BEGIN
				SELECT
					allocation.AssociateAllocationId AS Id
					,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName
					,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
				FROM 
					[dbo].[AssociateAllocation] allocation
					INNER JOIN [dbo].[Employee] employee
					ON allocation.EmployeeId = employee.EmployeeId
					INNER JOIN [dbo].[RoleMaster] rolemaster
					ON allocation.RoleMasterId = rolemaster.RoleMasterID
				WHERE 
					allocation.ProjectID = @ProjectId AND employee.DepartmentId = @DepartmentID AND allocation.IsActive = 1 AND allocation.RoleMasterId IS NOT NULL
			END
			ELSE
			BEGIN
				SELECT 
					employee.EmployeeId AS Id
					,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName
					,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
                 FROM 
					[dbo].[Employee] employee
					INNER JOIN [dbo].[ServiceDepartmentRoles] serviceDept
					ON employee.EmployeeId = serviceDept.EmployeeID
					INNER JOIN [dbo].[RoleMaster] rolemaster
					ON serviceDept.RoleMasterID = rolemaster.RoleMasterID
                 WHERE 
					serviceDept.DepartmentID = @DepartmentID AND serviceDept.IsActive = 1 AND employee.IsActive = 1

			END
		END
END
