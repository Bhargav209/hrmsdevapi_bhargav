﻿-- ======================================================      
-- Author   : Basha        
-- Create date  : 12-12-2018    
-- Modified date : 27-12-2018    
-- Modified By  : Bhavani    
-- Description  : Create or Update Project Client Billing Roles
-- [dbo].[usp_CreateProjectClientBillingRoles]
-- ======================================================              

CREATE   PROCEDURE [dbo].[usp_CreateProjectClientBillingRoles]          
(            
@ProjectId INT,          
@ClientBillingRoleId VARCHAR(150),
@NoOfPositions INT,          
@CreatedUser VARCHAR(150),            
@SystemInfo VARCHAR(50),
@ProjectClientBillingRoleId INT NULL,
@AddendumNumber VARCHAR(100) NULL,
@ReasonForUpdate VARCHAR(MAX) NULL
)            
AS          
BEGIN       
SET NOCOUNT ON;   

	IF (@ProjectClientBillingRoleId = NULL OR @ProjectClientBillingRoleId = 0)
	BEGIN
	INSERT INTO ProjectClientBillingRoles (ProjectId,ClientBillingRoleId,NoOfPositions,CreatedUser,CreatedDate,SystemInfo,IsActive) VALUES 
		(@ProjectId,@ClientBillingRoleId,@NoOfPositions,@CreatedUser,GETDATE(),@SystemInfo,1)
	END

	ELSE
	BEGIN
	IF NOT EXISTS(SELECT 1 FROM ProjectClientBillingRoles WHERE ProjectClientBillingRoleId = @ProjectClientBillingRoleId AND ProjectId = @ProjectId AND ClientBillingRoleId = @ClientBillingRoleId AND NoOfPositions = @NoOfPositions AND IsActive = 1)
	
	UPDATE ProjectClientBillingRoles SET IsActive = 0, ModifiedUser = @CreatedUser, ModifiedDate = GETDATE(), SystemInfo = @SystemInfo WHERE ProjectClientBillingRoleId = @ProjectClientBillingRoleId 

	INSERT INTO ProjectClientBillingRoles (ProjectId,ClientBillingRoleId,NoOfPositions,CreatedUser,CreatedDate,SystemInfo,IsActive) VALUES 
		(@ProjectId,@ClientBillingRoleId,@NoOfPositions,@CreatedUser,GETDATE(),@SystemInfo,1)

	END
END