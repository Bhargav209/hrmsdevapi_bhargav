﻿-- ================================================================    
-- Author			:	Ramya Singamsetti
-- Create date		:	24-11-2017
-- Modified date	:	05-06-2018
-- Modified By		:	Santosh
-- Description		:	Gets Expertise Area by Talent requisitionId
-- ================================================================    
CREATE PROCEDURE [dbo].[usp_GetExpertiseAreaByTrId]
(
	@TalentRequisitionId INT    
   ,@RoleMasterId INT
)
AS        
BEGIN    
    
 SET NOCOUNT ON;    
     
SELECT  
  @TalentRequisitionId as TalentRequisitionId,  
  Case when isnull(rer.RequisitionExpertiseId,0)=0 then 0 else rer.RequisitionExpertiseId end as RequisitionExpertiseId,  
  ea.ExpertiseId,  
  ea.ExpertiseAreaDescription,  
  rer.ExpertiseDescription    
 FROM [dbo].[RequisitionExpertiseRequirements] rer  
 right join [dbo].ExpertiseArea ea on rer.ExpertiseAreaId=ea.ExpertiseId AND rer.TRId=@TalentRequisitionId  AND rer.RoleMasterId = @RoleMasterId  
  
END  
