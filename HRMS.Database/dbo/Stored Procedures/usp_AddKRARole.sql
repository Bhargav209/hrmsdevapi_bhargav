﻿--Add KRA Role while On-Boarding
CREATE PROCEDURE [dbo].[usp_AddKRARole]                
 (                
  @EmployeeCode varchar(20),
  @EmployeeId int,
  @KRARoleId int                           
 )                             
AS                                
BEGIN                            
                           
 SET NOCOUNT ON;                 
DECLARE @FinancialYearId INT
SELECT @FinancialYearId = ID FROM FinancialYear where IsActive =1 
  BEGIN TRY          
  INSERT INTO AssociateKRARoleMapper(                
     EmployeeCode,
	 EmployeeId,
	 KRARoleId,
	 FinancialYearId)                
    VALUES(   
	@EmployeeCode,
  @EmployeeId,
  @KRARoleId,
  @FinancialYearId             
    )                
SELECT @@ROWCOUNT                             
  END TRY          
  BEGIN CATCH         
   THROW    
  END CATCH                
 END 