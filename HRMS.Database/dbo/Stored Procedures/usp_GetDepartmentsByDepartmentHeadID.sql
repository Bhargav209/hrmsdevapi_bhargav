﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	09-03-2018
-- Modified date	:	09-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets departmentid and name by employee id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetDepartmentsByDepartmentHeadID]
(
	@EmployeeID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		 department.DepartmentId AS Id
		,department.Description AS Name
	FROM [Departments] department
	WHERE department.DepartmentHeadID = @EmployeeID and department.IsActive=1
END

