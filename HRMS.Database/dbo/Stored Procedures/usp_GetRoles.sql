﻿-- ====================================      
-- Author  : Santosh      
-- Create date  : 09-92-2018      
-- Modified date: 09-92-2018      
-- Modified By  : Santosh      
-- Description  : Gets all the roles      
-- ===================================      
CREATE PROCEDURE [dbo].[usp_GetRoles]    
AS    
BEGIN    
 SET NOCOUNT ON;    
      
 SELECT    
   roleMaster.RoleMasterID as RoleMasterId     
  ,LTRIM(RTRIM(CONCAT(prefix.PrefixName, ' ', sgRoles.SGRoleName, ' ', suffix.SuffixName))) AS RoleName    
  ,sgRoles.SGRoleID    
  ,dept.DepartmentCode    
  ,dept.DepartmentId
  ,roleMaster.PrefixID
  ,roleMaster.SuffixID 
  ,roleMaster.RoleDescription
  ,roleMaster.KeyResponsibilities      
  ,roleMaster.EducationQualification
  ,kraGroup.KRATitle
  ,roleMaster.KRAGroupID
 FROM [dbo].[RoleMaster] roleMaster    
  INNER JOIN [dbo].[SGRole] sgRoles    
 ON roleMaster.SGRoleID = sgRoles.SGRoleID    
 LEFT JOIN [dbo].[SGRolePrefix] prefix    
 ON roleMaster.PrefixID = prefix.PrefixID    
 LEFT JOIN [dbo].[SGRoleSuffix] suffix    
 ON roleMaster.SuffixID = suffix.SuffixID    
 INNER JOIN [dbo].[Departments] dept    
 ON roleMaster.DepartmentID = dept.DepartmentID   
 LEFT JOIN [dbo].[KRAGroup] kraGroup    
 ON roleMaster.KRAGroupID = kraGroup.KRAGroupID  
END

