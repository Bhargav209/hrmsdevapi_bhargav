﻿-- ========================================================              
-- Author   : Basha              
-- Create date  : 24-12-2018              
-- Modified date : 24-04-2019          
-- Modified By  : Sabiha              
-- Description  : Update Client              
-- ========================================================               
CREATE PROCEDURE [dbo].[usp_UpdateClient]          
(              
  @ClientId INT              
 ,@ClientCode VARCHAR(6)              
 ,@ClientName VARCHAR(50)              
 ,@ClientRegisterName VARCHAR(150)              
 ,@IsActive bit              
 ,@ModifiedUser VARCHAR(100)              
 ,@ModifiedDate   DATETIME              
 ,@SystemInfo VARCHAR(50)              
)              
AS              
BEGIN              
 SET NOCOUNT ON;              
              
 DECLARE @IsCategoryShortNameExists INT   , @ClientShortNameHash varchar(450)          
 SELECT @ClientName= RTRIM(LTRIM(@ClientName))              
              
 SELECT @ClientShortNameHash = CONVERT(varchar(50),UPPER(@ClientName));          
 SET @ClientShortNameHash  = HashBytes('SHA1', @ClientShortNameHash);          
               
  BEGIN TRY          
   Update Clients               
   SET              
   clientName=@ClientName,              
   clientRegisterName=@ClientRegisterName,            
   ClientNameHash=@ClientShortNameHash,          
   IsActive=@IsActive,    
   ModifiedUser=@ModifiedUser,  
   ModifiedDate=@ModifiedDate           
   WHERE              
   ClientId=@ClientId              
              
   SELECT @@ROWCOUNT                
          
   END TRY            
 BEGIN CATCH           
  SELECT -1          
 END CATCH            
END 