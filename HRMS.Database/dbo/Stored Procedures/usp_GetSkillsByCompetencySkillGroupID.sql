﻿

-- ================================================================
-- Author			:  Sushmitha    
-- Create date		:  31-10-2017      
-- Modified date	:  31-10-2017      
-- Modified By		:  Sushmitha      
-- Description		:  Gets Skill Group based upon CompetencyAreaId  and SkillGroupId
-- ================================================================
CREATE PROCEDURE [dbo].[usp_GetSkillsByCompetencySkillGroupID] 
@CompetencyAreaId INT,
@SkillGroupId INT
AS      
BEGIN  
  
 SET NOCOUNT ON;  
   
 SELECT 
	SkillId
   ,SkillName  
 FROM [dbo].[Skills]
 WHERE CompetencyAreaId = @CompetencyAreaId AND SkillGroupId = @SkillGroupId AND Isnull(IsActive,1) = 1
END

