﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-01-2018
-- Modified date	:	08-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Get talent requisition approvers list.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetTalentRequisitionApprovers]             
AS              
BEGIN          
          
 SET NOCOUNT ON;
 
 DECLARE @EmailTo VARCHAR(MAX)  
 
 SET @EmailTo = (SELECT EmailTo 
				 FROM NotificationConfiguration 
				 WHERE NotificationTypeID = (SELECT NotificationTypeID 
											 FROM NotificationType WHERE NotificationCode = 'TRSubmitForApproval')
				 AND CategoryId =(Select CategoryID from CategoryMaster where CategoryName='TalentRequisition'))
 SELECT      
    employee.EmployeeId AS Id
   ,employee.FirstName + ' ' +employee.LastName AS Name
 FROM 
	[dbo].[Employee] employee  
 INNER JOIN [dbo].[Users] users  
 ON employee.UserId = users.UserId  
 WHERE 
 --employee.IsActive=1 and users.IsActive=1 and
 users.EmailAddress IN (SELECT VALUE FROM [dbo].[UDF_SplitString](@EmailTo,';'))

END

