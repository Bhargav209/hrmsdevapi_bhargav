﻿
-- =======================================================
-- Author			:	Sushmitha
-- Create date		:	22-11-2017
-- Modified date	:	22-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Gets Associates Pending Resignations List 
-- ======================================================= 

CREATE PROCEDURE [dbo].[usp_GetAssociatePendingResignationsList]
@EmployeeID INT
AS
BEGIN
 SET NOCOUNT ON;

 DECLARE @StatusID INT
 DECLARE @DesignationID INT
 
 SELECT @DesignationID = Designation FROM Employee WHERE EmployeeId = @EmployeeID

 IF @DesignationID = 5 -- ApprovedByPM
 SELECT @StatusID = StatusId FROM [dbo].[Status] WHERE StatusCode = 'SubmittedForResignation'
 ELSE
 SELECT @StatusID =  StatusId FROM [dbo].[Status] WHERE StatusCode = 'ApprovedByPM'

 SELECT
  resignation.ResignationID
 ,resignation.DateOfResignation AS ResignationDate
 ,resignation.EmployeeID
 ,employee.EmployeeCode
 ,employee.FirstName + ' ' + employee.LastName AS EmployeeName
 ,designation.DesignationName AS Designation
 FROM [dbo].[AssociateResignation] resignation
 INNER JOIN [dbo].[Employee] employee
 ON resignation.EmployeeId = employee.EmployeeId
 INNER JOIN [dbo].[Designations] designation 
 ON employee.Designation = designation.DesignationID
 INNER JOIN [dbo].[AssociateResignationWorkFlow] EmployeeWorkFlow
 ON resignation.ResignationID = EmployeeWorkFlow.ResignationID
 WHERE EmployeeWorkFlow.ToEmployeeID = @EmployeeID AND resignation.StatusId = @StatusID
END
