﻿-- ======================================================    
-- Author   : Ramya      
-- Create date  : 07-03-2018  
-- Modified date : 
-- Modified By  : Ramya  
-- Description  : Get Employee Primary Allocation Project
-- usp_GetEmployeePrimaryAllocationProject 7
-- ======================================================            
CREATE PROCEDURE [dbo].[usp_GetEmployeePrimaryAllocationProject]
( 
@EmployeeID INT)
AS
BEGIN

SELECT 
	[allocation].AssociateAllocationId,
	[allocation].EmployeeId,
	[allocation].ProjectId,
	[percentage].Percentage as AllocationPercentage,
	[allocation].IsPrimary,
	[project].ProjectName as Project,
	[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
FROM AssociateAllocation [allocation]
INNER JOIN [Projects] AS [project] ON [allocation].[ProjectId] = ([project].[ProjectId])
INNER JOIN [AllocationPercentage] as [percentage] on [allocation].AllocationPercentage=[percentage].AllocationPercentageID
INNER JOIN [ProjectType] AS [projecttype] ON [project].[ProjectTypeId] = ([projecttype].[ProjectTypeId]) and [ProjectType].ProjectTypeId<>6
INNER JOIN [RoleMaster] AS [rolemaster] ON [allocation].[RoleMasterId] = ([rolemaster].[RoleMasterID])
WHERE allocation.EmployeeId=@EmployeeID and allocation.IsPrimary=1 and allocation.IsActive=1
END
