﻿          
CREATE PROCEDURE [dbo].[usp_rpt_GetRmgReportDataByMonthYear]      
(        
  @Month INT      
 ,@Year INT      
)             
AS               
BEGIN      
 SET NOCOUNT ON;        
       
 SELECT 
    JsonData 
 FROM FreezFinanceReportData 
 WHERE Month=@Month AND Year=@Year

END 
