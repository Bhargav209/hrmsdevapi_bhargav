﻿-- =======================================================================================
-- Author				:	Sushmitha
-- Create date			:	10-05-2018
-- Modified date		:	10-05-2018
-- Modified By			:	Sushmitha
-- Description			:	Gets list of KRA target periods.
-- =======================================================================================   

CREATE PROCEDURE [dbo].[usp_GetKRATargetPeriods]
AS
BEGIN
	SET NOCOUNT ON;      
                   
	SELECT 
		KRATargetPeriodID AS Id
	   ,KRATargetPeriod AS Name
	FROM [dbo].[KRATargetPeriod]

END

