﻿-- =============================================
-- Author : Kalyan Penumutchu
-- Date : 13-Aug-2018
-- Description : This sp accepts JDTemplateId and returns list of skills.
-- Exec USP_GetSkillsByJDTemplateTitleId 1
-- =============================================

CREATE PROCEDURE [dbo].[USP_GetSkillsByJDTemplateTitleId] 
	@JDTemplateTitleId int
AS

--1. Now get skill details based Template Title id
SELECT 
	JDTDetail.[TemplateDetailId],
	JDTDetail.[TemplateTitleId],
	ca.CompetencyAreaId,
	ca.CompetencyAreaCode,
	SG.SkillGroupId,
	SG.SkillGroupName,
	sk.SkillId,
	sk.SkillName,
	profL.ProficiencyLevelId,
	profL.ProficiencyLevelCode
FROM 
	JDTemplateDetail JDTDetail
	JOIN CompetencyArea CA ON CA.CompetencyAreaId = JDTDetail.[CompetencyAreaId]
	JOIN SkillGroup SG ON SG.SkillGroupId = JDTDetail.SkillGroupId
	JOIN Skills sk ON sk.SkillId =  JDTDetail.[SkillId]
	JOIN ProficiencyLevel profL ON profL.ProficiencyLevelId = JDTDetail.ProficiencyLevelId
WHERE 
	JDTDetail.[TemplateTitleId] = 1
