﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-02-2018
-- Modified date	:	12-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets roles, suffixes and prefixes.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRoleSuffixAndPrefix]
@DepartmentId INT
AS
BEGIN
 SET NOCOUNT ON;
	
	SELECT
		 SGRoleID AS Id
		,SGRoleName AS Name
	FROM
		[dbo].[SGRole]
	WHERE
		DepartmentId = @DepartmentId		

	SELECT
		 PrefixID AS Id
		,PrefixName AS Name
	FROM
		[dbo].[SGRolePrefix]

	SELECT
		 SuffixID AS Id
		,SuffixName AS Name
	FROM
		[dbo].[SGRoleSuffix]
END
