﻿-- ============================================================     
-- Author			:	Sushmitha            
-- Create date		:	26-03-2018            
-- Modified date	:	26-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all the employees under a project.
-- ============================================================
CREATE PROCEDURE [dbo].[usp_GetEmployeesByPrimaryProjectID]
(    
	@ProjectID INT 
)         
AS           
BEGIN 
	SET NOCOUNT ON;  

	SELECT
		 allocation.EmployeeId AS ID
	    ,[dbo].[udfGetEmployeeFullName](allocation.EmployeeId) AS Name
	FROM [dbo].[AssociateAllocation] allocation
	WHERE 
		allocation.ProjectId = @ProjectID AND allocation.IsActive = 1 AND allocation.IsPrimary = 1
END 
GO

