﻿-- ==========================================     
-- Author			:	Ramya Singamsetti          
-- Create date		:	12-01-2018          
-- Modified date	:	          
-- Modified By		:	            
-- Description		:	Close the requisition when all positions are allocated.
-- usp_CloseTalentRequisitionBasedonAllocatedPositions 1009   
-- ==========================================
CREATE PROCEDURE [dbo].[usp_CloseTalentRequisitionBasedonAllocatedPositions]
@TalentRequisitionId INT  
AS            
BEGIN        
        
 SET NOCOUNT ON;   
  
	DECLARE @CategoryId INT

	SELECT @CategoryId=[dbo].[udf_GetCategoryId]('TalentRequisition') 

	Declare @TotalPositions int,@StatusId int,@AllocatedPositions int
          
	SELECT @TotalPositions=SUM(NOOFBILLABLEPOSITIONS) + SUM(NOOFNONBILLABLEPOSITIONS) FROM REQUISITIONROLEDETAILS 
	WHERE TRID=@TalentRequisitionId
	GROUP BY TRID

	SELECT @AllocatedPositions=COUNT(*) FROM ASSOCIATEALLOCATION WHERE TRID=@TalentRequisitionId and IsActive=1

	Select @StatusId=StatusId from Status where StatusCode='Close' and CategoryID=@CategoryId

	if(@AllocatedPositions=@TotalPositions)
	UPDATE TalentRequisition set StatusId=@StatusId where TRId=@TalentRequisitionId
END

