﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	23-02-2018
-- Modified date	:	23-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Get role details by talent requisition id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRoleDetailsByTalentRequisitionID]
@ID INT
AS
BEGIN

SET NOCOUNT ON;  
	
	SELECT
		roleMaster.RoleMasterID
		,[dbo].[udf_GetRoleName](roleMaster.RoleMasterID) AS RoleName
		,roleMaster.EducationQualification
		,roleMaster.KeyResponsibilities
	FROM 
		[dbo].[RoleMaster] roleMaster
		INNER JOIN [dbo].[SGRole] sgRole
		ON roleMaster.SGRoleID = sgRole.SGRoleID
		INNER JOIN [dbo].[SGRolePrefix] prefix
		ON roleMaster.PrefixID = prefix.PrefixID
		INNER JOIN [dbo].[SGRoleSuffix] suffix
		ON roleMaster.SuffixID = suffix.SuffixID
		INNER JOIN [dbo].[RequisitionRoleDetails] requisitionRoleDetails
		ON roleMaster.RoleMasterID = requisitionRoleDetails.RoleMasterId
	WHERE requisitionRoleDetails.TRId = @ID

END

