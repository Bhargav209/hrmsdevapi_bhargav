﻿-- ================================================  
-- Author			:  Sushmitha     
-- Create date		:  27-11-2017      
-- Modified date	:  27-11-2017      
-- Modified By		:  Sushmitha      
-- Description		:  delete professional details of an employee.     
-- ================================================ 
CREATE PROCEDURE [dbo].[usp_DeleteProfessionalDetailsByID]      
@ID INT,
@ProgramType INT

AS      
BEGIN  
  
 SET NOCOUNT ON;
 
 IF @ProgramType = 3 --Cerificate

 BEGIN
 DELETE FROM [dbo].[AssociatesCertifications]
 WHERE
 ID = @ID 
 END

 ELSE

 BEGIN
 DELETE FROM [dbo].[AssociatesMemberShips]
 WHERE
 ID = @ID 

 END
 SELECT @@ROWCOUNT      
END

