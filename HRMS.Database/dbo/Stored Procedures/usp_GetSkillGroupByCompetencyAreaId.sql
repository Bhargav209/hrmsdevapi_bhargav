﻿-- ================================================================
-- Author			:  Santosh      
-- Create date		:  31-10-2017      
-- Modified date	:  31-10-2017      
-- Modified By		:  Santosh      
-- Description		:  Gets Skill Group based upon CompetencyAreaId  
-- ================================================================
CREATE PROCEDURE [dbo].[usp_GetSkillGroupByCompetencyAreaId]
@CompetencyAreaId INT    
AS      
BEGIN  
  
 SET NOCOUNT ON;
   
 SELECT 
	SkillGroupName
   ,SkillGroupId  
 FROM [dbo].[SkillGroup]
 WHERE CompetencyAreaId = @CompetencyAreaId AND IsActive = 1
END

