﻿CREATE PROCEDURE [dbo].[usp_rpt_GetProjectHistoryByEmployee]  
(  
  @EmployeeId INT  
)  
AS  
BEGIN  
       
 SET NOCOUNT ON;  
       
 SELECT  
  project.ProjectName AS ProjectName 
 ,pa.PracticeAreaCode AS Technology  
 ,CASE WHEN allocation.IsBillable = 1 THEN 'Yes' ELSE 'No' END AS Billable
 ,client.ClientName AS ClientName
 ,clientbillngrole.ClientBillingRoleName AS RoleName 
 ,allocation.EffectiveDate AS FromDate
 ,allocation.ReleaseDate AS ToDate

 FROM AssociateAllocation allocation 
 left JOIN ClientBillingRoles clientbillngrole  
 ON allocation.ClientBillingRoleId = clientbillngrole.ClientBillingRoleId
 INNER JOIN Projects project  
 ON allocation.ProjectId = project.ProjectId 
 INNER JOIN ProjectType projecttype
 ON projecttype.ProjectTypeId = project.ProjectTypeId 
 INNER JOIN Clients client  
 ON project.ClientId = client.ClientId  
 INNER JOIN PracticeArea pa  
 ON project.PracticeAreaId = pa.PracticeAreaId   
 
 WHERE allocation.EmployeeId = @EmployeeId AND project.ProjectTypeId <> 6
    
END 













