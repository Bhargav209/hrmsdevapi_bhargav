﻿-- =============================================
-- Author:		Bhavani
-- Create date: 13-12-2018 8:15:10
-- Description:	Fetches client billing roles based on the given search string
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetClientBillingRolesBySearchString]
(
@SearchString VARCHAR(20)
)	
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT 
		ClientBillingRoleId AS Id, ClientBillingRoleName AS Name
	FROM 
		ClientBillingRoles
	WHERE 
		 IsActive=1 AND ClientBillingRoleName LIKE '%' + @SearchString + '%'
    
END