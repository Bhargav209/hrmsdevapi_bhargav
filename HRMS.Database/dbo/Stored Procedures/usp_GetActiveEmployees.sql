﻿-- =================================================
-- Author			:	Santosh
-- Create date		:	06-02-2018
-- Modified date	:	06-02-2018
-- Modified By		:	Santosh
-- Description		:	Get all the active employees
-- =================================================
CREATE PROCEDURE [dbo].[usp_GetActiveEmployees]
AS
BEGIN
 SET NOCOUNT ON;
	SELECT
		 EmployeeID AS ID
		,[dbo].[udfGetEmployeeFullName](EmployeeID) AS Name
	FROM Employee
	WHERE IsActive = 1
END
