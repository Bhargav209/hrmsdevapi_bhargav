﻿    
    
-- ===============================================        
 -- Author   : Sushmitha        
 -- Create date  : 15-11-2017        
 -- Modified date : 22-11-2017        
 -- Modified By  : Kalyan.Penumutchu        
 -- Description  : Handles both Add and Update of skills by deleting exist record.        
 -- =======================================        
        
CREATE PROCEDURE [dbo].[usp_ManageAssociateSkills]         
 @ID INT,         
 @EmployeeId INT,         
 @CompetencyAreaId INT,         
 @SkillId INT,         
 @ProficiencyLevelId INT,         
 @Experience INT,         
 @LastUsed INT,         
 @IsPrimary BIT,          
 @CreatedUser VARCHAR(100),        
 @CreatedDate DATETIME,         
 @SystemInfo VARCHAR(50),        
 @SkillGroupId INT        
AS        
BEGIN        
SET NOCOUNT ON;        
        
--Update Skill - Begin        
        
IF(@ID !=0)        
 BEGIN        
       
  UPDATE         
  [dbo].[EmployeeSkills]        
  SET        
  ModifiedDate = GETDATE()        
  WHERE         
  Employeeid=@EmployeeId and SkillId = @SkillId AND ID<>@ID   
       
  IF(@@ROWCOUNT = 0)     
  BEGIN     
         UPDATE EmployeeSkills SET        
                SkillId = @SkillId,      
                ProficiencyLevelId = @ProficiencyLevelId,        
                Experience = @Experience,        
                LastUsed = @LastUsed,        
                IsPrimary =  @IsPrimary,        
                ModifiedDate = @CreatedDate,        
                ModifiedUser = @CreatedUser,        
                SystemInfo = @SystemInfo        
         WHERE ID = @ID         
         SELECT @@ROWCOUNT     
   END    
   ELSE     
       SELECT -2       
 END        
         
--Update Skill - End        
        
        
        
-- If below update is suuceess then employyee has such skill. So return -1, it indicates duplicate.        
IF(@ID = 0)        
 BEGIN        
        
  UPDATE         
   [dbo].[EmployeeSkills]        
   SET        
   ModifiedDate = GETDATE()        
   WHERE         
   EmployeeId = @EmployeeId         
  AND         
   SkillId = @SkillId        
        
  IF(@@ROWCOUNT>0)        
  BEGIN        
   SELECT -1        
  END        
  ELSE        
   BEGIN        
     INSERT INTO [dbo].[EmployeeSkills]         
     (         
      [EmployeeId],        
      [CompetencyAreaId],        
      [SkillId],        
      [ProficiencyLevelId],        
      [Experience],        
      [LastUsed],        
      [IsPrimary],        
      [SkillGroupId],        
      [CreatedUser],        
      [CreatedDate],        
      [SystemInfo],        
      [IsActive]        
     )        
     VALUES         
      (          
       @EmployeeId,        
       @CompetencyAreaId,        
       @SkillId,        
       @ProficiencyLevelId,        
       @Experience,        
       @LastUsed,        
       @IsPrimary,        
       @SkillGroupId,        
       @CreatedUser,        
       @CreatedDate,        
       @SystemInfo,        
       1        
      )        
      SELECT @@ROWCOUNT              
   END        
        
 END         
END 
