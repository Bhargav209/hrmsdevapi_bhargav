﻿-- ========================================================
-- Author			:	Santosh
-- Create date		:	06-11-2017
-- Modified date	:	06-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Creates a new Associate Resignation
-- ========================================================    
CREATE PROCEDURE [dbo].[usp_CreateAssociateResignation]
@EmployeeID INT,
@ReasonID INT,
@ReasonDescription VARCHAR(MAX),
@DateOfResignation DATETIME,
@StatusID INT,
@CreatedBy VARCHAR(100),
@CreatedDate DATETIME
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @ResignationID INT
	DECLARE @ToEmployeeID INT
	
	INSERT INTO [dbo].[AssociateResignation]
	(
		 EmployeeID
		,ReasonID
		,ReasonDescription
		,DateOfResignation
		,LastWorkingDate
		,StatusID
		,CreatedBy
		,CreatedDate
		,IsActive
	)
	VALUES
	(
		 @EmployeeID
		,@ReasonID
		,@ReasonDescription
		,@DateOfResignation
		,[dbo].[udf_NoticePeriodCalculation](@DateOfResignation)
		,@StatusID
		,@CreatedBy
		,@CreatedDate
		,1
	)

	SET @ResignationID = SCOPE_IDENTITY()
	SELECT @ToEmployeeID = ProgramManager FROM [dbo].[Employee] WHERE EmployeeID = @EmployeeID
	
	INSERT INTO [dbo].[AssociateResignationWorkFlow]
	(
		 [ResignationID]
		,[FromEmployeeID]
		,[ToEmployeeID]
		,[StatusID]
		,[CreatedBy]
		,[CreatedDate]
	)
	VALUES
	(
		 @ResignationID
		,@EmployeeID
		,@ToEmployeeID
		,@StatusID
		,@CreatedBy
		,@CreatedDate
	)

	UPDATE [dbo].[Employee] SET 
	 StatusId = @StatusID
	,ResignationDate = @DateOfResignation
	WHERE 
	EmployeeId = @EmployeeID

	SELECT 1		
END

