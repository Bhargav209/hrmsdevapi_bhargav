﻿-- ======================================      
-- Author   : Chandra  
-- Create date  : 06-05-2019  
-- Modified date : 06-05-2019  
-- Modified By  : Chandra  
-- Description  : Gets Aspects Master.   
-- ======================================     

CREATE PROCEDURE [dbo].[usp_DeleteKraAspect]          
@KraAspectId INT        
AS          
BEGIN                
                
 SET NOCOUNT ON;      
     
 declare @Kradefcount int;    
        
 select @Kradefcount = count (distinct KRADefinitionId)  from [dbo].[KRADefinition] KRADefinition       
 INNER JOIN [dbo].[KRAGroup] kraGroup      
 ON KRADefinition.KRAGroupId = kraGroup.KRAGroupId            
 INNER JOIN [dbo].[KRAStatus] kraStatus                
 ON kraGroup.KRAGroupId = kraStatus.KRAGroupId             
 INNER JOIN [dbo].[Status] [status]            
 ON kraStatus.StatusId = [status].StatusId      
 WHERE kraStatus.StatusId <> (SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA')) AND KRAAspectID=@KraAspectId   
  
 If @Kradefcount=0  
 BEGIN    
 DELETE FROM [dbo].[KRADefinition]           
 WHERE KRADefinitionId IN (  select  distinct KRADefinitionId  from [dbo].[KRADefinition] KRADefinition       
 INNER JOIN [dbo].[KRAGroup] kraGroup      
 ON KRADefinition.KRAGroupId = kraGroup.KRAGroupId            
 INNER JOIN [dbo].[KRAStatus] kraStatus                
 ON kraGroup.KRAGroupId = kraStatus.KRAGroupId             
 INNER JOIN [dbo].[Status] [status]            
 ON kraStatus.StatusId = [status].StatusId      
 WHERE kraStatus.StatusId = (SELECT [dbo].[udf_GetStatusId]('Draft', 'KRA')) AND KRAAspectID=@KraAspectId)     
 DELETE FROM [dbo].[KRAAspectMaster]           
 WHERE KRAAspectID = @KraAspectId    
 SELECT @@ROWCOUNT  
 END     
 ELSE    
  BEGIN          
  SELECT -14        
  END    
  
END 