﻿-- =====================================================
-- Author			:	Santosh
-- Create date		:	20-04-2018
-- Modified date	:	16-11-2018,24-04-2019 
-- Modified By		:	Bhavani,Mithun 
-- Description		:	Gets associates skills
 -- ====================================================      
CREATE PROCEDURE [dbo].[usp_rpt_GetAssociateSkillsReport]     

@SkillIds varchar(300)=''          
,@SkillNames varchar(1000)=''          
,@IsPrimary bit=0          
,@IsSecondary bit=0   
  
AS                 
BEGIN             
 SET NOCOUNT ON;          
 --          
 IF OBJECT_ID('tempdb..#skillSearch_skills') IS NOT NULL                    
    DROP TABLE #skillSearch_skills                    
                    
CREATE TABLE #skillSearch_skills(                    
             EmployeeID numeric(30)            
            ,primarySkill varchar(150)          
   ,secondarySkill varchar(150))                
                    
 DECLARE @ids INT;          
          
 if(@SkillIds != '')          
 Begin        
  --get skills and insert into temp table         
  Insert into #skillSearch_skills          
  select distinct          
  skSearch.EmployeeID          
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=1 then skSearch.SkillName else '' end) as primarySkill          
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=0 then skSearch.SkillName else '' end) as secondarySkill          
  from  SkillSearch skSearch   
  where          
  skSearch.SkillID IN (SELECT VALUE FROM UDF_SplitString( @SkillIds,','))          
      
 --get the required data        
   select           
    DISTINCT          
    skSearch.EmployeeId          
    ,skSearch.FirstName + ' '+ skSearch.LastName as EmployeeName          
    ,skSearch.DesignationName as Designation          
    ,STUFF((SELECT distinct ', ' + tempskillsearch.PrimarySkill          
       from #skillSearch_skills tempskillsearch          
       where tempskillsearch.EmployeeId = skSearch.EmployeeId          
       and tempskillsearch.PrimarySkill<>''FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'') PrimarySkill          
       ,STUFF((SELECT distinct ', ' + tempskillsearch.SecondarySkill          
       from #skillSearch_skills tempskillsearch          
       where tempskillsearch.EmployeeId = skSearch.EmployeeId          
       and tempskillsearch.SecondarySkill<>''          
       FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'') SecondarySkill          
      ,grd.GradeName as Grade          
   ,skSearch.Experience          
      
    from  SkillSearch skSearch     
    left join employee emp on emp.EmployeeId=skSearch.EmployeeID          
    left join Grades grd on grd.GradeId=emp.GradeId          
    left join ProjectManagers projmngr on projmngr.ProjectID=skSearch.ProjectId          
    where emp.IsActive=1           
    and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=1 and @IsSecondary=0) then 1 else Isnull(skSearch.IsSkillPrimary,0) END            
    and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=0 and @IsSecondary=1) then 0 else Isnull(skSearch.IsSkillPrimary,0) END           
    and projmngr.IsActive = 1          
    and skSearch.SkillID IN (SELECT VALUE FROM UDF_SplitString( @SkillIds,','))           
 End        
 ELSE          
 Begin        
  --get skills and insert into temp table          
  Insert into #skillSearch_skills          
  select distinct          
  skSearch.EmployeeID          
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=1 then skSearch.SkillName else '' end) as primarySkill          
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=0 then skSearch.SkillName else '' end) as secondarySkill          
  from  SkillSearch skSearch   
   
  --get the required data        
  select           
  DISTINCT          
  skSearch.EmployeeId          
  ,skSearch.FirstName + ' '+ skSearch.LastName as EmployeeName          
  ,skSearch.DesignationName as Designation          
  ,STUFF((SELECT distinct ', ' + tempskillsearch.PrimarySkill          
  from #skillSearch_skills tempskillsearch          
  where tempskillsearch.EmployeeId = skSearch.EmployeeId          
  and tempskillsearch.PrimarySkill<>''          
  FOR XML PATH(''), TYPE          
  ).value('.', 'NVARCHAR(MAX)')           
  ,1,1,'') PrimarySkill          
  ,STUFF((SELECT distinct ', ' + tempskillsearch.SecondarySkill          
  from #skillSearch_skills tempskillsearch          
  where tempskillsearch.EmployeeId = skSearch.EmployeeId          
  and tempskillsearch.SecondarySkill<>''          
  FOR XML PATH(''), TYPE          
  ).value('.', 'NVARCHAR(MAX)')           
  ,1,1,'') SecondarySkill          
  ,grd.GradeName as Grade          
  ,skSearch.Experience          
  from  SkillSearch skSearch    
  left join employee emp on emp.EmployeeId=skSearch.EmployeeID          
  left join Grades grd on grd.GradeId=emp.GradeId          
  left join ProjectManagers projmngr on projmngr.ProjectID=skSearch.ProjectId          
          
  where emp.IsActive=1           
  and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=1 and @IsSecondary=0) then 1 else Isnull(skSearch.IsSkillPrimary,0) END          
  and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=0 and @IsSecondary=1) then 0 else Isnull(skSearch.IsSkillPrimary,0) END           
  and projmngr.IsActive = 1          
  End        
END 