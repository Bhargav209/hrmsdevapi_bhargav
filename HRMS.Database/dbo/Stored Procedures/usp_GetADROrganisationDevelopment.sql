﻿-- ======================================      
-- Author   : Sabiha      
-- Create date  : 23-05-2019      
-- Modified date : 04-06-2019      
-- Modified By  :  Mithun     
-- Description  : Get ADR Organisation Development      
-- ======================================      
CREATE PROCEDURE [dbo].[usp_GetADROrganisationDevelopment] 
( @FinancialYearId INT)     
AS      
BEGIN      
 SET NOCOUNT ON;      
  SELECT      
    ADROrganisationDevelopmentID      
   ,ADROrganisationDevelopmentActivity  
    ,fy.FromYear as FromYear  
 ,fy.ToYear as ToYear  
  FROM ADROrganisationDevelopmentMaster orgMaster  
  INNER JOIN FinancialYear fy   
  ON orgMaster.FinancialYearId = fy.ID  
  WHERE orgMaster.FinancialYearId=@FinancialYearId
END 
