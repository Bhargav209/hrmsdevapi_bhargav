﻿    
-- ========================================================      
-- Author   : Prasanna       
-- Create date  : 02-01-2019      
-- Modified date : 02-01-2019,04-05-2019,04-09-2019      
-- Modified By  : Prasanna,Mithun      
-- Description  : Internal employee search by skill      
-- ========================================================         
      
CREATE PROCEDURE [dbo].[usp_GetEmployeesBySkill]        
@SkillIds varchar(300)=''        
,@SkillNames varchar(1000)=''        
,@IsPrimary bit=0        
,@IsSecondary bit=0        
,@IsCritical bit=0        
,@IsnonCritical bit=0        
,@IsBillable bit=0        
,@IsnonBillable bit=0        
AS               
BEGIN           
 SET NOCOUNT ON;        
 --        
 IF OBJECT_ID('tempdb..#skillSearch_skills') IS NOT NULL                  
    DROP TABLE #skillSearch_skills                  
                  
CREATE TABLE #skillSearch_skills(                  
             EmployeeID numeric(30)          
            ,primarySkill varchar(150)        
   ,secondarySkill varchar(150))              
                  
 DECLARE @ids INT;        
        
 if(@SkillIds != '')        
 Begin      
  --get skills and insert into temp table       
  Insert into #skillSearch_skills        
  select distinct        
  skSearch.EmployeeID        
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=1 then skSearch.SkillName else '' end) as primarySkill        
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=0 then skSearch.SkillName else '' end) as secondarySkill        
  from  SkillSearch skSearch where         
  Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=1 and @IsnonCritical=0) then 1 else Isnull(skSearch.IsCritical,0) END          
  and Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=0 and @IsnonCritical=1) then 0 else Isnull(skSearch.IsCritical,0) END         
  and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=1 and @IsnonBillable=0) then 1 else Isnull(skSearch.IsBillable,0) END          
  and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=0 and @IsnonBillable=1) then 0 else Isnull(skSearch.IsBillable,0) END         
  and skSearch.SkillID IN (SELECT VALUE FROM UDF_SplitString( @SkillIds,','))        
    
 --get the required data      
   select         
    DISTINCT        
    skSearch.EmployeeId        
    ,skSearch.FirstName + ' '+ skSearch.LastName as EmployeeName        
    ,skSearch.DesignationName as Designation        
    ,STUFF((SELECT distinct ', ' + tempskillsearch.PrimarySkill        
       from #skillSearch_skills tempskillsearch        
       where tempskillsearch.EmployeeId = skSearch.EmployeeId        
       and tempskillsearch.PrimarySkill<>''FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'') PrimarySkill        
       ,STUFF((SELECT distinct ', ' + tempskillsearch.SecondarySkill        
       from #skillSearch_skills tempskillsearch        
       where tempskillsearch.EmployeeId = skSearch.EmployeeId        
       and tempskillsearch.SecondarySkill<>''        
       FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'') SecondarySkill        
      ,grd.GradeName as Grade        
    ,skSearch.Experience        
    ,skSearch.ProjectName        
    ,skSearch.ProjectId        
    ,Isnull(skSearch.IsBillable,0) IsBillable        
    ,Isnull(skSearch.IsCritical,0) IsCritical        
    ,projLead.FirstName+' '+projLead.LastName as LeadName        
    ,rptmngr.FirstName+' '+rptmngr.LastName as ManagerName   
    ,(SELECT [Percentage] FROM allocationpercentage allocationpercentage WHERE allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID ) AS Allocationpercentage       
        
    from  SkillSearch skSearch   
    INNER join AssociateAllocation allocation ON allocation.EmployeeId = skSearch.EmployeeID AND allocation.ProjectID = skSearch.ProjectId AND allocation.ReleaseDate IS NULL  
  --left join AllocationPercentage allocationpercentage ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID       
    left join employee emp on emp.EmployeeId=skSearch.EmployeeID        
    left join Grades grd on grd.GradeId=emp.GradeId        
    left join ProjectManagers projmngr on projmngr.ProjectID=skSearch.ProjectId        
    left join employee projLead on projLead.EmployeeId=projmngr.LeadID        
    left join employee rptmngr on rptmngr.EmployeeId=projmngr.ReportingManagerID        
        
    where emp.IsActive=1         
    and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=1 and @IsSecondary=0) then 1 else Isnull(skSearch.IsSkillPrimary,0) END          
    and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=0 and @IsSecondary=1) then 0 else Isnull(skSearch.IsSkillPrimary,0) END         
    and Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=1 and @IsnonCritical=0) then 1 else Isnull(skSearch.IsCritical,0) END          
    and Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=0 and @IsnonCritical=1) then 0 else Isnull(skSearch.IsCritical,0) END         
    and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=1 and @IsnonBillable=0) then 1 else Isnull(skSearch.IsBillable,0) END          
    and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=0 and @IsnonBillable=1) then 0 else Isnull(skSearch.IsBillable,0) END         
    and projmngr.IsActive = 1        
    and skSearch.SkillID IN (SELECT VALUE FROM UDF_SplitString( @SkillIds,','))         
 End      
 ELSE        
 Begin      
  --get skills and insert into temp table        
  Insert into #skillSearch_skills        
  select distinct        
  skSearch.EmployeeID        
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=1 then skSearch.SkillName else '' end) as primarySkill        
  ,(case when Isnull(skSearch.IsSkillPrimary,0)=0 then skSearch.SkillName else '' end) as secondarySkill        
  from  SkillSearch skSearch where         
  Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=1 and @IsnonCritical=0) then 1 else Isnull(skSearch.IsCritical,0) END          
  and Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=0 and @IsnonCritical=1) then 0 else Isnull(skSearch.IsCritical,0) END         
  and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=1 and @IsnonBillable=0) then 1 else Isnull(skSearch.IsBillable,0) END          
  and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=0 and @IsnonBillable=1) then 0 else Isnull(skSearch.IsBillable,0) END         
  --get the required data      
  select         
  DISTINCT        
  skSearch.EmployeeId        
  ,skSearch.FirstName + ' '+ skSearch.LastName as EmployeeName        
  ,skSearch.DesignationName as Designation        
  ,STUFF((SELECT distinct ', ' + tempskillsearch.PrimarySkill        
  from #skillSearch_skills tempskillsearch        
  where tempskillsearch.EmployeeId = skSearch.EmployeeId        
  and tempskillsearch.PrimarySkill<>''        
  FOR XML PATH(''), TYPE        
  ).value('.', 'NVARCHAR(MAX)')         
  ,1,1,'') PrimarySkill        
  ,STUFF((SELECT distinct ', ' + tempskillsearch.SecondarySkill        
  from #skillSearch_skills tempskillsearch        
  where tempskillsearch.EmployeeId = skSearch.EmployeeId        
  and tempskillsearch.SecondarySkill<>''        
  FOR XML PATH(''), TYPE        
  ).value('.', 'NVARCHAR(MAX)')         
  ,1,1,'') SecondarySkill        
  ,grd.GradeName as Grade        
  ,skSearch.Experience        
  ,skSearch.ProjectName        
  ,skSearch.ProjectId        
  ,Isnull(skSearch.IsBillable,0) IsBillable        
  ,Isnull(skSearch.IsCritical,0) IsCritical        
  ,projLead.FirstName+' '+projLead.LastName as LeadName        
  ,rptmngr.FirstName+' '+rptmngr.LastName as ManagerName        
  ,(SELECT [Percentage] FROM allocationpercentage allocationpercentage WHERE allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID ) AS Allocationpercentage           
      
  from  SkillSearch skSearch  
  INNER join AssociateAllocation allocation ON allocation.EmployeeId = skSearch.EmployeeID AND allocation.ProjectID = skSearch.ProjectId AND allocation.ReleaseDate IS NULL  
  --left join AllocationPercentage allocationpercentage ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID            
  left join employee emp on emp.EmployeeId=skSearch.EmployeeID        
  left join Grades grd on grd.GradeId=emp.GradeId        
  left join ProjectManagers projmngr on projmngr.ProjectID=skSearch.ProjectId        
  left join employee projLead on projLead.EmployeeId=projmngr.LeadID        
  left join employee rptmngr on rptmngr.EmployeeId=projmngr.ReportingManagerID        
        
  where emp.IsActive=1         
  and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=1 and @IsSecondary=0) then 1 else Isnull(skSearch.IsSkillPrimary,0) END        
  and Isnull(skSearch.IsSkillPrimary,0) = CASE when(@IsPrimary=0 and @IsSecondary=1) then 0 else Isnull(skSearch.IsSkillPrimary,0) END         
  and Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=1 and @IsnonCritical=0) then 1 else Isnull(skSearch.IsCritical,0) END          
  and Isnull(skSearch.IsCritical,0) = CASE when(@IsCritical=0 and @IsnonCritical=1) then 0 else Isnull(skSearch.IsCritical,0) END         
  and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=1 and @IsnonBillable=0) then 1 else Isnull(skSearch.IsBillable,0) END          
  and Isnull(skSearch.IsBillable,0) = CASE when(@IsBillable=0 and @IsnonBillable=1) then 0 else Isnull(skSearch.IsBillable,0) END         
  and projmngr.IsActive = 1        
  End      
END 