﻿-- ============================================          
-- Author   : Santosh          
-- Create date  : 30-01-2018          
-- Modified date : 30-01-2018              
-- Modified By  : Santosh          
-- Description  : Create Appreciation    
-- ============================================      
CREATE PROCEDURE [dbo].[usp_CreateAppreciation]     
(      
  @ADRCycleID   INT    
 ,@FinancialYearID  INT    
 ,@FromEmployeeID  INT    
 ,@ToEmployeeID   INT    
 ,@AppreciationTypeID INT    
 ,@AppreciationMessage VARCHAR(MAX)    
 ,@AppreciationDate  DATETIME    
 ,@DateCreated   DATETIME    
 ,@CreatedUser   VARCHAR(150)    
 ,@SystemInfo   VARCHAR(50)    
)      
AS        
BEGIN      
 SET NOCOUNT ON;      
 INSERT INTO [dbo].[AssociateAppreciation]     
  (    
    ADRCycleID    
   ,FinancialYearID    
   ,FromEmployeeID    
   ,ToEmployeeID    
   ,AppreciationTypeID    
   ,AppreciationMessage    
   ,AppreciationDate    
   ,IsActive  
   ,DateCreated    
   ,CreatedUser    
   ,SystemInfo    
  )    
 VALUES    
 (    
    @ADRCycleID    
   ,@FinancialYearID    
   ,@FromEmployeeID    
   ,@ToEmployeeID    
   ,@AppreciationTypeID    
   ,@AppreciationMessage    
   ,@AppreciationDate   
   ,1   
   ,@DateCreated    
   ,@CreatedUser    
   ,@SystemInfo    
  )    
 SELECT @@ROWCOUNT      
END

