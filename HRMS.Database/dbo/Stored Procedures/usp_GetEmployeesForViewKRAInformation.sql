﻿-- ============================================             
-- Author   : Chandra                    
-- Create date  : 22-04-2018                    
-- Modified date : 22-04-2018                    
-- Modified By  : Chandra                    
-- Description  : Gets Employee details by project id          
-- ============================================        
        
CREATE PROCEDURE [dbo].[usp_GetEmployeesForViewKRAInformation]        
(        
  @ProjectID INT        
 ,@DepartmentID INT        
 ,@FinancialYear INT    
    
)        
AS               
BEGIN          
 SET NOCOUNT ON;        
       
   IF (@ProjectID > 0)        
   BEGIN        
    SELECT        
     allocation.EmployeeId AS Id        
     ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName        
     ,(SELECT KRATitle FROM KRAGROUP WHERE KRAGroupID=AssociateKRAMapper.KRAGroupID) AS RoleName      
     , AssociateKRAMapper.KRAGroupID As KRAGroupId      
    FROM         
     [dbo].[AssociateAllocation] allocation        
     INNER JOIN [dbo].[Employee] employee        
     ON allocation.EmployeeId = employee.EmployeeId        
     left outer JOIN [dbo].[AssociateKRAMapper] AssociateKRAMapper        
     ON AssociateKRAMapper.EmployeeID = employee.EmployeeId AND AssociateKRAMapper.FinancialYearID= @FinancialYear       
    WHERE         
     allocation.ProjectID = @ProjectID AND employee.DepartmentId = @DepartmentID AND allocation.ReleaseDate IS NULL  
         
   END        
   ELSE        
   BEGIN        
    SELECT         
     employee.EmployeeId AS Id        
     ,[dbo].[udfGetEmployeeFullName](employee.EmployeeId) AS AssociateName        
     ,(SELECT KRATitle FROM KRAGROUP WHERE KRAGroupID=AssociateKRAMapper.KRAGroupID) AS RoleName    
     , AssociateKRAMapper.KRAGroupID As KRAGroupId         
     FROM         
     [dbo].[Employee] employee        
     left outer JOIN [dbo].[AssociateKRAMapper] AssociateKRAMapper        
     ON AssociateKRAMapper.EmployeeID = employee.EmployeeId AND AssociateKRAMapper.FinancialYearID= @FinancialYear     
                 WHERE         
     employee.DepartmentID =@DepartmentID AND employee.IsActive = 1      
   END        
END 