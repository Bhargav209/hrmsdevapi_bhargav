﻿-- ========================================================        
-- Author   : Mithun        
-- Create date  : 28-05-2019        
-- Modified date : 29-05-2019 ,13-06-2019       
-- Modified By  : Mithun, Sabiha        
-- Description  : Create ADR Section        
-- ========================================================         
        
CREATE PROCEDURE [dbo].[usp_CreateADRSection]        
(          
@ADRSectionName VARCHAR(150),     
@ADRMeasurementAreaId INT,
@DepartmentId INT,       
@CreatedDate DATETIME,          
@CreatedUser VARCHAR(150),          
@SystemInfo VARCHAR(50)          
)          
AS        
BEGIN        
        
 SET NOCOUNT ON;    
   
 DECLARE @FinancialYearId INT   
  SELECT @FinancialYearId = ID from FinancialYear where  IsActive = 1;  
        
         
 INSERT INTO         
 [dbo].[ADRSection]          
 (ADRSectionName, ADRMeasurementAreaId, FinancialYearId, CreatedDate, CreatedUser, SystemInfo,DepartmentId)          
 VALUES          
 (@ADRSectionName, @ADRMeasurementAreaId, @FinancialYearId, @CreatedDate, @CreatedUser, @SystemInfo,@DepartmentId)          
          
 SELECT @@ROWCOUNT        
        
END 