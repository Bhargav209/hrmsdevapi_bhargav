﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	28-03-2018
-- Modified date	:	28-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Adds Notification type.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateNotificationConfiguration]
( 
@NotificationTypeID INT,
@CategoryId INT,
@EmailFrom NVARCHAR(MAX),
@EmailTo NVARCHAR(MAX),
@EmailCC NVARCHAR(MAX),
@Subject NVARCHAR(150),
@Body NVARCHAR(MAX), 
@CreatedUser VARCHAR(100),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON;  

 DECLARE @CategoryExists INT = 0;
 SELECT @CategoryExists = COUNT(CategoryID) FROM CategoryMaster WHERE CategoryID=@CategoryId

 IF @CategoryExists <> 0
  BEGIN
		 INSERT INTO 
			[dbo].[NotificationConfiguration]  
			(NotificationTypeID, CategoryId, EmailFrom, EmailTo, EmailCC, EmailSubject, EmailContent, CreatedBy, SystemInfo)  
		 VALUES  
			(@NotificationTypeID, @CategoryId,  @EmailFrom, @EmailTo, @EmailCC, @Subject, @Body, @CreatedUser, @SystemInfo) 
		 SELECT @@ROWCOUNT
  END
 ELSE
 select -10

END

