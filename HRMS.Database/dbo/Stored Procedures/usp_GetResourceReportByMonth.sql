﻿-- ================================================      
-- Author   :  Basha shaik        
-- Create date  :  19-11-2018          
-- Modified date :  19-11-2018          
-- Modified By  :  Basha shaik     
-- Description  :  Get ResourceReport By Month.         
-- ===========================================    
CREATE PROCEDURE [dbo].[usp_GetResourceReportByMonth] --1,5,2017     
(            
@frommonth int            
,@tomonth int            
,@year int            
          
)            
as             
 BEGIN            
            
DECLARE  @startDate DATETIME ,@endDate DATETIME                   
SET @startDate = DATEFROMPARTS (@year, @frommonth, 1);            
SET @endDate =EOMONTH( DATEFROMPARTS (@year, @tomonth, 1));            
            
          
SET NOCOUNT ON          
IF OBJECT_ID('tempdb..#employee_utilization') IS NOT NULL          
    DROP TABLE #employee_utilization          
          
CREATE TABLE #employee_utilization(          
             EmployeeId numeric(10)  
            ,EmployeeCode VARCHAR(100)                
            ,EmployeeName VARCHAR(100)  
            ,JoinDate  DATETIME             
            ,ProjectName VARCHAR(100)                        
            ,AllocationMonth VARCHAR(20)          
            ,UtilizedPercentage numeric(3)          
            ,MonthCount numeric(3)
			,AvgMonths VARCHAR(MAX)
			,ReleasedDate  DATETIME )          
          
DECLARE           
					  @EmployeeId numeric(10)      
					 ,@EmployeeCode VARCHAR(100)         
                     ,@EmployeeName VARCHAR(100)   
                     ,@JoinDate  DATETIME            
                     ,@ProjectName VARCHAR(100)          
                     ,@EffectiveDate DATETIME          
                     ,@ReleaseDate DATETIME          
                     ,@AllocationPercentage numeric(3)          
                     ,@BeginDate DATETIME          
                     ,@TotalDays numeric(2)          
                     ,@UtilizedDays numeric(2)          
                     ,@UtilizedPercentage numeric(3)          
                     ,@months AS VARCHAR(MAX) 
					 ,@monthColumns AS VARCHAR(MAX)          
                     ,@monthCount AS INT          
                     ,@avgMonths AS Varchar(MAX)
					 ,@empMonths AS Varchar(MAX) 
					 ,@ReleasedDate AS DATETIME          
          
SET @BeginDate = @startDate          
          
SET @avgMonths=''
SET @empMonths=''     
    
WHILE @BeginDate < @endDate          
BEGIN    
SET @monthCount=0      
if(@monthColumns IS NULL OR @monthColumns = '')          
SET @monthColumns = 'ISNULL(Convert(VARCHAR,' + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + '),''N/A'') AS ' + (CONVERT(varchar(12),DATENAME(month,@BeginDate)))-- + '' + convert(varchar,year(@BeginDate)))          
ELSE          
SET @monthColumns = @monthColumns + ', ISNULL(Convert(VARCHAR,' + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + '),''N/A'') AS ' + (CONVERT(varchar(12),DATENAME(month,@BeginDate)))-- + '' + convert(varchar,year(@BeginDate)))          

if(@months IS NULL OR @months = '')          
SET @months =  (CONVERT(varchar(12),DATENAME(month,@BeginDate)))        
ELSE          
SET @months = @months + ', ' + (CONVERT(varchar(12),DATENAME(month,@BeginDate)))          

          
if(@avgMonths IS NULL OR @avgMonths = '')          
SET @avgMonths =  'IIF(' + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + ' IS NULL, 0,'  + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + ')'-- + '' + convert(varchar,year(@BeginDate)))          
ELSE          
SET @avgMonths = @avgMonths + ' +  IIF(' + (CONVERT(varchar(12),DATENAME(month,@BeginDate)))+ ' IS NULL, 0,' + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + ')'-- + '' + convert(varchar,year(@BeginDate)))          

if(@empMonths IS NULL OR @empMonths = '')          
SET @empMonths =  'IIF(' + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + ' IS NULL, 0, 1)'           
ELSE          
SET @empMonths = @empMonths + ' +  IIF(' + (CONVERT(varchar(12),DATENAME(month,@BeginDate)))+ ' IS NULL, 0,1)'             
          
SET @BeginDate = DATEADD(MONTH,1,@BeginDate)    

SET @monthCount=@monthCount+1          
END                     
          
DECLARE CurUtilization CURSOR FOR           
SELECT          
       emp.EmployeeId,    
       emp.EmployeeCode,        
       (emp.FirstName+' '+emp.LastName)as EmployeeName,          
       proj.projectName,    
       emp.JoinDate,
       alloca.EffectiveDate,          
       alloca.ReleaseDate,          
       allocpercent.Percentage AllocationPercentage,
	   (SELECT MAX(ISNULL(ast.ReleaseDate,'01/01/2100')) FROM AssociateAllocation ast WHERE ast.EmployeeId = emp.EmployeeId ) ReleasedDate                    
       from AssociateAllocation alloca          
       inner join Employee emp on alloca.EmployeeId=emp.EmployeeId           
       left join AllocationPercentage allocpercent on alloca.AllocationPercentage=allocpercent.AllocationPercentageID           
       inner join Projects proj on alloca.ProjectId=proj.ProjectId             
       WHERE 
	   --proj.ProjectName NOT LIKE '%Talent Pool%' AND          
       alloca.EffectiveDate <= @endDate and (alloca.ReleaseDate IS NULL OR alloca.ReleaseDate >= @startDate) --AND emp.EmployeeId = 13  --AND proj.ProjectName not like '%TalentPool%'          
       order by emp.EmployeeId,alloca.EffectiveDate,proj.projectName          
          
OPEN CurUtilization            
          
FETCH NEXT FROM CurUtilization INTO @EmployeeId,@EmployeeCode,@EmployeeName,@ProjectName,@JoinDate,@EffectiveDate, @ReleaseDate, @AllocationPercentage, @ReleasedDate          
WHILE @@FETCH_STATUS = 0             
BEGIN          
SET @BeginDate = @startDate 
SET @monthCount = 0   
--SET @avgMonths='' 
WHILE @BeginDate < @endDate          
BEGIN            
if(@EffectiveDate <= EOMONTH(@BeginDate) AND (@ReleaseDate IS NULL OR @ReleaseDate >= @BeginDate) )          
BEGIN
SET @monthCount = @monthCount + 1
--if(@avgMonths IS NULL OR @avgMonths = '')          
--SET @avgMonths = 'IIF(' + (CONVERT(varchar(12),DATENAME(month,@BeginDate))) + ', -1, 0)'-- + '' + convert(varchar,year(@BeginDate)))          
--ELSE          
--SET @avgMonths = @avgMonths + ' +  IIF(' + (CONVERT(varchar(12),DATENAME(month,@BeginDate)))+ ', -1, 0)'-- + '' + convert(varchar,year(@BeginDate))) 
END   
SET @BeginDate = DATEADD(MONTH,1,@BeginDate)        
END

SET @BeginDate = @startDate          
WHILE @BeginDate < @endDate          
BEGIN          
SET @TotalDays = DAY(EOMONTH(@BeginDate));          
SET @UtilizedDays = -1;          
SET @UtilizedPercentage = 0;     
          
if(@EffectiveDate <= EOMONTH(@BeginDate) AND (@ReleaseDate IS NULL OR @ReleaseDate >= @BeginDate) )          
BEGIN          
          
if(MONTH(@BeginDate) = MONTH(@EffectiveDate) and YEAR(@BeginDate) = YEAR(@EffectiveDate))          
BEGIN          
if(@ReleaseDate IS NULL OR @ReleaseDate >= EOMONTH(@BeginDate))          
SET @UtilizedDays = @TotalDays - DAY(@EffectiveDate) + 1;          
ELSE if(@ReleaseDate < EOMONTH(@BeginDate))          
SET @UtilizedDays =  DAY(@ReleaseDate) - DAY(@EffectiveDate) + 1;          
END          
ELSE          
BEGIN          
if(@ReleaseDate IS NULL OR @ReleaseDate >= EOMONTH(@BeginDate))          
SET @UtilizedDays = @TotalDays;          
ELSE if(@ReleaseDate < EOMONTH(@BeginDate))          
SET @UtilizedDays =  DAY(@ReleaseDate);          
END
END 
ELSE
SET @UtilizedPercentage  = NULL    

      
IF  (@UtilizedDays >= 0)         
SET @UtilizedPercentage  = (@UtilizedDays/@TotalDays)*@AllocationPercentage; 
ELSE
SET @UtilizedPercentage  = NULL         

IF(@JoinDate > @BeginDate and (MONTH(@JoinDate) <> MONTH(@BeginDate) OR YEAR(@JoinDate) <> YEAR(@BeginDate)))
INSERT INTO #employee_utilization(EmployeeId,EmployeeCode,EmployeeName ,ProjectName,JoinDate,ReleasedDate,AllocationMonth ,UtilizedPercentage,MonthCount,AvgMonths) VALUES(@EmployeeId,@EmployeeCode,@EmployeeName,@ProjectName,@JoinDate,@ReleasedDate,(CONVERT(varchar(12),DATENAME(month,@BeginDate)))-- + '' + convert(varchar,year(@BeginDate)))    
, NULL,@monthCount,@avgMonths)
ELSE IF(@ReleasedDate < @BeginDate AND (MONTH(@JoinDate) <> MONTH(@BeginDate) OR YEAR(@JoinDate) <> YEAR(@BeginDate)))
INSERT INTO #employee_utilization(EmployeeId,EmployeeCode,EmployeeName ,ProjectName,JoinDate,ReleasedDate,AllocationMonth ,UtilizedPercentage,MonthCount,AvgMonths) VALUES(@EmployeeId,@EmployeeCode,@EmployeeName,@ProjectName,@JoinDate,@ReleasedDate,(CONVERT(varchar(12),DATENAME(month,@BeginDate)))-- + '' + convert(varchar,year(@BeginDate)))    
, NULL,@monthCount,@avgMonths)
ELSE IF(@ProjectName LIKE '%Talent Pool%')        
INSERT INTO #employee_utilization(EmployeeId,EmployeeCode,EmployeeName ,ProjectName,JoinDate,ReleasedDate,AllocationMonth ,UtilizedPercentage,MonthCount,AvgMonths) VALUES(@EmployeeId,@EmployeeCode,@EmployeeName,@ProjectName,@JoinDate,@ReleasedDate,(CONVERT(varchar(12),DATENAME(month,@BeginDate)))-- + '' + convert(varchar,year(@BeginDate)))    
, 0,@monthCount,@avgMonths)  
ELSE
INSERT INTO #employee_utilization(EmployeeId,EmployeeCode,EmployeeName ,ProjectName,JoinDate,ReleasedDate,AllocationMonth ,UtilizedPercentage,MonthCount,AvgMonths) VALUES(@EmployeeId,@EmployeeCode,@EmployeeName,@ProjectName,@JoinDate,@ReleasedDate,(CONVERT(varchar(12),DATENAME(month,@BeginDate)))-- + '' + convert(varchar,year(@BeginDate)))    
, @UtilizedPercentage,@monthCount,@avgMonths)       
SET @BeginDate = DATEADD(MONTH,1,@BeginDate)          
END
          
          
FETCH NEXT FROM CurUtilization INTO @EmployeeId,@EmployeeCode,@EmployeeName,@ProjectName,@JoinDate, @EffectiveDate, @ReleaseDate, @AllocationPercentage, @ReleasedDate          
END              
          
CLOSE CurUtilization            
DEALLOCATE CurUtilization           

--SELECT EmployeeId, EmployeeCode,EmployeeName,JoinDate,MAX(MonthCount) MonthCount,  AllocationMonth, SUM(UtilizedPercentage) UtilizedPercentage, @avgMonths,@empMonths FROM #employee_utilization GROUP BY EmployeeId, EmployeeCode,EmployeeName,JoinDate,AllocationMonth

DECLARE @DynamicPIVOT AS VARCHAR(MAX)          
SELECT @DynamicPIVOT = 'SELECT EmployeeId, EmployeeCode,EmployeeName,convert(date,JoinDate) JoinDate,(CASE WHEN convert(date,ReleasedDate) =''2100-01-01'' THEN NULL ELSE convert(date,ReleasedDate) END )ReleaseDate,' + @monthColumns +',Floor(('+@avgMonths+')/(' + @empMonths + ')) [AverageUtilizationpercentage] FROM (          
       SELECT EmployeeId, EmployeeCode,EmployeeName,JoinDate,ReleasedDate ,Max(MonthCount) MonthCount,  AllocationMonth, SUM(UtilizedPercentage) UtilizedPercentage FROM #employee_utilization  GROUP BY EmployeeId, EmployeeCode,EmployeeName,JoinDate,ReleasedDate,AllocationMonth        
          
) emps          
PIVOT (          
    MAX(UtilizedPercentage) FOR AllocationMonth IN (' + @months  + ')          
) Result;'          
          
EXEC (@DynamicPIVOT) 
DROP TABLE #employee_utilization
--print  @DynamicPIVOT        
          
END
