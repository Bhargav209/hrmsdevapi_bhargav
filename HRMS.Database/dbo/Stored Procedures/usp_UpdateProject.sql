﻿--exec usp_UpdateProject 405,'TestP112',2,17,2,24,152,5,'2019-02-07 14:25:07.910','2019-12-07 14:25:07.910','bhavani.chintamadaka@senecaglobal.com','2019-02-07 14:25:07.910','192.168.2.236',1,'Program Manager'  
  
CREATE PROCEDURE [dbo].[usp_UpdateProject]          
 (          
 @ProjectId INT,            
  @ProjectName VARCHAR(100),          
  @ProjectTypeId INT,            
  @ClientId INT,           
  @DomainId INT,        
  @ProjectStateId INT,          
  @ProgramManagerId INT,           
  @PracticeAreaId INT,        
  @ActualStartDate DATETIME,          
  @ActualEndDate DATETIME,          
  @ModifiedUser  VARCHAR(100),          
  @ModifiedDate  DATETIME,          
  @SystemInfo VARCHAR(50),          
  @DepartmentId INT,          
  @UserRole VARCHAR(20)          
 )                       
AS                          
BEGIN                      
                     
 SET NOCOUNT ON;           
          
 DECLARE @AllocationCount INT          
 DECLARE @RoleId INT        
 DECLARE @CategoryId INT        
 DECLARE @StatusId INT         
         
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'        
 SELECT @StatusId=StatusId FROM Status WHERE StatusCode = 'Closed' and CategoryID = @CategoryId        
      
BEGIN TRY      
IF(@ProjectStateId=@StatusId AND @ActualEndDate IS NOT NULL)   --Ensures that It is a closed block        
 BEGIN        
        
  SELECT @AllocationCount = COUNT(*) FROM AssociateAllocation where ProjectId = @ProjectId AND ReleaseDate IS NULL          
   IF (@AllocationCount > 0)   --has allocations       
   BEGIN      
       SELECT -2;      
   END       
   ELSE --No Allocation So close the project and deactivate program managers of a given project      
   BEGIN      
   UPDATE Projects SET         
           ProjectStateId = @ProjectStateId         
           ,ActualEndDate = @ActualEndDate          
           ,ModifiedUser =@ModifiedUser          
           ,ModifiedDate =@ModifiedDate          
           ,SystemInfo =@SystemInfo                
         WHERE ProjectId = @ProjectId         
            
  SELECT @@ROWCOUNT    
    --update project manager table                       
       IF(@@ROWCOUNT > 0)          
        BEGIN                 
          IF EXISTS (SELECT 1 FROM ProjectManagers where ProjectID = @ProjectId AND IsActive = 1)                 
          UPDATE ProjectManagers SET                    
                 IsActive = 0          
                ,ModifiedBy = @ModifiedUser          
                ,ModifiedDate = @ModifiedDate          
          WHERE ProjectID = @ProjectId  AND IsActive = 1          
        END          
   END      
END      
ELSE -- Other than Closed      
  BEGIN          
     SELECT @RoleId = RoleId from roles WHERE UPPER(RoleName) = UPPER(@UserRole)        
       IF(@RoleId = 5 OR @RoleId=3)  -- 5 is program manager.ensure that roles table have always 5 as program manager          
          UPDATE Projects SET           
            ProjectName=@ProjectName          
           ,ProjectStateId = @ProjectStateId          
           ,ActualStartDate=@ActualStartDate          
           ,ActualEndDate = @ActualEndDate          
           ,ClientId=@ClientId         
           ,DomainId=@DomainId           
           ,PracticeAreaId=@PracticeAreaId          
           ,ProjectTypeId = @ProjectTypeId                                                
           ,DepartmentId =@DepartmentId          
           ,ModifiedUser =@ModifiedUser          
           ,ModifiedDate =@ModifiedDate          
           ,SystemInfo =@SystemInfo                
         WHERE ProjectId = @ProjectId     
   SELECT @@ROWCOUNT        
            
       --update project manager table                       
       IF(@@ROWCOUNT > 0 AND @RoleId=3)          
        BEGIN                 
          IF EXISTS (SELECT 1 FROM ProjectManagers where ProjectID = @ProjectId AND IsActive = 1)                 
          UPDATE ProjectManagers SET          
                 ProgramManagerID = @ProgramManagerId          
                ,ModifiedBy = @ModifiedUser          
                ,ModifiedDate = @ModifiedDate          
          WHERE ProjectID = @ProjectId  AND IsActive = 1          
        END       
    SELECT @@ROWCOUNT       
     END      
 END TRY        
 BEGIN CATCH      
     SELECT ERROR_NUMBER( ) --2627 is Violation in unique index  ;      
 END CATCH      
 END 