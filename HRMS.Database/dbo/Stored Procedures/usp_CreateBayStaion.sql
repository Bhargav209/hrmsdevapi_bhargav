﻿-- ========================================================    
-- Author   : Chandra    
-- Create date  : 05-15-2019    
-- Modified date :    
-- Modified By  :     
-- Description  : Adds BayStation.    
-- ========================================================     
    
CREATE PROCEDURE [dbo].[usp_CreateBayStaion]    
(      
@WorkStationId INT,    
@BayId INT,    
@CreatedUser VARCHAR(150),      
@SystemInfo VARCHAR(50)      
)      
AS    
BEGIN    
    
 SET NOCOUNT ON;      
    
  INSERT INTO     
 [dbo].[WorkStation]      
 (WorkstationId,BayId, CreatedUser, SystemInfo)      
 VALUES      
 (@WorkStationId,@BayId, @CreatedUser, @SystemInfo)      
      
 SELECT @@ROWCOUNT    
    
END 