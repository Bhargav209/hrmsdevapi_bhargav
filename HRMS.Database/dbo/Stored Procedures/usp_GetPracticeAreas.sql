﻿CREATE PROCEDURE [dbo].[usp_GetPracticeAreas]  
                
AS                  
BEGIN              
             
 SET NOCOUNT ON;   
 SELECT   
  PracticeAreaId AS PracticeAreaId  
 ,PracticeAreaCode AS PracticeAreaCode  
 ,PracticeAreaDescription AS PracticeAreaDescription  
 FROM PracticeArea  
 WHERE IsActive = 1   
  ORDER BY PracticeAreaCode
END    
