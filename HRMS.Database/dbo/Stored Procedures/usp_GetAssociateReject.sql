﻿-- ==========================================================  
-- Author   : Basha              
-- Create date  : 20-11-2017              
-- Modified date : 20-11-2017              
-- Modified By  : Basha              
-- Description  : Gets associates  Reject  
-- ==========================================================  
CREATE PROCEDURE [dbo].[usp_GetAssociateReject]  
@EmployeeID INT  
AS              
BEGIN          
      
 SET NOCOUNT ON;  
        
 SELECT * FROM [dbo].[vw_AssociateReject]   
 WHERE empID = @EmployeeID  
END
