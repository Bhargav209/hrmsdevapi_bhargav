﻿-- =======================================     
-- Author			:	Basha          
-- Create date		:	20-07-2018    
-- Modified date	:	20-07-2018            
-- Modified By		:	Basha          
-- Description		:	Update KRA MeasurementType       
-- =======================================

CREATE PROCEDURE [dbo].[usp_UpdateKRAMeasurementType]
@KraMeasurementTypeId INT ,
@KRAMeasurementType varchar(30)

AS
 BEGIN
SET NOCOUNT ON; 

DECLARE  
@KRAMeasurementTypeExists varchar (30),
@KRAMeasurementTypeIdDepends INT,
@DraftStatusId INT,
@CategoryId INT,
@undraftcount INT,
@KRAMeasuretype varchar(30)

SELECT @KRAMeasuretype= RTRIM(LTRIM(@KRAMeasurementType))

IF(@KRAMeasuretype <>'')
	BEGIN

	select @CategoryId = CategoryID from CategoryMaster where CategoryName='KRA'

	--checking if KRAMeasurementType already exists or not 
	 SELECT @KRAMeasurementTypeExists = COUNT(KRAMeasurementType) FROM KRAMeasurementType WHERE KRAMeasurementType=@KRAMeasuretype
	 IF (@KRAMeasurementTypeExists > 0)
	   SELECT -1
	   ELSE
		   BEGIN
				   --checking if KRAMeasurementType have any dependency
				 SELECT @KRAMeasurementTypeIdDepends = COUNT(KRAMeasurementTypeID) FROM KRADefinition WHERE KRAMeasurementTypeID=@KraMeasurementTypeId
				 IF (@KRAMeasurementTypeIdDepends > 0)
				  SELECT 9
				  ELSE
				  BEGIN

					  --Able to update only if KRA in  Draft state
					SELECT  @undraftcount = Count(*) from KRAMeasurementType kmt
						 Inner join KRADefinition kdef on kmt.KRAMeasurementTypeID=kdef.KRAMeasurementTypeID
						 inner join KRAStatus kstatus on kdef.KRAGroupId=kstatus.KRAGroupId where kmt.KRAMeasurementTypeID=@KraMeasurementTypeId and kstatus.StatusId
						 in( SELECT  StatusId FROM [dbo].[Status] WHERE StatusCode <> 'Draft' AND CategoryID = @CategoryId)

					 IF(@undraftcount = 0)
						  BEGIN

								UPDATE 
									[dbo].[KRAMeasurementType]  
								SET 
									KRAMeasurementType = @KRAMeasuretype
								WHERE 
									KraMeasurementTypeId = @KraMeasurementTypeId
								 SELECT @@ROWCOUNT
							 END
						ELSE
						SELECT -11
					END
		END 
	END
ELSE
	SELECT -11

END
