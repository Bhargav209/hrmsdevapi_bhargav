﻿-- ======================================    
-- Author			:	Santosh       
-- Create date		:	08-01-2017          
-- Modified date	:	12-01-2017    
-- Modified By		:	Santosh    
-- Description		:	Update KRASet    
-- ======================================    
CREATE PROCEDURE [dbo].[usp_UpdateKRASet]
(    
@KRASetID INT,       
@KRAAspectMetric VARCHAR(500),    
@KRAAspectTarget VARCHAR(250),     
@DateModified DATETIME,    
@ModifiedUser VARCHAR(150),    
@SystemInfo VARCHAR(50)    
)    
AS         
BEGIN    
    
 SET NOCOUNT ON;
 /*
 UPDATE [dbo].[KRASet] SET
  KRAAspectMetric       = @KRAAspectMetric
 ,KRAAspectTarget	    = @KRAAspectTarget
 ,DateModified		    = @DateModified
 ,ModifiedUser		    = @ModifiedUser
 ,SystemInfo            = @SystemInfo
 WHERE KRASetID = @KRASetID
    */
  SELECT @@ROWCOUNT    
END

