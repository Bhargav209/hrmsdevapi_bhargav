﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	30-03-2018
-- Modified date	:	30-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets list of notification types.
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_GetEmployeeWorkEmails]
(
@SearchString VARCHAR(20)
)
AS         
BEGIN
	SET NOCOUNT ON;    

	SELECT
		EmailAddress AS EmailID
	FROM
		[dbo].[Users]
	WHERE
		EmailAddress LIKE '%' + @SearchString + '%' AND IsActive = 1

END

