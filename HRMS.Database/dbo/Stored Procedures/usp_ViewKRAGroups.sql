﻿-- ========================================================================
-- Author			:	Santosh
-- Create date		:	18-04-2018  
-- Modified date	:	18-04-2018
-- Modified By		:	Santosh
-- Description		:	View KRA Groups by DepartmentID and FinancialYearID
-- =========================================================================
CREATE PROCEDURE [dbo].[usp_ViewKRAGroups]
(
	@DepartmentID INT
   ,@FinancialYearID INT
)
AS                    
BEGIN
	
	SET NOCOUNT ON;      
                   
	SELECT       
		kraGroup.KRAGroupId   
		,kraGroup.KRATitle
		,[status].StatusCode
	FROM   
	[dbo].[KRAGroup] kraGroup      
	INNER JOIN [dbo].[KRAStatus] kraStatus      
	ON kraGroup.KRAGroupId = kraStatus.KRAGroupId
	INNER JOIN [dbo].[Status] [status]
	on kraStatus.StatusId = [status].StatusId
	WHERE kraStatus.FinancialYearId = @FinancialYearID AND kraGroup.DepartmentId = @DepartmentID

 END
