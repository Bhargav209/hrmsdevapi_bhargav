﻿CREATE TYPE [dbo].[ScaleDetailsType] AS TABLE (
    [Id]          INT           NOT NULL,
    [Description] VARCHAR (150) NOT NULL);

