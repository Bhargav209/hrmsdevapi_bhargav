﻿using AP.Utility;
using System;
using System.Configuration;
using System.Net.Mail;

namespace AP.Notification
{
    public class NotificationManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationDetail"></param>
        public static void SendEmail(NotificationDetail notificationDetail)
        {

            MailMessage objMailMessage = new MailMessage();
            SmtpClient objSmtpClient = new SmtpClient();
            try
            {
                string smtpClientaddress = Convert.ToString(ConfigurationManager.AppSettings["SMTPClient"]);

                objMailMessage.From = new MailAddress(notificationDetail.FromEmail);

                //objMailMessage.To.Add(new MailAddress(ToAddress));
                string[] toAddresses = !string.IsNullOrEmpty(notificationDetail.ToEmail) ? notificationDetail.ToEmail.Split(';') : new string[] { };
                if (toAddresses.Length > 0)
                {
                    foreach (string address in toAddresses)
                    {
                        if (address != string.Empty)
                            objMailMessage.To.Add(address);
                    }
                }

                //objMailMessage.CC.Add(new MailAddress(ccAddress));
                notificationDetail.CcEmail = notificationDetail.CcEmail.TrimEnd(';');
                string[] addresses = !string.IsNullOrEmpty(notificationDetail.CcEmail) ? notificationDetail.CcEmail.Split(';') : new string[] { };
                if (addresses.Length > 0)
                {
                    foreach (string address in addresses)
                    {
                        if (address != string.Empty)
                            objMailMessage.CC.Add(address);
                    }
                }

                objMailMessage.Subject = notificationDetail.Subject;
                objMailMessage.IsBodyHtml = true;
                objMailMessage.Body = notificationDetail.EmailBody;

                objSmtpClient = new SmtpClient(smtpClientaddress);
                objSmtpClient.UseDefaultCredentials = false;
                objSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendMail"]))
                    objSmtpClient.Send(objMailMessage);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to send notification.");
            }
            finally
            {
                objMailMessage = null;
                objSmtpClient = null;
            }
        }
    }
}