﻿-- =====================================================
-- Author			:	Santosh
-- Create date		:	20-04-2018
-- Modified date	:	16-11-2018
-- Modified By		:	Bhavani
-- Description		:	Gets associates skills
 -- ====================================================      
CREATE PROCEDURE [dbo].[usp_rpt_GetAssociateSkillsReport] 
(
	 @CompetencyAreaID INT 
	,@SkillGroupID INT 
	,@SkillID INT  
	
)
AS          
BEGIN
	
	SET NOCOUNT ON;
DECLARE	@SQL NVARCHAR(MAX)

	SET @SQL= 'SELECT   
		 employee.EmployeeCode
		,dbo.udfGetEmployeeFullName(es.EmployeeId) AS EmployeeName
		,ca.CompetencyAreaCode AS CompetencyArea  
		,sg.SkillGroupName AS SkillGroup    
		,skill.SkillName AS Skill     
		,ISNULL(es.LastUsed,0) AS [LastUsed] 
		,ISNULL(es.IsPrimary,0) as IsPrimary
		,pl.ProficiencyLevelCode  AS ProficiencyLevel  
	FROM EmployeeSkills es    
	INNER JOIN CompetencyArea ca  
	ON es.CompetencyAreaId = ca.CompetencyAreaId
	INNER JOIN SkillGroup sg      
	ON es.SkillGroupID = sg.SkillGroupId      
	INNER JOIN Skills skill    
	ON es.SkillId = skill.SkillId    
	INNER JOIN ProficiencyLevel pl    
	ON es.ProficiencyLevelId = pl.ProficiencyLevelId
	INNER JOIN Employee employee    
	ON es.EmployeeId = employee.EmployeeId    
	WHERE employee.IsActive=1 '

IF(@CompetencyAreaID > 0 AND @SkillGroupID > 0)
SET @SQL= @SQL + 'AND ca.CompetencyAreaId = ''' +  CAST( @CompetencyAreaID AS VARCHAR) + ''' AND sg.SkillGroupId = ''' +  CAST( @SkillGroupID AS VARCHAR) + ''' AND skill.SkillId = ''' +  CAST( @SkillID AS VARCHAR) + ''''

ELSE 
SET @SQL= @SQL + 'AND skill.SkillId = ''' +  CAST( @SkillID AS VARCHAR) + ''''

EXEC sp_executesql @SQL

END
