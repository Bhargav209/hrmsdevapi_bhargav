﻿-- ========================================================    
-- Author   : Praveen    
-- Create date  : 06-11-2018    
-- Modified date : 01-03-2019    
-- Modified By  : Sabiha    
-- Description  : Update ClientBillingRole.    
-- [dbo].[usp_UpdateClientBillingRole] 118,'demo is required',153,3,'12-Jan-2019','15-Jan-2019','2019-01-11 19:41:52.900','BHAVANI','123'    
-- ========================================================       
      
CREATE PROCEDURE [dbo].[usp_UpdateClientBillingRole]    
(      
  @ClientBillingRoleId INT    
 ,@ClientBillingRoleName VARCHAR(70)     
 ,@ProjectId INT    
 ,@NoOfPositions INT    
 ,@StartDate DATE    
 ,@EndDate DATE    
 ,@ModifiedDate   DATETIME    
 ,@ModifiedUser   VARCHAR(100)    
 ,@SystemInfo   VARCHAR(50)    
 ,@ClientBillingPercentage INT  
)      
AS      
DECLARE @ExistingPositions INT    
BEGIN    
    
 SET NOCOUNT ON;    
    
 SELECT @ExistingPositions = [NoOfPositions] FROM ClientBillingRoles WHERE ClientBillingRoleId = @ClientBillingRoleId AND IsActive=1    
    
 IF (@ExistingPositions > @NoOfPositions)    
 SELECT 0    
    
 ELSE    
 INSERT INTO [dbo].[ClientBillingRoleHistory]    
    ([ClientBillingRoleId]    
           ,[ClientBillingRoleName]    
     ,[ProjectId]        
     ,[NoOfPositions]    
     ,[StartDate]    
     ,[EndDate]    
    
           ,[CreatedUser]               
           ,[CreatedDate]               
           ,[SystemInfo]  
     ,[ClientBillingPercentage])    
    
 SELECT [ClientBillingRoleId]    
           ,[ClientBillingRoleName]    
     ,[ProjectId]        
     ,[NoOfPositions]    
     ,[StartDate]    
     ,[EndDate]    
    
           ,@ModifiedUser          
           ,@ModifiedDate               
           ,@SystemInfo   
     ,[ClientBillingPercentage]  
        
 FROM ClientBillingRoles WHERE ClientBillingRoleId = @ClientBillingRoleId     
   
 IF(@EndDate is NOT NULL)  
 BEGIN  
  UPDATE [dbo].[ClientBillingRoles]    
  SET   
    [IsActive]     = 0   
   ,[EndDate]     = @EndDate   
   ,[ModifiedUser]    =   @ModifiedUser  
            ,[ModifiedDate]    =   @ModifiedDate  
            ,[SystemInfo]    = @SystemInfo  
  WHERE   
   ClientBillingRoleId = @ClientBillingRoleId   
  --SELECT @@ROWCOUNT  
  SELECT @ClientBillingRoleId   
 END  
  
 ELSE  
 BEGIN  
  UPDATE [dbo].[ClientBillingRoles]    
  SET   
   [ClientBillingRoleName] =  @ClientBillingRoleName  
   ,[NoOfPositions]   = @NoOfPositions  
   ,[StartDate]    = @StartDate  
   ,[ModifiedUser]    =   @ModifiedUser  
            ,[ModifiedDate]    =   @ModifiedDate  
            ,[SystemInfo]    = @SystemInfo  
   ,[ClientBillingPercentage]  =   @ClientBillingPercentage  
  WHERE   
   ClientBillingRoleId = @ClientBillingRoleId   
  
 IF(@ExistingPositions < @NoOfPositions)  
  SELECT 1  
   
 ELSE  
  --SELECT @@ROWCOUNT  
  SELECT @ClientBillingRoleId  
 END   
END