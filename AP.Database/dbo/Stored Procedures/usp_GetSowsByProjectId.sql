﻿-- =================================================    
-- Author   : Basha    
-- Create date  : 27-12-2018    
-- Modified date : 10-01-2019    
-- Modified By  : Mithun    
-- Description  : Get all the Awards     
-- =================================================    
CREATE PROCEDURE [dbo].[usp_GetSowsByProjectId]    
(    
@ProjectId INT    
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON;               
 SELECT 
   [Id] AS Id   
  ,[SOWId] AS SOWId     
  ,[SOWSignedDate] AS SOWSignedDate
  ,[SOWFileName] AS SOWFileName     
  ,[IsActive] AS IsActive 
 FROM     
  [dbo].[SOW]         
 WHERE     
  ProjectId=@ProjectId    
              
END