﻿CREATE PROCEDURE [dbo].[usp_GetCurrentFinancialYearByMonth]
AS         
BEGIN    
 SET NOCOUNT ON; 
DECLARE 
@MONTH INT=MONTH(GETDATE()),
@YEAR INT=YEAR(GETDATE())

IF @MONTH > 3
	BEGIN
	SELECT @YEAR
	END
ELSE
 SELECT @YEAR -1
END