﻿--IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[usp_GetRolesAndPositionsByRequisitionId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
--BEGIN
--DROP PROCEDURE dbo.usp_GetRolesAndPositionsByRequisitionId
--END
--GO

-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-01-2018
-- Modified date	:	08-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Get requisition details by talent requisition ID.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRolesAndPositionsByRequisitionId] 
@TalentRequisitionId INT
AS
BEGIN

SET NOCOUNT ON;

SELECT
	roles.RoleId
   ,roles.RoleName
   ,ISNULL(requisitionRoleDetails.NoOfBillablePositions, 0) AS NoOfBillablePositions
   ,ISNULL(requisitionRoleDetails.NoOfNonBillablePositions, 0) AS NoOfNonBillablePositions
   ,(ISNULL(requisitionRoleDetails.NoOfBillablePositions, 0) + ISNULL(requisitionRoleDetails.NoOfNonBillablePositions, 0)) AS TotalPositions
   ,(SELECT COUNT(EmployeeID) FROM TalentRequisitionEmployeeTag WHERE TalentRequisitionID = @TalentRequisitionId AND RoleID = requisitionRoleDetails.RoleId  ) AS TaggedEmployees
FROM 
	[dbo].[RequisitionRoleDetails] requisitionRoleDetails  
 INNER JOIN [dbo].[TalentRequisition] requisition
 ON requisitionRoleDetails.TRId = requisition.TRId
 INNER JOIN [dbo].[Roles] roles   
 ON requisitionRoleDetails.RoleId = roles.RoleId 

WHERE 
	requisitionRoleDetails.TRId = @TalentRequisitionId

END

