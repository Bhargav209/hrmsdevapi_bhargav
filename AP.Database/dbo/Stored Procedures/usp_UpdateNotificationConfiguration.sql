﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	28-03-2018
-- Modified date	:	28-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Updates Notification Type.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_UpdateNotificationConfiguration]
(
@NotificationTypeID INT,
@CategoryId INT,
@EmailFrom NVARCHAR(MAX),
@EmailTo NVARCHAR(MAX),
@EmailCC NVARCHAR(MAX),
@Subject NVARCHAR(150),
@Body NVARCHAR(MAX), 
@ModifiedUser VARCHAR(100),  
@SystemInfo VARCHAR(50)  
)
AS
BEGIN    
    
 SET NOCOUNT ON;  
 
 DECLARE @CategoryExists INT = 0;
 SELECT @CategoryExists = COUNT(CategoryID) FROM CategoryMaster WHERE CategoryID=@CategoryId

 IF @CategoryExists <> 0
  BEGIN
		 UPDATE 
			[dbo].[NotificationConfiguration] 
		 SET
			 EmailFrom                = @EmailFrom
			,EmailTo                  = @EmailTo
			,EmailCC                  = @EmailCC
			,EmailSubject             = @Subject
			,EmailContent             = @Body
			,ModifiedBy		          = @ModifiedUser
			,SystemInfo               = @SystemInfo
		 WHERE 
			NotificationTypeID = @NotificationTypeID
    
	     SELECT @@ROWCOUNT 
 END
ELSE
 SELECT -10

END
