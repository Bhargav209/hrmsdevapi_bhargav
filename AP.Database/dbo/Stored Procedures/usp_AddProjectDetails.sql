﻿CREATE PROCEDURE [dbo].[usp_AddProjectDetails]
 (
  @ProjectCode VARCHAR(50),
  @ProjectName VARCHAR(50),
  @ProjectTypeId INT,  
  @ClientId INT, 
  @IsActive BIT,
  @ProgramManagerId INT, 
  @PracticeAreaId INT,
  @PlannedStartDate DATETIME,
  @PlannedEndDate DATETIME,
  @ActualStartDate DATETIME,
  @ActualEndDate DATETIME,
  @CreatedUser  VARCHAR(100),
  @CreatedDate  DATETIME,
  @SystemInfo VARCHAR(50),
  @DepartmentId INT
 )             
AS                
BEGIN            
           
 SET NOCOUNT ON; 
 DECLARE @ProjectId INT

 IF EXISTS (SELECT * FROM Projects WHERE projectcode=@ProjectCode  AND IsActive = 1)
 SELECT -1;
 
ELSE IF EXISTS (SELECT * FROM Projects WHERE ProjectName=@ProjectName  AND IsActive = 1)
 SELECT -2;
 ELSE
 BEGIN
    INSERT INTO Projects(
	    ProjectCode
	   ,ProjectName
	   ,PlannedStartDate
	   ,PlannedEndDate
	   ,ProjectTypeId
	   ,ClientId
	   ,IsActive
	   ,CreatedUser
	   ,CreatedDate	   
	   ,SystemInfo
	   ,ActualStartDate
	   ,ActualEndDate
	   ,DepartmentId
	   ,PracticeAreaId)
    VALUES(
	    @ProjectCode
	   ,@ProjectName
	   ,@PlannedStartDate
	   ,@PlannedEndDate
	   ,@ProjectTypeId
	   ,@ClientId
	   ,@IsActive
	   ,@CreatedUser
	   ,@CreatedDate	   
	   ,@SystemInfo
	   ,@ActualStartDate
	   ,@ActualEndDate
	   ,@DepartmentId
	   ,@PracticeAreaId)

	SET @ProjectId =SCOPE_IDENTITY()
					 
    IF(@ProgramManagerId != 0 AND @ProgramManagerId > 0)

  	INSERT INTO ProjectManagers(
	    ProjectId	   
	   ,ProgramManagerID
	   ,IsActive
	   ,CreatedBy
	   ,CreatedDate	   
	   ,SystemInfo
	  )	
	VALUES(
	    @ProjectId	  
	   ,@ProgramManagerId
	   ,'1'
	   ,@CreatedUser
	   ,@CreatedDate	  
	   ,@SystemInfo
	  )

	SELECT @@ROWCOUNT; 
 END
 END
