﻿-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	30-11-2017            
-- Modified date	:	30-11-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all the competency areas       
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetCompetencyAreas]             
AS              
BEGIN          
          
 SET NOCOUNT ON;          
           
 SELECT  
	CompetencyAreaId
   ,CompetencyAreaCode
   ,CompetencyAreaDescription
   ,IsActive  
 FROM 
	[dbo].[CompetencyArea]
 WHERE
	IsActive = 1                 
END