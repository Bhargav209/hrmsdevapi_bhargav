﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-01-2018
-- Modified date	:	12-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Get requisition by talent requisiton Id.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRequisitionHistoryByRequisitionId] --1036
@TalentRequisitionId INT  
AS  
BEGIN  
  
SET NOCOUNT ON;  
DECLARE @CategoryId INT

SELECT @CategoryId=[dbo].[udf_GetCategoryId]('TalentRequisition') 

SELECT        
    fromEmployee.FirstName + ' ' + fromEmployee.LastName AS FromEmployeeName  
   ,STUFF((SELECT ', ' + CONCAT (FirstName, ' ', MiddleName, ' ', LastName) 
	FROM Employee WHERE EmployeeId IN (SELECT VALUE FROM [dbo].[UDF_SplitString](requisitionWorkFlow.ToEmployeeID,','))
	FOR XML PATH('')), 1, 1, '') 
   ,[status].StatusCode  
   ,requisitionWorkFlow.Comments  
   ,requisitionWorkFlow.CreatedDate     
FROM   
 [dbo].[TalentRequisitionWorkFlow] requisitionWorkFlow    
 INNER JOIN [dbo].[Employee] fromEmployee  
 ON requisitionWorkFlow.FromEmployeeID = fromEmployee.EmployeeId  
 INNER JOIN [dbo].[Status] [status]  
 ON requisitionWorkFlow.StatusID = [status].StatusId AND [status].CategoryID=@CategoryId 
WHERE  
 requisitionWorkFlow.TalentRequisitionID = @TalentRequisitionId  
END
