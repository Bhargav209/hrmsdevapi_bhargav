﻿-- ==========================================     
-- Author			:	Basha            
-- Create date		:	28-06-2018            
-- Modified date	:	28-06-2018            
-- Modified By		:	Basha            
-- Description		:	Gets Status By Role      
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetStatusByRole] 
@RoleName varchar(50),
@CategoryId INT            
AS              
BEGIN          
          
 SET NOCOUNT ON;    
 
 --Declaring SQL Statement  for selecting statusid and statuscode for particular categoryId
 DECLARE @SQL NVARCHAR(MAX),
 		 @SendBackForHRMReviewDH varchar,
		 @ApprovedDH varchar   
		 
SET @SendBackForHRMReviewDH = (SELECT [dbo].[udf_GetStatusId]('SendBackForHRMReview','KRA'))
SET @ApprovedDH = (SELECT [dbo].[udf_GetStatusId]('Approved','KRA'))

SET @SQL = 'SELECT distinct
					 rolestatus.statusid 
					,status.StatusCode 
					from Rolestatus rolestatus
					 JOIN Status status
					ON status.StatusId = roleStatus.statusId 
		    WHERE status.CategoryId = ''' +  CAST(@categoryId AS VARCHAR) + ''''
      

	IF(@RoleName = 'Department Head')
	  SET @SQL = @SQL + ' AND status.StatusId in ('''+ @SendBackForHRMReviewDH +''','''+ @ApprovedDH +''')'

	EXEC sp_executesql @SQL

END