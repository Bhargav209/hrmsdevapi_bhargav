﻿-- ==========================================                 
-- Author   : Santosh                
-- Create date  : 17-04-2018                
-- Modified date : 12-09-2018                    
-- Modified By  : Bhavani                
-- Description  : Get Utilization Report    
--[dbo].[usp_rpt_FinanceReport] '01-Aug-2018', '31-Aug-2018', 0, 256, 1    
-- ==========================================              
CREATE PROCEDURE [dbo].[usp_rpt_FinanceReport]    
(      
	@FromDate DATETIME    
   ,@ToDate DATETIME    
   ,@ProjectID INT    
   --,@RowsPerPage INT    
   --,@PageNumber INT    
)           
AS             
BEGIN    
 SET NOCOUNT ON;      
     
 SELECT DISTINCT  
   employee.EmployeeCode    
  ,dbo.udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName    
  ,dbo.udfGetEmployeeFullName(managers.ProgramManagerID) AS ProgramManagerName    
  ,dbo.udfGetEmployeeFullName(allocation.ReportingManagerId) AS ReportingManagerName    
  ,dbo.udfGetEmployeeFullName(managers.LeadID) AS LeadName    
  --,dbo.udf_GetRoleName(rolemaster.RoleMasterID) AS RoleName    
  ,designation.DesignationName    
  ,department.[Description]    
  ,grade.GradeName    
  ,client.ClientName   
  --,skills.SkillCode  As SkillCode     
  ,dbo.udf_GetEmployeeSkills(employee.EmployeeId) AS SkillCode    
  ,project.ProjectName    
  ,ISNULL(allocation.IsBillable,0) AS [IsBillable]   
  ,ISNULL(allocation.IsCritical,0) AS [IsCritical]  
  ,allocation.ClientBillingPercentage   
  ,allocationpercentage.[Percentage] AS Allocationpercentage    
  ,allocation.InternalBillingPercentage    
  ,ISNULL(internalrole.InternalBillingRoleCode,'') as  InternalBillingRoleCode     
  ,ISNULL(clientrole.ClientBillingRoleCode,'') as ClientBillingRoleCode  
  ,CASE WHEN @FromDate > allocation.EffectiveDate THEN @FromDate ELSE allocation.EffectiveDate END AS FromDate    
  ,CASE WHEN @ToDate > allocation.ReleaseDate THEN allocation.ReleaseDate  ELSE @ToDate END AS ToDate    
  FROM [dbo].[AssociateAllocation] allocation    
  LEFT JOIN [dbo].[ClientBillingRoles] clientrole    
  ON allocation.ClientBillingRoleId = clientrole.ClientBillingRoleId    
  LEFT JOIN [dbo].[InternalBillingRoles] internalrole    
  ON allocation.InternalBillingRoleId = internalrole.InternalBillingRoleId    
  --INNER JOIN [dbo].[RoleMaster] rolemaster    
  --ON allocation.RoleMasterID = rolemaster.RoleMasterID    
  INNER JOIN [dbo].[Projects] project    
  ON allocation.ProjectId = project.ProjectId    
  INNER JOIN ProjectManagers managers ON managers.ProjectID = project.ProjectId AND managers.IsActive=1  
  INNER JOIN [dbo].[Clients] client    
  ON project.ClientId = client.ClientId    
  INNER JOIN [dbo].[Employee] employee    
  ON allocation.EmployeeId = employee.EmployeeId   
  INNER JOIN [dbo].[EmployeeSkills] employeeSkills                      
  ON employee.EmployeeId = employeeSkills.EmployeeId   
  INNER JOIN [dbo].[Skills] skills                      
  ON employeeSkills.SkillId = skills.SkillId      
  INNER JOIN [dbo].[Grades] grade    
  ON employee.GradeId = grade.GradeId    
  INNER JOIN [dbo].[Departments] department    
  ON employee.DepartmentId = department.DepartmentId    
  INNER JOIN [dbo].[Designations] designation    
  ON employee.Designation = designation.DesignationId    
  INNER JOIN [dbo].[AllocationPercentage] allocationpercentage    
  ON allocation.AllocationPercentage = allocationpercentage.AllocationPercentageID    
  WHERE  project.IsActive = 1 --AND rolemaster.IsActive = 1   
  AND((allocation.EffectiveDate <= @ToDate) AND allocation.EffectiveDate <> ISNULL(allocation.ReleaseDate,''))  
  AND ((allocation.ReleaseDate >= @FromDate AND allocation.ReleaseDate <= @ToDate)  
  OR allocation.ReleaseDate IS NULL OR allocation.IsActive = 1)   
  AND allocation.ProjectId = CASE @ProjectID WHEN 0 THEN allocation.ProjectId ELSE @ProjectID END    
  AND employeeSkills.IsPrimary=1  
  ORDER BY employee.EmployeeCode  
 -- OFFSET (@PageNumber - 1) ROWS  
 -- FETCH NEXT @RowsPerPage ROWS ONLY    
END 