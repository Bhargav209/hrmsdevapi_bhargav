﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 10-01-2019    
-- Modified date : 10-01-2019    
-- Modified By  : Mithun    
-- Description  : Get addendums By SOW id     
-- =================================================    
CREATE PROCEDURE [dbo].[usp_GetAddendumsBySOWId]    
(    
 @Id INT,
 @ProjectId INT 
,@RoleName VARCHAR(50)    
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON;
  DECLARE @RoleId INT
 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)
 IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head                  
 SELECT 
   [AddendumId]
  ,[ProjectId]     
  ,[SOWId]
  ,[AddendumNo] 
  ,[RecipientName]
  ,[AddendumDate]
  ,[Note]   
  ,[Id]
 FROM     
  [dbo].[Addendum]         
WHERE     
  AddendumId=@Id AND ProjectId=@ProjectId
 ELSE 
 SELECT -1             
END
