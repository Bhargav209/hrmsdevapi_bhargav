﻿-- ========================================  
-- Author   : Ramya     
-- Create date  : 14-2-1018   
-- Modified date :  
-- Modified By  : 
-- Description  : Gets Current Financial Year    
-- ========================================  
CREATE PROCEDURE [dbo].[usp_GetCurrentFinancialYear]  
AS       
BEGIN  
  
 SET NOCOUNT ON;    
 SELECT  
  ID
 ,CAST(FromYear AS VARCHAR(4)) + ' - ' + CAST(ToYear AS VARCHAR(4)) AS Name
 FROM [dbo].[FinancialYear]
 where IsActive=1
END