﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	20-03-2018            
-- Modified date	:	20-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	updates roleid in allocation table.      
-- ============================================


CREATE PROCEDURE [dbo].[usp_InsertUpdateRoleMasterId]
(
 @Id INT
,@RoleMasterId INT
,@ProjectId INT
,@DepartmentId INT
,@DateModified DATETIME
,@ModifiedUser VARCHAR(150)
,@CreatedDate DATETIME
,@CreatedUser VARCHAR(150)
,@SystemInfo VARCHAR(50)
)
AS
BEGIN
 SET NOCOUNT ON;
	
	IF (@ProjectId IS NOT NULL)  --Technology & Delivery
	BEGIN
		UPDATE [dbo].[AssociateAllocation]
		SET
			RoleMasterId	        =	@RoleMasterId
			,ModifiedDate			=	@DateModified
			,ModifiedBy				=	@ModifiedUser
			,SystemInfo				=	@SystemInfo
		WHERE 
			AssociateAllocationId = @Id
		END
	ELSE
	BEGIN
	
		DECLARE @ServiceDepartmentRoleId INT

		SELECT 
			@ServiceDepartmentRoleId = ServiceDepartmentRoleID 
		FROM 
			[dbo].[ServiceDepartmentRoles]
		WHERE 
			EmployeeID = @Id AND DepartmentID = @DepartmentId AND RoleMasterID = @RoleMasterId AND IsActive = 1

		IF (@ServiceDepartmentRoleId IS NULL)
		BEGIN
			
			UPDATE [dbo].[ServiceDepartmentRoles]
			SET
				IsActive	            =	0
				,ModifiedDate			=	@DateModified
				,ModifiedUser			=	@ModifiedUser
				,SystemInfo				=	@SystemInfo
			WHERE 
				EmployeeID = @Id AND DepartmentID = @DepartmentId AND IsActive = 1

			INSERT INTO [dbo].[ServiceDepartmentRoles]
				(      
					RoleMasterID      
					,EmployeeID
					,IsActive
					,DepartmentId
					,CreatedDate
					,CreatedUser     
					,SystemInfo      
				)            
			VALUES            
				(      
					@RoleMasterId       
					,@Id
					,1
					,@DepartmentId
					,@CreatedDate
					,@CreatedUser     
					,@SystemInfo    
				)
	END
	END
	SELECT @@ROWCOUNT
END
