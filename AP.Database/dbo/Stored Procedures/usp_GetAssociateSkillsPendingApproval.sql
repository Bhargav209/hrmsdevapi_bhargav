﻿-- ==========================================================      
-- Author   : Basha                  
-- Create date  : 04-01-2019                  
-- Modified date : 04-01-2019                  
-- Modified By  : Basha                  
-- Description  : Gets associates Skills pending for approaval      
-- ==========================================================      
CREATE PROCEDURE [dbo].[usp_GetAssociateSkillsPendingApproval]      
@EmployeeID INT      
AS                  
BEGIN              
          
 SET NOCOUNT ON;                  
 SELECT * FROM [dbo].[vw_AssociateSkillsPendingApproval]       
 WHERE empID = @EmployeeID     
    
END 