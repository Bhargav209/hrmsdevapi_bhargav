﻿-- =================================================      
-- Author   : Santosh      
-- Create date  : 06-02-2018      
-- Modified date : 06-02-2018      
-- Modified By  : Basha, Bhavani     
-- Description  : Gets AssociatesforRelease And ResourceReports      
-- =================================================      
CREATE PROCEDURE [dbo].[usp_GetAssociatesforReleaseAndResourceReports]     
@EmployeeID INT,
@RoleName varchar(100)
AS        
BEGIN        
SET NOCOUNT ON;        
      
DECLARE      
  @UserId INT,      
  @RoleId INT,      
  @PMRoleId INT,      
  @HRMRoleId INT,      
  @HRARoleId INT ,
  @TrainingDeptHeadRoleId INT,
  @ProjectTypeId INT,
  @LoggedInRoleId INT
      
IF(@EmployeeID = -1)      
 BEGIN      
    SELECT        
      EmployeeID AS ID        
     ,[dbo].[udfGetEmployeeFullName](EmployeeID) AS Name        
    FROM Employee        
    WHERE IsActive = 1  Order by Name      
 END      
ELSE      
 BEGIN      
   SELECT @UserId = UserId from Employee where EmployeeId=@EmployeeID
   SELECT @LoggedInRoleId = RoleId from Roles where RoleName = @RoleName    
   SELECT @RoleId = RoleId from UserRoles where UserId = @UserId AND RoleId = @LoggedInRoleId    
   SELECT @PMRoleId = RoleId from Roles where RoleName ='Program Manager'       
   SELECT @HRMRoleId = RoleId from Roles where RoleName ='HRM'       
   SELECT @HRARoleId = RoleId from Roles where RoleName='HRA'  
   SELECT @TrainingDeptHeadRoleId = RoleId from Roles where RoleName='Training Department Head'  
   SELECT @ProjectTypeId = ProjectTypeId FROM ProjectType WHERE ProjectTypeCode ='Talent Pool'
       
      
   IF(@RoleId = @PMRoleId or @RoleId = @TrainingDeptHeadRoleId)      
      
    SELECT DISTINCT EmployeeID AS ID        
      ,[dbo].[udfGetEmployeeFullName](EmployeeID) AS Name , ProjectId  ,EffectiveDate  
       FROM  AssociateAllocation WHERE ProjectId in (      
      SELECT projectManager.ProjectId FROM ProjectManagers projectManager
	  INNER JOIN Projects project ON projectManager.ProjectID = project.ProjectId
      WHERE projectManager.ProgramManagerID=@EmployeeID AND projectManager.IsActive=1 
	  AND project.ProjectTypeId != @ProjectTypeId AND project.IsActive=1) AND IsActive=1   
	      
      
		ELSE IF  (@RoleId = @HRMRoleId or @RoleId = @HRARoleId )      
      
    SELECT DISTINCT emp.EmployeeId AS ID  
	,[dbo].[udfGetEmployeeFullName]( emp.EmployeeId) as Name,  ProjectId    ,EffectiveDate  
    FROM AssociateAllocation allocation       
     INNER JOIN Employee emp on allocation.EmployeeId = emp.EmployeeId       
    WHERE allocation.IsActive=1 AND  emp.IsActive=1 AND ProjectId in (      
      SELECT projectManager.ProjectId FROM ProjectManagers projectManager
	  INNER JOIN Projects project ON projectManager.ProjectID = project.ProjectId
      WHERE project.ProjectTypeId != @ProjectTypeId AND project.IsActive=1)

    Order by Name      
      
      ELSE       
    SELECT -1  --No data showing other than PM, HRM, HRA    
	  
      
 END      
END
