﻿
-- ========================================================
-- Author			:	kalyan.Penumutchu
-- Create date		:	24-05-2018
-- Modified date	:	24-05-2018
-- Modified By		:	
-- Description		:	Get scale details by master Id.
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_GetKRAScaleDetailsByMaster] 
(
	@ScaleMasterId int = 0
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  
		 KRAScaleDetailID
		,KRAScale
		,ScaleDescription
	FROM 
		KRAScaleDetails
	WHERE
		KRAScaleMasterID = @ScaleMasterId
END