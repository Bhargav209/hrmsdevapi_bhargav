﻿-- =================================================    
-- AUTHOR		: PRASANNA    
-- CREATE DATE  : 28-01-2019    
-- MODIFIED DATE: 28-01-2019    
-- MODIFIED BY  : MITHUN    
-- DESCRIPTION  : GET WISH LIST Details TO POPULATE
-- =================================================    
CREATE PROCEDURE [dbo].[USP_GetWishListDetails]    
(  
  @MANAGERID INT  
)    
AS                  
BEGIN         

 SET NOCOUNT ON;

 SELECT WLIST.ID,WLIST.WISHLISTNAME,designation.DesignationName as 'Designation',EMP.FIRSTNAME+' '+EMP.LASTNAME AS 'EmployeeName'
 FROM WISHLIST WLIST
 INNER JOIN EMPLOYEE EMP ON EMP.EMPLOYEEID=WLIST.EMPLOYEEID
 left join Designations designation on designation.DesignationId=emp.Designation
 WHERE WLIST.MANAGERID=@MANAGERID

END