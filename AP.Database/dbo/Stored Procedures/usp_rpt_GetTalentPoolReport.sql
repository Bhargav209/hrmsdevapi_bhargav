﻿-- ================================================
-- Author			:	Santosh
-- Create date		:	26-04-2018
-- Modified date	:	12-02-2019
-- Modified By		:	Prasanna
-- Description		:	Get Talent Pool report
 -- ===============================================
CREATE PROCEDURE [dbo].[usp_rpt_GetTalentPoolReport]
(
  @ProjectID INT
)
AS
BEGIN
     
 SET NOCOUNT ON;
     
 SELECT
  employee.EmployeeCode
 ,dbo.udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName
 ,designation.DesignationCode AS Designation
 ,grade.GradeCode AS Grade
 --,employee.Experience
 FROM AssociateAllocation allocation
 INNER JOIN Employee employee
 ON allocation.EmployeeId = employee.EmployeeId
 INNER JOIN Designations designation
 ON employee.Designation = designation.DesignationId
 INNER JOIN Grades grade
 ON employee.GradeId = grade.GradeId
 WHERE allocation.IsActive = 1 AND allocation.ProjectId = @ProjectID
  
END