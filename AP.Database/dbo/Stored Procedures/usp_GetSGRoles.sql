﻿CREATE PROCEDURE [dbo].[usp_GetSGRoles]
AS
BEGIN
SELECT 
	sg.SGRoleID,
	sg.SGRoleName,
	department.Description AS [Department Name]
FROM 
	SGRole sg 
INNER JOIN 
	Departments department 
ON sg.DepartmentId = department.DepartmentId
END

