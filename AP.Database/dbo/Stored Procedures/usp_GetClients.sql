﻿CREATE PROCEDURE [dbo].[usp_GetClients]
              
AS                
BEGIN            
           
 SET NOCOUNT ON; 
 SELECT 
  ClientId AS ClientId
 ,ClientLegalName AS ClientLegalName
 ,ClientShortName AS ClientName
 FROM Clients
 WHERE IsActive = 1 

END 
