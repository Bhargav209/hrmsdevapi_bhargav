﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	27-11-2017
-- Modified date	:	27-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Adds a certification detail of an employee.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_AddCertificationDetails]            
@EmployeeID INT, 
@CertificationID INT,
@ValidFrom VARCHAR(4),
@Institution VARCHAR(150),
@Specialization VARCHAR(150),
@ValidUpto VARCHAR(4),
@CreatedUser VARCHAR(100),
@CreatedDate DATETIME,
@SystemInfo VARCHAR(50),
@SkillGroupID INT      
AS            
BEGIN        
        
 SET NOCOUNT ON;
         
 INSERT INTO [dbo].[AssociatesCertifications]            
 (
    EmployeeID
   ,CertificationID
   ,ValidFrom
   ,Institution
   ,Specialization
   ,ValidUpto
   ,CreatedUser
   ,CreatedDate
   ,SystemInfo
   ,SkillGroupID        
 )            
 VALUES            
 (
    @EmployeeID
   ,@CertificationID
   ,@ValidFrom
   ,@Institution
   ,@Specialization
   ,@ValidUpto
   ,@CreatedUser
   ,@CreatedDate
   ,@SystemInfo
   ,@SkillGroupID      
 )            
              
 SELECT @@ROWCOUNT            
END 