﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-04-2018
-- Modified date	:	12-04-2018
-- Modified By		:	Sushmitha
-- Description		:	Creates a KRA definition.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateKRADefinition]
(  
@KRAGroupId INT,
@KRAAspectId INT, 
@Metric VARCHAR(500),
@KRAMeasurementTypeID INT,
@FinancialYearID INT,
@KRAOperatorID INT,
@KRAScaleMasterID INT,
@KRATargetValue DECIMAL(5,2),
@KRATargetValeAsDate Datetime NULL,
@KRATargetText VARCHAR(100) NULL,
@KRATargetPeriodID INT,
@CreatedUser VARCHAR(50),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON; 
 
 DECLARE @MaximumScale DECIMAL(5,2) 

	SELECT @MaximumScale = MaximumScale FROM [dbo].[KRAScaleMaster] WHERE KRAScaleMasterID = @KRAScaleMasterID

	IF (@KRATargetValue > @MaximumScale)
		RETURN -7; --target value cannot be greater than maximum scale

	INSERT INTO [dbo].[KRADefinition]  
	(
		KRAGroupId
	   ,KRAAspectId
	   ,Metric
	   ,KRAMeasurementTypeID
	   ,FinancialYearId
	   ,KRAOperatorID
	   ,KRAScaleMasterID
	   ,TargetValue
	   ,KRATargetText
	   ,TargetValueAsDate
	   ,KRATargetPeriodID
	   ,CreatedUser
	   ,SystemInfo
	)  
	VALUES
	(
		@KRAGroupId
	   ,@KRAAspectId
	   ,@Metric
	   ,@KRAMeasurementTypeID 
	   ,@FinancialYearID
	   ,@KRAOperatorID
	   ,@KRAScaleMasterID
	   ,@KRATargetValue
	   ,@KRATargetText
	   ,@KRATargetValeAsDate
	   ,@KRATargetPeriodID
	   ,@CreatedUser
	   ,@SystemInfo
	)  

	SELECT @@ROWCOUNT

END
