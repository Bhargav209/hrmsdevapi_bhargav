﻿-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	06-12-2017            
-- Modified date	:	11-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Get Roles by Talent Requisition ID.
-- ==========================================
CREATE PROCEDURE [dbo].[usp_GetRolesByTalentRequisitionId]
@TalentRequisitionId INT      
AS            
BEGIN        
        
 SET NOCOUNT ON;   
         
  SELECT DISTINCT
	 roleDetails.RoleMasterId AS Id
	,[dbo].[udf_GetRoleName](roleDetails.RoleMasterId) AS Name
 FROM
	[dbo].[RequisitionRoleDetails] roleDetails
	INNER JOIN [dbo].[TalentRequisition] talentRequisition
	ON roleDetails.TRId = talentRequisition.TRId
	INNER JOIN [dbo].[RoleMaster] rolemaster  
	ON roleDetails.RoleMasterId = rolemaster.RoleMasterID
 WHERE 
	roleDetails.TRId = @TalentRequisitionId
END
