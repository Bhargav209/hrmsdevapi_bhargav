﻿-- ==========================================
-- Author                  :      Basha
-- Create date             :      16-05-2018
-- Modified date           :      18-05-2018                
-- Modified By             :      Basha      
-- Description             :      Gets award nomination
-- ==========================================      
CREATE PROCEDURE [dbo].[usp_GetAwardNomination]  
       @FinancialYearID INT,      
       @RoleName VARCHAR(150),
       @EmployeeID INT 

AS          
BEGIN      
 SET NOCOUNT ON;

  --Check for Project Manager
   DECLARE @ProjectManager INT = 0
   DECLARE @ProjectID      INT = 0
   SELECT @ProjectManager = ReportingManagerID, @ProjectID = ProjectID FROM ProjectManagers WHERE ReportingManagerID = @EmployeeID 
   IF(@RoleName = 'Project Manager' AND @ProjectManager = 0)
   RETURN

   --Check for Deaprtment Head  
   DECLARE @DepartmentHeadCount INT = 0
   SELECT @DepartmentHeadCount = COUNT(*) from Departments WHERE DepartmentHeadID = @EmployeeID
   IF(@RoleName = 'Department Head' AND @DepartmentHeadCount = 0)
   RETURN  

      DECLARE @SQL NVARCHAR(MAX)
      SET @SQL = '
         
                           SELECT 
                                         awardNomination.AwardNominationId,
                                         award.AwardTitle,
                                         award.AwardId,
                                         awardType.AwardTypeId,
                                         awardType.AwardType,
                                         departments.DepartmentId,
                                         departments.Description,
                                         projects.ProjectId,
                                         projects.ProjectCode,
                                         projects.ProjectName,
                                         status.StatusDescription,
                                         count(*) AS Count
                           FROM AwardNomination awardNomination
                                  INNER JOIN Award award
                                  ON award.AwardId=awardNomination.AwardId
                                  INNER JOIN AwardType awardType
                                  ON awardType.AwardTypeId=awardNomination.AwardTypeId
                                  INNER JOIN Departments departments
                                  ON departments.DepartmentId=awardNomination.DepartmentId
                                  INNER JOIN Projects projects
                                  ON projects.ProjectId=awardNomination.ProjectId
                                  INNER JOIN Status status
                                  ON status.StatusId=awardNomination.StatusId WHERE FinancialYearId = ''' +  CAST(@FinancialYearID AS VARCHAR) + ''''

       IF(@RoleName = 'Project Manager')
       BEGIN
              SET @SQL = @SQL + ' AND awardNomination.ProjectID = ''' +  CAST( @ProjectID AS VARCHAR) + ''''
       END

       IF(@RoleName = 'Department Head')
       BEGIN
              SET @SQL = @SQL + ' --AND status.StatusCode IN (''SumbmittedForDepartmentHeadApproval'',''ApprovedByDepartmentHead'') 
              AND departments.DepartmentId IN (select DepartmentId from Departments WHERE DepartmentHeadID = ''' + CAST(@EmployeeID AS VARCHAR) +''')'
       END

       IF(@RoleName = 'HR Head')
       BEGIN
              SET @SQL = @SQL + '-- AND status.StatusCode IN (''SubmittedForHRHeadApproval'',''ApprovedByHRHead'') 
              
              '
       END

       SET @SQL = @SQL + ' GROUP by award.AwardTitle,
                                         award.AwardId,
                                         awardType.AwardTypeId,
                                         awardType.AwardType,
                                         departments.DepartmentId,
                                         departments.Description,
                                         projects.ProjectId,
                                         projects.ProjectCode,
                                         projects.ProjectName,
                                         status.StatusDescription,
                                         awardNomination.AwardNominationId '
      EXEC sp_executesql @SQL

END
