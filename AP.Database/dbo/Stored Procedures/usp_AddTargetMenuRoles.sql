﻿-- =======================================     
-- Author			:	Sunitha          
-- Create date		:	12-03-2018    
-- Modified date	:	12-03-2018              
-- Modified By		:	Sunitha          
-- Description		:	Add TargetMenus         
-- =======================================
CREATE PROCEDURE [dbo].[usp_AddTargetMenuRoles]        
@RoleId INT,  
@MenuId Int,      
@CreatedUser VARCHAR(100),      
@SystemInfo VARCHAR(50),      
@CreatedDate DATETIME     
AS          
BEGIN      
      
 SET NOCOUNT ON;  

 INSERT INTO [dbo].[MenuRoles]          
 (      
   MenuId  
  ,RoleId    
  ,IsActive      
  ,CreatedBy      
  ,CreatedDate       
  ,SystemInfo      
 )            
 VALUES            
 (      
   @MenuId
  ,@RoleId       
  ,1        
  ,@CreatedUser      
  ,@CreatedDate       
  ,@SystemInfo    
 ) 
        
 SELECT @@ROWCOUNT          
END