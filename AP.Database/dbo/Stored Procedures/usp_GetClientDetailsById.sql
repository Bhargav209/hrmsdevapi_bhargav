﻿-- ========================================================
-- Author			:	Basha
-- Create date		:	24-12-2018
-- Modified date	:	24-12-2018
-- Modified By		:	Basha
-- Description		:	Get Client Details By Id
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_GetClientDetailsById]
(
	@ClientId INT
)
AS
BEGIN
 SET NOCOUNT ON;
 SELECT ClientId, 
		ClientCode,
		ClientShortName,
		ClientLegalName, 
		IsActive
  FROM
  Clients
  WHERE ClientId=@ClientId
END
