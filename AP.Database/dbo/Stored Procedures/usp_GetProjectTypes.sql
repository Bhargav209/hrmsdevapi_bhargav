﻿CREATE PROCEDURE [dbo].[usp_GetProjectTypes]
              
AS                
BEGIN            
           
 SET NOCOUNT ON; 
 SELECT 
  ProjectTypeId AS ProjectTypeId
 ,ProjectTypeCode AS ProjectTypeCode
 ,Description AS ProjectType
 FROM ProjectType
 WHERE IsActive = 1 

END   
