﻿--==========================================  
-- Author   : Santosh    
-- Create date  : 30-01-2018  
-- Modified date : 30-01-2018    
-- Modified By  : Santosh  
-- Description  : Get Appreciation Type  
-- ============================================    
CREATE PROCEDURE [dbo].[usp_GetAppreciationType]    
AS         
BEGIN    
 SET NOCOUNT ON;      
  SELECT    
   ID  
  ,[Type] AS Name  
 FROM [dbo].[AppreciationType]  
  
END