﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	27-11-2017
-- Modified date	:	27-11-2017
-- Modified By		:	Sushmitha
-- Description		:	updates a membership detail of an employee.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_UpdateMembershipDetails]         
@ID INT,   
@ProgramTitle VARCHAR(150),
@ValidFrom VARCHAR(4),
@Institution VARCHAR(150),
@Specialization VARCHAR(150),
@ValidUpto VARCHAR(4),
@ModifiedUser VARCHAR(100),
@ModifiedDate DATETIME,
@SystemInfo VARCHAR(50)  
AS          
BEGIN      
      
 SET NOCOUNT ON;     
       
 UPDATE [dbo].[AssociatesMemberShips]          
 SET  
	ProgramTitle	=	@ProgramTitle  
   ,ValidFrom		=	@ValidFrom
   ,Institution		=	@Institution
   ,Specialization	=	@Specialization
   ,ValidUpto		=	@ValidUpto   
   ,ModifiedUser	=	@ModifiedUser   
   ,ModifiedDate	=	@ModifiedDate  
   ,SystemInfo		=	@SystemInfo 
 WHERE ID	=	@ID
            
 SELECT @@ROWCOUNT          
END
