﻿-- =========================================================   
-- Author			:	Santosh  
-- Create date		:	09-05-2018    
-- Modified date	:	24-05-2018    
-- Modified By		:	Santosh    
-- Description		:	Gets KRA Aspect Group By KRA GroupId    
-- =========================================================     
CREATE PROCEDURE [dbo].[usp_GetKRAAspectForKRAGroup]
(    
@KRAGroupId INT,    
@FinancialYearId INT    
)    
AS                      
BEGIN                  
                  
 SET NOCOUNT ON;        
                     
 SELECT DISTINCT  
  kraDefinition.KRAAspectId    
 ,aspectMaster.AspectName AS KRAAspectName    
 ,kraStatus.StatusId    
 ,CAST(financialYear.FromYear AS VARCHAR(4)) + ' - ' + CAST(financialYear.ToYear AS VARCHAR(4)) AS FinancialYear    
 ,kraDefinition.FinancialYearId    
 FROM     
 [dbo].[KRADefinition] kraDefinition       
 INNER JOIN [dbo].[KRAGroup] kraGroup        
 ON kraDefinition.KRAGroupId = kraGroup.KRAGroupId     
 INNER JOIN [dbo].[KRAAspectMaster] kraAspect    
 ON kraDefinition.KRAAspectId = kraAspect.KRAAspectID     
 INNER JOIN [dbo].[KRAStatus] kraStatus    
 ON kraGroup.KRAGroupId = kraStatus.KRAGroupId AND kraStatus.FinancialYearId = @FinancialYearId      
 INNER JOIN [dbo].[FinancialYear] financialYear    
 ON kraDefinition.FinancialYearId = financialYear.ID 
  INNER JOIN [dbo].[AspectMaster] aspectMaster    
 ON kraAspect.AspectId = aspectMaster.AspectId    
 WHERE    
 kraDefinition.KRAGroupId = @KRAGroupId AND kraDefinition.FinancialYearId = @FinancialYearId    
          
 END 