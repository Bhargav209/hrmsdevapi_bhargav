﻿CREATE PROCEDURE [dbo].[usp_GetAllFinancialYears]    
AS         
BEGIN        
 SET NOCOUNT ON;    
 SELECT ID,FromYear,ToYear,IsActive from [dbo].[FinancialYear]
 END