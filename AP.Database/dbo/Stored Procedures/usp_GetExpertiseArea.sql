﻿-- ================================================  
-- Author			: Ramya Singamsetti      
-- Create date		: 24-11-2017      
-- Modified date	:  
-- Modified By		:      
-- Description		: Gets Expertise Area master   
-- ================================================  
CREATE PROCEDURE [dbo].[usp_GetExpertiseArea]      
AS      
BEGIN  
  
 SET NOCOUNT ON;  
   
 SELECT   
  ea.ExpertiseId  
 ,ea.ExpertiseAreaDescription 
 FROM [dbo].[ExpertiseArea] ea
 WHERE ea.IsActive = 1
END