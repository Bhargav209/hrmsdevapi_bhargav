﻿-- ========================================================    
-- Author   : Basha    
-- Create date  : 24-12-2018    
-- Modified date : 24-12-2018    
-- Modified By  : Basha    
-- Description  : Add Client    
-- ========================================================     
CREATE PROCEDURE [dbo].[usp_CreateClient] 
(    
 @ClientCode VARCHAR(50)    
 ,@ClientShortName VARCHAR(100)    
 ,@ClientLegalName VARCHAR(100)    
 ,@IsActive bit    
 ,@CreatedUser VARCHAR(100)    
 ,@CreatedDate   DATETIME    
 ,@SystemInfo VARCHAR(50)    
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
    
 DECLARE @IsCategoryShortNameExists INT, @ClientShortNameHash nvarchar(450)    
 SELECT @ClientShortName= RTRIM(LTRIM(@ClientShortName))    

 SELECT @ClientShortNameHash = CONVERT(nvarchar,UPPER(@ClientShortName));
 SET @ClientShortNameHash  = HashBytes('SHA1', @ClientShortNameHash);
     
	 BEGIN TRY
		   INSERT INTO Clients    
			(    
			ClientCode      
		   ,ClientShortName      
		   ,ClientLegalName    
		   ,IsActive    
		   ,ClientShortNameHash
		   ,CreatedUser      
		   ,CreatedDate    
		   ,SystemInfo       
			)      
		   VALUES      
		   (      
		   @ClientCode      
		   ,@ClientShortName      
		   ,@ClientLegalName   
		   ,@IsActive     
		   ,@ClientShortNameHash
		   ,@CreatedUser      
		   ,@CreatedDate    
		   ,@SystemInfo     
			)    
		   SELECT @@ROWCOUNT  
     
  END TRY  
	BEGIN CATCH 
	SELECT -1
	END CATCH  
END



