﻿-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	30-11-2017            
-- Modified date	:	30-11-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all the skills       
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetSkills]             
AS              
BEGIN          
          
 SET NOCOUNT ON;          
           
 SELECT  
	skills.SkillId      
   ,skills.SkillCode
   ,skills.SkillDescription
   ,skills.IsActive
   ,skillGroup.SkillGroupName   
 FROM 
	[dbo].[Skills] skills
 INNER JOIN 
	[dbo].[SkillGroup] skillGroup ON skills.SkillGroupId = skillGroup.SkillGroupId
 WHERE
	skills.IsActive = 1 AND skills.IsApproved = 1                   
END