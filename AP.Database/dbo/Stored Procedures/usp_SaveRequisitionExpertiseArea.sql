﻿-- ================================================================  
-- Author			:	Ramya Singamsetti  
-- Create date		:	24-11-2017  
-- Modified date	:	05-06-2018  
-- Modified By		:   Santosh  
-- Description		:	Save Expertise Area by Talent requisitionId  
-- ================================================================  
CREATE PROCEDURE [dbo].[usp_SaveRequisitionExpertiseArea]    
@TalentRequisitionId int,    
@RolemasterId INT,  
@ExpertiseAreaId int,    
@ExpertiseDescription varchar(250),    
@CreatedUser VARCHAR(100),      
@CreatedDate DATETIME,    
@SystemInfo VARCHAR(50)    
AS          
BEGIN      
SET NOCOUNT ON;    
    
 DECLARE @requisitionexpertiseid INT    
      
 IF EXISTS(SELECT requisitionexpertiseid FROM requisitionexpertiserequirements WHERE trid = @Talentrequisitionid
	       AND expertiseareaid = @Expertiseareaid AND RolemasterId = @RolemasterId)    
 BEGIN
	SELECT @Requisitionexpertiseid = requisitionexpertiseid FROM requisitionexpertiserequirements
	WHERE trid = @Talentrequisitionid AND expertiseareaid = @Expertiseareaid AND RolemasterId = @RolemasterId
	
	UPDATE requisitionexpertiserequirements SET expertisedescription=@expertisedescription
	WHERE requisitionexpertiseid=@requisitionexpertiseid
 END
 ELSE    
 BEGIN    
	INSERT INTO [dbo].[requisitionexpertiserequirements]          
	(    
		 Trid,    
		 RolemasterId,  
		 Expertiseareaid,    
		 Expertisedescription,    
		 Createdby,    
		 Createddate,    
		 Modifiedby,    
		 Modifieddate,    
		 Systeminfo    
	)          
	VALUES          
	(    
		  @Talentrequisitionid    
		 ,@Rolemasterid  
		 ,@Expertiseareaid    
		 ,@Expertisedescription    
		 ,@Createduser      
		 ,@Createddate    
		 ,@Createduser    
		 ,@Createddate    
		 ,@Systeminfo      
	)          
 END          
 SELECT @@ROWCOUNT      
END 