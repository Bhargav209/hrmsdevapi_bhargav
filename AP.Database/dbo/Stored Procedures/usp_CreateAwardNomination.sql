﻿-- ============================================          
-- Author        : Basha          
-- Create date   : 14-05-2018          
-- Modified date : 16-05-2018              
-- Modified By   : Basha          
-- Description   : Create Nomination    
-- ============================================      
CREATE PROCEDURE [dbo].[usp_CreateAwardNomination]    
(      
     
       @AwardId INT ,
       @AwardTypeId INT ,
       @ADRCycleId INT ,
       @FinancialYearId INT,
       @NominationDescription VARCHAR(100)  ,
       @FromEmployeeId INT  ,
       @NomineeComments VARCHAR(500)  ,
       @ReviewerComments VARCHAR(500) = NULL ,
       @DepartmentId INT,
       @ProjectId INT,
       @CreatedBy VARCHAR(100)  ,
       @CreatedDate DATETIME  ,
       @ModifiedBy VARCHAR(100) NULL ,
       @ModifiedDate DATETIME NULL ,     
       @SystemInfo VARCHAR(50),   
       @ToEmployeeId INT
)      
AS        
BEGIN      
 SET NOCOUNT ON;   

DECLARE @AwardNominationId INT = 0;
SELECT @AwardNominationId = AwardNominationId FROM [AwardNomination] WHERE AwardId = @AwardId AND 
              ADRCycleId = @ADRCycleId AND FinancialYearId = @FinancialYearId AND AwardTypeId = @AwardTypeId AND NominationDescription = @NominationDescription 


--Selecting StatusId when its Draft i.e, when its not submitted 
DECLARE @StatusId INT = 0;
SELECT  @StatusId =  status.StatusId FROM [dbo].[Status] status 
                            JOIN CategoryMaster categoryMaster ON status.CategoryID = categoryMaster.CategoryID 
                                                                where status.StatusCode ='Draft' AND categoryMaster.CategoryName ='ADR'  

       

   IF (@AwardNominationId=0)
       BEGIN
              -- inserting into Award Nomintion table for new record
              INSERT INTO [dbo].[AwardNomination]
                       (    
                           AwardId,
                           AwardTypeId,
                           ADRCycleId ,
                           FinancialYearId,
						   StatusId,
                           NominationDescription,
                           FromEmployeeId ,
                           NomineeComments ,
                           ReviewerComments,
                           DepartmentId,
                           ProjectId,
                           CreatedBy ,
                           CreatedDate,
                           SystemInfo    
                       )    
                      VALUES    
                      (    
                           @AwardId,
                           @AwardTypeId,
                           @ADRCycleId ,
                           @FinancialYearId,
						   @StatusId,
                           @NominationDescription,
                           @FromEmployeeId ,
                           @NomineeComments ,
                           @ReviewerComments,
                           @DepartmentId,
                           @ProjectId,
                           @CreatedBy  ,
                           @CreatedDate,
                           @SystemInfo     
                       ) 
              SET @AwardNominationId = SCOPE_IDENTITY()
       END
      -- inserting into AwardEmployeeMapper table for new record
              INSERT INTO [dbo].[AwardEmployeeMapper]
                (                  
                     AwardNominationId,
                     EmployeeId,                        
                     CreatedBy,
                     CreatedDate,
                     SystemInfo
                )
              VALUES
              (             
                      @AwardNominationId
                     ,@ToEmployeeId
                     ,@CreatedBy
                     ,@CreatedDate
                     ,@SystemInfo   
              ) 
   SELECT COUNT(*)  FROM [dbo].[AwardEmployeeMapper] WHERE  AwardNominationId = @AwardNominationId       
END
