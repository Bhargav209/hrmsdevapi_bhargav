﻿-- ================================================  
-- Author			: Ramya Singamsetti      
-- Create date		: 24-11-2017      
-- Modified date	: 29-12-2017
-- Modified By		: Sushmitha    
-- Description		: Gets Expertise Area by Talent requisition ID
-- exec [dbo].[usp_SubmitRequisitionByTrId] 989
-- ================================================  
CREATE PROCEDURE [dbo].[usp_SubmitRequisitionByTrId]
@Talentrequisitionid INT,
@StatusId INT,
@FromEmployeeId INT,
@ToEmployeeId VARCHAR(50),
@Modifieduser VARCHAR(100),
@Modifieddate DATETIME,
@Systeminfo VARCHAR(50)
AS      
BEGIN  
 SET NOCOUNT ON;


  UPDATE 
	[dbo].[TalentRequisition]
  SET 
    RaisedBy=@FromEmployeeId,
	Statusid=@Statusid, 
	Modifieduser=@Modifieduser,
	Modifieddate=@Modifieddate,
	Systeminfo=@Systeminfo 
  WHERE 
	Trid=@Talentrequisitionid

  INSERT INTO 
  [dbo].[TalentRequisitionWorkFlow] 
	([TalentRequisitionID]
      ,[StatusID]
      ,[FromEmployeeID]
      ,[ToEmployeeID]
      ,[CreatedBy]
      ,[CreatedDate])
  VALUES 
	  (@Talentrequisitionid
	  ,@StatusId
	  ,@FromEmployeeId
	  ,@ToEmployeeId
	  ,@Modifieduser
	  ,@Modifieddate)
	  

  SELECT @@ROWCOUNT

END