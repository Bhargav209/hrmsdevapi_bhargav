﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	05-06-2018            
-- Modified date	:	05-06-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets department types list
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetDepartmentTypes]
AS
BEGIN
 SET NOCOUNT ON;

	--To get department types
	SELECT
		 DepartmentTypeId AS Id
		,DepartmentTypeDescription AS Name
	FROM
		[dbo].[DepartmentType]

END
