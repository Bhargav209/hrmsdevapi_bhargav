﻿ -- ===============================================
 -- Author			:	Kalyan
 -- Create date	:	28-08-2018
 -- =======================================

CREATE PROCEDURE [dbo].[usp_ManageAssociateSkills] 
	@ID INT, 
	@EmployeeId INT, 
	@CompetencyAreaId INT, 
	@SkillId INT, 
	@ProficiencyLevelId INT, 
	@Experience INT, 
	@LastUsed INT, 
	@IsPrimary BIT, 	
	@CreatedUser VARCHAR(100),
	@CreatedDate DATETIME,	
	@SystemInfo VARCHAR(50),
	@SkillGroupId INT
AS
BEGIN
SET NOCOUNT ON;
	UPDATE 
		[dbo].[EmployeeSkills]
	 SET
		ModifiedDate = GETDATE()
	 WHERE 
		EmployeeId = @EmployeeId 
	AND 
		SkillId = @SkillId

IF(@@ROWCOUNT = 0)

	BEGIN
		INSERT INTO [dbo].[EmployeeSkills] 
		( 
			[EmployeeId],
			[CompetencyAreaId],
			[SkillId],
			[ProficiencyLevelId],
			[Experience],
			[LastUsed],
			[IsPrimary],
			[SkillGroupId],
			[CreatedUser],
			[CreatedDate],
			[SystemInfo],
			[IsActive]
		)
	VALUES 
		(		
			@EmployeeId,
			@CompetencyAreaId,
			@SkillId,
			@ProficiencyLevelId,
			@Experience,
			@LastUsed,
			@IsPrimary,
			@SkillGroupId,
			@CreatedUser,
			@CreatedDate,
			@SystemInfo,
			1
		)
		SELECT @@ROWCOUNT
	END
	ELSE
		BEGIN
			SELECT -1
		END
END


