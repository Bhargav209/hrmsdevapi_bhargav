﻿-- =============================================
-- Author        : Sushmitha            
-- Create date   : 15-11-2017              
-- Modified date :            
-- Modified By   : Sushmitha              
-- Description   : Gets all the joined associates     
-- =============================================    
CREATE PROCEDURE [dbo].[usp_GetJoinedAssociates]             
AS              
BEGIN          
          
 SET NOCOUNT ON;          
           
 SELECT      
    employee.EmployeeId  
   ,employee.FirstName + ' ' +employee.LastName AS EmployeeName  
   ,employee.Designation  
   ,employee.JoinDate  
   ,employee.HRAdvisor  
   ,practiceArea.PracticeAreaCode AS TechnologyName 
   ,designation.DesignationName  
   ,dept.DepartmentCode  
 FROM [dbo].[Employee] employee  
 INNER JOIN [dbo].[Departments] dept  
 ON employee.DepartmentId = dept.DepartmentId   
 INNER JOIN [dbo].[Designations] designation  
 ON employee.Designation = designation.DesignationId  
 LEFT JOIN [dbo].[PracticeArea] practiceArea  
 ON employee.CompetencyGroup = practiceArea.PracticeAreaId  
 WHERE employee.IsActive = 1 AND (employee.StatusId IS NULL OR employee.StatusId IN (2, 3))
 ORDER BY JoinDate Asc
END