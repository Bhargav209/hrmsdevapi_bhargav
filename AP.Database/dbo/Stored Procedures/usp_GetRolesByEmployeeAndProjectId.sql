﻿-- =================================================   
-- Author			:	Sushmitha            
-- Create date		:	01-03-2018           
-- Modified date	:	01-03-2018             
-- Modified By		:	Sushmitha            
-- Description		:	Gets roles by employee and project id   
-- ================================================= 

CREATE PROCEDURE [dbo].[usp_GetRolesByEmployeeAndProjectId]
@ProjectId INT,
@EmployeeId INT
AS
BEGIN
 SET NOCOUNT ON;

 SELECT
	[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
	,allocation.RoleMasterID AS RoleId
 FROM
	[dbo].[AssociateAllocation] allocation
	INNER JOIN [dbo].[RoleMaster] rolemaster
	ON allocation.RoleMasterId = rolemaster.RoleMasterID
 WHERE
	allocation.EmployeeId = @EmployeeId AND allocation.ProjectId = @ProjectId AND allocation.IsActive = 1 AND rolemaster.IsActive = 1
 END

