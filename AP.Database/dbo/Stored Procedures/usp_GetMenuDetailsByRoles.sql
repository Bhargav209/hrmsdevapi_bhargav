﻿-- ==============================================         
-- Author            : Santosh  
-- Create date       : 16-11-2017  
-- Modified date     : 07-06-2018  
-- Modified By       : Basha
-- Description       : GetMenuDetailsByRoles  
-- ==============================================        
CREATE PROCEDURE [dbo].[usp_GetMenuDetailsByRoles] 
(
	@Roles VARCHAR(150)        
)
AS  
BEGIN
	
	SET NOCOUNT ON;  
    
	DECLARE  @ChildTable TABLE
	(
		SNo INT IDENTITY(1,1),
        MenuId INT
    )
    --Get Child Menus
    INSERT INTO @ChildTable
    SELECT DISTINCT  
		mm.MenuId
	FROM [dbo].[MenuRoles] mr  
    INNER JOIN [dbo].[MenuMaster] mm  
    ON mr.MenuId = mm.MenuId  
    INNER JOIN [dbo].[Roles] r  
    ON mr.RoleId =  r.RoleId  
    WHERE mm.IsActive = 1 AND r.RoleName IN (SELECT Value FROM [UDF_SplitString](@Roles, ','))    

    --Get Parent Menus
    INSERT INTO @ChildTable
    SELECT DISTINCT 
            parent.MenuId  
    FROM MenuMaster child
    INNER JOIN MenuMaster parent
    ON child.ParentId = parent.MenuId
    INNER JOIN[dbo].[MenuRoles] mr  
    ON child.MenuId = mr.MenuId
    INNER JOIN [dbo].[Roles] r  
    ON mr.RoleId =  r.RoleId  
    WHERE child.IsActive = 1 AND r.RoleName IN (SELECT Value FROM [UDF_SplitString](@Roles, ',')) AND parent.ParentId = 0  

	--Get Dashboard and viewKra  Menus
	DECLARE  @AllMenuTable TABLE
	(
		SNo INT IDENTITY(1,1),
        MenuId INT
    )
  INSERT INTO @AllMenuTable
    SELECT parent.MenuId  
    FROM AllMenus allmenus
    INNER JOIN MenuMaster parent
    ON allmenus.MenuId = parent.MenuId

	DECLARE  @ParentTable TABLE
    (
		SNo INT IDENTITY(1,1),
		MenuId INT
    )
	INSERT INTO @ParentTable
    SELECT DISTINCT 
		parent.MenuId  
    FROM MenuMaster child
    INNER JOIN MenuMaster parent
    ON child.ParentId = parent.MenuId
    INNER JOIN[dbo].[MenuRoles] mr  
    ON child.MenuId = mr.MenuId
    INNER JOIN [dbo].[Roles] r  
    ON mr.RoleId =  r.RoleId  
    WHERE child.IsActive = 1 AND r.RoleName IN (SELECT Value FROM [UDF_SplitString](@Roles, ','))  AND parent.ParentId <> 0    

    DECLARE @TotalRecords INT = 0
    DECLARE @Count INT = 1
    DECLARE @MenuId INT = 0
    DECLARE @ParentId INT = 0
    SELECT @TotalRecords = COUNT(1) FROM @ParentTable

    WHILE (@Count <= @TotalRecords) 
    BEGIN
		SELECT @MenuId = MenuId FROM @ParentTable WHERE SNo = @Count
        
		SELECT @ParentId = ParentId FROM MenuMaster WHERE MenuId = @MenuId
        
		WHILE @ParentId <> 0
        BEGIN
			SELECT @ParentId = ParentId, @MenuId = MenuId FROM MenuMaster WHERE MenuId = @ParentId
            
			INSERT INTO @ParentTable 
            SELECT @MenuId
			
			SET @ParentId = @ParentId
        END
        SET @Count = @Count + 1
    END    

    --Get all the menu along with parent menus
    SELECT  
		 MenuId  
        ,Title  
        ,IsActive  
        ,[Path]  
        ,DisplayOrder  
        ,ParentId  
        ,Parameter  
        ,NodeId  
        ,Style  
        FROM MenuMaster
        WHERE MenuMaster.MenuId IN (SELECT MENUID FROM @ChildTable
									UNION
									SELECT MENUID FROM @ParentTable
									UNION
									SELECT MENUID FROM @AllMenuTable
									)
END
