﻿-- =================================================    
-- AUTHOR		: PRASANNA    
-- CREATE DATE  : 28-01-2019    
-- MODIFIED DATE: 28-01-2019    
-- MODIFIED BY  : MITHUN    
-- DESCRIPTION  : GET WISH LIST NAMES TO POPULATE
-- =================================================    
CREATE PROCEDURE [dbo].[USP_GetWishListNames]    
(  
  @MANAGERID INT  
)    
AS                  
BEGIN         

 SET NOCOUNT ON;
 SELECT max(ID) as Id,WISHLISTNAME 
 FROM WISHLIST 
 WHERE MANAGERID=@MANAGERID
 group by WISHLISTNAME

END