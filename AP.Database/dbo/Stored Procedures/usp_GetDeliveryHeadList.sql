﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	22-11-2017            
-- Modified date	:	28-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all delivery heads       
-- ============================================  
CREATE PROCEDURE [dbo].[usp_GetDeliveryHeadList]                 
AS                  
BEGIN              
              
 SET NOCOUNT ON;    
                 
  SELECT     
	employee.EmployeeId,    
	[dbo].udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName    
 FROM 
	[dbo].[Employee] employee    
	INNER JOIN [dbo].[Users] [user]    
	ON employee.UserId = [user].UserId     
	INNER JOIN [dbo].[UserRoles] userrole    
	ON [user].UserId = userrole.UserId     
 WHERE 
	employee.IsActive = 1 AND userrole.IsActive = 1 AND userrole.RoleId = (SELECT RoleId FROM [dbo].[Roles] WHERE RoleName = 'Delivery Head') and [user].IsActive=1    
 END