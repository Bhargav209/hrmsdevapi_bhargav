﻿-- =============================================
-- Author:		Bhavani
-- Create date: 31-08-2018
-- Description:	[dbo].[usp_ValidatePersonalData]
-- [dbo].[usp_ValidatePersonalData] 101,'chinnu@gmail.com','sSaCJLJtf4aWhmKj4AskgA==', 'ply2nh8SNYqGo6dI/CCH/w==', 'PAN', 'ZeJkRMmidQ/gBOKF8DGEBQ==','2/GTvjG1suv13VxWQPslWA==','PASS'
--[dbo].[usp_ValidatePersonalData] 101,'1@gmail.com','SaCJLJtf4aWhmKj4AskgA==', 'ly2nh8SNYqGo6dI/CCH/w==', 'PAN', 'eJkRMmidQ/gBOKF8DGEBQ==','/GTvjG1suv13VxWQPslWA==','PASS'
-- =============================================
CREATE PROCEDURE [dbo].[usp_ValidatePersonalData]
@EmployeeId int,
@PersonalEmailAddress varchar(100), --1
@MobileNo varchar(30), --2
@AadharNumber varchar(50) null, --3
@PANNumber varchar(50) null, --4
@PFNumber varchar(50) null, --5
@UANNumber varchar(50) null, --6
@PassportNumber varchar(50) null --7

AS

DECLARE @DuplicateFields Table
(
Id int
);

BEGIN

SET NOCOUNT ON;
		
		IF(@PersonalEmailAddress != '' AND @PersonalEmailAddress IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND PersonalEmailAddress=@PersonalEmailAddress)
		BEGIN
		INSERT INTO @DuplicateFields(Id) VALUES(1)
		END
		END

		IF(@MobileNo != '' AND @MobileNo IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND MobileNo=@MobileNo)
		BEGIN
			INSERT INTO @DuplicateFields(Id) VALUES(2)
		END
		END

		IF(@AadharNumber != '' AND @AadharNumber IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND AadharNumber=@AadharNumber)
		BEGIN
			INSERT INTO @DuplicateFields(Id) VALUES(3)
		END
		END

		IF(@PANNumber != '' AND @PANNumber IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND PANNumber=@PANNumber)
		BEGIN
			INSERT INTO @DuplicateFields(Id) VALUES(4)
		END
		END

		IF(@PFNumber != '' AND @PFNumber IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND PFNumber=@PFNumber)
		BEGIN
			INSERT INTO @DuplicateFields(Id) VALUES(5)
		END
		END

		IF(@UANNumber != '' AND @UANNumber IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND UANNumber=@UANNumber)
		BEGIN
			INSERT INTO @DuplicateFields(Id) VALUES(6)
		END
		END

		IF(@PassportNumber != '' AND @PassportNumber IS NOT NULL)
		BEGIN
		IF EXISTS(SELECT 1 FROM Employee WHERE EmployeeId != @EmployeeId AND IsActive=1 AND PassportNumber=@PassportNumber)
		BEGIN
			INSERT INTO @DuplicateFields(Id) VALUES(7)
		END
		END

		SELECT Id FROM @DuplicateFields
END


GO


