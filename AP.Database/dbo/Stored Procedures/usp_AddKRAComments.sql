﻿-- ==================================     
-- Author			:  Sushmitha            
-- Create date		:  20-06-2018    
-- Modified date	:  20-06-2018    
-- Modified By		:  Sushmitha    
-- Description		:  Add KRA Comments    
-- ================================== 

CREATE PROCEDURE [dbo].[usp_AddKRAComments]
(
 @KRAGroupId INT 
,@FinancialYearId INT     
,@FromEmployeeId INT  
,@Comments VARCHAR(MAX)  
,@CommentedDate DATETIME
,@StatusID INT
)
AS
BEGIN

 DECLARE @ApprovedStatusId INT

 SET @ApprovedStatusId = (SELECT [dbo].[udf_GetStatusId]('Approved', 'KRA'))

 IF (@ApprovedStatusId = @StatusID)
 BEGIN
	SELECT -12 --Cannot add comments as KRAs are already approved.
 END
 ELSE
 BEGIN
	IF @Comments <> ''  
	BEGIN   
	  INSERT INTO [dbo].[KRAComment]
		(
			KRAGroupId
		   ,FinancialYearId
		   ,EmployeeId
		   ,Comments
		   ,CommentedDate
		)
	  VALUES
		(
			 @KRAGroupId
			,@FinancialYearId
			,@FromEmployeeId
			,@Comments
			,@CommentedDate
		)
	END
 END
  SELECT @@ROWCOUNT
END

