﻿-- =======================================     
-- Author			:	Santosh          
-- Create date		:	25-05-2018     
-- Modified date	:	25-05-2018            
-- Modified By		:	Santosh          
-- Description		:	Updates kra scale master    
-- =======================================
CREATE PROCEDURE [dbo].[usp_UpdateKRAScaleDetails]         
  @tblScaleDetails ScaleDetailsType READONLY  
AS          
BEGIN

	SET NOCOUNT ON;

	 MERGE INTO [dbo].[KRAScaleDetails] kraScaleDetails
	 USING @tblScaleDetails kraScaleDetailsType
     ON kraScaleDetails.KRAScaleDetailID = kraScaleDetailsType.[Id]
     WHEN MATCHED THEN
     UPDATE SET kraScaleDetails.ScaleDescription = kraScaleDetailsType.[Description];
END