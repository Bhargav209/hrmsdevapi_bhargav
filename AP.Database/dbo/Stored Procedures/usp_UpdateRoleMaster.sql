﻿-- ========================================================
-- Author			:	Santosh 
-- Create date		:	13-02-2018
-- Modified date	:	13-02-2018
-- Modified By		:	Santosh
-- Description		:	Update role.
-- ========================================================   
  
CREATE PROCEDURE [dbo].[usp_UpdateRoleMaster]
(  
  @RoleMasterID INT  
 ,@RoleDescription NVARCHAR(500) NULL
 ,@KeyResponsibilities NVARCHAR(MAX)     
 ,@EducationQualification NVARCHAR(MAX)
 ,@ModifiedDate   DATETIME
 ,@ModifiedUser   VARCHAR(150)
 ,@SystemInfo   VARCHAR(50)
 ,@KRAGroupID INT 
)  
AS  
BEGIN

	SET NOCOUNT ON;

		UPDATE [dbo].[RoleMaster]  
		SET  
			RoleDescription 			    =   @RoleDescription        
			,KeyResponsibilities    	    =   @KeyResponsibilities    
			,EducationQualification   	    =   @EducationQualification 
			,ModifiedDate    			    =   @ModifiedDate  
			,ModifiedUser	      			=   @ModifiedUser    
			,SystemInfo    				    =   @SystemInfo 
			,KRAGroupID                     =   @KRAGroupID   
		WHERE RoleMasterID = @RoleMasterID

		SELECT @@ROWCOUNT    
END