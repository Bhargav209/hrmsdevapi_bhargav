﻿-- ===============================================    
-- Author   : Santosh    
-- Create date  : 20-04-2018    
-- Modified date : 01-03-2018    
-- Modified By  : Sabiha    
-- Description  : Get domain wise report    
 -- ==============================================    
CREATE PROCEDURE [dbo].[usp_rpt_DomainCountReport]    
AS              
BEGIN    
     
 SET NOCOUNT ON;    
     
 SELECT     
 COUNT(Distinct employeeprojects.EmployeeId) ResourceCount    
 ,employeeprojects.DomainID    
 ,domain.DomainName    
 FROM EmployeeProjects employeeprojects    
 INNER JOIN Domain domain      
 ON employeeprojects.DomainID = domain.DomainID    
 GROUP BY employeeprojects.DomainID, domain.DomainName   
END  