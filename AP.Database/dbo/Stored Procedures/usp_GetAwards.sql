﻿-- =================================================
-- Author			:	Basha
-- Create date		:	15-05-2018
-- Modified date	:	15-05-2018
-- Modified By		:	Basha
-- Description		:	Get all the Awards 
-- =================================================
CREATE PROCEDURE [dbo].[usp_GetAwards]
AS              
BEGIN     

 SET NOCOUNT ON;           
 SELECT AwardId AS Id,
 AwardTitle AS Name 
 FROM [dbo].[Award]              
END