﻿-- ========================================================    
-- Author			:	Ramya Singamsetti
-- Create date		:	28-11-2017
-- Modified date	:   05-06-2018
-- Modified by		:   Santosh
-- Description		:	Save requisition learning aptitude
-- ========================================================    
CREATE PROCEDURE [dbo].[usp_SaveRequisitionLearningAptitude]  
@TalentRequisitionId INT,  
@RoleMasterId INT,
@Learningaptitudeid INT,  
@Createduser VARCHAR(100),    
@Createddate DATETIME,  
@Systeminfo VARCHAR(50)  
AS        
BEGIN    
    
SET NOCOUNT ON;  
  
 DECLARE @requisitionlearningaptitudeid INT  
    
 IF EXISTS(SELECT requisitionlearningaptitudeid FROM requisitionlearningaptitudes WHERE trid = @Talentrequisitionid AND RoleMasterId = @RoleMasterId)  
 BEGIN  
	SELECT @requisitionlearningaptitudeid = requisitionlearningaptitudeid  
	FROM requisitionlearningaptitudes   
	WHERE trid = @talentrequisitionid AND RoleMasterId = @RoleMasterId  
   
	UPDATE requisitionlearningaptitudes   
	SET learningaptitudeid = @learningaptitudeid   
	WHERE requisitionlearningaptitudeid = @requisitionlearningaptitudeid  
 END  
 ELSE  
 BEGIN  
	INSERT INTO [dbo].[requisitionlearningaptitudes]        
	(  
		Trid, 
		RoleMasterId,
		Learningaptitudeid,    
		Createdby,  
		Createddate,  
		Modifiedby,  
		Modifieddate,  
		Systeminfo  
	)        
	VALUES        
	(  
		 @Talentrequisitionid
		,@RoleMasterId  
		,@Learningaptitudeid  
		,@Createduser    
		,@Createddate  
		,@Createduser  
		,@Createddate  
		,@Systeminfo    
	)        
 END
         
 SELECT @@ROWCOUNT    
END