﻿-- ===============================================                    
 -- Author   : Basha                    
 -- Create date : 28-08-2018                    
 -- =======================================                    
                    
CREATE PROCEDURE [dbo].[usp_UpdateSkillStatusAssociateWorkFlow]                            
  @EmployeeId INT,                                        
  @SubmittedRequisitionId INT                
                
AS                    
BEGIN                    
SET NOCOUNT ON;                                        
  BEGIN TRY            
     UPDATE   
    [dbo].[SkillsWorkFlow]       
     SET   
    [Status]=18    
     WHERE   
    SubmittedBy = @EmployeeId and [Status]=20  and SubmittedRequisitionId=  @SubmittedRequisitionId            
                                
  SELECT @@ROWCOUNT                    
  END TRY      
  BEGIN CATCH     
  SELECT -4    
  END CATCH     
                
END 