﻿-- =======================================
-- Author			:  Santosh
-- Create date		:  21-04-2018
-- Modified date	:  21-05-2018
-- Modified By		:  Santosh
-- Description		:  Get KRA Comments
-- =======================================       
CREATE PROCEDURE [dbo].[usp_GetKRAComments]
 @FinancialYearId INT    
,@KRAGroupId INT
AS          
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		 [dbo].[udfGetEmployeeFullName](EmployeeId) AS EmployeeName
		,Comments
		,CommentedDate
	FROM [dbo].[KRAComment]
	WHERE KRAGroupId = @KRAGroupId AND FinancialYearId = @FinancialYearId
	ORDER BY CommentedDate DESC
END