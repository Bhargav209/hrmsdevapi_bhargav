﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 11-01-2019    
-- Modified date : 11-01-2019    
-- Modified By  : Mithun    
-- Description  : update SOW  Details    
-- =================================================   

--exec [usp_UpdateSOWDetails] 2, 'wos87',22,'test','2019/01/16','Department Head','2019/01/17','fdgfdg','local'

CREATE PROCEDURE [dbo].[usp_UpdateSOWDetails]    
(   
@Id int,
  @SOWId VARCHAR(20) 
 ,@ProjectId INT
 ,@SOWFileName VARCHAR(50)
 ,@SOWSignedDate DATETIME
 ,@RoleName VARCHAR(50)  
 ,@ModifiedDate DATETIME
 ,@ModifiedUser VARCHAR(100)
 ,@SystemInfo VARCHAR(100)
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON;
 DECLARE @RoleId INT


 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)
 IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head  
  BEGIN 
      
    UPDATE SOW SET 
      SOWId=@SOWId
     ,SOWFileName=@SOWFileName
     ,SOWSignedDate=@SOWSignedDate 
     ,ModifiedDate = @ModifiedDate
     ,ModifiedUser = @ModifiedUser
     ,SystemInfo = @SystemInfo  
    FROM SOW sow    
    WHERE sow.Id=@Id AND ProjectId=@ProjectId
	
	SELECT @@ROWCOUNT
 END
ELSE
  SELECT -2      
END
