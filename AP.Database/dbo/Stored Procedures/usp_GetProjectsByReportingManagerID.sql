﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	26-03-2018            
-- Modified date	:	26-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all the projects under a reporting manager.
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetProjectsByProjectManagerID]
(    
 @ProjectManagerID INT 
)         
AS           
BEGIN 
	SET NOCOUNT ON;  

	SELECT
		 project.ProjectId AS Id
	    ,project.ProjectName AS Name
	FROM [dbo].[Projects] project
	INNER JOIN [dbo].[ProjectManagers] projectManagers
	ON project.ProjectId = projectManagers.ProjectId
	WHERE 
		projectManagers.ProgramManagerID = @ProjectManagerID and projectManagers.IsActive=1 and project.IsActive=1
END 
