﻿-- ==========================================
-- Author			:	Chandra
-- Create date		:	12-14-2018
-- Modified date	:	12-14-2018               
-- Modified By		:	Chandra      
-- Dsacription		:	Gets Employee skills
-- [dbo].[usp_GetEmployeeSkills]
 -- ==========================================      
CREATE PROCEDURE [dbo].[usp_GetEmployeeSkills] 
AS          
BEGIN      
      
 SET NOCOUNT ON;

SELECT    
  sa.ID as SkillsSubmittedForApprovalId
 ,emp.FirstName  as EmployeeName
 ,emp.EmployeeId  as empID
 ,s.SkillName  as SkillName
 ,sa.CompetencyAreaId  as CompetencyAreaId
 ,sa.SkillId  as SkillId
 ,sa.ProficiencyLevelId  as ProficiencyLevelId
 ,sa.SkillGroupId  as SkillGroupId
 ,sa.Experience  as experience
 ,sa.LastUsed as LastUsed
 ,sa.IsPrimary as  isPrimary
 ,pl.ProficiencyLevelCode as ProficiencyLevel
 ,st.StatusCode as StatusCode
 ,swf.WorkFlowId as WorkFlowId
 FROM SkillsSubmittedForApproval sa  
 INNER JOIN Employee emp    
 ON sa.EmployeeId = emp.EmployeeId  
 INNER JOIN Skills s    
 ON sa.SkillId = s.SkillId    
 INNER JOIN ProficiencyLevel pl    
 ON sa.ProficiencyLevelId = pl.ProficiencyLevelId
 INNER JOIN SkillsWorkFlow swf    
 ON sa.ID = swf.SubmittedRequisitionId 
 INNER JOIN Status st    
 ON swf.Status = st.StatusId
 where st.StatusCode='SubmittedForApproval'

END