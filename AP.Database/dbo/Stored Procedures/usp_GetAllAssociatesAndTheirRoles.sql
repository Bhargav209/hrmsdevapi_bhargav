﻿-- ==================================================  
-- Author                  :  Ramya.Singamsetti
-- Create date             :  20-06-2018     
-- Modified date     :      
-- Modified By             :      
-- Description             :  Method to get all associates and their roles. 
-- ====================================================
CREATE PROCEDURE [dbo].[usp_GetAllAssociatesAndTheirRoles]   
AS
BEGIN

-- Service Department
    SELECT DISTINCT 
		 [serviceDepartmentRoles].EmployeeID AS EmployeeId
		,[emp].EmployeeCode
		,[dbo].[udfGetEmployeeFullName](serviceDepartmentRoles.EmployeeId) AS AssociateName
		,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
		,[rolemaster].RoleMasterID
	FROM ServiceDepartmentRoles serviceDepartmentRoles
	INNER JOIN [Employee] AS [emp] ON [emp].EmployeeId=serviceDepartmentRoles.EmployeeID
	INNER JOIN [RoleMaster] AS [rolemaster] ON serviceDepartmentRoles.RoleMasterID = [rolemaster].[RoleMasterID] 
	WHERE serviceDepartmentRoles.IsActive=1 and emp.IsActive=1 and rolemaster.IsActive=1

UNION
-- Delivery Department
SELECT DISTINCT 
		 [allocation].EmployeeID AS EmployeeId
		,[emp].EmployeeCode
		,[dbo].[udfGetEmployeeFullName](allocation.EmployeeId) AS AssociateName
		,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
		,rolemaster.RoleMasterID
	FROM AssociateAllocation allocation
	INNER JOIN [Employee] AS [emp] ON [emp].EmployeeId=allocation.EmployeeID and emp.IsActive=1
	INNER JOIN [RoleMaster] AS [rolemaster] ON allocation.RoleMasterID = [rolemaster].[RoleMasterID] and rolemaster.IsActive=1
	WHERE allocation.IsPrimary=1 and allocation.IsActive=1

END
