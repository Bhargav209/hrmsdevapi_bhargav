﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	13-03-2018            
-- Modified date	:	13-03-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Gets all associates under a department      
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetAssociatesByDepartmentId]
(
@DepartmentID INT
)
AS
BEGIN

SET NOCOUNT ON;  
	
	SELECT
		 EmployeeId AS Id
		,[dbo].[udfGetEmployeeFullName](EmployeeId) AS Name
	FROM 
		[dbo].[Employee]
	WHERE DepartmentId = @DepartmentID AND IsActive = 1 AND FirstName != 'default'

END