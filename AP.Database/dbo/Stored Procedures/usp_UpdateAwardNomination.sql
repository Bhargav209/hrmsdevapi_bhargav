﻿-- ============================================
-- Author			:	Basha   
-- Create date		:	15-05-2018      
-- Modified date	:	15-05-2018
-- Modified By		:	Basha
-- Description		:	Update Award Nomination
-- ============================================  
CREATE PROCEDURE [dbo].[usp_UpdateAwardNomination]
(  
	  @AwardId INT,
	  @AwardTypeId INT,
	  @AwardNominationId INT,
	  @NominationDescription VARCHAR(100)  ,
	  @NomineeComments VARCHAR(500)  ,
	  @ReviewerComments VARCHAR(500)= NULL,
	  @ToEmployeeId INT,
	  @CreatedBy VARCHAR(100) ,
	  @CreatedDate DATETIME,
	  @ModifiedBy VARCHAR(100) NULL ,
	  @ModifiedDate DATETIME NULL ,
	  @SystemInfo VARCHAR(50),
	  @RoleName VARCHAR(150),
      @EmployeeID INT 
)
AS
BEGIN
 SET NOCOUNT ON;

  --Check for Project Manager
   DECLARE @ProjectManager INT = 0   
   SELECT @ProjectManager = ReportingManagerID from ProjectManagers WHERE ReportingManagerID = @EmployeeID 
   IF(@RoleName = 'Project Manager' AND @ProjectManager > 0)
   BEGIN
			   UPDATE [dbo].[AwardNomination]
				SET
					  AwardId				 =  @AwardId,
					  AwardTypeId			 =  @AwardTypeId,
					  NominationDescription	 =	@NominationDescription
					 ,NomineeComments        =  @NomineeComments
					 ,ModifiedDate			 =	@ModifiedDate
					 ,ModifiedBy		     =	@ModifiedBy
					 ,SystemInfo			 =	@SystemInfo
			   WHERE AwardNominationId		 =  @AwardNominationId

			--Insert to AwardEmployeeMapper if To employee not exists
			SELECT @ToEmployeeId = EmployeeId FROM [AwardEmployeeMapper] WHERE  EmployeeId = @ToEmployeeId
			IF (@ToEmployeeId = 0)
				BEGIN
					INSERT INTO [dbo].[AwardEmployeeMapper]
						(
						AwardNominationId,
						EmployeeId,			
						CreatedBy,
						CreatedDate,
						SystemInfo
						)
					VALUES
					(
						@AwardNominationId
					   ,@ToEmployeeId
					   ,@CreatedBy
					   ,@CreatedDate
					   ,@SystemInfo   
					)
				END
	END

	--Check for Deaprtment Head  
	DECLARE @DepartmentHeadCount INT = 0
	SELECT @DepartmentHeadCount = COUNT(*) FROM Departments WHERE DepartmentHeadID = @EmployeeID
	IF((@RoleName = 'Department Head' AND @DepartmentHeadCount > 0) OR (@RoleName = 'HR Head') )	  
	BEGIN
				UPDATE [dbo].[AwardNomination]
							SET
								ReviewerComments         =  @ReviewerComments
							WHERE AwardNominationId =  @AwardNominationId
	END

	 SELECT COUNT(*)  FROM [dbo].[AwardEmployeeMapper] WHERE  AwardNominationId = @AwardNominationId 

END 


	