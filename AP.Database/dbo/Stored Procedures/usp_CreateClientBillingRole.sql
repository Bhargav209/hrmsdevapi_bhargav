-- ======================================================        
-- Author   : Praveen          
-- Create date  :   
-- Modified date : 01-03-2019      
-- Modified By  : Sabiha      
-- Description  : Create Client Billing Role  
-- [dbo].[usp_CreateClientBillingRole]  
-- ======================================================   
CREATE PROCEDURE [dbo].[usp_CreateClientBillingRole]     
(  
 @ClientBillingRoleName VARCHAR(70)  
 ,@ProjectId INT  
 ,@IsActive BIT = 1  
 ,@NoOfPositions INT  
 ,@CreatedDate   DATETIME  
 ,@CreatedUser   VARCHAR(100)  
 ,@SystemInfo   VARCHAR(50)     
 ,@StartDate DATE NULL  
 ,@ClientBillingPercentage INT NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON;   
   
 INSERT INTO [dbo].[ClientBillingRoles]  
           ([ClientBillingRoleName]  
     ,[ProjectId]      
     ,[IsActive]  
     ,[NoOfPositions]  
     ,[StartDate]  
           ,[CreatedUser]             
           ,[CreatedDate]             
           ,[SystemInfo]  
     ,[ClientBillingPercentage])  
     VALUES  
           (@ClientBillingRoleName  
           ,@ProjectId  
     ,@IsActive  
     ,@NoOfPositions  
     ,@StartDate  
           ,@CreatedUser             
           ,@CreatedDate            
           ,@SystemInfo  
     ,@ClientBillingPercentage)    
   
 SELECT @@ROWCOUNT  
  
END   