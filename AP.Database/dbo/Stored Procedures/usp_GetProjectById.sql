﻿
CREATE PROCEDURE [dbo].[usp_GetProjectById]    
 (    
  @ProjectId INT    
 )                 
AS                    
BEGIN                
               
 SET NOCOUNT ON;    
     
  SELECT DISTINCT    
   project.ProjectId AS ProjectId    
  ,project.ProjectCode AS ProjectCode    
  ,project.ProjectName AS ProjectName   
  ,project.ActualStartDate AS ActualStartDate    
  ,project.ActualEndDate AS ActualEndDate
  ,project.ClientId AS ClientId 
  ,client.ClientShortName AS ClientName                                
  ,projectmanager.ProgramManagerID AS ManagerId    
  ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName       
  ,practicearea.PracticeAreaId AS PracticeAreaId  
  ,practicearea.PracticeAreaCode AS PracticeAreaCode  
  ,projecttype.ProjectTypeId AS ProjectTypeId   
  ,projecttype.Description AS ProjectTypeDescription                                        
  ,dept.DepartmentId AS DepartmentId  
  ,dept.DepartmentCode AS DepartmentCode 
  ,domain.DomainId AS DomainId
  ,domain.DomainName AS DomainName
  ,state.StatusId AS ProjectStateId
  ,state.StatusCode AS ProjectState
  --,status.StatusId AS WorkFlowStatusId
  --,status.StatusCode AS WorkFlowStatus 
  ,(SELECT TOP 1 w.WorkFlowStatus FROM ProjectWorkFlow w WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc) AS WorkFlowStatusId
  ,(SELECT TOP 1 s.StatusCode FROM ProjectWorkFlow w INNER JOIN status s ON s.StatusId = w.WorkFlowStatus WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc ) AS WorkFlowStatus     
 FROM Projects project    
 INNER JOIN ProjectManagers projectmanager ON project.ProjectId = projectmanager.ProjectId 
 INNER JOIN  Status state ON project.ProjectStateId = state.StatusId
 INNER JOIN Domain domain ON project.DomainId = domain.DomainId 
 --INNER JOIN ProjectWorkFlow workflow ON project.ProjectId = workflow.ProjectId
 --INNER JOIN Status status ON workflow.WorkFlowStatus = status.StatusId   
 INNER JOIN Clients client ON project.ClientId = client.ClientId  
 INNER JOIN ProjectType projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId  
 INNER JOIN PracticeArea practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId  
 INNER JOIN Departments dept ON project.DepartmentId = dept.DepartmentId  
 INNER JOIN Employee programmanager ON projectmanager.ProgramManagerID = programmanager.EmployeeId    
 WHERE project.ProjectId=@ProjectId AND projectmanager.IsActive=1    
END     