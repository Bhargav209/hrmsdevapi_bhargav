﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	22-05-2018
-- Modified date	:	22-05-2018
-- Modified By		:	Sushmitha
-- Description		:	Adds KRA scale master.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateScaleMaster]
(  
@MinimumScale INT, 
@MaximumScale INT,  
@KRAScaleTitle VARCHAR(50)
)  
AS
BEGIN

 SET NOCOUNT ON;  

 DECLARE @KRAScaleMasterId INT
 DECLARE @KRAScaleId INT

 SELECT @KRAScaleId = COUNT(KRAScaleMasterID) FROM KRAScaleMaster WHERE KRAScaleTitle = @KRAScaleTitle
 IF (@KRAScaleId > 0)
   SELECT -1  --Duplicate kra scale title.
 ELSE
 BEGIN
 INSERT INTO 
	[dbo].[KRAScaleMaster]  
	(MinimumScale, MaximumScale, KRAScaleTitle)  
 VALUES  
	(@MinimumScale, @MaximumScale, @KRAScaleTitle)  
  
 SET @KRAScaleMasterId = SCOPE_IDENTITY()

 SELECT @KRAScaleMasterId
 END
END
