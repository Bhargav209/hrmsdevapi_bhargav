﻿-- =======================================     
-- Author			:	Santosh          
-- Create date		:	31-10-2017          
-- Modified date	:	31-10-2017          
-- Modified By		:	Santosh          
-- Description		:	Updates a Category         
-- =======================================
CREATE PROCEDURE [dbo].[usp_UpdateCategory]         
@CategoryID INT,  
@CategoryName VARCHAR(50),         
@IsActive BIT,      
@ModifiedUser VARCHAR(100),      
@SystemInfo VARCHAR(50),      
@ModifiedDate DATETIME     
AS          
BEGIN      
      
 SET NOCOUNT ON;     
       
 UPDATE [dbo].[CategoryMaster]          
 SET  
	CategoryName	=	@CategoryName  
   ,IsActive		=	@IsActive   
   ,ModifiedUser	=	@ModifiedUser   
   ,ModifiedDate	=	@ModifiedDate  
   ,SystemInfo		=	@SystemInfo  
 WHERE CategoryID	=	@CategoryID  
            
 SELECT @@ROWCOUNT          
END