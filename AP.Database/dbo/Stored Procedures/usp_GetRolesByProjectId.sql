﻿-- =================================================   
-- Author			:	Sushmitha            
-- Create date		:	28-02-2018           
-- Modified date	:	28-02-2018             
-- Modified By		:	Sushmitha            
-- Description		:	Gets roles by projectid    
-- =================================================  

CREATE PROCEDURE [dbo].[usp_GetRolesByProjectId]
@ProjectId INT
AS
BEGIN
 SET NOCOUNT ON;

 SELECT
	[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
	,rolemaster.RoleMasterID AS RoleId
	,projectRoles.ProjectId AS ProjectId
 FROM
	[dbo].[ProjectRoles] projectRoles
	INNER JOIN [dbo].[RoleMaster] rolemaster
	ON projectRoles.RoleMasterId = rolemaster.RoleMasterID
 WHERE
	projectRoles.ProjectId = @ProjectId AND projectRoles.IsActive = 1 AND rolemaster.IsActive = 1
 END
