﻿-- =======================================================================================
-- Author				:	Sushmitha
-- Create date			:	10-05-2018
-- Modified date		:	10-05-2018
-- Modified By			:	Sushmitha
-- Description			:	Gets list of KRA Operators.
-- =======================================================================================   

CREATE PROCEDURE [dbo].[usp_GetKRAOperators]
AS
BEGIN
	SET NOCOUNT ON;      
                   
	SELECT    
		KRAOperatorID AS Id
	   ,Operator AS Name
	FROM [dbo].[KRAOperator]  

END  