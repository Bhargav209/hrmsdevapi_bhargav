﻿
-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	20-07-2018
-- Modified date	:	20-07-2018
-- Modified By		:	Sushmitha
-- Description		:	Adds Aspects Master.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateAspectMaster]
(  
@AspectName VARCHAR(70), 
@CreatedDate DATETIME,  
@CreatedUser VARCHAR(150),  
@SystemInfo VARCHAR(50)  
)  
AS
BEGIN

 SET NOCOUNT ON;  

 UPDATE 
	[dbo].[AspectMaster]
 SET
	ModifiedDate = GETDATE()
 WHERE 
	AspectName = @AspectName

 IF (@@ROWCOUNT = 0)
 BEGIN
 
 INSERT INTO 
	[dbo].[AspectMaster]  
	(AspectName, CreatedDate, CreatedUser, SystemInfo)  
 VALUES  
	(@AspectName, @CreatedDate, @CreatedUser, @SystemInfo)  
  
 SELECT @@ROWCOUNT
    
END
ELSE

SELECT -1 --duplicate

END
