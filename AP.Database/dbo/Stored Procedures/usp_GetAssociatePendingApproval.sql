﻿-- ==========================================================
-- Author			:	Santosh            
-- Create date		:	20-11-2017            
-- Modified date	:	20-11-2017            
-- Modified By		:	Santosh            
-- Description		:	Gets associates pending for approaval
-- ==========================================================
CREATE PROCEDURE [dbo].[usp_GetAssociatePendingApproval]
@EmployeeID INT
AS            
BEGIN        
    
 SET NOCOUNT ON;
      
	SELECT * FROM [dbo].[vw_AssociatePendingApproval] 
	WHERE empID = @EmployeeID
END