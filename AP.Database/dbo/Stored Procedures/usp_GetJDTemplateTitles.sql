﻿-- ==========================================     
-- Author			:	Basha            
-- Create date		:	10-08-2018            
-- Modified date	:	10-2018-2018            
-- Modified By		:	Basha            
-- Description		:	Gets all JD TemplateTitles    
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetJDTemplateTitles]
AS              
BEGIN          
          
 SET NOCOUNT ON;          
           
 SELECT  
		 TemplateTitleId as Id
		,TemplateTitle   as Name   
 FROM [dbo].[JDTemplateTitleMaster]                   
END
