﻿-- ========================================================  
-- Author			:	Sushmitha  
-- Create date		:	12-04-2018  
-- Modified date	:	12-04-2018  
-- Modified By		:	Santosh
-- Description		:	Gets KRA definition By KRA GroupId.  
-- ========================================================   
CREATE PROCEDURE [dbo].[usp_GetKRADefinitionByKRAGroupId]
(  
 @KRAGroupId INT
,@FinancialYearId INT
,@KRAAspectId INT
)  
AS                    
BEGIN                
                
 SET NOCOUNT ON;      
                   
 SELECT  
  kraDefinition.KRADefinitionId   
 ,kraDefinition.KRAAspectId  
 ,kraDefinition.Metric  
 ,kraOperator.KRAOperatorID AS KRAOperatorId
 ,kraOperator.Operator
 ,kraMeasurement.KRAMeasurementTypeID AS KRAMeasurementTypeId
 ,kraMeasurement.KRAMeasurementType
 ,kraScaleMaster.KRAScaleMasterID AS KRAScaleMasterId
 ,CAST(kraScaleMaster.MinimumScale AS VARCHAR(1)) + ' - ' + CAST(kraScaleMaster.MaximumScale AS VARCHAR(2)) as ScaleLevel
 ,kraDefinition.TargetValue AS KRATargetValue
 ,CAST(kraDefinition.TargetValueAsDate AS Varchar(50)) AS KRATargetValueAsDate
 ,kraDefinition.KRATargetText
 ,kraTargetPeriod.KRATargetPeriodID AS KRATargetPeriodId
 ,kraTargetPeriod.KRATargetPeriod
 FROM   
 [dbo].[KRADefinition] kraDefinition     
 INNER JOIN [dbo].[KRAGroup] kraGroup      
 ON kraDefinition.KRAGroupId = kraGroup.KRAGroupId
 INNER JOIN [dbo].[KRAOperator] kraOperator
 ON kraDefinition.KRAOperatorID = kraOperator.KRAOperatorID  
 INNER JOIN [dbo].[KRAMeasurementType] kraMeasurement
 ON kraDefinition.KRAMeasurementTypeID = kraMeasurement.KRAMeasurementTypeID 
 LEFT JOIN [dbo].[KRAScaleMaster] kraScaleMaster
 ON kraDefinition.KRAScaleMasterID = kraScaleMaster.KRAScaleMasterID 
 INNER JOIN [dbo].[KRATargetPeriod] kraTargetPeriod
 ON kraDefinition.KRATargetPeriodID = kraTargetPeriod.KRATargetPeriodID
 WHERE kraDefinition.KRAGroupId = @KRAGroupId AND kraDefinition.FinancialYearId = @FinancialYearId
 AND kraDefinition.KRAAspectId = @KRAAspectId         
 END  