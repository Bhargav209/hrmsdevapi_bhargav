﻿-- =======================================================
-- Author			:	Santosh
-- Create date		:	06-11-2017
-- Modified date	:	06-11-2017
-- Modified By		:	Santosh
-- Description		:	Gets Associate Resignation Details  
-- =======================================================     
CREATE PROCEDURE [dbo].[usp_GetAssociateResignationDetails]
@EmployeeID INT
AS
BEGIN
 SET NOCOUNT ON;
      
 SELECT
  ar.ResignationID
 ,ar.EmployeeID
 ,ar.ReasonID
 ,ar.ReasonDescription
 ,ar.DateOfResignation
 ,ar.LastWorkingDate
 ,ar.StatusID
 ,e.FirstName
 ,e.LastName 
 ,e.JoinDate
 ,dept.DepartmentCode
 ,desg.DesignationName
 FROM [dbo].[AssociateResignation] ar
 INNER JOIN [dbo].[Employee] e
 ON ar.EmployeeId = e.EmployeeId
 INNER JOIN [dbo].[Departments] dept
 ON e.DepartmentId = dept.DepartmentId
 INNER JOIN [dbo].[Designations] desg 
 ON e.Designation = desg.DesignationID
 WHERE e.IsActive = 1 AND ar.EmployeeID = @EmployeeID
END