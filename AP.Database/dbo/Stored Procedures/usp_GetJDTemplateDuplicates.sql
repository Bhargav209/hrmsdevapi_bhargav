﻿-- ==========================================     
-- Author			:	Basha            
-- Create date		:	10-08-2018            
-- Modified date	:	10-2018-2018            
-- Modified By		:	Basha            
-- Description		:	Gets JD TemplateDuplicates
-- ==========================================  
CREATE PROCEDURE [dbo].[usp_GetJDTemplateDuplicates]
AS              
BEGIN          
          
 SET NOCOUNT ON;        
SELECT  distinct  jdTempDetail.TemplateTitleId, t.* 
from [JDTemplateDetail] jdTempDetail
join (
    select  count(*) as cnt
    from [JDTemplateDetail]
    group by TemplateTitleId, CompetencyAreaId,SkillGroupId,SkillId,ProficiencyLevelId
    having count(*) > 1
) t on jdTempDetail.TemplateTitleId = t.cnt    
SELECT @@ROWCOUNT
END
