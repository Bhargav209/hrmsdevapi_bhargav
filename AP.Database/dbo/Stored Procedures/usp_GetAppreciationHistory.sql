﻿-- ================================================            
-- Author   : Santosh            
-- Create date  : 30-01-2018            
-- Modified date : 30-01-2018                
-- Modified By  : Santosh            
-- Description  : Get Appreciation history      
-- ================================================        
CREATE PROCEDURE [dbo].[usp_GetAppreciationHistory]
(
 @EmployeeID INT
)       
AS       
BEGIN
 SET NOCOUNT ON;
  SELECT
    appreciation.ID
   ,[dbo].[udfGetEmployeeFullName](appreciation.ToEmployeeID) AS ToEmployeeName
   ,appType.[Type] AS AppreciationType
   ,AppreciationMessage
   ,AppreciationDate 
   ,adrCycle.ADRCycle
   ,CONCAT(financialYear.FromYear, ' - ', financialYear.ToYear) AS FinancialYear
  FROM [dbo].[AssociateAppreciation] appreciation      
  INNER JOIN [dbo].[AppreciationType] appType        
  ON appreciation.AppreciationTypeID = appType.ID  
  INNER JOIN [dbo].[ADRCycle] adrCycle
  ON appreciation.ADRCycleID = adrCycle.ADRCycleID 
  INNER JOIN [dbo].[FinancialYear] financialYear
  ON appreciation.FinancialYearID = financialYear.ID
  WHERE FromEmployeeID = @EmployeeID AND appreciation.IsActive = 1  
END 