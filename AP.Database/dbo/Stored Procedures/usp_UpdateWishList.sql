﻿CREATE PROCEDURE [dbo].[usp_UpdateWishList]  
@ManagerId INT
,@WishListName VARCHAR(150)
,@EmployeeIds VARCHAR(1000)
,@CreatedUser VARCHAR(150)         
,@SystemInfo VARCHAR(50)
AS  
BEGIN  
 SET NOCOUNT ON;    
	--check employees already associated to the wishlist or not.
	if not exists(select id from wishlist where ManagerId=@ManagerId and WishListName=@WishListName and EmployeeID in(SELECT VALUE FROM [UDF_SplitString] (@EmployeeIds, ',')))
	Begin
		DECLARE @Count INT
		DECLARE @RowCount INT
		DECLARE @employee_id NUMERIC(38,0)
		SET @count=1
		--create table and insert emp ids
		DECLARE @ListofEmpIDs TABLE(ID int IDENTITY(1,1),EmployeeId VARCHAR(100));
		INSERT INTO @ListofEmpIDs SELECT VALUE FROM [UDF_SplitString] (@EmployeeIds, ',');

		SELECT @RowCount=COUNT(1) FROM @ListofEmpIDs
		WHILE(@count<=@RowCount)
			BEGIN   
			--
				SELECT @employee_id=EmployeeId FROM @ListofEmpIDs WHERE ID=@Count;
			
				Insert into WishList
				(
					WishListName
					,EmployeeID
					,ManagerId
					,CreatedUser
					,CreatedDate
					,SystemInfo
				)
				values
				(
					@WishListName
					,@employee_id
					,@ManagerId
					,@CreatedUser
					,GetDate()
					,@SystemInfo
				)
				SELECT @@ROWCOUNT
				SET @count=@count+1;
			END
	End
	else
		select -1;
END

