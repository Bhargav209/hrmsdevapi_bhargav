﻿-- =============================================
-- Author:		Bhavani
-- Create date: 16-11-2018 8:15:10
-- Description:	Fetches skills based on the given search string
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetSkillsBySearchString]
(
@SearchString VARCHAR(20)
)	
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT 
		SkillId AS Id, SkillName AS Name
	FROM 
		Skills
	WHERE 
		 IsActive=1 AND SkillName LIKE '%' + @SearchString + '%'
    
END
