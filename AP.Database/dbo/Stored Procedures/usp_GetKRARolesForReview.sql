﻿-- ==============================================  
-- Author			:	Santosh  
-- Create date		:	16-02-2018  
-- Modified date	:	07-03-2018  
-- Modified By		:	Santosh  
-- Description		:	Get KRA Roles For Review  
-- ==============================================       
CREATE PROCEDURE [dbo].[usp_GetKRARolesForReview]
(        
 @FinancialYearID INT,        
 @DepartmentID INT        
)        
AS        
BEGIN        
         
 SET NOCOUNT ON;      
      
 SELECT DISTINCT      
     roleMaster.RoleMasterID        
     ,LTRIM(RTRIM(CONCAT(prefix.PrefixName, ' ', sgRoles.SGRoleName, ' ', suffix.SuffixName))) AS RoleName    
     ,roleTemplate.StatusID
	 ,[status].StatusDescription AS StatusCode    
  FROM [dbo].[KRARoleTemplate] roleTemplate        
  INNER JOIN [dbo].[RoleMaster] roleMaster        
  ON roleTemplate.RoleMasterID = roleMaster.RoleMasterID        
  INNER JOIN [dbo].[SGRole] sgRoles
  ON roleMaster.SGRoleID = sgRoles.SGRoleID              
  INNER JOIN [dbo].[Status] [status]
  ON roleTemplate.StatusID = [status].StatusID           
  LEFT JOIN [dbo].[SGRolePrefix] prefix              
  ON roleMaster.PrefixID = prefix.PrefixID      
  LEFT JOIN [dbo].[SGRoleSuffix] suffix              
  ON roleMaster.SuffixID = suffix.SuffixID      
  WHERE roleMaster.DepartmentID = @DepartmentID  
  AND roleTemplate.FinancialYearID = @FinancialYearID    
END