﻿--EXEC usp_ProjectRejected 405,'bhavani.chintamadaka@senecaglobal.com','2019-02-07 14:25:07.910','192.168.2.236'

CREATE PROCEDURE [dbo].[usp_ProjectRejected]      
 (      
  @ProjectId INT,
  @EmployeeId INT,  
  @CreatedUser VARCHAR(100),  
  @CreatedDate DATETIME,      
  @SystemInfo VARCHAR(50)  
 )                   
AS                      
BEGIN                  
                 
 SET NOCOUNT ON;       
   
  BEGIN      
   DECLARE @WorkFlowStatus INT    
   DECLARE @SubmittedTo INT   
   DECLARE @ProjectState INT  
   DECLARE @CategoryId INT    
   DECLARE @EmailTo VARCHAR(100)    
   DECLARE @ToUserId INT  
      
   SELECT @CategoryId = CategoryID FROM CategoryMaster where CategoryName = 'PPC'    
   SELECT @EmailTo=EmailTo FROM NotificationConfiguration WHERE CategoryId=@CategoryId     
   SELECT @ToUserId=UserId FROM Users WHERE EmailAddress = @EmailTo     
   SELECT @SubmittedTo=EmployeeId FROM Employee WHERE UserId=@ToUserId             
   SELECT @WorkFlowStatus = StatusId FROM STATUS WHERE CategoryID = @CategoryId AND StatusCode = 'RejectedByDH'   
   SELECT @ProjectState = StatusId FROM STATUS WHERE CategoryID = @CategoryId AND StatusCode = 'Drafted'  
  
    INSERT INTO ProjectWorkFlow(      
     SubmittedBy      
    ,SubmittedTo     
    ,SubmittedDate    
    ,WorkFlowStatus   
    ,ProjectId      
    ,Comments )      
    VALUES(      
     @EmployeeId      
    ,@SubmittedTo      
    ,@CreatedDate     
    ,@WorkFlowStatus      
    ,@ProjectId   
 ,NULL)     
       
  --update project state    
   UPDATE Projects SET      
     ProjectStateId = @ProjectState     
    ,ModifiedUser = @CreatedUser    
    ,ModifiedDate = @CreatedDate      
    ,SystemInfo = @SystemInfo      
        
    WHERE ProjectId = @ProjectId  
      
 SELECT @ProjectId;       
 END      
 END   