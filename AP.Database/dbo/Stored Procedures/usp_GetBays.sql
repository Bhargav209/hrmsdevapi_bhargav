﻿-- ========================================================
-- Author			:	Basha
-- Create date		:	06-09-2018
-- Modified date	:	06-09-2018
-- Modified By		:	Basha
-- Description		:	Gets BayStation Information.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetBays]
AS
BEGIN

 SET NOCOUNT ON; 
 SELECT   [BayId],[Name],[Description] from BayInformation
END
