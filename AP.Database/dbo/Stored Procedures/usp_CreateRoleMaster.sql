﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-02-2018
-- Modified date	:	12-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Adds roles.
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_CreateRoleMaster]   
(
  @SGRoleID   INT
 ,@PrefixID   INT NULL
 ,@SuffixID   INT NULL
 ,@DepartmentID  INT
 ,@RoleDescription NVARCHAR(500) NULL
 ,@KeyResponsibilities NVARCHAR(MAX)    
 ,@EducationQualification NVARCHAR(MAX)
 ,@CreatedDate   DATETIME
 ,@CreatedUser   VARCHAR(150)
 ,@SystemInfo   VARCHAR(50)  
 ,@KRAGroupID  INT NULL
)
AS
BEGIN
 SET NOCOUNT ON;

  UPDATE 
	[dbo].[RoleMaster]
 SET
	ModifiedDate = GETDATE()
 WHERE 
	SGRoleID = @SGRoleID AND ISNULL(PrefixID,0) = @PrefixID AND ISNULL(SuffixID,0) = @SuffixID

   IF (@@ROWCOUNT = 0)
   BEGIN

	 INSERT INTO [dbo].[RoleMaster]
	  (
		 SGRoleID  
		,PrefixID  
		,SuffixID  
		,DepartmentID  
		,RoleDescription  
		,KeyResponsibilities  
		,EducationQualification 
		,CreatedDate  
		,CreatedUser  
		,SystemInfo
		,KRAGroupID  
	  )  
	 VALUES  
	 (  
		 @SGRoleID  
		,CASE @PrefixID WHEN 0 THEN NULL ELSE @PrefixID END  
		,CASE @SuffixID WHEN 0 THEN NULL ELSE @SuffixID END 
		,@DepartmentID  
		,@RoleDescription  
		,@KeyResponsibilities  
		,@EducationQualification
		,@CreatedDate  
		,@CreatedUser  
		,@SystemInfo 
		,@KRAGroupID 
	  )
	SELECT CAST(SCOPE_IDENTITY() AS INT)
END
ELSE

SELECT 0

END
