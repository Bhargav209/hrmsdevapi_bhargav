﻿-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	06-12-2017            
-- Modified date	:	11-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Get experience by TalentRequistionId.     
-- ==========================================
CREATE PROCEDURE [dbo].[usp_GetExperienceByTalentRequistionId]
@TalentRequisitionId INT,
@RoleMasterId int      
AS            
BEGIN        
        
 SET NOCOUNT ON;   

  SELECT 
	 MIN(MinimumExperience) AS MinimumExperience , MAX(MaximumExperience) AS MaximumExperience
 FROM
	[dbo].[RequisitionRoleDetails]
 WHERE
	TRId = @TalentRequisitionId and RoleMasterId=@RoleMasterId

END