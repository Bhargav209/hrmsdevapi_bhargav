﻿-- =======================================================================================
-- Author				:	Sushmitha
-- Create date			:	10-05-2018
-- Modified date		:	10-05-2018
-- Modified By			:	Sushmitha
-- Description			:	Gets list of KRA scales.
-- =======================================================================================   

CREATE PROCEDURE [dbo].[usp_GetKRAScales]
AS
BEGIN
       SET NOCOUNT ON;      
                   
       SELECT 
           KRAScaleMasterID
          ,MinimumScale
          ,MaximumScale
		  ,KRAScaleTitle + '(' + CAST(MinimumScale AS VARCHAR(1)) + ' - ' + CAST(MaximumScale AS VARCHAR(2)) + ')' AS ScaleLevel
       FROM [dbo].[KRAScaleMaster]
END
