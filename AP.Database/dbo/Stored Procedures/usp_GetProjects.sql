﻿--=======================================================
-- Author			:	Santosh
-- Create date		:	30-01-2018
-- Modified date	:	30-01-2018  
-- Modified By		:	Santosh
-- Description		:	Get projects 
-- =====================================
CREATE PROCEDURE [dbo].[usp_GetProjects]
AS       
BEGIN  
	SET NOCOUNT ON;    
		SELECT
			 ProjectID
			,ProjectCode
			,ProjectName
		FROM [dbo].[Projects] 
		WHERE IsActive = 1
END