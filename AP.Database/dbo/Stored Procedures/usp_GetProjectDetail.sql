﻿CREATE PROCEDURE [dbo].[usp_GetProjectDetail]  
 (  
 @RoleName VARCHAR(100),  
 @EmployeeId INT  
 )               
AS                  
BEGIN              
             
 SET NOCOUNT ON;  
   
 DECLARE @RoleId INT  
  
 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)  
  IF(@RoleId = 5) -- 5 is program manager.ensure that roles table have always 5 as program manager  
  SELECT   
   project.ProjectId AS ProjectId  
  ,project.ProjectCode AS ProjectCode  
  ,project.ProjectName AS ProjectName  
  ,project.ActualStartDate AS ActualStartDate  
  ,project.ActualEndDate AS ActualEndDate  
  ,client.ClientShortName AS ClientName  
  ,practicearea.PracticeAreaId AS PracticeAreaId  
  ,practicearea.PracticeAreaCode AS PracticeAreaCode  
  ,projecttype.ProjectTypeId AS ProjectTypeId   
  ,projecttype.Description AS ProjectTypeDescription                                        
  ,dept.DepartmentId AS DepartmentId  
  ,dept.DepartmentCode AS DepartmentCode 
  ,domain.DomainId AS DomainId
  ,domain.DomainName AS DomainName
  ,state.StatusId AS ProjectStateId
  ,state.StatusCode AS ProjectState
  ,(SELECT TOP 1 w.WorkFlowStatus FROM ProjectWorkFlow w WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc) AS WorkFlowStatusId
  ,(SELECT TOP 1 s.StatusCode FROM ProjectWorkFlow w INNER JOIN status s ON s.StatusId = w.WorkFlowStatus WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc ) AS WorkFlowStatus           
  ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName   
 FROM Projects project  
 INNER JOIN ProjectManagers projmanager ON project.ProjectId = projmanager.ProjectId 
 INNER JOIN  Status state ON project.ProjectStateId = state.StatusId 
 INNER JOIN Domain domain ON project.DomainId = domain.DomainId 
 INNER JOIN Clients client ON project.ClientId = client.ClientId  
 INNER JOIN ProjectType projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId  
 INNER JOIN PracticeArea practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId  
 INNER JOIN Departments dept ON project.DepartmentId = dept.DepartmentId  
 INNER JOIN Employee programmanager ON projmanager.ProgramManagerID = programmanager.EmployeeId   
  
 WHERE (projmanager.IsActive = 1 AND projmanager.ProgramManagerID=@EmployeeId)  
  
 ELSE IF(@RoleId = 3)-- 3 is department head.ensure that roles table have always 3 as department head  
 SELECT   
   project.ProjectId AS ProjectId  
  ,project.ProjectCode AS ProjectCode  
  ,project.ProjectName AS ProjectName  
  ,project.ActualStartDate AS ActualStartDate  
  ,project.ActualEndDate AS ActualEndDate    
  ,client.ClientShortName AS ClientName  
  ,practicearea.PracticeAreaId AS PracticeAreaId  
  ,practicearea.PracticeAreaCode AS PracticeAreaCode  
  ,projecttype.ProjectTypeId AS ProjectTypeId   
  ,projecttype.Description AS ProjectTypeDescription                                        
  ,dept.DepartmentId AS DepartmentId  
  ,dept.DepartmentCode AS DepartmentCode 
  ,domain.DomainId AS DomainId
  ,domain.DomainName AS DomainName
  ,status.StatusId AS ProjectStateId
  ,status.StatusCode AS ProjectState   
  ,(SELECT TOP 1 w.WorkFlowStatus FROM ProjectWorkFlow w WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc) AS WorkFlowStatusId
  ,(SELECT TOP 1 s.StatusCode FROM ProjectWorkFlow w INNER JOIN status s ON s.StatusId = w.WorkFlowStatus WHERE w.ProjectId =project.ProjectId order by w.SubmittedDate desc,w.WorkFlowId desc ) AS WorkFlowStatus              
  ,programmanager.FirstName + ' ' + programmanager.LastName AS ManagerName   
 FROM Projects project  
 LEFT OUTER JOIN ProjectManagers projmanager ON project.ProjectId = projmanager.ProjectId AND projmanager.IsActive = 1 
 LEFT OUTER JOIN Domain domain ON project.DomainId = domain.DomainId     
 LEFT OUTER  JOIN  Status status ON project.ProjectStateId = status.StatusId    
 INNER JOIN Clients client ON project.ClientId = client.ClientId  
 INNER  JOIN ProjectType projecttype ON project.ProjectTypeId = projecttype.ProjectTypeId  
 INNER JOIN PracticeArea practicearea ON project.PracticeAreaId = practicearea.PracticeAreaId  
 INNER JOIN Departments dept ON project.DepartmentId = dept.DepartmentId  
 LEFT OUTER JOIN Employee programmanager ON projmanager.ProgramManagerID = programmanager.EmployeeId  
  
END    