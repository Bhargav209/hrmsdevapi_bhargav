﻿-- ==============================================    
-- Author   : Santosh    
-- Create date  : 04-12-2017    
-- Modified date : 11-01-2018
-- Modified By  : Santosh    
-- Description  : Multipurpose skill search    
-- ==============================================     
CREATE PROCEDURE [dbo].[usp_SkillSearch]    
AS    
 SET NOCOUNT ON;    
 TRUNCATE TABLE [dbo].[SkillSearch]    
         
 INSERT INTO [dbo].[SkillSearch]    
 SELECT DISTINCT    
   emp.EmployeeID    
  ,emp.FirstName     
  ,emp.LastName     
  ,emp.ExperienceExcludingCareerBreak    
  ,ISNULL(allocation.RoleMasterId, 0)  
  ,[dbo].[udf_GetRoleName]([role].RoleMasterID) AS RoleName   
  ,desg.DesignationID    
  ,desg.DesignationCode    
  ,proj.ProjectCode    
  ,proj.ProjectName    
  ,allocation.IsPrimary      
  ,allocation.IsCritical    
  ,allocation.IsBillable    
  ,skill.CompetencyAreaId    
  ,comp.CompetencyAreaCode    
  ,empSkills.SkillGroupId    
  ,skillGrp.SkillGroupName    
  ,empSkills.SkillID    
  ,skill.SkillName    
  ,empSkills.ProficiencyLevelID    
  ,pl.ProficiencyLevelCode    
  ,emp.EmployeeCode    
 FROM Employee emp    
 INNER JOIN AssociateAllocation allocation    
 ON emp.EmployeeId = allocation.EmployeeID AND allocation.IsActive = 1    
 INNER JOIN Designations desg    
 ON emp.Designation = desg.DesignationID    
 LEFT JOIN RoleMaster [role]    
 ON allocation.RoleMasterId = [role].RoleMasterID    
 INNER JOIN Projects proj    
 ON allocation.ProjectId = proj.ProjectID    
 LEFT JOIN EmployeeSkills empSkills    
 ON emp.EmployeeId = empSkills.EmployeeID    
 LEFT JOIN Skills skill    
 ON empSkills.SkillId = skill.SkillID    
 LEFT JOIN SkillGroup skillGrp    
 ON empSkills.SkillGroupId = skillGrp.SkillGroupID     
 LEFT JOIN CompetencyArea comp    
 ON empSkills.CompetencyAreaId = comp.CompetencyAreaID    
 LEFT JOIN ProficiencyLevel pl    
 ON empSkills.ProficiencyLevelId = pl.ProficiencyLevelID    
 WHERE emp.IsActive = 1
