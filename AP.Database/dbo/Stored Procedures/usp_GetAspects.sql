﻿-- ======================================    
-- Author			:	Sushmitha
-- Create date		:	20-07-2018
-- Modified date	:	20-07-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets Aspects Master. 
-- ======================================    
CREATE PROCEDURE [dbo].[usp_GetAspects]  
AS         
BEGIN
	SET NOCOUNT ON;   
	 
		SELECT
			 AspectId
			,AspectName as KRAAspectName
		FROM [dbo].[AspectMaster]

END