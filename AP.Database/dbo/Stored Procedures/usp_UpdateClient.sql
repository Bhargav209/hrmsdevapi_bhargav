﻿-- ========================================================    
-- Author   : Basha    
-- Create date  : 24-12-2018    
-- Modified date : 24-12-2018    
-- Modified By  : Basha    
-- Description  : Update Client    
-- ========================================================     
CREATE PROCEDURE [dbo].[usp_UpdateClient]
(    
  @ClientId INT    
 ,@ClientCode VARCHAR(50)    
 ,@ClientShortName VARCHAR(100)    
 ,@ClientLegalName VARCHAR(100)    
 ,@IsActive bit    
 ,@ModifiedUser VARCHAR(100)    
 ,@ModifiedDate   DATETIME    
 ,@SystemInfo VARCHAR(50)    
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
    
 DECLARE @IsCategoryShortNameExists INT   , @ClientShortNameHash nvarchar(450)
 SELECT @ClientShortName= RTRIM(LTRIM(@ClientShortName))    
    
 SELECT @ClientShortNameHash = CONVERT(nvarchar,UPPER(@ClientShortName));
 SET @ClientShortNameHash  = HashBytes('SHA1', @ClientShortNameHash);
     
  BEGIN TRY
   Update Clients     
   SET    
   clientShortName=@ClientShortName,    
   clientLegalName=@ClientLegalName,  
   ClientShortNameHash=@ClientShortNameHash,
   IsActive=@IsActive    
   WHERE    
   ClientId=@ClientId    
    
   SELECT @@ROWCOUNT      

   END TRY  
	BEGIN CATCH 
		SELECT -1
	END CATCH  
END