﻿-- ========================================================
-- Author			:	Basha
-- Create date		:	24-12-2018
-- Modified date	:	24-12-2018
-- Modified By		:	Basha
-- Description		:	Get Client Details
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_GetClientDetails]
AS
BEGIN
 SET NOCOUNT ON;
 SELECT ClientId, 
		ClientCode,
		ClientShortName,
		ClientLegalName, 
		IsActive
  FROM
  Clients
  where IsActive=1
END