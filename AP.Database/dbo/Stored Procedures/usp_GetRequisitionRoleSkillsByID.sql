﻿-- ==========================================     
-- Author			:	Sushmitha          
-- Create date		:	04-12-2017            
-- Modified date	:	04-12-2017            
-- Modified By		:	Sushmitha            
-- Description		:	Get requisition role skills by ID.     
-- ==========================================
CREATE PROCEDURE [dbo].[usp_GetRequisitionRoleSkillsByID]
@TRID INT,
@RequistionRoleDetailID INT        
AS            
BEGIN        
        
 SET NOCOUNT ON;
        
 SELECT
	 skillMapping.TRID as TalentRequisitionId
	,skillMapping.RequistiionRoleDetailID AS RequistionRoleDetailID
	 ,roleSkills.CompetencyAreaID
	,roleSkills.SkillGroupID
	,roleSkills.SkillID
	,roleSkills.ProficiencyLevelID
	,skills.SkillName
	,competencyArea.CompetencyAreaCode
	,skillGroup.SkillGroupName
	,proficiency.ProficiencyLevelCode     
 FROM
	[dbo].[RequisitionRoleSkills] roleSkills
	INNER JOIN [dbo].[RequisitionRoleSkillMapping] skillMapping  
	ON roleSkills.RoleSkillMappingID = skillMapping.ID
	INNER JOIN [dbo].[CompetencyArea] competencyArea
	ON roleSkills.CompetencyAreaID = competencyArea.CompetencyAreaId
	INNER JOIN [dbo].[SkillGroup] skillGroup
	ON roleSkills.SkillGroupID = skillGroup.SkillGroupId
	INNER JOIN [dbo].[Skills] skills
	ON roleSkills.SkillID = skills.SkillId
	INNER JOIN [dbo].[ProficiencyLevel] proficiency
	ON roleSkills.ProficiencyLevelID = proficiency.ProficiencyLevelId
 WHERE skillMapping.TRID = @TRID AND skillMapping.RequistiionRoleDetailID = @RequistionRoleDetailID
END