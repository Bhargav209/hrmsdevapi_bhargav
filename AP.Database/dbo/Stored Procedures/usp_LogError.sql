﻿-- ==================================  
-- Author		:	Santosh
-- Create date  :	13-12-2017
-- Modified date:	13-12-2017  
-- Modified By  :	Santosh 
-- Description  :	Error logging
-- ================================== 
CREATE PROCEDURE [dbo].[usp_LogError]
@FileName VARCHAR(100),
@ErrorMessage VARCHAR(MAX),
@CreatedDate DATETIME, 
@Type INT
AS
BEGIN
        
	SET NOCOUNT ON;
	IF @Type = 1
	BEGIN
		INSERT INTO [dbo].[ErrorLogUI]
		(
			 [FileName]
			,ErrorMessage
			,CreatedDate
		)            
		VALUES            
		(      
		 @FileName    
		,@ErrorMessage
		,@CreatedDate     
		)
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[ErrorLogService]            
		(
		 [FileName]
		,ErrorMessage
		,CreatedDate       
		)            
		VALUES          
		(
		 @FileName     
		,@ErrorMessage          
		,@CreatedDate     
		)        
	END     
	SELECT @@ROWCOUNT           
END