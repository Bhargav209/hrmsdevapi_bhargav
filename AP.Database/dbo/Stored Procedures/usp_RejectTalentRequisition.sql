﻿-- ========================================================
-- Author			    :	Sushmitha
-- Create date			:	05-02-2018
-- Modified date	    :	05-02-2018
-- Modified By			:	Sushmitha
-- Description			:	Reject requisition.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_RejectTalentRequisition]
@TalentrequisitionId INT,
@RejectedByID INT,
@ToEmployeeID INT,
@StatusId INT,
@Comments VARCHAR(1500),
@Modifieduser VARCHAR(100),
@Modifieddate DATETIME,
@Createduser VARCHAR(100),
@Createddate DATETIME,
@Systeminfo VARCHAR(50)
AS
BEGIN  
 SET NOCOUNT ON;

 UPDATE 
	[dbo].[TalentRequisition]
 SET 
	Statusid = @StatusId, 
	Modifieduser = @Modifieduser,
	Modifieddate = @Modifieddate,
	Systeminfo = @Systeminfo 
 WHERE 
	Trid = @TalentrequisitionId

 INSERT INTO [dbo].[TalentRequisitionWorkFlow]	            
 (      
	TalentRequisitionID
	,Comments 
	,StatusID
	,FromEmployeeID
	,ToEmployeeID    
	,CreatedBy       
	,CreatedDate 
 )            
 VALUES            
 (      
	@TalentrequisitionId 
	,@Comments     
	,@StatusId     
	,@RejectedByID     
	,@ToEmployeeID    
	,@Createduser     
	,@Createddate    
 )        
  SELECT @@ROWCOUNT

END

