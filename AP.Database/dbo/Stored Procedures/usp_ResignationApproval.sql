﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	16-11-2017
-- Modified date	:	16-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Resignation Approval
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_ResignationApproval]
@ResignationID INT,
@ApprovedBy INT,
@ApprovedDate DATE,
@Remarks VARCHAR(MAX),
@DueRemarks VARCHAR(MAX),
@NoDueRemarks VARCHAR(MAX),
@ResignationRecommendation VARCHAR(100),
@CreatedBy VARCHAR(100),
@CreatedDate DATETIME,
@LastWorkingDate DATETIME,
@StatusID INT
AS
BEGIN
SET NOCOUNT ON;

 DECLARE @ID INT
 DECLARE @EmployeeID INT
 DECLARE @FromEmployeeID INT 

 
 SELECT @EmployeeID = EmployeeID FROM AssociateResignation WHERE ResignationID = @ResignationID
 
 INSERT INTO [dbo].[ResignationApproval]
	(
		 ResignationID
		,ApprovedByID
		,ApprovedDate
		,Remarks
		,DueRemarks
		,NoDueRemarks
		,ResignationRecommendation
		,CreatedBy
		,CreatedDate
	)
	VALUES
	(
		 @ResignationID
		,@ApprovedBy
		,@ApprovedDate
		,@Remarks
		,@DueRemarks
		,@NoDueRemarks
		,@ResignationRecommendation
		,@CreatedBy
		,@CreatedDate
	)

	SET @ID = SCOPE_IDENTITY()

	UPDATE [dbo].[Employee]
	SET RelievingDate = @LastWorkingDate
	    ,StatusId     = @StatusID
	WHERE EmployeeId  = @EmployeeID

	
	UPDATE AssociateResignation
	SET LastWorkingDate = @LastWorkingDate
		,StatusID        = @StatusID 
	WHERE ResignationID = @ResignationID
		
	SELECT @FromEmployeeID = ProgramManager FROM [dbo].[Employee] WHERE EmployeeID = @EmployeeID
	
	INSERT INTO [dbo].[AssociateResignationWorkFlow]
	(
		 [ResignationID]
		,[FromEmployeeID]
		,[ToEmployeeID]
		,[StatusID]
		,[CreatedBy]
		,[CreatedDate]
	)
	VALUES
	(
		 @ID
		,@FromEmployeeID
		,@EmployeeID
		,@StatusID
		,@CreatedBy
		,@CreatedDate
	)

	SELECT @@ROWCOUNT
END
