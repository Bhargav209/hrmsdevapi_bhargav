﻿-- =======================================================    
-- Author		:	Santosh       
-- Create date  :	16-11-2017             
-- Modified date:	16-11-2017    
-- Modified By  :	Santosh      
-- Description  :	Updates a notification category        
-- =======================================================
CREATE PROCEDURE [dbo].[usp_EditNotificationConfiguration]              
@NotificationConfigID INT,             
@EmailFrom VARCHAR(MAX),          
@EmailTo VARCHAR(MAX),          
@EmailCC VARCHAR(MAX),          
@EmailSubject VARCHAR(150),          
@ModifiedBy VARCHAR(150),          
@ModifiedDate DATETIME,  
@SystemInfo VARCHAR(50)  
AS              
BEGIN          
          
SET NOCOUNT ON;          
           
 UPDATE [dbo].[NotificationConfiguration]  
 SET      
	 EmailFrom		=	@EmailFrom  
	,EmailTo		=	@EmailTo    
	,EmailCC		=	@EmailCC   
	,EmailSubject	=	@EmailSubject    
	,ModifiedBy		=	@ModifiedBy    
	,ModifiedDate	=	@ModifiedDate  
	,SystemInfo		=	@SystemInfo  
 WHERE NotificationConfigID = @NotificationConfigID  
                     
 SELECT @@ROWCOUNT              
END