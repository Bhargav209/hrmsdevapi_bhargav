﻿-- ============================================  
-- Author			:	Prasanna          
-- Create date		:	28-01-2019  
-- Modified date	:	28-01-2019    
-- Description		:	Delete wish list  
-- ============================================    
CREATE PROCEDURE [dbo].[usp_DeleteWishList]  
@WishListId INT

AS  
BEGIN  
 SET NOCOUNT ON;    
	Delete from wishList where Id=@WishListId;

	SELECT @@ROWCOUNT
END
