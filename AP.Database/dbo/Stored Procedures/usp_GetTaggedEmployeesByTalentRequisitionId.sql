﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	24-01-2018
-- Modified date	:	24-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Get tagged employees for requisition by trid.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetTaggedEmployeesByTalentRequisitionId]
@TalentRequisitionId INT
AS
BEGIN

	DECLARE @CategoryId INT

	SELECT @CategoryId=[dbo].[udf_GetCategoryId]('TalentRequisition')

 SELECT      
    employeeTag.ID
   ,dbo.udfGetEmployeeFullName(employeeTag.EmployeeId) AS EmployeeName
   ,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
   ,talentRequisition.StatusId
FROM 
	[dbo].[TalentRequisitionEmployeeTag] employeeTag
 INNER JOIN [dbo].[RoleMaster] roleMaster
 ON employeeTag.RoleMasterID = roleMaster.RoleMasterID
 INNER JOIN [dbo].[TalentRequisition] talentRequisition
 ON employeeTag.TalentRequisitionID = talentRequisition.TRId
 INNER JOIN [dbo].[Status] [status]
 ON [status].StatusId = talentRequisition.StatusId AND [status].CategoryID=@CategoryId
WHERE
	employeeTag.TalentRequisitionID = @TalentRequisitionId 
END
