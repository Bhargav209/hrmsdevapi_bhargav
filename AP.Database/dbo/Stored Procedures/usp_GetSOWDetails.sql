﻿-- =================================================    
-- Author   : Mithun    
-- Create date  : 11-01-2019    
-- Modified date : 11-01-2019    
-- Modified By  : Mithun    
-- Description  : Get SOW  Details for Edit    
-- =================================================    
CREATE PROCEDURE [dbo].[usp_GetSOWDetails]    
(  
  @Id INT,
  @ProjectId INT
 ,@RoleName VARCHAR(50)    
)    
AS                  
BEGIN         
    
 SET NOCOUNT ON;
 DECLARE @RoleId INT

 SELECT @RoleId=RoleId FROM Roles WHERE UPPER(RoleName) = UPPER(@RoleName)
 IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head             
 SELECT   
   sow.Id AS Id 
  ,sow.ProjectId AS ProjectId   
  ,sow.SOWId AS SOWId
  ,sow.SOWFileName AS SOWFileName
  ,sow.SOWSignedDate AS SOWSignedDate  
  
 FROM SOW sow     
 WHERE sow.Id=@Id AND ProjectId=@ProjectId 
 ELSE
 SELECT -1   
              
END
