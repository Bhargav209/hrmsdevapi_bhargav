﻿-- ========================================================
-- Author			:	Mithun
-- Create date		:	01-11-2018
-- Modified date	:	01-11-2018
-- Modified By		:	
-- Description		:	Gets list of Reporting managers, program Managers and Leads
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetAllLeadManagers]
(
@SearchString VARCHAR(20)
)
AS         

BEGIN
	SET NOCOUNT ON; 

		SELECT  EmployeeId AS Id, [dbo].[udfGetEmployeeFullName](EmployeeId) AS Name 
		FROM Employee emp 
		 JOIN (
        SELECT IsActive,ReportingManagerID FROM ProjectManagers 
        UNION 
        SELECT IsActive,ProgramManagerID FROM ProjectManagers 
        UNION 
        SELECT IsActive,LeadID AS[Lead] FROM ProjectManagers) projmanager ON emp.EmployeeId = projmanager.ReportingManagerID
        AND emp.IsActive=1 AND projmanager.IsActive=1 AND
		(FirstName LIKE '%' + @SearchString + '%' OR LastName LIKE '%' + @SearchString + '%')

END