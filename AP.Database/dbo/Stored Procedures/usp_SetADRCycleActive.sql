﻿-- ===========================================
-- Author			:	Santosh  
-- Create date		:	24-01-2018  
-- Modified date	:	24-01-2018      
-- Modified By		:	Santosh  
-- Description		:	Set ADR cycle Active  
-- ===========================================
CREATE PROCEDURE [dbo].[usp_SetADRCycleActive]
(
	@ADRCycleID INT,
	@IsActive BIT,
	@DateModified DATETIME,
	@ModifiedUser VARCHAR(150),
	@SystemInfo  VARCHAR(50)
)
AS   
BEGIN
SET NOCOUNT ON;
	
	--Set other cycles to inactive
	UPDATE [dbo].[ADRCycle]
	SET
		 IsActive		=	0
		,DateModified	=	@DateModified
		,ModifiedUser	=	@ModifiedUser
		,SystemInfo		=	@SystemInfo
	WHERE ADRCycleID <> @ADRCycleID
	
	--Set the current one to ative
	UPDATE [dbo].[ADRCycle]
	SET
		 IsActive		=	 @IsActive
		,DateModified	=	 @DateModified
		,ModifiedUser	=	 @ModifiedUser
		,SystemInfo		=	 @SystemInfo
	WHERE ADRCycleID = @ADRCycleID

	SELECT @@ROWCOUNT
END 
