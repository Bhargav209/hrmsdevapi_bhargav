﻿-- ============================================  
-- Author			:	Sushmitha          
-- Create date		:	23-04-2018  
-- Modified date	:	17-05-2018    
-- Modified By		:	Santosh
-- Description		:	Create KRA Workflow  
-- ============================================    
CREATE PROCEDURE [dbo].[usp_CreateKRAWorkFlow]  
 @FinancialYearID INT  
,@StatusID INT  
,@FromEmployeeID INT  
,@ToEmployeeID INT  
,@DepartmentID INT
,@KRAGroupID VARCHAR(150)  
,@CreatedBy VARCHAR(100) 
,@CreatedDate DATETIME
,@Comments VARCHAR(MAX)
AS  
BEGIN  
 SET NOCOUNT ON;  
    
	DECLARE @WorkFlowId INT
	DECLARE @Status INT

  INSERT INTO [dbo].[KRAworkflow]    
   (   
        FinancialYearID  
       ,StatusID  
       ,FromEmployeeID  
       ,ToEmployeeID  
       ,DepartmentID  
       ,CreatedBy    
       ,CreatedDate
	   ,Comments 
   )     
  VALUES       
   (  
        @FinancialYearID  
       ,@StatusID  
       ,@FromEmployeeID  
       ,@ToEmployeeID  
       ,@DepartmentID  
       ,@CreatedBy  
       ,@CreatedDate 
	   ,@Comments
   )  
  
  SET @WorkFlowId = SCOPE_IDENTITY()

  INSERT INTO [dbo].[KRASubmittedGroup]    
   (   
        WorkFlowId  
       ,KRAGroupId  
       ,CreatedBy    
       ,CreatedDate
   )     
	SELECT
			@WorkFlowId
			,VALUE AS KRAGroupId
			,@CreatedBy  
			,@CreatedDate
	FROM [dbo].[UDF_SplitString](@KRAGroupId,',')

  SET @Status = (SELECT @@ROWCOUNT)
  IF (@Status > 0 )
	SELECT 1
  ELSE
	SELECT @Status
END