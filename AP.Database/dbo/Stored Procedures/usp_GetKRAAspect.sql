﻿-- ======================================    
-- Author			:	Santosh       
-- Create date		:	08-01-2017          
-- Modified date	:	06-02-2017    
-- Modified By		:	Santosh    
-- Description		:	Get KRA Aspects    
-- ======================================    
CREATE PROCEDURE [dbo].[usp_GetKRAAspect]  
@DepartmentId INT  
AS         
BEGIN
	SET NOCOUNT ON;    
		SELECT
			 kraAspect.KRAAspectID
			,aspectMaster.AspectName AS KRAAspectName
			,aspectMaster.AspectId
			,DateCreated AS CreatedDate
			,kraAspect.CreatedUser AS CreatedBy
			,DateModified AS ModifiedDate
			,kraAspect.ModifiedUser AS ModifiedBy
		FROM [dbo].[KRAAspectMaster] kraAspect
		INNER JOIN [dbo].[AspectMaster] aspectMaster
		ON kraAspect.AspectId = aspectMaster.AspectId
		WHERE DepartmentId = @DepartmentId  
END