﻿-- ====================================
-- Author			: Santosh      
-- Create date		: 24-04-2018      
-- Modified date	: 24-04-2018      
-- Modified By		: Santosh      
-- Description		: Get KRA Title
-- ====================================      
CREATE PROCEDURE [dbo].[usp_GetKRATitle]
(
	@DepartmentID INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT
		 kraGroup.KRAGroupID AS ID
		,kraGroup.KRATitle AS Name
	FROM  [dbo].[KRAGroup] kraGroup
	WHERE kraGroup.DepartmentID = @DepartmentID		
END
