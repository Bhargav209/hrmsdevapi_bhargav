﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	26-12-2017
-- Modified date	:	28-12-2017
-- Modified By		:	Sushmitha
-- Description		:	To tag an employee.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_TalentRequisitionEmployeeTag]       
@TalentRequisitionID INT, 
@EmployeeId INT,
@RoleMasterID INT,
@CreatedUser VARCHAR(100),
@CreatedDate DATETIME,
@SystemInfo VARCHAR(50)     
AS            
BEGIN        
        
 SET NOCOUNT ON;
         
 INSERT INTO [dbo].[TalentRequisitionEmployeeTag]            
 (
    TalentRequisitionID
   ,EmployeeId
   ,RoleMasterID
   ,CreatedUser
   ,CreatedDate
   ,SystemInfo       
 )            
 VALUES            
 (
    @TalentRequisitionID
   ,@EmployeeID
   ,@RoleMasterID
   ,@CreatedUser
   ,@CreatedDate
   ,@SystemInfo      
 )            
              
 SELECT @@ROWCOUNT            
END