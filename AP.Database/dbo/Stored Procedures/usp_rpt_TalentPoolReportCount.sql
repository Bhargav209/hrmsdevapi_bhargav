﻿-- ==================================================
-- Author			:	Santosh
-- Create date		:	20-04-2018
-- Modified date	:	20-04-2018
-- Modified By		:	Santosh
-- Description		:	Get Talent Pool report count
 -- =================================================
CREATE PROCEDURE [dbo].[usp_rpt_TalentPoolReportCount]
AS          
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	COUNT(allocation.ProjectId) ResourceCount
	,allocation.ProjectID
	,project.ProjectName
	FROM AssociateAllocation allocation
	INNER JOIN Projects project  
	ON allocation.ProjectId = project.ProjectId
	WHERE project.ProjectTypeID = (SELECT ProjectTypeID FROM ProjectType WHERE ProjectTypeCode = 'Talent Pool')
	AND allocation.IsActive = 1
	GROUP BY allocation.ProjectId, project.ProjectName
END