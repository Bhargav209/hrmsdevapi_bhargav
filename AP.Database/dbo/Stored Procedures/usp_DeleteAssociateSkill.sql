﻿-- ================================================  
-- Author			:  Sushmitha     
-- Create date		:  15-11-2017      
-- Modified date	:  15-11-2017      
-- Modified By		:  Sushmitha      
-- Description		:  delete records from employee skill table.     
-- ================================================ 
CREATE PROCEDURE [dbo].[usp_DeleteAssociateSkill]      
@ID INT

AS      
BEGIN  
  
 SET NOCOUNT ON;
   
 DELETE FROM [dbo].[EmployeeSkills]
 WHERE
 ID = @ID 
  
 SELECT @@ROWCOUNT      
END
