﻿-- ============================================     
-- Author			:	RAMYA            
-- Create date		:	22-03-2018            
-- Modified date	:	22-03-2018            
-- Modified By		:	RAMYA            
-- Description		:	Gets all associates details by role
-- usp_GetAssociatesByDepartmentAndRole 2,0     
-- ============================================

CREATE PROCEDURE [dbo].[usp_GetAssociatesByDepartmentAndRole]
(
@DepartmentID INT,
@RoleMasterID INT
)
AS
BEGIN

SET NOCOUNT ON; 

	DECLARE  @DeliveryDepartmentID INT
	SELECT @DeliveryDepartmentID=DepartmentId from Departments where DepartmentCode='Delivery'

	IF(@DepartmentID=@DeliveryDepartmentID)
	BEGIN
		SELECT 
		[allocation].AssociateAllocationId,
		[allocation].EmployeeId,
		[dbo].[udfGetEmployeeFullName](EmployeeId) AS AssociateName,
		[allocation].ProjectId,
		[percentage].Percentage as AllocationPercentage,		
		[project].ProjectName as Project,
		[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
		FROM AssociateAllocation [allocation]
		INNER JOIN [Projects] AS [project] ON [allocation].[ProjectId] = ([project].[ProjectId])
		INNER JOIN [AllocationPercentage] as [percentage] on [allocation].AllocationPercentage=[percentage].AllocationPercentageID
		INNER JOIN [ProjectType] AS [projecttype] ON [project].[ProjectTypeId] = ([projecttype].[ProjectTypeId]) and [ProjectType].ProjectTypeId<>6
		INNER JOIN [RoleMaster] AS [rolemaster] ON [allocation].[RoleMasterId] = ([rolemaster].[RoleMasterID])
		WHERE allocation.RoleMasterId=@RoleMasterID and allocation.IsPrimary=1 and allocation.IsActive=1

	END

	ELSE
	BEGIN
		SELECT
		 NULL as AssociateAllocationId
		,EmployeeId AS EmployeeId
		,[dbo].[udfGetEmployeeFullName](EmployeeId) AS AssociateName
		,NULL as ProjectId
		,NULL as AllocationPercentage
		,NULL as Project
		,NULL as RoleName
		FROM 
			[dbo].[Employee]
		WHERE DepartmentId = @DepartmentID AND IsActive = 1 AND EmployeeId <>1
	END

END
