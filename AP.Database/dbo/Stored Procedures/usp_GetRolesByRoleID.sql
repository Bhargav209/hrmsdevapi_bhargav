﻿-- =========================================  
-- Author			:	Santosh       
-- Create date		:	16-02-2018  
-- Modified date	:	24-04-2018  
-- Modified By		:	Santosh  
-- Description		:	Get role by RoleID  
-- =========================================          
CREATE PROCEDURE [dbo].[usp_GetRolesByRoleID] 
(  
 @RoleID INT  
)  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
      
  SELECT        
   roleMaster.RoleMasterID        
   ,LTRIM(RTRIM(CONCAT(prefix.PrefixName, ' ', sgRoles.SGRoleName, ' ', suffix.SuffixName))) AS RoleName           
   ,dept.[Description] AS DepartmentCode
   ,roleMaster.RoleDescription    
   ,roleMaster.KeyResponsibilities          
   ,roleMaster.EducationQualification
   ,kraGroup.KRATitle          
   ,roleMaster.KRAGroupID      
   FROM [dbo].[RoleMaster] roleMaster        
   INNER JOIN [dbo].[SGRole] sgRoles        
   ON roleMaster.SGRoleID = sgRoles.SGRoleID        
   LEFT JOIN [dbo].[SGRolePrefix] prefix        
   ON roleMaster.PrefixID = prefix.PrefixID        
   LEFT JOIN [dbo].[SGRoleSuffix] suffix        
  ON roleMaster.SuffixID = suffix.SuffixID        
  INNER JOIN [dbo].[Departments] dept        
  ON roleMaster.DepartmentID = dept.DepartmentID
  LEFT JOIN [dbo].[KRAGroup] kraGroup            
  ON roleMaster.KRAGroupID = kraGroup.KRAGroupID 
  WHERE roleMaster.RoleMasterID = @RoleID        
  
END  