﻿--exec usp_CreateProject 'TestP1','TestP1',2,17,2,152,5,'2019-02-07 14:25:07.910','2019-12-07 14:25:07.910','bhavani.chintamadaka@senecaglobal.com','2019-02-07 14:25:07.910','192.168.2.236',1

CREATE PROCEDURE [dbo].[usp_CreateProject]    
 (    
  @ProjectCode VARCHAR(50),    
  @ProjectName VARCHAR(50),    
  @ProjectTypeId INT,      
  @ClientId INT,     
  @DomainId INT,    
  @ProgramManagerId INT,     
  @PracticeAreaId INT,   
  @ActualStartDate DATETIME ,    
  @ActualEndDate DATETIME ,    
  @CreatedUser  VARCHAR(100),    
  @CreatedDate  DATETIME,    
  @SystemInfo VARCHAR(50),    
  @DepartmentId INT    
 )                 
AS                    
BEGIN                
               
 SET NOCOUNT ON;     
 DECLARE @ProjectId INT 
 DECLARE @ProjectState INT 
 DECLARE @CategoryId INT      
  
  SELECT @CategoryId = CategoryID FROM CategoryMaster where CategoryName = 'PPC'  
  SELECT @ProjectState=StatusId FROM STATUS WHERE CategoryID = @CategoryId AND StatusCode='Drafted' 
    
 IF EXISTS (SELECT * FROM Projects WHERE projectcode=@ProjectCode  OR ProjectName=@ProjectName)    
 SELECT -1;    
   
 ELSE    
 BEGIN    
    INSERT INTO Projects(    
     ProjectCode    
    ,ProjectName  
    ,ProjectTypeId    
    ,ClientId    
    ,DomainId    
    ,CreatedUser    
    ,CreatedDate        
    ,SystemInfo    
    ,ActualStartDate    
    ,ActualEndDate    
    ,DepartmentId    
    ,PracticeAreaId
	,ProjectStateId)    
    VALUES(    
     @ProjectCode    
    ,@ProjectName    
    ,@ProjectTypeId    
    ,@ClientId    
    ,@DomainId   
    ,@CreatedUser    
    ,@CreatedDate       
    ,@SystemInfo    
    ,@ActualStartDate    
    ,@ActualEndDate    
    ,@DepartmentId    
    ,@PracticeAreaId
	,@ProjectState)    
    
 SET @ProjectId =SCOPE_IDENTITY()     
        
   INSERT INTO ProjectManagers(    
     ProjectId        
    ,ProgramManagerID    
    ,IsActive    
    ,CreatedBy    
    ,CreatedDate        
    ,SystemInfo    
    )     
 VALUES(    
     @ProjectId       
    ,@ProgramManagerId    
    ,1    
    ,@CreatedUser    
    ,@CreatedDate        
    ,@SystemInfo    
    )    
    
 SELECT @ProjectId;     
 END    
 END 