﻿-- =======================================================================================
-- Author				:	Sushmitha
-- Create date			:	11-05-2018
-- Modified date		:	11-05-2018
-- Modified By			:	Sushmitha
-- Description			:	Gets list of KRA scale values.
-- =======================================================================================   

CREATE PROCEDURE [dbo].[usp_GetKRAScaleValues]
AS
BEGIN
       SET NOCOUNT ON;      

	SELECT DISTINCT 
		kraScale.KRAScaleMasterID AS Id,
        STUFF( (SELECT ', ' +  CONVERT(VARCHAR, scaleDetails.KRAScale) + ' - ', scaleDetails.ScaleDescription
               FROM KRAScaleDetails scaleDetails
               WHERE scaleDetails.KRAScaleMasterID = kraScale.KRAScaleMasterID
               FOR XML PATH(''), TYPE).value('.', 'varchar(max)')
            ,1,1,'')
		AS Name
      FROM 
		[dbo].[KRAScaleDetails] kraScale

END