﻿-- =========================================================================
-- Author			:	Sushmitha
-- Create date		:	28-11-2017
-- Modified date	:	28-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Get NotificationDetails To Submit Talent requisition
-- ========================================================================= 
CREATE PROCEDURE [dbo].[usp_GetNotificationDetailsToSubmitTR] 
@TalentRequisitionId INT
AS
BEGIN
 SET NOCOUNT ON;

 SELECT
	[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS RoleName
 FROM
	[dbo].[TalentRequisition] talentRequisition
	INNER JOIN [dbo].[RequisitionRoleDetails] requisitionRoleDetails
	ON talentRequisition.TRId = requisitionRoleDetails.TRId
	INNER JOIN [dbo].[RoleMaster] rolemaster
	ON requisitionRoleDetails.RoleMasterId = rolemaster.RoleMasterID
 WHERE
	talentRequisition.TRId = @TalentRequisitionId
 END