﻿
-- ======================================================    
-- Author   : Ramya      
-- Create date  : 06-03-2018  
-- Modified date : 08-11-2018
-- Modified By  : BHAVANI  
-- Description  : Allocate an associate to the project
-- [dbo].[usp_AllocateAssociate] 1004,41,42,21,'15-Mar-2018',1,0,0,0,0,1,101,0,0,'15-Mar-2018','ramya', '192.168.2.208'
-- ======================================================            
CREATE PROCEDURE [dbo].[usp_AllocateAssociate]
( 
 @TalentRequisitionId int,
 @ProjectId Int ,
 @EmployeeId INT, 
 @RoleMasterId int,
 @EffectiveDate DateTime,
 @AllocationPercentageId int,
 @InternalBillingPercentage int,
 @ClientBillingPercentage int,
 @IsCritical bit,
 @IsBillable bit,
 @IsPrimary bit,
 @ReportingManagerId int, 
 @ClientBillingRoleId int,
 @InternalBillingRoleId int, 
 @CreatedDate Datetime,
 @CreatedBy varchar(150),
 @SystemInfo varchar(50)
)  
AS  
BEGIN  
 SET NOCOUNT ON;

       DECLARE @ProjectType varchar(100),@AssociateAllocationId INT,@PoolEffectiveDate DateTime,@ProjectEffectiveDate DateTime,@TotalUtilizationpercentage int
	   DECLARE @TalentPoolProjectID INT,@TalentPoolReportingManagerId INT,@AllocationPercentage INT,@ProjectProgramManagerId INT,@ProjectLeadId INT,@ProjectTypeId INT
	   
	   /* Get Program Manager and Lead for that project */

	   Select @ProjectProgramManagerId=ProgramManagerID,@ProjectLeadId=LeadID from ProjectManagers where ProjectID= @ProjectId and IsActive=1


	   --SELECT @ProjectType=ProjectTypeCode FROM ProjectType WHERE ProjectTypeId=6
	   SELECT @ProjectTypeId = ProjectTypeId FROM ProjectType WHERE ProjectTypeCode LIKE '%TALENT POOL%'
	   /* Check user has talentpool project or not*/
	   IF EXISTS(SELECT 1 FROM [AssociateAllocation] AS [allocation]
			INNER JOIN [Projects] AS [project] ON [allocation].[ProjectId] = ([project].[ProjectId])
			WHERE ([allocation].[EmployeeId] = @EmployeeId) AND ([allocation].[IsActive] = 1) AND ([project].[IsActive] = 1) AND ([project].[ProjectTypeId] = @ProjectTypeId))
		BEGIN	
		 
			SELECT @AssociateAllocationId=allocation.AssociateAllocationId,@PoolEffectiveDate=allocation.EffectiveDate,@TalentPoolReportingManagerId=projectmanager.ReportingManagerId,@TalentPoolProjectID=allocation.ProjectId FROM [AssociateAllocation] AS 
			[allocation]
				INNER JOIN [Projects] AS [project] ON [allocation].[ProjectId] = ([project].[ProjectId])
				INNER JOIN [ProjectManagers] As [projectmanager] ON [project].[ProjectId] = ([projectmanager].[ProjectId])
				WHERE ([allocation].[EmployeeId] = @EmployeeId) AND ([allocation].[IsActive] = 1) AND ([project].[IsActive] = 1) AND ([project].[ProjectTypeId] = @ProjectTypeId) AND ([projectmanager].[IsActive] = 1)
		
			UPDATE AssociateAllocation
					SET ReleaseDate=
						CASE WHEN @PoolEffectiveDate=@EffectiveDate then @EffectiveDate 
							 ELSE DATEADD(day, -1, @EffectiveDate)
						END ,
				   IsActive=0,
				   ModifiedDate=GETDATE(),
				   ModifiedBy=@CreatedBy,
				   SystemInfo=@SystemInfo
			   WHERE
			   AssociateAllocationId=@AssociateAllocationId
		END

		/* Check already allocated to same project */
		IF EXISTS(SELECT 1 FROM [AssociateAllocation] AS [allocation]			
			WHERE [allocation].[EmployeeId] = @EmployeeId AND [allocation].[IsActive] = 1 AND allocation.ProjectId=@ProjectId AND allocation.RoleMasterId=@RoleMasterId)
		BEGIN
		
			SET @AssociateAllocationId=0
			SELECT @AssociateAllocationId=AssociateAllocationId,@ProjectEffectiveDate=EffectiveDate  FROM [AssociateAllocation] AS [allocation]			
				WHERE [allocation].[EmployeeId] = @EmployeeId AND [allocation].[IsActive] = 1 AND allocation.ProjectId=@ProjectId AND allocation.RoleMasterId=@RoleMasterId

			UPDATE AssociateAllocation
					SET ReleaseDate=
						CASE WHEN @ProjectEffectiveDate=@EffectiveDate then @EffectiveDate 
							 ELSE DATEADD(day, -1, @EffectiveDate)
						END ,
				   IsActive=0,
				   ModifiedDate=GETDATE(),
				   ModifiedBy=@CreatedBy,
				   SystemInfo=@SystemInfo
			   WHERE
			   AssociateAllocationId=@AssociateAllocationId
		END

		IF(@@ROWCOUNT=0)
		BEGIN
		
			INSERT INTO [dbo].[AssociateAllocation]
				   ([ProjectId]
				   ,[EmployeeId]
				   ,[RoleMasterId]
				   ,[TRId]
				   ,[IsActive]
				   ,[AllocationPercentage]
				   ,[InternalBillingPercentage]
				   ,[ClientBillingPercentage]
				   ,[IsCritical]
				   ,[EffectiveDate]
				   ,[AllocationDate]
				   ,[CreatedBy]           
				   ,[CreateDate]         
				   ,[SystemInfo]
				   ,[ReportingManagerId]          
				   ,[IsBillable]
				   ,[IsPrimary]
				   ,[ClientBillingRoleId]
				   ,[InternalBillingRoleId]
				   ,[ProgramManagerID]
				   ,[LeadID]
				   )         
          
			 VALUES
				   (@ProjectId
				   ,@EmployeeId
				   ,@RoleMasterID
				   ,@TalentRequisitionId
				   ,1
				   ,@AllocationPercentageId
				   ,@InternalBillingPercentage
				   ,@ClientBillingPercentage
				   ,@IsCritical
				   ,@EffectiveDate
				   ,Getdate()
				   ,@CreatedBy           
				   ,Getdate()           
				   ,@SystemInfo
				   ,@ReportingManagerId           
				   ,@IsBillable
				   ,@IsPrimary
				   ,@ClientBillingRoleId
				   ,@InternalBillingRoleId
				   ,@ProjectProgramManagerId
				   ,@ProjectLeadId      
				   )

			SELECT @@ROWCOUNT

		   /* Get Toatl allocation percentage */
		  
			SELECT @TotalUtilizationpercentage=  SUM(Cast(per.Percentage as int))
			FROM [AssociateAllocation] AS [allocation]
			INNER JOIN [AllocationPercentage] per ON [allocation].AllocationPercentage = per.AllocationPercentageID
			INNER JOIN [Projects] AS [project] ON [allocation].[ProjectId] = ([project].[ProjectId])
			WHERE ([allocation].[EmployeeId] = @EmployeeId) AND ([allocation].[IsActive] = 1) AND ([project].[ProjectTypeId] <> @ProjectTypeId)
			--([project].[ProjectName] NOT LIKE '%TALENT POOL%')
			GROUP BY [allocation].[EmployeeId]
			
			IF(@TotalUtilizationpercentage<100)
			BEGIN

				SELECT @AllocationPercentage=AllocationPercentageID From AllocationPercentage where Percentage=(100-@TotalUtilizationpercentage)

				INSERT INTO [dbo].[AssociateAllocation]
					   ([ProjectId]
					   ,[EmployeeId]
					   ,[RoleMasterId]
					   ,[TRId]
					   ,[IsActive]
					   ,[AllocationPercentage]
					   ,[InternalBillingPercentage]
					   ,[ClientBillingPercentage]
					   ,[IsCritical]
					   ,[EffectiveDate]
					   ,[AllocationDate]
					   ,[CreatedBy]           
					   ,[CreateDate]         
					   ,[SystemInfo]
					   ,[ReportingManagerId]          
					   ,[IsBillable]
					   ,[ProgramManagerID]
					   ,[LeadID]	   
					   )         
          
				 VALUES
					   (@TalentPoolProjectID
					   ,@EmployeeId
					   ,@RoleMasterID
					   ,@TalentRequisitionId
					   ,1
					   ,@AllocationPercentage
					   ,0
					   ,0
					   ,0
					   ,@EffectiveDate
					   ,Getdate()
					   ,@CreatedBy           
					   ,Getdate()           
					   ,@SystemInfo
					   ,@TalentPoolReportingManagerId           
					   ,0
					   ,@ProjectProgramManagerId
					   ,@ProjectLeadId   	   
					   )
			END

			/* Making Inactive for other primary projects if there is other than the current project is IsPrimary is true */
			IF(@IsPrimary=1)
			BEGIN

			Update [AssociateAllocation] SET IsPrimary=0 
			where AssociateAllocationId in(
				SELECT AssociateAllocationId FROM [AssociateAllocation] AS [allocation]			
						WHERE [allocation].[EmployeeId] = @EmployeeId 
						     AND [allocation].[IsActive] = 1 
							 AND allocation.ProjectId<>@ProjectId 
							 AND allocation.IsPrimary=1)
			END			  	
		END

		/* Update Reporting Manager in Employee */
		UPDATE Employee set ReportingManager = (Select ReportingManagerId from AssociateAllocation WHERE EmployeeId = @EmployeeId AND IsActive = 1 AND IsPrimary=1) 
		WHERE EmployeeId = @EmployeeId AND IsActive = 1

END



GO


