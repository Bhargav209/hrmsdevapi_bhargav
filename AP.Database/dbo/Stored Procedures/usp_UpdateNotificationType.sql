﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	28-03-2018
-- Modified date	:	28-03-2018
-- Modified By		:	Sushmitha
-- Description		:	Updates Notification Type.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_UpdateNotificationType]
(
@NotificationTypeID INT,       
@NotificationCode NVARCHAR(50),
@NotificationDescription NVARCHAR(150),
@CategoryId INT,
@ModifiedUser VARCHAR(100),    
@SystemInfo VARCHAR(50)
)
AS
BEGIN    
    
 SET NOCOUNT ON;  

 UPDATE 
	[dbo].[NotificationType]
 SET
	ModifiedDate = GETDATE()
 WHERE 
	NotificationCode = @NotificationCode AND NotificationTypeID <> @NotificationTypeID

 IF (@@ROWCOUNT = 0)
 BEGIN

 UPDATE 
	[dbo].[NotificationType] 
 SET
	 NotificationCode         = @NotificationCode
	,NotificationDesc         = @NotificationDescription
	,ModifiedUser		      = @ModifiedUser
	,SystemInfo               = @SystemInfo
 WHERE 
	NotificationTypeID = @NotificationTypeID AND CategoryId = @CategoryId
    
 SELECT @@ROWCOUNT 

END
ELSE
SELECT -1
END
