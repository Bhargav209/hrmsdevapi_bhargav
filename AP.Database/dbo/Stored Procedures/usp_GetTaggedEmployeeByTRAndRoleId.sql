﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	08-01-2018
-- Modified date	:	08-01-2018
-- Modified By		:	Sushmitha
-- Description		:	Get tagged employees for requisition by trid and roleid.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetTaggedEmployeeByTRAndRoleId] 
@TalentRequisitionId INT,
@RoleMasterId INT
AS
BEGIN

SELECT      
    employee.EmployeeCode
   ,employee.FirstName + ' ' + employee.LastName AS EmployeeName
   ,allocation.IsBillable
   ,allocation.IsPrimary
   ,allocation.IsCritical
   ,projects.ProjectName
FROM 
	[dbo].[TalentRequisition] talentRequisition  
 INNER JOIN [dbo].[TalentRequisitionEmployeeTag] employeeTag  
 ON talentRequisition.TRId = employeeTag.TalentRequisitionID
 INNER JOIN [dbo].[RequisitionRoleDetails] roleDetails
 ON employeeTag.RoleMasterID = roleDetails.RoleMasterId AND employeeTag.TalentRequisitionID = roleDetails.TRId
 INNER JOIN [dbo].[Employee] employee
 ON employeeTag.EmployeeId = employee.EmployeeId
 INNER JOIN [dbo].[AssociateAllocation] allocation
 ON allocation.EmployeeId = employee.EmployeeId AND allocation.IsActive = 1
 INNER JOIN [dbo].[Projects] projects
 ON allocation.ProjectId = projects.ProjectId
WHERE
	talentRequisition.TRId = @TalentRequisitionId AND employeeTag.RoleMasterID = @RoleMasterId


END