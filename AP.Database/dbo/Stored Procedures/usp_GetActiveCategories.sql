﻿-- =================================================   
-- Author			:	Santosh            
-- Create date		:	31-10-2017            
-- Modified date	:	01-11-2017            
-- Modified By		:	Santosh            
-- Description		:	Gets all the active category    
-- =================================================  
CREATE PROCEDURE [dbo].[usp_GetActiveCategories]           
AS            
BEGIN        
        
 SET NOCOUNT ON;   
         
 SELECT   
	CategoryID    
	,CategoryName    
 FROM [dbo].[CategoryMaster]    
 WHERE IsActive = 1    
END