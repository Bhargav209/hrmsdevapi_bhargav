﻿-- ============================================     
-- Author			:	Sushmitha            
-- Create date		:	22-05-2018            
-- Modified date	:	22-05-2018            
-- Modified By		:	Sushmitha            
-- Description		:	Delete scale master by id
-- ============================================

CREATE PROCEDURE [dbo].[usp_DeleteScaleMaster]
(  
  @KRAScaleMasterId INT   
)  
AS  
BEGIN  
 SET NOCOUNT ON;  

 IF EXISTS (SELECT KRADefinitionId FROM [dbo].[KRADefinition] WHERE KRAScaleMasterID = @KRAScaleMasterId)
	SELECT 4  --Unable to perform delete as it has aspects in definition table.
 
 ELSE
 BEGIN
	DELETE
	FROM 
		[dbo].[KRAScaleDetails]    
	WHERE 
		KRAScaleMasterID = @KRAScaleMasterId 

	DELETE
	FROM 
		[dbo].[KRAScaleMaster]    
	WHERE 
		KRAScaleMasterID = @KRAScaleMasterId
 END

   SELECT @@ROWCOUNT 
END  

