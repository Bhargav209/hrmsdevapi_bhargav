-- ========================================================
-- Author			:	Praveen
-- Create date		:	06-11-2018
-- Modified date	:	06-11-2018
-- Modified By		:	Praveen
-- Description		:	Get ClientBillingRoles.
-- ========================================================      
CREATE PROCEDURE [dbo].[usp_GetClientBillingRoles]    
AS    
BEGIN    
 SET NOCOUNT ON;    
      
 SELECT    
   clientBillingRoles.ClientBillingRoleId     
  ,clientBillingRoles.ClientBillingRoleCode    
  ,clientBillingRoles.ClientBillingRoleName     
  --,clients.ClientId    
  --,clients.ClientCode
  --,clients.ClientName
  ,clientBillingRoles.IsActive   
 FROM [dbo].[ClientBillingRoles] clientBillingRoles    
 --INNER JOIN [dbo].[Clients] clients ON clientBillingRoles.ClientId = clients.ClientId AND clients.IsActive = 1    
 WHERE clientBillingRoles.IsActive = 1 
 --ORDER BY clients.ClientName, clientBillingRoles.ClientBillingRoleCode
END
