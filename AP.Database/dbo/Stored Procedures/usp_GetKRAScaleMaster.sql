﻿-- =======================================================================================
-- Author				:	Sushmitha
-- Create date			:	22-05-2018
-- Modified date		:	22-05-2018
-- Modified By			:	Sushmitha
-- Description			:	Gets list of KRA scale masters.
-- =======================================================================================   
CREATE PROCEDURE [dbo].[usp_GetKRAScaleMaster]
AS
BEGIN
       SET NOCOUNT ON;      
                   
       SELECT
           kraScaleMaster.KRAScaleMasterID
          ,MinimumScale
          ,MaximumScale
		  ,CAST(MinimumScale AS VARCHAR(1)) + ' - ' + CAST(MaximumScale AS VARCHAR(2)) AS ScaleLevel
		  ,KRAScaleTitle
       FROM [dbo].[KRAScaleMaster] kraScaleMaster
END
