﻿-- ========================================================      
-- Author   : Basha      
-- Create date  : 27-12-2018      
-- Modified date : 11-01-2019     
-- Modified By  : Mithun      
-- Description  : Adds SOW .      
-- ======================================================== 
     
CREATE PROCEDURE [dbo].[usp_CreateSOW]      
(        
@ProjectId INT,       
@SOWId Varchar(50), 
@SOWFileName VARCHAR(50),  
@SignedDate VARCHAR(50),  
@IsActive Bit,  
@CreatedDate DATETIME,        
@CreatedUser VARCHAR(150),        
@SystemInfo VARCHAR(50)        
)        
AS      
BEGIN      
 SET NOCOUNT ON;  
  
 INSERT INTO       
 [dbo].[SOW]        
 (ProjectId, SOWId,SOWFileName,SOWSignedDate,IsActive, CreatedDate, CreatedUser, SystemInfo)        
 VALUES      
 (@ProjectId, @SOWId,@SOWFileName,@SignedDate ,@IsActive,@CreatedDate,@CreatedUser, @SystemInfo)        
        
SELECT @@ROWCOUNT      
END