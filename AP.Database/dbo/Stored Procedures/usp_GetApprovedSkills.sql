﻿-- ========================================================
-- Author			:	Prasanna R
-- Create date		:	12-02-2019
-- Modified date	:	12-02-2019
-- Modified By		:	Prasanna R
-- Description		:	Get Skills
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetApprovedSkills]    
(    
@EmployeeID INT    
)    
AS    
BEGIN    
    
SET NOCOUNT ON;     

SELECT  DISTINCT    
  es.ID,      
  es.EmployeeId,     
  es.SkillId,    
  es.SkillGroupId,        
  sk.SkillName ,      
  es.Experience,      
  es.IsPrimary,      
  es.LastUsed,        
  es.CompetencyAreaId,     
  es.proficiencyLevelId,    
  sk.SkillCode    
    
FROM  EmployeeSkills es
 INNER JOIN skills sk on es.skillId=sk.skillId    

WHERE     
 es.EmployeeId=@EmployeeID 
END 

