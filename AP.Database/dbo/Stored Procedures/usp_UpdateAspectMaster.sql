﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	20-07-2018
-- Modified date	:	20-07-2018
-- Modified By		:	Sushmitha
-- Description		:	Updates Aspects Master.

-- Author			:	Kalyan
-- Create date		:	-
-- Modified date	:	24-07-2018
-- Modified By		:	-
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_UpdateAspectMaster]
(
@AspectId INT,       
@AspectName VARCHAR(70), 
@ModifiedDate DATETIME,    
@ModifiedUser VARCHAR(150),    
@SystemInfo VARCHAR(50)
)
AS
BEGIN    
    
 SET NOCOUNT ON;  

 IF NOT EXISTS( SELECT 1 FROM KRAAspectMaster WHERE AspectId = @AspectId)
 BEGIN

	UPDATE 
		[dbo].[AspectMaster]
	SET
		ModifiedDate = GETDATE()
	WHERE 
		AspectName = @AspectName AND AspectId <> @AspectId

	IF (@@ROWCOUNT = 0)
	BEGIN
		UPDATE 
			[dbo].[AspectMaster] 
		SET
			AspectName            = @AspectName
			,ModifiedDate		  = @ModifiedDate
			,ModifiedUser		  = @ModifiedUser
			,SystemInfo           = @SystemInfo
		WHERE 
			AspectId = @AspectId

	SELECT @@ROWCOUNT 
	END
	ELSE
		BEGIN
			SELECT -1 --duplicate
		END
 END
 ELSE -- Main IF
	BEGIN
		SELECT -14  --Aspects already mapped to a department.
	END
END

