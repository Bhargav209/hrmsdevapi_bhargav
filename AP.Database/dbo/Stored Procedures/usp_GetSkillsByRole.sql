﻿-- ==========================================  
-- Author   : Santosh  
-- Create date  : 19-02-2018  
-- Modified date : 19-02-2018                  
-- Modified By  : Santosh        
-- Description  : Gets skills by role ID  
-- [dbo].[usp_GetSkillsByRole]  1  
 -- ==========================================        
CREATE PROCEDURE [dbo].[usp_GetSkillsByRole]    
 @RoleMasterID INT    
 AS            
BEGIN        
        
 SET NOCOUNT ON;  
  
 SELECT      
  skill.CompetencySkillsId  
 ,skill.CompetencyAreaId  
 ,ca.CompetencyAreaCode AS CompetencyAreaCode   
 ,skill.SkillGroupID      
 ,sg.SkillGroupName      
 ,skill.SkillId      
 ,s.SkillName             
 ,skill.ProficiencyLevelId      
 ,pl.ProficiencyLevelCode  
 ,skill.IsPrimary     
 FROM CompetencySkills skill      
 INNER JOIN CompetencyArea ca    
 ON skill.CompetencyAreaId = ca.CompetencyAreaId and ca.CompetencyAreaId<>28      
 INNER JOIN SkillGroup sg        
 ON skill.SkillGroupID = sg.SkillGroupId        
 INNER JOIN Skills s      
 ON skill.SkillId = s.SkillId      
 INNER JOIN ProficiencyLevel pl      
 ON skill.ProficiencyLevelId = pl.ProficiencyLevelId      
 WHERE skill.RoleMasterID = @RoleMasterID  
 END