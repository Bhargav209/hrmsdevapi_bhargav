﻿-- ==========================================
-- Author			:	Chandra
-- Create date		:	01-02-2018
-- Modified date	:	01-02-2018               
-- Modified By		:	Chandra      
-- Dsacription		:	Get Employee skills For Notification
-- [dbo].[usp_GetEmployeeskillsForNotification]
 -- ==========================================      
CREATE PROCEDURE [dbo].[usp_GetEmployeeskillsForNotification] 
(
@SkillId INT,
@empID INT
)
AS          
BEGIN      
      
 SET NOCOUNT ON;

SELECT    
  
 emp.FirstName  as EmployeeName
 ,s.SkillName  as SkillName
 ,sa.Experience  as experience
 ,sa.LastUsed as LastUsed
 ,sa.IsPrimary as  isPrimary
 ,pl.ProficiencyLevelCode as ProficiencyLevel
 ,st.StatusCode as StatusCode
 ,sg.SkillGroupName as SkillGroupName
 ,us.EmailAddress as ToEmailAddress

 FROM EmployeeSkills sa  
 INNER JOIN Employee emp    
 ON sa.EmployeeId = emp.EmployeeId  
 INNER JOIN Users us    
 ON emp.UserId = us.UserId
 INNER JOIN Skills s    
 ON sa.SkillId = s.SkillId    
 INNER JOIN ProficiencyLevel pl    
 ON sa.ProficiencyLevelId = pl.ProficiencyLevelId
 INNER JOIN SkillGroup sg    
 ON sa.SkillGroupId = sg.SkillGroupId
 INNER JOIN SkillsWorkFlow swf    
 ON sa.RequisitionId = swf.SubmittedRequisitionId 
 left JOIN Status st    
 ON swf.Status = st.StatusId
 where sa.SkillId=@SkillId and sa.EmployeeId=@empID

END