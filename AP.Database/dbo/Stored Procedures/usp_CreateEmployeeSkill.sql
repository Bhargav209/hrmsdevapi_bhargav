﻿-- ========================================================
-- Author			:	Chandra
-- Create date		:	12-31-2018
-- Modified date	:	12-31-2018
-- Modified By		:	Chandra
-- Description		:	Create Employee Skill after RM approval
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateEmployeeSkill]
(
@SkillsSubmittedForApprovalId INT,
@EmployeeId INT,
@CompetencyAreaId INT,
@SkillId INT,
@ProficiencyLevelId INT,
@Experience INT,
@LastUsed INT,
@IsPrimary BIT,
@CreatedUser VARCHAR(150),
@CreatedDate DateTime,
@SystemInfo VARCHAR(50),
@SkillGroupId INT,
@WorkFlowId INT,
@SubmittedBy INT,
@SubmittedTo INT

)  
AS
BEGIN

 SET NOCOUNT ON;

 declare @StatusId INT;
 declare @RequisitionId INT;
 select @StatusId = statusId from Status where StatusCode='ApprovedByLead'
   
 UPDATE	[dbo].[SkillsSubmittedForApproval]  SET
	 
	EmployeeId=@EmployeeId,
	CompetencyAreaId=@CompetencyAreaId,
	SkillId=@SkillId,
	ProficiencyLevelId=@ProficiencyLevelId,
	Experience=@Experience,
	LastUsed=@LastUsed,
	IsPrimary=@IsPrimary,
	IsActive=1,
	ModifiedUser=@CreatedUser,
	ModifiedDate=@CreatedDate,
	SystemInfo=@SystemInfo,
	SkillGroupId=@SkillGroupId  

	where ID=@SkillsSubmittedForApprovalId
  
  	
INSERT INTO 
	[dbo].[EmployeeSkills]  
	(EmployeeId,CompetencyAreaId,SkillId,ProficiencyLevelId,Experience,LastUsed,IsPrimary,IsActive,CreatedUser,CreatedDate,SystemInfo,SkillGroupId,RequisitionId)  
 VALUES  
	(@EmployeeId,@CompetencyAreaId,@SkillId,@ProficiencyLevelId,@Experience,@LastUsed,@IsPrimary,1,@CreatedUser,@CreatedDate,@SystemInfo,@SkillGroupId,@SkillsSubmittedForApprovalId)  

UPDATE 
	[dbo].[SkillsWorkFlow] 
	SET  
	SubmittedBy=@SubmittedBy,
	SubmittedTo=@SubmittedTo, 
	Status=@StatusId,
	ApprovedDate= @CreatedDate,
	SubmittedRequisitionId =@SkillsSubmittedForApprovalId 
	where WorkFlowId=@WorkFlowId
	  
 SELECT @@ROWCOUNT

END
