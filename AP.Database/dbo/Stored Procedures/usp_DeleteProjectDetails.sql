﻿CREATE PROCEDURE [dbo].[usp_DeleteProjectDetails]      
 (      
  @ProjectId INT  
)                   
AS                      
BEGIN                  
                 
 SET NOCOUNT ON;  
DECLARE @CategoryId INT  
DECLARE @StatusId INT  
  
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'    
 SELECT @StatusId=StatusId FROM status WHERE StatusCode='Drafted' AND CategoryID=@CategoryId    
  
  IF EXISTS(SELECT 1 FROM ClientBillingRoles  WHERE ProjectId = @ProjectId  )  
    DELETE ClientBillingRoles WHERE ProjectId = @ProjectId  
  
IF EXISTS (SELECT 1 FROM SOW WHERE ProjectId = @ProjectId)  
    DELETE SOW WHERE ProjectId = @ProjectId  
  
IF EXISTS (SELECT 1 FROM ProjectManagers WHERE ProjectId = @ProjectId)  
    DELETE ProjectManagers WHERE ProjectId = @ProjectId  
  
IF EXISTS (SELECT 1 FROM projects WHERE ProjectId = @ProjectId AND ProjectStateId=@StatusId)        
    DELETE projects WHERE ProjectId = @ProjectId AND ProjectStateId=@StatusId   
     
SELECT @@ROWCOUNT  
 END