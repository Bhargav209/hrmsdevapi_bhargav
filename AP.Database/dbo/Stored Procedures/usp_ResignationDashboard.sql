﻿-- =======================================empl==========   
-- Author			:	Sushmitha          
-- Create date		:	16-11-2017            
-- Modified date	:	         
-- Modified By		:	Sushmitha            
-- Description		:	Gets Resignation Dashboard  
-- =================================================  
CREATE PROCEDURE [dbo].[usp_ResignationDashboard]
@EmployeeID INT            
AS            
BEGIN        
        
 SET NOCOUNT ON; 

 SELECT
	  allocation.IsBillable
	 ,allocation.IsCritical
	 ,allocation.IsPrimary
	 ,allocation.ClientBillingPercentage
	 ,allocation.InternalBillingPercentage
	 ,project.ProjectName
	 ,employee.FirstName + ' ' + employee.LastName AS ReportingManager
 FROM [dbo].[AssociateAllocation] allocation 
 INNER JOIN [dbo].[Projects] project
 ON allocation.ProjectId = project.ProjectId
 INNER JOIN [dbo].[Employee] employee
 ON allocation.ReportingManagerId = employee.EmployeeId
 WHERE allocation.IsActive = 1 and allocation.EmployeeId = @EmployeeID        
END
