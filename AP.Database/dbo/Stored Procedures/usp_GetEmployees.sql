﻿-- ========================================================  
-- Author   : Sushmitha  
-- Create date  : 02-04-2018  
-- Modified date : 26-02-2019  
-- Modified By  : Sabiha  
-- Description  : Gets list of associates by search string  
-- ========================================================   
CREATE PROCEDURE [dbo].[usp_GetEmployees]  
(  
@SearchString VARCHAR(20)  
)  
AS           
BEGIN  
 SET NOCOUNT ON;      
  
 SELECT  
   EmployeeId AS Id  
     ,[dbo].[udfGetEmployeeFullName](EmployeeId) AS Name  
  ,IsActive  
 FROM  
  [dbo].[Employee]  
 WHERE  
  (FirstName LIKE '%' + @SearchString + '%' OR LastName LIKE '%' + @SearchString + '%') AND IsActive = 1  
  
END  
  