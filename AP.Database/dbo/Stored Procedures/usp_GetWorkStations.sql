﻿-- Author			:	Chandra
-- Create date		:	08-11-2018
-- Modified date	:	26-11-2018
-- Modified By		:	prasanna
-- Description		:	Gets Work Station Information.
-- ======================================================== 
CREATE PROCEDURE [dbo].[usp_GetWorkStations]
(
@BayIds INT
)
AS         
BEGIN
	SET NOCOUNT ON;    

	SELECT
		[Id]
		,(case when WorkStationSuffix is null then CAST(WorkStationId AS VARCHAR)
		else
		CAST(WorkStationId AS VARCHAR)+'-'+WorkStationSuffix End) as WorkStationCode
		,[IsOccupied]
	FROM
		[dbo].[WorkStation]
	WHERE
		BayId= @BayIds
END
