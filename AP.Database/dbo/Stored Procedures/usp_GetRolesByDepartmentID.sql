﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	12-02-2018
-- Modified date	:	12-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Gets Roles by DepartmentId.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_GetRolesByDepartmentID]    
(    
 @DepartmentID INT    
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
    
 SELECT    
   roleMaster.RoleMasterID AS Id    
  ,[dbo].[udf_GetRoleName](rolemaster.RoleMasterID) AS Name    
 FROM [dbo].[RoleMaster] roleMaster    
 INNER JOIN [dbo].[SGRole] sgRoles    
 ON sgRoles.SGRoleID = roleMaster.SGRoleID    
 LEFT JOIN [dbo].[SGRolePrefix] prefix    
 ON roleMaster.PrefixID = prefix.PrefixID    
 LEFT JOIN [dbo].[SGRoleSuffix] suffix    
 ON roleMaster.SuffixID = suffix.SuffixID    
 WHERE roleMaster.DepartmentID = @DepartmentID    
 ORDER BY Name    
END 