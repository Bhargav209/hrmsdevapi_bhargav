﻿-- ======================================
-- Author			:	Ramya
-- Create date		:	05-03-2018
-- Modified date	:	
-- Modified By		:	
-- Description		:	Get Domains
-- ======================================
CREATE PROCEDURE [dbo].[usp_GetDomain]
AS
BEGIN
	SET NOCOUNT ON;
		SELECT
			 DomainID
			,DomainName
			,CreatedDate
			,CreatedUser AS CreatedBy
			,ModifiedDate
			,ModifiedUser AS ModifiedBy
		FROM [dbo].[Domain]
		ORDER BY CreatedDate DESC
END