﻿CREATE PROCEDURE [dbo].[usp_CreateFinancialYearAndStatus]
@FromYear INT,
@ToYear INT,
@IsActive BIT

AS
 BEGIN
SET NOCOUNT ON;  

 DECLARE 
@CurrentMonth INT = MONTH(GETDATE()),
@CurrentYear INT = YEAR(GETDATE()),
@FinancialYear INT

--checking if from year already exists or not 
 SELECT @FinancialYear = COUNT(ID) FROM FinancialYear WHERE FromYear = @FromYear AND ToYear = @ToYear
 IF (@FinancialYear > 0)
   SELECT -1

--Checking if fromYear is equal to currentyear and month  after march and isactive =true 
--OR fromYear is not equal to currentyear and month  after march and isactive =false after march ,able to insert  or else return error code
ELSE 
BEGIN

IF (((@FromYear <> @CurrentYear AND @CurrentMonth > 3) AND  (@IsActive = 0)) 
			OR ((@FromYear = @CurrentYear AND @CurrentMonth > 3) AND  (@IsActive = 1))
			OR ((@FromYear = @CurrentYear-1 AND @CurrentMonth <= 3) AND  (@IsActive = 1)))
	  BEGIN

			 INSERT INTO 
				[dbo].[FinancialYear]  
				(FromYear, ToYear, IsActive)  
			 VALUES  
				(@FromYear,@ToYear,@IsActive) 
			 SELECT @@ROWCOUNT
	  END
	
ELSE
	SELECT -11
END
END