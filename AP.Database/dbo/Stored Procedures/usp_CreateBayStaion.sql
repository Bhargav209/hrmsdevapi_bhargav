﻿-- ========================================================
-- Author			:	Basha
-- Create date		:	06-09-2018
-- Modified date	:	06-09-2018
-- Modified By		:	Basha
-- Description		:	Adds BayStation.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_CreateBayStaion]
(  
@WorkStationId INT,
@BayId INT,
@CreatedUser VARCHAR(150),
@SystemInfo VARCHAR(50),
@IsOccupied BIT
)  
AS
BEGIN

 SET NOCOUNT ON;  

  INSERT INTO 
	[dbo].[WorkStation]  
	(WorkstationId,BayId, CreatedUser, SystemInfo, IsOccupied)  
 VALUES  
	(@WorkStationId,@BayId, @CreatedUser, @SystemInfo,@IsOccupied)  
  
 SELECT @@ROWCOUNT

END