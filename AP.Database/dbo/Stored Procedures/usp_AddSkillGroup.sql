﻿-- ================================================  
-- Author			:  Santosh      
-- Create date		:  31-10-2017      
-- Modified date	:  31-10-2017      
-- Modified By		:  Santosh      
-- Description		:  Adds Skill Group master     
-- ================================================  

-- ================================================  
-- Author			:  Kalyan.Penumutchu      
-- Create date		:  13-11-2017      
-- Modified date	:  13-11-2017      
-- Modified By		:  Kalyan.Penumutchu      
-- Description		:  IsActive is no longer requiered from UI. So hard-coded inside SP     
-- ================================================ 
 
CREATE PROCEDURE [dbo].[usp_AddSkillGroup]      
@SkillGroupName VARCHAR(100),     
@Description VARCHAR(MAX),
@CreatedUser VARCHAR(100),  
@SystemInfo VARCHAR(50),  
@CompetencyAreaId INT,
@CreatedDate DATETIME 
AS      
BEGIN  
  
 SET NOCOUNT ON;  
   
 INSERT INTO [dbo].[SkillGroup]      
	(
		 SkillGroupName
		,[Description]
		,IsActive
		,CreatedUser
		,ModifiedUser
		,CreatedDate
		,ModifiedDate
		,SystemInfo
		,CompetencyAreaId
	)      
 VALUES      
	(
		 @SkillGroupName
		,@Description
		,1
		,@CreatedUser
		,@CreatedUser
		,@CreatedDate
		,@CreatedDate
		,@SystemInfo
		,@CompetencyAreaId
	)      
        
 SELECT @@ROWCOUNT      
END