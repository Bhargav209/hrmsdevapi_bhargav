﻿
-- ===============================================                          
 -- Author   : Basha                          
 -- Create date : 28-08-2018                          
 -- =======================================                          
                          
CREATE PROCEDURE [dbo].[usp_ManageAssociateWorkFlow]                                  
 @EmployeeId INT,                           
 @CompetencyAreaId INT,                           
 @SkillId INT,                           
 @ProficiencyLevelId INT,                           
 @Experience INT,                           
 @LastUsed INT,                           
 @IsPrimary BIT,                            
 @CreatedUser VARCHAR(100),                          
 @CreatedDate DATETIME,                           
 @SystemInfo VARCHAR(50),                          
 @SkillGroupId INT                          
AS                          
BEGIN                          
SET NOCOUNT ON;                                 
                          
 DECLARE @categoryId INT, @statusId INT,@LeadId INT ,@Id INT  ,@TechCompetencyAreaCount INT ,@NonTechCompetencyAreaCount INT  ,@IsDuplicateSkillExist INT                 
 SELECT @categoryId=categoryId from  CategoryMaster where CategoryName='Skills'                        
 SELECT @statusId=StatusId FROM Status where StatusCode='Draft' AND CategoryID=@categoryId                   
              
 SELECT @TechCompetencyAreaCount=COUNT(CompetencyAreaId) FROM EmployeeSkills where CompetencyAreaId=1 AND EmployeeId=@EmployeeId and IsPrimary=1              
              
 SELECT @NonTechCompetencyAreaCount=COUNT(CompetencyAreaId) FROM EmployeeSkills where CompetencyAreaId <>1 AND EmployeeId=@EmployeeId and IsPrimary=1              
      
 SELECT @IsDuplicateSkillExist =COUNT(EmployeeId) from [SkillsSubmittedForApproval] where EmployeeId=@EmployeeId and SkillId=@SkillId and IsActive=1      
      
  IF( @TechCompetencyAreaCount > 50)        
     BEGIN            
       SELECT -2        
     END            
              
  ELSE  IF( @NonTechCompetencyAreaCount > 10)          
    BEGIN          
      SELECT -3              
    END      
      
  ELSE IF( @IsDuplicateSkillExist > 0)          
    BEGIN          
     UPDATE [SkillsSubmittedForApproval] SET         
            ProficiencyLevelId=@ProficiencyLevelId   
     ,LastUsed=@LastUsed  
     ,Experience=@Experience  
     ,IsPrimary=@IsPrimary      
     WHERE EmployeeId=@EmployeeId and SkillId=@SkillId and IsActive=1      
  select 1  
    END              
              
  ELSE          
    BEGIN               
     select @LeadId= dbo.[udf_GetAssociateLead](@EmployeeId)            
       if (@LeadId =-1)          
         SELECT -4          
           
       BEGIN TRY               
        INSERT INTO [dbo].[SkillsSubmittedForApproval]                           
         (                           
   [EmployeeId],                          
   [CompetencyAreaId],                          
   [SkillId],                          
   [ProficiencyLevelId],                          
   [Experience],                          
   [LastUsed],                          
   [IsPrimary],                          
   [SkillGroupId],                          
   [CreatedUser],                          
   [CreatedDate],                          
   [SystemInfo],                          
   [IsActive]                          
  )                          
  VALUES                           
  (                            
   @EmployeeId  
     ,                
    @CompetencyAreaId,                          
   @SkillId,                          
   @ProficiencyLevelId,                          
   @Experience,                          
   @LastUsed,                          
   @IsPrimary,                          
   @SkillGroupId,                          
   @CreatedUser,                          
   @CreatedDate,                          
   @SystemInfo,                          
            1                
  )                          
  SET @Id = SCOPE_IDENTITY();                      
                        
   INSERT INTO [dbo].[SkillsWorkFlow]                           
         (                           
   [SubmittedBy],            
   [SubmittedTo],                        
   [SubmittedDate],                        
   [Status],                        
   [SubmittedRequisitionId]                        
  )                          
  VALUES              
  (                            
  @EmployeeId,                        
  @LeadId,                        
  @CreatedDate,                        
  @statusId,                        
  @Id                        
  )                          
 -- SELECT @@ROWCOUNT                        
  END TRY            
  BEGIN CATCH           
     SELECT -4          
  END CATCH  
  SELECT @Id           
 END      
END 