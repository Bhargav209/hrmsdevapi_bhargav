﻿-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	27-11-2017
-- Modified date	:	27-11-2017
-- Modified By		:	Sushmitha
-- Description		:	Adds a membership detail of an employee.
-- ======================================================== 

CREATE PROCEDURE [dbo].[usp_AddMembershipDetails]            
@EmployeeID INT, 
@ProgramTitle VARCHAR(150),
@ValidFrom VARCHAR(4),
@Institution VARCHAR(150),
@Specialization VARCHAR(150),
@ValidUpto VARCHAR(4),
@CreatedUser VARCHAR(100),
@CreatedDate DATETIME,
@SystemInfo VARCHAR(50)     
AS            
BEGIN        
        
 SET NOCOUNT ON;
         
 INSERT INTO [dbo].[AssociatesMemberShips]            
 (
    EmployeeID
   ,ProgramTitle
   ,ValidFrom
   ,Institution
   ,Specialization
   ,ValidUpto
   ,CreatedUser
   ,CreatedDate
   ,SystemInfo         
 )            
 VALUES            
 (
    @EmployeeID
   ,@ProgramTitle
   ,@ValidFrom
   ,@Institution
   ,@Specialization
   ,@ValidUpto
   ,@CreatedUser
   ,@CreatedDate
   ,@SystemInfo     
 )            
              
 SELECT @@ROWCOUNT            
END 