﻿-- ========================================================================
-- Author			:	Sushmitha
-- Create date		:	30-04-2018  
-- Modified date	:	30-04-2018
-- Modified By		:	Sushmitha
-- Description		:	Initiate KRAs by HR Head.
-- =========================================================================

CREATE PROCEDURE [dbo].[usp_InitiateKRAs]
(
   @FinancialYearId INT
)
AS                    
BEGIN

SET NOCOUNT ON;      
    
	DECLARE @Count INT

	SELECT 
		@Count = COUNT(StatusId) 
	FROM 
		[dbo].[KRAStatus] 
	WHERE 
		FinancialYearId = @FinancialYearId AND StatusId != (SELECT [dbo].[udf_GetStatusId]('Approved', 'KRA')) GROUP BY FinancialYearId  --Approved

	IF (@Count >= 1)
		SELECT 0 
	ELSE 
		SELECT 1
 END  
