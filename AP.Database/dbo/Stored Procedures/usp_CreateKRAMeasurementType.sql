﻿-- =======================================     
-- Author			:	Basha          
-- Create date		:	20-07-2018    
-- Modified date	:	20-07-2018            
-- Modified By		:	Basha          
-- Description		:	Create KRA MeasurementType       
-- =======================================

CREATE PROCEDURE [dbo].[usp_CreateKRAMeasurementType]
@KRAMeasurementType varchar(70)

AS
 BEGIN
SET NOCOUNT ON;  

DECLARE
@KRAMeasurementTypeExists varchar(70),
@KRAMeasuretype varchar(70)

SELECT @KRAMeasuretype= RTRIM(LTRIM(@KRAMeasurementType))
IF(@KRAMeasuretype <>'')
	BEGIN
		--checking if KRAMeasurementType already exists or not 
		 SELECT @KRAMeasurementTypeExists = COUNT(KRAMeasurementType) FROM KRAMeasurementType WHERE KRAMeasurementType=@KRAMeasuretype
		 IF (@KRAMeasurementTypeExists > 0)
		   SELECT -1
		 ELSE  
			  BEGIN
					 INSERT INTO 
						[dbo].[KRAMeasurementType]  
						(KRAMeasurementType)  
					 VALUES  
						(@KRAMeasuretype) 
					 SELECT @@ROWCOUNT
			  END	
	END
ELSE
SELECT -11
END