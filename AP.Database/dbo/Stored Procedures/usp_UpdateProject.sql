﻿--exec usp_UpdateProject 405,'TestP112',2,17,2,24,152,5,'2019-02-07 14:25:07.910','2019-12-07 14:25:07.910','bhavani.chintamadaka@senecaglobal.com','2019-02-07 14:25:07.910','192.168.2.236',1,'Program Manager'    
    
CREATE PROCEDURE [dbo].[usp_UpdateProject]      
 (      
  @ProjectId INT,        
  @ProjectName VARCHAR(50),      
  @ProjectTypeId INT,        
  @ClientId INT,       
  @DomainId INT,    
  @ProjectStateId INT,      
  @ProgramManagerId INT,       
  @PracticeAreaId INT,    
  @ActualStartDate DATETIME,      
  @ActualEndDate DATETIME,      
  @ModifiedUser  VARCHAR(100),      
  @ModifiedDate  DATETIME,      
  @SystemInfo VARCHAR(50),      
  @DepartmentId INT,      
  @UserRole VARCHAR(20)      
 )                   
AS                      
BEGIN                  
                 
 SET NOCOUNT ON;       
      
 DECLARE @AllocationCount INT      
 DECLARE @RoleId INT    
 DECLARE @CategoryId INT    
 DECLARE @StatusId INT     
     
 SELECT @CategoryId=CategoryID FROM CategoryMaster WHERE CategoryName='PPC'    
 SELECT @StatusId=StatusId FROM Status WHERE StatusCode = 'Closed' and CategoryID = @CategoryId    
  
 IF EXISTS (SELECT * FROM Projects WHERE  ProjectName=@ProjectName AND ProjectId <> @ProjectId)        
 SELECT -1;    
    
 ELSE IF(@ProjectStateId=@StatusId)      
 BEGIN      
  SELECT @AllocationCount = COUNT(*) FROM AssociateAllocation where ProjectId = @ProjectId AND IsActive = 1      
   IF (@AllocationCount = 0)      
    BEGIN      
     SELECT @RoleId = RoleId from roles WHERE UPPER(RoleName) = UPPER(@UserRole)      
   IF EXISTS (SELECT 1 FROM UserRoles WHERE RoleId = @RoleId)      
       IF(@RoleId = 5)  -- 5 is program manager.ensure that roles table have always 5 as program manager      
          UPDATE Projects SET       
            ProjectName=@ProjectName      
           ,ProjectStateId = @ProjectStateId      
           ,ActualStartDate=@ActualStartDate      
           ,ActualEndDate = @ActualEndDate      
           ,ClientId=@ClientId     
     ,DomainId=@DomainId       
           ,PracticeAreaId=@PracticeAreaId      
           ,ProjectTypeId = @ProjectTypeId                                            
           ,DepartmentId =@DepartmentId      
           ,ModifiedUser =@ModifiedUser      
           ,ModifiedDate =@ModifiedDate      
           ,SystemInfo =@SystemInfo            
         WHERE ProjectId = @ProjectId     
       ELSE IF(@RoleId = 3)  -- 3 is department head.ensure that roles table have always 3 as department head      
       BEGIN      
        UPDATE Projects SET       
            ProjectName=@ProjectName      
           ,ProjectStateId = @ProjectStateId      
           ,ActualStartDate=@ActualStartDate      
           ,ActualEndDate = @ActualEndDate      
           ,ClientId=@ClientId    
     ,DomainId=@DomainId          
           ,PracticeAreaId=@PracticeAreaId      
           ,ProjectTypeId = @ProjectTypeId                                            
           ,DepartmentId =@DepartmentId      
           ,ModifiedUser =@ModifiedUser      
           ,ModifiedDate =@ModifiedDate      
           ,SystemInfo =@SystemInfo                
       WHERE ProjectId = @ProjectId     
    --update project manager table                   
       IF(@ProgramManagerId > 0)      
        BEGIN             
          IF EXISTS (SELECT 1 FROM ProjectManagers where ProgramManagerID = @ProgramManagerId and IsActive = 1)             
          UPDATE ProjectManagers SET      
                 ProgramManagerID = @ProgramManagerId      
                ,ModifiedBy = @ModifiedUser      
                ,ModifiedDate = @ModifiedDate      
          WHERE ProjectID = @ProjectId AND IsActive = 1         
        END      
     END      
      select @@ROWCOUNT      
 END       
  ELSE       
    SELECT -3;      
END      
ELSE      
BEGIN      
 IF EXISTS (SELECT 1 FROM projects WHERE ProjectId=@ProjectId)       
  BEGIN      
     SELECT @RoleId = RoleId from roles WHERE  UPPER(RoleName) = UPPER(@UserRole)      
     IF(@RoleId = 5)       
      UPDATE Projects SET         
        ProjectName=@ProjectName      
       ,ProjectStateId = @ProjectStateId       
       ,ActualStartDate=@ActualStartDate      
       ,ActualEndDate = @ActualEndDate      
       ,ClientId=@ClientId    
    ,DomainId=@DomainId          
       ,PracticeAreaId=@PracticeAreaId      
       ,ProjectTypeId = @ProjectTypeId                                            
       ,DepartmentId =@DepartmentId      
       ,ModifiedUser =@ModifiedUser      
       ,ModifiedDate =@ModifiedDate      
       ,SystemInfo =@SystemInfo          
     WHERE ProjectId = @ProjectId     
    ELSE IF(@RoleId = 3)      
     BEGIN      
        UPDATE Projects SET         
           ProjectName=@ProjectName      
          ,ProjectStateId = @ProjectStateId       
          ,ActualStartDate=@ActualStartDate      
          ,ActualEndDate = @ActualEndDate      
          ,ClientId=@ClientId    
    ,DomainId=@DomainId          
          ,PracticeAreaId=@PracticeAreaId      
          ,ProjectTypeId = @ProjectTypeId                                            
          ,DepartmentId =@DepartmentId      
          ,ModifiedUser =@ModifiedUser      
          ,ModifiedDate =@ModifiedDate      
          ,SystemInfo =@SystemInfo          
        WHERE ProjectId = @ProjectId       
      
        --update project manager table                   
        IF(@ProgramManagerId > 0)      
         BEGIN             
           IF EXISTS (SELECT 1 FROM ProjectManagers where ProgramManagerID = @ProgramManagerId and IsActive = 1)             
              UPDATE ProjectManagers SET      
                  ProgramManagerID = @ProgramManagerId      
                 ,ModifiedBy = @ModifiedUser      
                 ,ModifiedDate = @ModifiedDate      
              WHERE ProjectID = @ProjectId AND IsActive = 1      
         END      
      END      
      
     select @@ROWCOUNT      
   END      
ELSE      
 BEGIN      
   SELECT -2;      
 END      
 END      
END 