﻿-- =======================================     
-- Author			:	Basha          
-- Create date		:	20-07-2018    
-- Modified date	:	20-07-2018            
-- Modified By		:	Basha          
-- Description		:	Delete KRA MeasurementType       
-- =======================================

CREATE PROCEDURE [dbo].[usp_DeleteKRAMeasurementType]
@KraMeasurementTypeId INT 
AS
 BEGIN
SET NOCOUNT ON; 

DECLARE  
@KRAMeasurementTypeIdDepends INT,
@DraftStatusId INT,
@CategoryId INT,
@undraftcount INT

select @CategoryId = CategoryID from CategoryMaster where CategoryName='KRA'

   --checking if KRAMeasurementType have any dependency
 SELECT @KRAMeasurementTypeIdDepends = COUNT(KRAMeasurementTypeID) FROM KRADefinition WHERE KRAMeasurementTypeID=@KraMeasurementTypeId
 IF (@KRAMeasurementTypeIdDepends > 0)
   SELECT 9

  --Able to update only if KRA in  Draft state
 SELECT  @undraftcount = Count(*) from KRAMeasurementType kmt
	 Inner join KRADefinition kdef on kmt.KRAMeasurementTypeID=kdef.KRAMeasurementTypeID
	 inner join KRAStatus kstatus on kdef.KRAGroupId=kstatus.KRAGroupId where kmt.KRAMeasurementTypeID=@KraMeasurementTypeId and kstatus.StatusId
	 in( SELECT  StatusId FROM [dbo].[Status] WHERE StatusCode <> 'Draft' AND CategoryID = @CategoryId)


 IF (@undraftcount = 0)
 BEGIN
   DELETE FROM [dbo].[KRAMeasurementType]
		WHERE
		KRAMeasurementTypeID = @KraMeasurementTypeId
   SELECT @@ROWCOUNT
 END
  ELSE
   SELECT 0
END