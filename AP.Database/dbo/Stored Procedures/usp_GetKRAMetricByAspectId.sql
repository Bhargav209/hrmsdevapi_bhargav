﻿--IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[usp_GetKRAMetricByAspectId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
--BEGIN
--DROP PROCEDURE dbo.usp_GetKRAMetricByAspectId
--END
--GO

-- ========================================================
-- Author			:	Sushmitha
-- Create date		:	06-02-2018
-- Modified date	:	06-02-2018
-- Modified By		:	Sushmitha
-- Description		:	Get KRA metrics by KRA aspect Id.
-- ======================================================== 

--CREATE PROCEDURE [dbo].[usp_GetKRAMetricByAspectId]
--(
--@KRAAspectId INT,
--@KRARoleId INT,
--@FinancialYearId INT
--)
--AS
--BEGIN

--SET NOCOUNT ON;   
       
-- SELECT        
--	 aspects.KRAAspectID
--	,aspects.KRAAspectName  
--	,kraSet.KRAAspectMetric AS KRAMetric    
-- FROM 
--	[dbo].[KRAAspectMaster] aspects
--	INNER JOIN [dbo].[KRASet] kraSet
--	ON aspects.KRAAspectID = kraSet.KRAAspectID       
-- WHERE 
--	aspects.KRAAspectID =  @KRAAspectId and kraSet.KRARoleID = @KRARoleId and kraSet.FinancialYearID = @FinancialYearId
	   
--END
