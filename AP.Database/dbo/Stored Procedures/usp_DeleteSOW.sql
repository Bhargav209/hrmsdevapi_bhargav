﻿  
-- ========================================================      
-- Author   : Sabiha      
-- Create date  : 05-03-2019      
-- Modified date : 05-03-2019  
-- Modified By  : Sabiha      
-- Description  : Delete SOW  
-- ========================================================       
CREATE PROCEDURE [dbo].[usp_DeleteSOW]   
(      
 @Id INT      
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
    IF EXISTS (SELECT 1 FROM SOW WHERE Id = @Id)  
 BEGIN  
  DELETE FROM SOW WHERE Id = @Id   
  SELECT @@ROWCOUNT  
 END  
 ELSE   
  SELECT -1   
END  
  