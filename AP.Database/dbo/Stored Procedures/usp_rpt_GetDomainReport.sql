﻿-- ============================================    
-- Author   : Santosh    
-- Create date  : 26-04-2018    
-- Modified date : 26-02-2019    
-- Modified By  : Sabiha    
-- Description  : Get domain report    
 -- ===========================================    
CREATE PROCEDURE [dbo].[usp_rpt_GetDomainReport]  
(  
  @DomainID INT  
)    
AS              
BEGIN    
     
 SET NOCOUNT ON;    
  
 SELECT  DISTINCT  
     employee.EmployeeId   
  ,employee.EmployeeCode    
 ,dbo.udfGetEmployeeFullName(employee.EmployeeId) AS EmployeeName  
 ,designation.DesignationCode AS Designation  
 ,grade.GradeCode AS Grade  
 ,ISNULL(employee.TotalExperience,0) as Experience  
 FROM EmployeeProjects employeeprojects  
 INNER JOIN Domain domain  
 ON employeeprojects.DomainID = domain.DomainID  
 INNER JOIN Employee employee  
 ON employeeprojects.EmployeeId = employee.EmployeeId  
 INNER JOIN Designations designation  
 ON employee.Designation = designation.DesignationId  
 INNER JOIN Grades grade  
 ON employee.GradeId = grade.GradeId  
 WHERE domain.DomainID = @DomainID  
  
END  