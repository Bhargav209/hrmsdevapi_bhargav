﻿-- ==================================================
-- Author			:	Chandra
-- Create date		:	12-11-2018
-- Modified date	:	12-11-2018
-- Modified By		:	Chandra
-- Description		:	Get Work Station Report Count
 -- =================================================
CREATE PROCEDURE [dbo].[usp_chart_WorkStationReportCount]
(
@BayId INT
)
AS          
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	COUNT(workStation.BayId) WorkStationCount
	,workStation.IsOccupied
	,(case WHEN workStation.IsOccupied = 1 THEN 'Occupied' ELSE 'Vacant' END) Name
	FROM WorkStation workStation	
	where workStation.BayId=@BayId
	GROUP BY workStation.BayId,workStation.IsOccupied

END



