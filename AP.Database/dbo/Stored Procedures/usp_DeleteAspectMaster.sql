﻿-- ===============================================  
-- Author			:	Sushmitha
-- Create date		:	20-07-2018
-- Modified date	:	20-07-2018
-- Modified By		:	Sushmitha       
-- Description		:  Sets a Category to inactive         
-- =============================================== 

CREATE PROCEDURE [dbo].[usp_DeleteAspectMaster]
@AspectId INT
AS
BEGIN      
      
 SET NOCOUNT ON;
IF NOT EXISTS( SELECT 1 FROM KRAAspectMaster WHERE AspectId = @AspectId)
 BEGIN
	DELETE FROM [dbo].[AspectMaster] 
	WHERE AspectId = @AspectId  
            
	SELECT @@ROWCOUNT  
 END
ELSE
	BEGIN
		SELECT -14  --Aspects already mapped a department.
	END      
END
