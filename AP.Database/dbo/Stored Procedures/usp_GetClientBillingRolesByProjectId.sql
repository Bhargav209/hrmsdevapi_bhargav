﻿-- ========================================================  
-- Author   : Praveen  
-- Create date  : 06-11-2018  
-- Modified date : 01-03-2019  
-- Modified By  : Sabiha  
-- Description  : Get ClientBillingRoles by Project  
-- [dbo].[usp_GetClientBillingRolesByProjectId] 153  
-- ========================================================   
CREATE PROCEDURE [dbo].[usp_GetClientBillingRolesByProjectId]  
(  
  @ProjectId INT  
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
        
 SELECT      
  clientBillingRoles.ClientBillingRoleId   
 ,clientBillingRoles.ClientBillingRoleName  
 ,clientBillingRoles.NoOfPositions  
 ,clientBillingRoles.StartDate    
 ,clientBillingRoles.EndDate  
 ,clientBillingRoles.ClientBillingPercentage  
 ,projects.ProjectId  
 ,projects.ProjectName  
 ,allocationpercentage.Percentage  
   
 FROM [dbo].[ClientBillingRoles] clientBillingRoles     
 INNER JOIN [dbo].[Projects] projects ON projects.ProjectId = clientBillingRoles.ProjectId   
 INNER JOIN [dbo].[AllocationPercentage] allocationpercentage  ON    
 clientBillingRoles.ClientBillingPercentage= allocationpercentage.AllocationPercentageID  
   
 WHERE   
   clientBillingRoles.ProjectId = @ProjectId   
  
END  