﻿CREATE TYPE [dbo].[ScaleDetailsType] AS TABLE(
      [Id] [int] NOT NULL,
      [Description] [varchar](150) NOT NULL
)
GO
