﻿CREATE TABLE [dbo].[AssociateKRAMapper](
	[EmployeeID] [int] NOT NULL,
	[KRARoleCategoryID] [int] NOT NULL,
	[KRARoleID] [int] NOT NULL,
	[FinancialYearID] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_Employee] FOREIGN KEY([KRARoleID])
REFERENCES [dbo].[KRARole] ([KRARoleID])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_Employee]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_FinancialYear] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_FinancialYear]
GO

ALTER TABLE [dbo].[AssociateKRAMapper]  WITH CHECK ADD  CONSTRAINT [FK_KRAAssociateMapper_KRARoleCategory] FOREIGN KEY([KRARoleCategoryID])
REFERENCES [dbo].[RoleCategory] ([RoleCategoryID])
GO

ALTER TABLE [dbo].[AssociateKRAMapper] CHECK CONSTRAINT [FK_KRAAssociateMapper_KRARoleCategory]
GO