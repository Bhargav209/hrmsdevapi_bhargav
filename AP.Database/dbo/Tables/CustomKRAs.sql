﻿CREATE TABLE [dbo].[CustomKRAs](
	[CustomKRAID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[KRAAspectID] [int] NOT NULL,
	[KRAAspectMetric] [varchar](500) NOT NULL,
	[KRAAspectTarget] [varchar](250) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NULL,
 [FinancialYearID] INT NOT NULL DEFAULT 1, 
    CONSTRAINT [PK_CustomKRAs] PRIMARY KEY CLUSTERED 
(
	[CustomKRAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CustomKRAs] ADD  CONSTRAINT [DF_CustomKRAs_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[CustomKRAs] ADD  CONSTRAINT [DF_CustomKRAs_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[CustomKRAs] ADD  CONSTRAINT [DF_CustomKRAs_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO

ALTER TABLE [dbo].[CustomKRAs] ADD  CONSTRAINT [DF_CustomKRAs_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[CustomKRAs]  WITH CHECK ADD  CONSTRAINT [FK_CustomKRAs_KRAAspectMaster] FOREIGN KEY([KRAAspectID])
REFERENCES [dbo].[KRAAspectMaster] ([KRAAspectID])
GO

ALTER TABLE [dbo].[CustomKRAs] CHECK CONSTRAINT [FK_CustomKRAs_KRAAspectMaster]
GO


