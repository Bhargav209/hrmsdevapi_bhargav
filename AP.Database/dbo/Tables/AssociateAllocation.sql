﻿CREATE TABLE [dbo].[AssociateAllocation](
	[AssociateAllocationId] [int] IDENTITY(1,1) NOT NULL,
	[TRId] [int] NULL,
	[ProjectId] [int] NULL,
	[EmployeeId] [int] NULL,
	[RoleMasterId] [int] NULL,
	[IsActive] [bit] NULL,
	[AllocationPercentage] [decimal](18, 0) NULL,
	[InternalBillingPercentage] [decimal](18, 0) NULL,
	[IsCritical] [bit] NULL,
	[EffectiveDate] [date] NULL,
	[AllocationDate] [date] NULL,
	[CreatedBy] [varchar](200) NULL,
	[ModifiedBy] [varchar](200) NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](100) NULL,
	[ReportingManagerId] [int] NULL,
	[IsPrimary] [bit] NULL,
	[IsBillable] [bit] NULL,
	[InternalBillingRoleId] [int] NULL,
	[ClientBillingRoleId] [int] NULL,
	[ReleaseDate] [date] NULL,
	[ClientBillingPercentage] [decimal](18, 0) NULL,
 [ProgramManagerID] INT NULL, 
    [LeadID] INT NULL, 
    CONSTRAINT [PK_AssociateAllocation] PRIMARY KEY CLUSTERED 
(
	[AssociateAllocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY], 
    CONSTRAINT [FK_AssociateAllocation_ProgramManagerID] FOREIGN KEY ([ProgramManagerID]) REFERENCES [Employee]([EmployeeId]), 
    CONSTRAINT [FK_AssociateAllocation_LeadID] FOREIGN KEY ([LeadID]) REFERENCES [Employee]([EmployeeId])
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AssociateAllocation] ADD  DEFAULT ((0)) FOR [IsPrimary]
GO

ALTER TABLE [dbo].[AssociateAllocation] ADD  DEFAULT ((0)) FOR [ClientBillingPercentage]
GO

ALTER TABLE [dbo].[AssociateAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_AssociateAllocation_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateAllocation] CHECK CONSTRAINT [FK_AssociateAllocation_Employee]
GO

ALTER TABLE [dbo].[AssociateAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_AssociateAllocation_ReportingManagerID] FOREIGN KEY([ReportingManagerId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateAllocation] CHECK CONSTRAINT [FK_AssociateAllocation_ReportingManagerID]
GO

ALTER TABLE [dbo].[AssociateAllocation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAllocation_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[AssociateAllocation] CHECK CONSTRAINT [FK_AssociateAllocation_Project]
GO

ALTER TABLE [dbo].[AssociateAllocation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAllocation_RoleMaster] FOREIGN KEY([RoleMasterId])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[AssociateAllocation] CHECK CONSTRAINT [FK_AssociateAllocation_RoleMaster]
GO

ALTER TABLE [dbo].[AssociateAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_AssociateAllocation_TalentRequisition] FOREIGN KEY([TRId])
REFERENCES [dbo].[TalentRequisition] ([TRId])
GO

ALTER TABLE [dbo].[AssociateAllocation] CHECK CONSTRAINT [FK_AssociateAllocation_TalentRequisition]
GO

ALTER TABLE [dbo].[AssociateAllocation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAllocation_ClientBillingRole] FOREIGN KEY([ClientBillingRoleId])
REFERENCES [dbo].[ClientBillingRoles] ([ClientBillingRoleId])
GO

ALTER TABLE [dbo].[AssociateAllocation] CHECK CONSTRAINT [FK_AssociateAllocation_ClientBillingRole]
GO

























CREATE NONCLUSTERED INDEX [IX_AssociateAllocation_ProjectId] ON [dbo].[AssociateAllocation]
(
	[ProjectId] ASC
)
