﻿CREATE TABLE [dbo].[FamilyDetails] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]   INT           NULL,
    [Name]         VARCHAR (100) NULL,
    [Relationship] VARCHAR (50)  NULL,
    [DateofBirth]  DATE          NULL,
    [Occupation]   VARCHAR (100) NULL,
    [IsActive]     BIT           NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_FamilyDetails_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_FamilyDetails_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      CONSTRAINT [DF_FamilyDetails_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_FamilyDetails_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_FamilyDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FamilyDetails_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);