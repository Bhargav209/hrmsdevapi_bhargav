﻿CREATE TABLE [dbo].[AssociatesMemberShipsHistory](
	[ID] [int] NOT NULL,
	[EmployeeID] [int] NULL,
	[ProgramTitle] [varchar](150) NULL,
	[ValidFrom] [varchar](4) NULL,
	[Institution] [varchar](150) NULL,
	[Specialization] [varchar](150) NULL,
	[ValidUpto] [varchar](4) NULL
) ON [PRIMARY]
GO