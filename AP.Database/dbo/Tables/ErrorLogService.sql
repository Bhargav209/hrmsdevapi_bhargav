﻿CREATE TABLE [dbo].[ErrorLogService](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](50) NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[CreatedDate] [nchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
