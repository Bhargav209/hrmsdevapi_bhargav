﻿CREATE TABLE [dbo].[Grades] (
    [GradeId]      INT           IDENTITY (1, 1) NOT NULL,
    [GradeCode]    VARCHAR (100) NULL,
    [GradeName]    VARCHAR (256) NULL,
    [IsActive]     BIT           NULL,
    [CreatedUser]  VARCHAR (100) CONSTRAINT [DF_Grades_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser] VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_Grades_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME      NULL,
    [SystemInfo]   VARCHAR (50)  CONSTRAINT [DF_Grades_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK__Grades__54F87A57A8FEC25C] PRIMARY KEY CLUSTERED ([GradeId] ASC)
);

