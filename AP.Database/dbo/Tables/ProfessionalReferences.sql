﻿CREATE TABLE [dbo].[ProfessionalReferences] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]         INT           NULL,
    [Name]               VARCHAR (100) NULL,
    [Designation]        VARCHAR (100) NULL,
    [CompanyName]        VARCHAR (100) NULL,
    [CompanyAddress]     VARCHAR (MAX) NULL,
    [OfficeEmailAddress] VARCHAR (100) NULL,
    [MobileNo]           VARCHAR (20)  NULL,
    [IsActive]           BIT           NULL,
    [CreatedUser]        VARCHAR (100) CONSTRAINT [DF_ProfessionalReferences_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]       VARCHAR (100) NULL,
    [CreatedDate]        DATETIME      CONSTRAINT [DF_ProfessionalReferences_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]       DATETIME      CONSTRAINT [DF_ProfessionalReferences_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]         VARCHAR (50)  CONSTRAINT [DF_ProfessionalReferences_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_ProfessionalReferences] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ProfessionalReferences_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);



