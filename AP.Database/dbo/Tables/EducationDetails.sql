﻿CREATE TABLE [dbo].[EducationDetails] (
    [ID]                       INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]               INT           NULL,
    [EducationalQualification] VARCHAR (100) NULL,
    --[AcadamicYearFrom]         DATETIME      NULL,
    --[AcadamicYearTo]           DATETIME      NULL,
	[AcadamicCompletedYear]        DATETIME      NULL,
    [NameofInstitution]        VARCHAR (100) NULL,
    [Specialization]           VARCHAR (100) NULL,
    [ProgramType]              VARCHAR (100) NULL,
    [Grade]                    VARCHAR (10)  NULL,
    [Marks%]                   VARCHAR (10)  NULL,
    [IsActive]                 BIT           NULL,
    [CreatedUser]              VARCHAR (100) CONSTRAINT [DF_EducationDetails_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]             VARCHAR (100) NULL,
    [CreatedDate]              DATETIME      CONSTRAINT [DF_EducationDetails_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]             DATETIME      NULL,
    [SystemInfo]               VARCHAR (50)  CONSTRAINT [DF_EducationDetails_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_EducationDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_EducationDetails_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);

