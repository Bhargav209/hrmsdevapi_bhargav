﻿CREATE TABLE [dbo].[RequisitionLearningAptitudes] (
    [RequisitionLearningAptitudeId] INT          IDENTITY (1, 1) NOT NULL,
    [TRId]                          INT          NOT NULL,
    [LearningAptitudeId]            INT          NOT NULL,
    [CreatedBy]                     VARCHAR (50) NULL,
    [CreatedDate]                   DATETIME     NULL,
    [ModifiedBy]                    VARCHAR (50) NULL,
    [ModifiedDate]                  DATETIME     NULL,
    [SystemInfo]                    VARCHAR (50) NULL,
    [RoleMasterId] INT NULL, 
    CONSTRAINT [PK_RequisitionLearningAptitudes] PRIMARY KEY CLUSTERED ([RequisitionLearningAptitudeId] ASC),
    CONSTRAINT [FK_RequisitionLearningAptitudes_LearningAptitude] FOREIGN KEY ([LearningAptitudeId]) REFERENCES [dbo].[LearningAptitude] ([LearningAptitudeId]),
    CONSTRAINT [FK_RequisitionLearningAptitudes_TalentRequisition] FOREIGN KEY ([TRId]) REFERENCES [dbo].[TalentRequisition] ([TRId])
);





