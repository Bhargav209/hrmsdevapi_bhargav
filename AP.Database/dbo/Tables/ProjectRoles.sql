﻿CREATE TABLE [dbo].[ProjectRoles](
	[ProjectRoleId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[RoleMasterId] [int] NOT NULL,
	[Responsibilities] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_ProjectRoles] PRIMARY KEY CLUSTERED 
(
	[ProjectRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProjectRoles] ADD  CONSTRAINT [DF_ProjectRoles_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[ProjectRoles] ADD  CONSTRAINT [DF_ProjectRoles_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ProjectRoles] ADD  CONSTRAINT [DF_ProjectRoles_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[ProjectRoles]  WITH CHECK ADD  CONSTRAINT [FK_ProjectRoles_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[ProjectRoles] CHECK CONSTRAINT [FK_ProjectRoles_Project]
GO

ALTER TABLE [dbo].[ProjectRoles]  WITH CHECK ADD  CONSTRAINT [FK_ProjectRoles_RoleMaster] FOREIGN KEY([RoleMasterId])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[ProjectRoles] CHECK CONSTRAINT [FK_ProjectRoles_RoleMaster]
GO



