﻿CREATE TABLE [dbo].[ProjectClientBillingRole](
	[ProjectClientBillingRoleId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[ClientBillingRoleId] [int] NOT NULL,
	[NoOfPositions] [int] NULL,
	[AddendumNumber] [varchar](50) NULL,
	[ReasonForUpdate] [varchar](max) NULL,
	[CreatedUser] [varchar](100) NOT NULL DEFAULT (suser_sname()),
	[CreatedDate] [datetime] NOT NULL DEFAULT (getdate()),
	[ModifiedUser] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](100) NOT NULL DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))),
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ProjectClientBillingRoles] PRIMARY KEY CLUSTERED 
(
	[ProjectClientBillingRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


ALTER TABLE [dbo].[ProjectClientBillingRole]  WITH CHECK ADD  CONSTRAINT [FK_ProjectClientBillingRoles_ClientBillingRoles] FOREIGN KEY([ClientBillingRoleId])
REFERENCES [dbo].[ClientBillingRoles] ([ClientBillingRoleId])
GO

ALTER TABLE [dbo].[ProjectClientBillingRole] CHECK CONSTRAINT [FK_ProjectClientBillingRoles_ClientBillingRoles]
GO

ALTER TABLE [dbo].[ProjectClientBillingRole]  WITH CHECK ADD  CONSTRAINT [FK_ProjectClientBillingRoles_Projects] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[ProjectClientBillingRole] CHECK CONSTRAINT [FK_ProjectClientBillingRoles_Projects]
GO