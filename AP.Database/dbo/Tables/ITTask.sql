﻿CREATE TABLE [dbo].[ITTask] (
    [ITNotificationID] INT          IDENTITY (1, 1) NOT NULL,
    [EmployeeCode]     VARCHAR (50) NOT NULL,
    [TaskID]           INT          NOT NULL,
    [StatusId]         INT          NOT NULL,
    CONSTRAINT [PK_ITTask] PRIMARY KEY CLUSTERED ([ITNotificationID] ASC),
    CONSTRAINT [FK_ITTask_Status] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[ITTaskMaster] ([ITTaskID])
);

