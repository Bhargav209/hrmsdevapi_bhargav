﻿CREATE TABLE [dbo].[KRAMeasurementType]
(
	[KRAMeasurementTypeID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [KRAMeasurementType] NVARCHAR(70) NOT NULL
)
