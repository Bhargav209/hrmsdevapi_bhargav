﻿CREATE TABLE [dbo].[AssociatesCertificationsHistory](
	[ID] [int] NOT NULL,
	[EmployeeID] [int] NULL,
	[CertificationID] [int] NULL,
	[ValidFrom] [varchar](4) NULL,
	[Institution] [varchar](150) NULL,
	[Specialization] [varchar](150) NULL,
	[ValidUpto] [varchar](4) NULL,
	[SkillGroupID] [int] NULL
) ON [PRIMARY]
GO