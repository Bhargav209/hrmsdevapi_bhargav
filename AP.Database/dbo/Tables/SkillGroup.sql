﻿CREATE TABLE [dbo].[SkillGroup](
	[SkillGroupId] [int] IDENTITY(1,1) NOT NULL,
	[SkillGroupName] [varchar](100) NOT NULL,
	[Description] [varchar](MAX) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[CompetencyAreaId] [int] NOT NULL,
 CONSTRAINT [PK_SkillGroup] PRIMARY KEY CLUSTERED 
(
	[SkillGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_SkillGroup_SkillGroupName_CompetencyAreaId] UNIQUE NONCLUSTERED 
(
	[SkillGroupName] ASC,
	[CompetencyAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SkillGroup] ADD  CONSTRAINT [DF_SkillGroup_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[SkillGroup] ADD  CONSTRAINT [DF_SkillGroup_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[SkillGroup] ADD  CONSTRAINT [DF_SkillGroup_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[SkillGroup]  WITH CHECK ADD  CONSTRAINT [FK_SkillGroup_CompetencyArea] FOREIGN KEY([CompetencyAreaId])
REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId])
GO

ALTER TABLE [dbo].[SkillGroup] CHECK CONSTRAINT [FK_SkillGroup_CompetencyArea]
GO
