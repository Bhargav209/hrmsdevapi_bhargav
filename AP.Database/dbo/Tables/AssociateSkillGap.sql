﻿CREATE TABLE [dbo].[AssociateSkillGap] (
    [AssociateSkillGapId]        INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]                 INT           NULL,
    [ProjectSkillId]             INT           NULL,
    [CompetencyAreaId]           INT           NULL,
    [StatusId]                   INT           NULL,
    [CurrentProficiencyLevelId]  INT           NULL,
    [RequiredProficiencyLevelId] INT           NULL,
    [IsActive]                   BIT           NULL,
    [CreatedUser]                VARCHAR (100) NULL,
    [ModifiedUser]               VARCHAR (100) NULL,
    [CreatedDate]                DATETIME      NULL,
    [ModifiedDate]               DATETIME      NULL,
    [SystemInfo]                 VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([AssociateSkillGapId] ASC),
    CONSTRAINT [FK_AssociateSkillGap_CompetencyArea] FOREIGN KEY ([CompetencyAreaId]) REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId]),
    CONSTRAINT [FK_AssociateSkillGap_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AssociateSkillGap_ProficiencyLevel] FOREIGN KEY ([CurrentProficiencyLevelId]) REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId]),
    CONSTRAINT [FK_AssociateSkillGap_ProficiencyLevel1] FOREIGN KEY ([RequiredProficiencyLevelId]) REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId]),
    CONSTRAINT [FK_AssociateSkillGap_ProjectSkills] FOREIGN KEY ([ProjectSkillId]) REFERENCES [dbo].[ProjectSkills] ([ProjectSkillId]) 
);

