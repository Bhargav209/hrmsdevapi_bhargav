﻿CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeCode] [varchar](50) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Photograph] [varbinary](max) NULL,
	[AccessCardNo] [varchar](100) NULL,
	[Gender] [varchar](10) NULL,
	[GradeId] [int] NULL,
	[Designation] [int] NULL,
	[MaritalStatus] [varchar](50) NULL,
	[Qualification] [varchar](100) NULL,
	[TelephoneNo] [varchar](30) NULL,
	[MobileNo] [varchar](30) NULL,
	[WorkEmailAddress] [varchar](100) NULL,
	[PersonalEmailAddress] [varchar](100) NULL,
	[DateofBirth] [datetime] NULL,
	[JoinDate] [datetime] NULL,
	[ConfirmationDate] [datetime] NULL,
	[RelievingDate] [datetime] NULL,
	[BloogGroup] [varchar](50) NULL,
	[Nationality] [varchar](50) NULL,
	[PANNumber] [varchar](50) NULL,
	[PassportNumber] [varchar](50) NULL,
	[PassportIssuingOffice] [varchar](50) NULL,
	[PassportDateValidUpto] [varchar](50) NULL,
	[ReportingManager] [int] NULL,
	[ProgramManager] [int] NULL,
	[DepartmentId] [int] NULL,
	[SystemInfo] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[DocumentsUploadFlag] [bit] NULL,
	[CubicalNumber] [nvarchar](50) NULL,
	[AlternateMobileNo] [varchar](15) NULL,
	[StatusId] [int] NULL,
	[BGVInitiatedDate] [datetime] NULL,
	[BGVCompletionDate] [datetime] NULL,
	[BGVStatusId] [int] NULL,
	[Experience] [decimal](18, 2) NULL,
	[CompetencyGroup] [int] NULL,
	[BGVTargetDate] [datetime] NULL,
	[EmployeeTypeId] [int] NULL,
	[UserId] [int] NULL,
	[ResignationDate] [datetime] NULL,
	[BGVStatus] [varchar](50) NULL,
	[PAID] [int] NULL,
	[HRAdvisor] [varchar](100) NULL,
	[UANNumber] [varchar](50) NULL,
	[AadharNumber] [varchar](50) NULL,
	[PFNumber] [varchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
   [EmploymentStartDate] DATE NULL, 
    [CareerBreak] INT NULL, 
    [TotalExperience] [decimal](18, 2) NULL, 
	[ExperienceExcludingCareerBreak] [decimal](18, 2) NULL
    CONSTRAINT [PK_EmployeeId] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Employee] ADD  CONSTRAINT [DF_Employee_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[Employee] ADD  CONSTRAINT [DF_Employee_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[Employee] ADD  CONSTRAINT [DF_Employee_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_DepartmentId]
GO

ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_EmployeeTypeId] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_EmployeeTypeId]
GO

ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_GradeId] FOREIGN KEY([GradeId])
REFERENCES [dbo].[Grades] ([GradeId])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_GradeId]
GO

ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_ProspectiveAssociate] FOREIGN KEY([PAID])
REFERENCES [dbo].[ProspectiveAssociate] ([ID])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_ProspectiveAssociate]
GO


ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_UserId]
GO



CREATE NONCLUSTERED INDEX [IX_EMployee_UserId_IsActive]
    ON [dbo].[Employee]([UserId] ASC, [IsActive] ASC)
    INCLUDE([EmployeeId]);
GO
