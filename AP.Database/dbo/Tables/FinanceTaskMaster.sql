﻿CREATE TABLE [dbo].[FinanceTaskMaster] (
    [FinanceTaskID] INT           IDENTITY (1, 1) NOT NULL,
    [TaskName]      NVARCHAR (50) NULL,
    CONSTRAINT [PK_FinanceTaskMaster] PRIMARY KEY CLUSTERED ([FinanceTaskID] ASC)
);

