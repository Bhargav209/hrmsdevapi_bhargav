﻿CREATE TABLE [dbo].[AssociateUtilize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[RoleMasterId] [int] NULL,
	[PercentUtilization] [int] NOT NULL,
	[Billable] [bit] NULL,
	[Critical] [bit] NULL,
	[ManagerId] [int] NOT NULL,
	[PrimaryManager] [bit] NULL,
	[ActualStartDate] [datetime] NULL,
	[ActualEndDate] [datetime] NULL,
	[CreatedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AssociateUtilize] ADD  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[AssociateUtilize] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[AssociateUtilize] ADD  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[AssociateUtilize]  WITH CHECK ADD  CONSTRAINT [FK_AssociateUtilize_EmployeeId] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateUtilize] CHECK CONSTRAINT [FK_AssociateUtilize_EmployeeId]
GO


