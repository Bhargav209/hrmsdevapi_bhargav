﻿CREATE TABLE [dbo].[DepartmentType]
(
	[DepartmentTypeId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DepartmentTypeDescription] VARCHAR(50) NULL
)
