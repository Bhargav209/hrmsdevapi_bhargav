﻿CREATE TABLE [dbo].[TalentRequisition] (
    [TRId]                  INT             IDENTITY (1, 1) NOT NULL,
    [DepartmentId]          INT             NOT NULL,
    [PracticeAreaId]         INT             NULL,
    [ProjectId]             INT             NULL,
    [RequestedDate]         DATETIME        NOT NULL,
    [RequiredDate]          DATETIME        NULL,
    [TargetFulfillmentDate] DATETIME        NULL,
    [StatusId]              INT             NOT NULL,
    [ApprovedBy]            INT             NULL,
    [IsActive]              BIT             NOT NULL,
    [CreatedUser]           VARCHAR (100)   CONSTRAINT [DF_TalentRequisition_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]          VARCHAR (100)   NULL,
    [CreatedDate]           DATETIME        CONSTRAINT [DF_TalentRequisition_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]          DATETIME        NULL,
    [SystemInfo]            VARCHAR (50)    CONSTRAINT [DF_TalentRequisition_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [Remarks]               NVARCHAR (1500) NULL,
    [TRCode]                VARCHAR (20)    NULL,
    [RequisitionType]       INT             NULL,
    [RaisedBy]              INT             NULL,
    [DraftedBy]             INT             NULL,
    [ClientID] INT NULL, 
    [ProjectDuration] INT NULL, 
    CONSTRAINT [PK_TalentRequisition] PRIMARY KEY CLUSTERED ([TRId] ASC),
    CONSTRAINT [FK_TalentRequisition_Departments] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId]),
    CONSTRAINT [FK_TalentRequisition_ProjectId] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([ProjectId]),
    CONSTRAINT [FK_TalentRequisition_PracticeArea] FOREIGN KEY ([PracticeAreaId]) REFERENCES [dbo].[PracticeArea] ([PracticeAreaId]),
);










