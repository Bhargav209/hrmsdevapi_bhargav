﻿CREATE TABLE [dbo].[BehaviorArea](
	[BehaviorAreaId] [int] IDENTITY(1,1) NOT NULL,
	[BehaviorAreaDescription] [varchar](100) NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BehaviorArea] PRIMARY KEY CLUSTERED 
(
	[BehaviorAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BehaviorArea] ADD  CONSTRAINT [DF_BehaviorArea_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[BehaviorArea] ADD  CONSTRAINT [DF_BehaviorArea_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO