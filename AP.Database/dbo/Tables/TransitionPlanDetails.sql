﻿CREATE TABLE [dbo].[TransitionPlanDetails] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [TransitionId] INT           NOT NULL,
    [CategoryId]   INT           NOT NULL,
    [TaskId]       INT           NOT NULL,
    [StartDate]    DATE          NOT NULL,
    [EndDate]      DATE          NOT NULL,
    [StatusId]     INT           NOT NULL,
    [Remarks]      VARCHAR (MAX) NOT NULL,
    [CreatedBy]    VARCHAR (100) NULL,
    [CreatedDate]  DATETIME      NULL,
    [ModifiedBy]   VARCHAR (100) NULL,
    [ModifiedDate] DATETIME      NULL,
    
    CONSTRAINT [FK_TransitionPlanDetails_TransitionPlan] FOREIGN KEY ([TransitionId]) REFERENCES [dbo].[TransitionPlan] ([TransitionId])
);

