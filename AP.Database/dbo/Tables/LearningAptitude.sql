﻿CREATE TABLE [dbo].[LearningAptitude](
	[LearningAptitudeId] [int] IDENTITY(1,1) NOT NULL,
	[LearningAptitudeDescription] [varchar](250) NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_LearningAptitude] PRIMARY KEY CLUSTERED 
(
	[LearningAptitudeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LearningAptitude] ADD  CONSTRAINT [DF_LearningAptitude_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[LearningAptitude] ADD  CONSTRAINT [DF_LearningAptitude_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO