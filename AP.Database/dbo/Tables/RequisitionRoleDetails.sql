﻿CREATE TABLE [dbo].[RequisitionRoleDetails](
	[RequisitionRoleDetailID] [int] IDENTITY(1,1) NOT NULL,
	[TRId] [int] NOT NULL,
	[RoleMasterId] [int] NOT NULL,
	[NoOfBillablePositions] [int] NULL,
	[NoOfNonBillablePositions] [int] NULL,
	[RoleDescription] [nvarchar](max) NULL,
	[KeyResponsibilities] [nvarchar](max) NULL,
	[EssentialEducationQualification] [nvarchar](max) NULL,
	[DesiredEducationQualification] [nvarchar](max) NULL,
	[ProjectSpecificResponsibilities] [nvarchar](max) NULL,
	[Expertise] [nvarchar](max) NULL,
	[MinimumExperience] [int] NULL,
	[MaximumExperience] [int] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[TAPositions] [int] NULL,
 CONSTRAINT [PK_RequistiionRoleDetails] PRIMARY KEY CLUSTERED 
(
	[RequisitionRoleDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[RequisitionRoleDetails] ADD  CONSTRAINT [DF_RequistiionRoleDetails_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[RequisitionRoleDetails] ADD  CONSTRAINT [DF_RequistiionRoleDetails_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[RequisitionRoleDetails] ADD  CONSTRAINT [DF_RequisitionRoleDetails_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[RequisitionRoleDetails]  WITH CHECK ADD  CONSTRAINT [FK_RequisitionRoleDetails_RoleMaster] FOREIGN KEY([RoleMasterId])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[RequisitionRoleDetails] CHECK CONSTRAINT [FK_RequisitionRoleDetails_RoleMaster]
GO

ALTER TABLE [dbo].[RequisitionRoleDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_RequisitionRoleDetails_TalentRequisition] FOREIGN KEY([TRId])
REFERENCES [dbo].[TalentRequisition] ([TRId])
GO

ALTER TABLE [dbo].[RequisitionRoleDetails] CHECK CONSTRAINT [FK_RequisitionRoleDetails_TalentRequisition]
GO


