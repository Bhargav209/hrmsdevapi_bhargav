﻿CREATE TABLE [dbo].[Clients](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[ClientCode] [varchar](50) NULL,
	[ClientName] [varchar](256) NULL,
	[ClientShortName] [varchar](100) NOT NULL,
	[ClientLegalName] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[ClientShortNameHash] [nvarchar](450) NULL,
 CONSTRAINT [PK__Clients] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_ClientShortNameHash] UNIQUE NONCLUSTERED 
(
	[ClientShortNameHash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO