﻿CREATE TABLE [dbo].[KRAStatus]
(
	[KRAStatusId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [KRAGroupId] INT NOT NULL, 
    [FinancialYearId] INT NOT NULL, 
    [StatusId] INT NOT NULL, 
    CONSTRAINT [FK_KRAStatus_KRACombination] FOREIGN KEY ([KRAGroupId]) REFERENCES [KRAGroup]([KRAGroupId]), 
    CONSTRAINT [FK_KRAStatus_FinancialYear] FOREIGN KEY ([FinancialYearId]) REFERENCES [FinancialYear]([ID])
)
