﻿CREATE TABLE [dbo].[TalentRequisitionEmployeeTag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TalentRequisitionID] [int] NULL,
	[EmployeeID] [int] NULL,
	[RoleMasterID] [int] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[IsUntagged] [bit] NOT NULL,
 CONSTRAINT [PK_TalentRequisitionEmployeeTag] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] ADD  CONSTRAINT [DF_TalentRequisitionEmployeeTag_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] ADD  CONSTRAINT [DF_TalentRequisitionEmployeeTag_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] ADD  CONSTRAINT [DF_TalentRequisitionEmployeeTag_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] ADD  CONSTRAINT [DF_TalentRequisitionEmployeeTag_IsUntagged]  DEFAULT ((0)) FOR [IsUntagged]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag]  WITH CHECK ADD  CONSTRAINT [FK_TalentRequisitionEmployeeTag_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] CHECK CONSTRAINT [FK_TalentRequisitionEmployeeTag_Employee]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag]  WITH CHECK ADD  CONSTRAINT [FK_TalentRequisitionEmployeeTag_RoleMaster] FOREIGN KEY([RoleMasterID])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] CHECK CONSTRAINT [FK_TalentRequisitionEmployeeTag_RoleMaster]
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag]  WITH CHECK ADD  CONSTRAINT [FK_TalentRequisitionEmployeeTag_TalentRequisition] FOREIGN KEY([TalentRequisitionID])
REFERENCES [dbo].[TalentRequisition] ([TRId])
GO

ALTER TABLE [dbo].[TalentRequisitionEmployeeTag] CHECK CONSTRAINT [FK_TalentRequisitionEmployeeTag_TalentRequisition]
GO