﻿CREATE TABLE [dbo].[RoleMaster](
	[RoleMasterID] [int] IDENTITY(1,1) NOT NULL,
	[SGRoleID] [int] NOT NULL,
	[PrefixID] [int] NULL,
	[SuffixID] [int] NULL,
	[DepartmentID] [int] NULL,
	[RoleDescription] [nvarchar](500) NULL,
	[KeyResponsibilities] [nvarchar](max) NULL,
	[EducationQualification] [nvarchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](150) NULL,
	[ModifiedUser] [varchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 [KRAGroupID] INT NULL, 
    CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED 
(
	[RoleMasterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[RoleMaster] ADD  CONSTRAINT [DF_RoleMaster_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[RoleMaster]  WITH CHECK ADD  CONSTRAINT [FK_RoleMaster_Department] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO

ALTER TABLE [dbo].[RoleMaster] CHECK CONSTRAINT [FK_RoleMaster_Department]
GO

ALTER TABLE [dbo].[RoleMaster]  WITH CHECK ADD  CONSTRAINT [FK_RoleMaster_SGRolePrefix] FOREIGN KEY([PrefixID])
REFERENCES [dbo].[SGRolePrefix] ([PrefixID])
GO

ALTER TABLE [dbo].[RoleMaster] CHECK CONSTRAINT [FK_RoleMaster_SGRolePrefix]
GO

ALTER TABLE [dbo].[RoleMaster]  WITH CHECK ADD  CONSTRAINT [FK_RoleMaster_SGRoles] FOREIGN KEY([SGRoleID])
REFERENCES [dbo].[SGRole] ([SGRoleID])
GO

ALTER TABLE [dbo].[RoleMaster] CHECK CONSTRAINT [FK_RoleMaster_SGRoles]
GO

ALTER TABLE [dbo].[RoleMaster]  WITH CHECK ADD  CONSTRAINT [FK_RoleMaster_SGRoleSuffix] FOREIGN KEY([SuffixID])
REFERENCES [dbo].[SGRoleSuffix] ([SuffixID])
GO

ALTER TABLE [dbo].[RoleMaster]  WITH CHECK ADD  CONSTRAINT [FK_RoleMaster_KRAGroup] FOREIGN KEY([KRAGroupID])
REFERENCES [dbo].[KRAGroup] ([KRAGroupID])
GO


ALTER TABLE [dbo].[RoleMaster] CHECK CONSTRAINT [FK_RoleMaster_SGRoleSuffix]
GO

CREATE NONCLUSTERED INDEX [IX_RoleMaster_DepartmentId] ON [dbo].[RoleMaster]
(
	[DepartmentID] ASC
)
