﻿CREATE TABLE [dbo].[AwardDepartmentMapper]
(
	[Id] INT IDENTITY(1,1) NOT NULL,
	[AwardId] INT NOT NULL,
	[AwardTypeId] INT  NOT NULL,
	[DepartmentId] INT  NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL ,
	[CreatedDate] [datetime] NOT NULL ,
	[ModifiedBy] [varchar](100) NULL ,
	[ModifiedDate] [datetime] NULL ,	
	[SystemInfo] [varchar](50) NOT NULL , 
    CONSTRAINT [PK_AwardDepartmentMapper] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_AwardDepartmentMapper_Award] FOREIGN KEY ([AwardId]) REFERENCES [Award]([AwardId]),
	CONSTRAINT [FK_AwardDepartmentMapper_AwardType] FOREIGN KEY ([AwardTypeId]) REFERENCES [AwardType]([AwardTypeId]),
	CONSTRAINT [FK_AwardDepartmentMapper_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [Departments]([DepartmentId]),
	
)
Go
ALTER TABLE [dbo].[AwardDepartmentMapper] ADD  CONSTRAINT [DF_AwardDepartmentMapper_CreatedDate]  DEFAULT (getdate()) FOR CreatedDate
GO

ALTER TABLE [dbo].[AwardDepartmentMapper] ADD  CONSTRAINT [DF_AwardDepartmentMapper_CreatedBy]  DEFAULT (suser_sname()) FOR CreatedBy
GO

ALTER TABLE [dbo].[AwardDepartmentMapper] ADD  CONSTRAINT [DF_AwardDepartmentMapper_ModifiedDate]  DEFAULT (getdate()) FOR ModifiedDate
GO

ALTER TABLE [dbo].[AwardDepartmentMapper] ADD  CONSTRAINT [DF_AwardDepartmentMapper_ModifiedBy]  DEFAULT (suser_sname()) FOR ModifiedBy
GO

ALTER TABLE [dbo].[AwardDepartmentMapper] ADD  CONSTRAINT [DF_AwardDepartmentMapper_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO



