﻿CREATE TABLE [dbo].[TransitionPlan](
	[TransitionId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[RoleMasterId] [int] NOT NULL,
 CONSTRAINT [PK_TransitionPlan_1] PRIMARY KEY CLUSTERED 
(
	[TransitionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TransitionPlan]  WITH CHECK ADD  CONSTRAINT [FK_TransitionPlan_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[TransitionPlan] CHECK CONSTRAINT [FK_TransitionPlan_Employee]
GO

ALTER TABLE [dbo].[TransitionPlan]  WITH CHECK ADD  CONSTRAINT [FK_TransitionPlan_RoleMaster] FOREIGN KEY([RoleMasterId])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[TransitionPlan] CHECK CONSTRAINT [FK_TransitionPlan_RoleMaster]
GO
