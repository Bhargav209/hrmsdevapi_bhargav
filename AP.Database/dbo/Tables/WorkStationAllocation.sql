﻿Create table [dbo].[WorkStationAllocation](
	   [Id] int not null Identity(1,1)
	  ,[WorkstationId] int not null
	  ,[EmployeeId] INT NULL
	  ,[CreatedDate] datetime not null DEFAULT (getdate())
      ,[CreatedUser] varchar(100)not null DEFAULT (suser_sname())
      ,[ModifiedDate] datetime null
      ,[ModifiedUser] varchar(100)
      ,[SystemInfo] varchar(50)  NOT null DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))),    
    
    CONSTRAINT [FK_WorkStationAllocation_WorkstationId] FOREIGN KEY(WorkstationId) REFERENCES Workstation(Id),
	   CONSTRAINT [PK_WorkStationAllocation] PRIMARY KEY ([Id]),
	   CONSTRAINT FK_WorkStationAllocation_EmployeeId FOREIGN KEY (EmployeeId) REFERENCES EMPLOYEE(EmployeeId)
	)
