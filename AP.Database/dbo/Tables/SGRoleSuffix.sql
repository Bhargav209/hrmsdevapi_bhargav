﻿CREATE TABLE [dbo].[SGRoleSuffix](
	[SuffixID] [int] IDENTITY(1,1) NOT NULL,
	[SuffixName] [varchar](50) NOT NULL,
	[CreatedBy] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_SGRoleSuffix] PRIMARY KEY CLUSTERED 
(
	[SuffixID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SGRoleSuffix] ADD  CONSTRAINT [DF_SGRoleSuffix_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO