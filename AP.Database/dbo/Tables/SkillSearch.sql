﻿CREATE TABLE [dbo].[SkillSearch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NULL,
	[FirstName] [varchar](150) NULL,
	[LastName] [varchar](150) NULL,
	[Experience] [decimal](5, 2) NULL,
	[RoleMasterId] [int] NULL,
	[RoleDescription] [varchar](50) NULL,
	[DesignationID] [int] NULL,
	[DesignationCode] [varchar](50) NULL,
	[ProjectCode] [varchar](15) NULL,
	[ProjectName] [varchar](150) NULL,
	[IsPrimary] [bit] NULL,
	[IsCritical] [bit] NULL,
	[IsBillable] [bit] NULL,
	[CompetencyAreaID] [int] NULL,
	[CompetencyAreaCode] [varchar](150) NULL,
	[SkillIGroupID] [int] NULL,
	[SkillGroupName] [varchar](150) NULL,
	[SkillID] [int] NULL,
	[SkillName] [varchar](150) NULL,
	[ProficiencyLevelID] [int] NULL,
	[ProficiencyLevelCode] [varchar](150) NULL,
	[EmployeeCode] [varchar](10) NULL,
	[ProjectId] [int] NULL,
	[IsSkillPrimary] [bit] DEFAULT 0,
	[DesignationName] [varchar](150) NULL,
 CONSTRAINT [PK_SkillSearch] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SkillSearch]  WITH CHECK ADD FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

