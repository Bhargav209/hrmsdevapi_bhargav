﻿CREATE TABLE [dbo].[ServiceDepartmentRoles](
	[ServiceDepartmentRoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleMasterID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[IsActive] BIT NOT NULL,
	[CreatedUser] [varchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedUser] [varchar](150) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 [DepartmentID] INT NOT NULL, 
    CONSTRAINT [PK_ServiceDepartmentRoles] PRIMARY KEY CLUSTERED 
(
	[ServiceDepartmentRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ServiceDepartmentRoles] ADD  CONSTRAINT [DF_ServiceDepartmentRoles_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[ServiceDepartmentRoles] ADD  CONSTRAINT [DF_ServiceDepartmentRoles_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ServiceDepartmentRoles] ADD  CONSTRAINT [DF_ServiceDepartmentRoles_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[ServiceDepartmentRoles] ADD  CONSTRAINT [DF_ServiceDepartmentRoles_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO



