﻿CREATE TABLE [dbo].[AspectMaster]
(
	[AspectId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AspectName] VARCHAR(70) NOT NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT (getdate()), 
    [CreatedUser] VARCHAR(100) NOT NULL DEFAULT (suser_sname()), 
    [ModifiedDate] DATETIME NULL DEFAULT (getdate()), 
    [ModifiedUser] VARCHAR(100) NULL DEFAULT (suser_sname()), 
    [SystemInfo] VARCHAR(50) NOT NULL DEFAULT (CONVERT([char](15),connectionproperty('client_net_address')))
)
