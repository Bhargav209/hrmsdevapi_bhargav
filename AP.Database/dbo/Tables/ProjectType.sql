﻿CREATE TABLE [dbo].[ProjectType] (
    [ProjectTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [ProjectTypeCode] VARCHAR (100) NULL,
    [IsActive]        BIT           NULL,
    [CreatedUser]     VARCHAR (100) CONSTRAINT [DF_ProjectType_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]    VARCHAR (100) NULL,
    [CreatedDate]     DATETIME      CONSTRAINT [DF_ProjectType_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]    DATETIME      NULL,
    [SystemInfo]      VARCHAR (50)  CONSTRAINT [DF_ProjectType_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [Description]     VARCHAR (265) NULL,
    PRIMARY KEY CLUSTERED ([ProjectTypeId] ASC)
);

