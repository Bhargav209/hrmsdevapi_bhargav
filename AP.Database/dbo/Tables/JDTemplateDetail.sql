﻿CREATE TABLE [dbo].[JDTemplateDetail]
(
	[TemplateDetailId] INT   IDENTITY(1,1) NOT NULL,
	[TemplateTitleId] INT NOT NULL, 
    [CompetencyAreaId] INT NOT NULL, 
    [SkillGroupId] INT NOT NULL, 
    [SkillId] INT NOT NULL, 
    [ProficiencyLevelId] INT NOT NULL,
	[CreatedUser]      VARCHAR (100) CONSTRAINT [DF_JDTemplateDetail_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      CONSTRAINT [DF_JDTemplateDetail_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  CONSTRAINT [DF_JDTemplateDetail_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
	PRIMARY KEY CLUSTERED ([TemplateDetailId] ASC),
	CONSTRAINT [FK_JDTemplateDetail_TemplateTitleId] FOREIGN KEY ([TemplateTitleId]) REFERENCES [dbo].[JDTemplateTitleMaster] ([TemplateTitleId]),
	CONSTRAINT [FK_JDTemplateDetail_CompetencyareaId] FOREIGN KEY ([CompetencyAreaId]) REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId]),
	CONSTRAINT [FK_JDTemplateDetail_SkillGroupId] FOREIGN KEY ([SkillGroupId]) REFERENCES [dbo].[SkillGroup] ([SkillGroupId]),
	CONSTRAINT [FK_JDTemplateDetail_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Skills] ([SkillId]),
	CONSTRAINT [FK_JDTemplateDetail_ProficiencyLevelId] FOREIGN KEY ([ProficiencyLevelId]) REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId])
)
