﻿CREATE TABLE [dbo].[EmployeeProjects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[OrganizationName] [varchar](100) NULL,
	[ProjectName] [varchar](max) NULL,
	[DomainID] [int] NULL,
	[Duration] [int] NULL,
	[RoleMasterId] [int] NULL,
	[KeyAchievements] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_EmployeeProject] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Domain] FOREIGN KEY([DomainID])
REFERENCES [dbo].[Domain] ([DomainID])
GO

ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Domain]
GO

ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Employee]
GO

ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_RoleMaster] FOREIGN KEY([RoleMasterId])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_RoleMaster]
GO

