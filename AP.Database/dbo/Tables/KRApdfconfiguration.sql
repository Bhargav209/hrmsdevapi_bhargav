﻿CREATE TABLE [dbo].[KRApdfconfiguration]
(
	[KRApdfconfigurationID] INT NOT NULL PRIMARY KEY IDENTITY,
	[Section1] VARCHAR(MAX) NULL,
	[Section2]  VARCHAR(MAX) NULL,
	CreatedBy VARCHAR(150) NOT NULL DEFAULT suser_sname(),
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE(),
    ModifiedBy VARCHAR(150)  NULL,
	ModifiedDate DATETIME  NULL,
	SystemInfo VARCHAR(50) NOT NULL DEFAULT CONVERT([char](15),connectionproperty('client_net_address')),
)
