﻿CREATE TABLE [dbo].[EmployeeSkillsHistory](
	[ID] [int] NOT NULL,
	[EmployeeId] [int] NULL,
	[CompetencyAreaId] [int] NULL,
	[SkillId] [int] NULL,
	[ProficiencyLevelId] [int] NULL,
	[Experience] [int] NULL,
	[LastUsed] [int] NULL,
	[IsPrimary] [bit] NULL,
	[SkillGroupId] [int] NULL
) ON [PRIMARY]
