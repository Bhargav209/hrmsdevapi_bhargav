﻿CREATE TABLE [dbo].[Status](
	[StatusId] [int] NOT NULL,
	[StatusCode] [nvarchar](256) NULL,
	[StatusDescription] [nvarchar](256) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[Category] [varchar](100) NULL,
	[CategoryID] [int] NULL,
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Status] ADD  CONSTRAINT [DF_Status_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[Status] ADD  CONSTRAINT [DF_Status_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Status] ADD  CONSTRAINT [DF_Status_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[Status]  WITH CHECK ADD  CONSTRAINT [FK_Status_CategoryMaster] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[CategoryMaster] ([CategoryID])
GO

ALTER TABLE [dbo].[Status] CHECK CONSTRAINT [FK_Status_CategoryMaster]
GO


