﻿CREATE TABLE [dbo].[BayInformation](
	   [BayId] int not null Identity(1,1)
	  ,[Name] varchar(100) not null
	  ,[Description] varchar(max)
	  ,[CreatedDate] datetime not null DEFAULT (getdate())
      ,[CreatedUser] varchar(100)not null DEFAULT (suser_sname())
      ,[ModifiedDate] datetime null
      ,[ModifiedUser] varchar(100)
      ,[SystemInfo] varchar(50)  NOT null DEFAULT (CONVERT([char](15),connectionproperty('client_net_address')))
	  ,CONSTRAINT [PK_BayInformation] PRIMARY KEY ([BayId])
)