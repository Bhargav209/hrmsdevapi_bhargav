﻿CREATE TABLE [dbo].[TransitionPlanCategories] (
    [CategoryId]   INT            IDENTITY (1, 1) NOT NULL,
    [CategoryName] NVARCHAR (100) NOT NULL,
    [IsActive]     BIT            CONSTRAINT [DF_TaskCategories_IsActive] DEFAULT ((1)) NULL,
    [CreatedBy]    NVARCHAR (100) NULL,
    [CreatedDate]  DATETIME       CONSTRAINT [DF_TaskCategories_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifyBy]     NVARCHAR (100) NULL,
    [ModifiedDate] DATETIME       NULL,
    CONSTRAINT [PK_TaskCategories] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);

