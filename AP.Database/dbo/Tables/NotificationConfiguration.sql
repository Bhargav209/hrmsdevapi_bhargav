﻿
CREATE TABLE [dbo].[NotificationConfiguration](
	[NotificationConfigID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationTypeID] [int] NOT NULL,
	[EmailFrom] [nvarchar](max) NULL,
	[EmailTo] [nvarchar](max) NULL,
	[EmailCC] [nvarchar](max) NULL,
	[EmailSubject] [nvarchar](150) NULL,
	[EmailContent] [nvarchar](max) NULL,
	[SLA] [int] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](100) NULL,
	[ModifiedDate] [datetime] NULL DEFAULT (getdate()),
	[SystemInfo] [nvarchar](50) NULL,
 [CategoryId] INT NOT NULL, 
    CONSTRAINT [PK_NotificationConfiguration] PRIMARY KEY CLUSTERED 
(
	[NotificationConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
