﻿CREATE TABLE [dbo].[AwardWorkFlow]
(
	[AwardWorkFlowId] INT IDENTITY(1,1) NOT NULL,
	[FinancialYearId] INT  NOT NULL,
	[StatusId] INT  NOT NULL,
	[FromEmployeeId] INT  NOT NULL,
	[ToEmployeeId] INT  NOT NULL,
	[Comments][varchar](1500) NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL ,
	[CreatedDate] [datetime] NOT NULL, 
    CONSTRAINT [PK_AwardWorkFlow] PRIMARY KEY ([AwardWorkFlowId]),
	CONSTRAINT [FK_AwardWorkFlow_FinancialYear] FOREIGN KEY ([FinancialYearId]) REFERENCES [FinancialYear]([ID]),
	CONSTRAINT [FK_AwardWorkFlow_FromEmployee] FOREIGN KEY ([FromEmployeeId]) REFERENCES [Employee]([EmployeeId]),
	CONSTRAINT [FK_AwardWorkFlow_ToEmployee] FOREIGN KEY ([ToEmployeeId]) REFERENCES [Employee]([EmployeeId]),
)
Go
ALTER TABLE [dbo].[AwardWorkFlow] ADD  CONSTRAINT [DF_AwardWorkFlow_CreatedDate]  DEFAULT (getdate()) FOR CreatedDate
GO

ALTER TABLE [dbo].[AwardWorkFlow] ADD  CONSTRAINT [DF_AwardWorkFlow_CreatedBy]  DEFAULT (suser_sname()) FOR CreatedBy
GO