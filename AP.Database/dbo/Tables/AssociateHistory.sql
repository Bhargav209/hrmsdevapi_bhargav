﻿
CREATE TABLE [dbo].[AssociateHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[DesignationId] [nvarchar](100) NULL,
	[GradeId] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_AssociateHistory_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](100) NULL,
	[Remarks] [nvarchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [nvarchar](100) NULL,
	[DepartmentId] [int] NOT NULL,
	[PracticeAreaId] [int] NULL,
	[SystemInfo] [varchar](100) NULL,
 CONSTRAINT [PK_AssociateHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
