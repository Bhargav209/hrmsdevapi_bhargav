﻿CREATE TABLE [dbo].[NotificationType](
	[NotificationTypeID] [int] NOT NULL,
	[NotificationCode] [nvarchar](50) NOT NULL,
	[NotificationDesc] [nvarchar](150) NULL,
 [CreatedUser] NVARCHAR(100) NULL, 
    [CreatedDate] DATETIME NULL DEFAULT (getdate()), 
    [ModifiedUser] NVARCHAR(100) NULL, 
    [ModifiedDate] DATETIME NULL DEFAULT (getdate()), 
    [SystemInfo] NVARCHAR(50) NULL, 
    [CategoryId] INT NOT NULL, 
    CONSTRAINT [FK_NotificationType_CategoryMaster] FOREIGN KEY ([CategoryId]) REFERENCES [CategoryMaster]([CategoryId])
) ON [PRIMARY]

GO