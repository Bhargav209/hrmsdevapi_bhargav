﻿CREATE TABLE [dbo].[AssociatesMemberShips](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[ProgramTitle] [varchar](150) NOT NULL,
	[ValidFrom] [varchar](4) NOT NULL,
	[Institution] [varchar](150) NULL,
	[Specialization] [varchar](150) NULL,
	[ValidUpto] [varchar](4) NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
    CONSTRAINT [PK_AssociatesMemberShips] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssociatesMemberShips]  WITH CHECK ADD  CONSTRAINT [FK_AssociatesMemberShips_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociatesMemberShips] CHECK CONSTRAINT [FK_AssociatesMemberShips_Employee]
GO

