﻿CREATE TABLE [dbo].[Technologies] (
    [TechnologyId]   INT           IDENTITY (1, 1) NOT NULL,
    [TechnologyCode] VARCHAR (100) NULL,
    [TechnologyName] VARCHAR (256) NULL,
    [IsActive]       BIT           NULL,
    [CreatedUser]    VARCHAR (100) CONSTRAINT [DF_Technologies_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]   VARCHAR (100) NULL,
    [CreatedDate]    DATETIME      CONSTRAINT [DF_Technologies_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]   DATETIME      NULL,
    [SystemInfo]     VARCHAR (50)  CONSTRAINT [DF_Technologies_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK__Technolo__70570158DE165F90] PRIMARY KEY CLUSTERED ([TechnologyId] ASC)
);

