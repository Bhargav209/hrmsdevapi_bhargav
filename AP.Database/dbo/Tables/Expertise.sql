﻿CREATE TABLE [dbo].[ExpertiseArea](
	[ExpertiseId] [int] IDENTITY(1,1) NOT NULL,
	[ExpertiseAreaDescription] [varchar](max) NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Expertise] PRIMARY KEY CLUSTERED 
(
	[ExpertiseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ExpertiseArea] ADD  CONSTRAINT [DF_Expertise_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ExpertiseArea] ADD  CONSTRAINT [DF_Expertise_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
