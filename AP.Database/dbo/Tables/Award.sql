﻿CREATE TABLE [dbo].[Award]
(
	[AwardId] INT IDENTITY(1,1) NOT NULL,
	[AwardTitle][varchar](50) NOT NULL,
	[AwardSubTitle] [varchar](50) NOT NULL,
	[AwardDescription] [varchar](500) NOT NULL,
	[NumberOfAwards][int]  NULL,
	[PriceAmount] INT  NULL,
	[CreatedBy] [varchar](100) NOT NULL ,
	[CreatedDate] [datetime] NOT NULL ,
	[ModifiedBy] [varchar](100) NULL ,
	[ModifiedDate] [datetime] NULL ,	
	[SystemInfo] [varchar](50) NOT NULL , 
    CONSTRAINT [PK_Award] PRIMARY KEY ([AwardId]), 
)
GO
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_CreatedDate]  DEFAULT (getdate()) FOR CreatedDate
GO

ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_CreatedBy]  DEFAULT (suser_sname()) FOR CreatedBy
GO

ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_ModifiedDate]  DEFAULT (getdate()) FOR ModifiedDate
GO

ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_ModifiedBy]  DEFAULT (suser_sname()) FOR ModifiedBy
GO

ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO
