﻿CREATE TABLE [dbo].[AssociateExitApprovalLevel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalLevel] [int] NOT NULL,
	[ApprovedBy] [nvarchar](50) NOT NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_AssociateExitApprovalLevel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

