﻿CREATE TABLE [dbo].[ClientBillingRoles](
	[ClientBillingRoleId] [int] IDENTITY(1,1) NOT NULL,
	[ClientBillingRoleCode] [varchar](50) NULL,
	[ClientBillingRoleName] [varchar](70) NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL CONSTRAINT [DF_ClientBillingRoles_CreatedUser]  DEFAULT (suser_sname()),
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_ClientBillingRoles_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL CONSTRAINT [DF_ClientBillingRoles_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))),
	[ProjectId] [int] NULL,
	[NoOfPositions] [int] NULL,
	[AddendumNumber] [varchar](50) NULL,
	[ReasonForUpdate] [varchar](500) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
 [ClientBillingPercentage] INT NULL, 
    CONSTRAINT [PK__ClientBillingRoles] PRIMARY KEY CLUSTERED 
(
	[ClientBillingRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[ClientBillingRoles]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientBillingRole_ProjectId] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[ClientBillingRoles] CHECK CONSTRAINT [FK_ClientBillingRole_ProjectId]
GO
