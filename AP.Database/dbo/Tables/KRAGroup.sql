﻿CREATE TABLE [dbo].[KRAGroup]
(
	[KRAGroupId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DepartmentId] INT NOT NULL, 
    [RoleCategoryId] INT NOT NULL, 
    [ProjectTypeId] INT NULL, 
    [KRATitle] VARCHAR(100) NOT NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT (getdate()), 
    [CreatedUser] VARCHAR(50) NULL, 
    [ModifiedDate] DATETIME NOT NULL DEFAULT (getdate()), 
    [ModifiedUser] VARCHAR(50) NULL, 
    [SystemInfo] VARCHAR(50) NULL, 
    CONSTRAINT [FK_KRACombination_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [Departments]([DepartmentId]), 
    CONSTRAINT [FK_KRACombination_ProjectType] FOREIGN KEY ([ProjectTypeId]) REFERENCES [ProjectType]([ProjectTypeId]), 
    CONSTRAINT [FK_KRACombination_RoleCategory] FOREIGN KEY ([RoleCategoryId]) REFERENCES [RoleCategory]([RoleCategoryID])
)
