﻿CREATE TABLE [dbo].[ProjectManagers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NULL,
	[ReportingManagerID] [int] NULL,
	[ProgramManagerID] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 [LeadID] INT NULL, 
    CONSTRAINT [PK_ProjectManagers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY], 
    CONSTRAINT [FK_ProjectManagers_Projects] FOREIGN KEY ([ProjectID]) REFERENCES [Projects]([ProjectId]), 
    CONSTRAINT [FK_ProjectManagers_ReportingManager] FOREIGN KEY ([ReportingManagerID]) REFERENCES [Employee]([EmployeeId]), 
    CONSTRAINT [FK_ProjectManagers_ProgramManager] FOREIGN KEY ([ProgramManagerID]) REFERENCES [Employee]([EmployeeId]), 
    CONSTRAINT [FK_ProjectManagers_Lead] FOREIGN KEY ([LeadID]) REFERENCES [Employee]([EmployeeId])
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProjectManagers] ADD  CONSTRAINT [DF_ProjectManagers_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ProjectManagers] ADD  CONSTRAINT [DF_ProjectManagers_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO


