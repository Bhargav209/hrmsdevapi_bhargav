﻿CREATE TABLE [dbo].[KRAComment](
	[KRACommentId] [int] IDENTITY(1,1) NOT NULL,
	[KRAGroupId] [int] NOT NULL,
	[FinancialYearId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Comments] [varchar](1500) NULL,
	[CommentedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_KRAComment] PRIMARY KEY CLUSTERED 
(
	[KRACommentId] ASC
)
) ON [PRIMARY]

