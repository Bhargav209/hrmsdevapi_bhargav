﻿CREATE TABLE [dbo].[TalentRequisitionWorkFlow](
	[WorkFlowID] [int] IDENTITY(1,1) NOT NULL,
	[TalentRequisitionID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[FromEmployeeID] [int] NOT NULL,
	[ToEmployeeID] [varchar](50) NULL,
	[Comments] [varchar](1500) NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TalentRequisitionWorkFlow] PRIMARY KEY CLUSTERED 
(
	[WorkFlowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TalentRequisitionWorkFlow] ADD  CONSTRAINT [DF_TalentRequisitionWorkFlow_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[TalentRequisitionWorkFlow] ADD  CONSTRAINT [DF_TalentRequisitionWorkFlow_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[TalentRequisitionWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_TalentRequisitionWorkFlow_Employee] FOREIGN KEY([FromEmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[TalentRequisitionWorkFlow] CHECK CONSTRAINT [FK_TalentRequisitionWorkFlow_Employee]
GO

ALTER TABLE [dbo].[TalentRequisitionWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_TalentRequisitionWorkFlow_TalentRequisition] FOREIGN KEY([TalentRequisitionID])
REFERENCES [dbo].[TalentRequisition] ([TRId])
GO

ALTER TABLE [dbo].[TalentRequisitionWorkFlow] CHECK CONSTRAINT [FK_TalentRequisitionWorkFlow_TalentRequisition]
GO
