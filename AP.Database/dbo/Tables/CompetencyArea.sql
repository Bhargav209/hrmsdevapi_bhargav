﻿CREATE TABLE [dbo].[CompetencyArea] (
    [CompetencyAreaId]          INT            IDENTITY (1, 1) NOT NULL,
    [CompetencyAreaCode]        NVARCHAR (100) NOT NULL,
    [CompetencyAreaDescription] NVARCHAR (256) NULL,
    [IsActive]                  BIT            NULL,
    [CreatedUser]               VARCHAR (100)  NULL,
    [CreatedDate]               DATETIME       NULL,
    [ModifiedUser]              VARCHAR (100)  NULL,
    [ModifiedDate]              DATETIME       NULL,
    [SystemInfo]                VARCHAR (50)   NULL,
    CONSTRAINT [PK_CompetencyArea] PRIMARY KEY CLUSTERED ([CompetencyAreaId] ASC)
);

