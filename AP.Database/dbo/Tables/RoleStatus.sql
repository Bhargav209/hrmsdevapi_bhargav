﻿
CREATE TABLE [dbo].[RoleStatus](
	[RoleStatusId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[StatusId] [int]NOT NULL,
	[CategoryId] [int]NOT NULL,
	[CreatedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_RoleStatus] PRIMARY KEY CLUSTERED 
(
	[RoleStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

