﻿CREATE TABLE [dbo].[KRAScaleDetails]
(
	[KRAScaleDetailID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [KRAScaleMasterID] INT NULL, 
    [KRAScale] INT NOT NULL, 
    [ScaleDescription] VARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_KRAScaleDetails_KRAScaleMaster] FOREIGN KEY ([KRAScaleMasterID]) REFERENCES [KRAScaleMaster]([KRAScaleMasterID])
)
