﻿CREATE TABLE [dbo].[NotificationTypeMaster] (
    [NotificationTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [NotificationCode]   NVARCHAR (50) NULL,
    [NotificationDesc]   NVARCHAR (50) NULL,
    CONSTRAINT [PK_NotificationTypeMaster] PRIMARY KEY CLUSTERED ([NotificationTypeID] ASC)
);

