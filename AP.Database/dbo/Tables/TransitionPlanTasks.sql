﻿CREATE TABLE [dbo].[TransitionPlanTasks] (
    [ID]                       INT            IDENTITY (1, 1) NOT NULL,
    [TransitionPlanCategoryId] INT            NOT NULL,
    [Task]                     NVARCHAR (MAX) NULL,
    [IsActive]                 BIT            CONSTRAINT [DF_TransitionPlanTasks_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]                NVARCHAR (100) NULL,
    [CreatedDate]              DATETIME       CONSTRAINT [DF_TransitionPlanTasks_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedBy]               NVARCHAR (100) NULL,
    [ModifiedDate]             DATETIME       NULL,
    CONSTRAINT [PK_TransitionPlanTasks] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TransitionPlanTasks_TransitionPlanCategories] FOREIGN KEY ([TransitionPlanCategoryId]) REFERENCES [dbo].[TransitionPlanCategories] ([CategoryId])
);


GO
CREATE NONCLUSTERED INDEX [IX_TransitionPlanTasks]
    ON [dbo].[TransitionPlanTasks]([ID] ASC);

