﻿CREATE TABLE [dbo].[ADRCycle](
	[ADRCycleID] [int] IDENTITY(1,1) NOT NULL,
	[ADRCycle] [nvarchar](50) NOT NULL,
	[IsActive] BIT NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedUser] [varchar](150) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedUser] [varchar](150) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ADRCycle] PRIMARY KEY CLUSTERED 
(
	[ADRCycleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ADRCycle] ADD  CONSTRAINT [DF_ADRCycle_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[ADRCycle] ADD  CONSTRAINT [DF_ADRCycle_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO

ALTER TABLE [dbo].[ADRCycle] ADD  CONSTRAINT [DF_ADRCycle_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

