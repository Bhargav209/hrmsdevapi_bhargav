﻿
CREATE   TABLE [dbo].[SOW](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SOWId] [varchar](50) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[SOWSignedDate] [datetime] NULL,
	[IsActive] bit NUll,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
[SOWFileName] VARCHAR(50) NULL, 
    PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SOW] ADD  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[SOW] ADD  CONSTRAINT [DF_SOW_CreatedDate]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[SOW] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[SOW] ADD  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[SOW]  WITH CHECK ADD  CONSTRAINT [FK_SOW_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[SOW] CHECK CONSTRAINT [FK_SOW_Project]
GO
