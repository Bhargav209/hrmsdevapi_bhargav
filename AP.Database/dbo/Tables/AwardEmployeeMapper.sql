﻿CREATE TABLE [dbo].[AwardEmployeeMapper]
(
	[AwardEmployeeId] INT IDENTITY(1,1) NOT NULL,
	[AwardNominationId] INT NOT NULL,
	[EmployeeId] INT NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL ,
	[CreatedDate] [datetime] NOT NULL ,
	[ModifiedBy] [varchar](100) NULL ,
	[ModifiedDate] [datetime] NULL ,	
	[SystemInfo] [varchar](50) NOT NULL, 
    CONSTRAINT [PK_AwardEmployeeMapper] PRIMARY KEY ([AwardEmployeeId]), 
	CONSTRAINT [FK_AwardEmployeeMapper_AwardNomination] FOREIGN KEY ([AwardNominationId]) REFERENCES [AwardNomination]([AwardNominationId]),
	CONSTRAINT [FK_AwardEmployeeMapper_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [Employee]([EmployeeId]),
)
Go
ALTER TABLE [dbo].[AwardEmployeeMapper] ADD  CONSTRAINT [DF_AwardEmployeeMapper_CreatedDate]  DEFAULT (getdate()) FOR CreatedDate
GO

ALTER TABLE [dbo].[AwardEmployeeMapper] ADD  CONSTRAINT [DF_AwardEmployeeMapper_CreatedBy]  DEFAULT (suser_sname()) FOR CreatedBy
GO

ALTER TABLE [dbo].[AwardEmployeeMapper] ADD  CONSTRAINT [DF_AwardEmployeeMapper_ModifiedDate]  DEFAULT (getdate()) FOR ModifiedDate
GO

ALTER TABLE [dbo].[AwardEmployeeMapper] ADD  CONSTRAINT [DF_AwardEmployeeMapper_ModifiedBy]  DEFAULT (suser_sname()) FOR ModifiedBy
GO

ALTER TABLE [dbo].[AwardEmployeeMapper] ADD  CONSTRAINT [DF_AwardEmployeeMapper_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO
