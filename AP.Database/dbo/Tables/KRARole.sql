﻿CREATE TABLE [dbo].[KRARole](
	[KRARoleID] [int] IDENTITY(1,1) NOT NULL,
	[KRARoleCategoryID] [int] NOT NULL,
	[KRARoleName] [varchar](150) NOT NULL,
	[KRARoleDescription] [varchar](max) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_KRARole] PRIMARY KEY CLUSTERED 
(
	[KRARoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[KRARole] ADD  CONSTRAINT [DF_KRARole_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[KRARole] ADD  CONSTRAINT [DF_KRARole_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[KRARole] ADD  CONSTRAINT [DF_KRARole_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO

ALTER TABLE [dbo].[KRARole] ADD  CONSTRAINT [DF_KRARole_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[KRARole] ADD  CONSTRAINT [DF_KRARole_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[KRARole]  WITH CHECK ADD  CONSTRAINT [FK_KRARole_KRARoleCategory] FOREIGN KEY([KRARoleCategoryID])
REFERENCES [dbo].[RoleCategory] ([RoleCategoryID])
GO

ALTER TABLE [dbo].[KRARole] CHECK CONSTRAINT [FK_KRARole_KRARoleCategory]
GO

