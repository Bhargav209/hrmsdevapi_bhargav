﻿CREATE TABLE [dbo].[KRAAspectMaster](
	[KRAAspectID] [int] IDENTITY(1,1) NOT NULL,
	[AspectId] INT NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
	[DepartmentId] [int] NOT NULL
 CONSTRAINT [PK_KRAAspectMaster] PRIMARY KEY CLUSTERED 
(
	[KRAAspectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[KRAAspectMaster] ADD  CONSTRAINT [DF_KRAAspectMaster_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[KRAAspectMaster] ADD  CONSTRAINT [DF_KRAAspectMaster_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[KRAAspectMaster] ADD  CONSTRAINT [DF_KRAAspectMaster_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO

ALTER TABLE [dbo].[KRAAspectMaster] ADD  CONSTRAINT [DF_KRAAspectMaster_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[KRAAspectMaster] ADD  CONSTRAINT [DF_KRAAspectMaster_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[KRAAspectMaster]  WITH CHECK ADD  CONSTRAINT [FK_KRAAspectMaster_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO


CREATE NONCLUSTERED INDEX [IX_KRAAspectMaster_AspectId] ON [dbo].[KRAAspectMaster]
(
	[AspectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO