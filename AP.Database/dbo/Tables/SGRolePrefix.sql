﻿CREATE TABLE [dbo].[SGRolePrefix](
	[PrefixID] [int] IDENTITY(1,1) NOT NULL,
	[PrefixName] [varchar](50) NOT NULL,
	[CreatedBy] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_SGRolePrefix] PRIMARY KEY CLUSTERED 
(
	[PrefixID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SGRolePrefix] ADD  CONSTRAINT [DF_SGRolePrefix_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
