﻿CREATE TABLE [dbo].[KRAWorkFlow](
	[WorkFlowID] [int] IDENTITY(1,1) NOT NULL,
	[FinancialYearID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[FromEmployeeID] [int] NOT NULL,
	[ToEmployeeID] [int] NULL,
	[DepartmentID] [int] NULL,
	[Comments] [varchar](1500) NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_KRAWorkFlow] PRIMARY KEY CLUSTERED 
(
	[WorkFlowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[KRAWorkFlow] ADD  CONSTRAINT [DF_KRAWorkFlow_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[KRAWorkFlow] ADD  CONSTRAINT [DF_KRAWorkFlow_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[KRAWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_KRAWorkFlow_Employee] FOREIGN KEY([FromEmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[KRAWorkFlow] CHECK CONSTRAINT [FK_KRAWorkFlow_Employee]
GO




