﻿
CREATE TABLE [dbo].[ProjectWorkFlow](
	[WorkFlowId] [int] IDENTITY(1,1) NOT NULL,
	[SubmittedBy] [int] NOT NULL,
	[SubmittedTo] [int] NOT NULL,
	[SubmittedDate] [datetime] NOT NULL,
	[WorkFlowStatus] [int] NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Comments] varchar(250) NULL,
 CONSTRAINT [PK_ProjectWorkFlow_1] PRIMARY KEY CLUSTERED 
(
	[WorkFlowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProjectWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_Projects_ProjectId] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[ProjectWorkFlow] CHECK CONSTRAINT [FK_Projects_ProjectId]
GO
