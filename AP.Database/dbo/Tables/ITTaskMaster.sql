﻿CREATE TABLE [dbo].[ITTaskMaster] (
    [ITTaskID] INT           IDENTITY (1, 1) NOT NULL,
    [TaskName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ITTaskMaster] PRIMARY KEY CLUSTERED ([ITTaskID] ASC)
);

