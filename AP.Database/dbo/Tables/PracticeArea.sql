﻿CREATE TABLE [dbo].[PracticeArea] (
    [PracticeAreaId]          INT            IDENTITY (1, 1) NOT NULL,
    [PracticeAreaCode]        NVARCHAR (256) NULL,
    [PracticeAreaDescription] NVARCHAR (256) NULL,
    [IsActive]                BIT            NULL,
    [CreatedUser]             VARCHAR (100)  NULL,
    [CreatedDate]             DATETIME       NULL,
    [ModifiedUser]            VARCHAR (100)  NULL,
    [ModifiedDate]            DATETIME       NULL,
    [SystemInfo]              VARCHAR (50)   NULL,
    CONSTRAINT [PK_PracticeArea] PRIMARY KEY CLUSTERED ([PracticeAreaId] ASC)
);

