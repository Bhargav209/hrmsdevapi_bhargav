﻿CREATE TABLE [dbo].[ProfessionalDetails] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]      INT           NULL,
    [ProgramTitle]    VARCHAR (100) NULL,
    [ProgramType]     VARCHAR (30)  NULL,
    [Year]            INT           NULL,
    [Institution]     VARCHAR (100) NULL,
    [Specialization]  VARCHAR (100) NULL,
    [CurrentValidity] VARCHAR (50)  NULL,
    [IsActive]        BIT           NULL,
    [CreatedUser]     VARCHAR (100) CONSTRAINT [DF_ProfessionalDetails_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]    VARCHAR (100) NULL,
    [CreatedDate]     DATETIME      CONSTRAINT [DF_ProfessionalDetails_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]    DATETIME      CONSTRAINT [DF_ProfessionalDetails_ModifiedDate] DEFAULT (getdate()) NULL,
    [SystemInfo]      VARCHAR (50)  CONSTRAINT [DF_ProfessionalDetails_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK_ProfessionalDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ProfessionalDetails_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([EmployeeId])
);



