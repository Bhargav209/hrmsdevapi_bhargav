﻿
CREATE TABLE [dbo].[AssociateResignationWorkFlow](
	[WorkFlowID] [int] IDENTITY(1,1) NOT NULL,
	[ResignationID] [int] NOT NULL,
	[FromEmployeeID] [int] NOT NULL,
	[ToEmployeeID] [int] NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AssociateResignationWorkFlow] PRIMARY KEY CLUSTERED 
(
	[WorkFlowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssociateResignationWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_AssociateResignationWorkFlow_AssociateResignation] FOREIGN KEY([ResignationID])
REFERENCES [dbo].[AssociateResignation] ([ResignationID])
GO

ALTER TABLE [dbo].[AssociateResignationWorkFlow] CHECK CONSTRAINT [FK_AssociateResignationWorkFlow_AssociateResignation]
GO

ALTER TABLE [dbo].[AssociateResignationWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_AssociateResignationWorkFlow_FromEmployeeID] FOREIGN KEY([FromEmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateResignationWorkFlow] CHECK CONSTRAINT [FK_AssociateResignationWorkFlow_FromEmployeeID]
GO




