﻿CREATE TABLE [dbo].[RoleCategory](
	[RoleCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[RoleCategoryName] [varchar](150) NOT NULL,
	[RoleCategoryDescription] [varchar](max) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RoleCategory] PRIMARY KEY CLUSTERED 
(
	[RoleCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RoleCategory] ADD  CONSTRAINT [DF_RoleCategory_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[RoleCategory] ADD  CONSTRAINT [DF_RoleCategory_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[RoleCategory] ADD  CONSTRAINT [DF_RoleCategory_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO

ALTER TABLE [dbo].[RoleCategory] ADD  CONSTRAINT [DF_RoleCategory_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[RoleCategory] ADD  CONSTRAINT [DF_RoleCategory_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO


