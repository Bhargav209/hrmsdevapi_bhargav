﻿CREATE TABLE [dbo].[ValueType](
	[ValueTypeKey] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[IsActive] [bit] NULL,
	[ValueTypeID] [nvarchar](255) NOT NULL,
	[ValueTypeName] [nvarchar](255) NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_lkValueType] PRIMARY KEY CLUSTERED 
(
	[ValueTypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UNQ_lkValueType] UNIQUE NONCLUSTERED 
(
	[ValueTypeID] ASC,
	[IsActive] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO