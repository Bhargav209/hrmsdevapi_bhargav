﻿CREATE TABLE [dbo].[KRAScaleMaster]
(
	[KRAScaleMasterID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MinimumScale] INT NOT NULL, 
    [MaximumScale] INT NOT NULL, 
    [KRAScaleTitle] NVARCHAR(50) NOT NULL
)
