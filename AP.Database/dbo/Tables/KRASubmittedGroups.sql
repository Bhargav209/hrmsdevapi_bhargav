﻿CREATE TABLE [dbo].[KRASubmittedGroup]
(
	[KRASubmittedGroupId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [WorkFlowId] INT NOT NULL, 
    [KRAGroupId] INT NOT NULL, 
    [CreatedBy] VARCHAR(100) NULL DEFAULT (suser_sname()), 
    [CreatedDate] DATETIME NULL DEFAULT (getdate())
)
