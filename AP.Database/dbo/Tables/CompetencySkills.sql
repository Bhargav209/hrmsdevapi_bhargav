﻿CREATE TABLE [dbo].[CompetencySkills](
	[CompetencySkillsId] [int] IDENTITY(1,1) NOT NULL,
	[RoleMasterID] [int] NULL,
	[CompetencyAreaId] [int] NULL,
	[SkillId] [int] NULL,
	[SkillGroupID] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[IsPrimary] [bit] NULL,
	[ProficiencyLevelId] [int] NOT NULL,
 CONSTRAINT [PK_CompetencySkills] PRIMARY KEY CLUSTERED 
(
	[CompetencySkillsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_RoleCompSkill] UNIQUE NONCLUSTERED 
(
	[RoleMasterID] ASC,
	[CompetencyAreaId] ASC,
	[SkillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CompetencySkills]  WITH CHECK ADD  CONSTRAINT [FK_CompetencySkills_CompetencyArea] FOREIGN KEY([CompetencyAreaId])
REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId])
GO

ALTER TABLE [dbo].[CompetencySkills] CHECK CONSTRAINT [FK_CompetencySkills_CompetencyArea]
GO

ALTER TABLE [dbo].[CompetencySkills]  WITH CHECK ADD  CONSTRAINT [FK_CompetencySkills_CompetencySkills] FOREIGN KEY([CompetencySkillsId])
REFERENCES [dbo].[CompetencySkills] ([CompetencySkillsId])
GO

ALTER TABLE [dbo].[CompetencySkills] CHECK CONSTRAINT [FK_CompetencySkills_CompetencySkills]
GO

ALTER TABLE [dbo].[CompetencySkills]  WITH CHECK ADD  CONSTRAINT [FK_CompetencySkills_RoleMaster] FOREIGN KEY([RoleMasterID])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[CompetencySkills] CHECK CONSTRAINT [FK_CompetencySkills_RoleMaster]
GO

ALTER TABLE [dbo].[CompetencySkills]  WITH CHECK ADD  CONSTRAINT [FK_CompetencySkills_SkillGroupID] FOREIGN KEY([SkillGroupID])
REFERENCES [dbo].[SkillGroup] ([SkillGroupId])
GO

ALTER TABLE [dbo].[CompetencySkills] CHECK CONSTRAINT [FK_CompetencySkills_SkillGroupID]
GO

ALTER TABLE [dbo].[CompetencySkills]  WITH CHECK ADD  CONSTRAINT [FK_CompetencySkills_Skills] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skills] ([SkillId])
GO

ALTER TABLE [dbo].[CompetencySkills] CHECK CONSTRAINT [FK_CompetencySkills_Skills]
GO

ALTER TABLE [dbo].[CompetencySkills]  WITH CHECK ADD  CONSTRAINT [FK_RoleCompSkill_ProficiencyLevel] FOREIGN KEY([ProficiencyLevelId])
REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId])
GO

ALTER TABLE [dbo].[CompetencySkills] CHECK CONSTRAINT [FK_RoleCompSkill_ProficiencyLevel]
GO

