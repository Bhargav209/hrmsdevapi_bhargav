﻿CREATE TABLE [dbo].[SkillsWorkFlow](
	[WorkFlowId] [int] NOT NULL,
	[SubmittedBy] [int] NOT NULL,
	[SubmittedTo] [int] NOT NULL,
	[SubmittedDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[SubmittedRequisitionId] [int] NOT NULL,
 CONSTRAINT [PK_SkillsWorkFlow_1] PRIMARY KEY CLUSTERED 
(
	[WorkFlowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SkillsWorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_SkillsWorkFlow_SubmittedRequisitionId] FOREIGN KEY([SubmittedRequisitionId])
REFERENCES [dbo].[SkillsSubmittedForApproval] ([ID])
GO

ALTER TABLE [dbo].[SkillsWorkFlow] CHECK CONSTRAINT [FK_SkillsWorkFlow_SubmittedRequisitionId]
GO