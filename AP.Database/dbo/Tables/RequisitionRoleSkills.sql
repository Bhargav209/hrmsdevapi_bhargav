﻿CREATE TABLE [dbo].[RequisitionRoleSkills](
	[RoleSkillMappingID] [int] NOT NULL,
	[CompetencyAreaID] [int] NOT NULL,
	[SkillGroupID] [int] NOT NULL,
	[SkillID] [int] NOT NULL,
	[ProficiencyLevelID] [int] NOT NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] ADD  CONSTRAINT [DF_RequisitionRoleSkills_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] ADD  CONSTRAINT [DF_RequisitionRoleSkills_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] ADD  CONSTRAINT [DF_RequisitionRoleSkills_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills]  WITH NOCHECK ADD  CONSTRAINT [FK_RequisitionRoleSkills_CompetencyArea] FOREIGN KEY([CompetencyAreaID])
REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId])
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] CHECK CONSTRAINT [FK_RequisitionRoleSkills_CompetencyArea]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills]  WITH CHECK ADD  CONSTRAINT [FK_RequisitionRoleSkills_ProficiencyLevel] FOREIGN KEY([ProficiencyLevelID])
REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId])
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] CHECK CONSTRAINT [FK_RequisitionRoleSkills_ProficiencyLevel]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills]  WITH CHECK ADD  CONSTRAINT [FK_RequisitionRoleSkills_RequisitionRoleSkillsMapping] FOREIGN KEY([RoleSkillMappingID])
REFERENCES [dbo].[RequisitionRoleSkillMapping] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] CHECK CONSTRAINT [FK_RequisitionRoleSkills_RequisitionRoleSkillsMapping]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills]  WITH CHECK ADD  CONSTRAINT [FK_RequisitionRoleSkills_SkillGroup] FOREIGN KEY([SkillGroupID])
REFERENCES [dbo].[SkillGroup] ([SkillGroupId])
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] CHECK CONSTRAINT [FK_RequisitionRoleSkills_SkillGroup]
GO

ALTER TABLE [dbo].[RequisitionRoleSkills]  WITH NOCHECK ADD  CONSTRAINT [FK_RequisitionRoleSkills_Skills] FOREIGN KEY([SkillID])
REFERENCES [dbo].[Skills] ([SkillId])
GO

ALTER TABLE [dbo].[RequisitionRoleSkills] CHECK CONSTRAINT [FK_RequisitionRoleSkills_Skills]
GO


