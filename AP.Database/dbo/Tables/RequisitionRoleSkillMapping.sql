﻿CREATE TABLE [dbo].[RequisitionRoleSkillMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRID] [int] NOT NULL,
	[RequistiionRoleDetailID] [int] NOT NULL,
 CONSTRAINT [PK_RequisitionRoleSkillMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RequisitionRoleSkillMapping]  WITH CHECK ADD  CONSTRAINT [FK_RequisitionRoleSkillMapping_RequisitionRoleDetails] FOREIGN KEY([RequistiionRoleDetailID])
REFERENCES [dbo].[RequisitionRoleDetails] ([RequisitionRoleDetailID])
GO

ALTER TABLE [dbo].[RequisitionRoleSkillMapping] CHECK CONSTRAINT [FK_RequisitionRoleSkillMapping_RequisitionRoleDetails]
GO