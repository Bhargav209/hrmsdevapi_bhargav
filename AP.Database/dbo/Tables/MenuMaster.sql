﻿CREATE TABLE [dbo].[MenuMaster](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NULL,
	[Path] [nvarchar](250) NULL,
	[DisplayOrder] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[CreatedBy] [varchar](100) NULL DEFAULT (suser_sname()),
	[ModifiedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL DEFAULT (getdate()),
	[SystemInfo] [varchar](50) NULL DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))),
	[Parameter] [nvarchar](50) NULL,
	[NodeId] [nvarchar](50) NULL,
	[Style] [nvarchar](50) NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
