﻿CREATE TABLE [dbo].[Notification] (
    [NotificationID]     INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeCode]       NVARCHAR (50) NOT NULL,
    [NotificationTypeID] INT           NULL,
    [StatusId]           INT           NOT NULL,
    [CreatedUser]        NVARCHAR (50) NULL,
    [CreatedDate]        DATETIME      NULL,
    [Remarks]            NVARCHAR (50) NULL,
    CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED ([NotificationID] ASC),
    CONSTRAINT [FK_Notification_NotificationTypeMaster] FOREIGN KEY ([NotificationTypeID]) REFERENCES [dbo].[NotificationTypeMaster] ([NotificationTypeID])
);

