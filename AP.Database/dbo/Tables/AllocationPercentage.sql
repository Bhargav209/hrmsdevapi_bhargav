﻿CREATE TABLE [dbo].[AllocationPercentage](
	[AllocationPercentageID] [int] IDENTITY(1,1) NOT NULL,
	[Percentage] [decimal](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AllocationPercentage] PRIMARY KEY CLUSTERED 
(
	[AllocationPercentageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AllocationPercentage] ADD  CONSTRAINT [DF_AllocationPercentage_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO															

ALTER TABLE [dbo].[AllocationPercentage] ADD  CONSTRAINT [DF_AllocationPercentage_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO	

ALTER TABLE [dbo].[AllocationPercentage] ADD  CONSTRAINT [DF_AllocationPercentage_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO															
															
ALTER TABLE [dbo].[AllocationPercentage] ADD  CONSTRAINT [DF_AllocationPercentage_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO															
															
ALTER TABLE [dbo].[AllocationPercentage] ADD  CONSTRAINT [DF_AllocationPercentage_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO															


