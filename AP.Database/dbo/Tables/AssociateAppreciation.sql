﻿

CREATE TABLE [dbo].[AssociateAppreciation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ADRCycleID] [int] NOT NULL,
	[FinancialYearID] [int] NOT NULL,
	[FromEmployeeID] [int] NULL,
	[ToEmployeeID] [int] NULL,
	[AppreciationTypeID] [int] NOT NULL,
	[AppreciationMessage] [varchar](max) NULL,
	[AppreciationDate] [datetime] NOT NULL,
	[DeleteReason] [varchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedUser] [varchar](150) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedUser] [varchar](150) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AssociateAppreciation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssociateAppreciation] ADD  CONSTRAINT [DF_AssociateAppreciation_AppreciationDate]  DEFAULT (getdate()) FOR [AppreciationDate]
GO

ALTER TABLE [dbo].[AssociateAppreciation] ADD  CONSTRAINT [DF_AssociateAppreciation_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[AssociateAppreciation] ADD  CONSTRAINT [DF_AssociateAppreciation_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO

ALTER TABLE [dbo].[AssociateAppreciation] ADD  CONSTRAINT [DF_AssociateAppreciation_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[AssociateAppreciation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAppreciation_ADRCycle] FOREIGN KEY([ADRCycleID])
REFERENCES [dbo].[ADRCycle] ([ADRCycleID])
GO

ALTER TABLE [dbo].[AssociateAppreciation] CHECK CONSTRAINT [FK_AssociateAppreciation_ADRCycle]
GO

ALTER TABLE [dbo].[AssociateAppreciation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAppreciation_AppreciationType] FOREIGN KEY([AppreciationTypeID])
REFERENCES [dbo].[AppreciationType] ([ID])
GO

ALTER TABLE [dbo].[AssociateAppreciation] CHECK CONSTRAINT [FK_AssociateAppreciation_AppreciationType]
GO

ALTER TABLE [dbo].[AssociateAppreciation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAppreciation_FinancialYear] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[AssociateAppreciation] CHECK CONSTRAINT [FK_AssociateAppreciation_FinancialYear]
GO

ALTER TABLE [dbo].[AssociateAppreciation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAppreciation_FromEmployee] FOREIGN KEY([FromEmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateAppreciation] CHECK CONSTRAINT [FK_AssociateAppreciation_FromEmployee]
GO

ALTER TABLE [dbo].[AssociateAppreciation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateAppreciation_ToEmployee] FOREIGN KEY([ToEmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateAppreciation] CHECK CONSTRAINT [FK_AssociateAppreciation_ToEmployee]
GO

