﻿CREATE TABLE [dbo].[ProspectiveAssociate] (
    [ID]                   INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]            VARCHAR (100) NULL,
    [MiddleName]           VARCHAR (100) NULL,
    [LastName]             VARCHAR (100) NULL,
    [Gender]               VARCHAR (10)  NULL,
    [DesignationId]        INT           NULL,
    [GradeId]              INT           NULL,
    [Technology]           VARCHAR (100) NULL,
    [DepartmentId]         INT           NULL,
    [HRAdvisorName]        VARCHAR (100) NULL,
    [JoiningStatusId]      INT           NULL,
    [JoinDate]             DATETIME      NULL,
    [IsActive]             BIT           NULL,
    [CreatedUser]          VARCHAR (100) CONSTRAINT [DF_ProspectiveAssociate_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]         VARCHAR (100) NULL,
    [CreatedDate]          DATETIME      CONSTRAINT [DF_ProspectiveAssociate_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]         DATETIME      NULL,
    [SystemInfo]           VARCHAR (50)  CONSTRAINT [DF_ProspectiveAssociate_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [EmploymentType]       VARCHAR (100) NULL,
    [MaritalStatus]        VARCHAR (50)  NULL,
    [BGVStatusId]          INT           NULL,
    [TechnologyID]         INT           NULL,
    [EmployeeID]           INT           NULL,
    [RecruitedBy]          VARCHAR (100) NULL,
    [StatusID]             INT           NULL,
    [ReasonForDropOut]     VARCHAR (150) NULL,
    [ManagerId]            INT           NULL,
    [PersonalEmailAddress] VARCHAR (50)  NULL,
    [MobileNo]             VARCHAR (30)  NULL,
    CONSTRAINT [PK_ProspectiveAssociate] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId]),
    CONSTRAINT [FK_DesignationId] FOREIGN KEY ([DesignationId]) REFERENCES [dbo].[Designations] ([DesignationId]),
    CONSTRAINT [FK_GradeID] FOREIGN KEY ([GradeId]) REFERENCES [dbo].[Grades] ([GradeId])
);





