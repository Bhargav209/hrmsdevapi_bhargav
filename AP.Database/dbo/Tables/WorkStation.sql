﻿Create table [dbo].[WorkStation](
	   [Id] int not null Identity(1,1)
	  ,[BayId] int not null
	  ,[CreatedDate] datetime not null DEFAULT (getdate())
      ,[CreatedUser] varchar(100)not null DEFAULT (suser_sname())
      ,[ModifiedDate] datetime null
      ,[ModifiedUser] varchar(100)
      ,[SystemInfo] varchar(50)  NOT null DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))),
	   [WorkstationId] INT NOT NULL, 
    [IsOccupied] BIT NULL,     
    [WorkStationSuffix] NCHAR(10) NULL, 
    CONSTRAINT [FK_WorkStation_BayId] FOREIGN KEY(BayId) REFERENCES BayInformation(BayId),
	   CONSTRAINT [PK_WorkStation] PRIMARY KEY ([Id])
	)
