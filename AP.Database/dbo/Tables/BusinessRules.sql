﻿CREATE TABLE [dbo].[BusinessRules] (
    [BusinessRuleId]    INT           IDENTITY (1, 1) NOT NULL,
    [BusinessRuleCode]  VARCHAR (50)  NOT NULL,
    [Description]       VARCHAR (500) NOT NULL,
    [IsActive]          BIT           NOT NULL,
    [BusinessRuleValue] VARCHAR (50)  NULL,
    [CreatedBy]         VARCHAR (100) NOT NULL,
    [CreatedDate]       DATETIME      NOT NULL,
    [ModifiedBy]        VARCHAR (100) NULL,
    [ModifiedDate]      DATETIME      NULL,
    [SystemInfo]        VARCHAR (50)  NULL,
    CONSTRAINT [BusinessRules_pk] PRIMARY KEY CLUSTERED ([BusinessRuleId] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [businessrule_uk_idx]
    ON [dbo].[BusinessRules]([BusinessRuleCode] ASC);

