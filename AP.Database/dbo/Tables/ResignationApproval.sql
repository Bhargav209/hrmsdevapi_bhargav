﻿CREATE TABLE [dbo].[ResignationApproval] (
    [ID]                        INT           IDENTITY (1, 1) NOT NULL,
    [ResignationID]             INT           NULL,
    [ApprovedByID]              INT           NULL,
    [ApprovedDate]              DATE          NULL,
    [Remarks]                   VARCHAR (MAX) NULL,
    [DueRemarks]                VARCHAR (MAX) NULL,
    [NoDueRemarks]              VARCHAR (MAX) NULL,
    [ResignationRecommendation] VARCHAR (100) NULL,
    [CreatedBy]                 VARCHAR (100) NULL,
    [CreatedDate]               DATETIME      NULL,
    [ModifiedBy]                VARCHAR (100) NULL,
    [ModifiedDate]              DATETIME      NULL,
    CONSTRAINT [PK_ResignationApproval_1] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ResignationApproval_AssociateResignation] FOREIGN KEY ([ResignationID]) REFERENCES [dbo].[AssociateResignation] ([ResignationID])
);

