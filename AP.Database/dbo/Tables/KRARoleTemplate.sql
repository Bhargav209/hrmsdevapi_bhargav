﻿CREATE TABLE [dbo].[KRARoleTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleMasterID] [int] NOT NULL,
	[FinancialYearID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[KRAAspectID] [int] NOT NULL,
	[KRAAspectMetric] [varchar](500) NOT NULL,
	[KRAAspectTarget] [varchar](250) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedUser] [varchar](100) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedUser] [varchar](100) NULL,
	[SystemInfo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_KRARoleTepmlate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO 

ALTER TABLE [dbo].[KRARoleTemplate]  WITH CHECK ADD  CONSTRAINT [FK_KRARoleTepmlate_KRAAspectMaster] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[KRARoleTemplate] CHECK CONSTRAINT [FK_KRARoleTepmlate_KRAAspectMaster]
GO

ALTER TABLE [dbo].[KRARoleTemplate]  WITH CHECK ADD  CONSTRAINT [FK_KRARoleTepmlate_RoleMaster] FOREIGN KEY([RoleMasterID])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[KRARoleTemplate] CHECK CONSTRAINT [FK_KRARoleTepmlate_RoleMaster]
GO


