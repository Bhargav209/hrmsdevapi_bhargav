﻿CREATE TABLE [dbo].[ADRAssociateKRAReview](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[KRASetID] [int] NOT NULL,
	[ADRCycleID] [int] NOT NULL,
	[FinancialYearID] [int] NOT NULL,
	[CriticalTasksPerformed] [varchar](max) NULL,
	[PerformanceAchievement] [varchar](max) NULL,
	[PerformanceAchievementAnnual] [varchar](max) NULL,
	[KRAFulfillmentAnnualID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [varchar](150) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](150) NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_ADRAssocaiteKRAAspect] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] ADD  CONSTRAINT [DF_ADRAssociateKRAReview_CreatedDate1]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] ADD  CONSTRAINT [DF_ADRAssociateKRAReview_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] ADD  CONSTRAINT [DF_ADRAssociateKRAAspect_CreatedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] ADD  CONSTRAINT [DF_ADRAssociateKRAReview_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] ADD  CONSTRAINT [DF_ADRAssociateKRAAspect_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssociateKRAReview_ADRCycle] FOREIGN KEY([ADRCycleID])
REFERENCES [dbo].[ADRCycle] ([ADRCycleID])
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] CHECK CONSTRAINT [FK_ADRAssociateKRAReview_ADRCycle]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssociateKRAReview_ADRKRAFulfillement] FOREIGN KEY([KRAFulfillmentAnnualID])
REFERENCES [dbo].[ADRKRAFulfillement] ([ID])
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] CHECK CONSTRAINT [FK_ADRAssociateKRAReview_ADRKRAFulfillement]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssociateKRAReview_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] CHECK CONSTRAINT [FK_ADRAssociateKRAReview_Employee]
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssociateKRAReview_FinancialYear] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[ADRAssociateKRAReview] CHECK CONSTRAINT [FK_ADRAssociateKRAReview_FinancialYear]
GO

GO

