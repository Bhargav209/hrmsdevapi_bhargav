﻿CREATE TABLE [dbo].[AdminTaskMaster] (
    [AdminTaskID] INT           IDENTITY (1, 1) NOT NULL,
    [TaskName]    NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AdminTaskMaster] PRIMARY KEY CLUSTERED ([AdminTaskID] ASC)
);

