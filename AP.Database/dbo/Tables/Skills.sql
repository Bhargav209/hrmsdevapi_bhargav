﻿CREATE TABLE [dbo].[Skills](
	[SkillId] [int] IDENTITY(1,1) NOT NULL,
	[SkillCode] [varchar](100) NULL,
	[SkillName] [varchar](256) NULL,
	[SkillDescription] [varchar](max) NULL,
	[CompetencyAreaId] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[IsApproved] [bit] NULL,
	[SkillGroupId] [int] NULL,
 CONSTRAINT [PK__Skills__DFA09187D25470AE] PRIMARY KEY CLUSTERED 
(
	[SkillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Skills] ADD  CONSTRAINT [DF_Skills_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[Skills] ADD  CONSTRAINT [DF_Skills_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[Skills] ADD  CONSTRAINT [DF_Skills_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Skills] ADD  CONSTRAINT [DF_Skills_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[Skills] ADD  CONSTRAINT [DF_Skills_IsApproved]  DEFAULT ((0)) FOR [IsApproved]
GO

ALTER TABLE [dbo].[Skills]  WITH CHECK ADD  CONSTRAINT [FK_Skills_CompetencyArea] FOREIGN KEY([CompetencyAreaId])
REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId])
GO

ALTER TABLE [dbo].[Skills] CHECK CONSTRAINT [FK_Skills_CompetencyArea]
GO

ALTER TABLE [dbo].[Skills]  WITH CHECK ADD  CONSTRAINT [FK_Skills_SkillGroup] FOREIGN KEY([SkillGroupId])
REFERENCES [dbo].[SkillGroup] ([SkillGroupId])
GO

ALTER TABLE [dbo].[Skills] CHECK CONSTRAINT [FK_Skills_SkillGroup]
GO