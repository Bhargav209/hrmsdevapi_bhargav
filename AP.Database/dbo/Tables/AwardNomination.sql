﻿CREATE TABLE [dbo].[AwardNomination]
(
	[AwardNominationId] INT IDENTITY(1,1) NOT NULL,
	[AwardId] INT NOT NULL,
	[AwardTypeId] INT  NOT NULL,
	[ADRCycleId] INT NOT NULL,
	[FinancialYearId] INT  NOT NULL,
	[NominationDescription] [varchar](100) NOT NULL ,
	[FromEmployeeId] INT NOT NULL ,
	[NomineeComments] [varchar](500) NOT NULL ,
	[ReviewerComments] [varchar](500) NULL ,
	[DepartmentId] INT NOT NULL,
	[ProjectId] INT NULL,
	[StatusId] INT  NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL ,
	[CreatedDate] [datetime] NOT NULL ,
	[ModifiedBy] [varchar](100) NULL ,
	[ModifiedDate] [datetime] NULL ,	
	[SystemInfo] [varchar](50) NOT NULL, 
    CONSTRAINT [PK_AwardNomination] PRIMARY KEY ([AwardNominationId]),
	CONSTRAINT [FK_AwardNomination_Award] FOREIGN KEY ([AwardId]) REFERENCES [Award]([AwardId]),
	CONSTRAINT [FK_AwardNomination_AwardType] FOREIGN KEY ([AwardTypeId]) REFERENCES [AwardType]([AwardTypeId]),
	CONSTRAINT [FK_AwardNomination_ADRCycle] FOREIGN KEY ([ADRCycleId]) REFERENCES [ADRCycle]([ADRCycleID]),
	CONSTRAINT [FK_AwardNomination_FinancialYear] FOREIGN KEY ([FinancialYearId]) REFERENCES [FinancialYear]([ID]),
	CONSTRAINT [FK_AwardNomination_Employee] FOREIGN KEY ([FromEmployeeId]) REFERENCES [Employee]([EmployeeId]),
	CONSTRAINT [FK_AwardNomination_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [Departments]([DepartmentId]),
	CONSTRAINT [FK_AwardNomination_Project] FOREIGN KEY ([ProjectId]) REFERENCES [Projects]([ProjectId]) 
)
Go
ALTER TABLE [dbo].[AwardNomination] ADD  CONSTRAINT [DF_AwardNomination_CreatedDate]  DEFAULT (getdate()) FOR CreatedDate
GO

ALTER TABLE [dbo].[AwardNomination] ADD  CONSTRAINT [DF_AwardNomination_CreatedBy]  DEFAULT (suser_sname()) FOR CreatedBy
GO

ALTER TABLE [dbo].[AwardNomination] ADD  CONSTRAINT [DF_AwardNomination_ModifiedDate]  DEFAULT (getdate()) FOR ModifiedDate
GO

ALTER TABLE [dbo].[AwardNomination] ADD  CONSTRAINT [DF_AwardNomination_ModifiedBy]  DEFAULT (suser_sname()) FOR ModifiedBy
GO

ALTER TABLE [dbo].[AwardNomination] ADD  CONSTRAINT [DF_AwardNomination_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

