﻿CREATE TABLE [dbo].[AdminTask] (
    [AdminNotificationID] INT          IDENTITY (1, 1) NOT NULL,
    [EmployeeCode]        VARCHAR (50) NOT NULL,
    [TaskID]              INT          NOT NULL,
    [StatusId]            INT          NOT NULL,
    CONSTRAINT [PK_AdminTask] PRIMARY KEY CLUSTERED ([AdminNotificationID] ASC),
    CONSTRAINT [FK_AdminTask_Status] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[AdminTaskMaster] ([AdminTaskID])
);

