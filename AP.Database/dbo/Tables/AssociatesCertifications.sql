﻿CREATE TABLE [dbo].[AssociatesCertifications](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[CertificationID] [int] NOT NULL,
	[ValidFrom] [varchar](4) NOT NULL,
	[Institution] [varchar](150) NULL,
	[Specialization] [varchar](150) NULL,
	[ValidUpto] [varchar](4) NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
 [SkillGroupID] INT NOT NULL, 
    CONSTRAINT [PK_AssociatesCertifications] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssociatesCertifications]  WITH CHECK ADD  CONSTRAINT [FK_AssociatesCertifications_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociatesCertifications] CHECK CONSTRAINT [FK_AssociatesCertifications_Employee]
GO

ALTER TABLE [dbo].[AssociatesCertifications]  WITH CHECK ADD  CONSTRAINT [FK_AssociatesCertifications_Skills] FOREIGN KEY([CertificationID])
REFERENCES [dbo].[Skills] ([SkillId])
GO

ALTER TABLE [dbo].[AssociatesCertifications] CHECK CONSTRAINT [FK_AssociatesCertifications_Skills]
GO

ALTER TABLE [dbo].[AssociatesCertifications]  WITH CHECK ADD  CONSTRAINT [FK_AssociatesCertifications_SkillGroup] FOREIGN KEY([SkillGroupID])
REFERENCES [dbo].[SkillGroup] ([SkillGroupId])
GO

ALTER TABLE [dbo].[AssociatesCertifications] CHECK CONSTRAINT [FK_AssociatesCertifications_SkillGroup]
GO