﻿CREATE TABLE [dbo].[SkillsSubmittedForApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[CompetencyAreaId] [int] NOT NULL,
	[SkillId] [int] NULL,
	[ProficiencyLevelId] [int] NULL,
	[Experience] [int] NULL,
	[LastUsed] [int] NULL,
	[IsPrimary] [bit] NULL,
	[IsActive] [bit] NULL,
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[SkillGroupId] [int] NULL,
 CONSTRAINT [PK_SkillsSubmittedForApproval_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] ADD  CONSTRAINT [DF_SkillsSubmittedForApproval_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] ADD  CONSTRAINT [DF_SkillsSubmittedForApproval_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] ADD  CONSTRAINT [DF_SkillsSubmittedForApproval_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval]  WITH CHECK ADD  CONSTRAINT [FK_SkillsSubmittedForApproval_CompetencyArea] FOREIGN KEY([CompetencyAreaId])
REFERENCES [dbo].[CompetencyArea] ([CompetencyAreaId])
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] CHECK CONSTRAINT [FK_SkillsSubmittedForApproval_CompetencyArea]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval]  WITH NOCHECK ADD  CONSTRAINT [FK_SkillsSubmittedForApproval_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] CHECK CONSTRAINT [FK_SkillsSubmittedForApproval_Employee]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval]  WITH NOCHECK ADD  CONSTRAINT [FK_SkillsSubmittedForApproval_ProficiencyLevel] FOREIGN KEY([ProficiencyLevelId])
REFERENCES [dbo].[ProficiencyLevel] ([ProficiencyLevelId])
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] CHECK CONSTRAINT [FK_SkillsSubmittedForApproval_ProficiencyLevel]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval]  WITH CHECK ADD  CONSTRAINT [FK_SkillsSubmittedForApproval_SkillGroup] FOREIGN KEY([SkillGroupId])
REFERENCES [dbo].[SkillGroup] ([SkillGroupId])
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] CHECK CONSTRAINT [FK_SkillsSubmittedForApproval_SkillGroup]
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval]  WITH CHECK ADD  CONSTRAINT [FK_SkillsSubmittedForApproval_Skills] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skills] ([SkillId])
GO

ALTER TABLE [dbo].[SkillsSubmittedForApproval] CHECK CONSTRAINT [FK_SkillsSubmittedForApproval_Skills]
GO