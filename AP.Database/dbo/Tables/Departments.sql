﻿CREATE TABLE [dbo].[Departments] (
    [DepartmentId]   INT           IDENTITY (1, 1) NOT NULL,
    [Description]    VARCHAR (50) NOT NULL,
    [IsActive]       BIT           NULL DEFAULT 1,
    [CreatedUser]    VARCHAR (100) CONSTRAINT [DF_Department_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]   VARCHAR (100) NULL,
    [CreatedDate]    DATETIME      CONSTRAINT [DF_Department_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]   DATETIME      NULL,
    [SystemInfo]     VARCHAR (50)  CONSTRAINT [DF_Departments_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [DepartmentCode] VARCHAR (20) NOT NULL,
    [DepartmentHeadID] INT NULL, 
    [DepartmentTypeId] INT NOT NULL , 
    CONSTRAINT [PK__Departme__B2079BED9864C3A2] PRIMARY KEY CLUSTERED ([DepartmentId] ASC), 
    CONSTRAINT [FK_Departments_DepartmentType] FOREIGN KEY ([DepartmentTypeId]) REFERENCES [DepartmentType]([DepartmentTypeId])
);



