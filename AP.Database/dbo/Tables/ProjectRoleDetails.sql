﻿CREATE TABLE [dbo].[ProjectRoleDetails](
	[RoleAssignmentId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[ProjectId] [int] NULL,
	[RoleMasterId] [int] NULL,
	[IsPrimaryRole] [bit] NULL,
	[StatusId] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	[RejectReason] [varchar](max) NULL,
 CONSTRAINT [PK_EmployeeRoleDetails] PRIMARY KEY CLUSTERED 
(
	[RoleAssignmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProjectRoleDetails]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeRoleDetails_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[ProjectRoleDetails] CHECK CONSTRAINT [FK_EmployeeRoleDetails_Employee]
GO

ALTER TABLE [dbo].[ProjectRoleDetails]  WITH CHECK ADD  CONSTRAINT [FK_ProjectRoleDetails_RoleMaster] FOREIGN KEY([RoleMasterId])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO

ALTER TABLE [dbo].[ProjectRoleDetails] CHECK CONSTRAINT [FK_ProjectRoleDetails_RoleMaster]
GO


