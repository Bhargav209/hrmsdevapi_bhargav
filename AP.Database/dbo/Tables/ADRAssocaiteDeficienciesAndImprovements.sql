﻿CREATE TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[CompetencyAreaID] [int] NOT NULL,
	[ADRCycleID] [int] NOT NULL,
	[FinancialYearID] [int] NOT NULL,
	[AreaTypeID] [int] NOT NULL,
	[Comments] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [varchar](150) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](150) NULL,
	[SystemInfo] [varchar](50) NULL,
 CONSTRAINT [PK_ADRAssocaiteDeficienciesAndImprovements] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] ADD  CONSTRAINT [DF_ADRAssocaiteDeficienciesAndImprovements_CreatedDate1]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] ADD  CONSTRAINT [DF_ADRAssocaiteDeficienciesAndImprovements_CreatedUser]  DEFAULT (suser_sname()) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] ADD  CONSTRAINT [DF_ADRAssocaiteDeficienciesAndImprovements_CreatedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] ADD  CONSTRAINT [DF_ADRAssocaiteDeficienciesAndImprovements_ModifiedUser]  DEFAULT (suser_sname()) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] ADD  CONSTRAINT [DF_ADRAssocaiteDeficienciesAndImprovements_SystemInfo]  DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) FOR [SystemInfo]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssocaiteDeficienciesAndImprovements_ADRCycle] FOREIGN KEY([ADRCycleID])
REFERENCES [dbo].[ADRCycle] ([ADRCycleID])
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] CHECK CONSTRAINT [FK_ADRAssocaiteDeficienciesAndImprovements_ADRCycle]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssocaiteDeficienciesAndImprovements_AreaType] FOREIGN KEY([AreaTypeID])
REFERENCES [dbo].[AreaType] ([ID])
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] CHECK CONSTRAINT [FK_ADRAssocaiteDeficienciesAndImprovements_AreaType]
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements]  WITH CHECK ADD  CONSTRAINT [FK_ADRAssocaiteDeficienciesAndImprovements_FinancialYear] FOREIGN KEY([FinancialYearID])
REFERENCES [dbo].[FinancialYear] ([ID])
GO

ALTER TABLE [dbo].[ADRAssocaiteDeficienciesAndImprovements] CHECK CONSTRAINT [FK_ADRAssocaiteDeficienciesAndImprovements_FinancialYear]
GO


