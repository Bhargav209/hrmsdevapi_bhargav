﻿CREATE TABLE [dbo].[Projects] (
    [ProjectId]        INT           IDENTITY (1, 1) NOT NULL,
    [ProjectCode]      VARCHAR (100) NULL,
    [ProjectName]      VARCHAR (256) NULL,
    [PlannedStartDate] DATETIME      NULL,
    [PlannedEndDate]   DATETIME      NULL,
    [ProjectTypeId]    INT           NULL,
    [ClientId]         INT           NULL,
    [StatusId]         INT           NULL,
    [IsActive]         BIT           NULL,
    [CreatedUser]      VARCHAR (100) CONSTRAINT [DF_Projects_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]     VARCHAR (100) NULL,
    [CreatedDate]      DATETIME      CONSTRAINT [DF_Projects_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]     DATETIME      NULL,
    [SystemInfo]       VARCHAR (50)  CONSTRAINT [DF_Projects_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    [ActualStartDate]  DATETIME      NULL,
    [ActualEndDate]    DATETIME      NULL,
    [DepartmentId]     INT           NULL,
    [PracticeAreaId] INT NOT NULL, 
    [LeadID] INT NULL, 
    [DomainId] INT NULL, 
    [ProjectStateId] INT NULL, 
    PRIMARY KEY CLUSTERED ([ProjectId] ASC),
    CONSTRAINT [FK_Projects_Clients1] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Clients] ([ClientId]),
    CONSTRAINT [FK_Projects_Departments] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Departments] ([DepartmentId]),
    CONSTRAINT [FK_Projects_ProjectTypeId] FOREIGN KEY ([ProjectTypeId]) REFERENCES [dbo].[ProjectType] ([ProjectTypeId]),
    CONSTRAINT [FK_Projects_PracticeArea] FOREIGN KEY ([PracticeAreaId]) REFERENCES [PracticeArea]([PracticeAreaId]),
	CONSTRAINT [FK_Projects_Domains] FOREIGN KEY ([DomainId]) REFERENCES [Domain]([DomainId])
);







