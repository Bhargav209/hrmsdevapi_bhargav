﻿CREATE TABLE [dbo].[AssociateResignation](
	[ResignationID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[ReasonID] [int] NOT NULL,
	[ReasonDescription] [varchar](max) NOT NULL,
	[DateOfResignation] [datetime] NOT NULL,
	[LastWorkingDate] [datetime] NOT NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit]  NULL,
 CONSTRAINT [PK_AssociateResignation] PRIMARY KEY CLUSTERED 
(
	[ResignationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssociateResignation]  WITH CHECK ADD  CONSTRAINT [FK_AssociateResignation_AssociateResignation] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO

ALTER TABLE [dbo].[AssociateResignation] CHECK CONSTRAINT [FK_AssociateResignation_AssociateResignation]
GO



