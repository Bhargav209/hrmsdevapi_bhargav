﻿CREATE TABLE [dbo].[InternalBillingRoles] (
    [InternalBillingRoleId]   INT           IDENTITY (1, 1) NOT NULL,
    [InternalBillingRoleCode] VARCHAR (100) NULL,
    [InternalBillingRoleName] VARCHAR (256) NULL,
    [IsActive]                BIT           NULL,
    [CreatedUser]             VARCHAR (100) CONSTRAINT [DF_InternalBillingRoles_CreatedUser] DEFAULT (suser_sname()) NULL,
    [ModifiedUser]            VARCHAR (100) NULL,
    [CreatedDate]             DATETIME      CONSTRAINT [DF_InternalBillingRoles_CreatedDate] DEFAULT (getdate()) NULL,
    [ModifiedDate]            DATETIME      NULL,
    [SystemInfo]              VARCHAR (50)  CONSTRAINT [DF_InternalBillingRoles_SystemInfo] DEFAULT (CONVERT([char](15),connectionproperty('client_net_address'))) NULL,
    CONSTRAINT [PK__InternalBillingRoles] PRIMARY KEY CLUSTERED ([InternalBillingRoleId] ASC)
);

