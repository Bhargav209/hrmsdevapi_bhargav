﻿

CREATE TABLE [dbo].[Addendum](
	[AddendumId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[Id] [Int] NOT NULL,
	[SOWId] [varchar](15) NOT NULL,
	[AddendumNo] [int]  NULL,
	[RecipientName] [varchar](30) NULL,
	[AddendumDate] [datetime] NOT NULL,
	[Note] [varchar](250) NULL,	
	[CreatedUser] [varchar](100) NULL,
	[ModifiedUser] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[SystemInfo] [varchar](50) NULL,
	
 CONSTRAINT [PK_AddendumId] PRIMARY KEY CLUSTERED 
(
	[AddendumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO

ALTER TABLE [dbo].[Addendum]  WITH CHECK ADD  CONSTRAINT [FK_Addendum_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([ProjectId])
GO

ALTER TABLE [dbo].[Addendum]  WITH CHECK ADD  CONSTRAINT [FK_Addendum_SOW] FOREIGN KEY([Id])
REFERENCES [dbo].[SOW] ([Id])
GO





