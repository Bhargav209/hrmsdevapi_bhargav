﻿CREATE TABLE [dbo].[lkValue](
	[ValueKey] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[IsActive] [bit] NULL,
	[ValueID] [nvarchar](255) NOT NULL,
	[ValueName] [nvarchar](255) NOT NULL,
	[ValueTypeKey] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_lkValue] PRIMARY KEY CLUSTERED 
(
	[ValueKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UNQ_lkValue] UNIQUE NONCLUSTERED 
(
	[ValueID] ASC,
	[ValueTypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[lkValue]  WITH CHECK ADD  CONSTRAINT [FK_lkValueType_lkValue] FOREIGN KEY([ValueTypeKey])
REFERENCES [dbo].[ValueType] ([ValueTypeKey])
GO

ALTER TABLE [dbo].[lkValue] CHECK CONSTRAINT [FK_lkValueType_lkValue]
GO