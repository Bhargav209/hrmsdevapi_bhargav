﻿CREATE FUNCTION [dbo].[udfGetEmployeeFullName](@EmployeeId int)  
RETURNS VARCHAR(300)   
AS   
BEGIN  
    RETURN
	(
		SELECT 
			CONCAT (FirstName, ' ', MiddleName, ' ', LastName) AS EmployeeName
		FROM 
			[dbo].[Employee] employee 
		WHERE 
			employee.employeeid=@EmployeeId 
	)
END; 