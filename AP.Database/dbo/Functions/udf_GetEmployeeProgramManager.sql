﻿CREATE FUNCTION [dbo].[udf_GetEmployeeProgramManager](@EmployeeId int)        
RETURNS varchar(300)         
AS         
      
BEGIN        
      
DECLARE @EmployeeprogramManagerCount INT       
DECLARE @programmanagers VARCHAR(MAX) = NULL, @programmanager VARCHAR(MAX)       
      
      
SELECT @EmployeeprogramManagerCount = COUNT(*) from  [dbo].[Projects] project    INNER join AssociateAllocation allocation                    
   ON allocation.ProjectId = project.ProjectId 
   INNER JOIN ProjectManagers PM ON project.ProjectId = PM.ProjectID AND  PM.IsActive=1  
   AND allocation.EmployeeId=@EmployeeId and allocation.IsActive=1
      
IF(@EmployeeprogramManagerCount > 1)       
BEGIN      
 DECLARE db_cursor CURSOR FOR       
  
  select (emp.FirstName+' '+emp.LastName) from  [dbo].[Projects] project    INNER join AssociateAllocation allocation                    
   ON allocation.ProjectId = project.ProjectId 
   INNER JOIN ProjectManagers PM ON project.ProjectId = PM.ProjectID AND  PM.IsActive=1   and allocation.EmployeeId=@EmployeeId and allocation.IsActive=1
   Inner join  Employee emp on PM.ProgramManagerID=emp.EmployeeId
      
OPEN db_cursor        
FETCH NEXT FROM db_cursor INTO @programmanager        
      
WHILE @@FETCH_STATUS = 0        
BEGIN        
 IF(@programmanagers IS NULL)      
  SET @programmanagers = @programmanager      
 ELSE      
  SET @programmanagers = @programmanagers + ', ' + @programmanager      
        
      FETCH NEXT FROM db_cursor INTO @programmanager       
END       
CLOSE db_cursor        
DEALLOCATE db_cursor       
END      
      
ELSE      
BEGIN      
 SELECT @programmanagers =  (emp.FirstName+' '+emp.LastName) from  [dbo].[Projects] project    INNER join AssociateAllocation allocation                    
   ON allocation.ProjectId = project.ProjectId 
   INNER JOIN ProjectManagers PM ON project.ProjectId = PM.ProjectID AND  PM.IsActive=1   
   AND allocation.EmployeeId=@EmployeeId and allocation.IsActive=1
   Inner join  Employee emp on PM.ProgramManagerID=emp.EmployeeId 
END      
      
    RETURN      
 (      
  SELECT @programmanagers      
 )      
END; 