﻿CREATE FUNCTION [dbo].[udf_GetAssociateLead]   
(  
@EmployeeId INT  
)          
RETURNS INT          
AS               
BEGIN    
  
DECLARE @LeadID INT,@IsAnyAllocation INT,@HighestAllocationPercentage INT

 --Checking If  any allocation happend to Employee
select @IsAnyAllocation =COUNT(EmployeeId)  FROM
 AssociateAllocation   where EmployeeId= @EmployeeId and IsActive=1

 
 --If Yes,then work flows from here
IF( @IsAnyAllocation > 0)
BEGIN
			--Checking If Employee allocated to single project
			IF( @IsAnyAllocation = 1)
			BEGIN
			--If Talent pool directly take LeadId from project 
				--else take Lead / PM/ RM from program manager
				select @LeadID = ISNULL(proj.LeadId,ISNULL(pgm.LeadID , ISNULL(pgm.ReportingManagerID, pgm.ProgramManagerID)))  
				From  Employee emp 
				 inner join AssociateAllocation aal on emp.EmployeeId= aal.EmployeeId  
				 inner join Projects proj  on aal.ProjectId=proj.ProjectId  
				 Left outer join ProjectManagers pgm  on aal.ProjectId=pgm.ProjectID AND  pgm.IsActive=1
				left outer join employee empl ON empl.EmployeeId = pgm.LeadID  
				 left outer join users us on empl.UserId=us.UserId  
				 where   aal.IsActive=1  and emp.EmployeeId=@EmployeeId
			END

			--If Employee allocated to multiple projects (project /Talentpool)
			ELSE 
			
			--Geeting Highest alocation by excluding Talent pool
			Set @HighestAllocationPercentage = (SELECT TOP 1 AllocationPercentage FROM (SELECT DISTINCT TOP (1) AllocationPercentage FROM AssociateAllocation aal 
											inner join Projects proj on aal.ProjectId = proj.ProjectId
											where aal.EmployeeId=@EmployeeId and aal.IsActive=1 AND proj.LeadID IS NULL  ORDER BY aal.AllocationPercentage DESC) a
										ORDER BY AllocationPercentage) 

			  --Then we are selecting project and ignoring Talentpool (if present) If not, we are taking highest allocation on comparing projects
				select @LeadID= ISNULL(pgm.LeadID , ISNULL(pgm.ReportingManagerID, pgm.ProgramManagerID))  
				From  Employee emp 
				 inner join AssociateAllocation aal on emp.EmployeeId= aal.EmployeeId  AND aal.AllocationPercentage =@HighestAllocationPercentage 
				 inner join Projects proj  on aal.ProjectId=proj.ProjectId AND proj.LeadID IS NULL 
				 inner join ProjectManagers pgm  on aal.ProjectId=pgm.ProjectID  
				 left outer join employee empl ON empl.EmployeeId = pgm.LeadID  
				 left outer join users us on empl.UserId=us.UserId  
				 where pgm.IsActive=1 and aal.IsActive=1  and emp.EmployeeId=@EmployeeId
	
 END
 ELSE
 BEGIN
	 set  @LeadID=-1;
	  END
	  RETURN @LeadID  		
END; 