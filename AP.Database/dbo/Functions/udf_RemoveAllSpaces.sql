﻿CREATE FUNCTION [dbo].[udf_RemoveAllSpaces]
(
    @InputStr VARCHAR(8000)
)
RETURNS VARCHAR(8000)
AS
BEGIN
DECLARE @ResultStr VARCHAR(8000)
SET @ResultStr = @InputStr
WHILE charindex(' ', @ResultStr) > 0
    SET @ResultStr = replace(@InputStr, ' ', '')

RETURN @ResultStr
END