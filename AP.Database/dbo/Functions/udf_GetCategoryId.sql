﻿CREATE FUNCTION [dbo].[udf_GetCategoryId](@CategoryName NVARCHAR(50))  
RETURNS INT   
AS   
BEGIN  
    RETURN
	(
		SELECT              
			[categoryMaster].CategoryID
		FROM [dbo].[CategoryMaster] categoryMaster        
		WHERE 
			categoryMaster.CategoryName = @CategoryName   
	)
END;
