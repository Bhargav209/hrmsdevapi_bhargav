﻿CREATE FUNCTION [dbo].[udf_GetEmployeeAllocationPercentage](@EmployeeId int)      
RETURNS varchar(300)       
AS       
    
BEGIN      
    
DECLARE @AllocationpercentagecountCount INT     
DECLARE @Allocations VARCHAR(MAX) = NULL, @Allocation VARCHAR(MAX)     
    
    
SELECT @AllocationpercentagecountCount = COUNT(*) FROM AssociateAllocation allocation
Inner join AllocationPercentage per on allocation.AllocationPercentage=per.AllocationPercentageID
  WHERE allocation.EmployeeId = @EmployeeId  and allocation.IsActive=1
    
IF(@AllocationpercentagecountCount > 1)     
BEGIN    
 DECLARE db_cursor CURSOR FOR     

  SELECT per.Percentage FROM AssociateAllocation allocation
Inner join AllocationPercentage per on allocation.AllocationPercentage=per.AllocationPercentageID
  WHERE allocation.EmployeeId = @EmployeeId  and allocation.IsActive=1
    
OPEN db_cursor      
FETCH NEXT FROM db_cursor INTO @Allocation      
    
WHILE @@FETCH_STATUS = 0      
BEGIN      
 IF(@Allocations IS NULL)    
  SET @Allocations = @Allocation    
 ELSE    
  SET @Allocations = @Allocations + ', ' + @Allocation    
      
      FETCH NEXT FROM db_cursor INTO @Allocation     
END     
CLOSE db_cursor      
DEALLOCATE db_cursor     
END    
    
ELSE    
BEGIN    
 SELECT @Allocations =  per.Percentage    
 FROM AssociateAllocation allocation
Inner join AllocationPercentage per on allocation.AllocationPercentage=per.AllocationPercentageID
  WHERE allocation.EmployeeId = @EmployeeId  and allocation.IsActive=1
END    
    
    RETURN    
 (    
  SELECT @Allocations    
 )    
END; 