﻿CREATE FUNCTION [dbo].[udf_GetRoleName](@RoleMasterID int)  
RETURNS varchar(300)   
AS   
BEGIN  
    RETURN
	(
		SELECT              
			LTRIM(RTRIM(CONCAT(prefix.PrefixName, ' ', sgRoles.SGRoleName, ' ', suffix.SuffixName))) AS RoleName          
		FROM [dbo].[RoleMaster] roleMaster        
		INNER JOIN [dbo].[SGRole] sgRoles        
		ON roleMaster.SGRoleID = sgRoles.SGRoleID        
		LEFT JOIN [dbo].[SGRolePrefix] prefix        
		ON roleMaster.PrefixID = prefix.PrefixID        
		LEFT JOIN [dbo].[SGRoleSuffix] suffix        
		ON roleMaster.SuffixID = suffix.SuffixID       
		WHERE 
			roleMaster.RoleMasterID = @RoleMasterID   
	)
END; 
