﻿
CREATE VIEW [dbo].[vw_AssociateSkillsPendingApproval]            
AS            
 SELECT   distinct          
   emp.EmployeeId AS empID          
  ,emp.EmployeeCode AS empCode          
  ,emp.FirstName  + ' ' + emp.LastName AS empName            
  ,comp.CompetencyAreaCode  AS CompetencyArea         
  ,skill.SkillName        
  ,sgp.SkillGroupName          
  ,pl.ProficiencyLevelCode  AS ProficiencyLevel       
  --,pgm.LeadID      
  ,us.EmailAddress AS ToEmailAddress      
  --,dbo.[udf_GetAssociateLead](emp.EmployeeId)     
          
  FROM SkillsSubmittedForApproval ssa         
 Inner join  EMPLOYEE emp           
 ON ssa.EmployeeId = emp.EmployeeId            
 INNER JOIN CompetencyArea comp            
 ON ssa.CompetencyAreaId = comp.CompetencyAreaId           
  INNER JOIN Skills skill            
 ON ssa.SkillId = skill.SkillId            
 INNER JOIN SkillGroup sgp            
 ON ssa.SkillGroupId = sgp.SkillGroupId          
 INNER JOIN ProficiencyLevel pl          
 ON ssa.ProficiencyLevelId = pl.ProficiencyLevelId         
  INNER JOIN SkillsWorkFlow swf          
 ON ssa.ID = swf.SubmittedRequisitionId              
 left outer join employee empl ON empl.EmployeeId = dbo.[udf_GetAssociateLead](emp.EmployeeId)     
 left outer join users us on empl.UserId=us.UserId        
 WHERE swf.Status = 18 AND ssa.IsActive = 1 