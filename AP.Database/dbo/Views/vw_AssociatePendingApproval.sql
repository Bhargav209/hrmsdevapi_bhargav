﻿CREATE VIEW [dbo].[vw_AssociatePendingApproval]  
AS  
 SELECT   
   emp.EmployeeId AS empID
  ,emp.EmployeeCode AS empCode
  ,emp.FirstName  + ' ' + emp.LastName AS empName  
  ,dept.DepartmentCode AS Department
  ,desg.DesignationName AS Designation
  ,pa.PracticeAreaDescription AS Technology
  ,employ.FirstName + ' ' + employ.LastName AS ReportingManager  
 FROM EMPLOYEE emp  
 INNER JOIN Designations desg  
 ON emp.Designation = desg.DesignationId  
 INNER JOIN Departments dept  
 ON emp.DepartmentId = dept.DepartmentId
 INNER JOIN PracticeArea pa
 ON emp.CompetencyGroup = pa.PracticeAreaId  
 INNER JOIN Employee employ
 ON emp.ReportingManager  = employ.EmployeeId 
 WHERE emp.StatusId = 2 AND emp.IsActive = 1