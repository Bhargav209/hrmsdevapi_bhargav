﻿CREATE TRIGGER trg_NotificationType_DML_BLOCK 
ON [HRMS2].[dbo].[NotificationType]
FOR INSERT, UPDATE, DELETE
AS
BEGIN TRY
	RAISERROR ('Can not perform dml operations on NotificationType table, either disable/delete the trigger', 16, 1);
END TRY
BEGIN CATCH
THROW
END CATCH
