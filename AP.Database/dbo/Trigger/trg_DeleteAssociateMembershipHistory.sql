﻿CREATE TRIGGER [dbo].[trg_DeleteAssociateMembershipHistory]   
ON [dbo].[AssociatesMemberShips]
FOR DELETE AS
--Audit OLD record.  
	INSERT INTO [dbo].[AssociatesMemberShipsHistory]
		( 
			 ID
			,EmployeeID
			,ProgramTitle
			,ValidFrom
			,Institution
			,Specialization
			,ValidUpto
		)  
	SELECT
			 d.ID
			,d.EmployeeId
			,d.ProgramTitle
			,d.ValidFrom
			,d.Institution
			,d.Specialization
 			,d.ValidUpto
	FROM Deleted d
