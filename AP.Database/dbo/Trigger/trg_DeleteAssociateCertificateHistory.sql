﻿CREATE TRIGGER [dbo].[trg_DeleteAssociateCertificateHistory]   
ON [dbo].[AssociatesCertifications]
FOR DELETE AS
--Audit OLD record.  
	INSERT INTO [dbo].[AssociatesCertificationsHistory]
		( 
			 ID
			,EmployeeID
			,CertificationID
			,ValidFrom
			,Institution
			,Specialization
			,ValidUpto
			,SkillGroupID
		)  
	SELECT
			 d.ID
			,d.EmployeeId
			,d.CertificationID
			,d.ValidFrom
			,d.Institution
			,d.Specialization
 			,d.ValidUpto
			,d.SkillGroupId
	FROM Deleted d