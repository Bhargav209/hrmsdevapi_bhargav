﻿-- =============================================
-- Author:		Bhavani
-- Create date: 14-11-2018 15:51:30
-- Description:	Trigger to update talent pool allocation when ever Technology or Competency is changed
-- =============================================
CREATE TRIGGER [dbo].[trg_updateEmployeePoolAllocation] 
   ON  [dbo].[Employee]
   AFTER UPDATE AS

DECLARE @EmployeeId INT, @AssociateAllocationId INT, @AllocationPercentageId INT, @ReleaseDate Date, @RoleMasterId INT, @PracticeAreaId INT,
@TalentpoolProjectId INT, @ReportingManagerId INT, @ModifiedUser varchar(100), @SystemInfo varchar(100), @ModifiedDate Date, @ProjectTypeId INT

IF (UPDATE([CompetencyGroup])) 
BEGIN  

SELECT @EmployeeId = I.EmployeeId
,@PracticeAreaId = I.CompetencyGroup
,@ModifiedUser = d.ModifiedUser
,@SystemInfo = d.SystemInfo
,@ModifiedDate = I.ModifiedDate
FROM inserted I INNER JOIN deleted d on d.EmployeeId = i.EmployeeId

SELECT @ProjectTypeId= ProjectTypeId FROM ProjectType WHERE ProjectTypeCode = 'Talent Pool' 

UPDATE allocation
SET 
 allocation.IsActive = 0
,allocation.ReleaseDate = CASE WHEN @ModifiedDate >= allocation.EffectiveDate THEN  @ModifiedDate ELSE allocation.EffectiveDate END
,allocation.ModifiedDate = GETDATE()
,allocation.ModifiedBy = @ModifiedUser
,allocation.SystemInfo = @SystemInfo
,@AssociateAllocationId = allocation.AssociateAllocationId
FROM
AssociateAllocation allocation
INNER JOIN Projects project on project.ProjectId = allocation.ProjectId
INNER JOIN ProjectType pt on pt.ProjectTypeId = project.ProjectTypeId
WHERE allocation.EmployeeId = @EmployeeId AND allocation.IsActive = 1 AND pt.ProjectTypeId = @ProjectTypeId

IF(@AssociateAllocationId > 0)
BEGIN
SELECT @RoleMasterId = RoleMasterId, @AllocationPercentageId = AllocationPercentage, @ReleaseDate = ReleaseDate FROM AssociateAllocation WHERE AssociateAllocationId = @AssociateAllocationId

SELECT @TalentpoolProjectId = talentpool.ProjectId, @ReportingManagerId = manager.ReportingManagerID from TalentPool talentpool
INNER JOIN ProjectManagers manager on manager.ProjectID = talentpool.ProjectId 
WHERE talentpool.PracticeAreaId = @PracticeAreaId AND manager.IsActive=1

INSERT INTO [dbo].[AssociateAllocation] 
	( 
	  [TRId]
	 ,[ProjectId]
	 ,[EmployeeId]
	 ,[RoleMasterId]
	 ,[IsActive]
	 ,[AllocationPercentage]
	 ,[InternalBillingPercentage]
	 ,[IsCritical]
	 ,[EffectiveDate]
	 ,[AllocationDate]
	 ,[CreatedBy]
	 ,[CreateDate]
	 ,[SystemInfo]
	 ,[ReportingManagerId]
	 ,[IsPrimary]
	 ,[IsBillable]
	 ,[ClientBillingPercentage]
	 )  
     values
	 (
		 1
		,@TalentpoolProjectId
		,@EmployeeId
		,@RoleMasterId
		,1
		,@AllocationPercentageId
		,0
		,0
		,CASE WHEN @ModifiedDate >= @ReleaseDate THEN  @ModifiedDate ELSE @ReleaseDate END
		,GETDATE()
		,@ModifiedUser
		,GETDATE()
		,@SystemInfo
		,@ReportingManagerID
		,0
		,0
		,0
  	 )  
	
END
END;
