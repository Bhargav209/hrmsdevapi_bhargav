﻿CREATE TRIGGER [dbo].[updateEmployeeData]   
ON [dbo].[Employee]
AFTER UPDATE AS
IF (UPDATE([Designation]) OR UPDATE([GradeId]) OR UPDATE([DepartmentId]) OR UPDATE([CompetencyGroup])) 
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[AssociateHistory] 
       ( 
        [EmployeeID]
       ,[DesignationId]
       ,[GradeId]
       ,[Remarks]
       ,[DepartmentId]
       ,[PracticeAreaId]
       ,[CreatedBy]
       ,[CreatedDate]
       ,[ModifiedDate]
       ,[ModifiedUser]
       ,[SystemInfo]
       )  
     SELECT
        d.EmployeeId,  
        d.Designation,  
        d.GradeId, 
        'History Has been recorded using trigger - updateEmployeeData ',
        d.DepartmentId,
        d.CompetencyGroup,
        d.CreatedUser,
        d.CreatedDate,
        d.ModifiedDate,
        d.ModifiedUser, 
        d.SystemInfo
          
       FROM Inserted i
       INNER JOIN Deleted d ON i.EmployeeID = d.EmployeeID
END;  
