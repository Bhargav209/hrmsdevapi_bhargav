﻿CREATE TRIGGER [dbo].[trg_EmployeeSkillsHistory]   
ON [dbo].[EmployeeSkills]
FOR DELETE AS 
	INSERT INTO [dbo].[EmployeeSkillsHistory]
		( 
			 ID
			,EmployeeId
			,CompetencyAreaId
			,SkillId
			,ProficiencyLevelId
			,Experience
			,LastUsed
			,IsPrimary
			,SkillGroupId
		)  
	SELECT
			 d.ID
			,d.EmployeeId
			,d.CompetencyAreaId
			,d.SkillId
			,d.ProficiencyLevelId
			,d.Experience
 			,d.LastUsed
			,d.IsPrimary
			,d.SkillGroupId
	FROM Deleted d