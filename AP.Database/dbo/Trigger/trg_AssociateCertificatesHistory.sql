﻿CREATE TRIGGER [dbo].[trg_UpdateAssociateCertificates]   
ON [dbo].[AssociatesCertifications]
AFTER UPDATE AS
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[AssociatesCertificationsHistory] 
	(
	 ID
	,EmployeeID
	,CertificationID
	,ValidFrom
	,Institution
	,Specialization
	,ValidUpto
	,SkillGroupID
	)  
     SELECT
		d.ID,
        d.EmployeeId,  
        d.CertificationID,  
        d.ValidFrom,  
        d.Institution,
		d.Specialization,
		d.ValidUpto,
		d.SkillGroupID
	FROM Inserted i
	INNER JOIN Deleted d ON i.ID = d.ID
END; 