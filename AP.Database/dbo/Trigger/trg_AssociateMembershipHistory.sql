﻿CREATE TRIGGER [dbo].[trg_UpdateAssociateMemberships]   
ON [dbo].[AssociatesMemberShips]
AFTER UPDATE AS
BEGIN  
  --Audit OLD record.  
  INSERT INTO [dbo].[AssociatesMemberShipsHistory]
	(
	 ID 
	,EmployeeID
	,ProgramTitle
	,ValidFrom
	,Institution
	,Specialization
	,ValidUpto
	)  
     SELECT
		d.ID,
        d.EmployeeId,  
        d.ProgramTitle,  
        d.ValidFrom,  
        d.Institution,
		d.Specialization,
		d.ValidUpto
	FROM Inserted i
	INNER JOIN Deleted d ON i.ID = d.ID
END; 