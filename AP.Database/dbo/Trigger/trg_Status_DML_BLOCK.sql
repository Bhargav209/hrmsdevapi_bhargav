﻿CREATE TRIGGER trg_Status_DML_BLOCK 
ON [HRMS2].[dbo].[Status]
FOR INSERT, UPDATE, DELETE
AS
BEGIN TRY
	RAISERROR ('Can not perform dml operations on Status table, either disable/delete the trigger', 16, 1);
END TRY
BEGIN CATCH
THROW
END CATCH
