﻿/****** Object:  Indexes [IX_KRAComments_FinancialYearId]    Script Date: 25/06/2018 2:07:12 PM ******/
CREATE NONCLUSTERED INDEX [IX_KRAComments_FinancialYearId] ON [dbo].[KRAComment]
(
	[FinancialYearId] ASC
)
GO