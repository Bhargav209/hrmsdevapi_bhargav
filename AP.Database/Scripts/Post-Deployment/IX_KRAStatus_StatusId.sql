﻿/****** Object:  Indexes [IX_KRAStatus_StatusId]    Script Date: 25/06/2018 2:07:12 PM ******/
CREATE NONCLUSTERED INDEX [IX_KRAStatus_StatusId] ON [dbo].[KRAStatus]
(
	[StatusId] ASC
)
GO