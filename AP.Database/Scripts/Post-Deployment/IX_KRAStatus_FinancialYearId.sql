﻿/****** Object:  Indexes [IX_KRAStatus_FinancialYearId]    Script Date: 25/06/2018 2:07:12 PM ******/
CREATE NONCLUSTERED INDEX [IX_KRAStatus_FinancialYearId] ON [dbo].[KRAStatus]
(
	[FinancialYearId] ASC
)
GO