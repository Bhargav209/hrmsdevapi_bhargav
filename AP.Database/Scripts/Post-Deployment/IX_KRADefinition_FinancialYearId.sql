﻿/****** Object:  Indexes [IX_KRADefinition_FinancialYearId]    Script Date: 25/06/2018 2:07:12 PM ******/
CREATE NONCLUSTERED INDEX [IX_KRADefinition_FinancialYearId] ON [dbo].[KRADefinition]
(
	[FinancialYearId] ASC
)
GO