﻿/****** Object:  Index [IX_NotificationConfiguration]    Script Date: 6/15/2018 10:15:07 AM ******/
CREATE NONCLUSTERED INDEX [IX_NotificationConfiguration] ON [dbo].[NotificationConfiguration]
(
	[NotificationTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


