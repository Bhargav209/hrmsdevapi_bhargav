﻿--SET IDENTITY_INSERT [dbo].[BehaviorArea] ON 
--INSERT [dbo].[BehaviorArea] ([BehaviorAreaId], [BehaviorAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (1, N'Client Centricity', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorArea] ([BehaviorAreaId], [BehaviorAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (2, N'Team Working', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorArea] ([BehaviorAreaId], [BehaviorAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (3, N'Honesty, Openness and Mutual Respect', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorArea] ([BehaviorAreaId], [BehaviorAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (4, N'Pursuit of Excellence', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorArea] ([BehaviorAreaId], [BehaviorAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (5, N'Entrepreneurial Spirit', 1, NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[BehaviorArea] OFF

--SET IDENTITY_INSERT [dbo].[BehaviorRatings] ON 
--INSERT [dbo].[BehaviorRatings] ([RatingId], [BehaviorRatingDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (1, N'Excellent', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorRatings] ([RatingId], [BehaviorRatingDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (2, N'Good', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorRatings] ([RatingId], [BehaviorRatingDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (3, N'Satisfactory', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorRatings] ([RatingId], [BehaviorRatingDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (4, N'Moderate', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[BehaviorRatings] ([RatingId], [BehaviorRatingDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (5, N'Poor', 1, NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[BehaviorRatings] OFF

--SET IDENTITY_INSERT [dbo].[ExpertiseArea] ON 
--INSERT [dbo].[ExpertiseArea] ([ExpertiseId], [ExpertiseAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (1, N'Business Domain', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[ExpertiseArea] ([ExpertiseId], [ExpertiseAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (2, N'Technology', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[ExpertiseArea] ([ExpertiseId], [ExpertiseAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (3, N'Software Engineering', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[ExpertiseArea] ([ExpertiseId], [ExpertiseAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (4, N'Process', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[ExpertiseArea] ([ExpertiseId], [ExpertiseAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (5, N'Information Security', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[ExpertiseArea] ([ExpertiseId], [ExpertiseAreaDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (6, N'People Related', 1, NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[ExpertiseArea] OFF

--SET IDENTITY_INSERT [dbo].[LearningAptitude] ON 
--INSERT [dbo].[LearningAptitude] ([LearningAptitudeId], [LearningAptitudeDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (1, N'High Learning Aptitude (Fast Paced Learner)', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[LearningAptitude] ([LearningAptitudeId], [LearningAptitudeDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (2, N'Medium Learning Aptitude (Moderate Paced Learner)', 1, NULL, NULL, NULL, NULL)
--INSERT [dbo].[LearningAptitude] ([LearningAptitudeId], [LearningAptitudeDescription], [IsActive], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate]) VALUES (3, N'Low Learning Aptitude (Slow Paced Learner)', 1, NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[LearningAptitude] OFF

--SET IDENTITY_INSERT [dbo].[RoleStatus] ON 
--INSERT [dbo].[RoleStatus] ([RoleStatusId], [RoleId],[StatusId], [CategoryId], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,13,1,5, NULL, NULL, NULL, NULL)
--INSERT [dbo].[RoleStatus] ([RoleStatusId], [RoleId],[StatusId], [CategoryId], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,13,2,5, NULL, NULL, NULL, NULL)
--INSERT [dbo].[RoleStatus] ([RoleStatusId], [RoleId],[StatusId], [CategoryId], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,13,3,5, NULL, NULL, NULL, NULL)
--INSERT [dbo].[RoleStatus] ([RoleStatusId], [RoleId],[StatusId], [CategoryId], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,13,4,5, NULL, NULL, NULL, NULL)
--INSERT [dbo].[RoleStatus] ([RoleStatusId], [RoleId],[StatusId], [CategoryId], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,89,3,5, NULL, NULL, NULL, NULL)
--INSERT [dbo].[RoleStatus] ([RoleStatusId], [RoleId],[StatusId], [CategoryId], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,89,4,5, NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[RoleStatus] OFF

---- disable constraints
--ALTER TABLE [dbo].[lkValue]
--NOCHECK CONSTRAINT FK_lkValueType_lkValue;
---- delete data in all tables
--DELETE FROM [dbo].[ValueType]


--DBCC CHECKIDENT ('HRMS.dbo.ValueType',RESEED, 0)


----ValueType
--SET IDENTITY_INSERT [dbo].[ValueType] ON 

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (1, 1, N'Qualification', N'Qualification Details', N'sa', CAST(N'2017-03-23 17:43:00.120' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (2, 1, N'Relation', N'Relation Details', N'sa', CAST(N'2017-03-23 17:43:00.123' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (3, 1, N'Country', N'Country Details', N'sa', CAST(N'2017-03-23 17:43:00.130' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (4, 1, N'BloodGroup', N'Blood Group', N'sa', CAST(N'2017-03-23 17:43:00.133' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (5, 1, N'BGVStatus', N'BGV Status', N'sa', CAST(N'2017-03-23 17:43:00.140' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (6, 1, N'RequisitionType', N'Requisition Type', N'sa', CAST(N'2017-03-23 17:43:00.207' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (7, 1, N'Gender ', N'Gender', N'sa', CAST(N'2017-03-23 17:43:00.220' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (8, 1, N'MaritalStatus', N'Marital Status', N'sa', CAST(N'2017-03-23 17:43:00.223' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (9, 1, N'GradeType ', N'Grade Type', N'sa', CAST(N'2017-08-08 15:15:54.887' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (10, 1, N'ExitType ', N'Exit Type', N'sa', CAST(N'2017-08-08 15:15:54.887' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (11, 1, N'Task Category ', N'Task Category', N'sa', CAST(N'2017-08-08 15:15:54.890' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (12, 1, N'Notice Period', N'Notice Period', N'sa', CAST(N'2017-08-08 15:17:22.553' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (13, 1, N'Reason', N'Resignation Reason', N'sa', CAST(N'2017-08-08 16:22:16.710' AS DateTime), NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (19, 1, N'Project Management', N'Project Management', N'sa', NULL, NULL, NULL)

--INSERT [dbo].[ValueType] ([ValueTypeKey], [IsActive], [ValueTypeID], [ValueTypeName], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (20, 1, N'Domain and Technology', N'Domain and Technology', N'sa', NULL, NULL, NULL)

--SET IDENTITY_INSERT [dbo].[ValueType] OFF

---- enable constraints
--ALTER TABLE lkValue
--CHECK CONSTRAINT FK_lkValueType_lkValue;


---- [lkValue]
--SET IDENTITY_INSERT [dbo].[lkValue] ON 

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (1, 1, N'10th', N'10th', 1, N'sa', CAST(N'2017-03-23T17:52:02.013' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (2, 1, N'12th', N'12th', 1, N'sa', CAST(N'2017-03-23T17:52:02.027' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (3, 1, N'Graduation', N'Graduation', 1, N'sa', CAST(N'2017-03-23T17:52:02.070' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (4, 1, N'Post Graduation', N'Post Graduation', 1, N'sa', CAST(N'2017-03-23T17:52:02.173' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (5, 1, N'Daughter', N'Daughter', 2, N'sa', CAST(N'2017-03-23T17:52:02.180' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (6, 1, N'Father', N'Father', 2, N'sa', CAST(N'2017-03-23T17:52:02.207' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (7, 1, N'Father-in-Law', N'Father-in-Law', 2, N'sa', CAST(N'2017-03-23T17:52:02.210' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (8, 1, N'Mother', N'Mother', 2, N'sa', CAST(N'2017-03-23T17:52:02.230' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (9, 1, N'Mother-in-Law', N'Mother-in-Law', 2, N'sa', CAST(N'2017-03-23T17:52:02.250' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (11, 1, N'Son', N'Son', 2, N'sa', CAST(N'2017-03-23T17:52:02.277' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (12, 1, N'India', N'India', 3, N'sa', CAST(N'2017-03-23T17:52:02.290' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (13, 1, N'USA', N'USA', 3, N'sa', CAST(N'2017-03-23T17:52:02.300' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (14, 1, N'UK', N'UK', 3, N'sa', CAST(N'2017-03-23T17:52:02.307' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (15, 1, N'O+', N'O+', 4, N'sa', CAST(N'2017-03-23T17:52:02.310' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (16, 1, N'O-', N'O-', 4, N'sa', CAST(N'2017-03-23T17:52:02.327' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (17, 1, N'A+', N'A+', 4, N'sa', CAST(N'2017-03-23T17:52:02.340' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (18, 1, N'A-', N'A-', 4, N'sa', CAST(N'2017-03-23T17:52:02.347' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (19, 1, N'B+', N'B+', 4, N'sa', CAST(N'2017-03-23T17:52:02.353' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (20, 1, N'B-', N'B-', 4, N'sa', CAST(N'2017-03-23T17:52:02.363' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (21, 1, N'AB+', N'AB+', 4, N'sa', CAST(N'2017-03-23T17:52:02.380' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (22, 1, N'AB-', N'AB-', 4, N'sa', CAST(N'2017-03-23T17:52:02.390' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (23, 1, N'Verified', N'Verified', 5, N'sa', CAST(N'2017-03-23T17:52:02.393' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (24, 1, N'NotVerified', N'NotVerified', 5, N'sa', CAST(N'2017-03-23T17:52:02.400' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (25, 1, N'New', N'New Request', 6, N'sa', CAST(N'2017-03-23T17:52:02.407' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (26, 1, N'Replacement ', N'Replacement Request', 6, N'sa', CAST(N'2017-03-23T17:52:02.410' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (27, 1, N'Female', N'Female', 7, N'sa', CAST(N'2017-03-23T17:52:02.420' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (28, 1, N'Male', N'Male', 7, N'sa', CAST(N'2017-03-23T17:52:02.430' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (29, 1, N'Single', N'Single', 8, N'sa', CAST(N'2017-03-23T17:52:02.437' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (30, 1, N'Married', N'Married', 8, N'sa', CAST(N'2017-03-23T17:52:02.443' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (34, 1, N'Friend', N'Friend', 2, N'sa', CAST(N'2017-03-23T17:52:02.477' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (35, 1, N'Brother-in-Law', N'Brother-in-Law', 2, N'sa', CAST(N'2017-03-23T17:52:02.490' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (36, 1, N'Uncle', N'Uncle', 2, N'sa', CAST(N'2017-03-23T17:52:02.503' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (37, 1, N'Aunt ', N'Aunt', 2, N'sa', CAST(N'2017-03-23T17:52:02.510' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (38, 1, N'Sister-in-Law', N'Sister-in-Law', 2, N'sa', CAST(N'2017-03-23T17:52:02.533' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (39, 1, N'Sister ', N'Sister', 2, N'sa', CAST(N'2017-03-23T17:52:02.540' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (40, 1, N'Brother', N'Brother', 2, N'sa', CAST(N'2017-03-23T17:52:02.547' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (44, 1, N'2', N'Notice Period', 12, N'sa', NULL, NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (45, 1, N'Percentage', N'Percentage', 9, N'sa', CAST(N'2017-09-13T17:10:54.970' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (46, 1, N'GPI', N'GPI', 9, N'sa', CAST(N'2017-09-13T17:10:55.037' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (47, 1, N'CPI', N'CPI', 9, N'sa', CAST(N'2017-09-13T17:10:55.037' AS DateTime), NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (48, 1, N'Spouse', N'Spouse', 2, N'sa', NULL, NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (49, 1, N'Test Reason', N'Test Reason', 13, N'sa', NULL, NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (50, 1, N'Widowed', N'Widowed', 8, NULL, NULL, NULL, NULL)

--INSERT [dbo].[lkValue] ([ValueKey], [IsActive], [ValueID], [ValueName], [ValueTypeKey], [CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]) VALUES (51, 1, N'Divorced/Separated', N'Divorced/Separated', 8, NULL, NULL, NULL, NULL)

--SET IDENTITY_INSERT [dbo].[lkValue] OFF


----All Menus Data
--SET IDENTITY_INSERT [dbo].[AllMenus] ON 
--INSERT [dbo].[AllMenus] ([ID], [MenuId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (1, 16, N'sa', NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[AllMenus] OFF


----EmployeeType
--SET IDENTITY_INSERT [dbo].[EmployeeType] ON 

--INSERT [dbo].[EmployeeType] ([EmployeeTypeId], [EmployeeTypeCode], [EmployeeType], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [IsActive]) VALUES (1, N'FTE', N'FTE', N'sa', NULL, CAST(N'2016-04-15 13:11:44.877' AS DateTime), NULL, N'192.168.2.158  ', 1)

--INSERT [dbo].[EmployeeType] ([EmployeeTypeId], [EmployeeTypeCode], [EmployeeType], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [IsActive]) VALUES (2, N'Contractors', N'Contractors', N'sa', NULL, CAST(N'2016-04-15 13:11:44.880' AS DateTime), NULL, N'192.168.2.158  ', 1)

--SET IDENTITY_INSERT [dbo].[EmployeeType] OFF

---- Grades
--SET IDENTITY_INSERT [dbo].[Grades] ON 

--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (1, N'G1 - A', N'G1 - A', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.963' AS DateTime), CAST(N'2016-10-07 14:44:45.120' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (2, N'G1 - B', N'G1 - B', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.963' AS DateTime), CAST(N'2016-10-07 14:44:55.607' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (3, N'G2 - A', N'G2 - A', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.963' AS DateTime), CAST(N'2016-10-07 14:45:04.650' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (4, N'G2 - B', N'G2 - B', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.967' AS DateTime), CAST(N'2016-10-07 14:45:17.367' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (5, N'G3 - A', N'G3 - A', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.967' AS DateTime), CAST(N'2016-10-07 14:45:46.043' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (6, N'G3 - B', N'G3 - B', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.967' AS DateTime), CAST(N'2016-10-07 14:45:54.350' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (7, N'G4 - B', N'G4 - B', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-01 16:51:14.967' AS DateTime), CAST(N'2016-10-07 14:46:08.683' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (8, N'T', N'T', 1, N'sa', NULL, CAST(N'2016-07-01 16:51:14.967' AS DateTime), NULL, N'192.168.2.151  ')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (9, N'G4 - A', N'G4 - A', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-04 12:16:58.830' AS DateTime), CAST(N'2016-10-07 14:45:33.783' AS DateTime), N'192.168.2.148')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (10, N'C', N'Contractor Grade', 1, N'ajay.mali@senecaglobal.com', NULL, CAST(N'2016-11-25 16:02:24.180' AS DateTime), NULL, N'192.168.11.2')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (11, N'G5-B', N'G5-B', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01 11:10:10.900' AS DateTime), NULL, N'192.168.11.2')
--INSERT [dbo].[Grades] ([GradeId], [GradeCode], [GradeName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (12, N'G6', N'G6', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01 11:29:34.593' AS DateTime), NULL, N'192.168.11.2')
--SET IDENTITY_INSERT [dbo].[Grades] OFF

---- Departments
--SET IDENTITY_INSERT [dbo].[Departments] ON 
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (1, N'Technology & Delivery', 1, N'sa', N'kalyan.penumutchu@senecaglobal.com', CAST(N'2016-04-15 13:11:44.773' AS DateTime), CAST(N'2018-07-17 18:31:47.343' AS DateTime), N'192.168.11.2', N'Delivery', 300, 1)
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (2, N'Administration', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-04-15 13:11:44.820' AS DateTime), CAST(N'2017-09-29 13:43:42.570' AS DateTime), N'192.168.2.206', N'Admin', 186, 2)
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (3, N'IT Services', 1, N'sa', N'rajitha.chintalapani@senecaglobal.com', CAST(N'2016-04-15 13:11:44.827' AS DateTime), CAST(N'2018-07-17 18:08:41.503' AS DateTime), N'192.168.11.2', N'IT', 59, 2)
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (4, N'Finance', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-04-15 13:11:44.830' AS DateTime), CAST(N'2017-10-09 19:48:59.183' AS DateTime), N'192.168.2.206', N'FD', 172, 2)
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (5, N'Quality and Information Security', 1, N'sa', N'kalyan.penumutchu@senecaglobal.com', CAST(N'2016-04-15 13:11:44.837' AS DateTime), CAST(N'2018-06-20 16:39:36.760' AS DateTime), N'192.168.11.2', N'QA', 172, 2)
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (6, N'Human Resources', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-04-15 13:11:44.837' AS DateTime), CAST(N'2017-10-09 20:03:55.177' AS DateTime), N'192.168.2.206', N'HR', 172, 2)
--INSERT [dbo].[Departments] ([DepartmentId], [Description], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentCode], [DepartmentHeadID], [DepartmentTypeId]) VALUES (7, N'Training', 1, N'it@senecaglobal.com', N'it@senecaglobal.com', CAST(N'2017-07-21 12:16:12.207' AS DateTime), CAST(N'2017-10-04 16:37:51.317' AS DateTime), N'192.168.2.206', N'Training Department', 172, 2)
--SET IDENTITY_INSERT [dbo].[Departments] OFF


----Designation Data After adding Grades
--SET IDENTITY_INSERT [dbo].[Designations] ON 

--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (1, N'ST', N'Software Trainee', 1, N'sa', N'bhavani.chintamadaka@senecaglobal.com', CAST(N'2016-04-15T13:11:44.853' AS DateTime), CAST(N'2016-07-04T11:15:12.070' AS DateTime), N'192.168.2.157', 1)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (2, N'SE', N'Software Engineer', 1, N'sa', NULL, CAST(N'2016-04-15T13:11:44.860' AS DateTime), NULL, NULL, 1)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (3, N'SSE', N'Senior Software Engineer', 1, N'sa', NULL, CAST(N'2016-04-15T13:11:44.867' AS DateTime), NULL, NULL, 2)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (4, N'TL', N'Technical Lead', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-04-15T13:11:44.870' AS DateTime), CAST(N'2017-09-01T10:55:14.670' AS DateTime), N'192.168.11.2', 6)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (5, N'PM', N'Program Manager', 0, N'bhavani.chintamadaka@senecaglobal.com', N'bhavani.chintamadaka@senecaglobal.com', CAST(N'2016-07-01T12:05:45.743' AS DateTime), CAST(N'2016-07-01T12:05:53.793' AS DateTime), N'192.168.2.157', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (6, N'CL', N'Competency Lead', 0, N'bhavani.chintamadaka@senecaglobal.com', N'bhavani.chintamadaka@senecaglobal.com', CAST(N'2016-07-04T11:10:36.023' AS DateTime), CAST(N'2016-07-04T11:10:44.473' AS DateTime), N'192.168.2.157', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (7, N'PJM', N'Project Manager', 1, N'priyanka.tripurani@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-04T18:01:12.457' AS DateTime), CAST(N'2016-08-04T14:31:19.383' AS DateTime), N'192.168.2.148', 9)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (8, N'TA', N'Technical Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', NULL, CAST(N'2016-07-05T17:06:34.147' AS DateTime), NULL, N'192.168.2.101', 3)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (9, N'STA', N'Senior Technical Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:25:10.837' AS DateTime), N'192.168.2.148', 4)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (10, N'AR', N'Architect', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:25:26.427' AS DateTime), N'192.168.2.148', 6)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (11, N'AA', N'Associate Architect', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:27:33.223' AS DateTime), N'192.168.2.148', 5)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (12, N'AQL', N'Associate QA Lead', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:27:49.687' AS DateTime), N'192.168.2.148', 5)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (13, N'ATL', N'Associate Technical Lead', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:28:03.923' AS DateTime), N'192.168.2.148', 5)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (14, N'ITSE', N'IT Support Engineer', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:28:15.337' AS DateTime), N'192.168.2.148', 1)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (15, N'ITSL', N'IT Support Lead', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:28:32.750' AS DateTime), N'192.168.2.148', 3)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (16, N'PBA', N'Principal Business Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:28:41.860' AS DateTime), N'192.168.2.148', 5)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (17, N'PD', N'Project Director', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:28:49.383' AS DateTime), N'192.168.2.148', 7)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (18, N'QA', N'QA Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:28:55.403' AS DateTime), N'192.168.2.148', 3)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (19, N'QAA', N'QA Automation Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:29:11.070' AS DateTime), N'192.168.2.148', 3)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (20, N'QAE', N'QA Engineer', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:29:16.913' AS DateTime), N'192.168.2.148', 1)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (21, N'SITSE', N'Senior IT Support Engineer', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:29:31.533' AS DateTime), N'192.168.2.148', 2)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (22, N'SQA', N'Senior QA Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:29:41.223' AS DateTime), N'192.168.2.148', 4)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (23, N'SQAA', N'Senior QA Automation Analyst', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:29:52.183' AS DateTime), N'192.168.2.148', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (24, N'SQAAE', N'Senior QA Automation Engineer', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:30:02.400' AS DateTime), N'192.168.2.148', 2)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (25, N'SQE', N'Senior QA Engineer', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:30:18.373' AS DateTime), N'192.168.2.148', 2)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (26, N'SA', N'Solution Architect', 1, N'rajesh.arthimalla@senecaglobal.com', N'sarada.nath@senecaglobal.com', CAST(N'2016-07-05T17:06:34.147' AS DateTime), CAST(N'2016-08-04T14:30:24.993' AS DateTime), N'192.168.2.148', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (27, N'Consultant', N'Contractor', 1, N'ajay.mali@senecaglobal.com', NULL, CAST(N'2016-11-25T16:02:01.457' AS DateTime), NULL, N'192.168.11.2', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (28, N'SDBA', N'Senior Database Administrator', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-03-13T12:19:20.933' AS DateTime), NULL, N'192.168.11.2', 4)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (29, N'Database Administrator', N'Database Administrator', 0, N'it@senecaglobal.com', N'it@senecaglobal.com', CAST(N'2017-05-18T11:43:25.047' AS DateTime), CAST(N'2017-10-04T14:42:29.083' AS DateTime), N'192.168.2.206', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (30, N'Technical Manager', N'Technical Manager', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-08-24T17:02:10.150' AS DateTime), NULL, N'192.168.11.2', 9)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (31, N'Manager - IT', N'Manager - IT', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-08-28T14:09:48.057' AS DateTime), NULL, N'192.168.11.2', 5)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (32, N'Senior Cloud System Analyst', N'Senior Cloud System Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-08-28T14:10:04.583' AS DateTime), NULL, N'192.168.11.2', 4)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (33, N'STL', N'Senior Technical Lead', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01T10:50:06.750' AS DateTime), NULL, N'192.168.11.2', 9)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (34, N'UI', N'UI Specialist', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01T10:59:44.527' AS DateTime), NULL, N'192.168.11.2', 4)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (35, N'SQAL', N'Senior QA Lead', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01T11:02:41.773' AS DateTime), NULL, N'192.168.11.2', 9)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (36, N'AVPDelivery', N'Associate Vice President - Delivery', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01T11:08:52.280' AS DateTime), NULL, N'192.168.11.2', 11)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (37, N'Senior Executive - HR', N'Senior Executive - HR', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01T11:12:21.470' AS DateTime), NULL, N'192.168.11.2', 2)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (38, N'SSA', N'Senior System Administrator', 1, N'it@senecaglobal.com', N'it@senecaglobal.com', CAST(N'2017-09-01T11:21:23.543' AS DateTime), CAST(N'2017-09-01T11:22:27.290' AS DateTime), N'192.168.11.2', 2)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (39, N'HRManager', N'Manager-HR', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-09-01T11:25:51.130' AS DateTime), NULL, N'192.168.11.2', NULL)
--INSERT [dbo].[Designations] ([DesignationId], [DesignationCode], [DesignationName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [GradeId]) VALUES (40, N'SVP-Operation', N'Senior Vice President-Operation', 1, N'it@senecaglobal.com', N'it@senecaglobal.com', CAST(N'2017-09-01T11:28:54.323' AS DateTime), CAST(N'2017-09-01T11:30:48.753' AS DateTime), N'192.168.11.2', NULL)
--SET IDENTITY_INSERT [dbo].[Designations] OFF

----Project Managers table Data 
--SET IDENTITY_INSERT [dbo].[ProjectManagers] ON 

--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (1, 1, 214, 152, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-01-31T18:04:42.480' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (2, 2, 214, 40, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'haranath.pinnamaneni@senecaglobal.com', CAST(N'2016-11-10T17:23:06.513' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (3, 3, 102, 99, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-10T16:50:27.097' AS DateTime), N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (4, 4, 212, 169, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-04T16:16:35.987' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (5, 5, 102, 134, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-05-05T15:28:14.053' AS DateTime), N'192.168.9.184')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (6, 6, 214, 152, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-10T17:32:12.320' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (7, 7, 214, 152, 1, N'it@senecaglobal.com', CAST(N'2016-10-14T15:36:04.000' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-14T14:26:33.990' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (8, 8, 212, 30, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T17:02:13.137' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (9, 9, 214, 152, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T17:11:17.903' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (10, 10, 212, 43, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T17:14:12.877' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (11, 11, 214, 229, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T17:16:01.547' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-12-07T13:45:01.670' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (12, 12, 214, 47, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T18:16:17.297' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (13, 13, 214, 214, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T18:18:43.243' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-06-09T14:21:30.720' AS DateTime), N'192.168.9.184')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (14, 14, 212, 25, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-14T18:23:47.660' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-03T11:08:27.273' AS DateTime), N'192.168.2.158')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (15, 15, 214, 192, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T09:52:07.037' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-24T16:04:25.460' AS DateTime), N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (16, 16, 214, 16, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T09:55:19.440' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (17, 17, 214, 211, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T09:57:48.207' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (18, 18, 212, 172, 0, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:01:25.093' AS DateTime), N'sushmitha.kalluri@senecaglobal.com', CAST(N'2017-10-05T17:51:22.480' AS DateTime), NULL)
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (19, 19, 214, 278, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:06:27.003' AS DateTime), N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-07-25T15:47:20.237' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (20, 20, 214, 214, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:08:02.690' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (21, 21, 102, 102, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:11:16.873' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-01-31T18:05:38.127' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (22, 22, 212, 210, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:25:42.550' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (23, 23, 214, 214, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:28:21.537' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (24, 24, 102, 74, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:30:13.803' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (25, 25, 212, 211, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:32:37.387' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-05-15T16:47:53.467' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (26, 26, 212, 42, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:42:02.993' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (27, 27, 212, 212, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:43:12.030' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (28, 28, 215, 215, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:46:25.953' AS DateTime), N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-07-25T16:06:19.513' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (29, 29, 214, 214, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:49:07.547' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (30, 30, 212, 212, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T10:51:35.063' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-03-31T16:25:38.577' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (31, 31, 212, 150, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T11:32:19.507' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-05-03T14:30:13.677' AS DateTime), N'192.168.9.184')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (32, 32, 214, 154, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-17T11:46:35.793' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-02-24T16:56:30.137' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (33, 33, 212, 133, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-10-31T11:52:24.657' AS DateTime), NULL, NULL, N'192.168.9.155')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (34, 35, 212, 212, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-08T11:14:28.900' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-05-03T14:50:28.940' AS DateTime), N'192.168.9.184')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (35, 36, 244, 244, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2016-11-15T10:00:05.257' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-06-09T14:08:14.277' AS DateTime), N'192.168.9.184')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (36, 38, 212, 21, 1, N'haranath.pinnamaneni@senecaglobal.com', CAST(N'2016-12-21T17:42:08.730' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2016-12-22T13:06:23.193' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (37, 39, 244, 7, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2017-01-25T17:26:31.867' AS DateTime), NULL, NULL, N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (38, 40, 212, 212, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2017-03-23T10:50:46.647' AS DateTime), N'shivudu.maddi@senecaglobal.com', CAST(N'2017-06-09T14:25:10.147' AS DateTime), N'192.168.9.184')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (39, 41, 212, 101, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2017-04-05T15:43:46.533' AS DateTime), NULL, NULL, N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (40, 50, 213, NULL, 1, N'shivudu.maddi@senecaglobal.com', CAST(N'2017-04-13T11:11:44.837' AS DateTime), NULL, NULL, N'192.168.2.186')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (41, 52, 214, 214, 1, N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-06-27T12:28:22.137' AS DateTime), NULL, NULL, N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (42, 53, 212, 25, 1, N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-07-19T15:48:35.823' AS DateTime), NULL, NULL, N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (43, 54, 212, 307, 1, N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-07-28T16:49:14.870' AS DateTime), N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-08-28T15:01:48.847' AS DateTime), N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (44, 55, 212, 132, 1, N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-08-11T16:43:10.483' AS DateTime), NULL, NULL, N'192.168.11.2')
--INSERT [dbo].[ProjectManagers] ([ID], [ProjectID], [ReportingManagerID], [LeadID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (45, 56, 212, 244, 1, N'kalyan.penumutchu@senecaglobal.com', CAST(N'2017-08-24T16:55:05.747' AS DateTime), NULL, NULL, N'192.168.11.2')
--SET IDENTITY_INSERT [dbo].[ProjectManagers] OFF

----AssociateExitApprovalLevel table Data --
--INSERT INTO [AssociateExitApprovalLevel] (ApprovalLevel, ApprovedBy, IsActive) VALUES (1, 'Program Manager', 1)
--INSERT INTO [AssociateExitApprovalLevel](ApprovalLevel, ApprovedBy, IsActive) VALUES (2, 'Delivery Head', 1)


----CategoryMaster
--SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 

--INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (1, N'TalentRequisition', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:57:23.447' AS DateTime), CAST(N'2017-11-17T15:57:18.593' AS DateTime), N'192.168.2.208  ')

--INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (2, N'AssociateExit', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:57:23.447' AS DateTime), CAST(N'2017-11-17T15:57:18.593' AS DateTime), N'192.168.2.208  ')

--INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (3, N'Skills', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:57:23.447' AS DateTime), CAST(N'2017-11-17T15:57:18.593' AS DateTime), N'192.168.2.208  ')

--INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (4, N'EPC', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:57:23.447' AS DateTime), CAST(N'2017-11-17T15:57:18.593' AS DateTime), N'192.168.2.208  ')

--INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (5, N'KRA', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:57:23.447' AS DateTime), CAST(N'2017-11-17T15:57:18.593' AS DateTime), N'192.168.2.208  ')

--INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo]) VALUES (6, N'ADR', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:57:23.447' AS DateTime), CAST(N'2017-11-17T15:57:18.593' AS DateTime), N'192.168.2.208  ')

--SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF


----Status
--SET IDENTITY_INSERT [dbo].[Status] ON 

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (1, N'Approved', N'Approved', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 4)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (2, N'Pending', N'Pending', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 4)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (3, N'Rejected', N'Rejected', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 4)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (4, N'SubmittedForResignation', N'SubmittedForResignation', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (5, N'ApprovedByPM', N'ApprovedByPM', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (6, N'ApprovedByDeliveryHead', N'ApprovedByDeliveryHead', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (7, N'ApprovedByIT', N'ApprovedByIT', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (8, N'ApprovedByAdmin', N'ApprovedByAdmin', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (9, N'ApprovedByFinance', N'ApprovedByFinance', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (10, N'ApprovedByHRA', N'ApprovedByHRA', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (11, N'Rejected', N'Rejected', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (12, N'Resigned', N'Resigned', 1, N'santosh.bolisetty@senecaglobal.com', N'santosh.bolisetty@senecaglobal.com', CAST(N'2017-11-17T15:54:43.057' AS DateTime), CAST(N'2017-11-17T15:54:48.357' AS DateTime), N'192.168.2.208  ', NULL, 2)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (13, N'Draft', N'Draft', 1, N'ramya.singamsetti', N'hrms', CAST(N'2017-11-27T11:51:12.820' AS DateTime), NULL, N'192.168.2.71   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (14, N'SubmittedForApproval', N'SubmittedForApproval', 1, N'ramya.singamsetti', N'hrms', CAST(N'2017-11-27T11:52:30.860' AS DateTime), NULL, N'192.168.2.71   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (16, N'RejectedByDeliveryHead', N'RejectedByDeliveryHead', 1, N'hrms', N'hrms', CAST(N'2018-01-08T18:35:19.960' AS DateTime), NULL, N'192.168.2.27   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (17, N'SubmittedForFinanceApproval', N'SubmittedForFinanceApproval', 1, N'hrms', N'hrms', CAST(N'2018-01-11T11:58:12.023' AS DateTime), NULL, N'192.168.2.71   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (18, N'Approved', N'Approved', 1, N'hrms', N'hrms', CAST(N'2018-01-11T11:59:13.800' AS DateTime), NULL, N'192.168.2.71   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (19, N'RejectedByFinanceHead', N'RejectedByFinanceHead', 1, N'hrms', N'hrms', CAST(N'2018-01-11T12:08:00.637' AS DateTime), NULL, N'192.168.2.71   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (20, N'Close', N'Close', 1, N'hrms', N'hrms', CAST(N'2018-01-11T15:08:33.140' AS DateTime), NULL, N'192.168.2.27   ', NULL, 1)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (21, N'Draft', N'Draft', 1, N'hrms', N'hrms', CAST(N'2018-01-16T11:13:55.363' AS DateTime), CAST(N'2018-01-16T11:13:55.363' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (22, N'SubmittedForDepartmentHeadReview', N'Submitted For Department Head Review', 1, N'hrms', N'hrms', CAST(N'2018-01-16T11:14:53.460' AS DateTime), CAST(N'2018-01-16T11:14:53.460' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (23, N'SendBackForHRHeadReview', N'Send Back For HR Head Review', 1, N'hrms', N'hrms', CAST(N'2018-01-17T09:31:08.070' AS DateTime), CAST(N'2018-01-17T09:31:08.070' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (24, N'ApprovedByDepartmentHead', N'Approved By Department Head', 1, N'hrms', N'hrms', CAST(N'2018-02-14T09:57:46.710' AS DateTime), CAST(N'2018-01-17T09:31:08.070' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (25, N'Approved', N'Approved', 1, N'hrms', N'hrms', CAST(N'2018-02-14T09:59:15.073' AS DateTime), CAST(N'2018-02-14T09:59:15.073' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (26, N'Rejected', N'Rejected', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:00:02.223' AS DateTime), CAST(N'2018-02-14T10:00:02.223' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (27, N'SubmittedForMDApproval', N'Submitted For MD Approval', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:00:51.680' AS DateTime), CAST(N'2018-02-14T10:00:51.680' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (28, N'SubmittedForHRHeadReview', N'Submitted For HR Head Review', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:03:38.280' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (29, N'SendBackForHRAReview', N'Send Back For HRA Review', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:06:28.567' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (30, N'Assigned', N'Assigned KRAs', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:07:35.677' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (31, N'Closed', N'Closed KRAs', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:08:23.900' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 5)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (32, N'AdminReviewDone', N'AdminReviewDone', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:10:49.917' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, NULL)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (33, N'TrainingReviewDone', N'TrainingReviewDone', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:11:19.510' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, NULL)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (34, N'ProjectReviewDone', N'ProjectReviewDone', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:11:42.207' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, NULL)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (35, N'QualityReviewDone', N'QualityReviewDone', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:12:26.583' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, NULL)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (36, N'SumbmittedForDepartmentHeadApproval', N'Sumbmitted For Department Head Approval', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:12:26.583' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 6)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (37, N'ApprovedByDepartmentHead', N'Approved By Department Head', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:12:26.583' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 6)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (38, N'SubmittedForHRHeadApproval', N'Submitted For HR Head Approval', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:12:26.583' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 6)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (39, N'ApprovedByHRHead', N'Approved By HR Head', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:12:26.583' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 6)

--INSERT [dbo].[Status] ([StatusId], [StatusCode], [StatusDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [Category], [CategoryID]) VALUES (40, N'Draft', N'Draft', 1, N'hrms', N'hrms', CAST(N'2018-02-14T10:12:26.583' AS DateTime), CAST(N'2018-02-14T10:03:38.280' AS DateTime), N'192.168.2.208  ', NULL, 6)

--SET IDENTITY_INSERT [dbo].[Status] OFF



----Roles
--SET IDENTITY_INSERT [dbo].[Roles] ON 

--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (1, N'SystemAdmin', N'System Admin', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-04-15 13:11:45.090' AS DateTime), CAST(N'2016-10-17 11:41:54.467' AS DateTime), N'192.168.9.155', 3, N'<p>System admin<br/></p>', N'<p>Any bachelor degree<br/></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (2, N'Architect', N'Architect', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.450' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (3, N'Project Manager', N'Project Manager', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (4, N'QA Lead', N'QA Lead ', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (5, N'Technical Lead', N'Technical Lead', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-04 12:41:14.453' AS DateTime), CAST(N'2016-10-17 11:37:22.327' AS DateTime), N'192.168.9.155', 1, N'<p><font color="#000"><span style="background-color: rgb(255, 255, 255);">Lead</span></font><br/></p>', N'<p><span style="background-color: rgb(255, 255, 255);"><font color="#000">B.Tech/MCA</font></span><br/></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (6, N'Designer', N'Designer', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (7, N'Developer', N'Developer', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (8, N'Tester', N'Tester', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (9, N'IT Support Engineer', N'IT Support Engineer', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (10, N'IT Support Lead', N'IT Support Lead', 1, N'sa', N'it@senecaglobal.com', CAST(N'2016-07-04 12:41:14.453' AS DateTime), CAST(N'2016-12-07 18:04:10.237' AS DateTime), N'192.168.11.2', 1, N'<p>Helpdesk</p>', N'<p><font color="#000000">Degree</font></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (11, N'Program Manager', N'Program Manager', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (12, N'MD', N'Managing Director', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (13, N'HRM', N'HR Manager', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (14, N'HRA', N'HR Advisor', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (15, N'ITHead', N'IT Head', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (16, N'AdminHead', N'Admin Head', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (17, N'FinanceHead', N'Finance Head', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (18, N'Technical Manager', N'Technical Manager', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (19, N'Associate QA Lead', N'Associate QA Lead', 1, N'sa', NULL, CAST(N'2016-07-04 12:41:14.453' AS DateTime), NULL, N'192.168.2.158  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (20, N'Delivery Head', N'Delivery Head', 1, N'sa', NULL, CAST(N'2016-10-14 17:23:00.197' AS DateTime), NULL, N'192.168.9.155  ', NULL, NULL, NULL)
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (21, N'Senior Technical Lead', N'Senior Technical Lead', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:47:06.183' AS DateTime), NULL, N'192.168.2.148', 1, N'<p><br/>To Lead a team.<br/></p>', N'<p>B Tech<br/></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (22, N'Senior QA Analyst', N'Senior QA Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:50:20.277' AS DateTime), NULL, N'192.168.2.148', 1, N'<p><font color="#000000">Quality Assurance</font></p>', N'<p><font color="#000000">B Tech</font></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (23, N'Senior QA Lead', N'Senior QA Lead', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:51:11.367' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>To lead the QA team</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (24, N'QA Analyst', N'QA Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:52:01.833' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Writing test cases</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (25, N'Senior Technical Analyst', N'Senior Technical Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:53:02.313' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Provide technical support to the team.</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (26, N'Senior IT Support Engineer', N'Senior IT Support Engineer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:53:47.803' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>To support the application.</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (27, N'Senior Systems Administrator', N'Senior Systems Administrator', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:54:32.127' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>System Administration</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (28, N'Senior QA Engineer', N'Senior QA Engineer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:55:04.303' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>QA</p>', N'<p>Btech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (29, N'Technical Analyst', N'Technical Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:56:50.380' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>To provide technical support to the team</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (30, N'Senior Software Engineer', N'Senior Software Engineer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:57:42.310' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Software Engineering</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (31, N'Associate Technical lead', N'Associate Technical lead', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:58:37.817' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Tech lead</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (32, N'QA Engineer', N'QA Engineer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 09:59:21.733' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>QA</p>', N'<p>Btech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (33, N'Software Engineer', N'Software Engineer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:01:54.990' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>SE</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (34, N'QA Automation Analyst', N'QA Automation Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:02:35.767' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>QA</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (35, N'Associate Architect', N'Associate Architect', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:03:16.777' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Architect</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (36, N'Project Director', N'Project Director', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:03:53.723' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Project director</p>', N'<p>B tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (37, N'Principal Business Analyst', N'Principal Business Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:04:33.173' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>BA</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (38, N'Senior Technical Manager', N'Senior Technical Manager', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:05:35.607' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Manage a technical team</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (39, N'Senior QA Automation Analyst', N'Senior QA Automation Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:06:47.643' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>QA</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (40, N'Senior Project Manager', N'Senior Project Manager', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:07:28.687' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Managing Project</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (41, N'Senior QA Automation Engineer', N'Senior QA Automation Engineer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:08:01.410' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>QA</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (42, N'Senior Architect', N'Senior Architect', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:09:03.847' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Technical help</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (43, N'Technology Specialist', N'Technology Specialist', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:10:00.260' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Technical guidance.</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (44, N'IT Support Enginer', N'IT Support Enginer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:10:54.680' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>Support</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (45, N'Senior QA Automation Enginer', N'Senior QA Automation Enginer', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-10-17 10:11:31.757' AS DateTime), NULL, N'192.168.2.148', 1, N'<p>QA</p>', N'<p>B Tech</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (46, N'Practise Director', N'Practise Director', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-12-08 17:30:04.077' AS DateTime), NULL, N'192.168.11.2', 1, N'<p><br/><span style="color: rgb(34, 34, 34);text-align: left;background-color: rgb(255, 255, 255);float: none;">Responsible for the particular technology</span><!--EndFragment--><br/><br/><br/></p>', N'<p>MS</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (47, N'Consultant', N'Contractor', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-12-14 18:18:59.957' AS DateTime), NULL, N'192.168.11.2', 1, N'<p><font color="#000000"> <br/><br/><!--StartFragment--><span style="color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);float: none;">Contractor</span><!--EndFragment--><br/><br/></font></p>', N'<p>Contractor</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (48, N'Senior Executive - PMO', N'PMO', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-12-21 17:01:42.680' AS DateTime), NULL, N'192.168.11.2', 1, N'<p><br/><br/><!--StartFragment--><span style="color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);float: none;">PMO</span><!--EndFragment--><br/><br/><br/></p>', N'<p><br/><br/><!--StartFragment--><span style="color: rgb(0, 0, 0);background-color: rgb(255, 255, 255);float: none;">PMO</span><!--EndFragment--><br/><br/><br/></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (49, N'Senior Cloud Analyst', N'Senior Cloud Analyst', 1, N'it@senecaglobal.com', NULL, CAST(N'2016-12-21 17:18:22.713' AS DateTime), NULL, N'192.168.11.2', 1, N'<p>Senior ClouSenior Cloud Analystd Analyst<br/></p>', N'<p>Senior Cloud Analyst<br/></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (50, N'Release Engineer', N'Release Engineer', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'Release Engineer', N'Release Engineer')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (51, N'System Admin Lead', N'System Admin Lead', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (52, N'HR Head', N'HR Head', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'HR Head', N'HR Head')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (53, N'Lead', N'Lead', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (54, N'Manager', N'Manager', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (55, N'QA Manager', N'QA Manager', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (56, N'Competency Leader', N'Competency Leader', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (57, N'Requirements Analyst', N'Requirements Analyst', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N' ', N' ')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (58, N'Configuration Controller', N'Configuration Controller', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (59, N'Technical Writer', N'Technical Writer', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 1, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (60, N'IT Support Analyst', N'IT Support Analyst', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 3, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (61, N'System Administrator', N'System Administrator', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 3, N' ', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (62, N'IT Services Manager', N'IT Services Manager', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 3, N'  ', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (63, N'IT Services Lead', N'IT Services Lead', 1, N'sa', NULL, CAST(N'2017-03-17 11:11:20.733' AS DateTime), NULL, N'192.168.2.158', 3, N'', N'')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (64, N'FinanceExecutive', N'Finance Executive', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-04-06 11:04:53.857' AS DateTime), NULL, N'192.168.11.2', 4, N'<p><font color="#000000">  </font></p>', N'<p>Graduation</p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (65, N'Senior Database Administrator', N'Senior Database Administrator', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-07-27 11:49:07.330' AS DateTime), NULL, N'192.168.11.2', 1, N'<p>Database Administration, Back up, Restore and Performance Tuning</p>', N'<p><font color="#000000">B.Tech, M.Tech, M.S</font></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (66, N'Executive - PMO', N'Executive - PMO', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-10-30 13:45:35.513' AS DateTime), NULL, N'192.168.11.2', 1, N'<p><font color="#000000">Technical Writer</font></p>', N'<p><font color="#000000">B.Tech</font></p>')
--INSERT [dbo].[Roles] ([RoleId], [RoleName], [RoleDescription], [IsActive], [CreatedUser], [ModifiedUser], [CreatedDate], [ModifiedDate], [SystemInfo], [DepartmentId], [KeyResponsibilities], [EducationQualification]) VALUES (67, N'Principal Technology Leader', N'Principal Technology Leader', 1, N'it@senecaglobal.com', NULL, CAST(N'2017-11-02 15:16:35.880' AS DateTime), NULL, N'192.168.11.2', 1, N'<p>Principal Technology Leader<br/></p>', N'<p><font color="#000000">B.Tech</font></p>')
--SET IDENTITY_INSERT [dbo].[Roles] OFF

--NotificationType
--GO
--SET IDENTITY_INSERT [dbo].[NotificationType] ON 

--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (1, N'IT', N'IT Department Notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (2, N'EPC', N'Emp Profile Creation Notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (3, N'Finance', N'Finance Department Notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (4, N'Admin', N'Admin Department Notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (5, N'PA', N'Prospective Associate Joining Notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (6, N'Allocation', N'AssociateAllocation')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (7, N'TRSbmitForApproval', N'Talent Requisition submitted for Approval')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (8, N'TRApproved', N'Talent Requisition Approved')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (9, N'TRRejected', N'Talent Requisition Rejected')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (10, N'KRA', N'KRA Notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (11, N'KRARejected', N'KRA Reject notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (12, N'KRAReview', N'KRA Review notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (13, N'KRAApprove', N'KRA Approve notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (14, N'KRAAprroved', N'KRA Aprroved notification')
--GO
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc]) VALUES (15, N'TagTAPositions', N'Tagged TA Positions toTalent Requisition')
--GO
--SET IDENTITY_INSERT [dbo].[NotificationType] OFF
--GO
--SET IDENTITY_INSERT [dbo].[NotificationConfiguration] ON 

----NotificationConfiguration
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo],[CategoryId]) VALUES (1, 6, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Resource Allocation to @ProjectName Project - @AssociateName', N'<p><span>Dear All,</span></p><p><span></span></p><p><span>@AssociateName has been allocated to @ProjectName Project effective from @EffectiveDate. <span style="color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);">@AssociateName </span>would report to @ReportingManager.</span></p><p><span></span></p><p><span>We request the current project lead/manager to raise service tickets (IT and Admin) for desk allocations, project related folder access, software installations, changes in the email groups, etc.</span></p><p><span></span></p><p><span>Thanks!</span><span></span></p><p><span><br/></span></p><p><span><font color="#f91b05">Disclaimer : This is an auto generated email.</font><br/></span></p>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL,NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo],[CategoryId]) VALUES (2, 7, N'it@senecaglobal.com', N'sushmitha.kalluri@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Talent requisiton {TRCode} submitted for approval - {ProjectName} ', N'<p>Dear {ApprovedBy},</p><p>Talent Requisition <strong>{TRCode}</strong> raised for the project <strong>{ProjectName}</strong> has been submitted for approval.<br/></p><p>Below are the details of talent requisition:<br/></p><table style = "border-collapse: collapse;"><tr><td style = "border: 1px solid black">TRCode:</td><td style = "border: 1px solid black">{TRCode}</td></tr><tr><td style = "border: 1px solid black">SubmittedBy:</td><td style = "border: 1px solid black">{SubmittedBy}</td></tr><tr><td  style = "border: 1px solid black">Role(s):</td><td  style = "border: 1px solid black">{RoleName}</td></tr></table><br/><div>  <span style="color: rgb(0,0,0);"><strong>DISCLAIMER</strong> - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must<br/> not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL,NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo],[CategoryId]) VALUES (3, 8, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Talent requisiton @TRCode approved - @ProjectName - @RoleName', N'<p>Dear @CreatedBy,</p><p>Talent Requisition <strong>@TRCode</strong> raised for the project <strong>@ProjectName</strong> and for the Role <strong>@RoleName</strong> has been approved successfully.<br/></p><p><br/></p><div>  <span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL,NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo],[CategoryId]) VALUES (4, 9, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Talent requisition @TRCode rejected - @ProjectName - @RoleName', N'<p>Dear @CreatedBy,</p><p>Talent Requisition <strong>@TRCode</strong> raised for the project <strong>@ProjectName</strong> and for the Role <strong>@RoleName</strong> has been rejected.<br/></p><p><br/></p><div>  <span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL,NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo],[CategoryId]) VALUES (5, 14, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'KRA Approved', N'Dear <AssociateName>,

--           KRA has been approved for the role <RoleName>.   
--<br>
--<div style="border: 2px dotted rgb(0, 51, 51);">  
--<span style="font-size:12pt;  font-family: ''Cambria'',''times new roman'',''garamond'',serif; color:#ff0000;">Disclaimer : This is an auto generated email.</span>
--</div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (7, 11, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'KRA Rejected', N'Dear <AssociateName>, <br/> KRA has been rejected for the role <RoleName>. Please find the remarks and change the KRA as required. <br/><br/>  <div style="border: 2px dotted rgb(0, 51, 51);">    <span style="font-size:12pt;  font-family: Cambria,times new roman,garamond,serif; color:#ff0000;">Disclaimer : This is an auto generated email.</span>  </div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (8, 13, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'KRA Submitted for Approval', N'Dear <AssociateName>, <br/> KRA has been submitted for approval. <br/><br/>  <div style="border: 2px dotted rgb(0, 51, 51);">    <span style="font-size:12pt;  font-family: Cambria,times new roman,garamond,serif; color:#ff0000;">Disclaimer : This is an auto generated email.</span>  </div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (15, 15, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Talent requisition @TRCode tagged - @ProjectName - @RoleName', N'<p>Dear @CreatedBy,</p><p>Talent Requisition <strong>@TRCode</strong> raised for the project <strong>@ProjectName</strong> and for the Role <strong>@RoleName</strong> has been tagged successfully for <strong>@TAPositions</strong> Position(s).<br/></p><p><br/></p><div>  <span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (16, 1, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'IT Notification of new associate - AssociateId :', N'<p>Hi Team,<br/></p><p>  @AssociateName has joined the organization today. Please create necessary user credentials. Following are the details for your attention:<br/></p><p> Designation :  @Designation<br/></p><p> Department :  @Department<br/></p><p>Mobile:  @Mobile</p><p><br/></p><div><span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email..</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (17, 2, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Employee Profile Creation Notification of new associate - AssociateId :', N'<p>Hi,</p><p> The associate profile of {AssociateName} is pending for your approval. Following are the details for your attention:</p><p>Associate Name : {AssociateName}</p><p> Designation : {Designation}</p><p> Department : {Department}</p><p><br/></p><div><span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (18, 3, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Finance Notification of new associate - AssociateId :', N'<p>Hi Team,</p><p><br/></p><p>  @AssociateName has joined the organization today. Please create Greytip account and PF account for the associate. Following are the details for your attention:</p><p> Designation : @Designation</p><p> Department : @Department</p><p>Mobile: @Mobile</p><p><br/></p><p><br/></p><div><span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (19, 4, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Admin Notification of new associate - AssociateId :', N'<p>Hi Team,<br/></p><p>  @AssociateName has joined the organization today. Please create Biometric registration and allocate desk for the associate. Following are the details for your attention:<br/></p><p> Designation :  @Designation<br/></p><p> Department :  @Department<br/></p><p>Mobile:  @Mobile</p><p><br/></p><div><span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (20, 5, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Prospective Associate Joining Notification', N'<p>Hi All,</p><p>Below is the list of Prospective Associates joining this month.</p><p><br/><br/><!--StartFragment--><span style="color: rgb(255, 0, 0);float: none;background-color: rgb(255, 255, 255);">Disclaimer : This is an auto generated email.</span><!--EndFragment--><br/></p>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (21, 12, N'it@senecaglobal.com', N'sreedhar.chemikala@senecaglobal.com;sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'KRA Submitted for Review', N'<p>Dear , </p><p><br/></p><p> KRA has been submitted for review. Please review and approve.         </p><p><br/></p><p><br/></p><div><span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span> </div>', 240, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--GO
--SET IDENTITY_INSERT [dbo].[NotificationConfiguration] OFF
--GO
--SET IDENTITY_INSERT [dbo].[MenuMaster] ON 

----MenuMaster
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (1, N'Associates', 1, N'', 2, 0, N'sa', NULL, CAST(N'2017-02-20T15:46:59.290' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liAssociates', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (2, N'Talent Management', 1, N'', 3, 0, N'sa', NULL, CAST(N'2017-02-20T15:46:59.333' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liTalentManagement', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (3, N'Team Management', 1, N'', 4, 0, N'sa', NULL, CAST(N'2017-02-20T15:46:59.333' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liTeamManagement', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (4, N'Performance management', 1, N'', 6, 0, N'sa', NULL, CAST(N'2017-02-20T15:46:59.333' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liPerformanceManagement', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (5, N'Reports', 1, N'', 7, 0, N'sa', NULL, CAST(N'2017-02-20T15:46:59.333' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liReports', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (6, N'Admin', 1, N'', 8, 0, N'sa', NULL, CAST(N'2017-02-20T15:46:59.333' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liAdmin', N'fa fa-street-view')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (7, N'Prospective Associates', 1, N'/ap/associates/view', 1, 1, N'sa', NULL, CAST(N'2017-02-20T16:00:56.860' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liProspectiveAssociate', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (8, N'Associate Joining', 1, N'/ap/associates/prospective-associates', 2, 1, N'sa', NULL, CAST(N'2017-02-20T16:00:56.860' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liAssociateJoining', N'fa fa-pencil-square')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (9, N'Associates Information', 1, N'/ap/associates/list', 3, 1, N'sa', NULL, CAST(N'2017-02-20T16:00:56.860' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liAssociatesChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (10, N'Talent Requisition', 1, N'/ap/talentmanagement/talentrequisition-history', 1, 2, N'sa', NULL, CAST(N'2017-02-20T16:06:13.987' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liTalentRequisitionChild', N'fa fa-user-plus')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (11, N'Associate Allocation', 1, N'/ap/talentmanagement/associateallocation', 3, 2, N'sa', NULL, CAST(N'2017-02-20T16:06:13.987' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liAssociateAllocationChild', N'fa fa-user')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (12, N'Associate Release', 1, N'/ap/talentmanagement/resourcerelease', 3, 2, N'sa', NULL, CAST(N'2017-02-20T16:06:13.990' AS DateTime), NULL, N'192.168.9.184  ', N'', N'', N'fa fa-user')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (13, N'Resource Report', 1, N'/ap/reports/resourcereport', 1, 5, N'sa', NULL, CAST(N'2017-02-21T16:30:24.993' AS DateTime), NULL, N'192.168.2.211  ', N'', N'liResourceReportChild', N'fa fa-user-plus')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (14, N'Finance Report', 1, N'/ap/reports/financereport', 2, 5, N'sa', NULL, CAST(N'2017-02-21T16:31:18.107' AS DateTime), NULL, N'192.168.2.211  ', N'', N'liFinanceReportChild', N'fa fa-user-plus')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (15, N'Import RMG Report', 1, N'/ap/reports/importRMGreport', 3, 5, N'sa', NULL, CAST(N'2017-02-21T16:32:30.177' AS DateTime), NULL, N'192.168.2.211  ', N'', N'liImportRMGReportChild', N'fa fa-pencil-square')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (16, N'Dashboard', 1, N'/ap/dashboard', 1, 0, N'sa', NULL, CAST(N'2017-02-28T11:06:57.950' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liDashboard', N'fa fa-home')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (17, N'Projects', 1, N'/ap/talentmanagement/projectsview', 18, 2, N'sa', NULL, CAST(N'2017-02-28T11:06:58.150' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liProjectsChild', N'fa fa-pencil-square')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (18, N'Project Role Assignment', 1, N'/ap/projectmanagement/projectroleassignment', 1, 3, N'sa', NULL, CAST(N'2017-02-28T11:06:58.447' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liProjectRoleAssignmentChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (19, N'Competency Assessment', 1, N'/ap/projectmanagement/skillgapassesment', 2, 3, N'sa', NULL, CAST(N'2017-02-22T14:20:07.883' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liSkillGapAssessmentChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (20, N'Project Trainings', 1, N'/ap/projectmanagement/projecttrainings', 3, 3, N'sa', NULL, CAST(N'2017-02-22T14:22:01.473' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liProjectTrainingsChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (21, N'Associate Feedback', 1, N'/ap/projectmanagement/associatefeedbackview', 4, 3, N'sa', NULL, CAST(N'2017-02-22T14:22:56.327' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liAssociateFeedbackChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (22, N'KRA Definition', 1, N'/ap/kra/kradefinition', 1, 4, N'sa', NULL, CAST(N'2017-02-22T14:24:22.850' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liKRADefinitionChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (23, N'KRA Review', 1, N'/ap/kra/krareview', 2, 4, N'sa', NULL, CAST(N'2017-02-22T14:25:20.593' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liKRAReviewChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (24, N'KRA Approval', 1, N'/ap/kra/kraapproval', 3, 4, N'sa', NULL, CAST(N'2017-02-22T14:26:43.330' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liKRAApprovalChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (25, N'KRA Assignment', 1, N'/ap/kra/kraassignment', 4, 4, N'sa', NULL, CAST(N'2017-02-22T14:27:33.237' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liKRAAssignmentChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (26, N'Assign User Roles', 1, N'/ap/admin/userrole', 1, 6, N'sa', NULL, CAST(N'2017-02-22T14:35:22.917' AS DateTime), NULL, N'192.168.2.48   ', N'userrole', N'liAssignUserRolesChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (27, N'Business Rules', 1, N'/ap/admin/lookup/businessrules', 2, 6, N'sa', NULL, CAST(N'2017-02-22T14:37:29.947' AS DateTime), NULL, N'192.168.2.48   ', N'businessrules', N'liBusinessRulesChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (28, N'Clients', 1, N'/ap/admin/lookup/clients', 4, 6, N'sa', NULL, CAST(N'2017-02-22T14:38:17.583' AS DateTime), NULL, N'192.168.2.48   ', N'clients', N'clientsId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (29, N'Competency Area', 1, N'/ap/admin/lookup/competencyarea', 5, 6, N'sa', NULL, CAST(N'2017-02-22T14:40:22.573' AS DateTime), NULL, N'192.168.2.48   ', N'competencyarea', N'competencyAreaId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (30, N'Departments', 1, N'/ap/admin/lookup/department', 6, 6, N'sa', NULL, CAST(N'2017-02-22T14:41:06.930' AS DateTime), NULL, N'192.168.2.48   ', N'department', N'liDepartmentsChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (31, N'Designations', 1, N'/ap/admin/lookup/designation', 7, 6, N'sa', NULL, CAST(N'2017-02-22T14:42:11.660' AS DateTime), NULL, N'192.168.2.48   ', N'designation', N'designationId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (32, N'Grades', 1, N'/ap/admin/lookup/grade', 8, 6, N'sa', NULL, CAST(N'2017-02-22T14:42:59.553' AS DateTime), NULL, N'192.168.2.48   ', N'grade', N'gradesId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (33, N'KRA Aspects', 1, N'/ap/admin/lookup/kraAspects', 9, 6, N'sa', NULL, CAST(N'2017-02-22T14:44:03.050' AS DateTime), NULL, N'192.168.2.48   ', N'kraAspects', N'liKRAAspectsChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (34, N'Map Associate Id', 1, N'/ap/admin/mapassociateid', 10, 6, N'sa', NULL, CAST(N'2017-02-22T14:44:54.930' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liMapAssociateIdChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (35, N'Notification Email', 1, N'/ap/admin/notificationemail', 12, 6, N'sa', NULL, CAST(N'2017-02-22T14:45:44.120' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liNotificationEmailChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (36, N'Practice Area', 1, N'/ap/admin/lookup/practicearea', 13, 6, N'sa', NULL, CAST(N'2017-02-22T14:46:53.437' AS DateTime), NULL, N'192.168.2.48   ', N'practicearea', N'liPracticeAreaChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (37, N'Proficiency Level', 1, N'/ap/admin/lookup/proficiencylevel', 14, 6, N'sa', NULL, CAST(N'2017-02-22T14:48:20.083' AS DateTime), NULL, N'192.168.2.48   ', N'proficiencylevel', N'proficiencyLevelId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (38, N'Project Type', 1, N'/ap/admin/lookup/projecttype', 15, 6, N'sa', NULL, CAST(N'2017-02-22T14:48:58.900' AS DateTime), NULL, N'192.168.2.48   ', N'projecttype', N'projectTypeId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (39, N'Roles', 1, N'/ap/admin/lookup/role', 16, 6, N'sa', NULL, CAST(N'2017-02-22T14:49:44.177' AS DateTime), NULL, N'192.168.2.48   ', N'role', N'liRolesChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (40, N'Skills', 1, N'/ap/admin/skills', 17, 6, N'sa', NULL, CAST(N'2017-02-22T14:50:25.823' AS DateTime), NULL, N'192.168.2.48   ', N'skills', N'liSkillsChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (41, N'Status', 0, N'/ap/admin/lookup/status', 18, 6, N'sa', NULL, CAST(N'2017-02-22T14:51:08.617' AS DateTime), NULL, N'192.168.2.48   ', N'status', N'statusId', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (42, N'Update Employee Status', 1, N'/ap/admin/employeestatus', 19, 6, N'sa', NULL, CAST(N'2017-02-22T14:52:09.497' AS DateTime), NULL, N'192.168.2.48   ', N'', N'liUpdateEmployeeStatusChild', N'')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (43, N'Project Associate Trainings', 1, N'/ap/projectmanagement/projecttrainings', 5, 3, N'sa', NULL, CAST(N'2017-02-24T12:03:44.120' AS DateTime), NULL, N'192.168.2.157  ', N'', N'liProjectAssociatesxyz', N'fa fa-user-plus')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (44, N'View KRA', 1, N'/ap/kra/viewKRA/username', 5, 4, N'hrms', NULL, CAST(N'2017-03-02T16:13:10.170' AS DateTime), NULL, N'192.168.9.184  ', N'', N'liViewKRAChild', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (45, N'Team Members Information', 1, N'/ap/projectmanagement/teammembersinformation', 5, 4, N'sa', NULL, CAST(N'2017-03-14T12:21:26.217' AS DateTime), NULL, N'192.168.2.171  ', NULL, N'liTeamMembersInformationChild', N'fa fa-user-plus')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (46, N'Associates Exit', 1, N'/ap/associateexit', 8, 0, N'sa', NULL, CAST(N'2017-09-14T12:41:33.790' AS DateTime), NULL, N'192.168.2.27   ', NULL, N'liAssociateExit', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (48, N'Associate Resignation', 1, N'/ap/associateexit/associateresignation', 8, 46, N'sa', NULL, CAST(N'2017-09-14T12:44:39.430' AS DateTime), NULL, N'192.168.2.27   ', NULL, N'liAssociateResignation', N'fa fa-users')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (49, N'KT Plan', 1, NULL, 3, 0, N'hrms', NULL, CAST(N'2017-09-14T12:48:41.880' AS DateTime), NULL, N'192.168.2.27   ', NULL, NULL, NULL)
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (54, N'Test1', 1, NULL, 4, 1, N'kalyan.penumutchu@senecaglobal.com', NULL, CAST(N'2017-09-22T12:02:14.217' AS DateTime), CAST(N'2017-09-22T12:02:14.217' AS DateTime), NULL, NULL, NULL, NULL)
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (56, N'Assign Manager To Project', 1, N'/ap/project/assignreportingmanager', 1, 58, N'sunitha.kanumuri@senecaglobal.com', NULL, CAST(N'2017-10-06T12:32:48.767' AS DateTime), CAST(N'2017-10-06T12:32:48.767' AS DateTime), N'192.168.2.206  ', NULL, NULL, N'fa fa-user')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (58, N'Project', 1, N'', 5, 0, N'sunitha.kanumuri@senecaglobal.com', NULL, CAST(N'2017-10-17T17:46:31.927' AS DateTime), CAST(N'2017-10-17T17:46:31.927' AS DateTime), N'192.168.2.209  ', NULL, N'liProject', N'fa fa-suitcase')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (64, N'Associate Promotions', 1, N'/ap/talentmanagement/associatepromotion', 3, 2, N'sunitha.kanumuri@senecaglobal.com', NULL, CAST(N'2017-10-23T14:41:14.630' AS DateTime), CAST(N'2017-10-23T14:41:14.630' AS DateTime), N'192.168.2.209  ', NULL, N'liAssociatePromotions', N'fa fa-user')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (68, N'Assign Manager To Associate', 1, N'/ap/talentmanagement/assignreportingmanager', 3, 2, N'sunitha.kanumuri@senecaglobal.com', NULL, CAST(N'2017-10-24T12:19:51.180' AS DateTime), CAST(N'2017-10-24T12:19:51.180' AS DateTime), N'192.168.2.209  ', NULL, N'', N'fa fa-user')
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (71, N'Category', 0, N'/ap/admin/lookup/category', 3, 6, N'sa', NULL, CAST(N'2017-11-06T19:21:13.973' AS DateTime), CAST(N'2017-11-06T19:21:13.973' AS DateTime), N'192.168.2.206  ', NULL, NULL, NULL)
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (75, N'Skill Group ', 1, N'/ap/admin/skillgroup', 17, 6, N'sa', NULL, CAST(N'2017-11-08T17:44:31.593' AS DateTime), CAST(N'2017-11-08T17:44:31.593' AS DateTime), N'192.168.2.206  ', NULL, NULL, NULL)
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (76, N'Profile Match', 0, N'/ap/talentmanagement/profilematch', 3, 2, N'sa', NULL, CAST(N'2017-12-15T00:00:00.000' AS DateTime), CAST(N'2017-12-15T14:19:08.647' AS DateTime), N'192.168.2.71   ', NULL, NULL, NULL)
--GO
--INSERT [dbo].[MenuMaster] ([MenuId], [Title], [IsActive], [Path], [DisplayOrder], [ParentId], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [SystemInfo], [Parameter], [NodeId], [Style]) VALUES (77, N'Notification Type', 1, N'/ap/admin/notificationtype', 11, 6, N'sa', NULL, CAST(N'2018-01-02T16:56:27.780' AS DateTime), CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206  ', NULL, NULL, NULL)
--GO
--SET IDENTITY_INSERT [dbo].[MenuMaster] OFF
--GO

--KRAType
--SET IDENTITY_INSERT [dbo].[KRAType] ON 

--INSERT [dbo].[KRAType] ([KRATypeID], [KRAType]) VALUES (1, N'Organizational KRAs')
--GO
--INSERT [dbo].[KRAType] ([KRATypeID], [KRAType]) VALUES (2, N'Additional KRAs')
--GO
--SET IDENTITY_INSERT [dbo].[KRAType] OFF
--GO

--SGROLES 
--SET IDENTITY_INSERT [dbo].[SGRole] ON 

--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (1, N'RoleTest', N'sa', CAST(N'2018-02-12T16:59:10.047' AS DateTime), N'192.168.1.2', 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (2, N'Developer', NULL, CAST(N'2018-02-15T10:47:26.293' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (3, N'Tester', NULL, CAST(N'2018-02-15T10:47:35.013' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (4, N'Architect', NULL, CAST(N'2018-02-15T10:47:43.863' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (5, N'Technical Manager', NULL, CAST(N'2018-02-21T13:55:59.713' AS DateTime), N'192.168.2.174', 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (8, N'Analyst', N'kalyan', CAST(N'2018-02-21T15:46:38.930' AS DateTime), N'192.168.2.174', 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (9, N'Designer', N'sunitha.kanumuri@senecaglobal.com', CAST(N'2018-02-21T16:38:26.760' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (10, N'Technical Lead', NULL, CAST(N'2018-03-12T14:52:00.837' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (11, N'Release Engineer', NULL, CAST(N'2018-04-09T10:59:47.730' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (12, N'Technical Writer', NULL, CAST(N'2018-04-09T11:01:01.943' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (13, N'Database Administrator', NULL, CAST(N'2018-04-09T11:31:38.797' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (14, N'QA Lead', NULL, CAST(N'2018-04-09T11:31:55.300' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (15, N'QA Manager', NULL, CAST(N'2018-04-09T11:32:06.110' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (16, N'DevOps Engineer', NULL, CAST(N'2018-04-09T11:32:18.553' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (17, N'DevOps Tester', NULL, CAST(N'2018-04-09T11:32:52.577' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (18, N'SysOps Engineer', NULL, CAST(N'2018-04-09T11:32:58.867' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (19, N'Cloud Engineer', NULL, CAST(N'2018-04-09T11:33:11.497' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (20, N'Cloud Architect', NULL, CAST(N'2018-04-09T11:33:22.553' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (21, N'Discovery Engineer', NULL, CAST(N'2018-04-09T11:33:34.533' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (22, N'Lead Cloud Services', NULL, CAST(N'2018-04-09T11:33:45.023' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (23, N'IT Support Engineer', NULL, CAST(N'2018-04-09T11:33:55.760' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (24, N'Lead IT Support ', NULL, CAST(N'2018-04-09T11:34:05.140' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (25, N'Lead System Administrator', NULL, CAST(N'2018-04-09T11:35:45.860' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (26, N'IT Support Analyst', NULL, CAST(N'2018-04-09T11:35:55.963' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (27, N'Information Technology Services Manager', NULL, CAST(N'2018-04-09T11:36:19.917' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (28, N'Program Manager', NULL, CAST(N'2018-04-09T11:36:31.613' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (29, N'Principal Technology Leader ', NULL, CAST(N'2018-04-09T11:36:42.860' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (30, N'Head of Delivery', NULL, CAST(N'2018-04-09T11:36:53.217' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (31, N'Managing Director', NULL, CAST(N'2018-04-09T11:37:05.893' AS DateTime), NULL, 1)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (33, N'Recruiter', NULL, CAST(N'2018-04-09T11:37:55.280' AS DateTime), NULL, 6)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (34, N'Human Resources Advisor', NULL, CAST(N'2018-04-09T11:38:08.280' AS DateTime), NULL, 6)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (35, N'Payroll and Compliance Administrator', NULL, CAST(N'2018-04-09T11:38:19.873' AS DateTime), NULL, 6)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (36, N'Lead, Talent Acquisition', NULL, CAST(N'2018-04-09T11:38:30.943' AS DateTime), NULL, 6)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (37, N'Lead, Human Resources', NULL, CAST(N'2018-04-09T11:38:40.850' AS DateTime), NULL, 6)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (38, N'Head, Human Resources', NULL, CAST(N'2018-04-09T11:38:54.287' AS DateTime), NULL, 6)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (39, N'Accounting & Compliance Executive', NULL, CAST(N'2018-04-09T11:42:18.043' AS DateTime), NULL, 4)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (40, N'Accounting & Compliance - Manager', NULL, CAST(N'2018-04-09T11:42:23.360' AS DateTime), NULL, 4)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (41, N'Head Finance & Accounts', NULL, CAST(N'2018-04-09T11:42:39.740' AS DateTime), NULL, 4)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (42, N'Training Coordinator', NULL, CAST(N'2018-04-09T11:43:10.077' AS DateTime), NULL, 7)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (43, N'Lead Training', NULL, CAST(N'2018-04-09T11:43:22.120' AS DateTime), NULL, 7)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (44, N'Head Training', NULL, CAST(N'2018-04-09T11:43:39.473' AS DateTime), NULL, 7)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (45, N'System Engineer', NULL, CAST(N'2018-04-09T11:43:59.623' AS DateTime), NULL, 3)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (46, N'System Administrator', NULL, CAST(N'2018-04-09T11:44:10.543' AS DateTime), NULL, 3)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (47, N'Network Administrator', NULL, CAST(N'2018-04-09T11:44:21.840' AS DateTime), NULL, 3)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (48, N'Lead IT Services', NULL, CAST(N'2018-04-09T11:44:33.273' AS DateTime), NULL, 3)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (50, N'Manager IT Services', NULL, CAST(N'2018-04-09T11:44:51.197' AS DateTime), NULL, 3)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (51, N'Head IT Services', NULL, CAST(N'2018-04-09T11:45:02.417' AS DateTime), NULL, 3)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (52, N'Process Advisor', NULL, CAST(N'2018-04-09T11:45:24.133' AS DateTime), NULL, 5)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (53, N'Lead Quality and Information Security', NULL, CAST(N'2018-04-09T11:45:35.233' AS DateTime), NULL, 5)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (54, N'Information Security Officer', NULL, CAST(N'2018-04-09T11:45:46.400' AS DateTime), NULL, 5)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (55, N'Head Quality and Information Security', NULL, CAST(N'2018-04-09T11:45:57.717' AS DateTime), NULL, 5)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (56, N'Administration Coordinator', NULL, CAST(N'2018-04-09T11:46:15.120' AS DateTime), NULL, 2)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (57, N'Front Office Coordinator', NULL, CAST(N'2018-04-09T11:46:26.743' AS DateTime), NULL, 2)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (58, N'Lead Administration', NULL, CAST(N'2018-04-09T11:46:38.120' AS DateTime), NULL, 2)
--INSERT [dbo].[SGRole] ([SGRoleID], [SGRoleName], [CreatedBy], [CreatedDate], [SystemInfo], [DepartmentId]) VALUES (59, N'Head Administration', NULL, CAST(N'2018-04-09T11:46:50.090' AS DateTime), NULL, 2)
--SET IDENTITY_INSERT [dbo].[SGRole] OFF

--Practice Area
--SET IDENTITY_INSERT [dbo].[PracticeArea] ON 
--INSERT [dbo].[PracticeArea] ([PracticeAreaId], [PracticeAreaCode], [PracticeAreaDescription], [IsActive], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo]) VALUES (8, N'Talent Pool', N'Talent Pool', 1, NULL, NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[PracticeArea] OFF

--To Update practice area id in project table
--UPDATE  p SET p.PracticeAreaId = prod.ProjectTypeId FROM Projects p JOIN [dbo].[ProjectsProd] prod ON prod.projectcode = p.ProjectCode

----DepartmentType
--SET IDENTITY_INSERT [dbo].[DepartmentType] ON 

--INSERT [dbo].[DepartmentType] ([DepartmentTypeId], [DepartmentType]) VALUES (1, N'Delivery')
--INSERT [dbo].[DepartmentType] ([DepartmentTypeId], [DepartmentType]) VALUES (2, N'Service')

--SET IDENTITY_INSERT [dbo].[DepartmentType] OFF

----KRAScaleMaster
--SET IDENTITY_INSERT [dbo].[KRAScaleMaster] ON 

--INSERT [dbo].[KRAScaleMaster] ([KRAScaleMasterID], [MinimumScale], [MaximumScale],[KRAScaleTitle]) VALUES (1, 1, 5,'One-To-Five')
--INSERT [dbo].[KRAScaleMaster] ([KRAScaleMasterID], [MinimumScale], [MaximumScale],[KRAScaleTitle]) VALUES (2, 1, 10,'One-To-Ten')
--INSERT [dbo].[KRAScaleMaster] ([KRAScaleMasterID], [MinimumScale], [MaximumScale],[KRAScaleTitle]) VALUES (3, 1, 20,'One-To-Twenty')
--SET IDENTITY_INSERT [dbo].[KRAScaleMaster] OFF

----KRAScaleDetails

--SET IDENTITY_INSERT [dbo].[KRAScaleDetails] ON 

--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (1, 1, 1, N'Poor')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (2, 1, 2, N'Average')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (3, 1, 3, N'Good')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (4, 1, 4, N'Very Good')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (5, 1, 5, N'Excellent')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (6, 2, 1, N'Very Poor')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (7, 2, 2, N'Poor')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (8, 2, 3, N'Significantly below average')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (9, 2, 4, N'Below Average')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (10, 2, 5, N'Average')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (11, 2, 6, N'Above Average')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (12, 2, 7, N'Significantly Above Average')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (13, 2, 8, N'Good')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (14, 2, 9, N'Very Good')
--INSERT [dbo].[KRAScaleDetails] ([KRAScaleDetailID], [KRAScaleMasterID], [KRAScale], [ScaleDescription]) VALUES (15, 2, 10, N'Excellent')
--SET IDENTITY_INSERT [dbo].[KRAScaleDetails] OFF

----KRAOperator

--SET IDENTITY_INSERT [dbo].[KRAOperator] ON 

--INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (1, N'>')
--INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (2, N'>=')
--INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (3, N'<')
--INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (4, N'<=')
--INSERT [dbo].[KRAOperator] ([KRAOperatorID], [Operator]) VALUES (5, N'=')
--SET IDENTITY_INSERT [dbo].[KRAOperator] OFF

----KRAScaleType

--SET IDENTITY_INSERT [dbo].[KRAMeasurementType] ON 

--INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (1, N'Scale')
--INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (2, N'Percentage')
--INSERT [dbo].[KRAMeasurementType] ([KRAMeasurementTypeID], [KRAMeasurementType]) VALUES (2, N'Number')
--SET IDENTITY_INSERT [dbo].[KRAMeasurementType] OFF

----KRATargetPeriods

--SET IDENTITY_INSERT [dbo].[KRATargetPeriod] ON 

--INSERT [dbo].[KRATargetPeriod] ([KRATargetPeriodID], [KRATargetPeriod]) VALUES (1, N'Quarterly')
--INSERT [dbo].[KRATargetPeriod] ([KRATargetPeriodID], [KRATargetPeriod]) VALUES (2, N'Half yearly')
--INSERT [dbo].[KRATargetPeriod] ([KRATargetPeriodID], [KRATargetPeriod]) VALUES (3, N'Yearly')
--SET IDENTITY_INSERT [dbo].[KRATargetPeriod] OFF

--SET IDENTITY_INSERT [dbo].[AwardType] ON 

--INSERT [dbo].[AwardType] ([AwardTypeId],[AwardType]) VALUES (1, N'Individual')
--INSERT [dbo].[AwardType] ([AwardTypeId],[AwardType]) VALUES (2, N'Team')

--SET IDENTITY_INSERT [dbo].[AwardType] OFF

--SET IDENTITY_INSERT [dbo].[Award] ON 

--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (1, N'Kaizen',N'Good Change and  Improvement',N'Recognize entrepreneurial spirit that thrives through effective change management and improvement',5,2500, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (2, N'Best Project',N'Sustained Delivery Performance',N'Recognize team working to achieve and sustain higher delivery performance',1,2500, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (3, N'Best Turnaround Project',N'Performance Turnaround',N'Recognize team working to turnaround project performance in adverse situation',1,2500, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (4, N'Best Service Department',N'Sustained Service Performance',N'Recognize team working to achieve and sustain higher service performance',1,2500, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (5, N'Trumpet',N'Business Generator Aid',N'Recognize entrepreneurial work that showcase technology and engineering capability OR integrates business and technology into unique proof of concepts, to aid in business generation',1,2500, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (6, N'Most Admired Team',N'Effective Teaming',N'Recognize multi-dimensional performance and developmental contribution through effective team working and leadership',1,2500, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206') 
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (7, N'Bravo Instant Recognition',N'Good Change and  Improvement',N'Recognize entrepreneurial spirit that thrives through effective change management and improvement',1,0, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[Award] ([AwardId],[AwardTitle],[AwardSubTitle],[AwardDescription],[NumberOfAwards],[PriceAmount],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo]) VALUES (8, N'RISE Instant Recognition',N'Good Change and  Improvement',N'Recognize entrepreneurial spirit that thrives through effective change management and improvement',1,0, N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--SET IDENTITY_INSERT [dbo].[Award] OFF

--SET IDENTITY_INSERT [dbo].[AwardDepartmentMapper] ON 
--Kaizen

--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (1,1,1,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (2,1,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (3,1,1,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (4,1,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')


--BestService Department
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (5,4,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--Best Project
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (6,2,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (7,2,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--Best TurnAround
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (8,3,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (9,3,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--Trumpet

--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (10,5,1,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (11,5,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (12,5,1,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (13,5,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--Most Admired Team

--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (14,6,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (15,6,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--Bravo Instant Recognition

--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (16,7,1,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (17,7,1,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--RISE Instant Recognition

--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (18,8,1,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (19,8,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (20,8,2,1,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')
--INSERT [dbo].[AwardDepartmentMapper] ([Id],[AwardId],[AwardTypeId],[DepartmentId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[SystemInfo])VALUES (21,8,2,2,N'hrms', CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'hrms',CAST(N'2018-01-02T16:56:27.780' AS DateTime), N'192.168.2.206')

--SET IDENTITY_INSERT [dbo].[AwardDepartmentMapper] OFF

--Notification Type

--SET IDENTITY_INSERT [dbo].[NotificationType] ON 

--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (1, N'TRApproved', N'Talent Requisition Approved', NULL, CAST(N'2018-06-14T17:03:26.110' AS DateTime), NULL, CAST(N'2018-06-14T17:03:26.110' AS DateTime), NULL, 1)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (1, N'EPC', N'Emp Profile Creation Notification', NULL, CAST(N'2018-06-14T17:01:21.677' AS DateTime), NULL, CAST(N'2018-06-14T17:01:21.677' AS DateTime), NULL, 4)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (1, N'KRASubmittedForDHReview', N'KRA Submitted for Department Head Review', NULL, CAST(N'2018-06-14T17:04:56.757' AS DateTime), NULL, CAST(N'2018-06-14T17:04:56.757' AS DateTime), NULL, 5)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (2, N'TRRejected', N'Talent Requisition Rejected', NULL, CAST(N'2018-06-14T17:03:48.370' AS DateTime), NULL, CAST(N'2018-06-14T17:03:48.370' AS DateTime), NULL, 1)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (2, N'KRASendBackForHRAReview', N'KRA Send back for HRA review', NULL, CAST(N'2018-06-14T17:05:21.747' AS DateTime), NULL, CAST(N'2018-06-14T17:05:21.747' AS DateTime), NULL, 5)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (3, N'TRSubmitForApproval', N'Talent Requisition submitted for Approval', NULL, CAST(N'2018-06-14T17:04:12.950' AS DateTime), NULL, CAST(N'2018-06-14T17:04:12.950' AS DateTime), NULL, 1)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (3, N'KRAApproved', N'KRA Approved', NULL, CAST(N'2018-06-14T17:05:56.050' AS DateTime), NULL, CAST(N'2018-06-14T17:05:56.050' AS DateTime), NULL, 5)
--INSERT [dbo].[NotificationType] ([NotificationTypeID], [NotificationCode], [NotificationDesc], [CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (4, N'DeleteKRAGroup', N'KRA Group has been deleted.', NULL, CAST(N'2018-06-25T10:45:48.853' AS DateTime), NULL, CAST(N'2018-06-25T10:45:48.853' AS DateTime), NULL, 5)
--SET IDENTITY_INSERT [dbo].[NotificationType] OFF

--Notification Configuration

--SET IDENTITY_INSERT [dbo].[NotificationConfiguration] ON 

--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (1, 1, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;', N'Employee Profile Creation Notification of new associate - AssociateId :', N'&lt;p&gt;Hi, The associate profile of {AssociateName} is pending for your approval. Following are the details for your attention:Associate Name : {AssociateName} Designation : {Designation} Department : {Department}Disclaimer : This is an auto generated email.&lt;/p&gt;', 0, N'it@senecaglobal.com', NULL, N'it@senecaglobal.com', NULL, N'192.168.2.36')
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (3, 1, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com', N'Talent requisiton @TRCode approved - @ProjectName - @RoleName', N'<p>Dear @CreatedBy,</p><p>Talent Requisition <strong>@TRCode</strong> raised for the project <strong>@ProjectName</strong> and for the Role <strong>@RoleName</strong> has been approved successfully.<br/></p><p><br/></p><div>  <span style="color: rgb(255, 0, 0);">Disclaimer : This is an auto generated email.</span></div>', 0, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (4, 2, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com;sushmitha.kalluri@senecaglobal.com', N'Talent requisiton {TRCode} submitted for approval has been rejected. - {ProjectName} ', N'<p>Hi,</p><p>Talent Requisition <strong>{TRCode}</strong> raised for the project <strong>{ProjectName}</strong> has been rejected due to the following reason <br/><strong>{Comments}</strong>.<br/></p><p><br/></p><div><span>Disclaimer : - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.</span></div>', 0, N'it@senecaglobal.com', NULL, NULL, NULL, NULL)
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (5, 1, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com;rajitha.chintalapani@senecaglobal.com', N'KRA for financial year {FinancialYear} submitted for review.', N'&lt;p&gt;Hi &lt;strong&gt;{ToEmployeeName}&lt;/strong&gt;,&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;KRA for financial year &lt;strong&gt;{FinancialYear}&lt;/strong&gt; for &lt;strong&gt;{DepartmentName}&lt;/strong&gt; has been submitted for your review by &lt;strong&gt;{FromEmployeeName}&lt;/strong&gt;.&lt;span class="ql-cursor"&gt;﻿&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;DISCLAIMER - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.&lt;/p&gt;', 0, NULL, NULL, N'sushmitha.kalluri@senecaglobal.com', NULL, N'192.168.11.2')
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (6, 2, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com;sushmitha.kalluri@senecaglobal.com', N'KRA for financial year {FinancialYear} has been sent back for review.', N'<p>Hi {ToEmployeeName},</p><p>KRA for financial year <strong>{FinancialYear}</strong> has been sent back for review by {FromEmployeeName} for <strong>{DepartmentName}</strong>. Please look into the comments<br/><strong>{Comments}</strong>.<br/></p><br/><br/><p><strong>DISCLAIMER</strong> - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.</></div>', 0, NULL, NULL, NULL, NULL, NULL)
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (7, 3, N'it@senecaglobal.com', N'sushmitha.kalluri@senecaglobal.com;sravanthi.kondi@senecaglobal.com;bhavani.chintamadaka@senecaglobal.com;sunitha.kanumuri@senecaglobal.com;ramya.singamsetti@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com', N'Talent requisiton {TRCode} submitted for approval - {ProjectName} ', N'&lt;p&gt;Hi, Talent Requisition {TRCode} raised for the project {ProjectName} has been submitted for approval. Below are the details of talent requisition: TR Code {TRCode} SubmittedBy {SubmittedBy} Role(s) {RoleName} DISCLAIMER - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.&lt;/p&gt;', 0, N'it@senecaglobal.com', CAST(N'2018-04-02T18:01:45.320' AS DateTime), N'ramya.singamsetti@senecaglobal.com', CAST(N'2018-04-02T18:01:45.320' AS DateTime), N'192.168.2.71')
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo]) VALUES (8, 3, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'kalyan.penumutchu@senecaglobal.com;santosh.bolisetty@senecaglobal.com;rajitha.chintalapani@senecaglobal.com;', N'KRA for financial year {FinancialYear} has been approved.', N'&lt;p&gt;Hi {ToEmployeeName},KRA for financial year {FinancialYear} has been approved {FromEmployeeName} for {DepartmentName}. DISCLAIMER - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.&lt;/p&gt;', 0, NULL, NULL, N'kalyan.penumutchu@senecaglobal.com', NULL, N'192.168.11.2')
--INSERT [dbo].[NotificationConfiguration] ([NotificationConfigID], [NotificationTypeID], [EmailFrom], [EmailTo], [EmailCC], [EmailSubject], [EmailContent], [SLA], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SystemInfo], [CategoryId]) VALUES (14, 4, N'it@senecaglobal.com', N'sravanthi.kondi@senecaglobal.com', N'sushmitha.kalluri@senecaglobal.com', N'KRA for financial year {FinancialYear} has been deleted.', N'&lt;p&gt;Hi, &lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;KRA &lt;strong&gt;{KRAGroup}&lt;/strong&gt; for financial year &lt;strong&gt;{FinancialYear}&lt;/strong&gt; has been deleted by &lt;strong&gt;{EmployeeId}&lt;/strong&gt;. &lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;DISCLAIMER&lt;/strong&gt; - The information contained in this email is confidential and intended for the named recipient(s) only. If you are not an intended recipient of this email you must not copy, distribute or take any further action in reliance on it. You should delete it and notify the sender immediately.&lt;/p&gt;', 0, NULL, CAST(N'2018-06-25T10:52:22.427' AS DateTime), N'sunitha.kanumuri@senecaglobal.com', CAST(N'2018-06-25T10:52:22.427' AS DateTime), N'192.168.11.2', 5)
--SET IDENTITY_INSERT [dbo].[NotificationConfiguration] OFF


--SET IDENTITY_INSERT [dbo].[JDTemplateTitleMaster] ON 
--INSERT [dbo].[JDTemplateTitleMaster] ([TemplateTitleId], [TemplateTitle],[TemplateDescription],[CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,'Developer','Developer', NULL, NULL, NULL, NULL)
--INSERT [dbo].[JDTemplateTitleMaster] ([TemplateTitleId], [TemplateTitle],[TemplateDescription],[CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (2,'Leader','Leader', NULL, NULL, NULL, NULL)
--INSERT [dbo].[JDTemplateTitleMaster] ([TemplateTitleId], [TemplateTitle],[TemplateDescription],[CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,'Architect','Architect', NULL, NULL, NULL, NULL)
--INSERT [dbo].[JDTemplateTitleMaster] ([TemplateTitleId], [TemplateTitle],[TemplateDescription],[CreatedUser], [CreatedDate], [ModifiedUser], [ModifiedDate],[SystemInfo]) VALUES (1,'Technical Analyst','Technical Analyst', NULL, NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[JDTemplateTitleMaster] OFF


