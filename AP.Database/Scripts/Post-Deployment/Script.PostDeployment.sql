﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]		
Mandatory - Right click -> Excution Settings -> SQLCMD mode			
--------------------------------------------------------------------------------------
*/

--:r .\SeedData.sql

--:r .\IX_CategoryMaster.sql
--:r .\IX_NotificationConfiguration.sql
--:r .\IX_NotificationConfiguration.sql

----------------------------START-------------------------------
--SGL15009-3687 Database - Project code column changes
ALTER TABLE Projects
ALTER COLUMN [ProjectCode] VARCHAR(30) NOT NULL
----------------------------END------------------------------------
----------------------------START-------------------------------
--SGL15009-3663 SOW - DB
ALTER TABLE SOW
ADD SOWFileName VARCHAR(50) NULL
----------------------------END------------------------------------
----------------------------START-------------------------------
--SGL15009-3657 Project Creation - DB Add domainId
ALTER TABLE PRojects
Add  DomainId INT NULL

ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Domains] FOREIGN KEY([DomainId])
REFERENCES [dbo].[Domain] ([DomainId])
----------------------------END------------------------------------
----------------------------START-------------------------------
--SGL15009-3657 Project Creation - DB Add projectstateid
ALTER TABLE PRojects
Add  ProjectStateId INT NULL
----------------------------END------------------------------------
----------------------------START-------------------------------
--SGL15009-3657 Project Creation - DB insert into categorymaster
INSERT INTO CategoryMaster(CategoryName,IsActive,CreatedUser,CreatedDate,SystemInfo) 
values('PPC',1,'mithun.pradhan@senecaglobal.com','2019-02-05 20:15:20.890','192.168.11.2')
----------------------------END------------------------------------
----------------------------START-------------------------------
--SGL15009-3657 Project Creation - DB insert into notificationtype
INSERT INTO NotificationType(NotificationTypeID,NotificationCode,NotificationDesc,CreatedUser,CreatedDate,SystemInfo,CategoryId) 
values(12,'PPC','Project Profile Creation','mithun.pradhan@senecaglobal.com','2019-02-05 20:15:20.890','192.168.11.2',9)
----------------------------END------------------------------------
----------------------------START-------------------------------
--SGL15009-3657 Project Creation - DB insert into status with categoryid
INSERT INTO STATUS (StatusId,StatusCode,StatusDescription,IsActive,CreatedUser,CreatedDate,SystemInfo,CategoryID)
            Values(21,'SubmittedForApproval','Submitted For Approval',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),			
                  (22,'ApprovedByDH','Approved By Delivery Head',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
			      (23,'RejectedByDH','RejectedBy Delivery Head',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (24,'Drafted','Drafted',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (25,'Created','Created',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9),
				  (26,'Closed','Closed',1,'mithun.pradhan@senecaglobal.com','2019-02-05 14:33:50.340','192.168.11.2',9)
----------------------------END------------------------------------