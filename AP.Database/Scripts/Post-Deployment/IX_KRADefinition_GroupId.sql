﻿/****** Object:  Indexes [IX_KRADefinition_GroupId]    Script Date: 25/06/2018 2:07:12 PM ******/
CREATE NONCLUSTERED INDEX [IX_KRADefinition_GroupId] ON [dbo].[KRADefinition]
(
	[KRAGroupId] ASC
)
GO