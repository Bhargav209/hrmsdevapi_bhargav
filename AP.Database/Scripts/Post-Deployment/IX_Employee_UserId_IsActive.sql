﻿CREATE NONCLUSTERED INDEX [IX_Employee_UserId_IsActive] ON [dbo].[Employee]
(
[UserId] ASC,
       [IsActive] ASC
)
INCLUDE ([EmployeeId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
