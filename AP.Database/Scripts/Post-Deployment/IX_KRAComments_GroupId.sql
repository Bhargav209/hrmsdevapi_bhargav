﻿/****** Object:  Indexes [IX_KRAComments_GroupId]    Script Date: 25/06/2018 2:07:12 PM ******/
CREATE NONCLUSTERED INDEX [IX_KRAComments_GroupId] ON [dbo].[KRAComment]
(
	[KRAGroupId] ASC
)
GO