﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace AP.Logging
{
    public class Logger
    {
        static readonly Object lockObj = new Object();
        public enum Severity
        {
            Critical = 1,
            Error = 2,
            Warning = 3,
            Debug = 4,
            Info = 5,
            None = 0
        }
        public static bool Write(string message, Severity severity)
        {
            Task<bool> task = Task.Factory.StartNew<bool>(() =>
            {
                return Log(message, severity);
            });

            return task.Result;
        }

        private static bool Log(string message, Severity type)
        {
            string logMessage = string.Format("{0},{1},{2},{3},{4},{5}", string.Format(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")), type, Assembly.GetCallingAssembly().GetName().Name, GetCurrentMethod(), message);
            string path = ConfigurationManager.AppSettings["LogPath"].ToString() + "\\Log" + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";
            bool returnValue = true;

            if (path != null)
            {
                StreamWriter streamWriter = null;

                try
                {
                    lock (lockObj)
                    {
                        streamWriter = new StreamWriter(path, true);
                        streamWriter.WriteLine(logMessage);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }
                catch
                {
                    returnValue = false;
                }
                
                finally
                {
                    if (streamWriter != null)
                    {
                        streamWriter.Close();
                        streamWriter.Dispose();
                    }
                }
            }

            return returnValue;
        }

        private static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(2);
            return sf.GetMethod().Name;
        }
    }
}
