﻿using System;
using System.Configuration;

namespace AP.Utility
{
    public class Config
    {
        #region ReadString
        /// <summary>
        /// Reads the string from configuration file and returns the string value
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public string ReadString(string keyName)
        {
           return ConfigurationManager.AppSettings[keyName].ToString();      
        }
        #endregion

        #region ReadInt
        /// <summary>
        /// Reads the from string configuration file and returns the integer value
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public int ReadInt(string keyName)
        {
            int value = Convert.ToInt32(ConfigurationManager.AppSettings[keyName].ToString());
            return value;
        } 
        #endregion
    }
}
