﻿using System.ComponentModel;
namespace AP.Utility
{
    public class Enumeration
    {
        #region Application Roles constant values
        public static readonly string ProgramManager = "Program Manager";
        public static readonly string HRHead = "HR Head";
        public static readonly string DeliveryHead = "Delivery Head";
        public static readonly string DepartmentHead = "Department Head";
        public static readonly string CompetencyLeader = "Competency Leader";
        public static readonly string FinanceDepartment = "FD";
        public static readonly string ApprovalDueWithPM = "ApprovalDue with PM";                // Approval Due with Pragram Mangaer
        public static readonly string ApprovalDueWithHRM = "ApprovalDue with HRM";              // Approval Due with HR Mangaer
        public static readonly string ApprovalDueWithDH = "ApprovalDue with DH";                // Approval Due with Department Head
        public static readonly string ApprovalDueWithFinanceHead = "ApprovalDue with FH";       // Approval Due with Finance Head
        public static readonly string ApprovalDueWithHRHead = "ApprovalDue with HRH";           // Approval Due with HR Head
        public static readonly string TagTA = "Tag TA";                                         // Tag TA
        public static readonly string Approved = "Approved";                                    // Approved
        public static readonly string Rejected = "Rejected";                                    // Rejected
        public static readonly string Closed = "Closed";                                        // Closed
        public static readonly string ApprovalDue = "ApprovalDue";                              // ApprovalDue
        #endregion

        #region Application Roles
        public enum ApplicationRoles
        {
            FinanceHead,
            HRHead,
            HRA,
            HRM,
            Lead,
            Manager
        }
        #endregion

        #region Department codes
        public enum DepartmentCodes
        {
            Delivery,
            Admin,
            IT,
            FD,
            QA,
            HR
        }
        #endregion

        #region Requisition Type
        public enum RequisitionType
        {
            New = 25,
            Replacement = 26
        }
        #endregion

        #region TR Approval Status
        public enum TRApprovalStatus
        {
            TRSubmitForApproval,
            TRApproved,
            TRRejected
        }
        #endregion

        #region Notification Status
        public enum NotificationStatus
        {
            Admin,
            Allocation,
            EPC,
            Finance,
            IT,
            KRAAprroved,
            KRARejected,
            KRAApprove,
            KRAReview,
            KRAAssignment,
            PA,
            TRSubmitForApproval,
            TRRejected,
            TRApproved,
            TagTAPositions
        }
        #endregion

        #region Notification Type
        public enum NotificationType  //Removed identity column from table, and as per the category these values are given
        {
            EPC = 1,
            TRSbmitForApproval = 3,
            TRRejected = 2,
            KRASubmittedForDHReview = 1,
            KRAApproved = 3,
            KRASendBackForHRMReview = 2,
            DeleteKRAGroup = 4,
            SubmittedForHRHeadReview = 7,
            SendBackForDepartmentHeadReview = 8,
            Allocation = 9,
            Release = 10,
            EPU = 1,
            ESU=11,
            PPC=12 ,
            PPCApproved = 17,
        }
        #endregion

        #region CategoryMaster
        public enum CategoryMaster
        {
            TalentRequisition = 1,
            AssociateExit = 2,
            Skills = 3,
            EPC = 4,
            KRA = 5,
            ADR = 6,
            EPU = 7,
            ESU = 8,
            PPC=9,
            PPCApproved =10,
        }
        #endregion

        #region Roles
        /// <summary>
        /// Roles enum values
        /// </summary>
        public enum Roles
        {
            PGM = 1,  // Program Manager
            TL = 16,  // Team Lead
            Dev = 20, // Developer
            T = 18,   // Tester
            AT = 19,  // Architect
            CDM = 21,  // Competency Development Manager
            CompetencytLead = 24,
            TechnicalLead = 5,
            ProjectManager = 3,
            ProgramManager
        }
        #endregion

        #region JoiningStatus
        /// <summary>
        /// JoiningStatus enum values
        /// </summary>
        public enum JoiningStatus
        {
            Joined = 1,
            NotJoined = 2
        }
        #endregion

        #region BGVStatus
        /// <summary>
        /// JoiningStatus enum values
        /// </summary>
        public enum BGVStatus
        {
            Verified = 1,
            NotVerified = 2
        }
        #endregion

        #region Category
        /// <summary>
        /// JoiningStatus enum values
        /// </summary>
        public enum Category
        {
            BGV = 1,
            CustomerStatus = 2,
            ProjectStatus = 3
        }
        #endregion

        #region SearchType
        public enum SearchType
        {
            Role,
            UserName,
            Code,
            Desc,
            DepartmentHead,
            DepartmentCode,
            Category,
            CompetencyArea,
            BusinessRuleValue,
            SkillGroup,
            AssociateID,
            AssociateName,
            Experience
        }

        #endregion

        #region StatusCategory
        /// <summary>
        /// StatusCategory
        /// </summary>
        public enum StatusCategory
        {
            TalentRequisition,
            ProjectManagement,
            AssociateExit
        }
        #endregion

        #region TalentRequisitionStatus
        /// <summary>
        /// TalentRequisitionStatus
        /// </summary>
        public enum TalentRequisitionStatusCodes
        {
            Draft = 1,
            SubmittedForApproval = 2,
            SubmittedForFinanceApproval = 3,
            RejectedByDeliveryHead = 4,
            RejectedByFinanceHead = 5,
            Approved = 6,
            Close = 7
        }
        #endregion
        public enum AssociateExitStatusCodes
        {
            SubmittedForResignation = 4,
            ApprovedByPM = 5,
            ApprovedByDeliveryHead = 6,
            ApprovedByIT = 7,
            ApprovedByAdmin = 8,
            ApprovedByFinance = 9,
            ApprovedByHRA = 10,
            Rejected = 11,
            Resigned = 12,
        }

        public enum ProjectTypes
        {
            [Description("Talent Pool")]
            TalentPool
        }

        public enum ProficiencyLevels
        {
            Expert,
            Advanced,
            Intermediate,
            Basic,
            Beginner
        }

        public enum ProgramType
        {
            FullTime,
            PartTime,
            DistanceEducation,
            Certification,
            MemberShip
        }

        public enum EPCStatus
        {
            Approved = 1,
            Pending = 2,
            Rejected = 3
        }
        public enum EPCNotificationStatus
        {
            Rejected = 2,
            Approved = 3
        }

        public enum SkillNotificationStatus
        {
            Pending = 1,
            Approved = 2
        }

        public enum ErrorLogType
        {
            UI = 1,
            Service = 2
        }

        public enum KRA
        {
            Draft = 1,
            SubmittedForDepartmentHeadReview = 2,
            SendBackForHRMReview = 3,
            Approved = 4,
            SubmittedForHRHeadReview = 5,
            SendBackForDepartmentHeadReview = 6
        }

        public enum KRAType
        {
            OrganizationLevelKRAs = 1,
            AdditionalKRAs = 2,
        }
    }
}