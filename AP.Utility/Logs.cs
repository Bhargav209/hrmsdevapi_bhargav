﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

namespace AP.Utility
{
   public static class Log
   {
      static DateTime m_logCreatedTime = default(DateTime);
      static string m_filePath = string.Empty;
      static string m_basePath = "";
      static int m_configHours;
      static Severity m_severity;
      static bool isToLog = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLog"]);
      static string LogfilePath = Convert.ToString(ConfigurationManager.AppSettings["LogfilePath"]);
      static string KraLogfilePath = Convert.ToString(ConfigurationManager.AppSettings["KRAPdfGenerationLog"]);
      static string log_type = "";

      #region SetLog
      /// <summary>
      /// Sets the Log
      /// </summary>
      static void SetLog()
      {
         Config config = new Config();
         m_basePath = config.ReadString("BasePath").ToString(); ;
         m_configHours = config.ReadInt("ConfigHrs");
         string severity = config.ReadString("Severity");

         switch (severity)
         {
            case "Critical":
               m_severity = Severity.Critical;
               break;
            case "Error":
               m_severity = Severity.Error;
               break;
            case "Warning":
               m_severity = Severity.Warning;
               break;
            case "Debug":
               m_severity = Severity.Debug;
               break;
            case "Info":
               m_severity = Severity.Info;
               break;
            case "None":
               m_severity = Severity.None;
               break;
         }
      }
      #endregion

      #region Severity
      /// <summary>
      /// Severity enum values
      /// </summary>
      public enum Severity
      {
         Critical = 1,
         Error = 2,
         Warning = 3,
         Debug = 4,
         Info = 5,
         None = 0
      }
      #endregion

      #region SetFilePath
      /// <summary>
      /// Sets the file path
      /// </summary>
      public static void SetFilePath()
      {
         if (m_filePath == string.Empty || m_logCreatedTime.AddHours(m_configHours) > m_logCreatedTime)
         {
            bool isExists = Directory.Exists(HostingEnvironment.MapPath(LogfilePath));
            if (!isExists)
               Directory.CreateDirectory(HostingEnvironment.MapPath(LogfilePath));

            m_logCreatedTime = DateTime.Now;
            m_filePath = HostingEnvironment.MapPath(LogfilePath + log_type + DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day) + ".txt";
         }

      }
      #endregion

      #region Print
      /// <summary>
      /// Asyncronous method to print message
      /// </summary>
      /// <param name="message"></param>
      static async void Print(string message)
      {
         using (StreamWriter writer = new StreamWriter(m_filePath, true))
         {
            await writer.WriteAsync(message);
         }
      }
      #endregion

      #region WriteMessageOnSeverity
      /// <summary>
      /// Writes the message based on severity
      /// </summary>
      /// <param name="messageString"></param>
      /// <param name="type"></param>
      public static void WriteMessageOnSeverity(string messageString, Severity type)
      {
         SetLog();
         SetFilePath();
         if (m_severity.ToString().ToLower() == "critical" && (type.ToString().ToLower() == "critical"))
         {
            Print(messageString);
         }
         else if (m_severity.ToString().ToLower() == "error" && (type.ToString().ToLower() == "critical" || type.ToString().ToLower() == "error"))
         {
            Print(messageString);
         }
         else if (m_severity.ToString().ToLower() == "warning" && (type.ToString().ToLower() == "critical" || type.ToString().ToLower() == "error" || type.ToString().ToLower() == "warning"))
         {
            Print(messageString);
         }
         else if (m_severity.ToString().ToLower() == "debug" && (type.ToString().ToLower() == "critical" || type.ToString().ToLower() == "error" || type.ToString().ToLower() == "warning" || type.ToString().ToLower() == "debug"))
         {
            Print(messageString);
         }
         else if (m_severity.ToString().ToLower() == "info" && (type.ToString().ToLower() == "critical" || type.ToString().ToLower() == "error" || type.ToString().ToLower() == "warning" || type.ToString().ToLower() == "debug" || type.ToString().ToLower() == "info"))
         {
            Print(messageString);
         }
      }
      #endregion

      #region Write
      /// <summary>
      /// Method to write log message to the file 
      /// </summary>
      /// <param name="msg"></param>
      /// <param name="type"></param>
      /// <param name="param"></param>
      public static void Write(string msg, Severity type, string param)
      {
         try
         {
            if (isToLog)
            {
               log_type = "warning";
               string message = string.Format("{0},{1},{2},{3},{4},{5}", string.Format(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")), Assembly.GetCallingAssembly().GetName().Name, GetCurrentMethod(), param, msg, Environment.NewLine);
               WriteMessageOnSeverity(message, type);
            }
         }
         catch
         {
            throw;
         }
      }
        #endregion

        #region KRAPDFWrite
        /// <summary>
        /// Method to write log message to the file 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="type"></param>
        /// <param name="param"></param>
        public static void KRAPDFWrite(string msg, Severity type, string param)
        {
            try
            {
                //if (isToLog)
                //{
                log_type = "info";
                if (m_filePath == string.Empty)
                {
                    string filepath = KraLogfilePath + "/Files/";
                    bool isExists = Directory.Exists(filepath);
                    if (!isExists)
                        Directory.CreateDirectory(filepath);
                    isExists = Directory.Exists(filepath + "/KRAPdfGenerationLog");
                    if (!isExists)
                        Directory.CreateDirectory(filepath + "/KRAPdfGenerationLog");

                    m_logCreatedTime = DateTime.Now;
                    m_filePath = filepath + "/KRAPdfGenerationLog/KRAPdfGenerationLog" + log_type + DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + ".txt";
                    File.AppendAllText(m_filePath, msg+ Environment.NewLine);
                }
                else
                    File.AppendAllText(m_filePath, msg + Environment.NewLine);
                //}
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region LogError
        /// <summary>
        /// Logs error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="type"></param>
        /// <param name="param"></param>
        public static void LogError(Exception ex, Severity type, string param)
      {
         if (isToLog)
         {
            string result = string.Empty;
            var stackTrace = new StackTrace(ex);
            var currentFrame = stackTrace.GetFrame(0);
            string errorMsg = ex.Message;
            if (errorMsg.IndexOf('.') >0)
            {
               int index = errorMsg.IndexOf('.');
               result = errorMsg.Substring(0, index);
            }
            if (ex.InnerException != null)
            {
               result = result + "," + ex.InnerException.InnerException.Message;
            }
            var query = stackTrace.GetFrames()         // get the frames
                          .Select(frame => new
                          {                   // get the info
                                 Method = frame.GetMethod(),
                             Class = frame.GetMethod().DeclaringType,
                          }).ToList();
            string filename = string.Empty;
            if (query.Count() > 0)
               filename = query.ToList()[query.Count() - 1].ToString();

            log_type = "error";
            try
            {
               string message = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"), Assembly.GetCallingAssembly().GetName().Name, currentFrame.GetMethod().Name, GetLineNumber(ex), param, result, filename, Environment.NewLine);
               WriteMessageOnSeverity(message, type);
            }
            catch
            {
               throw;
            }
         }
      }
      #endregion

      #region GetCurrentMethod
      /// <summary>
      /// Gets the current method
      /// </summary>
      /// <returns></returns>
      public static string GetCurrentMethod()
      {
         StackTrace st = new StackTrace();
         StackFrame sf = st.GetFrame(2);
         return sf.GetMethod().Name;
      }
      #endregion

      #region GetLineNumber
      /// <summary>
      /// Gets the line number
      /// </summary>
      /// <param name="ex"></param>
      /// <returns></returns>
      public static int GetLineNumber(Exception ex)
      {
         var lineNumber = 0;
         const string lineSearch = ":line ";
         var index = ex.StackTrace.LastIndexOf(lineSearch);
         if (index != -1)
         {
            var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
            if (int.TryParse(lineNumberText, out lineNumber))
            {
            }
         }
         return lineNumber;
      }
      #endregion

      #region LogEntityFrameworkGeneratedSQLQuery
      /// <summary>
      /// LogEntityFrameworkGeneratedSQLQuery
      /// </summary>
      /// <param name="queryString"></param>
      public static void LogEFGeneratedSQLQuery(string queryString)
      {
         File.AppendAllText(Convert.ToString(ConfigurationManager.AppSettings["EFSQLQueryLogPath"]), queryString);
      }
      #endregion
   }

}
