﻿using System;
using System.Runtime.Serialization;

namespace AP.Utility
{
    [Serializable]
    public class AssociatePortalException : Exception
    {
        public AssociatePortalException()
        : base() { }

        public AssociatePortalException(string message)
        : base(message) { }

        public AssociatePortalException(string format, params object[] args)
        : base(string.Format(format, args)) { }

        public AssociatePortalException(string message, Exception innerException)
        : base(message, innerException) { }

        public AssociatePortalException(string format, Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException) { }

        protected AssociatePortalException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }
    }
}
