﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using AP.Utility;
using AP.DomainEntities;

namespace AP.Services.Filters
{
    public class UserInfoActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Update the created user and systeminfo
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ActionArguments.Count > 0 && actionContext.Request != null && actionContext.Request.Headers.Referrer != null)
            {
                var kvPair = actionContext.ActionArguments.ElementAtOrDefault(0);
                BaseEntity bEntity = kvPair.Value as BaseEntity;

                if (bEntity == null) return;

                var requestHost = actionContext.Request.Headers.Referrer.GetLeftPart(UriPartial.Authority);
                bEntity.CurrentUser = HttpContext.Current.User.Identity.Name;
                bEntity.SystemInfo = Commons.GetClientIPAddress();

                actionContext.ActionArguments[kvPair.Key] = bEntity;
            }
        }
    }
}