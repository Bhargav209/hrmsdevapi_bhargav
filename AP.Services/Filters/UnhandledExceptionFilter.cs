﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace AP.Services.Filters
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;

            var exType = context.Exception.GetType();

            if (exType == typeof(UnauthorizedAccessException))
                status = HttpStatusCode.Unauthorized;
            else if (exType == typeof(ArgumentException))
                status = HttpStatusCode.NotFound;

            string message = context.Exception.Message;

            var errorResponse =
               context.Request.CreateResponse<string>(status, message);
            context.Response = errorResponse;

            base.OnException(context);
        }
    }
}