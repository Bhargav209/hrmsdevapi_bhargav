﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace AP.Services.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                var httpRequestHeader = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
                httpRequestHeader = httpRequestHeader.Substring("CustomAuthorization".Length);

                string role = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeader));
                if (Roles != role)
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("Unauthorized User")
                    };
                }
            }
            catch(Exception ex)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Invalid Authorization-Token: "+ ex.Message)
                };
            }
        }
    }
}