﻿using AP.API;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using static AP.Utility.Enumeration;

namespace AP.Services.Controllers
{
    public class TalentRequisitionController : BaseApiController
    {
        //#region Add or Update Requisition
        ///// <summary>
        ///// SaveRequisition
        ///// </summary>
        ///// <param name="data"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage AddOrUpdateRequisition(TalentRequistionData talentRequistionData)
        //{
        //    HttpResponseMessage httpResponseMessage = null;
        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Requisition().AddOrUpdateRequisition(talentRequistionData));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        //#region Add or Update Requisition
        ///// <summary>
        ///// SaveRequisition
        ///// </summary>
        ///// <param name="data"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage AddOrUpdateRequisitionNG2(TalentRequistionData talentRequistionData)
        //{
        //    HttpResponseMessage httpResponseMessage = null;
        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Requisition().AddOrUpdateRequisitionNG2(talentRequistionData));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        //#region Get All Requisitions
        ///// <summary>
        ///// this method is used to get all requisitions
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public HttpResponseMessage GetAllRequisition()
        //{
        //    HttpResponseMessage httpResponseMessage = null;
        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Requisition().GetAllRequisitions());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        #region GetRequisitionById
        /// <summary>
        /// this method is used to get all requisitions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisitionById(int id)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Requisition().GetRequisitionById(id));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRequisitionViewDetailsById
        /// <summary>
        /// this method is used to Get Requisition ViewDetails By TRId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisitionViewDetailsById(int id)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Requisition().GetRequisitionViewDetailsById(id));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        //#region Approve or Rejected Requisition
        ///// <summary>
        ///// this method is used to approve or rejected requisition
        ///// </summary>
        ///// <param name="approveRequisitionData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage ApproveRequisition(ApproveRequisitionData approveRequisitionData)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Requisition().ApproveRequisition(approveRequisitionData));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        #region Tag TA Positiions to Requisition
        /// <summary>
        /// this method is used to set ta positions
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage TagTAPositions(int roleDetailId, int positions)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Requisition().TagTAPositions(roleDetailId, positions));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        //#region Get RequisitionDetails By Search
        ///// <summary>
        ///// this method is used to Get RequisitionDetails By Search
        ///// </summary>
        ///// <param name="roleData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage GetRequisitionDetailsBySearch(SearchFilter searchFilter)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Requisition().GetRequisitionDetailsBySearch(searchFilter));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        #region RequisitionMasterScreenServices

        #region CreateRequisition
        /// <summary>
        /// SaveRequisition
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateRequisition(TalentRequistionData talentRequistionData)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().CreateRequisition(talentRequistionData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUserProjectTypes
        /// <summary>
        /// This method is used to get project types of an employee
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetUserProjectTypes(int employeeId)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetUserProjectTypes(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUserProjects
        /// <summary>
        /// This method is used to get projects an employee
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetUserProjects(int employeeId, int departmentId)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetUserProjects(employeeId, departmentId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRequisition
        /// <summary>
        /// this method is used to get requisition
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisition(int requisitionId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetRequisition(requisitionId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteRequistion
        /// <summary>
        /// this method is used to get requisition
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeleteRequistion(int requisitionId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().DeleteRequistion(requisitionId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region Get All Requisitions of an associate
        /// <summary>
        /// this method is used to get all requisitions of an associate
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisitionsByEmpId(int employeeId)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetRequisitionsByEmpId(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #endregion

        #region RolesandSkillsScreenServices


        #region GetRequisitionRoleDetailsByTrId
        /// <summary>
        /// GetRequisitionRoleDetailsByTrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisitionRoleDetailsByTrId(int trId)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetRequisitionRoleDetailsByTrId(trId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRequisitionRoleDataById
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisitionRoleDataById(int requisitionRoleDetailId)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetRequisitionRoleDataById(requisitionRoleDetailId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        //#region SaveRequisitionRoleSkills
        ///// <summary>
        ///// SaveRequisitionRoleSkills
        ///// </summary>
        ///// <param name="requisitionRoleDetails"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage SaveRequisitionRoleSkills(RequisitionRoleSkillList requisitionRoleSkills)
        //{
        //    HttpResponseMessage httpResponseMessage = null;
        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().SaveRequisitionRoleSkills(requisitionRoleSkills));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        #region GetRequisitionRoleDetailsByTrId
        /// <summary>
        /// GetRequisitionRoleDetailsByTrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRequisitionRolesByTrId(int trId)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetRequisitionRolesByTrId(trId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteRequisitionRoleSkill
        /// <summary>
        /// 
        /// </summary>
        /// <param name="talentRequisitionID"></param>
        /// <param name="reqRoleDetaildID"></param>
        /// <param name="skillGroupID"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage DeleteRequisitionRoleSkill(int talentRequisitionID, int requisitionRoleDetailID, int skillGroupID)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().DeleteRequisitionRoleSkill(talentRequisitionID, requisitionRoleDetailID, skillGroupID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        //#region GetReqRoleSkillsByRRId
        ///// <summary>
        ///// GetReqRoleSkillsByRRId
        ///// </summary>
        ///// <param name="requisitionRoleDetailId"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public HttpResponseMessage GetReqRoleSkillsByRRId(int requisitionRoleDetailId)
        //{
        //    HttpResponseMessage httpResponseMessage = null;
        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetReqRoleSkillsByRRId(requisitionRoleDetailId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        //#region GetReqRoleSkills
        ///// <summary>
        ///// GetReqRoleSkills
        ///// </summary>
        ///// <param name="requisitionRoleDetailId"></param>
        ///// <param name="competencyAreaId"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public HttpResponseMessage GetReqRoleSkills(int requisitionRoleDetailId,int competencyAreaId)
        //{
        //    HttpResponseMessage httpResponseMessage = null;
        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetReqRoleSkills(requisitionRoleDetailId, competencyAreaId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        #region DeleteRequisitionRoleDetails
        /// <summary>
        /// DeleteRequisitionRoleDetails
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <param name="talentRequisitionId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteRequisitionRoleDetails(int requisitionRoleDetailId, int talentRequisitionId, int roleMasterId)
        {
            try
            {
                return await new TalentRequisitionDetails().DeleteRequisitionRoleDetails(requisitionRoleDetailId, talentRequisitionId, roleMasterId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #endregion

        #region BehaviorAreaServices

        #region AddBehaviorArea
        /// <summary>
        /// AddBehaviorArea
        /// </summary>
        /// <param name="behaviorData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddBehaviorArea(BehaviorAreas behaviorData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (behaviorData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().AddBehaviorArea(behaviorData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateBehaviorArea
        /// <summary>
        /// UpdateBehaviorArea
        /// </summary>
        /// <param name="behaviorData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateBehaviorArea(BehaviorAreas behaviorData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (behaviorData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().UpdateBehaviorArea(behaviorData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteBehaviorArea
        /// <summary>
        /// DeleteBehaviorArea
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeleteBehaviorArea(int behaviorAreaId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().DeleteBehaviorArea(behaviorAreaId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetBehaviorArea
        /// <summary>
        /// GetBehaviorArea
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetBehaviorArea()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetBehaviorArea());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #endregion

        #region BehaviorRatingServices
        #region AddBehaviorRating
        /// <summary>
        /// Save BehaviorRating
        /// </summary>
        /// <param name="behaviorRatingData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddBehaviorRating(BehaviorRatings behaviorRatingData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (behaviorRatingData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().AddBehaviorRating(behaviorRatingData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateBehaviorRating
        /// <summary>
        /// UpdateBehaviorRating
        /// </summary>
        /// <param name="behaviorRatingData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateBehaviorRating(BehaviorRatings behaviorRatingData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (behaviorRatingData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().UpdateBehaviorRating(behaviorRatingData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteBehaviorRating
        /// <summary>
        /// DeleteBehaviorRating
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeleteBehaviorRating(int ratingId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().DeleteBehaviorRating(ratingId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetBehaviorRating
        /// <summary>
        /// GetBehaviorRating
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetBehaviorRating()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetBehaviorRating());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion 
        #endregion

        #region LearningAptitudeServices

        #region AddLearningAptitude
        /// <summary>
        /// AddLearningAptitude
        /// </summary>
        /// <param name="learningAptitudeData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddLearningAptitude(LearningAptitudeData learningAptitudeData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (learningAptitudeData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().AddLearningAptitude(learningAptitudeData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateLearningAptitude
        /// <summary>
        /// UpdateLearningAptitude
        /// </summary>
        /// <param name="learningAptitudeData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateLearningAptitude(LearningAptitudeData learningAptitudeData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (learningAptitudeData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().UpdateLearningAptitude(learningAptitudeData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteLearningAptitude
        /// <summary>
        /// DeleteLearningAptitude
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeleteLearningAptitude(int learningAptitudeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().DeleteLearningAptitude(learningAptitudeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetLearningAptitude
        /// <summary>
        /// GetLearningAptitude
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetLearningAptitude()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetLearningAptitude());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetLearningAptitudeByTrId
        /// <summary>
        /// GetLearningAptitudeByTrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetLearningAptitudeByTrId(int trId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetLearningAptitudeByTrId(trId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion



        #endregion

        #region AddRequisitionRoleDetails
        /// <summary>
        /// AddRequisitionRoleDetails
        /// </summary>
        /// <param name="requisitionRoleDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveRequisitionRoleDetails(RequisitionRoleDetails requisitionRoleDetails)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().SaveRequisitionRoleDetails(requisitionRoleDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region ExpertiseAreaServices

        #region AddExpertiseArea
        /// <summary>
        /// AddExpertiseArea
        /// </summary>
        /// <param name="expertiseServiceData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddExpertiseArea(ExpertiseAreaData expertiseAreaData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (expertiseAreaData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().AddExpertiseArea(expertiseAreaData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateExpertiseArea
        /// <summary>
        /// UpdateExpertiseArea
        /// </summary>
        /// <param name="expertiseServiceData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateExpertiseArea(ExpertiseAreaData expertiseAreaData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (expertiseAreaData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().UpdateExpertiseArea(expertiseAreaData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteExpertiseArea
        /// <summary>
        /// DeleteExpertiseArea
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeleteExpertiseArea(int expertiseId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().DeleteExpertiseArea(expertiseId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetExpertiseArea
        /// <summary>
        /// GetExpertiseArea
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ExpertiseAreaData>> GetExpertiseArea()
        {
            List<ExpertiseAreaData> lstExpertiseAreas;

            try
            {
                lstExpertiseAreas = await new TalentRequisitionDetails().GetExpertiseAreas();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get Expertise areas.");
            }

            return lstExpertiseAreas;
        }
        #endregion

        #endregion

        #region MyRegion

        #region WorkPlaceBehaviorMethods
        /// <summary>
        /// SaveRequisitionBehaviorArea
        /// </summary>
        /// <param name="behaviorAreaDataList"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveRequisitionBehaviorArea(List<RequisitionWorkPlaceBehavior> behaviorAreaDataList)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().SaveRequisitionBehaviorArea(behaviorAreaDataList));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetWorkPlaceBehaviorByTrId
        /// <summary>
        /// GetWorkPlaceBehaviorByTrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetWorkPlaceBehaviorByTrId(int trId, int roleMasterId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().GetWorkPlaceBehaviorByTrId(trId, roleMasterId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #endregion

        #region GetExpertiseAreaByTrId
        /// <summary>
        /// Get Expertise Area By TrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<RequisitionExpertiseArea>> GetExpertiseAreaByTrId(int trId, int roleMasterId)
        {
            List<RequisitionExpertiseArea> lstReqExpertiseAreas;

            try
            {
                lstReqExpertiseAreas = await new TalentRequisitionDetails().GetExpertiseAreaByTrId(trId, roleMasterId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get requisition expertise areas.");
            }

            return lstReqExpertiseAreas;
        }
        #endregion

        #region SaveRequisitionExpertiseAreas
        /// <summary>
        /// Save Requisition Expertise Area Details
        /// </summary>
        /// <param name="requisitionExpertiseAreaDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> SaveRequisitionExpertiseAreas(RequisitionExpertiseArea requisitionExpertiseAreaDetails)
        {
            try
            {
                if (requisitionExpertiseAreaDetails == null)
                    throw new ArgumentNullException("Requisition Expertise Areas cannot be null.");

                return await new TalentRequisitionDetails().SaveRequisitionExpertiseAreas(requisitionExpertiseAreaDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to save requisition expertise areas.");
            }
        }
        #endregion

        #region SubmitRequisitionForApproval
        /// <summary>
        /// Submit Requisition for approval
        /// </summary>
        /// <param name="submitRequisitionForApproval"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> SubmitRequisitionForApproval(SubmitRequisitionForApproval submitRequisitionForApproval)
        {
            try
            {
                return await new TalentRequisitionDetails().SubmitRequisitionForApproval(submitRequisitionForApproval);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to submit the requisition");
            }
        }
        #endregion

        #region GetLearningAptitudesByTrId
        /// <summary>
        /// Get Learning Aptitudes By TrId
        /// </summary>
        /// <param name="trId"></param>
        /// <param name="roleMasterId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<RequisitionLearningAptitudeData>> GetLearningAptitudesByTrId(int trId, int roleMasterId)
        {
            List<RequisitionLearningAptitudeData> lstReqExpertiseAreas;

            try
            {
                lstReqExpertiseAreas = await new TalentRequisitionDetails().GetLearningAptitudesByTrId(trId, roleMasterId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get Learning Aptitudes.");
            }

            return lstReqExpertiseAreas;
        }
        #endregion

        #region SaveRequisitionLearningAptitude
        /// <summary>
        /// Save Requisition Learning Aptitude Details
        /// </summary>
        /// <param name="requisitionLearningAptitudeDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> SaveRequisitionLearningAptitude(RequisitionLearningAptitudeData requisitionLearningAptitudeDetails)
        {
            try
            {
                if (requisitionLearningAptitudeDetails == null)
                    throw new ArgumentNullException("Requisition Learning Aptitude cannot be null.");

                return await new TalentRequisitionDetails().SaveRequisitionLearningAptitude(requisitionLearningAptitudeDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to save requisition expertise areas.");
            }
        }
        #endregion

        #region AddRequisitionRoleSkills
        /// <summary>
        /// Add Requisition Role Skills.
        /// </summary>
        /// <param name="requisitionRoleSkills"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> AddRequisitionRoleSkills(List<RequisitionRoleSkills> requisitionRoleSkills)
        {
            try
            {
                if (requisitionRoleSkills == null)
                    throw new ArgumentNullException("requisitionRoleSkills cannot be null.");

                return await new TalentRequisitionDetails().AddRequisitionRoleSkills(requisitionRoleSkills);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetRequisitionRoleSkillsByID
        /// <summary>
        /// Get Requisition Role Skills.
        /// </summary>
        /// <param name="TRID"></param>
        /// <param name="RequistionRoleDetailID"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<RequisitionRoleSkills>> GetRequisitionRoleSkillsByID(int TRID, int RequistionRoleDetailID)
        {
            List<RequisitionRoleSkills> lstSkills;
            try
            {
                lstSkills = await new TalentRequisitionDetails().GetRequisitionRoleSkillsByID(TRID, RequistionRoleDetailID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkills;
        }
        #endregion

        #region GetRolesByTalentRequisitionId
        /// <summary>
        /// Get Roles by Talent Requisition ID.
        /// </summary>
        /// <param name="TalentRequisitionId"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetRolesByTalentRequisitionId(int TalentRequisitionId)
        {
            List<GenericType> lstRoles;
            try
            {
                lstRoles = await new TalentRequisitionDetails().GetRolesByTalentRequisitionId(TalentRequisitionId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        #region GetSkillDetailsByTalentRequisitionRoleId
        /// <summary>
        /// Get Skill Details by Talent requisition and RoleId.
        /// </summary>
        /// <param name="TalentRequisitionId"></param>
        /// <param name="RoleId"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<RequisitionSearchFilter> GetSkillDetailsByTalentRequisitionRoleId(int TalentRequisitionId, int RoleMasterId)
        {
            RequisitionSearchFilter searchFilter = new RequisitionSearchFilter();
            try
            {
                searchFilter = await new TalentRequisitionDetails().GetSkillDetailsByTalentRequisitionRoleId(TalentRequisitionId, RoleMasterId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return searchFilter;
        }
        #endregion

        #region GetRequisitionDetailsBySearch
        /// <summary>
        /// Get Requisition details by search criteria.
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns>List</returns>
        [HttpPost]
        [UserInfoActionFilter]
        public SkillSearchDetails GetRequisitionDetailsBySearch(RequisitionSearchFilter searchFilter)
        {
            SkillSearchDetails searchResult;
            try
            {
                searchResult = new TalentRequisitionDetails().GetRequisitionDetailsBySearch(searchFilter);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return searchResult;
        }
        #endregion

        #region ChecksDuplicateRoleAgainstTrId
        /// <summary>
        /// Checks Duplicate Role Against TrId
        /// </summary>
        /// <param name="requisitionRoleDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage ChecksDuplicateRoleAgainstTrId(RequisitionRoleDetails requisitionRoleDetails)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new TalentRequisitionDetails().ChecksDuplicateRoleAgainstTrId(requisitionRoleDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetTalentRequisitionApprovers
        /// <summary>
        /// Get talent requisition approvers list.
        /// </summary>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetTalentRequisitionApprovers()
        {
            List<GenericType> lstApprovers;
            try
            {
                lstApprovers = await new TalentRequisitionDetails().GetTalentRequisitionApprovers();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstApprovers;
        }
        #endregion

        #region GetTaggedEmployeesByTalentRequisitionId
        /// <summary>
        ///  Get Tagged Employees by Talent Requisition ID.
        /// </summary>
        /// <param name="TalentRequisitionId"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<EmployeeTagDetails>> GetTaggedEmployeesByTalentRequisitionId(int talentRequisitionId)
        {
            List<EmployeeTagDetails> lstTaggedEmployees;
            try
            {
                lstTaggedEmployees = await new TalentRequisitionDetails().GetTaggedEmployeesByTalentRequisitionId(talentRequisitionId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstTaggedEmployees;
        }
        #endregion

        #region DeleteTaggedEmployees
        /// <summary>
        /// DeleteTaggedEmployees
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteTaggedEmployees(List<int> Ids)
        {
            try
            {
                return await new TalentRequisitionDetails().DeleteTaggedEmployees(Ids);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region RejectTalentRequisitionByDeliveryHead
        /// <summary>
        /// Reject talent requisition by delivery head
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> RejectTalentRequisitionByDeliveryHead(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            try
            {
                if (talentRequisitionDetails == null)
                    throw new ArgumentNullException("TalentRequisitionDetails cannot be null.");

                talentRequisitionDetails.StatusId = Convert.ToInt32(TalentRequisitionStatusCodes.RejectedByDeliveryHead);
                return await new TalentRequisitionDetails().RejectTalentRequisition(talentRequisitionDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region RejectTalentRequisitionByFinance
        /// <summary>
        /// Reject talent requisition by finance
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> RejectTalentRequisitionByFinance(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            try
            {
                if (talentRequisitionDetails == null)
                    throw new ArgumentNullException("TalentRequisitionDetails cannot be null.");

                talentRequisitionDetails.StatusId = Convert.ToInt32(TalentRequisitionStatusCodes.RejectedByFinanceHead);
                return await new TalentRequisitionDetails().RejectTalentRequisition(talentRequisitionDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}
