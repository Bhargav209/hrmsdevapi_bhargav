﻿using AP.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class KRAPdfConfigurationController : BaseApiController
    {
        #region GetKRAPdfConfiguarationDetails
        /// <summary>
        /// GetKRAPdfConfiguarationDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]        
        public HttpResponseMessage GetKRAPdfConfiguarationDetails()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new KRAPdfConfiguarationDetails().GetKRAPdfConfiguarationDetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
