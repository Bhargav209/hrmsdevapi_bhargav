﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Controllers;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AP.Services
{
    public class CommonController : BaseApiController
    {

        #region GetBusinessValues
        /// <summary>
        /// GetBusinessValues
        /// </summary>
        /// <param name="valueKey"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetBusinessValues(string valueKey)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Common().GetBusinessValues(valueKey));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetStatusId
        /// <summary>
        /// Get Status ID
        /// </summary>
        /// <param name="category"></param>
        /// <param name="statusCode"></param>
        /// <returns>int</returns>
        [HttpGet]
        public HttpResponseMessage GetStatusId(string category, string statusCode)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Common().GetStatusId(category, statusCode));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetSkillsbySkillGroupID
        /// <summary>
        /// GetSkillsbySkillGroupID
        /// </summary>
        /// <param name="SkillGroupID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<SkillData>> GetSkillsbySkillGroupID(int SkillGroupID)
        {
            List<SkillData> lstSkills;
            try
            {

                lstSkills = await new Common().GetSkillsbySkillGroupID(SkillGroupID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return lstSkills;
        }
        #endregion

        #region GetAllocationPercentages
        /// <summary>
        /// GetAllocationPercentages
        /// </summary>        
        [HttpGet]
        public HttpResponseMessage GetAllocationPercentages()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Common().GetAllocationPercentages());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}