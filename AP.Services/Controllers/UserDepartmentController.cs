﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class UserDepartmentController : BaseApiController
    {
        #region CreateDepartment
        /// <summary>
        /// Create department
        /// </summary>
        /// <param name="departmentData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateDepartment(DepartmentData departmentData)
        {
            HttpResponseMessage returnObject = null;
            try
            {
                returnObject = Request.CreateResponse(new UserDepartment().CreateDepartment(departmentData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetUserDepartmentDetails
        /// <summary>
        /// GetUserDepartmentDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        //[Route("api/GetUserDepartmentDetails")]
        [HttpGet]
        public HttpResponseMessage GetUserDepartmentDetails()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserDepartment().GetUserDepartmentDetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region Department Head
        /// <summary>
        /// Get Department Heads
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //public HttpResponseMessage GetDepartmentHeads()
        //{

        //    HttpResponseMessage returnObject = null;

        //    try
        //    {
        //        returnObject = Request.CreateResponse(new UserDepartment().GetDepartmentHeads());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return returnObject;
        //}
        #endregion

        #region UpdateUserDepartmentDetails
        /// <summary>
        /// Update user department details
        /// </summary>
        /// <param name="departmentData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateUserDepartmentDetails(DepartmentData userDepartmentNew)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserDepartment().UpdateUserDepartmentDetails(userDepartmentNew));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetDepartmentDetails
        /// <summary>
        /// GetDepartmentDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        // This service was written for Angular2 application and is being consumed in Angular2 application only.
        public HttpResponseMessage GetDepartmentDetails(bool isActive = true)
        {

            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserDepartment().GetDepartmentDetails(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

    }
}
