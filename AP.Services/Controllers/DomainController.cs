﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class DomainController : BaseApiController
    {
        #region GetDomains
        /// <summary>
        /// Gets Domains Master Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DomainMasterData>> GetDomains()
        {
            List<DomainMasterData> lstDomains;
            try
            {
                lstDomains = await new DomainMaster().GetDomains();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstDomains;
        }
        #endregion

        #region CreateDomain
        /// <summary>
        /// Create a new Domain
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateDomain(DomainMasterData domainMasterData)
        {
            try
            {
                return await new DomainMaster().CreateDomain(domainMasterData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateDomain
        /// <summary>
        /// Update a Domain based on Domain ID
        /// </summary>
        /// <param name="UpdateDomain"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateDomain(DomainMasterData domainMasterData)
        {
            try
            {
                return await new DomainMaster().UpdateDomain(domainMasterData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}
