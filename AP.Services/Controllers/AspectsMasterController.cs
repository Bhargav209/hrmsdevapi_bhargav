﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class AspectsMasterController : BaseApiController
    {
        #region GetAspectsMaster
        /// <summary>
        /// Gets Aspect Master
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AspectData>> GetAspectsMaster()
        {
            List<AspectData> lstAspects;
            try
            {
                lstAspects = await new AspectsMaster().GetAspectsMaster();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstAspects;
        }
        #endregion

        #region CreateAspectMaster
        /// <summary>
        /// Create a new Aspect
        /// </summary>
        /// <param name="aspectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateAspectMaster(AspectData aspectData)
        {
            try
            {
                return await new AspectsMaster().CreateAspectMaster(aspectData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateAspectMaster
        /// <summary>
        /// Update a Aspect
        /// </summary>
        /// <param name="aspectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateAspectMaster(AspectData aspectData)
        {
            try
            {
                return await new AspectsMaster().UpdateAspectMaster(aspectData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteAspectMaster
        /// <summary>
        /// Delete a Aspect
        /// </summary>
        /// <param name="aspectId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> DeleteAspectMaster(int aspectId)
        {
            try
            {
                return await new AspectsMaster().DeleteAspectMaster(aspectId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}
