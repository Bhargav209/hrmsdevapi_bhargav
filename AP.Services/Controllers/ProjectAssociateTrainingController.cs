﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ProjectAssociateTrainingController : BaseApiController
    {
        #region GetAssociateSkillGapByEmpId
        /// <summary>
        /// GetAssociateSkillGapByEmpId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateSkillGapByEmpId(int empId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().GetAssociateSkillGapByEmpId(empId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjectTrainingsByProjectId
        /// <summary>
        /// GetProjectTrainingsByProjectId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectTrainingsByProjectId(int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().GetProjectTrainingsByProjectId(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetAssessedByProjectID
        /// <summary>
        /// GetAssessedByProjectID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssessedByProjectID(int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().GetAssessedByProjectID(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjectAssociateTrainings
        /// <summary>
        /// GetProjectAssociateTrainings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectAssociateTrainings(int empId, int projectId, int? projectAssociateTrainingId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().GetProjectAssociateTrainings(empId, projectId, projectAssociateTrainingId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region SaveProjectAssociateTrainings
        /// <summary>
        /// SaveProjectAssociateTrainings
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveProjectAssociateTrainings(List<ProjectAssociateTrainingData> projectAssociateTraining)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().SaveProjectAssociateTrainings(projectAssociateTraining));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

        #region UpdateProjectAssociateTrainings
        /// <summary>
        /// UpdateProjectAssociateTrainings
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateProjectAssociateTrainings(List<ProjectAssociateTrainingData> projectAssociateTraining)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().UpdateProjectAssociateTrainings(projectAssociateTraining));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

        #region GetProficiencyLevels
        /// <summary>
        /// GetProficiencyLevels
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProficiencyLevels()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectAssociateTrainingDetails().GetProficiencyLevels());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
    }
}
