﻿using System;
using System.Net;
using System.Web.Http;
using System.Net.Http;
using System.Resources;
using AP.API;
using AP.Utility;
using AP.DomainEntities;
using AP.Services.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.Services.Controllers
{
    public class ProjectController : BaseApiController
    {
        #region GetProject
        /// <summary>
        /// GetProject
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        [HttpGet]
        public HttpResponseMessage GetProject(int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();
            try
            {               
                returnObject = Request.CreateResponse(new ProjectDetails().GetProject(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return returnObject;
        }
        #endregion

        #region PutProject
        /// <summary>
        /// Update project details
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> PutProject(ProjectData project)
        {          
            try
            {
                if (project == null)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().UpdateProject(project);                
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }            
        }
        #endregion
        #region DeleteProjectDetails
        /// <summary>
        /// DeleteProjectDetails
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        [Authorize(Roles = "Program Manager, Department Head")]
        public async Task<bool> DeleteProjectDetails(int projectId)
        {
            try
            {
                if (projectId == 0)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().DeleteProjectDetails(projectId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        #region PostProject
        /// <summary>
        ///Adds project details
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> PostProject(ProjectData project)
        {          
            try
            {
                if (project == null)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().AddProjectDetails(project);                
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }           
        }
        #endregion
        #region SubmitForApproval
        /// <summary>
        ///Submit For Approval
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> SubmitForApproval(int projectId, string userRole, int employeeId)
        {
            try
            {
                if (projectId == 0)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().SubmitForApproval(projectId, userRole,employeeId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        #region ApproveOrRejectByDH
        /// <summary>
        ///Approve Or Reject By DH
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> ApproveOrRejectByDH(int projectId,string status,int employeeId)
        {
            try
            {
                if (status == null || projectId == 0)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().ApproveOrRejectByDH(projectId, status,employeeId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        #region GetAssociates
        /// <summary>
        /// GetAssociates
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetAssociates()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetAssociates());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region Allocate associate to Project
        /// <summary>
        ///Allocate a associate to project
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        public HttpResponseMessage ResourceAllocate(AssociateAllocationDetails associateDetails)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                if (associateDetails == null)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }

                returnObject = Request.CreateResponse(new ProjectDetails().ResourceAllocate(associateDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjectRelease
        /// <summary>
        /// GetProjectRelease
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetProjectDetails(int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetProjectDetails(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetPercentUtilization
        /// <summary>
        /// GetPercentUtilization
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetPercentUtilization(int projectId, int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetPercentUtilization(projectId, employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region ReleaseAssociate
        /// <summary>
        /// ReleaseAssociate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public HttpResponseMessage ReleaseAssociate(AssociateAllocationDetails associateAllocationDetails)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().ReleaseAssociate(associateAllocationDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjectsList
        /// <summary>
        /// GetProjectsList
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        //[Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage GetProjectsList(string userRole, int employeeId, string dashboard)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();
            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetProjectsList(userRole, employeeId,dashboard));                               
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return returnObject;
        }


        #endregion

        #region GetProjectsStatuses
        /// <summary>
        /// GetProjectsStatuses
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        //[Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage GetProjectsStatuses()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();
            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetProjectsStatuses());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return returnObject;
        }


        #endregion

        #region GetTalentPools
        /// <summary>
        /// GetProjectsList
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetTalentPools()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetTalentPools());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }


        #endregion

        #region GetProjectsListByTypeId
        /// <summary>
        /// GetProjectsList
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetProjectsListByTypeId(int projectTypeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetProjectsListByTypeId(projectTypeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }


        #endregion


        #region GetEmpList
        /// <summary>
        /// GetEmpList
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetEmpList()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetEmpList());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetRolesList
        /// <summary>
        /// GetRolesList
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetRolesList(int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetRolesList(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }

        /// <summary>
        /// Get all active Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRolesList()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetRolesList());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetReportingManager
        /// <summary>
        /// GetReportingManager
        /// </summary>
        /// <param name="reportingManagerId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetReportingManager(int reportingManagerId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetReportingManager(reportingManagerId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjectTypes
        /// <summary>
        /// Get Project Types
        /// </summary>
        /// 
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public HttpResponseMessage GetProjectTypes()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetProjectTypes());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

        #region GetCustomers
        /// <summary>
        /// Get Customers
        /// </summary>
        /// 
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public HttpResponseMessage GetCustomers()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();
            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetCustomers());                            
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return returnObject;
        }
        #endregion

        #region AssignProgramManagerToProject
        /// <summary>
        /// Assign Program Manager To Project
        /// </summary>
        /// 
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public HttpResponseMessage AssignProgramManagerToProject(string userRole, int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {

                returnObject = Request.CreateResponse(new ProjectDetails().AssignProgramManagerToProject(userRole, employeeId));               
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return returnObject;
        }
        #endregion

        #region GetManagers
        /// <summary>
        /// Get Managers
        /// </summary>
        /// 
        [HttpGet]
        public HttpResponseMessage GetManagers()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetManagersList());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetManagers And Leads
        /// <summary>
        /// Get Managers
        /// </summary>
        /// 
        [HttpGet]
        public HttpResponseMessage GetManagersAndLeads()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetManagersAndLeads());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetReportingDetailsByProjectId
        /// <summary>
        /// GetReportingDetailsByProjectId
        /// </summary>
        /// 
        [HttpGet]
        public HttpResponseMessage GetReportingDetailsByProjectId(int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetReportingDetailsByProjectId(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region ProjectDataSearch
        /// <summary>
        /// ProjectDataSearch
        /// </summary>
        /// <param name="searchData"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ProjectDataSearch(SearchFilter searchFilter)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectDetails().ProjectDataSearch(searchFilter));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetEmpTalentPool
        /// <summary>
        /// GetEmpTalentPool
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetEmpTalentPool(int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetEmpTalentPool(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetEmpAllocationHistory
        /// <summary>
        /// GetEmpAllocationHistory
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetEmpAllocationHistory(int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetEmpAllocationHistory(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetManagerList
        /// <summary>
        /// GetManagerList
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetManagerList()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetManagerList());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region SaveManagersToProject
        /// <summary>
        /// SaveManagersToProject
        /// </summary>
        /// <param name="projectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveManagersToProject(ProjectData projectData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectDetails().SaveManagersToProject(projectData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateReportingManagerToAssociate
        /// <summary>
        /// Updates the reporting manager for a associate
        /// </summary>
        /// <param name="projectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateReportingManagerToAssociate(ProjectData projectData, bool isDelivery)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectDetails().UpdateReportingManagerToAssociate(projectData, isDelivery));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        /// <summary>
        /// Get Active Projects
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ProjectData>> GetActiveProjects()
        {
            List<ProjectData> lstProjects;
            try
            {
                lstProjects = await new ProjectDetails().GetActiveProjects();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstProjects;
        }

        /// <summary>
        /// Get Employees By ProjectsID
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetEmployeesByProjectID(int projectID)
        {
            List<GenericType> lstEmployees;
            try
            {
                lstEmployees = await new ProjectDetails().GetEmployeesByProjectID(projectID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmployees;
        }

        #region GetEmployees
        /// <summary> 
        /// Gets list of associates by search string
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetEmployees(string searchString)
        {
            List<GenericType> lstEmails;
            try
            {
                lstEmails = await new ProjectDetails().GetEmployees(searchString);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmails;
        }
        #endregion

        #region Get All Leads and Managers
        /// <summary> 
        /// Gets list of Leads and Managers by search string
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetAllLeadsManagers(string searchString)
        {
            List<GenericType> allLeadsManagers;
            try
            {
                allLeadsManagers = await new ProjectDetails().GetAllLeadsManagers(searchString);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return allLeadsManagers;
        }
        #endregion

        #region GetManagerandLeadByProjectID
        /// <summary>
        /// To get the lead name by project ID and employee ID
        /// </summary>
        /// <param name="projectID"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetManagerandLeadByProjectID(int projectID, int employeeID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectDetails().GetManagerandLeadByProjectID(projectID, employeeID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetSowsByProjectId
        /// <summary>
        /// GetSowsByProjectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage GetSowsByProjectId(int projectId)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetSowsByProjectId(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
        #region GetAddendumDetailsById
        /// <summary>
        /// GetAddendumDetailsById
        /// </summary>
        /// <param name="Id,projectId,roleName"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage GetAddendumDetailsById(int id,int projectId,string roleName)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetAddendumDetailsById(id, projectId, roleName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
        #region GetAddendums
        /// <summary>
        /// GetAddendums
        /// </summary>
        /// <param name="Id,projectid"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage GetAddendums(int id, int projectId)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetAddendums(id, projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetSOWDetails
        /// <summary>
        /// GetSOWDetails
        /// </summary>
        /// <param name="Id,projectid,rolename"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage GetSOWDetails(int id, int projectId, string roleName)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().GetSOWDetails(id, projectId, roleName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

        #region PostSOW
        /// <summary>
        ///Adds SOW details
        /// </summary>
        /// <param name="sowSign"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Program Manager, Department Head")]
        public HttpResponseMessage PostSOW(SOWSignData sowSign)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectDetails().PostSOW(sowSign));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion  

        #region DeleteSOW
        /// <summary>
        /// DeleteSOW
        /// </summary>
        /// <param name="sowId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        //[Authorize(Roles = "Program Manager, Department Head")]
        public async Task<bool> DeleteSOW(int Id)
        {
            try
            {
                if (Id == 0)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().DeleteSOW(Id);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region PutProject
        /// <summary>
        /// Update project details
        /// </summary>
        /// <param name="sowAndAddendumData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> UpdateSowAndAddendumDetails(SOWSignData sowAndAddendumData)
        {
            try
            {
                if (sowAndAddendumData == null)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ProjectDetails().UpdateSowAndAddendumDetails(sowAndAddendumData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region HasActiveClientBillingRoles
        ///<summary>
        ///close project
        ///</summary>
        /// /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> HasActiveClientBillingRoles( int projectId )
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
               return  await new ProjectDetails().HasActiveClientBillingRoles(projectId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

        }
        #endregion
        #region CloseProject
        ///<summary>
        ///close project
        ///</summary>
        /// /// <param name="ProjectData"></param>
        /// <returns></returns>

        [HttpPost]
        [UserInfoActionFilter]
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        public async Task<int> CloseProject(ProjectData projectData)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                return await new ProjectDetails().CloseProject(projectData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

        }
        #endregion
    }
}