﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class ProjectTypeMasterController : ApiController
    {
        #region CreateProjectType
        /// <summary>
        /// Create Project Type
        /// </summary>
        /// <param name="projectType"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateProjectType(ProjectTypeDetails projectType)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectTypeMaster().CreateProjectType(projectType));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProjectTypes
        /// <summary>
        /// Get Project Type Details
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectTypes(bool isActive=true)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectTypeMaster().GetProjectTypes(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateProjectType
        /// <summary>
        /// Update status master details
        /// </summary>
        /// <param name="projectType"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateProjectType(ProjectTypeDetails projectType)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectTypeMaster().UpdateProjectType(projectType));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
