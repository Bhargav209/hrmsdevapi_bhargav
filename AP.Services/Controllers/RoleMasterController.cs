﻿using System.Net.Http;
using System.Web.Http;
using System;
using System.Net;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class RoleMasterController : BaseApiController
    {
        #region CreateUserRole
        /// <summary>
        /// Create role master details
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateUserRole(RoleData roleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new RoleMaster().CreateUserRole(roleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoleMasterDetails
        /// <summary>
        /// Get role Master Details in Grid
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoleMasterDetails(bool isActive=true,int? departmentId = null)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new RoleMaster().GetRoleMasterDetails(isActive,departmentId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoleMasterDetailsByProjectId
        /// <summary>
        /// Get role Master Details by project id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoleMasterDetailsByProjectId(int projectId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new RoleMaster().GetRoleMasterDetailsByProjectId(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        //#region GetRoleMasterDetailsByID
        /// <summary>
        /// Get Role Master Deatils By Id
        /// </summary>
        /// <param name = "roleId" ></ param >
        /// < returns ></ returns >
        //[HttpGet]
        //public HttpResponseMessage GetRoleMasterDetailsByID(int id)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new RoleMaster().GetRoleMasterDetailByRoleId(id));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        //#region UpdateRoleMasterDetails
        ///// <summary>
        ///// Update RoleMaster Detils
        ///// </summary>
        ///// <param name="roleData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage UpdateRoleMasterDetails(RoleData roleData)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new RoleMaster().UpdateRoleMasterDetails(roleData));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion
    }
}