﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class BusinessRuleController : ApiController
    {
        #region CreateBusinessRule
        /// <summary>
        /// CreateBusinessRule
        /// </summary>
        /// <param name="businessRuleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateBusinessRule(BusinessRuleData businessRuleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new BusinessRule().CreateBusinessRule(businessRuleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateBusinessRule
        /// <summary>
        /// UpdateBusinessRule
        /// </summary>
        /// <param name="businessRuleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateBusinessRule(BusinessRuleData businessRuleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new BusinessRule().UpdateBusinessRule(businessRuleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetBusinessRuleDetails
        /// <summary>
        /// GetBusinessRuleDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="businessRuleCode"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetBusinessRuleDetails(bool isActive = true,string businessRuleCode = null)
        {
            HttpResponseMessage httpResponse = null;

            try
            {
                httpResponse = Request.CreateResponse(new BusinessRule().GetBusinessRules(isActive,businessRuleCode));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion
    }
}
