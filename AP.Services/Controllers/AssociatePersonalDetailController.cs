﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.DomainEntities;
using AP.API;
using AP.Utility;
using AP.Services.Filters;
using System.Collections.Generic;

namespace AP.Services.Controllers
{
    public class AssociatePersonalDetailController : BaseApiController
    {
        #region PersonalDetailsOperations
        /// <summary>
        /// PersonalDetailsOperations
        /// </summary>
        /// <param name="personalDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddPersonalDetails(PersonalDetails personalDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                List<GenericType> duplicateFields = new AssociatePersonalDetails().ValidatePersonalData(personalDetails);
                if (duplicateFields.Count == 0)
                    httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().AddPersonalDetails(personalDetails));
                else {
                    httpResponseMessage = Request.CreateResponse(duplicateFields);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdatePersonalDetails
        /// <summary>
        /// UpdatePersonalDetails
        /// </summary>
        /// <param name="personalDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdatePersonalDetails(PersonalDetails personalDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                List<GenericType> duplicateFields = new AssociatePersonalDetails().ValidatePersonalData(personalDetails);
                if (duplicateFields.Count == 0)
                    httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().UpdatePersonalDetails(personalDetails));
                else
                    httpResponseMessage = Request.CreateResponse(duplicateFields);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetPersonalDetailsByID
        /// <summary>
        /// GetPersonalDetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetPersonalDetailsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().GetPersonalDetailsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAssociateDetails
        /// <summary>
        /// GetAssociateDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateDetails(int? departmentId = null)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().GetAssociateDetails(departmentId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }

        /// <summary>
        /// GetDepartmentHeadDetails
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        //[HttpGet]
        //public HttpResponseMessage GetDepartmentHeadDetails(int? departmentId = null)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().GetDepartmentHeadDetails(departmentId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}

        #endregion

        #region Get AssociateDetails By Search
        /// <summary>
        /// Get AssociateDetails By Search
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetAssociateDetailsBySearch(SearchFilter searchFilter)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().GetAssociateDetailsBySearch(searchFilter));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region Get AssociateDetails
        /// <summary>
        /// Get AssociateDetails 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateDetailsByEmpID(int empID, string roleName)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().GetAssociateDetailsByEmpID(empID, roleName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateAssociateStatus
        /// <summary>
        /// UpdateAssociateStatus
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateAssociateStatus(int employeeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociatePersonalDetails().UpdateAssociateStatus(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
