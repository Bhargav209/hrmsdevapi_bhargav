﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ProjectTrainingMasterController : BaseApiController
    {
        #region GetTrainingData
        /// <summary>
        /// GetTrainingData
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetTrainingData()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectTrainingMaster().GetTrainingData());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjects
        /// <summary>
        /// GetProjects
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjects()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectTrainingMaster().GetProjects());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetTrainingDataById
        /// <summary>
        /// GetTrainingDataById
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetTrainingDataById(int trainingId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectTrainingMaster().GetTrainingDataById(trainingId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region CreateNewTraining
        /// <summary>
        /// Create New Training for Project
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateNewTraining(ProjectTrainingMasterData newTraining)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectTrainingMaster().CreateNewTraining(newTraining));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

        #region UpdateTrainingDetails
        /// <summary>
        /// update training for Project
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateTrainingDetails(ProjectTrainingMasterData newTraining)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectTrainingMaster().UpdateTrainingDetails(newTraining));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetTrainingDataByProjectId
        /// <summary>
        /// GetTrainingDataById
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetTrainingDataByProjectId(int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectTrainingMaster().GetTrainingDataByProjectId(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

    }
}
