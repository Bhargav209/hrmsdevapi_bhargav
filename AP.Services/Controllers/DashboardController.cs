﻿using System;
using System.Net.Mail;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using AP.DomainEntities;
using AP.API;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using AP.Utility;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class DashboardController : BaseApiController
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        [HttpPost]
        public void SendEmail(Email email)
        {
            MailMessage objMailMessage = new MailMessage();
            System.Net.NetworkCredential objSMTPUserInfo =
                new System.Net.NetworkCredential();
            SmtpClient objSmtpClient = new SmtpClient();
            string smtpClientaddress = Convert.ToString(ConfigurationManager.AppSettings["SMTPClient"]);

            try
            {
                objMailMessage.From = new MailAddress(email.FromEmail);
                objMailMessage.To.Add(new MailAddress(email.ToEmail));
                objMailMessage.CC.Add(new MailAddress(email.CcEmail));
                objMailMessage.Subject = email.Subject;
                objMailMessage.Body = email.EmailBody;

                objSmtpClient = new SmtpClient(smtpClientaddress);
                objSmtpClient.UseDefaultCredentials = false;
                objSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendMail"]))
                    objSmtpClient.Send(objMailMessage);
            }
            catch (Exception ex)
            { throw ex; }

            finally
            {
                objMailMessage = null;
                objSMTPUserInfo = null;
                objSmtpClient = null;
            }
        }

        #region GetHRHeadDetails
        /// <summary>
        /// GetHRHeadDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetHRHeadDetails()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DashboardDetails().GetHRHeadDetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetEmployeeDetails
        /// <summary>
        /// GetEmployeeDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeSkillDetails>> GetSkillDetails()
        {
            List<EmployeeSkillDetails> lstSkill;

            try
            {
                lstSkill = await new DashboardDetails().GetSkillDetails();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return lstSkill;
        }
        #endregion
        #region Create Employee Skill
        /// <summary>
        /// Create Appreciation
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateEmployeeSkill(EmployeeSkillDetails employeeSkill)
        {
            try
            {
                return await new DashboardDetails().CreateEmployeeSkill(employeeSkill);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetHRADetails
        /// <summary>
        /// Show employee details to HRA
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetHRADetails()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DashboardDetails().GetHRADetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUtilizeReports
        /// <summary>
        /// GetUtilizeReports
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetUtilizeReports(SearchFilter searchFilter)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new DashboardDetails().GetUtilizeReports(searchFilter));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region SaveReportData
        /// <summary>
        /// SaveReportData
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SaveReportData(List<AssociateReport> associateAllocationList)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new DashboardDetails().SaveReportData(associateAllocationList));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetActiveSkillsData
        /// <summary>
        /// Get active and approved Skill details from system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetActiveSkillsData()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new DashboardDetails().GetActiveSkillsData());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
        
        #region GetFinanceReport
        /// <summary>
        /// GetFinanceReport
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetFinanceReport(SearchFilter searchFilter)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new DashboardDetails().GetFinanceReport(searchFilter));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region SaveFinanceReportData
        /// <summary>
        /// SaveReportData
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SaveFinanceReportData(List<FinanceReport> associateAllocationList)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new DashboardDetails().SaveFinanceReportData(associateAllocationList));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetSkillGroups
        /// <summary>
        /// Get active SkillGroups from system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSkillGroups()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new DashboardDetails().GetSkillGroups());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetEmployeeCodeByEmpId
        /// <summary>
        /// Get EmployeeCode By EmpId
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmployeeCodeByEmpId(int empId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DashboardDetails().GetAssociateCodeByEmpID(empId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region TalentRequisitionDeliveryHeadDashboard


        #region GetPendingRequisitionsForApproval
        /// <summary>
        /// Get pending for approval requisitions in delivery head dashboard.
        /// </summary>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetPendingRequisitionsForApproval(int EmployeeId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitions;
            try
            {
                lstRequisitions = await new DashboardDetails().GetPendingRequisitionsForApproval(EmployeeId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRequisitions;
        }
        #endregion

        #region GetRolesAndPositionsByRequisitionId
        /// <summary>
        /// Get delivery head dashboard.
        /// </summary>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetRolesAndPositionsByRequisitionId(int talentRequisitionId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitions;
            try
            {
                lstRequisitions = await new DashboardDetails().GetRolesAndPositionsByRequisitionId(talentRequisitionId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRequisitions;
        }
        #endregion

        #region GetTaggedEmployeeByTRAndRoleId
        /// <summary>
        /// Get tagged employee details by trId and roleId.
        /// </summary>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetTaggedEmployeeByTRAndRoleId(int talentRequisitionId, int roleId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstEmployees;
            try
            {
                lstEmployees = await new DashboardDetails().GetTaggedEmployeeByTRAndRoleId(talentRequisitionId, roleId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmployees;
        }
        #endregion

        #region GetFinanceHeadList
        /// <summary>
        /// Gets list of finance heads.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetFinanceHeadList()
        {
            List<GenericType> lstFinanceHeads;
            try
            {
                lstFinanceHeads = await new DashboardDetails().GetFinanceHeadList();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstFinanceHeads;
        }
        #endregion

        #region ApproveTalentRequisition
        /// <summary>
        /// Approve talent requisition by delivery head
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> ApproveTalentRequisition(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            try
            {

                if (talentRequisitionDetails == null)
                    throw new ArgumentNullException("TalentRequisitionDetails cannot be null.");

                return await new DashboardDetails().ApproveTalentRequisition(talentRequisitionDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region ApproveTalentRequisitionByFinance
        /// <summary>
        /// Approve talent requisition by finance head.
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> ApproveTalentRequisitionByFinance(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            try
            {

                if (talentRequisitionDetails == null)
                    throw new ArgumentNullException("TalentRequisitionDetails cannot be null.");

                return await new DashboardDetails().ApproveTalentRequisitionByFinance(talentRequisitionDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetRequisitionWorkflowByTRId
        /// <summary>
        /// Get requisition workflow by trid.
        /// </summary>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetRequisitionWorkflowByTRId(int talentRequisitionId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitionHistory;
            try
            {
                lstRequisitionHistory = await new DashboardDetails().GetRequisitionWorkflowByTRId(talentRequisitionId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRequisitionHistory;
        }
        #endregion

        #endregion

        #region TalentRequisitionFinanceHeadDashboard

        #region GetRequisitionsForApprovalForFinance
        /// <summary>
        /// Get pending for approval requisitions in finance head dashboard.
        /// </summary>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetRequisitionsForApprovalForFinance(int employeeId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitions;
            try
            {
                lstRequisitions = await new DashboardDetails().GetRequisitionsForApprovalForFinance(employeeId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRequisitions;
        }
        #endregion

        #endregion

    }
}