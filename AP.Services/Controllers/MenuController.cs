﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.Services.Attributes;
using AP.Services.Filters;
using AP.DomainEntities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AP.Utility;

namespace AP.Services.Controllers
{
    public class MenuController : BaseApiController
    {
        /// <summary>
        /// Get Menu Details by logged in user role
        /// </summary>
        /// <param name="email ID"></param>
        /// <returns></returns>
        [HttpGet]
        [GzipCompression]
        public HttpResponseMessage GetMenuDetails(string roleName)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Menu().GetMenuDetails(roleName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }

        #region GetParentMenus
        /// <summary>
        /// GetParentMenus
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetParentMenus()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Menu().GetParentMenus());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region AddMenu
        /// <summary>
        /// AddMenu
        /// </summary>
        /// <param name="menuData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddMenu(MenuData menuData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (menuData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new Menu().AddMenu(menuData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateMenu
        /// <summary>
        /// UpdateMenu
        /// </summary>
        /// <param name="menuData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateMenu(MenuData menuData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (menuData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new Menu().UpdateMenu(menuData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetMenus
        /// <summary>
        /// GetMenus
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetMenus()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Menu().GetMenus());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region AddMenuRoles
        /// <summary>
        /// AddMenuRoles
        /// </summary>
        /// <param name="menuData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddMenuRoles(MenuRoleData menuRoleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (menuRoleData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new Menu().AddMenuRole(menuRoleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateMenuRole
        /// <summary>
        /// UpdateMenuRole
        /// </summary>
        /// <param name="menuRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateMenuRole(MenuRoleData menuRoleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (menuRoleData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new Menu().UpdateMenuRole(menuRoleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetMenuTitles
        /// <summary>
        /// GetMenuTitles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetMenuTitles()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Menu().GetMenuTitles());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// GetRoles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoles()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Menu().GetRoles());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetTargetMenuRoles
        /// <summary>
        /// GetTargetMenuRoles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Menus>> GetTargetMenuRoles(int RoleId)
        {

            try
            {
                return await new Menu().GetTargetMenuRoles(RoleId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion


        #region GetSourceMenuRoles
        /// <summary>
        /// GetSourceMenuRoles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Menus>> GetSourceMenuRoles(int RoleId)
        {

            try
            {
                return await new Menu().GetSourceMenuRoles(RoleId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region AddTargetMenuRoles
        /// <summary>
        /// AddTargetMenuRoles
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> AddTargetMenuRoles(MenuRoles MenuRoles)
        {
            try
            {
                return await new Menu().AddTargetMenuRoles(MenuRoles);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}

