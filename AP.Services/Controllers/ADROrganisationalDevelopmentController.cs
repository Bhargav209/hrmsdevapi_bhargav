﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ADROrganisationalDevelopmentController : ApiController
    {
        #region GetOrganisationDevelopment
        /// <summary>
        /// Gets GetOrganisation Development Master Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ADROrganisationDevelopmentData>> GetADROrganisationDevelopment(int financialYearId)
        {
            List<ADROrganisationDevelopmentData> lstOrgDevActivities;
            try
            {
                lstOrgDevActivities = await new ADROrganisationDevelopment().GetADROrganisationDevelopment(financialYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstOrgDevActivities;
        }
        #endregion

        #region CreateADROrganisationDevelopment
        /// <summary>
        /// Create a new ADROrganisationDevelopment
        /// </summary>
        /// <param name="organisationDevelopmentData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateADROrganisationDevelopment(ADROrganisationDevelopmentData organisationDevelopmentData)
        {
            try
            {
                return await new ADROrganisationDevelopment().CreateADROrganisationDevelopment(organisationDevelopmentData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateADROrganisationDevelopment
        /// <summary>
        /// Update ADROrganisationDevelopment
        /// </summary>
        /// <param name="organisationDevelopmentData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateADROrganisationDevelopment(ADROrganisationDevelopmentData organisationDevelopmentData)
        {
            try
            {
                return await new ADROrganisationDevelopment().UpdateADROrganisationDevelopment(organisationDevelopmentData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion



    }
}