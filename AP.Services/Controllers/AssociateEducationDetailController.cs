﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.API;
using AP.Utility;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class AssociateEducationDetailController : BaseApiController
    {
        #region UpdateEducationDetails
        /// <summary>
        /// Method to add/update education details.
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveEducationDetails(UserDetails details)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                if (details == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new AssociateEducationDetails().SaveEducationDetails(details));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetEducationDetailsByID
        /// <summary>
        /// To get education details based on employeeId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEducationDetailsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateEducationDetails().GetEducationDetailsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
