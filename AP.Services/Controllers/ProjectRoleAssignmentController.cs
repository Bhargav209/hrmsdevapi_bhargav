﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using Newtonsoft.Json;

namespace AP.Services.Controllers
{
    public class ProjectRoleAssignmentController : ApiController
    {
        #region AssignProjectRole
        /// <summary>
        /// Service call for assigning an associate's project role
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        //public HttpResponseMessage AssignProjectRole(ProjectRoleData roleAssignmentData,string statusCode)
        public HttpResponseMessage AssignProjectRole(dynamic data)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                ProjectRoleData roleAssignmentData = JsonConvert.DeserializeObject<ProjectRoleData>(data.ProjectRoleData.ToString());
                string status = data.Status;
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().AssignProjectRole(roleAssignmentData, status));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.InnerException?.InnerException?.Message ?? ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoleNotifications
        /// <summary>
        /// GetRoleNotifications
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoleNotifications(int employeeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().GetRoleNotifications(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion       

        #region RoleApproval
        /// <summary>
        /// RoleApproval
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage RoleApproval(dynamic data)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                ProjectRoleData roleAssignmentData = JsonConvert.DeserializeObject<ProjectRoleData>(data.ProjectRoleData.ToString());
                string status = data.Status;
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().RoleApproval(roleAssignmentData, status));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAssignedRoles
        /// <summary>
        /// GetAssignedRoles
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssignedRoles(int projectId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().GetAssignedRoles(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region RoleRejection
        /// <summary>
        /// RoleRejection
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage RoleRejection(dynamic data)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                ProjectRoleData roleAssignmentData = JsonConvert.DeserializeObject<ProjectRoleData>(data.ProjectRoleData.ToString());
                string status = data.Status;
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().RejectRole(roleAssignmentData, status));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region CountRoleNotifications
        /// <summary>
        /// CountRoleNotifications
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage CountRoleNotifications()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().CountRoleNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProjectManagerOrLeadId
        /// <summary>
        /// GetProjectManagerOrLeadId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectManagerOrLeadId(int employeeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().GetProjectManagerOrLeadId(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRolesByProjectId
        /// <summary>
        /// GetRolesByProjectId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRolesByProjectId(int projectId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().GetRolesByProjectId(projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoleMasterDetailByRoleId
        /// <summary>
        /// GetRoleMasterDetailByRoleId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoleMasterDetailByRoleId(int roleId, int projectId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectRoleAssignment().GetRoleMasterDetailByRoleId(roleId,projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
