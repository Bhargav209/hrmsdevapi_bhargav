﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class SkillGroupController : BaseApiController
    {
        #region CreateSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SkillGroupDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateSkillGroup(SkillGroupDetails skillGroupDetails)
        {
            try
            {
                if (skillGroupDetails == null)
                    throw new ArgumentNullException("SkillGroupDetails cannot be null.");

                return await new SkillGroup().CreateSkillGroup(skillGroupDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SkillGroupDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateSkillGroup(SkillGroupDetails SkillGroupDetails)
        {
            try
            {
                if (SkillGroupDetails == null)
                    throw new ArgumentNullException("SkillGroupDetails cannot be null.");

                return await new SkillGroup().UpdateSkillGroup(SkillGroupDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SkillGroupDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteSkillGroup(SkillGroupDetails skillGroupDetails)
        {
            try
            {
                if (skillGroupDetails == null)
                    throw new ArgumentNullException("SkillGroupDetails cannot be null.");

                return await new SkillGroup().DeleteSkillGroup(skillGroupDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetsSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<SkillGroupDetails>> GetsSkillGroup()
        {
            List<SkillGroupDetails> lstSkillGroups;
            try
            {
                lstSkillGroups = await new SkillGroup().GetsSkillGroup();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkillGroups;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<SkillGroupDetails>> GetSkillGroupByCompetencyAreaID(int competencyAreaID)
        {
            List<SkillGroupDetails> lstSkillGroups;
            try
            {
                lstSkillGroups = await new SkillGroup().GetSkillGroupByCompetencyAreaID(competencyAreaID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkillGroups;
        }
        #endregion
    }
}
