﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class UserGradeController : BaseApiController
    {
        #region CreateGrade
        /// <summary>
        /// Create grade
        /// </summary>
        /// <param name="gradeData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateGrade(GradeData gradeData)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserGrades().CreateGrade(gradeData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region UpdateGrade
       /// <summary>
       /// Update grade
       /// </summary>
       /// <param name="gradeData"></param>
       /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateGrade(GradeData gradeData)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserGrades().UpdateGrade(gradeData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetGradesDetails
        /// <summary>
        /// GetGradesDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetGradesDetails(bool isActive=true)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserGrades().GetGradesDetails(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetGradesById
        /// <summary>
        /// GetGradesById
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetGradesById(int gradeId)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new UserGrades().GetGradesById(gradeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        } 
        #endregion
    }
}
