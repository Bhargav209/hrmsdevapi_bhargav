﻿using AP.API;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class SkillSearchController : ApiController
    {
        #region GetAllSkillDetails
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeSkillDetails>> GetAllSkillDetails(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                lstSkill = await new SkillSearchDetail().GetAllSkillDetails(empID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkill;
        }
        #endregion
        [HttpPost]
        public async Task<List<SkillSearchData>> getEmployeeDetailsBySkill(SkillSearchData skilldata)
        {
            List<SkillSearchData> internalBillingRoles;

            try
            {
                internalBillingRoles = await new SkillSearchDetail().GetEmployeesBySkill(skilldata);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return internalBillingRoles;
        }

        [HttpGet]
        public async Task<List<GenericType>> GetSkillsBySearchString(string searchString)
        {
            List<GenericType> skillsList;

            try
            {
                skillsList = await new AssociateSkills().GetSkillsBySearchString(searchString);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }


            return skillsList;
        }
        #region GetProjectDetailByEmployeeId
        /// <summary>
        /// Get Project Detail By EmployeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectDetailByEmployeeId(int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateSkills().GetProjectDetailByEmployeeId(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
    }
}
