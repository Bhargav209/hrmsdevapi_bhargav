﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using AP.API;

namespace AP.Services.Controllers
{
    public class FileUploadAndDownloadController : BaseApiController
    {
        #region GetFileUploadedData
        /// <summary>
        /// Get File Uploaded Data
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetFileUploadedData(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new FileUploadAndDownloadDetails().GetFileUploadedData(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GeneratePDFReport
        /// <summary>
        /// GeneratePDFReport
        /// </summary>
        /// <param name="empId"></param>
        [HttpGet]
        public HttpResponseMessage GeneratePDFReport(int empId)
        {

            try
            {
                var bytes = new DashboardDetails().GeneratePDFReport(empId);
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(bytes)
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PostFile
        /// <summary>
        /// PostFile
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PostFile()
        {
            HttpResponseMessage result = null;

            try
            {
                result = Request.CreateResponse(new FileUploadAndDownloadDetails().UploadFile());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return result;
        }
        #endregion

        #region DeleteFileUploadedData
        [HttpGet]
        public HttpResponseMessage DeleteFileUploadedData(int empID, int iD)
        {
            HttpResponseMessage result = null;

            try
            {
                result = Request.CreateResponse(new FileUploadAndDownloadDetails().DeleteFileUploadedData(empID, iD));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return result;
        }
        #endregion
    }
}
