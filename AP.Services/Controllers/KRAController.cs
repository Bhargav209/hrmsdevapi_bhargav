﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class KRAController : BaseApiController
    {
        #region GetRoleCategory
        /// <summary>
        /// Gets KRA role category
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetRoleCategory()
        {
            List<GenericType> lstKRARoleCategories;
            try
            {
                lstKRARoleCategories = await new KRA().GetRoleCategory();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRARoleCategories;
        }
        #endregion

        /// <summary>
        /// Gets KRA Role by CategoryID
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public async Task<List<GenericType>> GetKRARoleByCategoryID(int kRARoleCategoryID)
        //{
        //    List<GenericType> lstKRARoles;
        //    try
        //    {
        //        lstKRARoles = await new KRA().GetKRARoleByCategoryID(kRARoleCategoryID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //    return lstKRARoles;
        //}            

        /// <summary>
        /// Create a new KRA Set
        /// </summary>
        /// <param name="kraSetData">kraSetData data model</param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> CreateKRASet(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        return await new KRA().CreateKRASet(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        /// <summary>
        /// Gets KRA Set(s) by FinancialYearID
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public async Task<List<KRASetData>> GetKRASetByFinancialYearID(int financialYearID)
        //{
        //    List<KRASetData> lstKRAASet;
        //    try
        //    {
        //        lstKRAASet = await new KRA().GetKRASetByFinancialYearID(financialYearID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //    return lstKRAASet;
        //}

        /// <summary>
        /// Clone KRAs
        /// </summary>
        /// <param name="sourcefinancialYearID"></param>
        /// <param name="targetfinancialYearID"></param>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public async Task<bool> CloneKRAs(int sourcefinancialYearID, int targetfinancialYearID)
        //    {
        //        try
        //        {
        //            return await new KRA().CloneKRAs(sourcefinancialYearID, targetfinancialYearID);
        //}
        //        catch (Exception ex)
        //        {
        //            Log.LogError(ex, Log.Severity.Error, "");
        //            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //{
        //    Content = new StringContent(ex.Message),
        //                ReasonPhrase = "Warning"
        //            });
        //        }
        //    }

        /// <summary>
        /// Get KRA sets
        /// </summary>
        /// <param name="kRARoleCategoryID"></param>
        /// <param name="kRARoleID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public async Task<List<KRASetData>> GetKRASets(int kRARoleCategoryID, int kRARoleID, int financialYearID)
        //{
        //    List<KRASetData> lstKRAASet;
        //    try
        //    {
        //        lstKRAASet = await new KRA().GetKRASets(kRARoleCategoryID, kRARoleID, financialYearID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //    return lstKRAASet;
        //}

        /// <summary>
        /// Update a KRA Set
        /// </summary>
        /// <param name="kraSetData">kraSetData model</param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> UpdateKRASet(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        return await new KRA().UpdateKRASet(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        /// <summary>
        /// Delete KRA by kraSetID
        /// </summary>
        /// <param name="kRASetID"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> DeleteKRAByKRASetID(int kRASetID)
        //{
        //    try
        //    {
        //        return await new KRA().DeleteKRAByKRASetID(kRASetID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        /// <summary>
        /// Delete KRAAspect for a particular financial year      
        /// </summary>
        /// <param name="KRAAspectID"></param>
        /// <param name="FinancialYearID"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public bool DeleteKraAspectFromKRASet(List<KRASetData> kraSetData)
        //{
        //    try
        //    {
        //        return new KRA().DeleteKraAspectFromKRASet(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        #region SubmitKRAForApproval
        /// <summary>
        /// Submit KRASet for approval.
        /// </summary>
        /// <param name="kraWorkFlowData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> SubmitKRAForApproval(KRAWorkFlowData kraWorkFlowData)
        {
            try
            {
                kraWorkFlowData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForDepartmentHeadReview);
                return await new KRA().SubmitKRA(kraWorkFlowData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region ApproveKRA
        /// <summary>
        /// Approve KRA.
        /// </summary>
        /// <param name="KRAWorkFlowData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> ApproveKRA(KRAWorkFlowData kraWorkFlowData)
        {
            try
            {
                kraWorkFlowData.StatusID = Convert.ToInt32(Enumeration.KRA.Approved);
                kraWorkFlowData.ToEmployeeID = kraWorkFlowData.FromEmployeeID;
                return await new KRA().SubmitKRA(kraWorkFlowData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region SendBackForHRMReview
        /// <summary>
        /// Send Back For HRMReview
        /// </summary>
        /// <param name="KRAWorkFlowData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> SendBackForHRMReview(KRAWorkFlowData kraWorkFlowData)
        {
            try
            {
                kraWorkFlowData.StatusID = Convert.ToInt32(Enumeration.KRA.SendBackForHRMReview);
                return await new KRA().SubmitKRA(kraWorkFlowData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region SendtoHRHeadReview
        /// <summary>
        /// Send to HR Head Review
        /// </summary>
        /// <param name="KRAWorkFlowData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> SendtoHRHeadReview(KRAWorkFlowData kraWorkFlowData)
        {
            try
            {
                kraWorkFlowData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview);
                return await new KRA().SubmitKRA(kraWorkFlowData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region SendBacktoDepartmentHead
        /// <summary>
        /// Send Back to Department Head
        /// </summary>
        /// <param name="KRAWorkFlowData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> SendBacktoDepartmentHead(KRAWorkFlowData kraWorkFlowData)
        {
            try
            {
                kraWorkFlowData.StatusID = Convert.ToInt32(Enumeration.KRA.SendBackForDepartmentHeadReview);
                return await new KRA().SubmitKRA(kraWorkFlowData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        /// <summary>
        /// Reject KRASet.
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> RejectKRA(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.Rejected);
        //        return await new KRA().SubmitKRA(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        /// <summary>
        /// Get KRA History
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRAHistory()
        {
            List<KRASetData> lstKRAHistory;
            try
            {
                lstKRAHistory = await new KRA().GetKRAHistory();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAHistory;
        }

        /// <summary>
        /// Get KRA WorkFlow History
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRAWorkFlowHistory(int financialYearID)
        {
            List<KRASetData> lstKRAWorkFlowHistory;
            try
            {
                lstKRAWorkFlowHistory = await new KRA().GetKRAWorkFlowHistory(financialYearID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAWorkFlowHistory;
        }

        #region CloneKRAByFinancialYearId
        /// <summary>
        /// Clone KRAs by financial Year id
        /// </summary>
        /// <param name="cloneKRAData"></param>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CloneKRAs(CloneKRAData cloneKRAData)
        {
            try
            {
                return await new KRA().CloneKRAs(cloneKRAData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteKRARoleByFinancialYearAndRoleId
        /// <summary>
        /// DeleteKRARoleByFinancialYearAndRoleId
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteKRARoleByFinancialYearAndRoleId(int roleID, int financialYearId)
        {
            try
            {
                return await new KRA().DeleteKRARoleByFinancialYearAndRoleId(roleID, financialYearId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        /// <summary>
        /// SubmitKRAForReview
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<List<GenericType>> SubmitKRAForHRHeadReview(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview);
        //        return await new KRA().ReviewKRA(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        //#region SubmitKRAForDepartmentHeadReview
        /// <summary>
        /// SubmitKRAForReview
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<List<GenericType>> SubmitKRAForDepartmentHeadReview(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForDepartmentHeadReview);
        //        return await new KRA().ReviewKRA(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}
        //#endregion

        /// <summary>
        /// Approve KRA By Department Head/HR Head
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<List<GenericType>> ApproveKRA(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.Approved);
        //        return await new KRA().ReviewKRA(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        //#region SendBackForHRHeadReview
        ///// <summary>
        ///// Send additionally added KRA back to HR Head Review
        ///// </summary>
        ///// <param name="kraSetData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<List<GenericType>> SendBackForHRHeadReview(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.SendBackForHRHeadReview);
        //        return await new KRA().ReviewKRA(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}
        //#endregion

        //#region SendBackForHRMReview
        ///// <summary>
        ///// Send KRA back to HRA Review
        ///// </summary>
        ///// <param name="kraSetData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<List<GenericType>> SendBackForHRMReview(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.SendBackForHRMReview);
        //        return await new KRA().ReviewKRA(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}
        //#endregion

        /// <summary>
        /// Get KRA Roles For Review.
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRARolesForReview(int financialYearID, int departmentID)
        {
            try
            {
                return await new KRA().GetKRARolesForReview(financialYearID, departmentID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #region GetKRARolesForDH
        /// <summary>
        /// Get KRA Roles on dashboard by statusid.
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRARolesForDH(int financialYearID, int departmentID)
        {
            try
            {
                int statusID = Convert.ToInt32(Enumeration.KRA.SubmittedForDepartmentHeadReview);
                return await new KRA().GetKRARoles(financialYearID, departmentID, statusID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetKRARolesForHRHead
        /// <summary>
        /// Get KRA Roles on dashboard by statusid.
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRARolesForHRHead(int financialYearID, int departmentID)
        {
            try
            {
            //int statusID = Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview);
            int statusID = -1;
                return await new KRA().GetKRARoles(financialYearID, departmentID, statusID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        /// <summary>
        /// Submit KRA For MD Approval
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> SubmitKRAForMDApproval(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForMDApproval);
        //        return await new KRA().KRAForMDAction(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}


        /// <summary>
        /// Reject KRA By MD
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> RejectKRAByMD(KRASetData kraSetData)
        //{
        //    try
        //    {
        //        kraSetData.StatusID = Convert.ToInt32(Enumeration.KRA.Rejected);
        //        return await new KRA().KRAForMDAction(kraSetData);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        #region KRAPendingForReviewByHOD
        /// <summary>
        /// KRAPendingForReviewByHOD
        /// </summary>
        /// <param name="ToEmployeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> KRAPendingForReviewByHOD(int toEmployeeID)
        {
            List<KRASetData> lstPendingForReviewKRA;
            try
            {
                lstPendingForReviewKRA = await new KRA().KRAPendingForReviewByHOD(toEmployeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstPendingForReviewKRA;
        }
        #endregion

        #region KRAPendingForReviewByHRHead
        /// <summary>
        /// Get KRA Pending For Review By HRHead
        /// </summary>
        /// <param name="ToEmployeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> KRAPendingForReviewByHRHead(int toEmployeeID)
        {
            List<KRASetData> lstPendingForReviewKRA;
            try
            {
                lstPendingForReviewKRA = await new KRA().KRAPendingForReviewByHRHead(toEmployeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstPendingForReviewKRA;
        }
        #endregion

        #region GetKRADepartmentStatus
        /// <summary>
        /// GetKRADepartmentStatus
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRADepartmentStatus()
        {
            List<KRASetData> lstKRADepartmentStatus;
            try
            {
                lstKRADepartmentStatus = await new KRA().GetKRADepartmentStatus();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRADepartmentStatus;
        }
        #endregion

        #region GetKRADepartmentWorkFlow
        /// <summary>
        /// GetKRADepartmentWorkFlow
        /// </summary>
        /// <param name="FinancialYearID"></param>
        /// <param name="DepartmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> GetKRADepartmentWorkFlow(int financialYearID, int departmentID)
        {
            List<KRASetData> lstKRADepartmentWorkFlow;
            try
            {
                lstKRADepartmentWorkFlow = await new KRA().GetKRADepartmentWorkFlow(financialYearID, departmentID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRADepartmentWorkFlow;
        }
        #endregion

        #region KRAPendingForReviewByMD
        /// <summary>
        /// KRAPendingForReviewByMD
        /// </summary>
        /// <param name="toEmployeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRASetData>> KRAPendingForReviewByMD(int employeeID)
        {
            List<KRASetData> lstPendingForReviewKRA;
            try
            {
                lstPendingForReviewKRA = await new KRA().KRAPendingForReviewByMD(employeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstPendingForReviewKRA;
        }
        #endregion

        #region CreateKRAForRole
        /// <summary>
        /// Create KRA for role
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateKRAByHRHead(KRARoleData kraRoleData)
        {
            try
            {
            //kraRoleData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview);
            kraRoleData.StatusID = -1;
                return await new KRA().CreateKRAForRole(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        public async Task<bool> CreateKRAByDepartmentHead(KRARoleData kraRoleData)
        {
            try
            {
            //kraRoleData.StatusID = Convert.ToInt32(Enumeration.KRA.SendBackForHRHeadReview);
            kraRoleData.StatusID = -1;
            return await new KRA().CreateKRAForRoleByDepartmentHead(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateKRARoleMetric
        /// <summary>
        /// Update a KRA Role Metric
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateKRARoleMetricByDepartmentHead(KRARoleData kraRoleData)
        {
            try
            {
            //kraRoleData.StatusID = Convert.ToInt32(Enumeration.KRA.SendBackForHRHeadReview);
            kraRoleData.StatusID = -1;
                return await new KRA().UpdateKRARoleMetricByDepartmentHead(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        /// <summary>
        /// Update a KRA Role Metric
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateKRARoleMetricByHRHead(KRARoleData kraRoleData)
        {
            try
            {
            //kraRoleData.StatusID = Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview);
            kraRoleData.StatusID = -1;
                return await new KRA().UpdateKRARoleMetricByHRHead(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region GetDepartmentsByDepartmentHeadID
        /// <summary>
        /// GetDepartmentDetailsByEmployeeID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetDepartmentsByDepartmentHeadID(int employeeID)
        {
            List<GenericType> lstDepartmentDetails;
            try
            {
                lstDepartmentDetails = await new KRA().GetDepartmentsByDepartmentHeadID(employeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstDepartmentDetails;
        }
        #endregion

        #region GetLoggedInUserRoles
        /// <summary>
        /// GetLoggedInUserRoles
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetLoggedInUserRoles(int employeeID, int finanicalYearId)
        {
            List<GenericType> lstRoles;
            try
            {
                lstRoles = await new KRA().GetLoggedInUserRoles(employeeID, finanicalYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        #region GetKRAWorkFlowPendingWithEmployeeID
        /// <summary>
        /// Returns the employee ID with whom KRA Work Flow is pending.
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <param name="financialYearID"></param>
        /// <returns>employee ID</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public int GetKRAWorkFlowPendingWithEmployeeID(int financialYearID, int departmentID)
        {
            try
            {
                return new KRA().GetKRAWorkFlowPendingWithEmployeeID(financialYearID, departmentID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region CreateKRADefinition
        /// <summary>
        /// creates kra definition
        /// </summary>
        /// <param name="kraDefinitionData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public int CreateKRADefinition(KRADefinitionData kraDefinitionData)
        {
            try
            {
                return new KRA().CreateKRADefinition(kraDefinitionData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region CreateKRAGroup
        /// <summary>
        /// creates kra group
        /// </summary>
        /// <param name="kraGroupData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public Task<int> CreateKRAGroup(KRAGroupData kraGroupData)
        {
            try
            {
                return new KRA().CreateKRAGroup(kraGroupData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region DeleteKRADefinition
        /// <summary>
        /// Delete KRADefinition
        /// </summary>
        /// <param name="kraDefinitionId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteKRADefinition(KRADefinitionData kraDefinitionData)
        {
            try
            {
                return await new KRA().DeleteKRADefinition(kraDefinitionData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteKRAMetricByKRADefinitionId
        /// <summary>
        /// Delete KRAMetric By KRADefinitionId
        /// </summary>
        /// <param name="kraDefinitionId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteKRAMetricByKRADefinitionId(KRADefinitionData kraDefinitionData)
        {
            try
            {
                return await new KRA().DeleteKRAMetricByKRADefinitionId(kraDefinitionData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetKRADefinitionById
        /// <summary>
        /// This method is used to get all the kra definitions under a kra group.
        /// </summary>
        /// <param name="kraGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRADefinitionData>> GetKRADefinitionById(int kraGroupId, int financialYearId)
        {
            List<KRADefinitionData> lstKRADefinitions;
            try
            {
                lstKRADefinitions = await new KRA().GetKRADefinitionById(kraGroupId, financialYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRADefinitions;
        }
        #endregion

        #region GetKRAGroups
        /// <summary>
        /// Gets list of KRA Groups.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAGroupData>> GetKRAGroups()
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                lstKRAGroups = await new KRA().GetKRAGroups();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAGroups;
        }
        #endregion

        #region GetKRAGroups
        /// <summary>
        /// Gets list of KRA Groups by financialyearid.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAGroupData>> GetKRAGroups(int financialYearId)
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                lstKRAGroups = await new KRA().GetKRAGroups(financialYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAGroups;
        }
        #endregion

        #region GetKRAGroups
        /// <summary>
        /// Gets list of KRA Groups by financialyearid.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAGroupData>> GetKRAGroupsByFinancial(int financialYearId)
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                lstKRAGroups = await new KRA().GetKRAGroupsByFinancial(financialYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAGroups;
        }
        #endregion

        #region ViewKRAGroups
        /// <summary>
        /// View KRA Groups by DepartmentID and FinancialYearID
        /// </summary>
        /// <param name="departmentID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAGroupData>> ViewKRAGroups(int departmentID, int financialYearID)
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                lstKRAGroups = await new KRA().ViewKRAGroups(departmentID, financialYearID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAGroups;
        }
        #endregion

        #region GetKRADefinitionByDepartmentID
        /// <summary>
        /// Get KRA Definitions By Department ID
        /// </summary>
        /// <param name="kraGroupID"></param>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRADefinitionData>> GetKRADefinitionByDepartmentID(int kraGroupID, int financialYearID, int departmentID)
        {
            List<KRADefinitionData> lstKRADefinitions;
            try
            {
                lstKRADefinitions = await new KRA().GetKRADefinitionByDepartmentID(kraGroupID, financialYearID, departmentID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRADefinitions;
        }
        #endregion

        //#region GetKRATitle
        ///// <summary>
        ///// Get KRA Title
        ///// </summary>
        ///// <param name="kraGroupID"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[UserInfoActionFilter]
        //public async Task<string> GetKRATitle(int kraGroupID)
        //{
        //    try
        //    {
        //        return await new KRA().GetKRATitle(kraGroupID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}
        //#endregion

        #region UpdateKRADefinition 
        /// <summary>
        /// Update KRA definition
        /// </summary>
        /// <param name="kraDefinitionData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateKRADefinition(KRADefinitionData kraDefinitionData)
        {
            try
            {
                return await new KRA().UpdateKRADefinition(kraDefinitionData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region InitiateKRAs 
        /// <summary>
        /// Initiate KRAs by HR Head
        /// </summary>
        /// <param name="FinancialYearID"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> InitiateKRAs(int FinancialYearId)
        {
            try
            {
                return await new KRA().InitiateKRAs(FinancialYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetOrganizationAndCustomKRAsForPMDH
        /// <summary>
        /// This method is used to get organization and custom KRAs when program manager or DH logs in. Displayed in dashboard.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<AssociateKRAs> GetOrganizationAndCustomKRAsForEmployee(int employeeID,int roleID, int financialYearID)
        {
            AssociateKRAs lstKRAs;
            try
            {
                lstKRAs = await new KRA().GetOrganizationAndCustomKRAsForEmployee(employeeID,roleID, financialYearID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAs;
        }
        #endregion

        #region GetOrganizationAndCustomKRAsForPMDH
        /// <summary>
        /// This method is used to get organization and custom KRAs when program manager or DH logs in. Displayed in dashboard.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<AssociateKRAs> GetViewKRAByEmployee(int employeeID, int financialYearID)
        {
            AssociateKRAs lstKRAs;
            try
            {
                lstKRAs = await new KRA().GetViewKRAByEmployee(employeeID, financialYearID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAs;
        }
        #endregion

        #region GetKRAOperators
        /// <summary>
        /// Gets list of KRA Operators.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GenericType>> GetKRAOperators()
        {
            List<GenericType> lstKRAOperators;
            try
            {
                lstKRAOperators = await new KRA().GetKRAOperators();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAOperators;
        }
        #endregion

        #region GetKRAMeasurementType
        /// <summary>
        /// Gets list of KRA measurement type.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GenericType>> GetKRAMeasurementType()
        {
            List<GenericType> lstKRAOperators;
            try
            {
                lstKRAOperators = await new KRA().GetKRAMeasurementType();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAOperators;
        }
        #endregion

        #region GetKRAScales
        /// <summary>
        /// Gets list of KRA scales.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<KRAScaleData>> GetKRAScales()
        {
            List<KRAScaleData> lstKRAScales;
            try
            {
                lstKRAScales = await new KRA().GetKRAScales();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAScales;
        }
        #endregion

        #region GetKRATargetPeriods
        /// <summary>
        /// Gets list of KRA target periods.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GenericType>> GetKRATargetPeriods()
        {
            List<GenericType> lstKRATargetPeriods;
            try
            {
                lstKRATargetPeriods = await new KRA().GetKRATargetPeriods();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRATargetPeriods;
        }
        #endregion

        #region GetKRAScaleValues
        /// <summary>
        /// Gets list of KRA scale values.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GenericType>> GetKRAScaleValues()
        {
            List<GenericType> lstKRAScaleValues;
            try
            {
                lstKRAScaleValues = await new KRA().GetKRAScaleValues();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAScaleValues;
        }
        #endregion

        #region GetKRAComments
        /// <summary>
        /// Get KRA comments.
        /// </summary>
        /// <param name="financialYearId"></param>
        /// <param name="kRAGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<KRAComments>> GetKRAComments(int financialYearId, int kRAGroupId)
        {
            try
            {
               return await new KRA().GetKRAComments(financialYearId, kRAGroupId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region AddKRAComments
        /// <summary>
        /// Adds KRA Comments.
        /// </summary>
        /// <param name="kraWorkFlowData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> AddKRAComments(KRAWorkFlowData kraWorkFlowData)
        {
            try
            {
                return await new KRA().AddKRAComments(kraWorkFlowData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteKRAGroup
        /// <summary>
        /// Delete KRAGroup
        /// </summary>
        /// <param name="kraSubmittedGroup"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> DeleteKRAGroup(KRASubmittedGroup kraSubmittedGroup)
        {
            try
            {
                return await new KRA().DeleteKRAGroup(kraSubmittedGroup);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region CreateKRAMeasurementType
        /// <summary>
        /// Create KRA MeasurementType
        /// </summary>
        /// <param name="KRAMeasurementTypeData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateKRAMeasurementType(KRAMeasurementTypeData KRAMeasurementTypeData)
        {
            try
            {
                return await new KRA().CreateKRAMeasurementType(KRAMeasurementTypeData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateKRAMeasurementType
        /// <summary>
        /// Update KRA MeasurementTypeData
        /// </summary>
        /// <param name="KRAMeasurementTypeData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateKRAMeasurementType(KRAMeasurementTypeData KRAMeasurementTypeData)
        {
            try
            {
                return await new KRA().UpdateKRAMeasurementType(KRAMeasurementTypeData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteKRAMeasurementType
        /// <summary>
        /// Delete KRA MeasurementType
        /// </summary>
        /// <param name="KRAMeasurementTypeId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> DeleteKRAMeasurementType(int KRAMeasurementTypeId)
        {
            try
            {
                return await new KRA().DeleteKRAMeasurementType(KRAMeasurementTypeId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}

