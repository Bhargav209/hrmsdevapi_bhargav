﻿using AP.API;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using static AP.DomainEntities.WorkStation;

namespace AP.Services.Controllers
{
    public class WorkStationController : ApiController
    {
        #region AllocateWorkstation
        /// <summary>
        /// Employee Desk Allocation
        /// </summary>
        /// <param name="assetManagementdetail"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<string> EmployeeDeskAllocation(int employeeId,string workStationId)
        {
            try
            {
                return await new WorkStationDetails().EmployeeDeskAllocation(employeeId, workStationId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion
        #region ReleaseDesk
        /// <summary>
        /// Release Desk
        /// </summary>
        /// <param name="employeeId,workStationId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> ReleaseDesk(int employeeId, int workStationId)
        {
            try
            {
                return await new WorkStationDetails().ReleaseDesk(employeeId, workStationId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion
        /// <summary>
        /// Get Work Stations List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<WorkStation>> GetWorkStationListByBayId(int BayIds)
        {
            try
            {
                return await new WorkStationDetails().GetWorkStationListByBayId(BayIds);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<WorkStationDataCount>> WorkStationReportCount(int bayId)
        {
            try
            {
                return await new WorkStationDetails().WorkStationReportCount(bayId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<WorkStation>> GetWorkStationDetailByWorkStationCode(string workstationCode)
        {
            try
            {
                return await new WorkStationDetails().GetWorkStationDetailByWorkStationCode(workstationCode);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
    }
}
