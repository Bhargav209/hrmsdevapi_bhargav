﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class KRAAspectsController : BaseApiController
    {
        #region GetKRAAspect
        /// <summary>
        /// Gets KRA Aspect Master
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AspectData>> GetKRAAspect(int departmentId)
        {
            List<AspectData> lstKRAAspect;
            try
            {
                lstKRAAspect = await new KRAAspects().GetKRAAspect(departmentId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRAAspect;
        }
        #endregion

        #region CreateKRAAspect
        /// <summary>
        /// Create a new KRA Aspect
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateKRAAspect(KRAAspectData kraAspectData)
        {
            try
            {
                return await new KRAAspects().CreateKRAAspect(kraAspectData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateKRAAspect
        /// <summary>
        /// Update a KRA Aspect
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateKRAAspect(KRAAspectData kraAspectData)
        {
            try
            {
                return await new KRAAspects().UpdateKRAAspect(kraAspectData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion


        #region DeleteKRAAspect
        /// <summary>
        /// Delete a KRA Aspect
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> DeleteKRAAspect(KRAAspectData kraAspectData)
        {
            try
            {
                return await new KRAAspects().DeleteKRAAspect(kraAspectData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}
