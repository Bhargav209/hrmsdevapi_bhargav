﻿using System.Web.Http;
using System.Net.Http;
using System.Net;
using System;
using System.Resources;
using AP.API;
using AP.Utility;
using AP.DomainEntities;
using System.Collections.Generic;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class CompetencySkillsController : BaseApiController
    {
        #region GetCompetencySkillsMappedData
        /// <summary>
        /// GetCompetencySkillsMappedData
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCompetencySkillsMappedData()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new CompetencySkills().GetCompetencySkillsMappedData());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

        #region GetCompetencyArea
        /// <summary>
        /// GetCompetencyArea
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCompetencyArea()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new CompetencySkills().GetCompetencyArea());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetSkillIDList
        /// <summary>
        /// GetSkillIDList
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetSkillIDList(ICollection<string> skillNameList)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
               returnObject = Request.CreateResponse(new CompetencySkills().GetSkillIDList(skillNameList));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region CompetencySkillSearch
        /// <summary>
        /// CompetencySkillSearch
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CompetencySkillSearch(SearchFilter searchFilter)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new CompetencySkills().CompetencySkillSearch(searchFilter));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetCompetencySkillsByRoleId
        /// <summary>
        /// CompetencySkillSearch
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCompetencySkillsByRoleId(int roleId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new CompetencySkills().GetCompetencySkillsByRoleId(roleId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region AddUpdateCompetencySkills
        /// <summary>
        /// AddUpdateCompetencySkills
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddUpdateCompetencySkills(CompetencySkillDetails csDetails)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new CompetencySkills().AddUpdateCompetencySkills(csDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion        

    }
}