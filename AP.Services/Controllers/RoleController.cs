﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Controllers;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AP.Services
{
    public class RoleController : BaseApiController
    {
        #region CreateRole
        /// <summary>
        /// Create a new Role
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateRole(RoleData roleData)
        {
            try
            {
                return await new Roles().CreateRole(roleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region AddSkillsToRole
        /// <summary>
        /// Create a new Role
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> AddSkillsToRole(RoleData roleData)
        {
            try
            {
                return await new Roles().AddSkillsToRole(roleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetSkillsByRole
        /// <summary>
        /// Create a new Role
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<RoleCompetencySkills>> GetSkillsByRole(int RoleMasterID)
        {
            try
            {
                return await new Roles().GetSkillsByRole(RoleMasterID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// Gets Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<RoleData>> GetRoles()
        {
            List<RoleData> lstRoles;
            try
            {
                lstRoles = await new Roles().GetRoles();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        #region GetRoleByRoleID
        /// <summary>
        /// Gets Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<RoleData> GetRoleByRoleID(int roleID)
        {
            try
            {
                return await new Roles().GetRoleByRoleID(roleID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetRoleSuffixAndPrefix
        /// <summary>
        /// Gets roles, suffixes and prefixes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<SGEntity> GetRoleSuffixAndPrefix(int departmentId)
        {
            SGEntity lstRoles;
            try
            {
                lstRoles = await new Roles().GetRoleSuffixAndPrefix(departmentId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        #region UpdateRole
        /// <summary>
        /// Update a Role
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateRole(RoleData roleData)
        {
            try
            {
                return await new Roles().UpdateRole(roleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetRoleDetailsByDepartmentID
        /// <summary>
        /// Get Roles Details by department ID.
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<RoleData>> GetRoleDetailsByDepartmentID(int DepartmentId)
        {
            List<RoleData> lstRoles;
            try
            {
                lstRoles = await new Roles().GetRoleDetailsByDepartmentID(DepartmentId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        #region GetRolesByDepartmentID
        /// <summary>
        /// Get Roles by department ID.
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetRolesByDepartmentID(int DepartmentId)
        {
            List<GenericType> lstRoles;
            try
            {
                lstRoles = await new Roles().GetRolesByDepartmentID(DepartmentId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        #region GetKRARolesByDepartmentID
        /// <summary>
        /// Get KRARoles by department ID.
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetKRARolesByDepartmentID(int DepartmentId)
        {
            List<GenericType> lstRoles;
            try
            {
                lstRoles = await new Roles().GetKRARolesByDepartmentID(DepartmentId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstRoles;
        }
        #endregion

        //#region CreateRoleSkills
        ///// <summary>
        ///// Create role master details
        ///// </summary>
        ///// <param name="roleData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage CreateRoleSkills(RoleData roleData)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Roles().CreateRoleSkills(roleData));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        //#region UpdateRoleSkills
        ///// <summary>
        ///// Create role master details
        ///// </summary>
        ///// <param name="roleData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage UpdateRoleSkills(RoleData roleData)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Roles().UpdateRoleSkills(roleData));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion


        #region GetKRATitleByDepartmentID
        /// <summary>
        /// Get Roles by department ID.
        /// </summary>
        /// <param name="departmentID"></param>
        /// <returns>List</returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetKRATitleByDepartmentID(int departmentID)
        {
            List<GenericType> lstKRATitles;
            try
            {
                lstKRATitles = await new Roles().GetKRATitleByDepartmentID(departmentID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKRATitles;
        }
        #endregion

        #region GetStatusByRole
        /// <summary>
        ///  Get Status By Role
        /// </summary>
        /// <param name="RoleName"></param>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetStatusByRole(string RoleName, int CategoryId)
        {
            try
            {
                return await new Roles().GetStatusByRole(RoleName, CategoryId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}