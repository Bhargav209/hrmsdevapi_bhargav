﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ADROrganisationValueController : ApiController
    {

        #region GetOrganisationValue
        /// <summary>
        /// Gets GetOrganisation Value Master Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ADROrganisationValueData>> GetADROrganisationValues(int financialYearId)
        {
            List<ADROrganisationValueData> lstOrgValues;
            try
            {
                lstOrgValues = await new ADROrganisationValue().GetADROrganisationValues(financialYearId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstOrgValues;
        }
        #endregion

        #region CreateADROrganisationValue
        /// <summary>
        /// Create a new ADROrganisationValue
        /// </summary>
        /// <param name="organisationValueData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateADROrganisationValue(ADROrganisationValueData organisationValueData)
        {
            try
            {
                return await new ADROrganisationValue().CreateADROrganisationValue(organisationValueData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateADROrganisationValue
        /// <summary>
        /// Update ADROrganisationValue
        /// </summary>
        /// <param name="organisationValueData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateADROrganisationValue(ADROrganisationValueData organisationValueData)
        {
            try
            {
                return await new ADROrganisationValue().UpdateADROrganisationValue(organisationValueData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}