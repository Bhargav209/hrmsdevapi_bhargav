﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.Services.Filters;
using AP.DomainEntities;
using AP.API;

namespace AP.Services.Controllers
{
    public class DesignationController : ApiController
    {
        #region SaveDesignationDetails
        /// <summary>
        /// Saves the designation details.
        /// </summary>
        /// <param name="designationData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveDesignationDetails(DesignationData designationData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DesignationDetails().SaveDesignationDetails(designationData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateDesignationDetails
        /// <summary>
        /// Updates designation details
        /// </summary>
        /// <param name="designationData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateDesignationDetails(DesignationData designationData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DesignationDetails().UpdateDesignationDetails(designationData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetDesignationDetails
        /// <summary>
        /// Gets designation details
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetDesignationDetails(bool isActive = true)
        {
            HttpResponseMessage httpResponse = null;

            try
            {
                httpResponse = Request.CreateResponse(new DesignationDetails().GetDesignationDetails(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion

    }
}