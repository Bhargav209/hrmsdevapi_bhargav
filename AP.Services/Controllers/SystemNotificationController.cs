﻿using System;
using System.Web.Http;
using System.Web;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.API;
using AP.Utility;
using AP.DomainEntities;

namespace AP.Services.Controllers
{
    public class SystemNotificationController : BaseApiController
    {
        #region AssociateProfileCreationSubmit
        /// <summary>
        /// Method to notify HR manager on employe profile submission
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage AssociateProfileSubmit(int empID)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                ProspectiveAssociateDetails associateDetails = new ProspectiveAssociateDetails();
                string associateCode = new DashboardDetails().GetAssociateCodeByEmpID(empID);
                associateDetails.UpdatePAStatus(empID, Convert.ToInt32(resourceManager.GetString("StatusPendingID")));

                if (!string.IsNullOrEmpty(associateCode))
                {
                    ProfileCreationNotificationJob profileCreationNotification = new ProfileCreationNotificationJob();
                    returnObject = Request.CreateResponse(profileCreationNotification.AddJob(associateCode, resourceManager.GetString("ProfileCreationNotification")));
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region AssociateProfileApproval
        /// <summary>
        /// Method to approve the employee profile
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public HttpResponseMessage AssociateProfileApproval(int empID, string reqType, string remarks)
        {
            HttpResponseMessage retObject = null;

            try
            {
                ProspectiveAssociateDetails associateDetails = new ProspectiveAssociateDetails();
                ProfileCreationNotificationJob profileCreationNotification = new ProfileCreationNotificationJob();
                string associateCode = new DashboardDetails().GetAssociateCodeByEmpID(empID);

                if (!string.IsNullOrEmpty(associateCode))
                {
                    retObject = Request.CreateResponse(profileCreationNotification.RemoveJob(associateCode, resourceManager.GetString("ProfileCreationNotification"), remarks, reqType));

                    if (reqType == resourceManager.GetString("Approve"))
                    {

                        // Delete PA details from PA display
                        //
                        associateDetails.DeletePADetails(empID);

                        //Update Associate as approved and show this employee record in the Associate tab
                        //
                        new AssociatePersonalDetails().UpdateAssociateStatus(empID, Convert.ToInt32(resourceManager.GetString("StatusApprovedID")));

                        //Add IT notification Job
                        //
                        ITNotificationJob itNotification = new ITNotificationJob();
                        bool isITAdded = itNotification.AddJob(associateCode, resourceManager.GetString("ITNotification"));

                        //Add Finance notification Job
                        //
                        FinanceNotificationJob financeNotification = new FinanceNotificationJob();
                        bool isFinanceAdded = financeNotification.AddJob(associateCode, resourceManager.GetString("FinanceNotification"));

                        //Add Admin notification Job
                        //
                        AdminNotificationJob adminNotification = new AdminNotificationJob();
                        bool isAdminAdded = adminNotification.AddJob(associateCode, resourceManager.GetString("AdminNotification"));

                        bool isAllocatedToPool = new Allocation().TalentPoolAllocation(empID);

                        if (isITAdded && isFinanceAdded && isAdminAdded)
                        {
                            retObject.StatusCode = HttpStatusCode.OK;
                        }
                    }
                    else
                    {
                        associateDetails.UpdatePAStatus(empID, Convert.ToInt32(resourceManager.GetString("StatusRejectedID")));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return retObject;
        }
        #endregion

        #region GetNotifications
        /// <summary>
        /// Method to get IT notification list
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetNotifications()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ITNotificationJob().GetItNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region CountNotifications
        /// <summary>
        /// Method to get IT notification count
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage CountNotifications()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ITNotificationJob().CountItNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region UpdateItNotification
        /// <summary>
        /// Method to update IT notification 
        /// </summary>
        /// <param name="employeeCode"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage UpdateItNotification(string employeeCode, int taskId)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ITNotificationJob().UpdateItNotifications(employeeCode, taskId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetFinanceNotifications
        /// <summary>
        /// Method to get Finance notification list
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetFinanceNotifications()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new FinanceNotificationJob().GetFinanceNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region CountFinanceNotifications
        /// <summary>
        /// Method to get Finance notification count
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage CountFinanceNotifications()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new FinanceNotificationJob().CountFinanceNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region UpdateFinanceNotification
        /// <summary>
        /// Method to update Finance notification
        /// </summary>
        /// <param name="employeeCode"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage UpdateFinanceNotification(string employeeCode, int taskId)
        {
            HttpResponseMessage returnValue = null;

            try
            {
                returnValue = Request.CreateResponse(new FinanceNotificationJob().UpdateFinanceNotifications(employeeCode, taskId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnValue;
        }
        #endregion

        #region GetAdminNotifications
        /// <summary>
        /// Method to get Admin notification
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetAdminNotifications()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new AdminNotificationJob().GetAdminNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region CountAdminNotifications
        /// <summary>
        /// Method to get Admin notification count
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage CountAdminNotifications()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new AdminNotificationJob().CountAdminNotifications());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region UpdateAdminNotification
        /// <summary>
        /// Method to update Admin notification
        /// </summary>
        /// <param name="employeeCode"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage UpdateAdminNotification(string employeeCode, int taskId)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new AdminNotificationJob().UpdateAdminNotifications(employeeCode, taskId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region merged from dashboard controller

        #region GetHRANotificationCount
        /// <summary>
        /// Get notification count to HRA
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetHRANotificationCount(int notificationTypeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DashboardDetails().GetHRANotificationCount(notificationTypeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetNotificationCount
        /// <summary>
        /// Method to get notification count based on type
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetNotificationCount(int notificationTypeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new DashboardDetails().GetNotificationCount(notificationTypeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #endregion

        #region GetNotificationType
        /// <summary>
        /// Method to get notification type
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetNotificationType()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ITNotificationJob().GetNotificationType());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetAllEmailIds
        /// <summary>
        /// Method to get all emailids
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetAllEmailIds()
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ITNotificationJob().GetAllEmailIds());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region SaveNotificationConfig
        /// <summary>
        /// Method to save notification configuration
        /// </summary>
        /// <param name="emailNotifyConfig"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public HttpResponseMessage SaveNotificationConfig(EmailNotificationConfiguration emailNotifyConfig)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                if (emailNotifyConfig == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                returnObject = Request.CreateResponse(new ITNotificationJob().SaveEmailNotificationConfiguration(emailNotifyConfig));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetNotificationConfiguration
        /// <summary>
        /// Method to get notification configuration
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetNotificationConfiguration(string notificationType)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new ITNotificationJob().GetNotificationConfiguration(notificationType));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
    }
}
