﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ClientBillingRoleController : BaseApiController
    {
        #region Create ClientBillingRole
        /// <summary>
        /// Create Client Billing Role
        /// </summary>
        /// <param name="clientBillingRoleDetails"></param>
        /// <returns></returns>
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateClientBillingRole(ClientBillingRoleDetails clientBillingRoleDetails)
        {
            try
            {
                return await new ClientBillingRoles().CreateClientBillingRole(clientBillingRoleDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region Update ClientBillingRole
        /// <summary>
        /// Update Client Billing Role
        /// </summary>
        /// <param name="clientBillingRoleDetails"></param>
        /// <returns></returns>
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateClientBillingRole(ClientBillingRoleDetails clientBillingRoleDetails)
        {
            try
            {
                return await new ClientBillingRoles().UpdateClientBillingRole(clientBillingRoleDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region Get ClientBillingRoles
        /// <summary>
        /// GetClientBillingRoles
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ClientBillingRoleDetails>> GetClientBillingRoles()
        {
            List<ClientBillingRoleDetails> clientBillingRoles;
            try
            {
                clientBillingRoles = await new ClientBillingRoles().GetClientBillingRoles();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return clientBillingRoles;
        }

        #endregion

        #region Get ClientBillingRolesByProject
        /// <summary>
        /// GetClientBillingRolesByProjectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ClientBillingRoleDetails>> GetClientBillingRolesByProjectId(int projectId)
        {
            List<ClientBillingRoleDetails> clientBillingRoles;
            try
            {
                clientBillingRoles = await new ClientBillingRoles().GetClientBillingRolesByProjectId(projectId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return clientBillingRoles;
        }

        #endregion
        #region GetEmployeesByClientBillingRoleId
        /// <summary>
        /// GetEmployeesByClientBillingRoleId
        /// </summary>
        /// <param name="clientBillingRoleId,projectId"></param>
        /// <returns></returns>
        /// 
        //[Authorize(Roles = "Program Manager, Department Head, HRA, HRM")]
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ClientBillingRoleDetails>> GetEmployeesByClientBillingRoleId(int clientBillingRoleId, int projectId)
        {
            List<ClientBillingRoleDetails> employeeName;
            try
            {
                employeeName = await new ClientBillingRoles().GetEmployeesByClientBillingRoleId(clientBillingRoleId,projectId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return employeeName;
        }

        #endregion

        #region DeleteClientBillingRole
        /// <summary>
        /// DeleteClientBillingRole
        /// </summary>
        /// <param name="clientBillingRoleId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        //[Authorize(Roles = "Program Manager, Department Head")]
        public async Task<bool> DeleteClientBillingRole(int clientBillingRoleId)
        {
            try
            {
                if (clientBillingRoleId == 0)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ClientBillingRoles().DeleteClientBillingRole(clientBillingRoleId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        #region CloseClientBillingRole
        /// <summary>
        /// CloseClientBillingRole
        /// </summary>
        /// <param name="clientBillingRoleId,endDate"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        //[Authorize(Roles = "Program Manager, Department Head")]
        public async Task<int> CloseClientBillingRole(int clientBillingRoleId,DateTime endDate)
        {
            try
            {
                if (clientBillingRoleId == 0)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }
                return await new ClientBillingRoles().CloseClientBillingRole(clientBillingRoleId, endDate);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}