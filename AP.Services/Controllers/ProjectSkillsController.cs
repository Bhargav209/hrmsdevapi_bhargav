﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.Utility;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class ProjectSkillsController : BaseApiController
    {

        //#region GetProject
        ///// <summary>
        ///// GetProjectSkills
        ///// </summary>        
        ///// <returns></returns>
        //[HttpGet]
        //public HttpResponseMessage GetProjectSkills(int projectID, int employeeId)
        //{
        //    HttpResponseMessage returnObject = new HttpResponseMessage();

        //    try
        //    {
        //        returnObject = Request.CreateResponse(new ProjectSkillsDetails().GetProjectSkills(projectID, employeeId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return returnObject;
        //}
        //#endregion

        #region GetRoles
        /// <summary>
        /// GetRolesByProjId
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRolesByProjId(int projectID)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectSkillsDetails().GetRolesByProjId(projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjSkillsById
        /// <summary>
        /// GetProjSkillsById
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjSkillsById(int currentID)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new ProjectSkillsDetails().getProjSkillsById(currentID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
        #region PostProjectSkills
        /// <summary>
        ///Adds and Updates project skills details
        /// </summary>
        /// <param name="projectSkills"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage PostProjectSkills(ProjectSkillsData projectSkills)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                if (projectSkills == null)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }

                returnObject = Request.CreateResponse(new ProjectSkillsDetails().AddUpdateProjectSkillsDetails(projectSkills));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion   
        #region DeletePADetailsByID
        /// <summary>
        /// DeletePADetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeleteProjectSkillByID(int ProjectSkillId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectSkillsDetails().DeleteProjectSkillByID(ProjectSkillId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion     
    }
}
