﻿using AP.DomainEntities;
using AP.Services.Filters;
using AP.API;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using AP.Utility;

namespace AP.Services.Controllers
{
    public class InternalBillingRoleController : BaseApiController
    {
        #region CreateInternalBillingRole
        /// <summary>
        /// Create Internal Billing Role
        /// </summary>
        /// <param name="InternalBillingRoleDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateInternalBillingRole(InternalBillingRoleDetails internalBillingRoleDetails)
        {
            try
            {
                return await new InternalBillingRoles().CreateInternalBillingRoles(internalBillingRoleDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateInternalBillingRole
        /// <summary>
        /// Update Internal Billing Role
        /// </summary>
        /// <param name="InternalBillingRoleDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateInternalBillingRole(InternalBillingRoleDetails internalBillingRoleDetails)
        {
            try
            {
                return await new InternalBillingRoles().UpdateInternalBillingRole(internalBillingRoleDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetInternalBillingRoles
        /// <summary>
        /// GetInternalBillingRoles
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public async Task<List<InternalBillingRoleDetails>> GetInternalBillingRoles(bool isActive = true)
        {
            List<InternalBillingRoleDetails> internalBillingRoles;

            try
            {
                internalBillingRoles = await new InternalBillingRoles().GetInternalBillingRoles(isActive);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return internalBillingRoles;
        }

        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<InternalBillingRoleDetails>> GetAllInternalBillingRoles()
        {
            List<InternalBillingRoleDetails> internalBillingRoles;

            try
            {
                internalBillingRoles = await new InternalBillingRoles().GetInternalBillingRoles();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return internalBillingRoles;
        }

        #endregion
    }
}