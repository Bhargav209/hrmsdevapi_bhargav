﻿using System.Web.Http;
using System.Collections.Generic;
using System.Net.Http;
using System;
using AP.API;
using System.Net;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.Services.Filters;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.Utility;

namespace AP.Services.Controllers
{
    public class ReportsController : BaseApiController
    {
        #region GetResourceByProject
        /// <summary>
        /// Returns total number of resource, billable and non billable resource details.
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<AllocationDetails> GetResourceByProject(int projectId)
        {
            try
            {
                return await new Report().GetResourceByProject(projectId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetAssociateSkillsReport
        /// <summary>
        /// Gets Associate Skills Report.
        /// </summary>
        /// <param name="skillSearchFilter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<SkillSearchData>> GetAssociateSkillsReport(SkillSearchData skilldata)
        {
            
            try
            {
                return await new Report().GetAssociateSkillsReport(skilldata);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DomainReportCount
        /// <summary>
        /// Get domain wise report count
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DomainDataCount>> DomainReportCount()
        {
            try
            {
                return await new Report().DomainReportCount();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region TalentPoolReportCount
        /// <summary>
        /// Get Talent Pool report count
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<TalentPoolDataCount>> TalentPoolReportCount()
        {
            try
            {
                return await new Report().TalentPoolReportCount();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetFinanceReports
        /// <summary>
        /// Gets finance reports
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<ReportsFilterData> GetFinanceReports(ReportsFilterData reportsFilterData)
        {
            try
            {
                reportsFilterData = await new Report().GetFinanceReports(reportsFilterData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return reportsFilterData;
        }
        #endregion

        #region GetRmgReportDataByMonthYear
        /// <summary>
        /// Get Rmg Report Data By Month Year
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ReportsData>> GetRmgReportDataByMonthYear(int month, int year)
        {
            List<ReportsData> reportsFilterData = new List<ReportsData>();
            try
            {
                reportsFilterData = await new Report().GetRmgReportDataByMonthYear(month, year);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return reportsFilterData;
        }
        #endregion

        #region GetFinanceReportToFreez
        /// <summary>
        /// Get Finance Report To Freez
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public bool GetFinanceReportToFreez()
        {
            try
            {
                var reportsFilterData = new Report().GetFinanceReportToFreez();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return true;
        }
        #endregion

        #region GetUtilizationReports
        /// <summary>
        /// Gets utilozation reports
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<ReportsFilterData> GetUtilizationReports(ReportsFilterData reportsFilterData)
        {
            try
            {
                reportsFilterData = await new Report().GetUtilizationReports(reportsFilterData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return reportsFilterData;
        }
        #endregion

        #region DomainReport
        /// <summary>
        /// Get domain wise report
        /// </summary>
        /// <param name="domainID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DomainData>> DomainReport(int domainID)
        {
            try
            {
                return await new Report().DomainReport(domainID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region TalentPoolReport
        /// <summary>
        /// Get Talent Pool report
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<TalentPoolReportData>> TalentPoolReport(int projectID)
        {
            try
            {
                return await new Report().TalentPoolReport(projectID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        #region GetProjectHistoryByEmployee
        /// <summary>
        /// Get Project History By Employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<TalentPoolReportData>> GetProjectHistoryByEmployee(int employeeId)
        {
            try
            {
                return await new Report().GetProjectHistoryByEmployee(employeeId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion


        #region GetUtilizationReportsByTechnology
        /// <summary>
        /// Gets utilization reports By Technology
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<ReportsFilterData> GetUtilizationReportsByTechnology(ReportsFilterData reportsFilterData)
        {
            try
            {
                reportsFilterData = await new Report().GetUtilizationReportsByTechnology(reportsFilterData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return reportsFilterData;
        }
        #endregion

        #region GetUtilizationReportsByMonth
        /// <summary>
        /// Gets utilization reports By Month
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<ReportsFilterData> GetUtilizationReportsByMonth(ReportsFilterData reportsFilterData)
        {
            if ((reportsFilterData.utilizationReportByMonthFilterData.FromMonth <= 0 ||
                reportsFilterData.utilizationReportByMonthFilterData.FromMonth > 12 )||
                (reportsFilterData.utilizationReportByMonthFilterData.ToMonth <= 0 ||
                reportsFilterData.utilizationReportByMonthFilterData.ToMonth > 12 )||
                reportsFilterData.utilizationReportByMonthFilterData.Year > DateTime.Now.Year)
            {
                return reportsFilterData;
            }
            else
            {
                try
                {
                    reportsFilterData = await new Report().GetUtilizationReportsByMonth(reportsFilterData);
                }
                catch (Exception ex)
                {
                    Log.LogError(ex, Log.Severity.Error, "");
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = "Warning"
                    });
                }
            }
            return reportsFilterData;
        }
        #endregion

    }
}
