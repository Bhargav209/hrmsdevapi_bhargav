﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.Utility;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class SkillGapAssesmentController : BaseApiController
    {
        #region GetCurrentProficiencyLevel
        /// <summary>
        /// GetCurrentProficiencyLevel
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCurrentProficiencyLevel(int employeeId, int skillId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new SkillGap().GetCurrentProficiencyLevel(employeeId, skillId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetProjectsByEmpID
        /// <summary>
        /// GetProjectsByEmpID
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectsByEmpID(int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new SkillGap().GetProjectsByEmpID(employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetRoleByIDs
        /// <summary>
        /// GetRoleByIDs
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoleByIDs(int employeeId,int projectId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new SkillGap().GetRoleByIDs(employeeId, projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetRequiredProficiencyLevel
        /// <summary>
        /// GetRequiredProficiencyLevel
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRequiredProficiencyLevel(int projectId, int skillId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new SkillGap().GetRequiredProficiencyLevel(projectId, skillId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
        #region GetAssociateSkillGapList
        /// <summary>
        /// GetAssociateSkillGapList
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateSkillGapList(int projectID)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new SkillGap().GetAssociateSkillGapList(projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

      
       
        #region PostSkillGapAssesment
        /// <summary>
        ///Adds and updates the skill gap assesment details
        /// </summary>
        /// <param name="skillGapAssesment"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage PostSkillGapAssesment(SkillGapData skillGapAssesment)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                if (skillGapAssesment == null)
                {
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));
                }

                returnObject = Request.CreateResponse(new SkillGap().AddUpdateSkillGapAssesment(skillGapAssesment));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion     
    }
}
