﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
   public class AssetManagementController : BaseApiController
   {
      #region CreateCategory
      /// <summary>
      /// Create Category
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> CreateCategory(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().CreateCategory(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }

      #endregion
      #region CreateAsset
      /// <summary>
      /// Create Asset
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> CreateAsset(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().CreateAsset(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }

      #endregion
      #region CreateVendor 
      /// <summary>
      /// Create Vendor
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> CreateVendor(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().CreateVendor(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
      #region CreateModel 
      /// <summary>
      /// Create Model
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> CreateModel(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().CreateModel(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
      #region CreateMake 
      /// <summary>
      /// Create Make
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> CreateMake(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().CreateMake(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion

      #region GetCategoryDetails
      /// <summary>
      /// GetAssetDetails
      /// </summary>
      /// <param name=""></param>
      /// <returns></returns>
      /// 

      [HttpGet]
      public HttpResponseMessage GetCategoryDetails()
      {
         HttpResponseMessage GgetAssetDetails = new HttpResponseMessage();
         try
         {
            GgetAssetDetails = Request.CreateResponse(new AssetManagement().GetCategoryDetails());
         }
         catch (Exception ex)
         {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return GgetAssetDetails;
      }
      #endregion
      #region GetAssetDetails
      /// <summary>
      /// GetAssetDetails
      /// </summary>
      /// <param name=""></param>
      /// <returns></returns>
      /// 

      [HttpGet]
      public HttpResponseMessage GetAssetDetails()
      {
         HttpResponseMessage GgetAssetDetails = new HttpResponseMessage();
         try
         {
            GgetAssetDetails = Request.CreateResponse(new AssetManagement().GetAssetDetails());
         }
         catch (Exception ex)
         {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return GgetAssetDetails;
      }
      #endregion
      #region GetVendorDetails
      /// <summary>
      /// GetVendorDetails
      /// </summary>
      /// <param name=""></param>
      /// <returns></returns>
      /// 

      [HttpGet]
      public HttpResponseMessage GetVendorDetails()
      {
         HttpResponseMessage getVendorDetails = new HttpResponseMessage();
         try
         {
            getVendorDetails = Request.CreateResponse(new AssetManagement().GetVendorDetails());
         }
         catch (Exception ex)
         {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return getVendorDetails;
      }
      #endregion
      #region GetModelDetails
      /// <summary>
      /// Get Model Details
      /// </summary>
      /// <param name=""></param>
      /// <returns></returns>
      /// 

      [HttpGet]
      public HttpResponseMessage GetModelDetails()
      {
         HttpResponseMessage getModelDetails = new HttpResponseMessage();
         try
         {
            getModelDetails = Request.CreateResponse(new AssetManagement().GetModelDetails());
         }
         catch (Exception ex)
         {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return getModelDetails;
      }
      #endregion
      #region GetMakeDetails
      /// <summary>
      /// Get Make Details
      /// </summary>
      /// <param name=""></param>
      /// <returns></returns>
      /// 

      [HttpGet]
      public HttpResponseMessage GetMakeDetails()
      {
         HttpResponseMessage getMakeDetails = new HttpResponseMessage();
         try
         {
            getMakeDetails = Request.CreateResponse(new AssetManagement().GetMakeDetails());
         }
         catch (Exception ex)
         {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return getMakeDetails;
      }
      #endregion

      #region UpdateCategoryDetail
      /// <summary>
      /// Update Category Detail
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> UpdateCategoryDetail(AssetManagementdetails assetManagementdetail)
      {
         try
         {           
            return await new AssetManagement().UpdateCategoryDetail(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
      #region UpdateAssetDetail
      /// <summary>
      /// Update Asset Detail
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> UpdateAssetDetail(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().UpdateAssetDetail(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
      #region UpdateVendorDetail
      /// <summary>
      /// Update Vendor Detail
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> UpdateVendorDetail(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().UpdateVendorDetail(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
      #region UpdateModelDetail
      /// <summary>
      /// Update Model Detail
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> UpdateModelDetail(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().UpdateModelDetail(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
      #region UpdateMakeDetail
      /// <summary>
      /// Update Make Detail
      /// </summary>
      /// <param name="assetManagementdetail"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> UpdateMakeDetail(AssetManagementdetails assetManagementdetail)
      {
         try
         {
            return await new AssetManagement().UpdateMakeDetail(assetManagementdetail);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
   }
}
