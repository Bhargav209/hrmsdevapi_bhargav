﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.DomainEntities;
using System.Collections.Generic;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class UserRoleController : BaseApiController
    {

        #region GetRoleMasterDetails
        /// <summary>
        /// Get Role Master details from Database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoleMasterDetails()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetRoleMasterDetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region AssignRole
        /// <summary>
        /// Assign role to user
        /// </summary>
        /// <param name="userRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AssignRole(UserRoleData userRoleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().AssignRole(userRoleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateRole
        /// <summary>
        /// Update user role
        /// </summary>
        /// <param name="userRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateRole(IEnumerable <UserRoleData> userRoleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                //userRoleData.ModifiedUser = RequestContext.Principal.Identity.Name;
                httpResponseMessage = Request.CreateResponse(new UserRoles().UpdateRole(userRoleData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateUserRole
        /// <summary>
        /// Update user role
        /// </summary>
        /// <param name="userRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateUserRole(UserRoleData userRoleData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                //userRoleData.ModifiedUser = RequestContext.Principal.Identity.Name;
                httpResponseMessage = Request.CreateResponse(new UserRoles().UpdateUserRole(userRoleData));
            }
            catch (Exception ex)
             {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// GetRoles
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRoles(bool isActive=true)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetRoles(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUserRoleDetails
        /// <summary>
        /// GetUserRoleDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetUserRoleDetails()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetUserRoleDetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUserRoleDetailsbyUserID
        /// <summary>
        /// GetUserRoleDetailsbyUserID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetUserRoleDetailsbyUserID(int userID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetUserRoleDetailsbyUserID(userID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUserRoleOnLgin
        /// <summary>
        /// GetUserRoleOnLgin
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetUserRoleOnLgin(string userName)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetUserRoleOnLgin(userName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetUsers
        /// <summary>
        /// Get users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetUsers()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetUsers());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        } 
        #endregion

        #region GetNames
        /// <summary>
        /// GetNames
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetNames()
        {
            HttpResponseMessage httpResponse = null;

            try
            {
                httpResponse = Request.CreateResponse(new UserRoles().GetNames());
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content=new StringContent(ex.Message),
                    ReasonPhrase="Warning"
                });
            }

            return httpResponse;
        }
        #endregion

        #region GetEmployeeOnUserName
        /// <summary>
        /// GetEmployeeOnUserName
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmployeeOnUserName(string userName)
        {
            HttpResponseMessage httpResponse = null;

            try
            {
                httpResponse = Request.CreateResponse(new UserRoles().GetEmployeeOnUserName(userName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion

        #region GetEmployeeResignStatus
        /// <summary>
        /// GetEmployeeResignStatus
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmployeeResignStatus()
        {
            HttpResponseMessage httpResponse = null;

            try
            {
                httpResponse = Request.CreateResponse(new UserRoles().GetEmployeeResignStatus());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion

        #region MapAssociateId
        /// <summary>
        /// Service that maps an associate's name to its corresponding email adddress
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage MapAssociateId(AssociateDetails associateDetails)
        {
            HttpResponseMessage httpResponse = null;
            try
            {
                httpResponse = Request.CreateResponse(new UserRoles().MapAssociateId(associateDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion

        #region UpdateEmployeeStatus
        /// <summary>
        /// Updates the employee status
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateEmployeeStatus(AssociateDetails associateDetails)
        {
            HttpResponseMessage httpResponse = null;
            try
            {
                httpResponse = Request.CreateResponse(new UserRoles().UpdateEmployeeStatus(associateDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion

        #region GetUsers
        /// <summary>
        /// Get users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetUnMappedUsers()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new UserRoles().GetUnMappedUsers());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
