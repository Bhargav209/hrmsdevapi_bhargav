﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class AwardNominationController : BaseApiController
    {

        /// <summary>
        /// Get Award Nomination List
        /// </summary>
        /// <param name="awardNominationData"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AwardNominationData>> GetAwardNominationList(int financialYearId, int fromEmployeeId,string roleName)
        {
            try
            {
                return await new AwardNomination().GetAwardNominationList(financialYearId, fromEmployeeId,roleName);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }


        /// <summary>
        /// Create Appreciation
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateAwardNomination(AwardNominationData AwardNominationData)
        {
            try
            {
                return await new AwardNomination().CreateAwardNomination(AwardNominationData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        /// <summary>
        /// Update AwardNomination
        /// </summary>
        /// <param name="AwardNominationData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateAwardNomination(AwardNominationData AwardNominationData)
        {
            try
            {
                return await new AwardNomination().UpdateAwardNomination(AwardNominationData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }



        /// <summary>
        /// GetAwards List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetAwardsList()
        {
            try
            {
                return await new AwardNomination().GetAwardsList();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        /// <summary>
        /// GetAward Type List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetAwardTypeList()
        {
            try
            {
                return await new AwardNomination().GetAwardTypeList();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
    }
}
