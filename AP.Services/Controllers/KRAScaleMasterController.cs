﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
   public class KRAScaleMasterController : BaseApiController
   {
      #region GetKRAScaleMasters
      /// <summary>
      /// Gets list of KRA scale masters.
      /// </summary>
      /// <returns></returns>
      [HttpGet]
      public async Task<List<KRAScaleData>> GetKRAScaleMasters()
      {
         List<KRAScaleData> lstKRAScales;
         try
         {
            lstKRAScales = await new KRAScaleMasterDetails().GetKRAScaleMasters();
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return lstKRAScales;
      }

      public async Task<List<KRAScaleDetails>> GetKRAScaleDetailsByMaster(int scaleMasterId)
      {
         List<KRAScaleDetails> lstKRAScales;
         try
         {
            lstKRAScales = await new KRAScaleMasterDetails().GetKRAScaleDetailsByMaster(scaleMasterId);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
         return lstKRAScales;
      }
      #endregion

      #region CreateScaleMaster
      /// <summary>
      /// Create a new KRA scale master
      /// </summary>
      /// <param name="kraScaleData"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> CreateScaleMaster(KRAScaleData kraScaleData)
      {
         try
         {
            return await new KRAScaleMasterDetails().CreateScaleMaster(kraScaleData);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
        #endregion

        #region UpdateScaleDetails
        /// <summary>
        /// Update a KRA scale master
        /// </summary>
        /// <param name="kraScaleDetails"></param>
        /// <returns></returns>
        [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> UpdateScaleDetails(List<KRAScaleDetails> kraScaleDetails, int MaximumScale)
      {
         try
         {
            return await new KRAScaleMasterDetails().UpdateScaleDetails(kraScaleDetails,MaximumScale);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion

      #region DeleteScaleMaster
      /// <summary>
      /// Delete scale master by id
      /// </summary>
      /// <param name="kraScaleMasterId"></param>
      /// <returns></returns>
      [HttpPost]
      [UserInfoActionFilter]
      public async Task<int> DeleteScaleMaster(int kraScaleMasterId)
      {
         try
         {
            return await new KRAScaleMasterDetails().DeleteScaleMaster(kraScaleMasterId);
         }
         catch (Exception ex)
         {
            Log.LogError(ex, Log.Severity.Error, "");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
               Content = new StringContent(ex.Message),
               ReasonPhrase = "Warning"
            });
         }
      }
      #endregion
   }
}
