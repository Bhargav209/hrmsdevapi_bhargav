﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ErrorLogController : BaseApiController
    {
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> LogError(ErrorLogDetails errorLog)
        {
            try
            {
                int type = (int)Enumeration.ErrorLogType.UI;
                return await new ErrorLog().AddErrorLog(errorLog, type);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
    }
}
