﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.DomainEntities;
using AP.API;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class ProficiencyLevelController : ApiController
    {
        #region CreateProficiencyLevel
        /// <summary>
        /// CreateProficiencyLevel
        /// </summary>
        /// <param name="proficiencyLevelData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateProficiencyLevel(ProficiencyLevelData proficiencyLevelData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProficiencyLevelDetails().CreateProficiencyLevel(proficiencyLevelData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateProficiencyLevel
        /// <summary>
        /// UpdateProficiencyLevel
        /// </summary>
        /// <param name="proficiencyLevelData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateProficiencyLevel(ProficiencyLevelData proficiencyLevelData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProficiencyLevelDetails().UpdateProficiencyLevel(proficiencyLevelData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProficiencyLevels
        /// <summary>
        /// GetProficiencyLevels
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProficiencyLevels(bool isActive=true)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProficiencyLevelDetails().GetProficiencyLevels(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        } 
        #endregion
    }
}