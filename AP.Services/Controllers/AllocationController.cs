﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System.Threading.Tasks;
using AP.Utility;

namespace AP.Services.Controllers
{
    public class AllocationController : BaseApiController
    {
        #region GetApprovedTalentRequisitionList
        /// <summary>
        /// To get all the approved talent requisition list
        /// </summary>
        /// <returns></returns>
        [HttpGet]       
        public HttpResponseMessage GetApprovedTalentRequisitionList()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().GetApprovedTalentRequisitionList());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetTaggedEmployeesForTalentRequisition
        /// <summary>
        /// Get Tagged Employees For TalentRequisition
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        [HttpGet]        
        public async Task<List<TalentRequisitionHistoryData>> GetTaggedEmployeesForTalentRequisition()
        {
            List<TalentRequisitionHistoryData> lstReqExpertiseAreas;

            try
            {
                lstReqExpertiseAreas = await new Allocation().GetTaggedEmployeesForTalentRequisition();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get requisition expertise areas.");
            }

            return lstReqExpertiseAreas;
        }
        #endregion

        //#region GetRequiredSkillsDetails
        ///// <summary>
        ///// To get skill detail of the selected role
        ///// </summary>
        ///// <param name="tRId"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public HttpResponseMessage GetRequiredSkillsDetails(int requisitionRoleDetailId)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Allocation().GetRequiredSkillsDetails(requisitionRoleDetailId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        //#region Save Allocation
        ///// <summary>
        ///// Save associate allocation
        ///// </summary>
        ///// <param name="tRId"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public HttpResponseMessage AllocateAssociates(List<AssociateAllocationDetails> associateAllocationList)
        //{
        //    HttpResponseMessage httpResponseMessage = null;

        //    try
        //    {
        //        httpResponseMessage = Request.CreateResponse(new Allocation().AllocateAssociates(associateAllocationList));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return httpResponseMessage;
        //}
        //#endregion

        #region Get Allocation Percentage
        /// <summary>
        /// Get Allocation Percentage
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllocatedPercentage(string empCode, decimal newPercentage)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetAllocatedPercentage(empCode, newPercentage));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region Get Allocation Percentage
        /// <summary>
        /// Get Allocation Percentage
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllocatedPercentage(int? empId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetAllocatedPercentage(empId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetAllocatedPercentages
        /// <summary>
        /// Get Allocation Percentage
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllocatedPercentages(string empIds)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetAllocatedPercentage(empIds));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion


        #region Close Talent Requisition
        /// <summary>
        /// Close Talent Requisition
        /// </summary>
        /// <param name="tRId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage CloseTalentRequisition(int tRId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().CloseTalentRequisition(tRId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion       

        //#region Get matching Profiles
        ///// <summary>
        ///// Get matching Profiles
        ///// </summary>
        ///// <param name="requisitionRoleId"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public HttpResponseMessage GetMatchingProfiles(int requisitionRoleId)
        //{
        //    HttpResponseMessage returnObject = new HttpResponseMessage();

        //    try
        //    {
        //        returnObject = Request.CreateResponse(new Allocation().GetMatchingProfiles(requisitionRoleId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return returnObject;
        //}
        //#endregion

        #region Get Reporting Manager
        /// <summary>
        /// Get matching Profiles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetReportingManager()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetReportingManager());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        //#region Get matched and un-matched skills for Associate
        ///// <summary>
        ///// Get matched and un-matched skills for Associate
        ///// </summary>
        ///// <param name="requisitionRoleId"></param>
        /////  <param name="empId"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public HttpResponseMessage GetMatchingskillsforAssociate(int requisitionRoleId,int empId)
        //{
        //    HttpResponseMessage returnObject = new HttpResponseMessage();

        //    try
        //    {
        //        returnObject = Request.CreateResponse(new Allocation().GetMatchingskillsforAssociate(requisitionRoleId,empId));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }

        //    return returnObject;
        //}
        //#endregion

        #region Check primary skills of Associate
        /// <summary>
        /// Get primary skills of Associate
        /// </summary>
        /// <param name="associateID"></param>
        ///  <param name=" roleID "></param>
        /// <returns>string message </returns>
        [HttpGet]
        public HttpResponseMessage CheckPrimaryRoleofAssociate(int associateID, int roleID)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().CheckPrimaryRoleofAssociate(associateID, roleID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region SetPrimaryRoleofAssociate
        /// <summary>
        /// SetPrimaryRoleofAssociate
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        [HttpPost]        
        public HttpResponseMessage SetPrimaryRoleofAssociate(List<AssociateAllocationDetails> associateAllocationList)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().SetPrimaryRoleofAssociate(associateAllocationList));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region VTS Services

        #region Get All Associate Details
        /// <summary>
        /// GetAllAssociateDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllAssociateDetails()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetAllAssociateDetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region Get Associate Details By Id
        /// <summary>
        /// GetAssociateDetailsById
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateDetailsById(string empId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetAssociateDetailsById(empId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
        #endregion

        #region GetEmployeeByDeptId
        /// <summary>
        /// GetEmployeeByDeptId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetEmployeeByDeptId(int deptId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().GetEmployeeByDeptId(deptId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetTalentPoolList
        /// <summary>
        /// GetTalentPoolList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetTalentPoolList()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().GetTalentPoolList());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region AddToTalentPool
        /// <summary>
        /// AddBehaviorArea
        /// </summary>
        /// <param name="talentPoolData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddToTalentPool(TalentPoolData talentPoolData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (talentPoolData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new Allocation().AddToTalentPool(talentPoolData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region Get Tagged Employees
        /// <summary>
        /// Get Tagged Employees By TalentRequisitionId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetTaggedEmployeesByTalentRequisitionId(int talentRequisitionId, int roleId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().GetTaggedEmployeesByTalentRequisitionId(talentRequisitionId, roleId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetEmployeePrimaryAllocationProject
        /// <summary>
        /// Get Employee Primary Allocation Project
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmployeePrimaryAllocationProject(int EmployeeId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().GetEmployeePrimaryAllocationProject(EmployeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region TemporaryAllocationsandReleases

        #region GetEmployeesForAllocations
        /// <summary>
        /// Get Employees For Allocations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<TagAssociateList>> GetEmployeesForAllocations()
        {
            List<TagAssociateList> httpResponseMessage = null;

            try
            {
                httpResponseMessage = await new Allocation().GetEmployeesForAllocations();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region TemporaryReleaseAssociate
        /// <summary>
        /// TemporaryReleaseAssociate
        /// </summary>
        /// <param name="associateAllocationDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage TemporaryReleaseAssociate(AssociateAllocationDetails associateAllocationDetails)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new Allocation().TemporaryReleaseAssociate(associateAllocationDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetAssociatesToRelease
        /// <summary>
        /// GetAssociatesToRelease
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetAssociatesToRelease(int employeeId, string roleName)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new Allocation().GetAssociatesToRelease(employeeId, roleName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
        #endregion   
    }
}
