﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class AssociateUtilizationController : BaseApiController
    {
        #region GetAssociatesUtilizaton
        /// <summary>
        /// GetAssociatesUtilizaton
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociatesUtilizaton()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateUtility().GetAssociatesUtilizaton());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetPercentUtilization
        /// <summary>
        /// GetPercentUtilization
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetPercentUtilization(int projectId, int employeeId)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();
            
            try
            {
               returnObject = Request.CreateResponse(new AssociateUtility().GetPercentUtilization(projectId, employeeId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region PostAssociateUtilize
        /// <summary>
        /// Add associate utilization
        /// </summary>
        /// <param name="associateUtilize"></param>
        /// <returns></returns>
        // POST: api/AssociateUtilizes
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage PostAssociateUtilize(AssociateUsage associateUtilize)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateUtility().AddAssociateUtilize(associateUtilize));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            
            return returnObject;
        }
        #endregion
        /// <summary>
        /// Import Associate UtilizeReport
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage ImportAssociateUtilizeReport()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateAllocationReport().ImportAssociateAllocationReport());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
    }
}
