﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class CustomKRAsController : BaseApiController
    {
        //#region SaveCustomKRAs
        ///// <summary>
        ///// Save KRAs added by PM (CustomKRA)
        ///// </summary>
        ///// <param name="kraSetData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public bool SaveCustomKRAs(CustomKRAs customKRAs)
        //{
        //    try
        //    {
        //        customKRAs.KRATypeID = Convert.ToInt32(Enumeration.KRAType.AdditionalKRAs);
        //        return new KRA().SaveCustomKRAs(customKRAs);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}

        //#endregion        

        #region GetProjectsByProjectManagerID
        /// <summary>
        /// This method is used to get all the projects under a reporting manager.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetProjectsByProjectManagerID(int projectManagerID)
        {
            List<GenericType> lstProjects;
            try
            {
                lstProjects = await new KRA().GetProjectsByProjectManagerID(projectManagerID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstProjects;
        }
        #endregion

        #region GetEmployeesByProjectID
        /// <summary>
        /// This method is used to get all the employees under a project.
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetEmployeesByProjectID(int projectID)
        {
            List<GenericType> lstEmployees;
            try
            {
                lstEmployees = await new KRA().GetEmployeesByProjectID(projectID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmployees;
        }
        #endregion

        #region GetEmployeesForDepartment
        /// <summary>
        /// This method is used to get all the employees under a department.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetEmployeesForDepartment(int employeeID)
        {
            List<GenericType> lstEmployees;
            try
            {
                lstEmployees = await new KRA().GetEmployeesForDepartment(employeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmployees;
        }
        #endregion

        //#region EditCustomKRAs 
        ///// <summary>
        ///// Update custom KRA
        ///// </summary>
        ///// <param name="kraSetData">kraSetData model</param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> EditCustomKRAs(CustomKRAs customKRAs)
        //{
        //    try
        //    {
        //        return await new KRA().EditCustomKRAs(customKRAs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}
        //#endregion

        //#region DeleteCustomKRA
        ///// <summary>
        ///// Delete customKRA
        ///// </summary>
        ///// <param name="customKRAID"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[UserInfoActionFilter]
        //public async Task<bool> DeleteCustomKRA(int customKRAID)
        //{
        //    try
        //    {
        //        return await new KRA().DeleteCustomKRA(customKRAID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, "");
        //        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //        {
        //            Content = new StringContent(ex.Message),
        //            ReasonPhrase = "Warning"
        //        });
        //    }
        //}
        //#endregion

    }
}