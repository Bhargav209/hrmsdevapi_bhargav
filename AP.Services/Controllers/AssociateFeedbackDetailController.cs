﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class AssociateFeedbackDetailController : BaseApiController
    {
        #region GetProjectsByUserID
        /// <summary>
        /// GetProjectsByUserID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProjectsByUserID(string userName)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().GetProjectsByUserID(userName));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProjectsByUserID
        /// <summary>
        /// GetProjectsByUserID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetManagerandLeadByProjectID(int projectID)
        { 
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProjectDetails().GetReportingDetailsByProjectId(projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAssociatesByProjectID
        /// <summary>
        /// GetAssociatesByProjectID
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociatesByProjectID(int projectID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().GetAssociatesByProjectID(projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAssociateFeedbacks
        /// <summary>
        /// GetAssociateFeedbacks
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateFeedbacks(string userName, int projectID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().GetAssociateFeedbacks(userName, projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAllAssociateFeedbacks
        /// <summary>
        /// GetAllAssociateFeedbacks
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllAssociateFeedbacks(int projectID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().GetAllAssociateFeedbacks(projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAssociateFeedbackByID
        /// <summary>
        /// GetAssociateFeedbackByID
        /// </summary>
        /// <param name="associateFeedbackID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateFeedbackByID(int associateFeedbackID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().GetAssociateFeedbackByID(associateFeedbackID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetSkillsByCompAreaId
        /// <summary>
        /// GetSkillsByCompAreaId
        /// </summary>
        /// <param name="competenctAreaID and projectID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSkillsByCompAreaId(int competenctAreaID, int projectID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().GetSkillsByCompAreaId(competenctAreaID, projectID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeleteAssociateFeedbackByID
        /// <summary>
        /// DeleteAssociateFeedbackByID
        /// </summary>
        /// <param name="associateFeedbackID"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage DeleteAssociateFeedbackByID(int associateFeedbackID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().DeleteAssociateFeedbackByID(associateFeedbackID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region SaveAssociateFeedback
        /// <summary>
        /// SaveAssociateFeedback
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveAssociateFeedback(EmployeeFeedback associateFeedbackDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFeedbackDetails().SaveAssociateFeedback(associateFeedbackDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
        
    }
}