﻿using System.Web.Http;
using System.Net.Http;
using System.Net;
using System;
using System.Resources;
using AP.API;
using AP.DomainEntities;
using AP.Utility;
using AP.Services.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.Services.Controllers
{
    public class AssociateSkillsController : BaseApiController
    {
        #region MasterSkillMethods

        #region CreateSkillsMaster
        /// <summary>
        /// Service to call CreateSkillsMaster method
        /// </summary>
        /// <param name="skillData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateSkillsMaster(SkillData skillData)
        {

            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateSkills().CreateSkillsMaster(skillData));
            }
            catch (Exception ex)
            {
            Log.LogError(ex, Log.Severity.Error, "Error in CreateSkillsMaster");
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetSkillsData
        /// <summary>
        /// isActive
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSkillsData(bool isActive=true)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateSkills().GetSkillsData(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetSkillsData
        /// <summary>
        /// Get active and approved Skill details from system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetActiveSkillsData()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateSkills().GetActiveSkillsData());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region UpdateSkillMasterDetails
        /// <summary>
        /// Service to call UpdateSkillMasterDetails
        /// </summary>
        /// <param name="skillData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateSkillMasterDetails(SkillData skillData)
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateSkills().UpdateSkillMasterDetails(skillData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        } 
        #endregion

        #endregion

        #region Associate Skill Tab Methods    
         
        #region GetAssociateSkillsByID
        /// <summary>
        /// Get Associate Skills Details By EmpID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateSkillsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetAssociateSkillsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetCompetencyArea
        /// <summary>
        /// to get competency are for the new skills design tab
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCompetencyArea()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetCompetencyArea());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetCompetencySkills
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="competencyAreaID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCompetencySkills(int roleID, int competencyAreaID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetCompetencySkils(roleID, competencyAreaID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProficiencyLevel
        /// <summary>
        /// to get proficiency level in the skills page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProficiencyLevel()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetProficiencyLevel());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion 

        #region GetSkills
        /// <summary>
        /// to get Skills skills page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSkills(int competenctAreaID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetSkills(competenctAreaID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region AddSkillDetails
        /// <summary>
        /// Add / Update Associate Skills
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddAssociateSkills(EmployeeSkillDetails skillDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().AddAssociateSkills(skillDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;

        }
        #endregion

        #region GetSkillsById
        /// <summary>
        /// to get Skills by Employee Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSkillsById(int empId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetSkillsById(empId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #endregion

        #region Associate Project Tab Methods

        #region AddAssociateProjects
        /// <summary>
        /// Add Associate Projects Details
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddAssociateProjects(UserDetails details)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().AddAssociateProjects(details));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetAssociateProjectDetailsByID
        /// <summary>
        /// Get Associate Project Details By ID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAssociateProjectDetailsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().GetAssociateProjectDetailsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateAssociateProjectDetails
        /// <summary>
        ///Update Associate Project Details
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateAssociateProjectDetails(UserDetails details)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().UpdateAssociateProjectDetails(details));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;

        }
        #endregion
        #endregion

        #region GetSkills
        /// <summary>
        /// Service to call GetSkills method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        // This service was written for Angular2 application and is being consumed in Angular2 application only.
        public HttpResponseMessage GetSkills()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();

            try
            {
                returnObject = Request.CreateResponse(new AssociateSkills().GetSkills());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetsAssociateSkills
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<EmployeeSkillDetails>> GetAssociateSkills(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                lstSkill = await new AssociateSkills().GetAssociateSkills(empID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkill;
        }
        #endregion

        #region GetsAssociateSkills
        /// <summary>
        /// Gets Skill Group based upon CompetencyAreaId  and SkillGroupId
        /// </summary>
        /// <param name="skillGroupID"></param>
        /// <param name="competencyID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<EmployeeSkillDetails>> GetSkillsByID(int skillGroupID, int competencyAreaID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                lstSkill = await new AssociateSkills().GetSkillsByID(skillGroupID, competencyAreaID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkill;
        }
        #endregion

        #region DeleteAssociateSkill
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteAssociateSkill(int id)
        {
            try
            {
                return await new AssociateSkills().DeleteAssociateSkill(id);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetSkillsBySearchString
        /// <summary>
        /// Service to call GetSkills method
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GenericType>> GetSkillsBySearchString(string searchString)
        {
            List<GenericType> skillsList;

            try
            {
                skillsList = await new AssociateSkills().GetSkillsBySearchString(searchString);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return skillsList;
        }
        #endregion
        #region GetDraftApproveStatusSkills
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<EmployeeSkillDetails>> GetDraftApproveStatusSkills(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                lstSkill = await new AssociateSkills().GetDraftApproveStatusSkills(empID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkill;
        }
        #endregion

        #region GetPendingStatusSkills
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<EmployeeSkillDetails>> GetPendingStatusSkills(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                lstSkill = await new AssociateSkills().GetPendingStatusSkills(empID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkill;
        }
        #endregion

        #region SubmitSkillDetails
        /// <summary>
        /// Submit Associate skills to  Lead for approval
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SubmitAssociateSkills(EmployeeSkillDetails skillDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateSkills().SubmitAssociateSkills(skillDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;

        }
        #endregion
    }
}