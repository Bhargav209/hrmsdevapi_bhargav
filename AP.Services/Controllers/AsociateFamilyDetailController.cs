﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.DomainEntities;
using AP.API;
using AP.Utility;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class AsociateFamilyDetailController : BaseApiController
    {
        #region GetFamilyDetailsByID
        /// <summary>
        /// GetFamilyDetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetFamilyDetailsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFamilyDetails().GetFamilyDetailsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateFamilyDetails
        /// <summary>
        /// UpdateFamilyDetails
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateFamilyDetails(UserDetails details)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateFamilyDetails().UpdateFamilyDetails(details));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

    }
}
