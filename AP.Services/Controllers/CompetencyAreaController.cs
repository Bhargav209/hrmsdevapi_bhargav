﻿using System;
using System.Net;
using System.Web.Http;
using System.Net.Http;
using AP.DomainEntities;
using AP.API;
using AP.Services.Filters;

namespace AP.Services
{
    public class CompetencyAreaController : ApiController
    {
        #region CreateCompetencyArea
        /// <summary>
        /// CreateCompetencyArea
        /// </summary>
        /// <param name="competencyAreaData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateCompetencyArea(CompetencyAreaData competencyAreaData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new CompetencyAreaDetails().CreateCompetencyArea(competencyAreaData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateCompetencyArea
        /// <summary>
        /// UpdateCompetencyArea
        /// </summary>
        /// <param name="competencyAreaData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateCompetencyArea(CompetencyAreaData competencyAreaData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new CompetencyAreaDetails().UpdateCompetencyArea(competencyAreaData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetCompetencyAreaDetails
        /// <summary>
        /// GetCompetencyAreaDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCompetencyAreaDetails(bool isActive=true)
        {
            HttpResponseMessage httpResponse = null;

            try
            {
                httpResponse = Request.CreateResponse(new CompetencyAreaDetails().GetCompetencyAreaDetails(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponse;
        }
        #endregion

      
    }

}