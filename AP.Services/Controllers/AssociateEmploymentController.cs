﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.DomainEntities;
using AP.API;
using AP.Utility;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class AssociateEmploymentController : BaseApiController
    {
        #region GetEmploymentDetailsByID
        /// <summary>
        /// GetEmploymentDetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmploymentDetailsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateEmploymentDetails().GetEmploymentDetailsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProfReferenceDetailsByID
        /// <summary>
        /// GetProfReferenceDetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProfReferenceDetailsByID(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateProfessionalDetails().GetProfReferenceDetailsByID(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region SaveEmployementDetails
        /// <summary>
        /// Add/Update EmployementDetails
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveEmployementDetails(UserDetails details)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateEmploymentDetails().SaveEmployementDetails(details));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region Assign Reporting Manager

        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AssignReportingManager(EmploymentDetails employeeData)
        {
            HttpResponseMessage response = null;
            try
            {
                response = Request.CreateResponse(new AssociateEmploymentDetails().AssignReportingManager(employeeData));
            }
            catch (Exception ex)
            {

                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetReportingManagers()
        {
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateEmploymentDetails().GetReportingManagers());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Error while fetching Reporting Managers"
                });
            }
            return httpResponseMessage;
        }

        #endregion

        #region UpdateDesignationToAssociate
        /// <summary>
        /// UpdateDesignationToAssociate
        /// </summary>
        /// <param name="designationData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateDesignationToAssociate(AssociateDesignationData designationData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateEmploymentDetails().UpdateDesignationToAssociate(designationData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetDesignationDetailsByID
        /// <summary>
        /// GetDesignationDetailsByID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetDesignationDetailsByID(int employeeID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateEmploymentDetails().GetDesignationDetailsByID(employeeID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

    }
}
