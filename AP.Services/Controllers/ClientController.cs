﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;

namespace AP.Services.Controllers
{
    [Authorize(Roles = "Program Manager,Department Head")]
    public class ClientController : BaseApiController
    {
        #region CreateClient
        /// <summary>
        /// Create Client
        /// </summary>
        /// <param name="clientDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateClient(ClientDetails clientDetails)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new Clients().CreateClient(clientDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region UpdateClient
        /// <summary>
        /// Update Client
        /// </summary>
        /// <param name="clientDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateClient(ClientDetails clientDetails)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new Clients().UpdateClient(clientDetails));
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetClientsDetails
        /// <summary>
        /// GetClientsDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetClientsDetails(bool isActive = true)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new Clients().GetClientsDetails(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion

        #region GetClientsById
        /// <summary>
        /// GetClientsById
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetClientsById(int clientId)
        {
            HttpResponseMessage returnObject = null;

            try
            {
                returnObject = Request.CreateResponse(new Clients().GetClientsById(clientId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return returnObject;
        }
        #endregion
    }
}
