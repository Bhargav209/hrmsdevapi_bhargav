﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.Services.Attributes;
using AP.Services.Filters;
using AP.DomainEntities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AP.Utility;
using AP.DataStorage;

namespace AP.Services.Controllers
{
    public class MasterDataController : BaseApiController
    {
        #region GetProjectsList
        /// <summary>
        /// GetProjectsList
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]

        public async Task<List<ProjectData>> GetProjectsList()
        {
            List<ProjectData> lstProjects;
            try
            {
                lstProjects = await new MasterData().GetProjectsList();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstProjects;
        }       

        #endregion
        #region Get Associate Functions

        /// <summary>
        /// Gets associates names by department id
        /// </summary>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetAssociatesByDepartmentId(int departmentID)
        {
            try
            {
                return await new MasterData().GetAssociatesByDepartmentId(departmentID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        /// <summary>
        /// Get Employees By projectid and departmentid
        /// </summary>
        /// <param name="projectID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateRoleMappingData>> GetEmployeesByDepartmentAndProjectID(int? projectId, int departmentId, bool isNew)
        {
            List<AssociateRoleMappingData> lstEmployees;
            try
            {
                lstEmployees = await new MasterData().GetEmployeesByDepartmentAndProjectID(projectId, departmentId, isNew);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmployees;
        }

        /// <summary>
        /// Get Employees By projectid and departmentid
        /// </summary>
        /// <param name="projectID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateRoleMappingData>> GetEmployeesForViewKraInformation(int? projectId, int departmentId, int financialYearId)
        {
            List<AssociateRoleMappingData> lstEmployees;
            try
            {
                lstEmployees = await new MasterData().GetEmployeesForViewKraInformation(projectId, departmentId, financialYearId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmployees;
        }

        /// <summary>
        /// Get Employee name By employeeID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<string> GetEmployeeNameByEmployeeId(int employeeID)
        {
            try
            {
                return await new MasterData().GetEmployeeNameByEmployeeId(employeeID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region Get Managers And Competency Leads
        /// <summary>
        /// Gets reporting managers, program managers and competency leads list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetManagersAndCompetencyLeads()
        {
            try
            {
                return await new MasterData().GetManagersAndCompetencyLeads();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region Get Program Managers

        /// <summary>
        /// Gets rprogram managers list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetProgramManagers()
        {
            try
            {
                return await new MasterData().GetProgramManagers();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region Get Financial Year Functions

        /// <summary>
        /// Gets current financial year
        /// </summary>
        /// <returns></returns>
        /// [HttpGet]
        [UserInfoActionFilter]
        public async Task<FinancialYearData> GetCurrentFinancialYear()
        {
            FinancialYearData lstFinancialYear;
            try
            {
                lstFinancialYear = await new MasterData().GetCurrentFinancialYear();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstFinancialYear;
        }

        /// <summary>
        /// Gets financial year list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetFinancialYearList()
        {
            List<GenericType> lstFinancialYear;
            try
            {
                lstFinancialYear = await new MasterData().GetFinancialYearList();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstFinancialYear;
        }

        #endregion

        #region Get ADR Cycle

        /// <summary>
        /// Gets ADR Cycle
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ADRCycleDetail>> GetADRCycle()
        {
            try
            {
                return await new MasterData().GetADRCycle();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region Get Department Types

        /// <summary>
        /// Gets department types list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetDepartmentTypes()
        {
            try
            {
                return await new MasterData().GetDepartmentTypes();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region Get Department By Department Type Id

        /// <summary>
        /// Gets department by department type id
        /// </summary>
        /// <param name="departmentTypeId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetDepartmentByDepartmentTypeId(int departmentTypeId)
        {
            try
            {
                return await new MasterData().GetDepartmentByDepartmentTypeId(departmentTypeId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetDesignations
        /// <summary> 
        /// Gets list of designations by search string
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetDesignations(string searchString)
        {
            List<GenericType> lstDesignations;
            try
            {
                lstDesignations = await new MasterData().GetDesignations(searchString);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstDesignations;
        }
        #endregion
    }
}

