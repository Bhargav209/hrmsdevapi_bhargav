﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class KRARoleController : BaseApiController
    {
        #region CreateKRAForRole
        /// <summary>
        /// Create KRA for role
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateKRAForRole(KRARoleData kraRoleData)
        {
            try
            {
                kraRoleData.StatusID = Convert.ToInt32(Enumeration.KRA.Draft);
                return await new KRARole().CreateKRAForRole(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region CloneKRARoleTemplate
        /// <summary>
        /// Clone KRAs
        /// </summary>
        /// <param name="kraRoleData"></param>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<List<KRARoleData>> CloneKRARoleTemplate(KRARoleData kraRoleData)
        {
            try
            {
                kraRoleData.StatusID = Convert.ToInt32(Enumeration.KRA.Draft);
                return await new KRARole().CloneKRARoleTemplate(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteKRARoleAspect
        /// <summary>
        /// DeleteKRARoleAspect
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="kraAspectID"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteKRARoleAspect(int roleID, int kraAspectID, int financialYearID)
        {
            try
            {
                return await new KRARole().DeleteKRARoleAspect(roleID, kraAspectID, financialYearID);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteKRARoleMetric
        /// <summary>
        /// DeleteKRARoleMetric
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteKRARoleMetric(int id)
        {
            try
            {
                return await new KRARole().DeleteKRARoleMetric(id);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateKRARoleMetric
        /// <summary>
        /// Update a KRA Role Metric
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateKRARoleMetric(KRARoleData kraRoleData)
        {
            try
            {
                return await new KRARole().UpdateKRARoleMetric(kraRoleData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetKRATemplateByRoleID
        /// <summary>
        /// GetKRATemplateByRoleID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRARoleData>> GetKRATemplateByRoleID(int RoleID, int FinancialYearID)
        {
            List<KRARoleData> lstKraTemplate;
            try
            {
                lstKraTemplate = await new KRARole().GetKRATemplateByRoleID(RoleID, FinancialYearID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstKraTemplate;
        }
        #endregion

        #region GetRoleName
        /// <summary>
        /// GetRoleName
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetRoleName(int roleId)
        {
            try
            {
                return new KRARole().GetRoleName(roleId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion  
        #region GetKRARoles
        /// <summary>
        /// Get KRA Roles
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<KRARoleData>> GetKRARoles()
        {
            List<KRARoleData> getKRARoles;
            try
            {
                getKRARoles = await new KRARole().GetKRARoles();               
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return getKRARoles;
        }
        #endregion   
        #region GetEmployeesByKRARole
        /// <summary>
        /// Get Employees By KRARole
        /// </summary>
        /// <param name="KRAroleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<KRARoleData>> GetEmployeesByKRARole(int KRARoleId,int financialYearId)
        {
            List<KRARoleData> getEmpByKRARole;
            try
            {
                getEmpByKRARole = await new KRARole().GetEmployeesByKRARole(KRARoleId,financialYearId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return getEmpByKRARole;
        }
        #endregion            

        #region GetDepartmentName
        /// <summary>
        /// GetDepartmentName
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetDepartmentName(int departmentId)
        {
            try
            {
                return new KRARole().GetDepartmentName(departmentId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}