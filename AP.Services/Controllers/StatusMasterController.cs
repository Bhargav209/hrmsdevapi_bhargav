﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;

namespace AP.Services.Controllers
{
    public class StatusMasterController : BaseApiController
    {
        #region CreateStatusMaster
        /// <summary>
        /// Create status master
        /// </summary>
        /// <param name="statusData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage CreateStatusMaster(StatusData statusData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new StatusMaster().CreateStatusMaster(statusData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetStatusMasterDetails
        /// <summary>
        /// Get Status Master Details
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetStatusMasterDetails(bool isActive=true)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new StatusMaster().GetStatusMasterDetails(isActive));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdateStatusMasterDetails
        /// <summary>
        /// Update status master details
        /// </summary>
        /// <param name="statusData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdateStatusMasterDetails(StatusData statusData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new StatusMaster().UpdateStatusMasterDetails(statusData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}