﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class JDTemplateController : BaseApiController
    {
        #region GetJDTemplateTitles
        /// <summary>
        /// Gets list JD TemplateTitles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GenericType>> GetJDTemplateTitles()
        {
            List<GenericType> lstJDTemplateTitles;
            try
            {
                lstJDTemplateTitles = await new JDtemplateDetails().GetJDTemplateTitles();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstJDTemplateTitles;
        }
        #endregion

        #region GetTemplateSkills
        /// <summary>
        /// Gets list TemplateSkills by TemplateId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<JDTemplateMasterData>> GetTemplateSkills(int templateTitleId)
        {
            List<JDTemplateMasterData> lstJDTemplateSkills;
            try
            {
                lstJDTemplateSkills = await new JDtemplateDetails().GetTemplateSkills(templateTitleId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstJDTemplateSkills;
        }
        #endregion        

        #region AddJDTemplateSkills
        /// <summary>
        /// Create  JDTemplateSkills
        /// </summary>
        /// <param name="JDTemplateDetailsData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> AddJDTemplateSkills(JDTemplateDetailsData JDTemplateDetailsData)
        {
            try
            {
                return await new JDtemplateDetails().AddJDTemplateSkills(JDTemplateDetailsData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}
