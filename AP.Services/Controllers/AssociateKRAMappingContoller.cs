﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class AssociateKRAMappingController : BaseApiController
    {
        #region GetEmployeeDetailsByDepartmentAndRole
        /// <summary>
        /// Get Employee Details By Department And Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmployeeDetailsByDepartmentAndRole(int DepartmentID, int RoleMasterID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new KRAMapping().GetEmployeeDetailsByDepartmentAndRole(DepartmentID, RoleMasterID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GenerateKRAPdfForAllAssociates
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<bool> GenerateKRAPdfForAllAssociates(int overideExisting)
        {
            try
            {
                //HttpResponseMessage httpResponseMessage = null;

                try
                {
                    return await new KRAMapping().GenerateKRAPdfForAllAssociates(overideExisting);
                    //httpResponseMessage = Request.CreateResponse(new KRAMapping().GenerateKRAPdfForAllAssociates());
                }
                catch (Exception ex)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = "Warning"
                    });
                }

                //return httpResponseMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion        

        #region UpdateAssociateRole
        /// <summary>
        /// Update role in allocation table for delivery or save role for service dept
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateAssociateRole(AssociateRoleMappingData roleMapping)
        {
            try
            {
                return await new KRAMapping().UpdateAssociateRole(roleMapping);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        //#region GenerateKRAPdfSelectedAllAssociates
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<bool> GenerateKRAPdfSelectedAllAssociates(KRAPdfData[] empList)
        //{
        //     try
        //        {
        //            return await new KRAMapping().GenerateKRAPdfSelectedAllAssociates(empList);
                    
        //        }
        //     catch (Exception ex)
        //        {
        //            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
        //            {
        //                Content = new StringContent(ex.Message),
        //                ReasonPhrase = "Warning"
        //            });
        //        }
        //}
        //#endregion        
    }
}
