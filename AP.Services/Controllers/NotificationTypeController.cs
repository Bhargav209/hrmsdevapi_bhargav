﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class NotificationTypeController : BaseApiController
    {
        #region CreateNotificationType
        /// <summary>
        /// Create a new KRA Aspect
        /// </summary>
        /// <param name="notificationTypeDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateNotificationType(NotificationTypeDetails notificationTypeDetails)
        {
            try
            {
                return await new NotificationType().CreateNotificationType(notificationTypeDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateNotificationType
        /// <summary>
        /// Update a NotificationType
        /// </summary>
        /// <param name="notificationTypeDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateNotificationType(NotificationTypeDetails notificationTypeDetails)
        {
            try
            {
                return await new NotificationType().UpdateNotificationType(notificationTypeDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetNotificationType
        /// <summary>
        /// Gets list of notification types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<NotificationTypeDetails>> GetNotificationType()
        {
            List<NotificationTypeDetails> lstNotificationType;
            try
            {
                lstNotificationType = await new NotificationType().GetNotificationType();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstNotificationType;
        }
        #endregion

        #region DeleteNotificationTypeByTypeId
        /// <summary>
        /// Delete Notification type by notification type id
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteNotificationTypeByTypeId(int notificationTypeId, int categoryId)
        {
            try
            {
                return await new API.NotificationType().DeleteNotificationTypeByTypeId(notificationTypeId, categoryId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
    }
}