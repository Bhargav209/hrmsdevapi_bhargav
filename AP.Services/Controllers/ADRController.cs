﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class ADRController : BaseApiController
    {
        #region Create ADR Cycle
        /// <summary>
        /// Create ADR Cycle
        /// </summary>
        /// <param name="ADR Cycle"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateADRCycle(ADRCycleDetail adrCycleDetail)
        {
            try
            {
                return await new ADR().CreateADRCycle(adrCycleDetail);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion
        #region CreateADRSection
        /// <summary>
        /// Create ADR Section
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateADRSection(ADRData adrData)
        {
            try
            {
                return await new ADR().CreateADRSection(adrData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion
        #region CreateAssociateADRDetail
        /// <summary>
        /// Create Associate ADR Detail
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateAssociateADRDetail(ADRData adrData)
        {
            try
            {
                return await new ADR().CreateAssociateADRDetail(adrData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #endregion

        #region GetAssociateADRDetail
        /// <summary>
        /// Get Associate ADR Detail
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ADRData>> GetAssociateADRDetail(int EmployeeID, int FinancialYearID, int AdrCycleId)
        {
            try
            {
                return await new ADR().GetAssociateADRDetail(EmployeeID,FinancialYearID,AdrCycleId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateAssociateADRDetail
        /// <summary>
        /// Update Associate ADR Detail
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateAssociateADRDetail(ADRData adrData)
        {
            try
            {
                return await new ADR().UpdateAssociateADRDetail(adrData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetADRSection
        /// <summary>
        /// Get ADR Section
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ADRData>> GetADRSection()
        {
            try
            {
                return await new ADR().GetADRSection();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        #region UpdateADRSection
        /// <summary>
        /// Update ADR Section
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateADRSection(ADRData adrData)
        {
            try
            {
                return await new ADR().UpdateADRSection(adrData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion
        /// <summary>
        /// Set ADR cycle active
        /// </summary>
        /// <param name="adrCycleDetail">kraSetData data model</param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> SetADRCycleActive(ADRCycleDetail adrCycleDetail)
        {
            try
            {
                return await new ADR().SetADRCycleActive(adrCycleDetail);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        #region GetEmployeeKRAByEmployeeId
        /// <summary>
        /// Gets Employee KRA's by employeeId, Financial year and ADRCycleID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAAspectData>> GetEmployeeKRAByEmployeeId(int EmployeeId, int FinancialYearId, int ADRCycleID)
        {
            try
            {
                return await new ADR().GetEmployeeKRAByEmployeeId(EmployeeId, FinancialYearId, ADRCycleID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetCommentsByEmployeeId
        /// <summary>
        /// Gets all the comments of KRA Metrics by employeeId, Financial year and ADRCycleID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAAspectData>> GetCommentsByEmployeeId(int EmployeeId, int FinancialYearId, int ADRCycleID)
        {
            try
            {
                return await new ADR().GetCommentsByEmployeeId(EmployeeId, FinancialYearId, ADRCycleID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetMetricsAndCommentsByAspectId
        /// <summary>
        /// Gets KRA Metrics and Comments by employeeId, Financial year and ADRCycleID
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAAspectData>> GetMetricsAndCommentsByAspectId(int EmployeeId, int FinancialYearId, int ADRCycleID, int KRAAspectId)
        {
            try
            {
                return await new ADR().GetMetricsAndCommentsByAspectId(EmployeeId, FinancialYearId, ADRCycleID, KRAAspectId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetCommentsByAspectAndMetricId
        /// <summary>
        /// Gets Comments by AspectId, MetricId, EmployeeId, Financial year and ADRCycleID.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<KRAAspectData>> GetCommentsByAspectAndMetricId(int EmployeeId, int FinancialYearId, int ADRCycleID, int KRAAspectId, int KRAMetricId)
        {
            try
            {
                return await new ADR().GetCommentsByAspectAndMetricId(EmployeeId, FinancialYearId, ADRCycleID, KRAAspectId, KRAMetricId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetADRMeasurementAreas
        /// <summary>
        /// Get ADR Measurement Areas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ADRData>> GetCurrentYearADRMeasurementAreas()
        {
            try
            {
                return await new ADR().GetCurrentYearADRMeasurementAreas();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}
