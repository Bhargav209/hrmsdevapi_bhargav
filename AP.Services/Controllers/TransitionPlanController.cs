﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class TransitionPlanController : BaseApiController
    {
        #region SaveTransitionPlanDetails
        /// <summary>
        /// SaveTransitionPlanDetails
        /// </summary>
        /// <param name="planData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveTransitionPlan(TransitionPlanData planData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (planData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().SaveTransitionPlan(planData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetProjectByReportingManager
        /// <summary>
        /// GetProjectByReportingManager
        /// </summary>
        /// <param name="reportingManagerId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetProjectByReportingManager(int reportingManagerId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().GetProjectByReportingManager(reportingManagerId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetRoleByEmployeeID
        /// <summary>
        /// GetRoleByEmployeeID
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetRoleByEmployeeID(int employeeId, int projectId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().GetRoleByEmployeeID(employeeId,projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetEmployeeById
        /// <summary>
        /// GetEmployeeById
        /// </summary>
        /// <param name="reportingManagerId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetEmployeeById(int reportingManagerId, int projectId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().GetEmployeeById(reportingManagerId, projectId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetTasksById
        /// <summary>
        /// GetTasksById
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetTasksById(int categoryId)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().GetTasksById(categoryId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetCategories
        /// <summary>
        /// GetCategories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public HttpResponseMessage GetCategories()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().GetCategories());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region SaveTransitionPlanDetails
        /// <summary>
        /// SaveTransitionPlanDetails
        /// </summary>
        /// <param name="planDetailsData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage SaveTransitionPlanDetails(List<TransitionPlanDetailsData> planDetailsData)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                if (planDetailsData == null)
                    throw new ArgumentNullException(resourceManager.GetString("DetailsMustBeProvidedExceptionMessage"));

                httpResponseMessage = Request.CreateResponse(new AssociateTransitionPlan().SaveTransitionPlanDetails(planDetailsData));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
