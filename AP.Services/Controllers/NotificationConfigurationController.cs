﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class NotificationConfigurationController : BaseApiController
    {
        #region GetNotificationConfigurationDetailsByNotificationTypeId
        /// <summary>
        /// Gets notification configuration details by notification type id
        /// </summary>
        /// <param name="notificationTypeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<EmailNotificationConfiguration> GetNotificationConfigurationDetailsByNotificationTypeId(int notificationTypeID, int categoryId)
        {
            EmailNotificationConfiguration notificationConfiguration;
            try
            {
                notificationConfiguration = await new NotificationConfiguration().GetNotificationConfigurationDetailsByNotificationTypeId(notificationTypeID, categoryId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return notificationConfiguration;
        }
        #endregion

        #region GetEmployeeWorkEmails
        /// <summary>
        /// Gets list of work emails of associates by search string
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<string>> GetEmployeeWorkEmails(string searchString)
        {
            List<string> lstEmails;
            try
            {
                lstEmails = await new NotificationConfiguration().GetEmployeeWorkEmails(searchString);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstEmails;
        }
        #endregion

        #region CreateNotificationConfiguration
        /// <summary>
        /// Create a new notification configuration
        /// </summary>
        /// <param name="notificationConfigurationDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateNotificationConfiguration(EmailNotificationConfiguration notificationConfigurationDetails)
        {
            try
            {
                return await new NotificationConfiguration().CreateNotificationConfiguration(notificationConfigurationDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateNotificationConfiguration
        /// <summary>
        /// Update a notification configuration
        /// </summary>
        /// <param name="notificationConfigurationDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateNotificationConfiguration(EmailNotificationConfiguration notificationConfigurationDetails)
        {
            try
            {
                return await new NotificationConfiguration().UpdateNotificationConfiguration(notificationConfigurationDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetNotificationTypeByCategory
        /// <summary>
        /// Gets list of notification types by Category
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<GenericType>> GetNotificationType(int categoryId)
        {
            List<GenericType> lstNotificationType;
            try
            {
                lstNotificationType = await new NotificationConfiguration().GetNotificationType(categoryId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstNotificationType;
        }
        #endregion
    }
}