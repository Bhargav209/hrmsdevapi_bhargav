﻿using AP.Utility;
using System.Resources;
using System.Web.Http;

namespace AP.Services.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseApiController : ApiController
    {
        protected ResourceManager resourceManager = null;

        public BaseApiController()
        {
            resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);
        }
    }
}
