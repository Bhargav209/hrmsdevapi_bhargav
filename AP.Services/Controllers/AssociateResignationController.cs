﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static AP.Utility.Enumeration;

namespace AP.Services.Controllers
{
    public class AssociateResignationController : BaseApiController
    {
        #region CreateAssociateResignation
        /// <summary>
        /// Save Resignation Details
        /// </summary>
        /// <param name="resignationDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateAssociateResignation(AssociateResignationData resignationDetails)
        {
            try
            {
                if (resignationDetails == null)
                    throw new ArgumentNullException("ResignationDetails cannot be null.");

                return await new AssociateResignationDetails().CreateAssociateResignation(resignationDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetResignationDetails
        /// <summary>
        /// Get Resignation Details
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<AssociateResignationData> GetResignationDetails(int employeeID)
        {
            try
            {
                return await new AssociateResignationDetails().GetResignationDetails(employeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region ResignationDashboard
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateResignationData>> ResignationDashboard(int employeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                lstResignation = await new AssociateResignationDetails().ResignationDashboard(employeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstResignation;
        }
        #endregion

        #region GetAssociatePendingResignationsList
        /// <summary>
        /// Gets list of pending associate resignations.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateResignationData>> GetAssociatePendingResignationsList(int EmployeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                lstResignation = await new AssociateResignationDetails().GetAssociatePendingResignationsList(EmployeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstResignation;
        }
        #endregion

        #region GetAssociateApprovedResignationsList
        /// <summary>
        /// Gets list of approved associate resignations.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateResignationData>> GetAssociateApprovedResignationsList(int EmployeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                lstResignation = await new AssociateResignationDetails().GetAssociateApprovedResignationsList(EmployeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstResignation;
        }
        #endregion

        #region GetAssociateRejectedResignationsList
        /// <summary>
        /// Gets list of rejected associate resignations.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateResignationData>> GetAssociateRejectedResignationsList(int EmployeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                lstResignation = await new AssociateResignationDetails().GetAssociateRejectedResignationsList(EmployeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstResignation;
        }
        #endregion

        #region GetDeliveryHeadsList
        /// <summary>
        /// Gets list of delivery heads.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateResignationData>> GetDeliveryHeadsList()
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                lstResignation = await new AssociateResignationDetails().GetDeliveryHeadList();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstResignation;
        }
        #endregion

        #region ApproveResignationByPM
        /// <summary>
        /// ApproveResignation
        /// </summary>
        /// <param name="approvalData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> ApproveResignationByPM(ResignationApprovalData approvalData)
        {
            try
            {
                 
                if (approvalData == null)
                    throw new ArgumentNullException("Approval Data cannot be null.");

                approvalData.StatusID = (int)AssociateExitStatusCodes.ApprovedByPM;

                return await new AssociateResignationDetails().ApproveResignation(approvalData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region ApproveResignationByDH
        /// <summary>
        /// ApproveResignation
        /// </summary>
        /// <param name="approvalData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> ApproveResignationByDH(ResignationApprovalData approvalData)
        {
            try
            {
                
                if (approvalData == null)
                    throw new ArgumentNullException("Approval Data cannot be null.");

                approvalData.StatusID = (int)AssociateExitStatusCodes.ApprovedByDeliveryHead;

                return await new AssociateResignationDetails().ApproveResignation(approvalData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region RejectResignation
        /// <summary>
        /// RejectResignation
        /// </summary>
        /// <param name="approvalData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> RejectResignation(ResignationApprovalData approvalData)
        {
            try
            {
                if (approvalData == null)
                    throw new ArgumentNullException("Approval Data cannot be null.");

                approvalData.StatusID = (int)AssociateExitStatusCodes.Rejected;

                return await new AssociateResignationDetails().ApproveResignation(approvalData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}