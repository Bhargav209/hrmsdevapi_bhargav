﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class CategoryController : BaseApiController
    {
        #region CreateCategory
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> CreateCategory(CategoryDetails categoryDetails)
        {
            try
            {
                if (categoryDetails == null)
                    throw new ArgumentNullException("CategoryDetails cannot be null.");

                return await new Category().CreateCategory(categoryDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateCategory
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateCategory(CategoryDetails categoryDetails)
        {
            try
            {
                if (categoryDetails == null)
                    throw new ArgumentNullException("CategoryDetails cannot be null.");

                return await new Category().UpdateCategory(categoryDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteCategory
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteCategory(CategoryDetails categoryDetails)
        {
            try
            {
                if (categoryDetails == null)
                    throw new ArgumentNullException("CategoryDetails cannot be null.");

                return await new Category().DeleteCategory(categoryDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetsCategory
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<CategoryDetails>> GetsCategory()
        {
            List<CategoryDetails> lstCategories;
            try
            {
                lstCategories = await new Category().GetCategories();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstCategories;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<CategoryDetails>> GetActiveCategories()
        {
            List<CategoryDetails> lstCategories;
            try
            {
                lstCategories = await new Category().GetActiveCategories();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstCategories;
        }
        #endregion
    }
}
