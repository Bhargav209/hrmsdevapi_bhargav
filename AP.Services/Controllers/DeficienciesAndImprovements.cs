﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class DeficienciesAndImprovementsController : BaseApiController
    {
        #region GetDeficienciesAndImprovements
        /// <summary>
        /// Gets GetDeficienciesAndImprovements
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<DeficienciesAndImprovementsData>> GetDeficienciesAndImprovements(int employeeId)
        {
            List<DeficienciesAndImprovementsData> lstDeficienciesAndImprovements;
            try
            {
                lstDeficienciesAndImprovements = await new DeficienciesAndImprovements().GetDeficienciesAndImprovements(employeeId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstDeficienciesAndImprovements;
        }
        #endregion

        #region CreateDeficienciesAndImprovements
        /// <summary>
        /// Create a new DeficienciesAndImprovements
        /// </summary>
        /// <param name="deficienciesAndImprovementsData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateDeficienciesAndImprovements(DeficienciesAndImprovementsData deficienciesAndImprovementsData)
        {
            try
            {
                return await new DeficienciesAndImprovements().CreateDeficienciesAndImprovements(deficienciesAndImprovementsData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateDeficienciesAndImprovements
        /// <summary>
        /// Update DeficienciesAndImprovements
        /// </summary>
        /// <param name="deficienciesAndImprovementsData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateDeficienciesAndImprovements(DeficienciesAndImprovementsData deficienciesAndImprovementsData)
        {
            try
            {
                return await new DeficienciesAndImprovements().UpdateDeficienciesAndImprovements(deficienciesAndImprovementsData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}
