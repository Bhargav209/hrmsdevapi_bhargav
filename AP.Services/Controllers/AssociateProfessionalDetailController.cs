﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.DomainEntities;
using AP.API;
using AP.Utility;
using AP.Services.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.Services.Controllers
{
    public class AssociateProfessionalDetailController : BaseApiController
    {
        #region GetSkillGroupByCertificate
        /// <summary>
        /// GetSkillGroupByCertificate
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<SkillGroupDetails>> GetSkillGroupsByCertificate()
        {
            List<SkillGroupDetails> lstSkillGroups;
            try
            {
                lstSkillGroups = await new AssociateProfessionalDetails().GetSkillGroupsByCertificate();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstSkillGroups;
        }
        #endregion

        #region AddCertificationDetails
        /// <summary>
        /// Add ceritification details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> AddCertificationDetails(ProfessionalDetails professionalDetails)
        {
            try
            {
                if (professionalDetails == null)
                    throw new ArgumentNullException("professionalDetails cannot be null.");

                return await new AssociateProfessionalDetails().AddCertificationDetails(professionalDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateCertificationDetails
        /// <summary>
        /// Updates ceritification details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateCertificationDetails(ProfessionalDetails professionalDetails)
        {
            try
            {
                if (professionalDetails == null)
                    throw new ArgumentNullException("professionalDetails cannot be null.");

                return await new AssociateProfessionalDetails().UpdateCertificationDetails(professionalDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region DeleteProfessionalDetailsByID
        /// <summary>
        /// DeleteProfessionalDetailsByID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="programType"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> DeleteProfessionalDetailsByID(int id, int programType)
        {
            try
            {
                return await new AssociateProfessionalDetails().DeleteProfessionalDetailsByID(id, programType);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region AddMembershipDetails
        /// <summary>
        /// Add membership details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> AddMembershipDetails(ProfessionalDetails professionalDetails)
        {
            try
            {
                if (professionalDetails == null)
                    throw new ArgumentNullException("professionalDetails cannot be null.");

                return await new AssociateProfessionalDetails().AddMembershipDetails(professionalDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateMembershipDetails
        /// <summary>
        /// Updates membership details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<bool> UpdateMembershipDetails(ProfessionalDetails professionalDetails)
        {
            try
            {
                if (professionalDetails == null)
                    throw new ArgumentNullException("professionalDetails cannot be null.");

                return await new AssociateProfessionalDetails().UpdateMembershipDetails(professionalDetails);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetProfessionalDetailsByEmployeeID
        /// <summary>
        /// GetProfessionalDetailsByEmployeeID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<ProfessionalDetails>> GetProfessionalDetailsByEmployeeID(int employeeID)
        {
            List<ProfessionalDetails> lstProfessionalDetails;
            try
            {

                lstProfessionalDetails = await new AssociateProfessionalDetails().GetProfessionalDetailsByEmployeeID(employeeID);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return lstProfessionalDetails;
        }
        #endregion
    }
}
