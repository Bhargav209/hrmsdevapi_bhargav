﻿using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class TagAssociateController : ApiController
    {
        [HttpGet]
        [Route("api/TagAssociate/GetTagListDetailsByManagerId")]
        public async Task<List<TagAssociateList>> GetTagListDetailsByManagerId(int managerId)
        {
            List<TagAssociateList> tagList;
            try
            {
                tagList = await new TagAssociateListDetails().GetTagListDetailsByManagerId(managerId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return tagList;
        }

        [HttpGet]
        [Route("api/TagAssociate/GetTagListNamesByManagerId")]
        public async Task<List<TagAssociateList>> GetTagListNamesByManagerId(int managerId)
        {
            List<TagAssociateList> tagList;

            try
            {
                tagList = await new TagAssociateListDetails().GetTagListNamesByManagerId(managerId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return tagList;
        }

        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateTagList(List<TagAssociateList> tagListDetails)
        {
            try
            {
                return await new TagAssociateListDetails().CreateTagList(tagListDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateTagAssociateList(List<TagAssociateList> tagListDetails)
        {
            try
            {
                return await new TagAssociateListDetails().UpdateTagAssociateList(tagListDetails);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }

        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> DeleteTagAssociate(int tagAssociateId)
        {
            try
            {
                return await new TagAssociateListDetails().DeleteTagAssociate(tagAssociateId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }


        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> DeleteTagList(string tagListName, int managerId)
        {
            try
            {
                return await new TagAssociateListDetails().DeleteTagList(tagListName, managerId);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
    }
}
