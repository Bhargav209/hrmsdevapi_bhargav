﻿//using AP.DomainEntities;   
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AP.Services.Controllers
{
    public class FinancialYearController : ApiController
    {
        #region GetsFinancialYear
        /// <summary>
        /// Get all FinancialYear
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<FinancialYearData>> GetFinancialYears()
        {
            List<FinancialYearData> lstsFinancialYears;
            try
            {
                lstsFinancialYears = await new FinancialYearDetails().GetFinancialYears();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstsFinancialYears;
        }

        #endregion

        #region GetCurrentFinancialYearByMonth
        /// <summary>
        ///GetCurrentFinancialYearByMonth
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<int> GetCurrentFinancialYearByMonth()
        {
            try
            {

                return await new FinancialYearDetails().GetCurrentFinancialYearByMonth();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region CreateFinancialYear
        /// <summary>
        /// Create FinancialYear
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> CreateFinancialYear(FinancialYearData financialYearData)
        {
            try
            {
                return await new FinancialYearDetails().CreateFinancialYear(financialYearData);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region UpdateFinancialYear
        /// <summary>
        /// Update a notification configuration
        /// </summary>
        /// <param name="financialYearData"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public async Task<int> UpdateFinancialYear(FinancialYearData financialYearData)
        {
            try
            {
                return await new FinancialYearDetails().UpdateFinancialYearStatus(financialYearData);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

    }
}
