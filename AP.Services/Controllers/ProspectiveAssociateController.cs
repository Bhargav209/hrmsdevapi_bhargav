﻿using System;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Resources;
using AP.API;
using AP.Utility;
using AP.DomainEntities;
using AP.Services.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.Services.Controllers
{
    public class ProspectiveAssociateController : BaseApiController
    {
        #region GetEmpTypes
        /// <summary>
        /// GetEmpTypes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetEmpTypes()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().GetEmpTypes());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetPADetails
        /// <summary>
        /// GetPADetails
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetPADetails()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().GetPADetails());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetPAStatus
        /// <summary>
        /// GetPAStatus by id
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetPAStatus(int empID)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().GetPAStatus(empID));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetPADetailsByID
        /// <summary>
        /// GetPADetailsByID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetPADetailsByID(int id)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().GetPADetailsByID(id));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region PagingForPADetails
        /// <summary>
        /// PagingForPADetails
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage PagingForPADetails(int pageNumber, int pageSize)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().PagingForPADetails(pageNumber, pageSize));

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region AddPADetails
        /// <summary>
        /// AddPADetails
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddPADetails(UserDetails userDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().AddPADetails(userDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region UpdatePADetails
        /// <summary>
        /// UpdatePADetails
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdatePADetails(UserDetails userDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().UpdatePADetails(userDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetHRAdvisors
        /// <summary>
        /// GetHRAdvisors
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetHRAdvisors()
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().GetHRAdvisors());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region DeletePADetailsByID
        /// <summary>
        /// DeletePADetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage DeletePADetailsByID(int empID, string reason)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().DeletePADetailsByID(empID, reason));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region PADetailsSearch
        /// <summary>
        /// PADetailsSearch
        /// </summary>
        /// <param name="searchData"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PADetailsSearch(SearchFilter searchFilter)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new ProspectiveAssociateDetails().PADetailsSearch(searchFilter));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetGradeByDesignation
        /// <summary>
        /// Gets grade by designation
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<GradeData> GetGradeByDesignation(int designationId)
        {
            try
            {
                return await new ProspectiveAssociateDetails().GetGradeByDesignation(designationId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
        }
        #endregion

        #region GetJoinedAssociates
        /// <summary>
        /// to get all the joined associates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [UserInfoActionFilter]
        public async Task<List<AssociateJoiningData>> GetJoinedAssociates()
        {
            List<AssociateJoiningData> lstAssociate;
            try
            {
                lstAssociate = await new ProspectiveAssociateDetails().GetJoinedAssociates();
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return lstAssociate;
        }
        #endregion

        #region AssociateProfileApproval
        /// <summary>
        /// Method to approve the employee profile
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public HttpResponseMessage AssociateProfileApproval(int empID, string reqType, string remarks)
        {
            HttpResponseMessage retObject = null;

            try
            {
                ProspectiveAssociateDetails associateDetails = new ProspectiveAssociateDetails();
                ProfileCreationNotificationJob profileCreationNotification = new ProfileCreationNotificationJob();
                string associateCode = new DashboardDetails().GetAssociateCodeByEmpID(empID);
                bool IsChanged = false;
                if (!string.IsNullOrEmpty(associateCode))
                {
                    if (reqType == resourceManager.GetString("Approve"))
                    {

                        // Delete PA details from PA display
                        //
                        retObject = Request.CreateResponse(associateDetails.DeletePADetails(empID));

                        //Update Associate as approved and show this employee record in the Associate tab
                        //
                        IsChanged=new AssociatePersonalDetails().UpdateAssociateStatus(empID, Convert.ToInt32(Enumeration.EPCStatus.Approved));                        

                        bool isAllocatedToPool = new Allocation().TalentPoolAllocation(empID);                       
                                               
                    }
                    else
                    {
                        retObject = Request.CreateResponse(new AssociatePersonalDetails().UpdateAssociateStatus(empID, Convert.ToInt32(Enumeration.EPCStatus.Rejected),remarks));                        
                    }
                    if (retObject!=null)
                        retObject.StatusCode = HttpStatusCode.OK;
                    
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return retObject;
        }
        #endregion
    }
}
