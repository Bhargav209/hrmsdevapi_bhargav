﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AP.API;
using AP.DomainEntities;
using AP.Services.Filters;
using System.Threading.Tasks;

namespace AP.Services.Controllers
{
    public class PracticeAreaController : ApiController
    {
        #region GetPracticeAreas
        /// <summary>
        /// To get the Practice area list
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetPracticeAreas()
        {
            HttpResponseMessage returnObject = new HttpResponseMessage();
            try
            {                
                returnObject = Request.CreateResponse(new PracticeAreaMaster().GetPracticeAreas());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }
            return returnObject;
        }
        #endregion

        #region AddPracticeArea
        /// <summary>
        /// to get practice area for the skill tab
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage AddPracticeArea(PracticeAreaDetails practiceAreaDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new PracticeAreaMaster().AddPracticeArea(practiceAreaDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion

        #region GetPracticeArea
        /// <summary>
        /// to get practice area for the skill tab
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [UserInfoActionFilter]
        public HttpResponseMessage UpdatePracticeArea(PracticeAreaDetails practiceAreaDetails)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                httpResponseMessage = Request.CreateResponse(new PracticeAreaMaster().UpdatePracticeArea(practiceAreaDetails));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Warning"
                });
            }

            return httpResponseMessage;
        }
        #endregion
    }
}
