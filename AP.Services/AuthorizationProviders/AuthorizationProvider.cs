﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Text;
using AP.API;
using AP.DomainEntities;
using System.Configuration;

namespace AP.Services.AuthorizationProviders
{
    public class AuthorizationProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            //This method is for Client Validation like client ID or anything whatever we want
            context.Validated();
            return Task.FromResult<object>(null);
        }

        //This is for checking the credential and assigning the role for user
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });


            //Check credential while login
            //Check for EnvironmentType in Web.config file. If value is other than PROD, then skip Office365Login.
            bool loginSuccess = false;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnvironmentType"]))
            {
                if (ConfigurationManager.AppSettings["EnvironmentType"].ToLower() == "PROD".ToLower())
                {
               //Office365Login office365Login = new Office365Login();
               //loginSuccess = office365Login.LoginUser(context.UserName, context.Password);
                  if (context.UserName.Length > 1)
                     loginSuccess = true;
                }
                else
                {
                    if (ConfigurationManager.AppSettings["EnvironmentType"].ToLower() == "DEV".ToLower()
                        || ConfigurationManager.AppSettings["EnvironmentType"].ToLower() == "QA".ToLower())
                        loginSuccess = true;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            StringBuilder strbldRoles = new StringBuilder();
            int employeeId=0;
            if (!loginSuccess)
            {
                context.SetError("invalid_grant", "Wrong username or password.");
                return Task.FromResult<object>(null);
            }
            //
            if (loginSuccess)
            {
                UserRoles userRoles = new UserRoles();
                UserDetails emp = userRoles.GetEmployeeOnUserName(context.UserName);
                if (emp == null || emp.empID == 0)
                {
                    context.SetError("invalid_grant", "Employee doesn't exist.");
                    return Task.FromResult<object>(null);
                }
                //
                List<LoginUserRole> loginUserRoles = userRoles.GetUserRoleOnLgin(context.UserName);
                if (loginUserRoles == null || loginUserRoles.Count == 0)
                {
                    context.SetError("invalid_grant", "No Roles have been assigned to this user.");
                    return Task.FromResult<object>(null);
                }
                //
                foreach (LoginUserRole userRole in loginUserRoles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, userRole.role));
                    strbldRoles.Append(userRole.role);
                    strbldRoles.Append(",");
                }
                employeeId = emp.empID;
                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));

            }
            if (!loginSuccess)
            {
                return Task.FromResult<object>(null);
            }


            string roleStr = strbldRoles.ToString();
            string trimmedRoles = roleStr.TrimEnd(',');


            var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                        "userName", context.UserName
                    },
                    {
                        "role", trimmedRoles
                    },
                    {
                        "employeeId", employeeId.ToString()
                    }
                });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
            return Task.FromResult<object>(null);

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}