﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AP.Services.Handlers
{
    public class APServiceHandler: DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string key = HttpUtility.ParseQueryString(request.RequestUri.Query).Get("key");
            string errorMessage = "You need to specify the api key to access the Web API.";

            try
            {

                if (!string.IsNullOrWhiteSpace(key))
                {
                    return base.SendAsync(request, cancellationToken);
                }

                else
                {
                    HttpResponseMessage response = request.CreateErrorResponse(HttpStatusCode.Forbidden, errorMessage);
                    throw new HttpResponseException(response);
                }
            }

            catch
            {
                HttpResponseMessage response = request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An unexpected error occured...");
                throw new HttpResponseException(response);
            }
        }
    }
}