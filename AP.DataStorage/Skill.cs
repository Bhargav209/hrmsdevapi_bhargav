//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AP.DataStorage
{
    using System;
    using System.Collections.Generic;
    
    public partial class Skill
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Skill()
        {
            this.AssociateFeedbacks = new HashSet<AssociateFeedback>();
            this.AssociatesCertifications = new HashSet<AssociatesCertification>();
            this.CompetencySkills = new HashSet<CompetencySkill>();
            this.RequisitionRoleSkills = new HashSet<RequisitionRoleSkill>();
            this.EmployeeSkills = new HashSet<EmployeeSkill>();
        }
    
        public int SkillId { get; set; }
        public string SkillCode { get; set; }
        public string SkillName { get; set; }
        public string SkillDescription { get; set; }
        public Nullable<int> CompetencyAreaId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedUser { get; set; }
        public string ModifiedUser { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string SystemInfo { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public Nullable<int> SkillGroupId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AssociateFeedback> AssociateFeedbacks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AssociatesCertification> AssociatesCertifications { get; set; }
        public virtual CompetencyArea CompetencyArea { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompetencySkill> CompetencySkills { get; set; }
        public virtual SkillGroup SkillGroup { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RequisitionRoleSkill> RequisitionRoleSkills { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeSkill> EmployeeSkills { get; set; }
    }
}
