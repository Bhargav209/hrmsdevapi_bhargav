//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AP.DataStorage
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProjectRoleDetail
    {
        public int RoleAssignmentId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> RoleMasterId { get; set; }
        public Nullable<bool> IsPrimaryRole { get; set; }
        public Nullable<int> StatusId { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string SystemInfo { get; set; }
        public string RejectReason { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual Status Status { get; set; }
        public virtual RoleMaster RoleMaster { get; set; }
    }
}
