﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
   public class KRAMeasurementTypeData
    {
        public int KRAMeasurementTypeId { get; set; }

        public string KRAMeasurementType { get; set; }
    }
}
