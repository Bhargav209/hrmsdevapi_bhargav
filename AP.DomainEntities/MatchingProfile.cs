﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class MatchingProfile
    {
        public int? EmployeeId { get; set; }
        public string AssociateName { get; set; }
        public decimal? Availability { get; set; }
        public int? CompetencyAreaId { get; set; }
        public List<EmployeeSkillDetails> skills { get; set; }
        public bool IsFullyMatched { get; set; } = false;
    }
}
