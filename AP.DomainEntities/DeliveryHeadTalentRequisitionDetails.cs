﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class DeliveryHeadTalentRequisitionDetails : BaseEntity
    {
        public string TRCode { get; set; }
        public string ProjectName { get; set; }
        public int RequisitionType { get; set; }
        public string RequestedBy { get; set; }
        public DateTime RequestedDate { get; set; }
        public string DepartmentCode { get; set; }
        public string RoleName { get; set; }
        public int? NoOfBillablePositions { get; set; }
        public int? NoOfNonBillablePositions { get; set; }
        public int? TotalPositions { get; set; }
        public int? TaggedEmployees { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public bool IsBillable { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsCritical { get; set; }
        public string Authorized { get; set; }
        public int TalentRequisitionId { get; set; }
        public int FromEmployeeID { get; set; }
        public List<GenericType> ApprovedByID { get; set; }
        public int StatusId { get; set; }
        public string Comments { get; set; }
        public int EmployeeId { get; set; }
        public int RoleId { get; set; }
        public string FromEmployeeName { get; set; }
        public string ToEmployeeName { get; set; }
        public string StatusCode { get; set; }
    }
}
