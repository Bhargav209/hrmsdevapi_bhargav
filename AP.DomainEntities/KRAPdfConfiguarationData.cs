﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRAPdfConfiguarationData:BaseEntity
    {
        public int KRApdfconfigurationID { get; set; }
        public string Section1 { get; set; }
        public string Section2 { get; set; }
    }
}
