﻿using System;

namespace AP.DomainEntities
{
   public class ProjectBillabilityData : BaseEntity
    {
        public int ProjectId { get; set; }
        public int? NoOfResources { get; set; }
        public string EffectiveDate { get; set; }
        public string Remarks { get; set; }
        public decimal? BillablePercentage { get; set; }
        public int Id { get; set; }
    }
}
