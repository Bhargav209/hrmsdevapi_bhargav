﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TRStatusesData : BaseEntity
    {
        public int TalentRequisitionId { get; set; }
        public int StatusId { get; set; }
        public int StatusFromUserId { get; set; }
        public int StatusToUserId { get; set; }        
    }
}
