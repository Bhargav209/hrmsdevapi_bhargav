﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TransitionPlanDetailsData : BaseEntity
    {
        public int ID{ get; set; }
        public int TransitionId { get; set; }
        public int CategoryId { get; set; }
        public int TaskId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StatusId { get; set; }
        public string Remarks { get; set; }
        public string ValueName { get; set; }
        public int ValueKey { get; set; }
        public string TaskName { get; set; }
        public string CategoryName { get; set; }
    }
}
