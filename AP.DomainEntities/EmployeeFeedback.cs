﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class EmployeeFeedback : BaseEntity
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int? LeadId { get; set; }
        public int? ManagerId { get; set; }
        public string ManagerLastName { get; set; }
        public string ManagerFirstName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ManagerName { get; set; }
        public string LeadName { get; set; }
        public string LeadFirstName { get; set; }
        public string LeadLastName { get; set; }
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpFeedback { get; set; }
        public int CompetencyAreaId { get; set; }
        public string CompetencyArea { get; set; }
        public int SkillId { get; set; }
        public string Skill { get; set; }
        public int ProficiencyLevelId { get; set; }
        public string ProficiencyLevel { get; set; }
        public string AssociateContribution { get; set; }
        public string ManagerFeedback { get; set; }
        public bool SkillApplied { get; set; }
        public string Status { get; set; }
        public string FeedbackStatus { get; set; }
       
    }
}
