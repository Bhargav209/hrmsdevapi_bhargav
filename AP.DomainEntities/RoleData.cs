﻿using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class RoleData : BaseEntity
    {
        public int? RoleId { get; set; }
        public int RoleMasterId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public string KeyResponsibilities { get; set; }
        public string EducationQualification { get; set; }
        public int DepartmentId { get; set; }
        public IEnumerable<RoleCompetencySkills> RoleCompetencySkills { get; set; }
        public List<int> DeltedSkillIds { get; set; }
        public int SGRoleID { get; set; }
        public int? PrefixID { get; set; }
        public int? SuffixID { get; set; }
        public string DepartmentCode { get; set; }
        public int? KRAGroupID { get; set; }
        public string KRATitle { get; set; }
    }
}
