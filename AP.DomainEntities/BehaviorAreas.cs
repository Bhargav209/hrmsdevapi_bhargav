﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class BehaviorAreas : BaseEntity
    {
        public int BehaviorAreaId { get; set; }
        public string BehaviorArea { get; set; }
    }
}
