﻿using System;

namespace AP.DomainEntities
{
   public class AssociateUsage : BaseEntity
   {
      public int Id { get; set; }
      public int ProjectId { get; set; }
      public int EmployeeId { get; set; }
      public Nullable<int> RoleMasterId { get; set; }
      public int PercentUtilization { get; set; }
      public Nullable<bool> Billable { get; set; }
      public Nullable<bool> Critical { get; set; }
      public int ManagerId { get; set; }
      public Nullable<bool> PrimaryManager { get; set; }
      public Nullable<System.DateTime> ActualStartDate { get; set; }
      public Nullable<System.DateTime> ActualEndDate { get; set; }
   }

   public class AssociateReport : BaseEntity
   {
      public int AllocationId { get; set; }
      public int AssociateId { get; set; }
      public string AssociateName { get; set; }
      public decimal? Experience { get; set; }
      public string ExpRange { get; set; }
      public string Designation { get; set; }
      public string Grade { get; set; }
      public string PrimarySkill { get; set; }
      public string CompetencyArea { get; set; }
      public string Client { get; set; }
      public string ProjectName { get; set; }
      public int? Utilization { get; set; }
      public Boolean Billable { get; set; }
      public Boolean Critical { get; set; }
      public string IsBillable { get; set; }
      public string IsCritical { get; set; }
      public DateTime? JoinDate { get; set; }
      public string DateofJoining { get; set; }
      public string ProgramManager { get; set; }
      public string TechnicalLead { get; set; }
      public string ReportingManager { get; set; }
      public string ProjectGroup { get; set; }
      public string TechnicalCompetency { get; set; }
      public string AssociateCode { get; set; }
   }
}
