﻿using System;

namespace AP.DomainEntities
{
    public class SkillData : BaseEntity
    {
        public int SkillId { get; set; }
        public string SkillCode { get; set; }
        public string SkillName { get; set; }
        public string RoleName { get; set; }
        public string SkillDescription { get; set; }
        public int? SkillGroupID { get; set; }
        public int? CompetencyAreaID { get; set; }
        public string Name { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public string SkillGroupName { get; set; }
    }

    public class SkillsInformation
    {
        public string RequestedSkills { get; set; }
        public string MatchedSkills { get; set; }
        public string UnMatchedSkills { get; set; }
    }
}
