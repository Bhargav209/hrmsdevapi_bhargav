﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRAGroupData : BaseEntity
    {
        public int KRAGroupId { get; set; }
        public int DepartmentId { get; set; }
        public int RoleCategoryId { get; set; }
        public int? ProjectTypeId { get; set; }
        public int FinancialYearId { get; set; }
        public string KRATitle { get; set; }
        public int StatusId { get; set; }
        public string DepartmentName { get; set; }
        public string RoleCategoryName { get; set; }
        public string ProjectTypeCode { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public int KRACount { get; set; }
    }
}
