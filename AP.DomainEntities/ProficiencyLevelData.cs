﻿using System;

namespace AP.DomainEntities
{
    public class ProficiencyLevelData : BaseEntity
    {
        public int ProficiencyLevelId { get; set; }
        public string ProficiencyLevelCode { get; set; }
        public string ProficiencyLevelDescription { get; set; }
    }
}
