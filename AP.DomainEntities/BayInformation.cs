﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class BayInformation:BaseEntity
    {
        public int BayId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
