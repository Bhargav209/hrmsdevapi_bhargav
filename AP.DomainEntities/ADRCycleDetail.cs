﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ADRCycleDetail : BaseEntity
    {
        public int ADRCycleID { get; set; }
        public string ADRCycle { get; set; }
        public int FinancialYearId { get; set; }
    }
}
