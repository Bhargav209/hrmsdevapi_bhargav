﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AssociateResignationData : BaseEntity
    {
        public int ResignationId { get; set; }
        public int EmployeeId { get; set; }
        public int? ReportingManagerID { get; set; }
        public int? ProgramManagerID { get; set; }
        public int ReasonId { get; set; }
        public string Reason { get; set; }
        public string ReasonDescription { get; set; }
        public int StatusId { get; set; }
        public DateTime DOR { get; set; }
        public DateTime? DOJ { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ProgramManagerName { get; set; }
        public string ReportingManagerName { get; set; }
        public string LeadName { get; set; }
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProjectName { get; set; }
        public DateTime? NoticePeriod { get; set; }
        public DateTime ResignationDate { get; set; }
        public string Status { get; set; }
        public bool? IsBillable { get; set; }
        public bool? IsCritical { get; set; }
        public bool? IsPrimary { get; set; }
        public string Designation { get; set; }
        public string EmployeeName { get; set; }

        public List<ProjectData> Project { get; set; }

    }
}
