﻿namespace AP.DomainEntities
{
    public class CategoryDetails : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int CategoryID { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string CategoryName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string CreatedUser { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Active { get; set; }
    }
}
