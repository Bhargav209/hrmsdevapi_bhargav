﻿using System;

namespace AP.DomainEntities
{
    public class ProjectRoleData : BaseEntity
    {
        public int RoleAssignmentId { get; set; }
        public int? EmployeeId { get; set; }
        public int? ProjectId { get; set; }
        public int? RoleMasterId { get; set; }
        public bool? IsPrimaryRole { get; set; }
        //public string FromDate { get; set; }
        //public string ToDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? StatusId { get; set; }
        public string AssociateName { get; set; }
        public string RoleName { get; set; }
        public string RejectReason { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string KeyResponsibilities { get; set; }
        public string ProjectName { get; set; }
        
    }
}
