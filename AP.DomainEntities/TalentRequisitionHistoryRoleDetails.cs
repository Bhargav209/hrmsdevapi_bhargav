﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TalentRequisitionHistoryRoleDetails
    {
        public int RequisitionRoleDetailID { get; set; }
        public string RoleName { get; set; }
        public int? NoOfBillablePositions { get; set; }
        public int? NoOfNonBillablePositions { get; set; }
        public int? TAPositions { get; set; }
    }
}
