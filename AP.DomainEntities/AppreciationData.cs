﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AppreciationData:BaseEntity
    {
        public int ADRCycleID { get; set; }
        public string ADRCycle { get; set; }
        public int FinancialYearID { get; set; }
        public string FinancialYear { get; set; }
        public List<GenericType> AssociateNames { get; set; }
        public int ToEmployeeID { get; set; }
        public string ToEmployeeName { get; set; }
        public int AppreciationTypeID { get; set; }
        public string AppreciationType { get; set; }
        public DateTime AppreciationDate { get; set; }        
        public string AppreciationMessage { get; set; }
        public int FromEmployeeID { get; set; }
        public string FromEmployeeName { get; set; }
        public string DeleteReason { get; set; }
        public int ID { get; set; }
    }

}
