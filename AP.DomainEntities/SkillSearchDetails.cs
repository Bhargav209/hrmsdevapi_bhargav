﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TalentRequisitionCommon
    {
        public int EmployeeID { get; set; }
    }

    public class TalentRequisitionEmployeeDetails : TalentRequisitionCommon
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public decimal Experience { get; set; }
        public string DesignationCode { get; set; }
        public string RoleDescription { get; set; }
        public string ProjectName { get; set; }
        public int RoleMasterId { get; set; }
    }

    public class TalentRequisitionProjectDetails : TalentRequisitionCommon
    {
        public string ProjectName { get; set; }
        public bool IsBillable { get; set; }
        public bool IsCritical { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class TalentRequisitionSkillDetails : TalentRequisitionCommon
    {
        public string CompetencyAreaCode { get; set; }
        public string SkillGroupName { get; set; }
        public string SkillName { get; set; }
        public string ProficiencyLevelCode { get; set; }
    }

    public class SkillSearchDetails : BaseEntity
    {
        public List<TalentRequisitionEmployeeDetails> talentRequisitionEmployeeDetails { get; set; }
        public List<TalentRequisitionProjectDetails> talentRequisitionProjectDetails { get; set; }
        public List<TalentRequisitionSkillDetails> talentRequisitionSkillDetails { get; set; }
    }
}
