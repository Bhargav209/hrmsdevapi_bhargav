﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{

    public class AssociateProjectData : BaseEntity
    {
        public int? AssociateAllocationID { get; set; }
        public int EmployeeId { get; set; }
        public int? ProjectId { get; set; }
        public string RoleName { get; set; }
        public string Project { get; set; }       
        public string AssociateName { get; set; }
        public decimal? AllocationPercentage { get; set; }        
    }

    public class KRAEmployeeList
    {
        public int? AssociateAllocationID { get; set; }
        public int EmployeeID { get; set; }
    }
}
