﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    class KRATransactionData
    {
        public int KRATransactionId { get; set; }
        public int? VersionNo { get; set; }
        public int? KRAId { get; set; }
        public string KRACode { get; set; }
        public int? RoleId { get; set; }
        public int? GradeId { get; set; }
        public int? DepartmentId { get; set; }
        public string Metric { get; set; }
        public string Measure { get; set; }
        public string Traget { get; set; }
        public string Year { get; set; }
        public int? StatusId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ReviewedBy { get; set; }
        public int? ApprovedBy { get; set; }
    }
}
