﻿using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class ProjectTrainingMasterData:BaseEntity
    {
        public int ProjectTrainingId { get; set; }
        public string ProjectTrainingCode { get; set; }
        public string ProjectTrainingName { get; set; }   
        public IEnumerable<int?> Projects { get; set; }
        public int ProjectId { get; set; }
        public string ProjectTrainingType { get; set; }
    }
}
