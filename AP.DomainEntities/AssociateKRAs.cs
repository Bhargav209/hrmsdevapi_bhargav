﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AssociateKRAs
    {
        public List<OrganizationKRAs> OrganizationKRAs { get; set; }
        public List<CustomKRAs> CustomKRAs { get; set; }
    }
    public class OrganizationKRAs
    {
        public string KRATitle { get; set; }        
        public string KRAAspectName { get; set; }
        public string KRAAspectMetric { get; set; }
        public string Operator { get; set; }
        public string KRAMeasurementType { get; set; }
        public string ScaleLevel { get; set; }
        public Decimal KRATargetValue { get; set; }
        public string KRATargetText { get; set; }
        public string KRATargetPeriod { get; set; }
    }
    public class CustomKRAs
    {
        public string KRAAspectName { get; set; }
        public string KRAAspectMetric { get; set; }
        public string KRAAspectTarget { get; set; }
        public int KRAAspectID { get; set; }
        public int CustomKRAID { get; set; }
        public int KRATypeID { get; set; }
        public int FinancialYearID { get; set; }
        public List<int> EmployeeIDs { get; set; }
    }
}
