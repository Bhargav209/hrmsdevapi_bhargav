﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class SGEntity
    {
        public List<GenericType> Prefix { get; set; }
        public List<GenericType> Suffix { get; set; }
        public List<GenericType> Roles { get; set; }
    }
}
