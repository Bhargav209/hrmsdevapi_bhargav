﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ErrorLogDetails : BaseEntity
    {
        public string FileName { get; set; }
        public string ErrorMessage { get; set; }
    }
}
