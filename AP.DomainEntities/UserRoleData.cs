﻿using System;

namespace AP.DomainEntities
{
    public class UserRoleData:BaseEntity
    {
        public int UserRolesID { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<bool> IsPrimary { get; set; }
    }
}
