﻿using System;

namespace AP.DomainEntities
{
    public class RequisitionWorkPlaceBehavior: BaseEntity
    {
        public int RequisitionBehaviorAreaId { get; set; }
        public int TalentRequisitionId { get; set; }
        public int BehaviorAreaId { get; set; }
        public string BehaviorArea { get; set; }
        public string BehaviorCharacteristics { get; set; }
        public int? RoleMasterId { get; set; }
        public Nullable<System.Int32> BehaviorRatingId { get; set; }

    }
}
