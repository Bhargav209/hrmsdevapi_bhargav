﻿using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class TalentRequisitionHistoryData : BaseEntity
    {
        public int TalentRequisitionId { get; set; }        
        public string TRCode { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int RoleMasterID { get; set; }
        public string RoleName { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string ProjectType { get; set; }    
        public string Status { get; set; }
        public int StatusId { get; set; }
        public bool TRSelect { get; set; }
        public int? NoOfPositions { get; set; }
        public string Remarks { get; set; }
        
        public string TRStatusUpdatedBy { get; set; }
        public string CreatedUser { get; set; }
        public IEnumerable<TalentRequisitionHistoryRoleDetails> TalentRequisitionHistoryRoleDetails { get; set; }
        public string RaisedUser { get; set; }
        public string DraftedUser { get; set; }
        public string ApprovedUser { get; set; }
        public string RequisitionName { get; set; }
        public int RequisitionRoleDetailID { get; set; }
        public string ClientName { get; set; }
        public int DepartmentId { get; set; }
    }
}
