﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class JDTemplateDetailsData : BaseEntity
    {
        public int TemplateTitleId { get; set; }
        public List<JDTemplateMasterData> JDTemplateSkills { get; set; }

    }

    public class JDTemplateMasterData 
    {        
        public int CompetencyAreaId { get; set; }
        public int SkillGroupId { get; set; }
        public int SkillId { get; set; }
        public string SkillGroupName { get; set; }
        public string SkillName { get; set; }
        public int ProficiencyLevelId { get; set; }
        public string CompetencyAreaCode { get; set; }
        public string ProficiencyLevelCode { get; set; }
    }
}
