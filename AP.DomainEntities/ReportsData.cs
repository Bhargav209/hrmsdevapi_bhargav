﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AllocationDetails
    {
        public List<ResourceAllocationDetails> lstBillableResources { get; set; }
        public List<ResourceAllocationDetails> lstNonBillableResources { get; set; }
        public AllocationCount AllocationCount = new AllocationCount();
    }

    public class AllocationCount
    {
        public string ProjectName { get; set; }
        public int ResourceCount { get; set; }
        public int BillableCount { get; set; }
        public int NonBillableCount { get; set; }
    }

    public class ResourceAllocationDetails
    {
        public string AssociateCode { get; set; }
        public string AssociateName { get; set; }
        public decimal AllocationPercentage { get; set; }
        public string InternalBillingRoleName { get; set; }
        public string ClientBillingRoleName { get; set; }
        public string IsPrimaryProject { get; set; }
        public string IsCriticalResource { get; set; }
    }

    public class SkillSearchFilter
    {
        public int CompetencyAreaID { get; set; }
        public int SkillGroupID { get; set; }
        public int SkillID { get; set; }
    }

    public class AssociateSkillData
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string CompetencyArea { get; set; }
        public string SkillGroup { get; set; }
        public string Skill { get; set; }
        public int LastUsed { get; set; }
        public bool IsPrimary { get; set; }
        public string ProficiencyLevel { get; set; }
    }

    public class DomainDataCount
    {
        public int ResourceCount { get; set; }
        public int DomainID { get; set; }
        public string DomainName { get; set; }
    }

    public class TalentPoolDataCount
    {
        public int ResourceCount { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
    }

    public class ReportsFilterData
    {
        public UtilizationReportFilterData utilizationReportFilterData = new UtilizationReportFilterData();
        public FinanceReportFilterData financeReportFilterData = new FinanceReportFilterData();
        public UtilizationReportByMonthFilterData utilizationReportByMonthFilterData = new UtilizationReportByMonthFilterData();
        public List<ReportsData> reportsData { get; set; }
        public int TotalCount { get; set; }
    }

    public class UtilizationReportFilterData : PagingData
    {
        public int EmployeeId { get; set; }
        public int ProjectId { get; set; }
        public int GradeId { get; set; }
        public int DesignationId { get; set; }
        public int ClientId { get; set; }
        public int AllocationPercentageId { get; set; }
        public int ProgramManagerId { get; set; }
        public string ExperienceRange { get; set; }
        public int MinExperience { get; set; }
        public int MaxExperience { get; set; }
        public int IsBillable { get; set; }
        public int IsCritical { get; set; }
        public bool isExportToExcel { get; set; }
        public int PracticeAreaId { get; set; }
    }
    public class Months 
    {
        public decimal EmployeeId { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string January { get; set; }
        public string February { get; set; }
        public string March { get; set; }
        public string April { get; set; }
        public string May { get; set; }
        public string June { get; set; }
        public string July { get; set; }
        public string August { get; set; }
        public string September { get; set; }
        public string October { get; set; }
        public string November { get; set; }
        public string December { get; set; }
        public decimal AverageUtilizationpercentage { get; set; }
    }
    public class UtilizationReportByMonthFilterData : PagingData
    {
        public int FromMonth { get; set; }
        public int ToMonth { get; set; }
        public int Year { get; set; }
    }

    public class FinanceReportFilterData : PagingData
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ProjectId { get; set; }
    }

    public class ReportsData : Months
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string ProgramManagerName { get; set; }
        public string ReportingManagerName { get; set; }
        public string LeadName { get; set; }
        public string RoleName { get; set; }
        public string SkillCode { get; set; }
        public string PrimarySkill { get; set; }
        public string SecondarySkill { get; set; }
        public string DesignationName { get; set; }
        public string Description { get; set; }
        public decimal Experience { get; set; }
        public DateTime JoinDate { get; set; }
        public string GradeName { get; set; }
        public string ClientName { get; set; }

        public string ClientCode { get; set; }
        public string ProjectName { get; set; }
        public bool IsBillable { get; set; }
        public bool IsCritical { get; set; }
        public decimal ClientBillingPercentage { get; set; }
        public decimal Allocationpercentage { get; set; }
        public decimal InternalBillingPercentage { get; set; }
        public string InternalBillingRoleCode { get; set; }
        public string ClientBillingRoleCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Technology { get; set; }
        public string AadharNumber { get; set; }
        public string EmployeeType { get; set; }
        public decimal ExperienceExcludingCareerBreak { get; set; }
        public int CareerBreak { get; set; }
        public string DepartmentName { get; set; }
    }

    public class DomainData
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string Grade { get; set; }
        public decimal Experience { get; set; }
    }

    public class TalentPoolReportData
    {
        public int EmployeeId { get; set; }
        public string EmployeeCode { get; set; }
        public string ProjectName { get; set; }
        public string ClientName { get; set; }
        public string RoleName { get; set; }
        public string Technology { get; set; }
        public string Billable { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string Grade { get; set; }       
        public decimal Experience { get; set; }
    }
}

