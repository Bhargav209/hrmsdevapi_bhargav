﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class RoleStatusData : BaseEntity
    {
        public int RoleStatusId { get; set; }
        public int RoleId { get; set; }
        public int StatusId { get; set; }
        public int CategoryId { get; set; }
    }
}
