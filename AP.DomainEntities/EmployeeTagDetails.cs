﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class EmployeeTagDetails
    {
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public int RoleMasterID { get; set; }
        public string EmployeeName { get; set; }
        public string RoleName { get; set; }
        public int StatusId { get; set; }
        

    }
    public class SubmitRequisitionForApproval : BaseEntity
    {
        public List<EmployeeTagDetails> employeeTagDetails { get; set; }
        public int TalentRequisitionID { get; set; }
        public List<int> ApprovedByID { get; set; }
        public int SubmittedByID { get; set; }
    }
}
