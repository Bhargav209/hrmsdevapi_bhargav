﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class LearningAptitudeData :  BaseEntity
    {
        public int LearningAptitudeId { get; set; }
        public string LearningAptitude { get; set; }
    }
}
