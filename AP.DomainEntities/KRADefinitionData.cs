﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRADefinitionData : BaseEntity
    {
      
        public int KRAGroupId { get; set; }
        public int KRAAspectId { get; set; }
        public string KRAAspectName { get; set; }
        public int StatusId { get; set; }
        public int FinancialYearId { get; set; }
        public string FinancialYear { get; set; }
        public string RoleName { get; set; }
        public List<MetricAndTarget> lstMetricAndTarget { get; set; }
    }

    public class MetricAndTarget
    {
        public int KRADefinitionId { get; set; }
        public int KRAAspectId { get; set; }
        public string Metric { get; set; }
        public int KRAMeasurementTypeId { get; set; }
        public int KRAOperatorId { get; set; }
        public int? KRAScaleMasterId { get; set; }
        public decimal? KRATargetValue { get; set; }
        public string KRATargetText { get; set; }
        public int KRATargetPeriodId { get; set; }
        public string Operator { get; set; }
        public string KRAMeasurementType { get; set; }
        public string ScaleLevel { get; set; }
        public string KRATargetPeriod { get; set; }
        public string TargetDescription { get; set; }
        public string KRATargetValueAsDate { get; set; }
    }

}
