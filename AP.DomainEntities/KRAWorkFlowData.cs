﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRAWorkFlowData : BaseEntity
    {
        public int FinancialYearID { get; set; }
        public int? StatusID { get; set; }
        public int FromEmployeeID { get; set; }
        public int? ToEmployeeID { get; set; }
        public int DepartmentID { get; set; }
        public List<GenericType> KRAGroupIDs { get; set; }
        public string Comments { get; set; }
    }

    public class KRAComments
    {
        public string EmployeeName { get; set; }
        public string Comments { get; set; }
        public DateTime CommentedDate { get; set; }
    }
}
