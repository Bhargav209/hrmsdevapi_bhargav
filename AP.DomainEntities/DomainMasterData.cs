﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class DomainMasterData : BaseEntity
    {
        public int DomainID { get; set; }
        public string DomainName { get; set; }       
    }
}
