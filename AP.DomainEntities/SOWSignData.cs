﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class SOWSignData : BaseEntity
    {
        public string SOWId { get; set; }
        public string SOWFileName { get; set; }
        public int ProjectId { get; set; }        
        public DateTime? SOWSignedDate { get; set; }       
        public Boolean SOW { get; set; }
        public Boolean Addendum { get; set; }        
        public string AddendumNo { get; set; }
        public int Id { get; set; }
        public int AddendumId { get; set; }
        public string RecipientName { get; set; }
        public DateTime? AddendumDate { get; set; }
        public string Note { get; set; }
        public string RoleName { get; set; }
        public string SignedDate { get; set; }
    }
}
