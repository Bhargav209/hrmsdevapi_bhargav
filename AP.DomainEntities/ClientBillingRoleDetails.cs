﻿using System;

namespace AP.DomainEntities
{
    public class ClientBillingRoleDetails : BaseEntity
    {
        public int ClientBillingRoleId { get; set; }
        public string ClientBillingRoleName { get; set; }
        public int ProjectId { get; set; }
        public int NoOfPositions { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ProjectName { get; set; }
        public int? ClientBillingPercentage { get; set; }
        public decimal? Percentage { get; set; }        
        public int AllocationCount { get; set; }
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
    }
}
