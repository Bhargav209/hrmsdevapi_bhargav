﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AssociateKRAData : BaseEntity
    {
        public int AssociateKRAId { get; set; }
        public int? KRAId { get; set; }
        public int? ProjectId { get; set; }
        public int RoleId { get; set; }
        public string EmployeeCode { get; set; }
        public string KRAAspect { get; set; }
        public string Metric { get; set; }
        public string Target { get; set; }
        public string Year { get; set; }
        public String RoleName { get; set; }
        public string EffectiveDate { get; set; }
        public string EffectiveTime { get; set; }        
    }
}
