﻿using System;

namespace AP.DomainEntities
{
    public class ClientDetails : BaseEntity
    {
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        //public string ClientShortName { get; set; }
        public string ClientRegisterName { get; set; }
    }
}
