﻿using System;

namespace AP.DomainEntities
{
    public class DepartmentData : BaseEntity
    {
        public int DepartmentId { get; set; }
        public string Description { get; set; }
        public string DepartmentCode { get; set; }
        public int ? DepartmentHeadId { get; set; }
        public int DepartmentTypeId { get; set; }
        public int ID { get; set; }
    }
}
