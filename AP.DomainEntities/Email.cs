﻿namespace AP.DomainEntities
{
    public class Email: BaseEntity
    {
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string CcEmail { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
    }

    public class EmailList
    {
        public string EmailId { get; set; }
        public string Username { get; set; }
    }
}
