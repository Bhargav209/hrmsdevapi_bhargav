﻿using System;

namespace AP.DomainEntities
{
    public class GradeData : BaseEntity
    {
        public int GradeId { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
    }
}
