﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class CommonProficiency : BaseEntity
    {
        public int ID { get; set; }
        public string validFrom { get; set; }
        public string institution { get; set; }
        public string specialization { get; set; }
        public string validUpto { get; set; }
        public int EmployeeID { get; set; }
        public int programTypeID { get; set; }
        public int programType { get; set; }
    }
}

