﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRASubmittedGroup
    {
        public List<int> KRAGroupIds { get; set; }
        public int FinancialYearId { get; set; }
        public string RoleName { get; set; }
        public int fromEmployeeId { get; set; }
        public string KRATitle { get; set; }
        public string Description { get; set; }
        public string FinancialYear { get; set; }
        public int StatusId { get; set; }
        public string ToEmployeeName { get; set; }
        public int DepartmentHeadId { get; set; }
        public string EmailAddress { get; set; }
    }
}
