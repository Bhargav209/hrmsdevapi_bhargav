﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TagAssociateList : BaseEntity
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int ManagerId { get; set; }
        public decimal Allocation { get; set; }
        public string Designation { get; set; }
        public string EmployeeName { get; set; }
        public string TagListId { get; set; }
        public string TagListName { get; set; }

    }
}
