﻿
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class ApproveRequisitionData : BaseEntity
    {
        public string Type { get; set; }
        public string Remarks { get; set; }
        public List<int> TalentRequisitionIds { get; set; }
    }
}
