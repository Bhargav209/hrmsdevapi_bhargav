﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRASetData : BaseEntity
    {
        public int ID { get; set; }
        public int RoleMasterID { get; set; }
        public int KRAAspectID { get; set; }
        public int FinancialYearID { get; set; }
        public string KRAAspectName { get; set; }
        public string FinancialYear { get; set; }
        public string KRAAspectMetric { get; set; }
        public string KRAAspectTarget { get; set; }
        public int? StatusID { get; set; }
        public string StatusCode { get; set; }
        public string FromEmployeeName { get; set; }
        public string ToEmployeeName { get; set; }
        public int FromEmployeeID { get; set; }
        public int? ToEmployeeID { get; set; }
        public string Comments { get; set; }
        public List<int> DepartmentIDs { get; set; }
        public string RoleName { get; set; }
        public string DepartmentCode { get; set; }
        public int DepartmentID { get; set; }
        public DateTime ActedOn { get; set; }

    }
}

