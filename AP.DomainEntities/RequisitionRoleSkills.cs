﻿using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class RequisitionRoleSkills : BaseEntity
    {
        public int RRSkillId { get; set; }
        public string OtherSkillCode { get; set; }
        public int CompetencyAreaId { get; set; }
        public int SkillId { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsDefaultSkill { get; set; }
        public int ProficiencyLevelId { get; set; }
        public int TalentRequisitionId { get; set; }
        public int RequistionRoleDetailID { get; set; }
        public string CompetencyAreaCode { get; set; }
        public string ProficiencyLevelCode { get; set; }
        public string SkillGroupName { get; set; }
        public string SkillName { get; set; }
        public string SkillCode { get; set; }
        public Nullable<int> SkillGroupId { get; set; }
        public int RoleSkillMappingID { get; set; }
    }

    //public class RequisitionRoleSkillList : RequisitionRoleSkills
    //{
    //    public List<RequisitionRoleSkills> BasicLevel { get; set; }
    //    public List<RequisitionRoleSkills> BeginnerLevel { get; set; }
    //    public List<RequisitionRoleSkills> IntermediateLevel { get; set; }
    //    public List<RequisitionRoleSkills> AdvanceLevel { get; set; }
    //    public List<RequisitionRoleSkills> ExpertLevel { get; set; }
    //    public List<RequisitionRoleSkills> AvailableSkills { get; set; }
    //}

}
