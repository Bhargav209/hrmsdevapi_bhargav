﻿using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class KRAAspectData
    {
        public int DepartmentId { get; set; }
        public List<AspectData> lstAspectData { get; set; }
        public int KRAAspectID { get; set; }
        public int AspectId { get; set; }
    }
    public class AspectData : BaseEntity
    {
        public int KRAAspectID { get; set; }
        public string KRAAspectName { get; set; }
        public int AspectId { get; set; }
        public string IsMappedAspect { get; set; }

    }
}
