﻿using System.Collections.Generic;

namespace AP.DomainEntities
{
  public class RequisitionLearningAptitudeData : BaseEntity
    {
        public int RequisitionLearningAptitudeId { get; set; }
        public int TalentRequisitionId { get; set; }
        public int LearningAptitudeId { get; set; }
        public string LearningAptitudeDescription { get; set; }
        public int RoleMasterId { get; set; }
    }
}
