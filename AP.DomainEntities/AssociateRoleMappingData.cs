﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AssociateRoleMappingData : BaseEntity
    {
        public List<int> IDs { get; set; }
        public int RoleMasterId { get; set; }
        public int FinancialYearID { get; set; }
        public int? KRAGroupId { get; set; }
        public int? ProjectId { get; set; }
        public int DepartmentId { get; set; }
        public int Id { get; set; }
        public string AssociateName { get; set; }
        public string RoleName { get; set; }
        public bool IsNew { get; set; }
    }
}
