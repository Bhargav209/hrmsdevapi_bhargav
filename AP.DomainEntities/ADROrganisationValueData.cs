﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ADROrganisationValueData : BaseEntity
    {
        public int ADROrganisationValueID { get; set; }
        public string ADROrganisationValue { get; set; }

        public int FromYear { get; set; }

        public int ToYear { get; set; }
        public int FinancialYearId { get; set; }


    }
}
