﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ADROrganisationDevelopmentData : BaseEntity
    {
        public int ADROrganisationDevelopmentID  { get; set; }
        public string ADROrganisationDevelopmentActivity { get; set; }
        public int FromYear { get; set; }
        public int ToYear { get; set; }
        public int FinancialYearId { get; set; }
    }
}
