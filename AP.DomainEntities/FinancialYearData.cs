﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
   public class FinancialYearData
    {
        public int Id { get; set; }
        public int FromYear { get; set; }
        public string Name { get; set; }
        public int ToYear { get; set; }
        public bool IsActive { get; set; }
    }
}
