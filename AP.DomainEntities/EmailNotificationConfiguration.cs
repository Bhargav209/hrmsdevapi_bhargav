﻿namespace AP.DomainEntities
{
    public class EmailNotificationConfiguration:BaseEntity
    {
        public int NotificationTypeID { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }

    }

}
