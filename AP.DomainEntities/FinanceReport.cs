﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class FinanceReport:BaseEntity
    {
        public int AllocationId { get; set; }
        public string AssociateID { get; set; }
        public string AssociateName { get; set; }
        public string Designation { get; set; }
        public int? roleID { get; set; }
        public string Grade { get; set; }
        public int? GradeID { get; set; }
        public string CompetencyGroup { get; set; }
        public string Client { get; set; }
        public int? ClientID { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public bool? Billable { get; set; }
        public string IsBillable { get; set; }
        public string IsCritical { get; set; }
        public decimal? BillablePercentage { get; set; }
        public decimal? AllocationPercentage { get; set; }
        public bool? Critical { get; set; }
        public int? managerId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string ProgramManager { get; set; }
        public string ReportingManager { get; set; }
        public string GroupHead { get; set; }
        public int? leadId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string ProjectGroup { get; set; }
        public int? InternalBillingRoleId { get; set; }
        public int? ClientBillingRoleId { get; set; }
        public string InternalBillingRoleCode { get; set; }
        public string InternalBilling { get; set; }
        public string ClientBilling { get; set; }
        public string BillablePercent { get; set; }
        public string AllocationPercent { get; set; }
        public decimal? InternalBillingPercentage { get; set; }
    }
}
