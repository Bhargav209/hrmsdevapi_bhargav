﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ProjectTypeDetails : BaseEntity
    {
        public int ProjectTypeId { get; set; }
        public string ProjectTypeCode { get; set; }
        public string Description { get; set; }       
    }
}
