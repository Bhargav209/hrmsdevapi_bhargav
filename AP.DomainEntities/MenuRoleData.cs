﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class MenuRoleData : BaseEntity
    {
        public int MenuRoleId { get; set; }
        public int MenuId { get; set; }
        public int RoleId { get; set; }
    }
}
