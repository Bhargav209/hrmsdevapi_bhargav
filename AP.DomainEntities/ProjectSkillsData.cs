﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ProjectSkillsData:BaseEntity
    {
        public int ProjectSkillId { get; set; }       
        public int ProjectId { get; set; }
        public int CompetencyAreaId { get; set; }
        public int SkillId { get; set; }
        public int ProficiencyId { get; set; }       
        public int ProjectRoleId { get; set; }
        public int RoleMasterId { get; set; }
        public IEnumerable<EmployeeSkillDetails> skills { get; set; }
    }
   
}
