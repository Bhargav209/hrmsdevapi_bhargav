﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class JDTemplateTitleData : BaseEntity
    {
        public int TemplateTitleId { get; set; }
        public string TemplateTitle { get; set; }
        public string TemplateDescription { get; set; }
    }
}
