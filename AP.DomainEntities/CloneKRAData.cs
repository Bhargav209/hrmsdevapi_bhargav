﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class CloneKRAData : BaseEntity
    {
        public int FromFinancialYearId { get; set; }
        public int ToFinancialYearId { get; set; }
        public List<int> DepartmentIds { get; set; }
        public List<int> GroupIds { get; set; }
        public int CloneType { get; set; }
    }
}
