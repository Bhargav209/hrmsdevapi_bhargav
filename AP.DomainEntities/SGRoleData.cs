﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
   public class SGRoleData
   {
     public string SGRoleName { get; set; }
      public int DepartmentId { get; set; }
      public string DepartmentName { get; set; }

   }
}
