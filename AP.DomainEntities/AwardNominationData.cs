﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AwardNominationData : BaseEntity
    {
        public int FinancialYearId { get; set; }
        public int ADRCycleId { get; set; }
        public int AwardTypeId { get; set; }
        public string AwardType { get; set; }
        public int AwardId { get; set; }
        public string AwardTitle { get; set; }
        public int AwardNominationId { get; set; }
        public int FromEmployeeId { get; set; }
        public int DepartmentId { get; set; }
        public int ProjectID { get; set; }
        public string NominationDescription { get; set; }
        public string NomineeComments { get; set; }
        public string ReviewerComments { get; set; }
        public string DepartmentName { get; set; }
        public string ProjectName { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string RoleName { get; set; }
        public List<GenericType> AssociateNames { get; set; }
    }
}
