﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AssociateADR : BaseEntity
    {
        public int EmployeeId { get; set; }
        public int KRASetId { get; set; }
        public int ADRCycleId { get; set; }
        public int FinancialYearId { get; set; }
        public string CriticalTasksPerformed { get; set; }
    }
}
