﻿namespace AP.DomainEntities
{
    public class CompetencyAreaData:BaseEntity
    {
        public int CompetencyAreaId { get; set; }
        public string CompetencyAreaCode { get; set; }
        public string CompetencyAreaDescription { get; set; }
    }
}
