﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ProjectAssociateTrainingData : BaseEntity
    {
        public int ProjectAssociateTrainingId { get; set; }
        public int AssociateSkillGapId { get; set; }
        public string SkillName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int ProjectTrainingId { get; set; }
        public int EmployeeId { get; set; }
        public int TrainingId { get; set; }
        public string TrainingName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool? IsSkillApplied { get; set; }
        public int AssessedById { get; set; }
        public string AssessedBy { get; set; }
        public DateTime? AssessedDate { get; set; }
        public string ProficiencyLevelCode { get; set; }
        public int ProficiencyLevelId { get; set; }
        public int? StatusId { get; set; }
        public int ProjectSkillId { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AssociateName { get; set; }
        public string StatusCode { get; set; }
        public string Remarks { get; set; }
        public string AssessedFirstName { get; set; }
        public string AssessedLastName { get; set; }

    }
}
