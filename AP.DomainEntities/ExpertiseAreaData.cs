﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ExpertiseAreaData : BaseEntity
    {
        public int ExpertiseId { get; set; }
        public string ExpertiseAreaDescription { get; set; }
    }
}
