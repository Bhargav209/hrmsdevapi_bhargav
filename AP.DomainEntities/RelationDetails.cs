﻿using AP.Utility;
using System;

namespace AP.DomainEntities
{
    public class RelationDetails : BaseEntity
    {
        public int ID { get; set; }
        public string EncryptedName { get; set; }
        public string name
        {
            get { return Commons.DecryptStringAES(EncryptedName); }
            set { EncryptedName = Commons.EncryptStringAES(value); }
        }
        public int? empID { get; set; }
        public string relationship { get; set; }
        public DateTime? dob { get; set; }
        public string occupation { get; set; }
        public string birthDate { get; set; }
    }
}
