﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRARoleData : BaseEntity
    {
        public int ID { get; set; }
        public int RoleID { get; set; }
        public int KRAAspectID { get; set; }
        public string KRAAspectMetric { get; set; }
        public string KRAAspectTarget { get; set; }
        public int CloneRoleID { get; set; }
        public string KRAAspectName { get; set; }
        public int FinancialYearID { get; set; }
        public int StatusID { get; set; }
        public int CloneFinancialYearID { get; set; }
        public int KRARoleID { get; set; }        
        public string KRARoleName { get; set; }        
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
    }
}
