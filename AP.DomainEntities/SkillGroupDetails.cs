﻿namespace AP.DomainEntities
{
    public class SkillGroupDetails : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int SkillGroupId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string SkillGroupName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int CompetencyAreaId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatedUser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CompetencyAreaCode { get; set; }
    }
}
