﻿using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class RequisitionRoleDetails : BaseEntity
    {
        public int RequisitionRoleDetailID { get; set; }
        public int RoleMasterId { get; set; }
        public Nullable<int> NoOfBillablePositions { get; set; }
        public Nullable<int> NoOfNonBillablePositions { get; set; }
        public Nullable<int> TAPositions { get; set; }
        public Nullable<int> MinimumExperience { get; set; }
        public Nullable<int> MaximumExperience { get; set; }
        public string RoleDescription { get; set; }
        public string KeyResponsibilities { get; set; }
        public string EssentialEducationQualification { get; set; }
        public string DesiredEducationQualification { get; set; }
        public string Expertise { get; set; }
        public string ProjectSpecificResponsibilities { get; set; }
        public IEnumerable<RequisitionRoleSkills> RequisitionRoleSkills { get; set; }
        public List<int> DeltedRRSkillIds { get; set; }
        public int TalentRequisitionId { get; set; }
        public string RoleName { get; set; }
        public string SkillName { get; set; }
        public string Experience { get; set; }
        public int SkillId { get; set; }
        public bool EitherExperienceorPositionsChanged { get; set; }
    }
}
