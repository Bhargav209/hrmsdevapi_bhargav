﻿using AP.Utility;
using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class UserDetails : BaseEntity
    {
        public int ID { get; set; }
        public int empID { get; set; }
        public string name { get; set; }
        public string empCode { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string gender { get; set; }
        public DateTime? dateOfJoining { get; set; }
        public DateTime? dob { get; set; }
        public string maritalStatus { get; set; }
        public int? joiningStatusID { get; set; }
        public int? gradeID { get; set; }
        public int? designationID { get; set; }
        public string designation { get; set; }
        public string employmentType { get; set; }
        public string technology { get; set; }
        public int? technologyID { get; set; }
        public int? deptID { get; set; }
        public string department { get; set; }
        public string hrAdvisor { get; set; }
        public int? bgvStatusID { get; set; }
        public string bgvStatus { get; set; }
        public IEnumerable<EducationDetails> qualifications { get; set; }
        public IEnumerable<EmploymentDetails> prevEmployerdetails { get; set; }
        public IEnumerable<ProfRefDetails> profReference { get; set; }
        public IEnumerable<ProfessionalDetails> certifications { get; set; }
        public IEnumerable<EmployeeSkillDetails> skills { get; set; }
        public IEnumerable<EmployeeProjectDetails> projects { get; set; }
        public IEnumerable<RelationDetails> relationsInfo { get; set; }
        public IEnumerable<EmergencyContactData> emergencyContactsInfoInfo { get; set; }
        public EmergencyContactData contactDetailsOne { get; set; }
        public EmergencyContactData contactDetailsTwo { get; set; }
        public string Birthdate { get; set; }
        public string joiningDate { get; set; }
        public string RecruitedBy { get; set; }
        public string EncryptedMobileNo { get; set; }
        public string mobileNo
        {
            get { return Commons.DecryptStringAES(EncryptedMobileNo); }
            set { EncryptedMobileNo = Commons.EncryptStringAES(value); }
        }
        public string personalEmail { get; set; }
        public int ? managerId { get; set; }
        public int ReportingManagerId { get; set; }
        public string dropoutReason { get; set; }
        public string GradeName { get; set; }
    }

    public class LoginUserRole
    {
        public string role { get; set; }
    }
}
