﻿using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class TalentRequistionData : BaseEntity
    {
        public int TalentRequisitionId { get; set; }
        public int DepartmentId { get; set; }
        public int? ProjectId { get; set; }
        public string RequestedDate { get; set; }
        public string RequiredDate { get; set; }
        public string TargetFulfillmentDate { get; set; }
        public string Status { get; set; }
        public int? ApprovedBy { get; set; }
        public string TRCode { get; set; }
        public IEnumerable<RequisitionRoleDetails> RequisitionRoleDetails { get; set; }
        public List<int> DeltedRRdetailsIds { get; set; }
        public bool IsDoNotSendNotifications { get; set; }
        public int? RequisitionType { get; set; }
        public int EmployeeId { get; set; }
        public int? RaisedBy { get; set; }
        public int? DraftedBy { get; set; }
        public Nullable<int> ProjectDuration{get;set;}
        public int ClientId { get; set; }
        public int? PracticeAreaId { get; set; }
        public int DepartmentTypeId { get; set; }
    }
}
