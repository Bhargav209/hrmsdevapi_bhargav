﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
   public class ResignationApprovalData : BaseEntity
   {
      public int Id { get; set; }
      public int EmployeeId { get; set; }
      public int ResignationID { get; set; }
      public int DepartmentID { get; set; }
      public int ApprovedByID { get; set; }
      public DateTime ApprovedDate { get; set; }
      public string Remarks { get; set; }
      public string DueRemarks { get; set; }
      public string NoDueRemarks { get; set; }
      public string ResignationRecommendation { get; set; }
      public DateTime? LastWorkingDate { get; set; }
      public int StatusID { get; set; }

      public int ToDeliveryHead { get; set; }
    }
}
