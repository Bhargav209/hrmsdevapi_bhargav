﻿using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class AssociateAllocationDetails : BaseEntity
    {
        public int AssociateAllocationId { get; set; }
        public int RAId { get; set; }
        public int? EmployeeId { get; set; }
        public int ProjectId { get; set; }
        public int? RoleMasterId { get; set; }
        public int? ReportingManagerId { get; set; }
        public int? RequisitionRoleDetailID { get; set; }
        public string RoleName { get; set; }
        public int? TalentRequisitionId { get; set; }
        public string DepartmentName { get; set; }
        public string ProjectType { get; set; }
        public string Project { get; set; }
        public int? RoleRequisitionID { get; set; }
        public int? TotalNoOfPosition { get; set; }
        public int? NoOfBillablePosition { get; set; }
        public int? NoOfNonBillablePosition { get; set; }
        public int? NoOfAllocatedBillablePosition { get; set; }
        public int? NoOfAllocatedNonBillablePosition { get; set; }
        public int? NoOfRemainingBillablePosition { get; set; }
        public int? NoOfRemainingNonBillablePosition { get; set; }
        public string EmployeeNameWithGrade { get; set; }
        public decimal ResourceAvailability { get; set; }
        public decimal? AllocationPercentage { get; set; }
        public decimal InternalBillingPercentage { get; set; } = 0;
        public Nullable<bool> isCritical { get; set; }       
        public Nullable<DateTime> EffectiveDate { get; set; }
        public string Status { get; set; }
        public string AssociateName { get; set; }
        public string ReportingManager { get; set; }
        public string Lead { get; set; }
        public decimal? Availability { get; set; }
        public int? CompetencyAreaId { get; set; }
        public List<EmployeeSkillDetails> skills { get; set; }
        public bool IsFullyMatched { get; set; } = false;
        public int ReleaseProjectId { get; set; }
        public bool IsPrimary { get; set; }
        public bool Billable { get; set; }
        public decimal? percentage { get; set; }
        public int? ClientBillingRoleId { get; set; }
        public int? InternalBillingRoleId { get; set; }
        public Nullable<DateTime> ReleaseDate { get; set; }
        public decimal ClientBillingPercentage { get; set; } = 0;
        public decimal ReleasingPercentage { get; set; }  
        public bool NotifyAll { get; set; }
        public int MakePrimaryProjectId { get; set; }
        
    }
}
