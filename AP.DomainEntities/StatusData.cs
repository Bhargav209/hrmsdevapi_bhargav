﻿using System;

namespace AP.DomainEntities
{
    public class StatusData : BaseEntity
    {
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        //public string Category { get; set; }
        public string CategoryName { get; set; }
    }
}
