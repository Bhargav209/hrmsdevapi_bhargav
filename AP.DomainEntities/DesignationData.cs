﻿namespace AP.DomainEntities
{
    public class DesignationData : BaseEntity
    {
        public int DesignationId { get; set; }
        public string DesignationCode { get; set; }
        public string DesignationName { get; set; }
        public int GradeId { get; set; }
        public string GradeCode { get; set; }
    }
}
