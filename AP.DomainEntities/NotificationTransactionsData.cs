﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    class NotificationTransactionsData: BaseEntity
    {
        public int Id { get; set; }
        public int NotificationConfigID { get; set; }
        public string EmailTo { get; set; }
    }
}
