﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ADRData : BaseEntity
    {
        public int ADRSectionId { get; set; }
        public int EmployeeID { get; set; }
        public int AspectId { get; set; }
        public int? AssociateADRDetailId { get; set; }
        public string AspectName { get; set; }
        public string Metric { get; set; }
        public string Operator { get; set; }
        public string StatusCode { get; set; }
        public string Contribution { get; set; }
        public string ManagerComments { get; set; }
        public decimal? Rating { get; set; }
        public decimal? SelfRating { get; set; }
        public string KRATargetPeriod { get; set; }
        public string KRAMeasurementType { get; set; }
        public decimal TargetValue { get; set; }
        public int AssociateKRAMapperId { get; set; }
        public int ADRMeasurementAreaId { get; set; }
        public string ADRSectionName { get; set; }
        public string ADRMeasurementAreaName { get; set; }
        public int FinancialYearId { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentDescription { get; set; }
        public int ADRCycleID { get; set; }
        public int AssociateADRMasterId { get; set; }
        public int StatusId { get; set; }
        public int KRADefinitionId { get; set; }
    }
}
