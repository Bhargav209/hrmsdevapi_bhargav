﻿using AP.Utility;
using System;

namespace AP.DomainEntities
{
    public class AssociateDetails : BaseEntity
    {
        public int empID { get; set; }
        public string empCode { get; set; }
        public string empName { get; set; }
        public string bgvStatus { get; set; }
        public string EncryptedMobileNumber { get; set; }
        public string MobileNo
        {
            get { return Commons.DecryptStringAES(EncryptedMobileNumber); }
            set { EncryptedMobileNumber = Commons.EncryptStringAES(value); }
        }

        public string RoleName { get; set; }

        public string WorkEmail { get; set; }
        public string PersonalEmail { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }
        public int? DepartmentId { get; set; }
        public string Department { get; set; }
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int? LeadId { get; set; }
        public int? ManagerId { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerLastName { get; set; }
        public string LeadFirstName { get; set; }
        public string LeadLastName { get; set; }
        public string ManagerName { get; set; }
        public string LeadName { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string Year { get; set; }
        public int? roleId { get; set; }
        public DateTime? lastWorkingDate { get; set; }
        public string Designation { get; set; }
        public string ReportingManager { get; set; }
        public string Technology { get; set; }
    }
}
