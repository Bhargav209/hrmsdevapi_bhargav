﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TransitionPlanData : BaseEntity
    {
        public int TransitionId { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public int RoleMasterId { get; set; }
        public int StatusId { get; set; }
        public string Remarks { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool isActive { get; set; }
        public string ProjectName { get; set; }
        public string RoleName { get; set; }
        public int ReportingMgr { get; set; }
    }
}
