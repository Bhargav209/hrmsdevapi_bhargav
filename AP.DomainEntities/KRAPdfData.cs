﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRAPdfData:BaseEntity
    {
        public int EmployeeId { get; set; }
        public int RoleMasterID { get; set; }
        public int KRAGroupID { get; set; }
        public string EmployeeCode { get; set; }
        public string AssociateName { get; set; }
        public string RoleName { get; set; }
        public string DepartmentHeadName { get; set; }
        public string DesignationName { get; set; }

    }
}
