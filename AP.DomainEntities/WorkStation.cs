﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class WorkStation : BaseEntity
    {
        public int Id { get; set; }
        public int WorkStationId { get; set; }
        public int BayId { get; set; }
        public int EmployeeId { get; set; }
        public bool IsOccupied { get; set; }
        public string AssociateName { get; set; }
        public string ProjectName { get; set; }
        public string LeadName { get; set; }
        public string WorkStationSuffix { get; set; }
        public string WorkStationCode { get; set; }

        public class WorkStationDataCount
        {
            public int WorkStationCount { get; set; }
            public int BayId { get; set; }
            public string Name { get; set; }
        }
    }
}
