﻿using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class EmployeeSkillDetails : BaseEntity
    {
        public int ID { get; set; }
        public int RoleId { get; set; }
        public int CompetencyAreaID { get; set; }
        public int PracticeAreaID { get; set; }
        public int? skillID { get; set; }
        public string SkillGroupName { get; set; }
        public string CompetencyAreaCode { get; set; }
        public string ProficiencyLevelCode { get; set; }
        public int empID { get; set; }

        public string empName { get; set; }

        public string empCode { get; set; }
        public int? experience { get; set; }
        public int? SkillExperience { get; set; }
        public int? LastUsed { get; set; }
        public int proficiencyLevelId { get; set; }
        public bool? isPrimary { get; set; }
        //public bool IsActive { get; set; }
        public string ProficiencyLevel { get; set; }
        public string CompetencyArea { get; set; }
        public int SkillGroupID { get; set; }
        public string SkillName { get; set; }
        public string EmployeeName { get; set; }
        public string StatusCode { get; set; }
        public string CreatedUser { get; set; }
        public int SubmittedBy { get; set; }
        public int SubmittedTo { get; set; }
        public string ToEmailAddress { get; set; }
        public int StatusId { get; set; }
        public int RequisitionId { get; set; }
        public int SkillsSubmittedForApprovalId { get; set; }
        public int WorkFlowId { get; set; }
        public IEnumerable<SkillDetails> skillDetails { get; set; }

    }
    public class SkillDetails
    {
        public int ID { get; set; }
        public int empID { get; set; }
        public int proficiencyLevelId { get; set; }
        public int CompetencyAreaID { get; set; }
        public int? skillID { get; set; }
        public int? experience { get; set; }
        public int? LastUsed { get; set; }
        public bool? isPrimary { get; set; }
        public int SkillGroupID { get; set; }
    }
}
