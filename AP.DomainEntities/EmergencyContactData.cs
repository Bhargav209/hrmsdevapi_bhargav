﻿using AP.Utility;

namespace AP.DomainEntities
{
    public class EmergencyContactData:BaseEntity
    {
        public int ID { get; set; }
        public bool? isPrimary { get; set; }
        public string contactType { get; set; }
        public int employeeID { get; set; }
        public string contactName { get; set; }
        public string relationship { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string EncryptedTelePhoneNo { get; set; }
        public string telephoneNo
        {
            get { return Commons.DecryptStringAES(EncryptedTelePhoneNo); }
            set { EncryptedTelePhoneNo = Commons.EncryptStringAES(value); }
        }
        public string EncryptedMobileNo { get; set; }
        public string mobileNo
        {
            get { return Commons.DecryptStringAES(EncryptedMobileNo); }
            set { EncryptedMobileNo = Commons.EncryptStringAES(value); }
        }
        public string emailAddress { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public string state { get; set; }

    }
}