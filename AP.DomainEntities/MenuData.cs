﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class MenuData:BaseEntity
    {
        public int MenuId { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public int ParentId { get; set; }
        public int DisplayOrder { get; set; }
        public string Parameter { get; set; }
        public string NodeId { get; set; }
        public string Style { get; set; }
        public List<MenuData> Categories { get; set; }
        public IEnumerable<RoleData> MenuRoles { get; set; }

        public  MenuData()
        {
            Categories = new List<MenuData>();
            MenuRoles = new List<RoleData>();
        }
    }
}
