﻿using System;
using System.Collections.Generic;

namespace AP.DomainEntities
{   
    public class ProjectData : BaseEntity
    {
        public int ProjectId { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        //public string ActualStartDate { get; set; }
        //public string ActualEndDate { get; set; }
        //public string PlannedEndDate { get; set; }
        //public string PlannedStartDate { get; set; }
        public int? CustomerId { get; set; }
        public int? ClientId { get; set; }
        public int? ProjectTypeId { get; set; }
        public int? ManagerId { get; set; }      
        public int? LeadId { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public DateTime? PlannedStartDate { get; set; }
        public DateTime? PlannedEndDate { get; set; }
        public List<int> RoleIds { get; set; }
        public List<int> DeletedRoleIds { get; set; }
        public int? DepartmentId { get; set; }
        public int? DepartmentHeadId { get; set; }
        
        public string ProjectTypeCode { get; set; }
        public string ProjectTypeDescription { get; set; }
        public string ClientName { get; set; }
        public string ClientShortName { get; set; }
        public int? ReportingManagerId { get; set; }
        public int? EmployeeId { get; set; }
        public int PracticeAreaId { get; set; }
        public string PracticeAreaCode { get; set; }
        public string UserRole { get; set; }
        public string DepartmentCode { get; set; }
        public string ManagerName { get; set; }
       
        public string ProjectState { get; set; }
        public int ProjectStateId { get; set; }
        public string DomainName { get; set; }
        public int? DomainId { get; set; }
        public string WorkFlowStatus { get; set; }
        public int? WorkFlowStatusId { get; set; }
        public string ReportingManagerName { get; set; }
        public string LeadName{ get; set; }

    }
}
