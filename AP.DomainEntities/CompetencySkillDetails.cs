﻿using System.Collections.Generic;

namespace AP.DomainEntities
{
    public class CompetencySkillDetails : BaseEntity
    {
        public int RoleID { get; set; }
        public int CompetencyID { get; set; }
        public IEnumerable<int?> Skills { get; set; }        
    }
}
