﻿namespace AP.DomainEntities
{
    public class UserCredentials
    {
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }

    }
}
