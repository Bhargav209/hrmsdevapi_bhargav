﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class GenericType : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class AssociateNameandProject 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProjectId { get; set; }
        public DateTime EffectiveDate { get; set; }
    }

    public class AssociateNamesProjectsList
    {
        public List<object> EmployeeNames;
        public List<AssociateNameandProject> EmployeeProjects;
    }
}
