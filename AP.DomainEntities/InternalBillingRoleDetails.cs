﻿namespace AP.DomainEntities
{
    public class InternalBillingRoleDetails : BaseEntity
    {
        public int InternalBillingRoleId { get; set; }
        public string InternalBillingRoleCode { get; set; }
        public string InternalBillingRoleName { get; set; }
    }
}
