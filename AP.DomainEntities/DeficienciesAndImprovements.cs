﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class DeficienciesAndImprovementsData : BaseEntity
    {
        public int DeficiencyAndImprovementId { get; set; }
        public int EmployeeId { get; set; }
        public int FinancialYearId { get; set; }
        public int CompetancyAreaId { get; set; }
        public int SkillGroupId { get; set; }
        public int SkillId { get; set; }
        public int ADRCycleId { get; set; }
        public bool IsDeficiency { get; set; }
        public int SourceOfOriginId { get; set; }
        public DateTime TargetDate { get; set; }
        public String Comments { get; set; }
        public String ActionItem { get; set; }
     
    }
}
