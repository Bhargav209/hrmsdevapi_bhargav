﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class KRAScaleData
    {
        public int KRAScaleMasterID { get; set; }
        public string ScaleLevel { get; set; }
        public int MinimumScale { get; set; }
        public int MaximumScale { get; set; }
        public string KRAScaleTitle { get; set; }
        public List<KRAScaleDetails> KRAScaleDetails { get; set; }
    }
    public class KRAScaleDetails
    {
        public int KRAScaleDetailId { get; set; }
        public int KRAScale { get; set; }
        public string ScaleDescription { get; set; }
    }
}
