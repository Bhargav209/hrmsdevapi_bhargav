﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class ManagersData
    {
        public string ReportingManagerName { get; set; }
        public string LeadName { get; set; }
        public string ProgramManagerName { get; set; }
        public int? ProgramManagerId { get; set; }
        public int? LeadId { get; set; }
        public int? ReportingManagerId { get; set; }
    }
}
