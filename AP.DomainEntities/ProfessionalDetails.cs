﻿namespace AP.DomainEntities
{
    public class ProfessionalDetails : CommonProficiency
    {
        public int certificationID { get; set; }
        public int skillGroupID { get; set; }
        public string programTitle { get; set; }
        public string SkillGroupName { get; set; }
        public string SkillName { get; set; }
    }
}
