﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class RequisitionSearchFilter
    {
        public int TalentRequisitionId { get; set; }
        public int RoleId { get; set; }
        public int MinimumExperience { get; set; }
        public int MaximumExperience { get; set; }
        public List<GenericType> Skills { get; set; }
        public List<GenericType> Proficiencies { get; set; }
        public int TotalPositions { get; set; }
    }
}
