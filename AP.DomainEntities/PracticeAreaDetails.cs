﻿using System;

namespace AP.DomainEntities
{
    public class PracticeAreaDetails : BaseEntity
    {
        public int PracticeAreaId { get; set; }
        public string PracticeAreaCode { get; set; }
        public string PracticeAreaDescription { get; set; }
    }
}
