﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class TalentPoolData  : BaseEntity
    {
        public int PracticeAreaId { get; set; }
        public int ProjectId { get; set; }

    }
}
