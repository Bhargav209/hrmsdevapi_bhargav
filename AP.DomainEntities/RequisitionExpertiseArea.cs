﻿namespace AP.DomainEntities
{
    public class RequisitionExpertiseArea:BaseEntity
    {
        public int RequisitionExpertiseId { get; set; }
        public int TalentRequisitionId { get; set; }
        public int ExpertiseAreaId { get; set; }
        public string ExpertiseDescription { get; set; }
        public string ExpertiseAreaDescription { get; set; }
        public int ExpertiseId { get; set; }
        public int RoleMasterId { get; set; }
    }
}
