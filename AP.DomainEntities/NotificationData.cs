﻿namespace AP.DomainEntities
{
    public class NotificationData : BaseEntity
    {
        public int? notificationTypeId { get; set; }
        public string notificationCode { get; set; }
        public string notificationDesc { get; set; }
        public string employeeCode { get; set; }
        public string taskName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int statusId { get; set; }
        public int taskId { get; set; }
        public string statusCode { get; set; }
        public string NotificationConfigurationFromMail { get; set; }
    }

}
