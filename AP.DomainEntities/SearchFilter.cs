﻿using System;

namespace AP.DomainEntities
{
    public class SearchFilter
    {
        public string IsBillable { get; set; }
        public string IsCritical { get; set; }
        public int Gender { get; set; }
        public string ProjectName { get; set; }
        public int ProjectId { get; set; }
        public int RoleId { get; set; }
        public int ClientId { get; set; }
        public int Utilization { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string AssociateCode { get; set; }
        public string AssociateName { get; set; }
        public string GroupHead { get; set; }
        public string GenderName { get; set; }
        public string Designation { get; set; }
        public string Experience { get; set; }
        public int GradeId { get; set; }
        public int SkillId { get; set; }
        public int CompetencyId { get; set; }
        public string ResourceType { get; set; }
        public string SearchType { get; set; }
        public string SearchData { get; set; }
        public string SearchCategory { get; set; }
        public int ManagerId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int[] SkillGroups { get; set; }
        public int AssociateId { get; set; }
    }
}
