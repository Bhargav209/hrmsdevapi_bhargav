﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class AssociateDesignationData : BaseEntity
    {
        public int EmployeeID { get; set; }
        public int DesignationID { get; set; }
        public int GradeID { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
