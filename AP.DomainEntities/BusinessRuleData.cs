﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
   public class BusinessRuleData : BaseEntity
    {
        public int BusinessRuleId { get; set; }
        public string BusinessRuleCode { get; set; }
        public string Description { get; set; }
        public string BusinessRuleValue { get; set; }
    }
}
