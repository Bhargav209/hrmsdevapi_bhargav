﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class SkillGapData:BaseEntity
    {
        public int AssociateSkillGapId { get; set; }
        public int EmpId { get; set; }
        public int ProjectId { get; set; }
       // public int ProjectSkillId { get; set; }
        public int SkillId { get; set; }
        public int CompetencyAreaId { get; set; }     
        public int StatusId { get; set; }
        public string Status { get; set; }
        public int CurrentProficiencyLevelId { get; set; }
        public string currProficiencyLevelDescription { get; set; }
        public int RequiredProficiencyLevelId { get; set; }
        public string empFirstName { get; set; }
        public string empLastName { get; set; }
        public string CompetencyAreaCode { get; set; }
        public string employeeName { get; set; }
        public string RequiredProficiencyLevel { get; set; }
        public string CurrentProficiencyLevel { get; set; }
        public string SkillCode { get; set; }
        public string StatusCode { get; set; }
    }
}
