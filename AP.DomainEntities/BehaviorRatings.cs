﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class BehaviorRatings : BaseEntity
    {
        public int BehaviorRatingId { get; set; }
        public string Rating { get; set; }
    }
}
