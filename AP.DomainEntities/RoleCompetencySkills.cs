﻿using System;

namespace AP.DomainEntities
{
    public class RoleCompetencySkills
    {
        public int CompetencySkillsId { get; set; }
        public Nullable<int> CompetencyAreaId { get; set; }
        public Nullable<int> SkillId { get; set; }
        public int RoleMasterID { get; set; }
        public bool  IsPrimary { get; set; }
        public int ProficiencyLevelId { get; set; }
        public Nullable<int> SkillGroupId { get; set; }
        public string CompetencyAreaCode { get; set; }
        public string SkillGroupName { get; set; }
        public string SkillName { get; set; }
        public string ProficiencyLevelCode { get; set; }
    }
}
