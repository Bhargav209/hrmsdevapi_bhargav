﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    public class MenuRoles
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public List<Menus> MenuList { get; set; }

    }

    public class Menus
    {
        public int MenuId { get; set; }

        public string MenuName { get; set; }
    }
}
