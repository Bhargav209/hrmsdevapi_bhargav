﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.DomainEntities
{
    /// <summary>
    /// Used in Associate Joining dashboard
    /// </summary>
    public class AssociateJoiningData : BaseEntity
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int Designation { get; set; }
        public DateTime? JoinDate { get; set; }
        public string HRAdvisor { get; set; }
        public string TechnologyName { get; set; }
        public string DesignationName { get; set; }
        public string DepartmentCode { get; set; }
        public int DepartmentId { get; set; }
    }
}
