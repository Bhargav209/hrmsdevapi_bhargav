﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class AspectsMaster
    {
        #region GetAspectsMaster
        /// <summary>
        /// Gets Aspect Master
        /// </summary>
        /// <returns></returns>
        public async Task<List<AspectData>> GetAspectsMaster()
        {
            List<AspectData> lstAspects;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAspects = await apEntities.Database.SqlQuery<AspectData>
                              ("[usp_GetAspects]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAspects.OrderBy(aspects => aspects.CreatedDate).ToList();
        }
        #endregion

        #region CreateAspectMaster
        /// <summary>
        /// Create a new Aspect Master
        /// </summary>
        /// <param name="aspectData"></param>
        /// <returns></returns>
        public async Task<int> CreateAspectMaster(AspectData aspectData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {

                    //VALIDATIONS
                    if (aspectData.KRAAspectName.Length > 70)
                        return -15; //Length greater than actual length provided.

                    int position = aspectData.KRAAspectName.IndexOf('-', 0);
                    int position1 = aspectData.KRAAspectName.IndexOf('&', 0);
                    int position2 = aspectData.KRAAspectName.IndexOf(',', 0);
                    if (position > 0)
                    {
                        if (aspectData.KRAAspectName.ElementAt(position) == aspectData.KRAAspectName.ElementAt(position + 1))
                            return -16; //Invalid KRA Aspects (same characters repeated multiple times).
                    }
                    else if (position1 > 0)
                    {
                        if (aspectData.KRAAspectName.ElementAt(position1) == aspectData.KRAAspectName.ElementAt(position1 + 1))
                            return -16; //Invalid KRA Aspects (same characters repeated multiple times).
                    }
                    else if (position2 > 0)
                    {
                        if (aspectData.KRAAspectName.ElementAt(position2) == aspectData.KRAAspectName.ElementAt(position2 + 1))
                            return -16; //Invalid KRA Aspects (same characters repeated multiple times).
                    }
                    using (var trans = apEntities.Database.BeginTransaction())
                    {

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateAspectMaster] @AspectName, @CreatedDate, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("AspectName", aspectData.KRAAspectName),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region UpdateAspectMaster
        /// <summary>
        /// Update a Aspect Master
        /// </summary>
        /// <param name="aspectData"></param>
        /// <returns></returns>
        public async Task<int> UpdateAspectMaster(AspectData aspectData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateAspectMaster] @AspectId, @AspectName, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("AspectId", aspectData.AspectId),
                                        new SqlParameter ("AspectName", aspectData.KRAAspectName),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region DeleteAspectMaster
        /// <summary>
        /// delete a Aspect Master
        /// </summary>
        /// <param name="aspectId"></param>
        /// <returns></returns>
        public async Task<int> DeleteAspectMaster(int aspectId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteAspectMaster] @AspectId",
                               new object[] {
                                        new SqlParameter ("AspectId", aspectId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
