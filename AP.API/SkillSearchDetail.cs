﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.API
{
    public class SkillSearchDetail
    {
        #region GetAllSkillDetails
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EmployeeSkillDetails>> GetAllSkillDetails(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkill = await apEntities.Database.SqlQuery<EmployeeSkillDetails>
                              ("[usp_GetAllSkillDetails] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", empID) }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get associate skills");
            }
            return lstSkill;
        }
        #endregion
        public async Task<List<SkillSearchData>> GetEmployeesBySkill(SkillSearchData skilldata)
        {
            List<SkillSearchData> skillSearchEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    skillSearchEmployees = await apEntities.Database.SqlQuery<SkillSearchData>
                                    ("[usp_GetEmployeesBySkill] @SkillIds,@SkillNames,@IsPrimary,@IsSecondary,@IsCritical,@IsnonCritical,@IsBillable,@IsnonBillable",
                                    new object[]
                                    {
                                        new SqlParameter("SkillIds",string.IsNullOrEmpty(skilldata.SkillIds)?"":skilldata.SkillIds),
                                        new SqlParameter("SkillNames",string.IsNullOrEmpty(skilldata.SkillNames)?"":skilldata.SkillNames),
                                        new SqlParameter("IsPrimary",skilldata.IsPrimary),
                                        new SqlParameter("IsSecondary",skilldata.IsSecondary),
                                        new SqlParameter("IsCritical",skilldata.IsCritical),
                                        new SqlParameter("IsnonCritical",skilldata.IsnonCritical),
                                        new SqlParameter("IsBillable",skilldata.IsBillable),
                                        new SqlParameter("IsnonBillable",skilldata.IsnonBillable),

                                    }).ToListAsync();

                    skillSearchEmployees = skillSearchEmployees.OrderBy(p => p.EmployeeName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return skillSearchEmployees;

        }

    }
}
