﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class KRAAspects
    {
        #region GetKRAAspect
        /// <summary>
        /// Gets KRA Aspect Master
        /// </summary>
        /// <returns></returns>
        public async Task<List<AspectData>> GetKRAAspect(int departmentId)
        {
            List<AspectData> lstKRAAspect;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAAspect = await apEntities.Database.SqlQuery<AspectData>
                              ("[usp_GetKRAAspect] @DepartmentId",
                              new object[] {
                                        new SqlParameter ("DepartmentId", departmentId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAAspect.OrderBy(aspects=>aspects.CreatedDate).ToList();
        }
        #endregion

        #region CreateKRAAspect
        /// <summary>
        /// Create a new KRA Aspect
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        public async Task<int> CreateKRAAspect(KRAAspectData kraAspectData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        if (kraAspectData.DepartmentId == 0)
                            throw new AssociatePortalException("Department Id cannot be null.");

                        foreach (var aspects in kraAspectData.lstAspectData)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateKRAAspect] @AspectId, @DepartmentId, @CreatedDate, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("AspectId", aspects.AspectId),
                                        new SqlParameter ("DepartmentId", kraAspectData.DepartmentId),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        }
                        trans.Commit();
                    }
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region UpdateKRAAspect
        /// <summary>
        /// Update a KRA Aspect
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        public async Task<int> UpdateKRAAspect(KRAAspectData kraAspectData)
        {
            int rowsAffected=0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        foreach (var aspects in kraAspectData.lstAspectData)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateKRAAspect] @KRAAspectID, @AspectId,@DepartmentId,@DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("KRAAspectID", kraAspectData.KRAAspectID),
                                        new SqlParameter ("AspectId", Convert.ToString(aspects.AspectId)),
                                        new SqlParameter ("DepartmentId", kraAspectData.DepartmentId),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion



        #region DeleteKRAAspect
        /// <summary>
        /// Delete a KRA Aspect
        /// </summary>
        /// <param name="kraAspectData"></param>
        /// <returns></returns>
        public async Task<int> DeleteKRAAspect(KRAAspectData kraAspectData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {  
                      rowsAffected = await apEntities.Database.SqlQuery<int>
                         ("[usp_DeleteKraAspect] @KRAAspectID",
                             new object[] {
                                        new SqlParameter ("KRAAspectID", kraAspectData.KRAAspectID),                                                
                             }
                          ).SingleOrDefaultAsync();                       
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
