﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using static AP.DomainEntities.WorkStation;

namespace AP.API
{
    public class WorkStationDetails
    {
        #region EmployeeDeskAllocation
        /// <summary>
        /// Employee Desk Allocation
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        public async Task<string> EmployeeDeskAllocation(int employeeId, string workStationId)
        {
            string rowsAffected;
            int workStationID = Convert.ToInt32(workStationId);
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<string>
                            ("[usp_AllocateWorkstation] @WorkStationId,@EmployeeId, @CreatedDate, @CreatedUser, @SystemInfo",
                            new object[] {
                                new SqlParameter("WorkStationId",workStationID),
                                new SqlParameter("EmployeeId",employeeId),
                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),

                            }).SingleOrDefaultAsync();
                        trans.Commit();                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region ReleaseDesk
        /// <summary>
        /// Release Desk
        /// </summary>
        /// <param name="employeeId,workStationId"></param>
        /// <returns></returns>
        public async Task<int> ReleaseDesk(int employeeId, int workStationId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                            ("[usp_ReleaseDesk] @WorkStationId,@EmployeeId, @ModifiedDate, @ModifiedUser, @SystemInfo",
                            new object[] {
                                new SqlParameter("WorkStationId",workStationId),
                                new SqlParameter("EmployeeId",employeeId),
                                new SqlParameter("ModifiedDate", DateTime.UtcNow),
                                new SqlParameter("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter("SystemInfo", Commons.GetClientIPAddress()),

                            }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        /// <summary>
        /// Get WorkStation List
        /// </summary>
        /// <returns></returns>
        public async Task<List<WorkStation>> GetWorkStationListByBayId(int bayIds)
        {
            List<WorkStation> lstWorkStation;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstWorkStation = await apEntities.Database.SqlQuery<WorkStation>
                              ("[usp_GetWorkStations] @BayIds",

                               new object[] {
                                        new SqlParameter ("BayIds", bayIds)
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Work Stations");
            }
            return lstWorkStation;
        }

        public async Task<List<WorkStationDataCount>> WorkStationReportCount(int bayId)
        {
            List<WorkStationDataCount> lstWorkStationDataCount;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstWorkStationDataCount = await apEntities.Database.SqlQuery<WorkStationDataCount>
                                               ("[usp_chart_WorkStationReportCount] @BayId",
                                               new object[] { new SqlParameter("BayId", bayId) }
                                               ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Work Stations");
            }
            return lstWorkStationDataCount;
        }

        public async Task<List<WorkStation>> GetWorkStationDetailByWorkStationCode(string workstationCode)
        {
            List<WorkStation> lstWorkStations;
            string code = null;
            string suffix = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(workstationCode) && workstationCode.Contains("-"))
                {
                    code = workstationCode.Split('-')[0];
                    suffix = workstationCode.Split('-')[0];
                }
                else
                    code = workstationCode;
                using (var apEntities = new APEntities())
                {
                    if (!string.IsNullOrWhiteSpace(suffix))
                    {
                        lstWorkStations = await apEntities.Database.SqlQuery<WorkStation>
                                            ("USP_GETWORKSTATIONDETAIL @workStationId,@workStationSuffix",
                                            new object[] {
                                                new SqlParameter("workStationId", code),
                                                new SqlParameter("workStationSuffix", suffix)
                                            }).ToListAsync();
                    }
                    else
                    {
                        lstWorkStations = await apEntities.Database.SqlQuery<WorkStation>
                                            ("USP_GETWORKSTATIONDETAIL @workStationId",
                                            new object[] {
                                                new SqlParameter("workStationId", code)
                                            }).ToListAsync();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Work Station Details");
            }
            return lstWorkStations;
        }
    }
}
