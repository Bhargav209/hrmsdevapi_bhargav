﻿using System;
using System.Net.Mail;
using System.Configuration;
using AP.Utility;
using AP.DomainEntities;
using AP.DataStorage;
using System.Collections.Generic;
using System.Web;
using System.Linq;

namespace AP.API
{
    public class BaseEmail
    {
        //#region SendEmail
        //NotificationTransaction notificationTransaction;
        ///// <summary>
        ///// Code to send email
        ///// </summary>
        ///// <param name="toAddress"></param>
        ///// <param name="fromAddress"></param>
        ///// <param name="ccAddress">The list CC addresses with semi-colon (;) seperated.</param>
        ///// <param name="subject"></param>
        ///// <param name="body"></param>
        //public void SendEmail(Email email,int NotificationConfigID)
        //{
        //    MailMessage objMailMessage = new MailMessage();
        //    SmtpClient objSmtpClient = new SmtpClient();
        //    string smtpClientaddress = Convert.ToString(ConfigurationManager.AppSettings["SMTPClient"]);

        //    try
        //    {
        //        using (APEntities hrmsEntity = new APEntities())
        //        {
        //            List<NotificationTransaction> notificationTransactionDetails = new List<NotificationTransaction>();
        //            objMailMessage.From = new MailAddress(email.FromEmail);
        //            email.ToEmail = email.ToEmail.TrimEnd(';');
        //            string[] toAddresses = !string.IsNullOrEmpty(email.ToEmail) ? email.ToEmail.Split(';') : new string[] { };
        //            if (toAddresses.Length > 0)
        //            {
        //                foreach (String address in toAddresses)
        //                {
        //                    objMailMessage.To.Add(address);
        //                    FillNotificationTransactionData(notificationTransactionDetails, email, address, NotificationConfigID);
        //                }
        //            }
        //            else
        //            {
        //                toAddresses = email.FromEmail.Split(';');
        //                throw new ApplicationException();
        //            }
        //            //objMailMessage.CC.Add(new MailAddress(ccAddress));
        //            email.CcEmail = email.CcEmail.TrimEnd(';');
        //            string[] addresses = !string.IsNullOrEmpty(email.CcEmail) ? email.CcEmail.Split(';') : new string[] { };
        //            if (addresses.Length > 0)
        //            {
        //                foreach (String address in addresses)
        //                {
        //                    objMailMessage.CC.Add(address);
        //                    FillNotificationTransactionData(notificationTransactionDetails, email, address, NotificationConfigID);
        //                }
        //            }
        //            //
        //            objMailMessage.Subject = email.Subject;
        //            objMailMessage.IsBodyHtml = true;
        //            objMailMessage.Body = email.EmailBody;
        //            objSmtpClient = new SmtpClient(smtpClientaddress);
        //            objSmtpClient.UseDefaultCredentials = false;
        //            objSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;


        //            if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendMail"]))
        //                objSmtpClient.Send(objMailMessage);

        //            if (notificationTransactionDetails.Count > 0)
        //            {
        //                hrmsEntity.NotificationTransactions.AddRange(notificationTransactionDetails);
        //                hrmsEntity.SaveChanges();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string parms = "[]";
        //        Log.LogError(ex, Log.Severity.Error, parms);

        //        throw new AssociatePortalException("Failed to send notification.");
        //    }
        //    finally
        //    {
        //        objMailMessage = null;
        //        objSmtpClient = null;
        //    }
        //}
        //#endregion
        //#region FillNotificationTransactionData
        ///// <summary>
        ///// Code to send email
        ///// </summary>
        ///// <param name="notificationTransactionDetails"></param> this param is to log mail sent transaction details
        ///// <param name="email"></param> this param having all email attributes
        ///// <param name="mailAddress">The list To/CC addresses with semi-colon (;) seperated.</param>
        ///// <param name="NotificationConfigID"></param> This param tells you the notificaiton to whom it is notifying 
        //private void FillNotificationTransactionData(List<NotificationTransaction> notificationTransactionDetails, Email email, string mailAddress, int NotificationConfigID)
        //{
        //    notificationTransaction = new NotificationTransaction();
        //    notificationTransaction.NotificationConfigID = NotificationConfigID;
        //    notificationTransaction.EmailTo = mailAddress;
        //    notificationTransaction.CreatedDate = DateTime.Now;
        //    notificationTransaction.CreatedUser = email.CurrentUser;
        //    notificationTransaction.SystemInfo = email.SystemInfo;
        //    notificationTransactionDetails.Add(notificationTransaction);
        //}
        //#endregion
        //#region BuildEmailObject
        ///// <summary>
        ///// </summary>
        ///// <param name="email"></param> this param having all email attributes
        ///// <param name="emailTo">The list To addresses with semi-colon (;) seperated.</param>
        ///// <param name="emailFrom">The param tells you from where the email is going</param>
        ///// <param name="emailCC">The list CC addresses with semi-colon (;) seperated.</param>
        ///// <param name="subject">Subject of the email</param>
        ///// <param name="emailContent">body of the email</param>
        ///// <param name="NotificationConfigID"></param> This param tells you the notificaiton to whom it is notifying 
        //public int BuildEmailObject(Email email, string emailTo, string emailFrom, string emailCC, string subject, string emailContent, string NotificationConfigID)
        //{
        //    try
        //    {
        //        using (APEntities hrmEntities = new APEntities())
        //        {
        //            string notificationConfigID = ConstantResources.ResourceManager.GetString(NotificationConfigID);
        //            var notificationConfigurationDetails = (from nc in hrmEntities.NotificationConfigurations
        //                                                    where nc.notificationCode == notificationConfigID
        //                                                    select new
        //                                                    {
        //                                                        nc.NotificationConfigID
        //                                                    }).FirstOrDefault();

        //            email.ToEmail = emailTo;
        //            email.FromEmail = emailFrom;
        //            email.CcEmail = emailCC;
        //            email.Subject = subject;
        //            email.EmailBody = emailContent;
        //            email.SystemInfo = Commons.GetClientIPAddress();
        //            email.CurrentUser = HttpContext.Current.User.Identity.Name;

        //            return notificationConfigurationDetails != null ? notificationConfigurationDetails.NotificationConfigID : 0;
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to get details.");
        //    }
        //}
        //#endregion       
    }
}
