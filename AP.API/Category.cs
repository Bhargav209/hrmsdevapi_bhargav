﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class Category
    {
        #region CreateCategory
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        public async Task<bool> CreateCategory(CategoryDetails categoryDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_AddCategory] @CategoryName, @IsActive, @CreatedUser, @SystemInfo, @CreatedDate",
                                   new object[] {
                                        new SqlParameter ("CategoryName", categoryDetails.CategoryName),
                                        new SqlParameter ("IsActive", categoryDetails.IsActive),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while adding a category");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region UpdateCategory
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCategory(CategoryDetails categoryDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateCategory] @CategoryID, @CategoryName, @IsActive, @ModifiedUser, @SystemInfo, @ModifiedDate",
                                   new object[] {
                                        new SqlParameter ("CategoryID", categoryDetails.CategoryID),
                                        new SqlParameter ("CategoryName", categoryDetails.CategoryName),
                                        new SqlParameter ("IsActive", categoryDetails.IsActive),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while updating a category");
            }
            return rowsAffected > 0 ? true : false;

        }
        #endregion

        #region GetCategories
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<CategoryDetails>> GetCategories()
        {
            List<CategoryDetails> lstCategories;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstCategories = await apEntities.Database.SqlQuery<CategoryDetails>
                              ("[usp_GetAllCategories]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get categories");
            }
            return lstCategories;
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<List<CategoryDetails>> GetActiveCategories()
        {
            List<CategoryDetails> lstCategories;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstCategories = await apEntities.Database.SqlQuery<CategoryDetails>
                              ("[usp_GetActiveCategories]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get active categories");
            }
            return lstCategories;
        }
        #endregion

        #region DeleteCategory
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCategory(CategoryDetails categoryDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteCategory] @CategoryID, @ModifiedUser, @SystemInfo, @ModifiedDate",
                                   new object[] {
                                        new SqlParameter ("CategoryID", categoryDetails.CategoryID),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting a category");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion
    }
}
