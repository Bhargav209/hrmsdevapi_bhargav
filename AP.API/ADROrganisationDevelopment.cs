﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class ADROrganisationDevelopment
    {
        #region GetADROrganisationDevelopment
        /// <summary>
        /// Gets GetADROrganisationDevelopment Master
        /// </summary>
        /// <returns></returns>
        public async Task<List<ADROrganisationDevelopmentData>> GetADROrganisationDevelopment(int financialYearId)
        {
            List<ADROrganisationDevelopmentData> lstOrgDevActivities;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstOrgDevActivities = await apEntities.Database.SqlQuery<ADROrganisationDevelopmentData>
                              ("[usp_GetADROrganisationDevelopment] @FinancialYearId",
                                   new object[] {
                                        new SqlParameter ("FinancialYearId", financialYearId),
                                       
                                   }
                                   ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstOrgDevActivities;
        }
        #endregion

        #region CreateADROrganisationDevelopment
        /// <summary>
        /// Create a new ADROrganisationDevelopment
        /// </summary>
        /// <param name="organisationDevelopmentData"></param>
        /// <returns></returns>
        public async Task<int> CreateADROrganisationDevelopment(ADROrganisationDevelopmentData organisationDevelopmentData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateADROrganisationDevelopment] @ADROrganisationDevelopmentActivity,@FinancialYearId, @CreatedDate, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ADROrganisationDevelopmentActivity", organisationDevelopmentData.ADROrganisationDevelopmentActivity),
                                        new SqlParameter ("FinancialYearId", organisationDevelopmentData.FinancialYearId),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region Update ADROrganisationDevelopment
        /// <summary>
        /// Update ADROrganisationDevelopment
        /// </summary>
        /// <param name="organisationDevelopmentData"></param>
        /// <returns></returns>
        public async Task<int> UpdateADROrganisationDevelopment(ADROrganisationDevelopmentData organisationDevelopmentData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateADROrganisationDevelopment] @ADROrganisationDevelopmentID, @ADROrganisationDevelopmentActivity, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("ADROrganisationDevelopmentID", organisationDevelopmentData.ADROrganisationDevelopmentID),
                                        new SqlParameter ("ADROrganisationDevelopmentActivity", organisationDevelopmentData.ADROrganisationDevelopmentActivity),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
