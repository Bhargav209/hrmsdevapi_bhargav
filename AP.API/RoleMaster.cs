﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.Data.Entity;
using System.Data.SqlClient;

namespace AP.API
{
    public class RoleMaster
    {
        #region CreateUserRole
        /// <summary>
        /// Create Role Master
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        public bool CreateUserRole(RoleData roleData)
        {
            bool userRetValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from r in hrmsEntities.Roles
                                    where r.RoleName == roleData.RoleName && r.IsActive == true
                                    select r).Count();
                    if (isExists == 0)
                    {
                        Role role = new Role();
                        role.RoleName = roleData.RoleName;
                        role.RoleDescription = roleData.RoleDescription;
                        role.KeyResponsibilities = roleData.KeyResponsibilities;
                        role.IsActive = roleData.IsActive;
                        role.CreatedUser = roleData.CurrentUser;
                        role.CreatedDate = DateTime.Now;
                        role.SystemInfo = roleData.SystemInfo;
                        role.DepartmentId = roleData.DepartmentId;
                        role.EducationQualification = roleData.EducationQualification;
                        hrmsEntities.Roles.Add(role);
                        userRetValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                    }
                    else
                        throw new AssociatePortalException("Role name already exists");
                }
            }

            catch
            {
                throw;
            }

            return userRetValue;
        }
        #endregion

        #region GetRoleMasterDetails
        /// <summary>
        /// Get role Master Details in Grid
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetRoleMasterDetails(bool isActive, int? departmentId = null)
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var query = (from role in hrmEntities.Roles
                                 join department in hrmEntities.Departments on role.DepartmentId equals department.DepartmentId
                                 into info
                                 from d in info.DefaultIfEmpty()
                                 select new { role.RoleId, role.RoleName, role.RoleDescription, role.KeyResponsibilities, EssentialEducationQualification = role.EducationQualification, IsActive = role.IsActive == true ? "Yes" : "No", role.DepartmentId, DepartmentCode = d.Description });

                    if (departmentId.HasValue)
                        query = query.Where(d => d.DepartmentId == departmentId && d.IsActive == "Yes");

                    if (isActive)
                        query = query.Where(i => i.IsActive == "Yes");

                    //var roleMasterDetails = query.OrderBy(r => r.RoleName).ToList();
                    var roleMasterDetails = query.ToList();

                    if (roleMasterDetails.Count > 0)
                    {
                        roleMasterDetails = roleMasterDetails.OrderBy(r => r.RoleName).ToList();
                        return roleMasterDetails;
                    }
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetRoleMasterDetails by project id
        /// <summary>
        /// Get Role Master details by project id
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetRoleMasterDetailsByProjectId(int projectId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var roleMasterDetails = hrmEntities.Database.SqlQuery<string>
                                 ("[usp_GetRoleMasterDetailsByProjectId] @ProjectId",
                                   new object[]  {
                                    new SqlParameter ("ProjectId", projectId),
                                  }
                                 ).ToList();

                    //(from pr in hrmEntities.ProjectRoles
                    //                         where pr.IsActive == true && pr.ProjectId == projectId
                    //                         select new
                    //                         {
                    //                             ProjectId = pr.ProjectId,
                    //                             RoleId = pr.RoleMasterId,
                    //                             RoleName = pr.Role.RoleName,
                    //                             RoleDescription = pr.Role.RoleDescription,
                    //                             KeyResponsibilities = pr.Responsibilities == null ? pr.Role.KeyResponsibilities : pr.Responsibilities,
                    //                             EducationQualification = pr.Role.EducationQualification
                    //                         }).ToList();

                    if (roleMasterDetails.Count > 0)
                    {
                        //roleMasterDetails = roleMasterDetails.OrderBy(r => r.RoleName).ToList();
                        return roleMasterDetails;
                    }
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        //#region GetRoleMasterDetailByRoleId
        ///// <summary>
        ///// GetRoleMasterDetails
        ///// </summary>
        ///// <returns></returns>
        ///// 
        //public object GetRoleMasterDetailByRoleId(int roleid)
        //{
        //   string parms = "[]";

        //   try
        //   {
        //      using (APEntities hrmEntities = new APEntities())
        //      {
        //         var ddlRoleData = (from skill in hrmEntities.CompetencySkills
        //                            where skill.RoleMasterID == roleid 
        //                               RoleCompetencySkills = a.CompetencySkills.Select(c => new
        //                               {
        //                                  CompetencySkillsId = c.CompetencySkillsId,
        //                                  CompetencyAreaCode = c.CompetencyArea.CompetencyAreaCode,
        //                                  CompetencyAreaId = c.CompetencyAreaId,
        //                                  SkillId = c.SkillId,
        //                                  SkillCode = c.Skill.SkillCode,
        //                                  ProficiencyLevelId = c.ProficiencyLevelId,
        //                                  ProficiencyLevelCode = c.ProficiencyLevel.ProficiencyLevelCode,
        //                                  SkillGroupName=c.Skill.SkillGroup.SkillGroupName,
        //                                  SkillGroupId=c.Skill.SkillGroupId,
        //                                  IsPrimary = c.IsPrimary.HasValue ? c.IsPrimary.Value : false
        //                               })
        //                            }).FirstOrDefault();
        //         return ddlRoleData;
        //      }
        //   }
        //   catch (Exception ex)
        //   {
        //      Log.LogError(ex, Log.Severity.Error, parms);
        //      throw;
        //   }

        //}
        //#endregion

        //#region UpdateRoleMasterDetails
        ///// <summary>
        ///// Update Role Master details
        ///// </summary>
        ///// <param name="roleData"></param>
        ///// <returns></returns>
        //public bool UpdateRoleMasterDetails(RoleData roleData)
        //{

        //   bool modifiedRetValue = false;

        //   using (APEntities hrmsEntities = new APEntities())
        //   {
        //      using (var dbContext = hrmsEntities.Database.BeginTransaction())
        //      {
        //         try
        //         {
        //            var isExists = (from r in hrmsEntities.Roles
        //                            where  r.RoleName == roleData.RoleName && r.IsActive==true && r.RoleId != roleData.RoleId
        //                            select r).Count();

        //            var department = (from dept in hrmsEntities.Departments
        //                              where dept.DepartmentId == roleData.DepartmentId
        //                              select dept).FirstOrDefault();
        //            if (isExists == 0)
        //            {
        //               Role role = hrmsEntities.Roles.First(r => r.RoleId == roleData.RoleId);
        //               if (!(bool)roleData.IsActive) //If role is marked for inactive
        //               {                       
        //                  List<int> lstSkillsToBeDeleted = new List<int>();
        //                  foreach (var roleCompetencySkill in roleData.RoleCompetencySkills)
        //                  {
        //                     lstSkillsToBeDeleted.Add(roleCompetencySkill.CompetencySkillsId);
        //                  }

        //                  if (DeleteRoleCompetencySkills(lstSkillsToBeDeleted))
        //                  {
        //                     role.IsActive = roleData.IsActive;
        //                     hrmsEntities.Entry(role).State = System.Data.Entity.EntityState.Modified;
        //                     dbContext.Commit();
        //                     return hrmsEntities.SaveChanges() > 0 ? true : false; 

        //                  }
        //               } 
        //               role.RoleName = roleData.RoleName;
        //               role.RoleDescription = roleData.RoleDescription;
        //               role.KeyResponsibilities = roleData.KeyResponsibilities;
        //               role.EducationQualification = roleData.EducationQualification;

        //               if (department.IsActive == true)
        //                  role.IsActive = roleData.IsActive;
        //               else
        //                  throw new AssociatePortalException("Department is inactive.");

        //               role.ModifiedUser = roleData.CurrentUser;
        //               role.SystemInfo = roleData.SystemInfo;
        //               role.ModifiedDate = DateTime.Now;
        //               role.DepartmentId = roleData.DepartmentId;
        //               modifiedRetValue = hrmsEntities.SaveChanges() > 0 ? true : false;                   


        //            }
        //            else
        //               throw new AssociatePortalException("Role name already exists");

        //            dbContext.Commit();
        //            return modifiedRetValue;
        //         }
        //         catch
        //         {
        //            dbContext.Rollback();
        //            throw;
        //         }
        //      }

        //   }
        //}
        //#endregion

        //#region PrivateMehtods
        //private bool AddRoleCompetencySkills(IEnumerable<RoleCompetencySkills> roleCompetencySkills, int roleId, string currentUser, string systemInfo)
        //{
        //   bool returnValue = false;
        //   using (APEntities hrmsEntities = new APEntities())
        //   {
        //      List<CompetencySkill> competencySkillList = new List<CompetencySkill>();
        //      foreach (var skill in roleCompetencySkills)
        //      {
        //         CompetencySkill roleCompetencySkill = new CompetencySkill()
        //         {

        //            RoleMasterID = roleId,
        //            CompetencyAreaId = skill.CompetencyAreaId,
        //            SkillId = skill.SkillId,
        //            ProficiencyLevelId = skill.ProficiencyLevelId,
        //            SkillGroupID = skill.SkillGroupId,
        //            CreatedUser = currentUser,
        //            IsPrimary = skill.IsPrimary,
        //            IsActive = true,
        //            CreatedDate = DateTime.Now,
        //            SystemInfo = systemInfo
        //         };
        //         competencySkillList.Add(roleCompetencySkill);
        //      }

        //      hrmsEntities.CompetencySkills.AddRange(competencySkillList);
        //      returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //   }

        //   return returnValue;
        //}

        //private bool DeleteRoleCompetencySkills(List<int> deleteids)
        //{
        //   bool returnValue = false;

        //   using (APEntities hrmsEntities = new APEntities())
        //   {
        //      var existCompetencySkills = hrmsEntities.CompetencySkills.Where(r => deleteids.Contains(r.CompetencySkillsId)).ToList();
        //      hrmsEntities.CompetencySkills.RemoveRange(existCompetencySkills);
        //      returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //   }

        //   return returnValue;
        //}
        //#endregion
    }
}
