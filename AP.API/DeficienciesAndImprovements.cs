﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class DeficienciesAndImprovements
    {
        #region GetDeficienciesAndImprovements
        /// <summary>
        /// Gets DeficienciesAndImprovements
        /// </summary>
        /// <returns></returns>
        public async Task<List<DeficienciesAndImprovementsData>> GetDeficienciesAndImprovements(int employeeId)
        {
            List<DeficienciesAndImprovementsData> lstDeficienciesAndImprovements;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstDeficienciesAndImprovements = await apEntities.Database.SqlQuery<DeficienciesAndImprovementsData>
                              ("[usp_GetDeficienciesandImprovementsByEmployeeId] @EmployeeId",
                                   new object[] {
                                        new SqlParameter ("EmployeeId", employeeId),

                                   }
                                   ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDeficienciesAndImprovements;
        }
        #endregion

        #region CreateDeficienciesAndImprovements
        /// <summary>
        /// Create a new DeficienciesAndImprovements
        /// </summary>
        /// <param name="deficienciesAndImprovementsData"></param>
        /// <returns></returns>
        public async Task<int> CreateDeficienciesAndImprovements(DeficienciesAndImprovementsData deficienciesAndImprovementsData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateDeficienyandImprovement] @EmployeeId, @FinancialYearId, @CompetancyAreaId, @SkillGroupId, @SkillId, @ADRCycleId, @isDeficiency, @SourceOfOriginId, @TargetDate, @Comments, @ActionItem, @CreatedUser, @CreatedDate, @SystemInfo ",
                                   new object[] {
                                        new SqlParameter ("EmployeeId", deficienciesAndImprovementsData.EmployeeId),
                                        new SqlParameter ("FinancialYearId", deficienciesAndImprovementsData.FinancialYearId),
                                        new SqlParameter ("CompetancyAreaId", deficienciesAndImprovementsData.CompetancyAreaId),
                                        new SqlParameter ("SkillGroupId", deficienciesAndImprovementsData.SkillGroupId),
                                        new SqlParameter ("SkillId", deficienciesAndImprovementsData.SkillId),
                                        new SqlParameter ("ADRCycleId", deficienciesAndImprovementsData.ADRCycleId),
                                        new SqlParameter ("isDeficiency", deficienciesAndImprovementsData.IsDeficiency),
                                        new SqlParameter ("SourceOfOriginId", deficienciesAndImprovementsData.SourceOfOriginId),
                                        new SqlParameter ("TargetDate", deficienciesAndImprovementsData.TargetDate),
                                        new SqlParameter ("Comments", deficienciesAndImprovementsData.Comments),
                                        new SqlParameter ("ActionItem", deficienciesAndImprovementsData.ActionItem),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion


        #region Update DeficienciesAndImprovements
        /// <summary>
        /// Update DeficienciesAndImprovements
        /// </summary>
        /// <param name="deficienciesAndImprovementsData"></param>
        /// <returns></returns>
        public async Task<int> UpdateDeficienciesAndImprovements(DeficienciesAndImprovementsData deficienciesAndImprovementsData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateDeficienyandImprovement] @DeficiencyAndImprovementId,  @FinancialYearId, @CompetancyAreaId,@SkillGroupId, @SkillId, @ADRCycleId , @isDeficiency, @SourceOfOriginId, @TargetDate, @Comments, @ActionItem, @ModifiedDate ,  @ModifiedUser,  @SystemInfo",
                               new object[] {
                                       new SqlParameter ("EmployeeId", deficienciesAndImprovementsData.EmployeeId),
                                        new SqlParameter ("FinancialYearId", deficienciesAndImprovementsData.FinancialYearId),
                                        new SqlParameter ("CompetancyAreaId", deficienciesAndImprovementsData.CompetancyAreaId),
                                        new SqlParameter ("SkillGroupId", deficienciesAndImprovementsData.SkillGroupId),
                                        new SqlParameter ("SkillId", deficienciesAndImprovementsData.SkillId),
                                        new SqlParameter ("ADRCycleId", deficienciesAndImprovementsData.ADRCycleId),
                                        new SqlParameter ("isDeficiency", deficienciesAndImprovementsData.IsDeficiency),
                                        new SqlParameter ("SourceOfOriginId", deficienciesAndImprovementsData.SourceOfOriginId),
                                        new SqlParameter ("TargetDate", deficienciesAndImprovementsData.TargetDate),
                                        new SqlParameter ("Comments", deficienciesAndImprovementsData.Comments),
                                        new SqlParameter ("ActionItem", deficienciesAndImprovementsData.ActionItem),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
