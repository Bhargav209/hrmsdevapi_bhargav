﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AP.DataStorage;
using System.Threading.Tasks;
using AP.DomainEntities;
using System.Data.SqlClient;
using AP.Utility;
using System.Web;

namespace AP.API
{
    public class AwardNomination
    {
        /// <summary>
        /// Get AwardNomination List
        /// </summary>
        /// <returns></returns>
        public async Task<List<AwardNominationData>> GetAwardNominationList(int financialYearId, int fromEmployeeId,string roleName)
        {
            List<AwardNominationData> lstAwardNominationList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAwardNominationList = await apEntities.Database.SqlQuery<AwardNominationData>
                              ("[usp_GetAwardNomination]@FinancialYearId, @RoleName,@EmployeeId",
                               new object[] {
                                        new SqlParameter ("FinancialYearId", financialYearId),
                                        new SqlParameter ("RoleName", roleName),
                                        new SqlParameter ("EmployeeId", fromEmployeeId),

                               }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAwardNominationList;
        }

        /// <summary>
        /// Create Award Nomination
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CreateAwardNomination(AwardNominationData awardNominationData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        for (int i = 0; i < awardNominationData.AssociateNames.Count; i++)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_CreateAwardNomination] @AwardId,@AwardTypeId, @ADRCycleId, @FinancialYearId,@NominationDescription,@FromEmployeeId, @NomineeComments,@ReviewerComments,@DepartmentId,@ProjectId,@CreatedBy,@CreatedDate,@ModifiedBy,@ModifiedDate,@SystemInfo,@ToEmployeeId",
                                       new object[] {
                                        new SqlParameter ("AwardId", awardNominationData.AwardId),
                                        new SqlParameter ("AwardTypeId", awardNominationData.AwardTypeId),
                                        new SqlParameter ("ADRCycleId", awardNominationData.ADRCycleId),
                                        new SqlParameter ("FinancialYearId",awardNominationData.FinancialYearId),
                                        new SqlParameter ("NominationDescription",awardNominationData.NominationDescription),
                                        new SqlParameter ("FromEmployeeId", awardNominationData.FromEmployeeId),
                                        new SqlParameter ("NomineeComments", awardNominationData.NomineeComments),
                                        new SqlParameter ("ReviewerComments", awardNominationData.ReviewerComments),
                                        new SqlParameter ("DepartmentId", awardNominationData.DepartmentId),
                                        new SqlParameter ("ProjectId", awardNominationData.ProjectID),                                        
                                        new SqlParameter ("CreatedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("ToEmployeeId", awardNominationData.AssociateNames[i].Id),
                                       }
                                       ).SingleOrDefaultAsync();
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a Award Nomination");
            }
            return rowsAffected > 0 ? true : false;
        }

        /// <summary>
        /// Update Award Nomination
        /// </summary>
        /// <param name="AwardNominationData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAwardNomination(AwardNominationData AwardNominationData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        for (int i = 0; i < AwardNominationData.AssociateNames.Count; i++)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                          ("[usp_UpdateAwardNomination] @AwardId,@AwardTypeId,@AwardNominationId, @NominationDescription,@NomineeComments,@ReviewerComments,@ToEmployeeId,@CreatedBy,@CreatedDate,@ModifiedBy,@ModifiedDate,@SystemInfo,@RoleName,@FromEmployeeId",
                              new object[] {
                                        new SqlParameter ("AwardId", AwardNominationData.AwardId),
                                        new SqlParameter ("AwardTypeId", AwardNominationData.AwardTypeId),
                                        new SqlParameter ("AwardNominationId", AwardNominationData.AwardNominationId),
                                        new SqlParameter ("NominationDescription",AwardNominationData.NominationDescription),
                                        new SqlParameter ("NomineeComments", AwardNominationData.NomineeComments),
                                        new SqlParameter ("ReviewerComments", AwardNominationData.ReviewerComments),
                                        new SqlParameter ("ToEmployeeId", AwardNominationData.AssociateNames[i].Id),                                        
                                        new SqlParameter ("CreatedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("RoleName", AwardNominationData.RoleName),
                                        new SqlParameter ("FromEmployeeId", AwardNominationData.FromEmployeeId),
                              }).SingleOrDefaultAsync();
                            trans.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while updating Award nomination");
            }
            return rowsAffected > 0 ? true : false;
        }

        /// <summary>
        /// Get Awards List
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetAwardsList()
        {
            List<GenericType> lstAwards;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAwards = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetAwards]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAwards;
        }

        /// <summary>
        /// Get Awards Type List
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetAwardTypeList()
        {
            List<GenericType> lstAwardType;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAwardType = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetAwardTypes]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAwardType;
        }
    }
}
