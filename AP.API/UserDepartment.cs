﻿using System;
using System.Collections.Generic;
using System.Linq;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.Threading.Tasks;

namespace AP.API
{
    public class UserDepartment
    {
        #region CreateDepartment
        /// <summary>
        /// Method to add new department data to database
        /// </summary>
        /// <param name="departmentData"></param>
        /// <returns></returns>
        public int CreateDepartment(DepartmentData departmentData)
        {
            int deptRetValue = 0;

            using (APEntities hrmsEntities = new APEntities())
            {
                try
                {
                    if (string.IsNullOrEmpty(departmentData.DepartmentCode.Trim()) || string.IsNullOrEmpty(departmentData.Description.Trim()))
                        return -13; //Mandatory fields cannot be empty

                    var isExists = (from d in hrmsEntities.Departments
                                    where d.DepartmentCode.ToLower().Trim() == departmentData.DepartmentCode.ToLower().Trim() || d.Description.ToLower().Trim() == departmentData.Description.ToLower().Trim()
                                    select d).Count();
                    if (isExists == 0)
                    {

                        if (departmentData.DepartmentTypeId == 1)
                        {
                            //Getting Department type count in Department table

                            var isDeptTypeCount = (from d in hrmsEntities.Departments
                                                   where d.DepartmentTypeId == departmentData.DepartmentTypeId
                                                   select d).Count();
                            if (isDeptTypeCount > 0)
                                return -1; //Duplicate department codes.
                        }
                        Department department = new Department();
                        department.DepartmentCode = departmentData.DepartmentCode;
                        department.Description = departmentData.Description;
                        department.DepartmentHeadID = departmentData.DepartmentHeadId;
                        department.CreatedUser = departmentData.CurrentUser;
                        department.CreatedDate = DateTime.Now;
                        department.SystemInfo = departmentData.SystemInfo;
                        department.DepartmentTypeId = departmentData.DepartmentTypeId;
                        hrmsEntities.Departments.Add(department);
                        deptRetValue = hrmsEntities.SaveChanges();
                    }
                    else
                        return -1; //Duplicate department codes.
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return deptRetValue;
        }
        #endregion

        #region GetUserDepartmentDetails
        /// <summary>
        /// GetUserDepartmentDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetUserDepartmentDetails()
        {           
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {                    
                        var department = hrmEntities.Database.SqlQuery<DepartmentData>
                               ("[usp_GetUserDepartmentDetails] ",
                                   new object[] { }
                                   ).ToList();                      
                        return department;                                      
                }
            }
            catch
            {
                throw;
            }        
        }
        #endregion

        #region UpdateUserDepartmentDetails
        /// <summary>
        /// Method to update existing department details
        /// </summary>
        /// <param name="departmentData"></param>
        /// <returns></returns>
        public int UpdateUserDepartmentDetails(DepartmentData departmentData)
        {
            int isUpdated = 0;

            using (APEntities hrmEntities = new APEntities())
            {
                if (string.IsNullOrEmpty(departmentData.Description.Trim()))
                    return -13; //Mandatory fields cannot be empty

                //Check for Department code duplicate
                int isExists = (from d in hrmEntities.Departments
                                where d.Description == departmentData.Description && d.DepartmentId != departmentData.DepartmentId
                                select d).Count();
                if (isExists > 0)
                    throw new AssociatePortalException(departmentData.DepartmentCode + " Department code already exists");

                Department department = hrmEntities.Departments.FirstOrDefault(i => i.DepartmentId == departmentData.DepartmentId);
                department.Description = departmentData.Description;
                department.DepartmentHeadID = departmentData.DepartmentHeadId;
                department.ModifiedDate = DateTime.Now;
                department.ModifiedUser = departmentData.CurrentUser;
                department.SystemInfo = departmentData.SystemInfo;
                hrmEntities.Entry(department).State = System.Data.Entity.EntityState.Modified;
                isUpdated = hrmEntities.SaveChanges();
            }

            return isUpdated;
        }
        #endregion

        #region Get Department Heads
        /// <summary>
        /// Get Department Heads
        /// </summary>
        /// <returns></returns>
        /// 
        //public IEnumerable<object> GetDepartmentHeads()
        //{
        //    try
        //    {
        //        using (APEntities hrmEntities = new APEntities())
        //        {
        //            var depHeads = (from dep in hrmEntities.Departments
        //                            join emp in hrmEntities.Employees on dep.DepartmentHead equals emp.EmployeeId
        //                            where dep.DepartmentHead != null && emp.IsActive == true && dep.IsActive == true
        //                            select new
        //                            {
        //                                EmployeeId = emp.EmployeeId,
        //                                EmployeeCode = emp.EmployeeCode,
        //                                EmployeeName = emp.FirstName + "" + emp.LastName
        //                            }).OrderBy(x => x.EmployeeName).ToList();

        //            return depHeads;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        #endregion

        #region GetDepartmentDetails
        /// <summary>
        /// GetDepartmentDetails
        /// </summary>
        /// /// <param name="isActive"></param>
        /// <returns></returns>
        /// // This service was written for Angular2 application and is being consumed in Angular2 application only.
        public IEnumerable<object> GetDepartmentDetails(bool isActive)
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var userDepartmentDetails = (from entity in hrmEntities.Departments
                                                 join emp in hrmEntities.Employees on entity.DepartmentHeadID equals emp.EmployeeId into DepartmentHeads
                                                 from departmentHead in DepartmentHeads.DefaultIfEmpty()
                                                 join departmentType in hrmEntities.DepartmentTypes on entity.DepartmentTypeId equals departmentType.DepartmentTypeId
                                                 select new
                                                 {
                                                     entity.DepartmentId,
                                                     entity.DepartmentCode,
                                                     entity.Description,
                                                     IsActive = entity.IsActive == true ? "Yes" : "No",
                                                     DepartmentHeadName = departmentHead != null ? departmentHead.FirstName + " " + departmentHead.LastName : "",
                                                     DepartmentHeadId = departmentHead != null ? departmentHead.EmployeeId : 0,
                                                     departmentType.DepartmentTypeId,
                                                     departmentType.DepartmentTypeDescription

                                                 }).OrderBy(dep => dep.DepartmentCode).ToList();

                    if (isActive == true)
                        userDepartmentDetails = userDepartmentDetails.Where(i => i.IsActive == "Yes").ToList();

                    if (userDepartmentDetails.Count != 0)
                        return userDepartmentDetails;
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
