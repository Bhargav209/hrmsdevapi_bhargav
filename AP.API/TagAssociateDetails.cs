﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class TagAssociateListDetails
    {
        public async Task<List<TagAssociateList>> GetTagListDetailsByManagerId(int managerId)
        {
            List<TagAssociateList> tagListDetails = new List<TagAssociateList>();
            try
            {
                using (var apentities = new APEntities())
                {
                    tagListDetails = await apentities.Database.SqlQuery<TagAssociateList>(
                        "[USP_GetTagAssociateDetails] @MANAGERID",
                        new object[]
                        {
                            new SqlParameter("@MANAGERID",managerId)
                        }).ToListAsync();
                    tagListDetails = tagListDetails.OrderBy(p => p.TagListName).ToList();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Tag list.");
            }

            return tagListDetails;
        }

        public async Task<List<TagAssociateList>> GetTagListNamesByManagerId(int managerId)
        {
            List<TagAssociateList> tagListNames = new List<TagAssociateList>();
            try
            {
                using (var apEntities = new APEntities())
                {
                    tagListNames = await apEntities.Database.SqlQuery<TagAssociateList>(
                        "USP_GetTagAssociateListNames @MANAGERID",
                        new object[]
                        {
                            new SqlParameter("@MANAGERID",managerId)
                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get tag list names.");
            }
            return tagListNames;
        }

        public async Task<int> CreateTagList(List<TagAssociateList> tagLists)
        {
            int rowsAffected = 0;

            try
            {

                using (var apEntities = new APEntities())
                {
                    using (var transaction = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>(
                        "[usp_CreateTagAssociateList] @ManagerId,@TagListName,@EmployeeIds,@CreatedUser,@SystemInfo",
                        new object[]
                        {
                            new SqlParameter("@ManagerId",tagLists.Select(p=>p.ManagerId).FirstOrDefault()),                           
                            new SqlParameter("@TagListName",tagLists.Select(p=>p.TagListName).FirstOrDefault()),
                            new SqlParameter("@EmployeeIds",string.Join(",", tagLists.Select(p=>p.EmployeeId).Distinct().ToList())),
                            new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                            new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                        }).SingleOrDefaultAsync();

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to insert Tag lists.");
            }

            return rowsAffected;
        }

        public async Task<int> UpdateTagAssociateList(List<TagAssociateList> tagLists)
        {
            int rowsAffected = 0;

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var transaction = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>(
                        "[usp_UpdateTagAssociateList] @ManagerId,@TagListName,@EmployeeIds,@CreatedUser,@SystemInfo",
                        new object[]
                        {
                            new SqlParameter("@ManagerId",tagLists.Select(p=>p.ManagerId).FirstOrDefault()),
                            new SqlParameter("@TagListName",tagLists.Select(p=>p.TagListName).FirstOrDefault()),
                            new SqlParameter("@EmployeeIds",string.Join(",", tagLists.Select(p=>p.EmployeeId).Distinct().ToList())),
                            new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                            new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                        }).SingleOrDefaultAsync();

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to update Tag lists.");
            }

            return rowsAffected;
        }

        public async Task<int> DeleteTagAssociate(int tagAssociateId)
        {
            int rowsAffected = 0;

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var transaction = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>(
                        "[usp_DeleteTagAssociate] @TagAssociateId",
                        new object[]
                        {
                            new SqlParameter("@TagAssociateId", tagAssociateId)
                        }).SingleOrDefaultAsync();

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to delete associate from Tag lists.");
            }

            return rowsAffected;
        }


        public async Task<int> DeleteTagList(string tagListName, int managerId)
        {
            int rowsAffected = 0;

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var transaction = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>(
                        "[usp_DeleteTagList] @TagAssociateListName, @ManagerId ",
                        new object[]
                        {
                            new SqlParameter("TagAssociateListName",tagListName),
                            new SqlParameter("ManagerId",managerId)
                        }).SingleOrDefaultAsync();

                        transaction.Commit();

                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to delete Tag list.");
            }

            return rowsAffected;
        }
    }
}
