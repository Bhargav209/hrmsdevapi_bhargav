﻿using System;
using System.Collections.Generic;
using AP.DomainEntities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DataStorage;
using System.Data.SqlClient;
using System.Web;
using AP.Utility;

namespace AP.API
{
    public class Appreciation
    {
        /// <summary>
        /// Gets Appreciation History
        /// </summary>
        /// <returns></returns>
        public async Task<List<AppreciationData>> GetAppreciationHistory(int empID)
        {
            List<AppreciationData> lstAppreciationHistory;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAppreciationHistory = await apEntities.Database.SqlQuery<AppreciationData>
                              ("[usp_GetAppreciationHistory] @empID",
                                   new object[] {
                                        new SqlParameter("empID",empID),
                                   }
                                   ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAppreciationHistory;
        }

        /// <summary>
        /// Gets My Appreciation
        /// </summary>
        /// <returns></returns>
        public async Task<List<AppreciationData>> GetMyAppreciation(int empID)
        {
            List<AppreciationData> lstMyAppreciations;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstMyAppreciations = await apEntities.Database.SqlQuery<AppreciationData>
                              ("[usp_GetMyAppreciation] @empID",
                                   new object[] {
                                        new SqlParameter("empID",empID),
                                   }
                                   ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstMyAppreciations;
        }

        /// <summary>
        /// Get AssociateNames List
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetAssociateNamesList()
        {
            List<GenericType> lstAssociateNamesList;
            try
            {
                using (var apEntities = new APEntities())
                {
                   int employeeId = -1;
                    string roleName = string.Empty;
                    lstAssociateNamesList = await apEntities.Database.SqlQuery<GenericType>
                               ("[usp_GetAssociatesforReleaseAndResourceReports] @EmployeeID, @RoleName",
                              new object[] {
                                    new SqlParameter("EmployeeID", employeeId),
                                new SqlParameter("RoleName", roleName)}).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAssociateNamesList;
        }

        /// <summary>
        /// Get Appreciation Type List
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetAppreciationTypeList()
        {
            List<GenericType> lstAppreciationTypeList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAppreciationTypeList = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetAppreciationType]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAppreciationTypeList;
        }

        /// <summary>
        /// Create Appreciation
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CreateAppreciation(AppreciationData AppreciationData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        for (int i = 0; i < AppreciationData.AssociateNames.Count; i++)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_CreateAppreciation]  @ADRCycleID, @FinancialYearID,@FromEmployeeID, @ToEmployeeID, @AppreciationTypeID, @AppreciationMessage, @AppreciationDate, @DateCreated, @CreatedUser, @SystemInfo",
                                       new object[] {
                                        new SqlParameter ("ADRCycleID", AppreciationData.ADRCycleID),
                                        new SqlParameter ("FinancialYearID",AppreciationData.FinancialYearID),
                                        new SqlParameter ("FromEmployeeID", AppreciationData.FromEmployeeID),
                                        new SqlParameter ("ToEmployeeID", AppreciationData.AssociateNames[i].Id),
                                        new SqlParameter ("AppreciationTypeID", AppreciationData.AppreciationTypeID),
                                        new SqlParameter ("AppreciationMessage", AppreciationData.AppreciationMessage),
                                        new SqlParameter ("AppreciationDate", Commons.GetDateTimeInIST(AppreciationData.AppreciationDate)),
                                        new SqlParameter ("DateCreated", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                       }
                                       ).SingleOrDefaultAsync();
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a Appreciation");
            }
            return rowsAffected > 0 ? true : false;
        }

        /// <summary>
        /// Update An Appreciation
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateAnAppreciation(AppreciationData AppreciationData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                              ("[usp_UpdateAppreciation] @ID, @AppreciationMessage,@DateModified,@ModifiedUser, @SystemInfo",
                                  new object[] {
                                        new SqlParameter ("ID", AppreciationData.ID),
                                        new SqlParameter ("AppreciationMessage", AppreciationData.AppreciationMessage),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                  }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while updating a Appreciation");
            }
            return rowsAffected > 0 ? true : false;
        }
    }
}
