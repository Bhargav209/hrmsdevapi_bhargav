﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Resources;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.SqlClient;

namespace AP.API
{
    public class ProjectSkillsDetails
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);
       
        //#region GetProjectSkills
        ///// <summary>
        ///// Retrieves the basic details of ProjectSkills
        ///// </summary>
        ///// <returns></returns>
        ///// 
        //public IEnumerable<object> GetProjectSkills(int projectID, int employeeId)
        //{
        //    try
        //    {
        //        using (APEntities hrmsEntity = new APEntities())
        //        {
        //            var projectSkills = (from ps in hrmsEntity.ProjectSkills
        //                                 join pr in hrmsEntity.ProjectRoles on ps.ProjectRoleId equals pr.ProjectRoleId
        //                                 join r in hrmsEntity.RoleMasters on pr.RoleMasterId equals r.RoleMasterID
        //                                 join comparea in hrmsEntity.CompetencyAreas on ps.CompetencyAreaId equals comparea.CompetencyAreaId
        //                                 join al in hrmsEntity.AssociateAllocations on ps.ProjectId equals al.ProjectId
        //                                 join proj in hrmsEntity.Projects on ps.ProjectId equals proj.ProjectId
        //                                 join projectmgrs in hrmsEntity.ProjectManagers on proj.ProjectId equals projectmgrs.ProjectID
        //                                 where ps.ProjectId == projectID && (projectmgrs.ReportingManagerID == employeeId || projectmgrs.ProgramManagerID == employeeId) && al.EmployeeId == employeeId && ps.IsActive == true && al.IsActive == true && proj.IsActive == true &&
        //                                 comparea.IsActive == true && pr.IsActive == true && r.IsActive == true
        //                                 //   where ps.ProjectSkillId == currentID && ps.IsActive == true && comparea.IsActive == true
        //                                 //&& pr.IsActive == true && r.IsActive == true
        //                                 group ps by new
        //                                 {
        //                                     r.RoleMasterID,
        //                                     r.RoleName,
        //                                     comparea.CompetencyAreaCode,
        //                                     ps.CompetencyAreaId,
        //                                     ps.ProjectRoleId,
        //                                     proj.ProjectName,
        //                                     proj.ProjectId
        //                                 }
        //                            into projectRoleSkills
        //                                 select new
        //                                 {
        //                                     //ProjectSkillId = projectRoleSkills.Key.ProjectSkillId,
        //                                     ProjectName = projectRoleSkills.Key.ProjectName,
        //                                     ProjectId = projectRoleSkills.Key.ProjectId,
        //                                     RoleId = projectRoleSkills.Key.RoleId,
        //                                     CompetencyAreaId = projectRoleSkills.Key.CompetencyAreaId,
        //                                     RoleName = projectRoleSkills.Key.RoleName,
        //                                     CompetencyAreaCode = projectRoleSkills.Key.CompetencyAreaCode,
        //                                     ProjectRoleId = projectRoleSkills.Key.ProjectRoleId,
        //                                     skills = (from pskill in hrmsEntity.ProjectSkills
        //                                               join s in hrmsEntity.Skills on pskill.SkillId equals s.SkillId
        //                                               join profLevel in hrmsEntity.ProficiencyLevels on pskill.ProficiencyId equals profLevel.ProficiencyLevelId
        //                                               join pr in hrmsEntity.ProjectRoles on pskill.ProjectRoleId equals pr.ProjectRoleId
        //                                               join r in hrmsEntity.Roles on pr.RoleMasterId equals r.RoleId
        //                                               where pskill.ProjectId == projectRoleSkills.Key.ProjectId && r.RoleId == projectRoleSkills.Key.RoleId && s.IsActive == true && profLevel.IsActive == true
        //                                               && pskill.CompetencyAreaId == projectRoleSkills.Key.CompetencyAreaId && pskill.IsActive == true
        //                                               select new
        //                                               {
        //                                                   ProjectSkillId = pskill.ProjectSkillId,
        //                                                   SkillId = pskill.SkillId,
        //                                                   SkillName = s.SkillCode,
        //                                                   ProficiencyLevelDescription = profLevel.ProficiencyLevelDescription,
        //                                                   ProficiencyId = pskill.ProficiencyId

        //                                               })

        //                                 }).ToList().Distinct();

        //            //var projectSkills = (from ps in hrmsEntity.ProjectSkills
        //            //                     join pr in hrmsEntity.ProjectRoles on ps.ProjectRoleId equals pr.ProjectRoleId
        //            //                     join r in hrmsEntity.Roles on pr.RoleId equals r.RoleId
        //            //                     join al in hrmsEntity.AssociateAllocations on ps.ProjectId equals al.ProjectId
        //            //                     join proj in hrmsEntity.Projects on ps.ProjectId equals proj.ProjectId
        //            //                     join comparea in hrmsEntity.CompetencyAreas on ps.CompetencyAreaId equals comparea.CompetencyAreaId
        //            //                     join skill in hrmsEntity.Skills on ps.SkillId equals skill.SkillId
        //            //                     join profLevel in hrmsEntity.ProficiencyLevels on ps.ProficiencyId equals profLevel.ProficiencyLevelId
        //            //                     where ps.ProjectId == projectID && (proj.LeadId == employeeId || proj.ManagerId == employeeId) && al.EmployeeId== employeeId && ps.IsActive == true && al.IsActive== true && proj.IsActive==true &&
        //            //                     comparea.IsActive==true && skill.IsActive==true && profLevel.IsActive==true && pr.IsActive == true && r.IsActive == true
        //            //                     select new
        //            //                     {
        //            //                         ProjectSkillId= ps.ProjectSkillId,
        //            //                         ProjectName = proj.ProjectName,
        //            //                         ProjectId=ps.ProjectId,
        //            //                         RoleName=r.RoleName,
        //            //                         RoleId=r.RoleId,
        //            //                         CompetencyAreaId = ps.CompetencyAreaId,
        //            //                         CompetencyAreaCode=comparea.CompetencyAreaCode,
        //            //                         SkillId = ps.SkillId,
        //            //                         SkillName = skill.SkillName,                                            
        //            //                         ProficiencyId = ps.ProficiencyId,
        //            //                         ProficiencyLevelDescription = profLevel.ProficiencyLevelDescription,
        //            //                         ProjectRoleId=ps.ProjectRoleId,                                            
        //            //                         IsActive = ps.IsActive
        //            //                     }).ToList();


        //            return projectSkills;
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to get details");
        //    }
        //}
        //#endregion


        #region GetProjSkillsById
        /// <summary>
        /// Retrieves the details of ProjectSkills of currentID
        /// </summary>
        /// <returns></returns>
        /// 
        public object getProjSkillsById(int currentID)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var projectSkills = (from ps in hrmsEntity.ProjectSkills
                                         join pr in hrmsEntity.ProjectRoles on ps.ProjectRoleId equals pr.ProjectRoleId
                                         join r in hrmsEntity.RoleMasters on pr.RoleMasterId equals r.RoleMasterID
                                         join comparea in hrmsEntity.CompetencyAreas on ps.CompetencyAreaId equals comparea.CompetencyAreaId
                                         where ps.ProjectSkillId == currentID && ps.IsActive == true && comparea.IsActive == true
                                      && pr.IsActive == true && r.IsActive == true
                                         group ps by new
                                         {
                                             r.RoleMasterID,
                                             ps.CompetencyAreaId
                                         }
                                     into projectRoleSkills
                                         select new
                                         {
                                             RoleId = projectRoleSkills.Key.RoleMasterID,
                                             CompetencyAreaId = projectRoleSkills.Key.CompetencyAreaId,

                                             skills = (from pskill in hrmsEntity.ProjectSkills
                                                       join s in hrmsEntity.Skills on pskill.SkillId equals s.SkillId
                                                       join pr in hrmsEntity.ProjectRoles on pskill.ProjectRoleId equals pr.ProjectRoleId
                                                       join r in hrmsEntity.RoleMasters on pr.RoleMasterId equals r.RoleMasterID
                                                       where pskill.ProjectSkillId == currentID && r.RoleMasterID == projectRoleSkills.Key.RoleMasterID
                                                       && pskill.CompetencyAreaId == projectRoleSkills.Key.CompetencyAreaId && pskill.IsActive == true
                                                       select new
                                                       {
                                                           SkillId = pskill.SkillId,
                                                           SkillName = s.SkillCode,
                                                           ProficiencyId = pskill.ProficiencyId

                                                       }),

                                             skillList = (from skill in hrmsEntity.Skills
                                                          where skill.CompetencyAreaId == projectRoleSkills.Key.CompetencyAreaId && skill.IsApproved == true
                                                          select new { skill.SkillId, SkillName = skill.SkillCode }),

                                         }).ToList().Distinct();











                    //var projectSkills = (from ps in hrmsEntity.ProjectSkills
                    //                     join pr in hrmsEntity.ProjectRoles on ps.ProjectRoleId equals pr.ProjectRoleId
                    //                     join r in hrmsEntity.Roles on pr.RoleId equals r.RoleId
                    //                     join proj in hrmsEntity.Projects on ps.ProjectId equals proj.ProjectId
                    //                     join comparea in hrmsEntity.CompetencyAreas on ps.CompetencyAreaId equals comparea.CompetencyAreaId
                    //                     join skill in hrmsEntity.Skills on ps.SkillId equals skill.SkillId
                    //                     join profLevel in hrmsEntity.ProficiencyLevels on ps.ProficiencyId equals profLevel.ProficiencyLevelId
                    //                     where ps.ProjectSkillId == currentID && ps.IsActive==true && proj.IsActive==true && comparea.IsActive==true
                    //                     && skill.IsActive==true && profLevel.IsActive==true && pr.IsActive == true && r.IsActive == true
                    //                     select new
                    //                     {
                    //                         ProjectSkillId = ps.ProjectSkillId,
                    //                         ProjectName = proj.ProjectName,
                    //                         ProjectId = ps.ProjectId,
                    //                         CompetencyAreaId = ps.CompetencyAreaId,
                    //                         CompetencyAreaCode = comparea.CompetencyAreaCode,
                    //                         SkillId = ps.SkillId,
                    //                         SkillName = skill.SkillName,
                    //                         RoleId = r.RoleId,
                    //                         RoleName = r.RoleName,
                    //                         ProficiencyId = ps.ProficiencyId,
                    //                         ProficiencyLevelDescription = profLevel.ProficiencyLevelDescription,
                    //                         IsActive = ps.IsActive

                    //                     }).FirstOrDefault();


                    return projectSkills;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region DeleteProjectSkillByID
        /// <summary>
        /// Dletes the details of Project role skills by ProjectSkillId
        /// </summary>
        /// <returns></returns>
        /// 
        public bool DeleteProjectSkillByID(int ProjectSkillId)
        {
            bool returnValue = false;
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    ProjectSkill ps = hrmsEntity.ProjectSkills.Where(p => p.ProjectSkillId == ProjectSkillId).FirstOrDefault();

                    if (ps != null)
                    {
                        ps.IsActive = false;                       
                    }

                    hrmsEntity.Entry(ps).State = EntityState.Modified;
                    returnValue = hrmsEntity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to delete details");
            }
            return returnValue;
        }
        #endregion


        

        #region GetRolesByProjId
        /// <summary>
        /// Retrieves the details of roles of projectID
        /// </summary>
        /// <returns></returns>
        /// 
        public object GetRolesByProjId(int projectID)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {

                    var roles = hrmsEntity.Database.SqlQuery<string>
                                 ("[usp_GetRolesByProjectId] @ProjectId",
                                   new object[]  {
                                    new SqlParameter ("ProjectId", projectID),
                                  }
                                 ).Distinct().ToList();

                    return roles;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region AddUpdateProjectSkillsDetails
        /// <summary>
        /// Add and update project skills details
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        /// 
        public int AddUpdateProjectSkillsDetails(ProjectSkillsData projectSkillsData)
        {
          try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    //foreach (EmployeeSkillDetails employeeSkillDetails in projectSkillsData.skills)
                    //{
                        ProjectSkill psData = hrmsEntity.ProjectSkills.Where(ps => ps.ProjectSkillId == projectSkillsData.ProjectSkillId).FirstOrDefault();
                        if (psData == null)
                            psData = new ProjectSkill();
                        psData.ProficiencyId = projectSkillsData.ProficiencyId;
                        psData.ProjectId = projectSkillsData.ProjectId;
                        psData.SkillId = projectSkillsData.SkillId;
                        psData.CompetencyAreaId = projectSkillsData.CompetencyAreaId;
                        psData.IsActive = true;
                        psData.SystemInfo = projectSkillsData.SystemInfo;
                        psData.ProjectRoleId = (from p in hrmsEntity.ProjectRoles.Where(pr => pr.RoleMasterId == projectSkillsData.RoleMasterId && pr.ProjectId == projectSkillsData.ProjectId) select p.ProjectRoleId).FirstOrDefault();
                        if (projectSkillsData.ProjectSkillId == 0)
                        {
                            var IsExist = hrmsEntity.ProjectSkills.Where(ps => ps.ProjectId == projectSkillsData.ProjectId && ps.ProjectRoleId == psData.ProjectRoleId && ps.SkillId == projectSkillsData.SkillId && ps.IsActive == true).ToList().Count();
                            if (IsExist == 0)
                            {
                                psData.CreatedUser = projectSkillsData.CurrentUser;
                                psData.CreatedDate = DateTime.Now;
                                hrmsEntity.ProjectSkills.Add(psData);
                            }
                            else
                            {
                                var skillName = (from sn in hrmsEntity.Skills.Where(s => s.SkillId == projectSkillsData.SkillId) select sn.SkillName).FirstOrDefault();
                                throw new AssociatePortalException("Skill" + " " + skillName + " " + "already exist in project");
                            }
                        }
                        else
                        {
                            ProjectSkill psexists = hrmsEntity.ProjectSkills.Where(p => p.ProjectId == projectSkillsData.ProjectId && p.ProjectRoleId == psData.ProjectRoleId && p.SkillId == projectSkillsData.SkillId && p.IsActive == true && p.ProjectSkillId != projectSkillsData.ProjectSkillId).FirstOrDefault();
                            if (psexists != null)
                                throw new AssociatePortalException("This skill already mapped to project");
                            else
                            {
                                ProjectSkill projectSkill = hrmsEntity.ProjectSkills.Where(ps => ps.ProjectSkillId == projectSkillsData.ProjectSkillId).FirstOrDefault();
                                if (projectSkill != null)
                                {
                                    psData.ModifiedUser = projectSkillsData.CurrentUser;
                                    psData.ModifiedDate = DateTime.Now;
                                    hrmsEntity.Entry(psData).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                        }

                  //  }
                    var result = hrmsEntity.SaveChanges();
                    return result;

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save/update details");
            }

        }
        #endregion

    }
}
