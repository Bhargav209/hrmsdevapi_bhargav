﻿using System;
using System.Linq;
using System.Resources;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using Quartz;
using System.Web;

namespace AP.API
{
    class ProspectiveAssociateNotificationJob : IJob
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        //#region AddJob
        ///// <summary>
        ///// This method is written to explicitly call from UI for future use.
        ///// Add the Prospective Associate joining job to the scheduler
        ///// </summary>
        ///// <param name="jobId"></param>
        ///// <param name="groupId"></param>
        ///// <param name="interval"></param>
        ///// 
        //public bool AddJob(string jobId, string groupId)
        //{
        //    bool retValue = true;

        //    try
        //    {
        //        using (APEntities hrmEntities = new APEntities())
        //        {
        //            string prospectiveAssociateNotificationCode = resourceManager.GetString("ProspectiveAssociateNotificationCode");
        //            NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == prospectiveAssociateNotificationCode).FirstOrDefault();

        //            if (notificationConfiguration.SLA == 0) throw new AssociatePortalException("Employee Profile Creation Notification's SLA can not be zero.");

        //            retValue = NotificationScheduler.AddJob<ProspectiveAssociateNotificationJob>(jobId, groupId, notificationConfiguration.SLA.GetValueOrDefault());

        //            if (retValue)
        //            {
        //                // Insert record to the JobDetail table //
        //                //
        //                string started = resourceManager.GetString("Started");
        //                var jobExist = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId && j.Status == started).FirstOrDefault();

        //                // Insert record to the JobDetail table 
        //                //
        //                if (jobExist == null)
        //                {
        //                    JobDetail jobDetail = new JobDetail() { JobCode = jobId, GroupID = groupId, Status = started, JobInterval = notificationConfiguration.SLA };
        //                    jobDetail = hrmEntities.JobDetails.Add(jobDetail);
        //                }

        //                int paNotificationTypeID = Convert.ToInt32(resourceManager.GetString("ProspectiveAssociateNotificationTypeID"));
        //                int pending = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
        //                var notificationExist = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == paNotificationTypeID && n.StatusId == pending).FirstOrDefault();

        //                // Insert record in to Notification table  
        //                //
        //                if (notificationExist == null)
        //                {
        //                    Notification notification = new Notification() { EmployeeCode = jobId, NotificationTypeID = paNotificationTypeID, StatusId = pending };
        //                    notification = hrmEntities.Notifications.Add(notification);
        //                }

        //                hrmEntities.SaveChanges(); 
        //            }

        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return retValue;
        //}
        //#endregion

        #region Execute
        /// <summary>
        /// Execution of the job for sending email
        /// </summary>
        /// <param name="context"></param>
        /// 
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    string paNotificationCode = resourceManager.GetString("ProspectiveAssociateNotificationCode");
                    //NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == paNotificationCode).FirstOrDefault();

                    Email email = new Email();
                    //int notificationConfigID = new BaseEmail().BuildEmailObject(email, notificationConfiguration.emailTo, notificationConfiguration.emailFrom,
                    //                               notificationConfiguration.emailCC, notificationConfiguration.emailSubject, notificationConfiguration.emailContent + GetHtml(), Enumeration.NotificationStatus.PA.ToString());
                    //new BaseEmail().SendEmail(email, notificationConfigID);
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region GetHtml
        /// <summary>
        /// Method to create dynamic html content for PA notification email
        /// </summary>
        /// <returns></returns>
        /// 
        public string GetHtml()
        {
            try
            {
                string messageBody = "<br><br><br><font>The following are the records: </font><br><br>";
                string htmlTableStart = "<table style=\"border-collapse:collapse; text-align:center;\" >";
                string htmlTableEnd = "</table>";
                string htmlHeaderRowStart = "<tr style =\"background-color:#6FA1D2; color:#ffffff;\">";
                string htmlHeaderRowEnd = "</tr>";
                string htmlTrStart = "<tr style =\"color:#555555;\">";
                string htmlTrEnd = "</tr>";
                string htmlTdStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px;\">";
                string htmlTdEnd = "</td>";

                messageBody += htmlTableStart;
                messageBody += htmlHeaderRowStart;
                messageBody += htmlTdStart + "Name" + htmlTdEnd;
                messageBody += htmlTdStart + "Technology" + htmlTdEnd;
                messageBody += htmlTdStart + "Designation" + htmlTdEnd;
                messageBody += htmlTdStart + "Department" + htmlTdEnd;
                messageBody += htmlTdStart + "Joining Date" + htmlTdEnd;
                messageBody += htmlTdStart + "HR Advisor" + htmlTdEnd;
                messageBody += htmlHeaderRowEnd;

                var paList = new ProspectiveAssociateDetails().GetPADetailsForTheMonth();

                foreach (UserDetails user in paList)
                {
                    messageBody = messageBody + htmlTrStart;
                    messageBody = messageBody + htmlTdStart + user.name + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + user.technology + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + user.designation + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + user.department + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + user.dateOfJoining.GetValueOrDefault().ToString() + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + user.hrAdvisor + htmlTdEnd;
                    messageBody = messageBody + htmlTrEnd;
                }
                messageBody = messageBody + htmlTableEnd;

                return messageBody;
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
