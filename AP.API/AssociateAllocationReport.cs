﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data.SqlClient;
using System.Web.Hosting;
using System.Configuration;

namespace AP.API
{
    public class AssociateAllocationReport
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);
        string connString = System.Configuration.ConfigurationManager.ConnectionStrings["APConnection"].ConnectionString;

        #region Import excel data to the database
        /// <summary>
        /// Import Associate Allocation Report
        /// </summary>
        /// <returns></returns>
        public object ImportAssociateAllocationReport()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    DataSet reports = GetDataFromExcel();

                    // Map skills
                   // MapSkills(reports);

                    //Add employees
                    AddEmployeeForReport(reports);

                    //Add projects
                    AddProjectForReport(reports);

                    //Add Skills
                    AddSkills(reports);

                    //Add TalentRequisition Request
                    AddTalentRequisition(reports);
                    AddRequisitionRoleDetails(reports);
                    AddRequisitionRoleSkills(reports);

                    //Allocate associate
                    return AddAssociateAllocation(reports);
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Upload failed.");
            }
        }
        /// <summary>
        /// Get data from Excel
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private DataSet GetDataFromExcel()
        {
            DataSet reports = null;
            var httpRequest = HttpContext.Current.Request;
            string UplodedfilePath = Convert.ToString(ConfigurationManager.AppSettings["Uploadedfiles"]);
            try
            {
                if (httpRequest.Files.Count > 0)
                {
                    string extension = Path.GetExtension(httpRequest.Files[0].FileName);
                    var postedFile = httpRequest.Files[0];

                    string fileName = httpRequest.Form["documentName"].ToString().Split('.')[0];

                    bool isExists = Directory.Exists(HostingEnvironment.MapPath(UplodedfilePath));

                    if (!isExists)
                        Directory.CreateDirectory(HostingEnvironment.MapPath(UplodedfilePath));

                    //Whatever we want to give fileName and serverpath to save the file/files.

                    var path = HostingEnvironment.MapPath(UplodedfilePath + fileName + "_" + DateTime.Now.ToString("yyyyMMddhhmmss")) + extension;
                    postedFile.SaveAs(path);

                    FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    excelReader.IsFirstRowAsColumnNames = true;
                    reports = excelReader.AsDataSet();
                    int noofColumns = reports.Tables[0].Columns.Count;
                    // Add Columns
                    for (int i = 0; i < noofColumns; i++)
                    {
                        reports.Tables[0].Columns[i].ColumnName = reports.Tables[0].Columns[i].ColumnName.Replace(" ", "");
                        reports.Tables[0].AcceptChanges();
                    }
                    DataTable dt = reports.Tables[0];

                    //Remove empty rows
                    List<DataRow> emptyRows = (from row in dt.AsEnumerable()
                                               where row.Field<string>("AssociateID") == ""
                                               || row.Field<string>("AssociateID") == null
                                               || row.Field<string>("AssociateID").StartsWith("Associate")
                                               select row).ToList();

                    foreach (DataRow row in emptyRows)
                    {
                        dt.Rows.Remove(row);
                        dt.AcceptChanges();
                    }

                    //Add Id colomns
                    dt.Columns.Add("ProjectId", typeof(int));
                    dt.Columns.Add("ClientId", typeof(int));
                    dt.Columns.Add("ProjectTypeId", typeof(int));
                    dt.Columns.Add("RoleId", typeof(int));
                    dt.Columns.Add("CompetencyId", typeof(int));
                    dt.Columns.Add("EmployeeId", typeof(int));
                    dt.Columns.Add("PracticeAreaId", typeof(int));
                    dt.Columns.Add("ManagerId", typeof(int));
                    dt.Columns.Add("TRId", typeof(int));
                    dt.Columns.Add("RequisitionRoleDetailId", typeof(int));
                    dt.Columns.Add("GradeId", typeof(int));

                    using (SqlConnection con = new SqlConnection(connString))
                    {
                        //con.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        DataTable tempTable = new DataTable();
                        da.SelectCommand = cmd;

                        cmd.CommandText = "select UPPER(EmployeeCode) as EmployeeCode, EmployeeId, CONCAT(UPPER(FirstName), ' ', UPPER(LastName)) as EmployeeName from Employee";
                        reports.Tables.Add("Employees");
                        da.Fill(reports.Tables["Employees"]);

                        cmd.CommandText = "select ClientId, UPPER(ClientCode) as ClientCode, UPPER(ClientName) as ClientName from Clients";
                        reports.Tables.Add("Clients");
                        da.Fill(reports.Tables["Clients"]);

                        cmd.CommandText = "select ProjectTypeId, UPPER(Description) as Description from ProjectType";
                        reports.Tables.Add("ProjectTypes");
                        da.Fill(reports.Tables["ProjectTypes"]);

                        cmd.CommandText = "select RoleId, UPPER(RoleName) as RoleName from Roles";
                        reports.Tables.Add("Roles");
                        da.Fill(reports.Tables["Roles"]);

                        cmd.CommandText = "select CompetencyAreaId from CompetencyArea where UPPER(CompetencyAreaCode) = 'TECHNOLOGY'";
                        reports.Tables.Add("CompetencyArea");
                        da.Fill(reports.Tables["CompetencyArea"]);

                        cmd.CommandText = "select CompetencySkillsId,RoleId,CompetencyAreaId,SkillId,IsActive,IsPrimary from CompetencySkills";
                        reports.Tables.Add("CompetencySkills");
                        da.Fill(reports.Tables["CompetencySkills"]);

                        cmd.CommandText = "select SkillId,UPPER(SkillName) as SkillName from Skills";
                        reports.Tables.Add("Skills");
                        da.Fill(reports.Tables["Skills"]);

                        cmd.CommandText = "select PracticeAreaId, UPPER(PracticeAreaCode) as PracticeAreaCode from PracticeArea";
                        reports.Tables.Add("PracticeAreas");
                        da.Fill(reports.Tables["PracticeAreas"]);

                        cmd.CommandText = "select ID,EmployeeId,RoleId,CompetencyAreaId,SkillId,ProficiencyLevelId,IsPrimary,PracticeAreaId from EmployeeSkills";
                        reports.Tables.Add("EmployeeSkills");
                        da.Fill(reports.Tables["EmployeeSkills"]);

                        cmd.CommandText = "select ProjectId,EmployeeId,RoleId,ReportingManagerId,RequisitionRoleDetailID,AssociateAllocationId,TRId from AssociateAllocation";
                        reports.Tables.Add("AssociateAllocation");
                        da.Fill(reports.Tables["AssociateAllocation"]);

                        cmd.CommandText = "select ProjectId, UPPER(ProjectCode) as ProjectCode, UPPER(ProjectName) as ProjectName from Projects";
                        reports.Tables.Add("Projects");
                        da.Fill(reports.Tables["Projects"]);

                        cmd.CommandText = "select GradeId, UPPER(GradeCode) as GradeCode, UPPER(GradeName) as GradeName from Grades";
                        reports.Tables.Add("Grades");
                        da.Fill(reports.Tables["Grades"]);

                        int CompetencyAreaId = reports.Tables["CompetencyArea"].AsEnumerable().Select(pa => pa.Field<int>("CompetencyAreaId")).FirstOrDefault();

                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["EmployeeId"] = reports.Tables["Employees"].AsEnumerable().Where(e => e.Field<string>("EmployeeCode") == dr["AssociateID"].ToString().Trim().ToUpper()).Select(e => e.Field<int>("EmployeeId")).FirstOrDefault();
                            dr["ManagerId"] = reports.Tables["Employees"].AsEnumerable().Where(e => e.Field<string>("EmployeeCode") == dr["ReportingManagerCode"].ToString().Trim().ToUpper()).Select(e => e.Field<int>("EmployeeId")).FirstOrDefault();
                            dr["ClientId"] = reports.Tables["Clients"].AsEnumerable().Where(c => c.Field<string>("ClientName") == dr["Client"].ToString().Trim().ToUpper()).Select(c => c.Field<int>("ClientId")).FirstOrDefault();
                            dr["ProjectTypeId"] = reports.Tables["ProjectTypes"].AsEnumerable().Where(p => p.Field<string>("Description") == dr["ProjectGroup"].ToString().Trim().ToUpper()).Select(p => p.Field<int>("ProjectTypeId")).FirstOrDefault();
                            dr["RoleId"] = reports.Tables["Roles"].AsEnumerable().Where(r => r.Field<string>("RoleName") == dr["Role"].ToString().Trim().ToUpper()).Select(r => r.Field<int>("RoleId")).FirstOrDefault();
                            dr["PracticeAreaId"] = reports.Tables["PracticeAreas"].AsEnumerable().Where(pa => pa.Field<string>("PracticeAreaCode") == dr["PracticeGroup"].ToString().Trim().ToUpper()).Select(pa => pa.Field<int>("PracticeAreaId")).FirstOrDefault();
                            dr["CompetencyId"] = CompetencyAreaId;
                            dr["ProjectId"] = reports.Tables["Projects"].AsEnumerable().Where(pa => pa.Field<string>("ProjectCode") == dr["ProjectCode"].ToString().Trim().ToUpper()).Select(pa => pa.Field<int>("ProjectId")).FirstOrDefault();
                            dr["GradeId"] = reports.Tables["Grades"].AsEnumerable().Where(pa => pa.Field<string>("GradeCode") == dr["Grade"].ToString().Trim().ToUpper()).Select(pa => pa.Field<int>("GradeId")).FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return reports;
        }
        /// <summary>
        /// Add/Update employees from reports
        /// </summary>
        /// <param name="dsReport"></param>
        private void AddEmployeeForReport(DataSet dsReport)
        {
            DataTable empTable = new DataTable();
            empTable.Columns.Add("FirstName");
            empTable.Columns.Add("LastName");
            empTable.Columns.Add("EmployeeTypeId", typeof(int));
            empTable.Columns.Add("EmployeeCode");
            empTable.Columns.Add("IsActive");
            empTable.Columns.Add("StatusId", typeof(int));
            empTable.Columns.Add("Experience");
            empTable.Columns.Add("CreatedDate");
            empTable.Columns.Add("CreatedUser");
            empTable.Columns.Add("SystemInfo");
            empTable.Columns.Add("GradeId");
            empTable.Columns.Add("JoinDate");
            Nullable<DateTime> myjoindate= new DateTime?();
            foreach (DataRow dr in dsReport.Tables["Master Sheet"].Rows)
            {
                try
                {
                    if (dr["EmployeeId"].ToString() == "0")
                    {
                        if (dr["DateOfJoining(DD-MM-YYYY)"].ToString() != "")
                        {
                            DataRow drEmp = empTable.NewRow();
                            drEmp["FirstName"] = dr["AssociateName"].ToString().Split(' ')[0];
                            drEmp["LastName"] = dr["AssociateName"].ToString().Split(' ').Length < 1 ? "" : dr["AssociateName"].ToString().Split(' ')[1];
                            drEmp["EmployeeTypeId"] = 1;
                            drEmp["EmployeeCode"] = dr["AssociateID"].ToString();
                            //emp.BloogGroup = "O+";
                            drEmp["IsActive"] = true;
                            drEmp["StatusId"] = 8;
                            drEmp["Experience"] = dr["TotalExperience"].ToString() == null ? 0 : Convert.ToDecimal(dr["TotalExperience"].ToString());
                            drEmp["CreatedDate"] = DateTime.Now;
                            drEmp["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                            drEmp["JoinDate"] = dr["DateOfJoining(DD-MM-YYYY)"].ToString() == "" ? myjoindate : Convert.ToDateTime(dr["DateOfJoining(DD-MM-YYYY)"]);
                            drEmp["SystemInfo"] = Commons.GetClientIPAddress();
                            drEmp["GradeId"] = Convert.ToInt32(dr["GradeId"].ToString());
                            empTable.Rows.Add(drEmp);
                        }
                        else
                        {
                            string errMessage = "Date of joining information is not provided";                          
                            string parms = new StringBuilder().AppendFormat("[AssociateUtility.AddEmployeeForReport : EmployeeCode - {0},Message - {1}]", dr["AssociateID"].ToString(), errMessage).ToString();
                            Log.Write(resourceManager.GetString("ListOfNotInsertedEmployees"), Log.Severity.Warning, parms);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message == "String was not recognized as a valid DateTime.")
                    {
                        string errMessage = "Date of joining format should be DD-MM-YYYY";
                        string parms = new StringBuilder().AppendFormat("[AssociateUtility.AddEmployeeForReport : EmployeeCode - {0}, DateOfJoining(DD-MM-YYYY) -{1},Message - {2}]", dr["AssociateID"].ToString(), dr["DateOfJoining(DD-MM-YYYY)"].ToString(), errMessage).ToString();
                        Log.Write(resourceManager.GetString("ListOfNotInsertedEmployees"), Log.Severity.Error, parms);
                    }
                    else
                    {
                        Log.LogError(ex, Log.Severity.Error, "");
                    }
                }
            }

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {
                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[Employee]";
                        bulkCopy.ColumnMappings.Add("EmployeeCode", "EmployeeCode");
                        bulkCopy.ColumnMappings.Add("FirstName", "FirstName");
                        bulkCopy.ColumnMappings.Add("LastName", "LastName");
                        bulkCopy.ColumnMappings.Add("EmployeeTypeId", "EmployeeTypeId");
                        bulkCopy.ColumnMappings.Add("IsActive", "IsActive");
                        bulkCopy.ColumnMappings.Add("StatusId", "StatusId");
                        bulkCopy.ColumnMappings.Add("Experience", "Experience");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");
                        bulkCopy.ColumnMappings.Add("GradeId", "GradeId");
                        bulkCopy.ColumnMappings.Add("JoinDate", "JoinDate");

                        bulkCopy.WriteToServer(empTable);
                    }

                    tran.Commit();

                    SqlDataAdapter da = new SqlDataAdapter();
                    string query = "select UPPER(EmployeeCode) as EmployeeCode, EmployeeId, CONCAT(UPPER(FirstName), ' ', UPPER(LastName)) as EmployeeName from Employee";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    da.SelectCommand = cmd;
                    da.Fill(dsReport.Tables["Employees"]);
                }
            }

            foreach (DataRow dr in dsReport.Tables["Master Sheet"].Rows)
            {
                dr["EmployeeId"] = dsReport.Tables["Employees"].AsEnumerable().Where(e => e.Field<string>("EmployeeCode") == dr["AssociateID"].ToString().Trim().ToUpper()).Select(e => e.Field<int>("EmployeeId")).FirstOrDefault();
                dr["ManagerId"] = dsReport.Tables["Employees"].AsEnumerable().Where(e => e.Field<string>("EmployeeCode") == dr["ReportingManagerCode"].ToString().Trim().ToUpper()).Select(e => e.Field<int>("EmployeeId")).FirstOrDefault();
            }

            //Updtae ManagerId for Associates
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                conn.Open();

                foreach (DataRow dr in dsReport.Tables["Master Sheet"].Rows)
                {
                    if ((int)dr["ManagerId"] != 0)
                    {
                        string query = "update Employee set ReportingManager = " + dr["ManagerId"] + " where EmployeeId = " + dr["EmployeeId"];
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                    }                    
                }
            }
        }
        /// <summary>
        /// Add/Update projects from reports
        /// </summary>
        /// <param name="dsReport"></param>
        private void AddProjectForReport(DataSet dsReport)
        {
            var drList = (from dr in dsReport.Tables["Master Sheet"].AsEnumerable()
                          select new
                          {
                              ProjectId = dr.Field<int>("ProjectId"),
                              ProjectCode = (dr.Field<string>("ProjectCode"))
                          }).Distinct().ToList();

            DataTable dtProject = new DataTable();
            dtProject.Columns.Add("ProjectName");
            dtProject.Columns.Add("ProjectCode");
            dtProject.Columns.Add("LeadId");
            dtProject.Columns.Add("ManagerId");
            dtProject.Columns.Add("IsActive");
            dtProject.Columns.Add("ProjectTypeId", typeof(int));
            dtProject.Columns.Add("ClientId", typeof(int));
            dtProject.Columns.Add("CreatedDate");
            dtProject.Columns.Add("CreatedUser");
            dtProject.Columns.Add("SystemInfo");

            foreach (var dr in drList)
            {
                try
                {
                    var projectDetail = (from p in dsReport.Tables["Master Sheet"].AsEnumerable()
                                         where p.Field<string>("ProjectCode") == dr.ProjectCode
                                         select new
                                         {
                                             ProjectGroupId = p.Field<int>("ProjectTypeId"),
                                             ClientId = p.Field<int>("ClientId"),
                                             ProjectName = p.Field<string>("ProjectName"),
                                         }).FirstOrDefault();

                    var managers = (from p in dsReport.Tables["Master Sheet"].AsEnumerable()
                                    where p.Field<string>("ProjectCode") == dr.ProjectCode && (p.Field<string>("Role").Trim().ToUpper().Contains("LEAD") || p.Field<string>("Role").Trim().ToUpper().Contains("HEAD"))
                                    select new
                                    {
                                        ManagerId = p.Field<int>("ManagerId"),
                                        TLId = p.Field<int>("EmployeeId")
                                    }).FirstOrDefault();
                    
                    if (dr.ProjectId == 0 && dr.ProjectCode != null && projectDetail.ClientId != 0 && projectDetail.ProjectGroupId != 0 && managers != null && managers.ManagerId != 0 && managers.TLId != 0)
                    {
                        DataRow drProject = dtProject.NewRow();
                        drProject["ProjectName"] = projectDetail.ProjectName.Trim();
                        drProject["ProjectCode"] = dr.ProjectCode.Trim();
                        drProject["ProjectTypeId"] = projectDetail.ProjectGroupId;
                        drProject["ClientId"] = projectDetail.ClientId;
                        drProject["ManagerId"] = managers.ManagerId;
                        drProject["LeadId"] = managers.TLId;
                        drProject["IsActive"] = true;
                        drProject["CreatedDate"] = DateTime.Now;
                        drProject["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                        drProject["SystemInfo"] = Commons.GetClientIPAddress();
                        dtProject.Rows.Add(drProject);
                    }
                    else
                    {                   
                        string errMessage = "Project or Client or ProjectType are not registered";
                        if (managers == null) { errMessage = errMessage + "; as no manager is there for the Project"; }
                        else
                        {
                            if (managers.ManagerId == 0) { errMessage = errMessage + "; as no manager is there for the Project"; }
                            if (managers.TLId == 0) { errMessage = errMessage + "; as no manager is there for the Project"; }
                        }
                        if (projectDetail.ClientId == 0) { errMessage = errMessage + "; as no client is there for the Project"; }
                        if (projectDetail.ProjectGroupId == 0) { errMessage = errMessage + "; as no ProjectGroupId is there for the Project"; }
                        
                        string parms = new StringBuilder().AppendFormat("[AssociateUtility.AddProjectForReport : ProjectCode - {0},Client - {1},ProjectName - {2},Message - {3}]", dr.ProjectCode, projectDetail.ClientId, projectDetail.ProjectName, errMessage).ToString();
                        Log.Write(resourceManager.GetString("ListOfNotInsertedProjects"), Log.Severity.Warning, parms);
                    }
                }
                catch (Exception ex)
                {
                    Log.LogError(ex, Log.Severity.Error, "");
                }
            }

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {
                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[Projects]";
                        bulkCopy.ColumnMappings.Add("ProjectName", "ProjectName");
                        bulkCopy.ColumnMappings.Add("ProjectCode", "ProjectCode");
                        bulkCopy.ColumnMappings.Add("ProjectTypeId", "ProjectTypeId");
                        bulkCopy.ColumnMappings.Add("ClientId", "ClientId");
                        bulkCopy.ColumnMappings.Add("LeadId", "LeadId");
                        bulkCopy.ColumnMappings.Add("ManagerId", "ManagerId");
                        bulkCopy.ColumnMappings.Add("IsActive", "IsActive");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");

                        bulkCopy.WriteToServer(dtProject);
                    }

                    tran.Commit();

                    SqlDataAdapter da = new SqlDataAdapter();
                    string query = "select ProjectId, UPPER(ProjectCode) as ProjectCode, UPPER(ProjectName) as ProjectName from Projects";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    da.SelectCommand = cmd;
                    da.Fill(dsReport.Tables["Projects"]);
                }
            }

            foreach (DataRow dr in dsReport.Tables["Master Sheet"].Rows)
            {
                dr["ProjectId"] = dsReport.Tables["Projects"].AsEnumerable().Where(pa => pa.Field<string>("ProjectCode") == dr["ProjectCode"].ToString().Trim().ToUpper()).Select(pa => pa.Field<int>("ProjectId")).FirstOrDefault();
            }
        }
        /// <summary>
        /// Mapping skill and competenct with role
        /// </summary>
        /// <param name="dsReport"></param>
        private void MapSkills(DataSet dsReport)
        {
            DataTable CompetencySkillstable = new DataTable();
            CompetencySkillstable.Columns.Add("RoleId", typeof(int));
            CompetencySkillstable.Columns.Add("CompetencyAreaId", typeof(int));
            CompetencySkillstable.Columns.Add("SkillId", typeof(int));
            CompetencySkillstable.Columns.Add("IsActive");
            CompetencySkillstable.Columns.Add("IsPrimary");
            CompetencySkillstable.Columns.Add("ProficiencyLevelId", typeof(int));
            CompetencySkillstable.Columns.Add("SystemInfo");
            CompetencySkillstable.Columns.Add("CreatedUser");
            CompetencySkillstable.Columns.Add("CreatedDate");
            int ProficiencyLevelId = 1;
            var drList = (from dr in dsReport.Tables[0].AsEnumerable()
                          select new
                          {
                              PrimarySkill = dr.Field<string>("PrimarySkill").Trim().ToUpper(),
                              Role = dr.Field<string>("Role").Trim().ToUpper(),
                              RoleId = dr.Field<int>("RoleId"),
                              CompetencyId = dr.Field<int>("CompetencyID")
                          }).Distinct().ToList();

            foreach (var dr in drList)
            {
                try
                {
                    List<int> Skills = new List<int>();
                    string[] primarySkill = dr.PrimarySkill == null || dr.PrimarySkill == "" ? new string[] { } : dr.PrimarySkill.ToString().Split(',');
                    for (int i = 0; i < primarySkill.Length; i++)
                    {
                        primarySkill[i] = primarySkill[i].Trim();
                        int SkillID = dsReport.Tables["Skills"].AsEnumerable().Where(s => s.Field<string>("SkillName").Trim().ToUpper() == primarySkill[i].Trim().ToUpper()).Select(e => e.Field<int>("SkillID")).FirstOrDefault();
                        Skills.Add(SkillID);
                    }
                    string roleName = dr.Role == null || dr.Role == "" ? "" : dr.Role;
                    if (dr.RoleId != 0 && dr.CompetencyId != 0 && Skills.Count > 0)
                    {
                        foreach (int skillId in Skills)
                        {
                            //Checking Role,Competency area,Skill combination in Database
                            int skillexistsinDB = dsReport.Tables["CompetencySkills"].AsEnumerable().Where(e => e.Field<int>("RoleId") == dr.RoleId && e.Field<int>("CompetencyAreaId") == dr.CompetencyId && e.Field<int>("SkillId") == skillId).Count();
                            //Checking Role,Competency area,Skill combination in report data
                            int skillexists = CompetencySkillstable.AsEnumerable().Where(e => e.Field<int>("RoleId") == dr.RoleId && e.Field<int>("CompetencyAreaId") == dr.CompetencyId && e.Field<int>("SkillId") == skillId).Count();
                            if (skillexists == 0 && skillexistsinDB == 0 && skillId != 0)
                            {
                                DataRow drcompetencySkills = CompetencySkillstable.NewRow();
                                drcompetencySkills["RoleId"] = dr.RoleId;
                                drcompetencySkills["CompetencyAreaId"] = dr.CompetencyId;
                                drcompetencySkills["SkillId"] = skillId;
                                drcompetencySkills["IsActive"] = true;
                                drcompetencySkills["IsPrimary"] = true;
                                drcompetencySkills["ProficiencyLevelId"] = ProficiencyLevelId;
                                drcompetencySkills["CreatedDate"] = DateTime.Now;
                                drcompetencySkills["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                                drcompetencySkills["SystemInfo"] = Commons.GetClientIPAddress();
                                CompetencySkillstable.Rows.Add(drcompetencySkills);
                            }
                        }
                    }
                    else
                    {
                        string errMessage = "Role or CompetencyArea or Skills are not registered";
                        string parms = new StringBuilder().AppendFormat("[MapSkills: primarySkill - {0},roleName - {1}],Message - {2}]", dr.PrimarySkill, roleName, errMessage).ToString();
                        Log.Write(resourceManager.GetString("ListOfNotMappedSkills"), Log.Severity.Warning, parms);
                    }
                }
                catch (Exception ex)
                {
                    Log.LogError(ex, Log.Severity.Error, "");
                }
            }
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {

                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {

                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[CompetencySkills]";
                        bulkCopy.ColumnMappings.Add("RoleId", "RoleId");
                        bulkCopy.ColumnMappings.Add("CompetencyAreaId", "CompetencyAreaId");
                        bulkCopy.ColumnMappings.Add("ProficiencyLevelId", "ProficiencyLevelId");
                        bulkCopy.ColumnMappings.Add("SkillId", "SkillId");
                        bulkCopy.ColumnMappings.Add("IsPrimary", "IsPrimary");
                        bulkCopy.ColumnMappings.Add("IsActive", "IsActive");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");
                        bulkCopy.WriteToServer(CompetencySkillstable);
                    }
                    tran.Commit();

                }
            }

        }
        /// <summary>
        /// Add Skills for associate
        /// </summary>
        /// <param name="dsReport"></param>
        private void AddSkills(DataSet dsReport)
        {
            DataTable EmployeeSkillstable = new DataTable();
            EmployeeSkillstable.Columns.Add("EmployeeId", typeof(int));
            EmployeeSkillstable.Columns.Add("RoleId", typeof(int));
            EmployeeSkillstable.Columns.Add("CompetencyAreaId", typeof(int));
            EmployeeSkillstable.Columns.Add("SkillId", typeof(int));
            EmployeeSkillstable.Columns.Add("ProficiencyLevelId", typeof(int));
            EmployeeSkillstable.Columns.Add("PracticeAreaId", typeof(int));
            EmployeeSkillstable.Columns.Add("Experience");
            EmployeeSkillstable.Columns.Add("IsActive");
            EmployeeSkillstable.Columns.Add("IsPrimary");
            EmployeeSkillstable.Columns.Add("SystemInfo");
            EmployeeSkillstable.Columns.Add("CreatedUser");
            EmployeeSkillstable.Columns.Add("CreatedDate");
            int ProficiencyLevelId = 1;
            var drList = (from dr in dsReport.Tables[0].AsEnumerable()
                          select new
                          {
                              AssociateID = dr.Field<string>("AssociateID"),
                              EmployeeId = dr.Field<int>("EmployeeId"),
                              PrimarySkill = dr.Field<string>("PrimarySkill").Trim().ToUpper(),
                              RoleId = dr.Field<int>("RoleId"),
                              PracticeAreaId = dr.Field<int>("PracticeAreaId"),
                              CompetencyId = dr.Field<int>("CompetencyID"),
                              TotalExperience = dr["TotalExperience"] == null ? 0 : Convert.ToDecimal(dr["TotalExperience"].ToString())
                          }).Distinct().ToList();

            foreach (var drempskills in drList)
            {
                try
                {                    
                    List<int> Skills = new List<int>();
                    string[] primarySkill = drempskills.PrimarySkill == null || drempskills.PrimarySkill == "" ? new string[] { } : drempskills.PrimarySkill.ToString().Split(',');
                    for (int i = 0; i < primarySkill.Length; i++)
                    {
                        primarySkill[i] = primarySkill[i].Trim();
                        int SkillID = dsReport.Tables["Skills"].AsEnumerable().Where(s => s.Field<string>("SkillName").Trim().ToUpper() == primarySkill[i].Trim().ToUpper()).Select(e => e.Field<int>("SkillID")).FirstOrDefault();
                        Skills.Add(SkillID);
                    }
                    if (drempskills.EmployeeId != 0 && drempskills.RoleId != 0 && drempskills.CompetencyId != 0 && Skills.Count > 0)
                    {
                        foreach (int skillId in Skills)
                        {
                            //Checking EmployeeID,Role,Competency area,Skill combination in Database
                            int EmpskillexistsinDB = dsReport.Tables["EmployeeSkills"].AsEnumerable().Where(e => e.Field<int>("EmployeeId") == drempskills.EmployeeId && e.Field<int>("RoleId") == drempskills.RoleId && e.Field<int>("CompetencyAreaId") == drempskills.CompetencyId && e.Field<int>("SkillId") == skillId).Count();
                            //Checking EmployeeID,Role,Competency area,Skill combination in report data
                            int Empskillexists = EmployeeSkillstable.AsEnumerable().Where(e => e.Field<int>("EmployeeId") == drempskills.EmployeeId && e.Field<int>("RoleId") == drempskills.RoleId && e.Field<int>("CompetencyAreaId") == drempskills.CompetencyId && e.Field<int>("SkillId") == skillId).Count();
                            if (EmpskillexistsinDB == 0 && Empskillexists == 0 && skillId != 0)
                            {
                                DataRow drskills = EmployeeSkillstable.NewRow();
                                drskills["EmployeeId"] = drempskills.EmployeeId;
                                drskills["RoleId"] = drempskills.RoleId;
                                drskills["CompetencyAreaId"] = drempskills.CompetencyId;
                                drskills["SkillId"] = skillId;
                                drskills["PracticeAreaId"] = drempskills.PracticeAreaId;
                                drskills["ProficiencyLevelId"] = ProficiencyLevelId;
                                drskills["Experience"] = Convert.ToInt32(drempskills.TotalExperience);
                                drskills["IsActive"] = true;
                                drskills["IsPrimary"] = true;
                                drskills["CreatedDate"] = DateTime.Now;
                                drskills["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                                drskills["SystemInfo"] = Commons.GetClientIPAddress();
                                EmployeeSkillstable.Rows.Add(drskills);
                            }                            

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.LogError(ex, Log.Severity.Error, "");
                }
            }



            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {

                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {

                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[EmployeeSkills]";
                        bulkCopy.ColumnMappings.Add("EmployeeId", "EmployeeId");
                        bulkCopy.ColumnMappings.Add("RoleId", "RoleId");
                        bulkCopy.ColumnMappings.Add("SkillId", "SkillId");
                        bulkCopy.ColumnMappings.Add("CompetencyAreaId", "CompetencyAreaId");
                        bulkCopy.ColumnMappings.Add("ProficiencyLevelId", "ProficiencyLevelId");
                        bulkCopy.ColumnMappings.Add("PracticeAreaId", "PracticeAreaId");
                        bulkCopy.ColumnMappings.Add("Experience", "Experience");
                        bulkCopy.ColumnMappings.Add("IsPrimary", "IsPrimary");
                        bulkCopy.ColumnMappings.Add("IsActive", "IsActive");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");
                        bulkCopy.WriteToServer(EmployeeSkillstable);
                    }
                    tran.Commit();

                }
            }
        }
        /// <summary>
        /// Allocating associate to project
        /// </summary>
        /// <param name="dsReport"></param>
        private object AddAssociateAllocation(DataSet dsReport)
        {
            List<object> errList = new List<object>();

            DataTable allocationTable = new DataTable();
            allocationTable.Columns.Add("TRId", typeof(int));
            allocationTable.Columns.Add("ProjectId", typeof(int));
            allocationTable.Columns.Add("EmployeeId", typeof(int));
            allocationTable.Columns.Add("RoleId", typeof(int));
            allocationTable.Columns.Add("AllocationPercentage");
            allocationTable.Columns.Add("BillablePecentage");
            allocationTable.Columns.Add("IsCritical");
            allocationTable.Columns.Add("IsBillable");
            allocationTable.Columns.Add("EffectiveDate");
            allocationTable.Columns.Add("AllocationDate");
            allocationTable.Columns.Add("ReportingManagerId", typeof(int));
            allocationTable.Columns.Add("RequisitionRoleDetailID", typeof(int));
            allocationTable.Columns.Add("IsActive");
            allocationTable.Columns.Add("CreateDate");
            allocationTable.Columns.Add("CreatedBy");
            allocationTable.Columns.Add("SystemInfo");
            try
            {
                DateTime defDate = DateTime.Today;
                double pu;
                foreach (DataRow dr in dsReport.Tables[0].Rows)
                {
                    if (dr.Field<int>("TRId") != 0 && dr.Field<int>("ProjectId") != 0 && dr.Field<int>("EmployeeId") != 0 && dr.Field<int>("RoleId") != 0 && dr.Field<int>("ManagerId") != 0 && dr.Field<int>("RequisitionRoleDetailId") != 0)
                    {
                        //checking whether the employee already allocated to that project or not
                        int empexistsinDB = dsReport.Tables["AssociateAllocation"].AsEnumerable().Where(e => e.Field<int>("EmployeeId") == dr.Field<int>("EmployeeId") && e.Field<int>("RoleId") == dr.Field<int>("RoleId") && e.Field<int>("ProjectId") == dr.Field<int>("ProjectId")).Count();
                        if (empexistsinDB == 0)
                        {
                            DataRow drallocation = allocationTable.NewRow();
                            drallocation["TRId"] = dr.Field<int>("TRId");
                            drallocation["ProjectId"] = dr.Field<int>("ProjectId");
                            drallocation["EmployeeId"] = dr.Field<int>("EmployeeId");
                            drallocation["RoleId"] = dr.Field<int>("RoleId");
                            drallocation["AllocationPercentage"] = double.TryParse(dr["%Utilization"].ToString(), out pu) ? Convert.ToInt32(pu * 100) : 0;
                            if (dr.Field<string>("Billing").Equals("Billable"))
                            {
                                drallocation["BillablePecentage"] = drallocation["AllocationPercentage"];
                                drallocation["IsBillable"] = true;
                                drallocation["IsCritical"] = true;
                            }
                            else {
                                drallocation["BillablePecentage"] = 0;
                                drallocation["IsBillable"] = false;
                                drallocation["IsCritical"] = false;
                            }                       
                            //drallocation["IsCritical"] = dr["RoleCritical"].ToString() == "Critical-YES" ? true : false;                            
                            drallocation["EffectiveDate"] = DateTime.TryParse(dr["DateofAllocation"].ToString().Trim(), out defDate) ? defDate : DateTime.Today;
                            drallocation["AllocationDate"] = DateTime.TryParse(dr["DateofAllocation"].ToString().Trim(), out defDate) ? defDate : DateTime.Today;
                            drallocation["ReportingManagerId"] = dr.Field<int>("ManagerId");
                            drallocation["RequisitionRoleDetailID"] = dr.Field<int>("RequisitionRoleDetailId");
                            drallocation["IsActive"] = true;
                            drallocation["CreateDate"] = DateTime.Now;
                            drallocation["CreatedBy"] = HttpContext.Current.User.Identity.Name;
                            drallocation["SystemInfo"] = Commons.GetClientIPAddress();
                            allocationTable.Rows.Add(drallocation);
                        }
                    }
                    else
                    {
                        string errMessage = string.Empty;

                        if (dr.Field<int>("ProjectId") == 0)
                        {
                            errMessage = "Project " + dr["ProjectCode"].ToString() + " is not registered; ";
                        }
                        if (dr.Field<int>("EmployeeId") == 0)
                        {
                            errMessage = errMessage + "Employee " + dr["AssociateID"].ToString() + "is not registered; ";
                        }
                        if (dr.Field<int>("RoleId") == 0)
                        {
                            errMessage = errMessage + "Role " + dr["Role"].ToString() + " is not registered; ";
                        }
                        if (dr.Field<int>("ManagerId") == 0)
                        {
                            errMessage = errMessage + "Manager " + dr["ReportingManagerCode"].ToString() + " is not registered; ";
                        }
                        if (dr.Field<int>("ClientId") == 0)
                        {
                            errMessage = errMessage + "Client " + dr["Client"].ToString() + " is not registered; ";
                        }

                        var obj = new
                        {
                            AssociateCode = dr["AssociateID"].ToString().Trim().ToUpper(),
                            ProjectCode = dr["ProjectCode"].ToString().Trim().ToUpper(),
                            AssociateRole = dr["Role"].ToString().Trim().ToUpper(),
                            ReportingManager = dr["ReportingManagerCode"].ToString().Trim().ToUpper(),
                            ErrorMessage = errMessage
                        };

                        errList.Add(obj);
                        string parms = new StringBuilder().AppendFormat("[AssociateUtilize: EmployeeId - {0},ProjectId - {1},Role - {2},ManagerId - {3},Error - {4}]", obj.AssociateCode, obj.ProjectCode, obj.AssociateRole, obj.ReportingManager, obj.ErrorMessage).ToString();
                        Log.Write(resourceManager.GetString("ListOfNotInsertedAssociateUtilization"), Log.Severity.Warning, parms);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
            }


            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {

                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {

                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[AssociateAllocation]";
                        bulkCopy.ColumnMappings.Add("TRId", "TRId");
                        bulkCopy.ColumnMappings.Add("ProjectId", "ProjectId");
                        bulkCopy.ColumnMappings.Add("EmployeeId", "EmployeeId");
                        bulkCopy.ColumnMappings.Add("RoleId", "RoleId");
                        bulkCopy.ColumnMappings.Add("AllocationPercentage", "AllocationPercentage");
                        bulkCopy.ColumnMappings.Add("BillablePecentage", "BillablePecentage");
                        bulkCopy.ColumnMappings.Add("IsCritical", "IsCritical");
                        bulkCopy.ColumnMappings.Add("IsBillable","IsBillable");
                        bulkCopy.ColumnMappings.Add("EffectiveDate", "EffectiveDate");
                        bulkCopy.ColumnMappings.Add("AllocationDate", "AllocationDate");
                        bulkCopy.ColumnMappings.Add("ReportingManagerId", "ReportingManagerId");
                        bulkCopy.ColumnMappings.Add("RequisitionRoleDetailID", "RequisitionRoleDetailID");
                        bulkCopy.ColumnMappings.Add("IsActive", "IsActive");
                        bulkCopy.ColumnMappings.Add("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add("CreatedBy", "CreatedBy");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");
                        bulkCopy.WriteToServer(allocationTable);
                    }
                    tran.Commit();

                }
            }

            var errorReport = new
            {
                reportCount = allocationTable.Rows.Count,
                errList = errList
            };
            return errorReport;
        }

        /// <summary>
        /// Raising requisition for allocating associate to project
        /// </summary>
        /// <param name="dsReport"></param>
        private void AddTalentRequisition(DataSet dsReport)
        {
            var drList = (from dr in dsReport.Tables["Master Sheet"].AsEnumerable()
                          where dr.Field<int>("ProjectId") != 0
                          select new
                          {
                              ProjectId = dr.Field<int>("ProjectId"),
                              ProjectGroupId = dr.Field<int>("ProjectTypeId")
                          }).Distinct().ToList();

            DataTable dtTR = new DataTable();
            dtTR.Columns.Add("ProjectId", typeof(int));
            dtTR.Columns.Add("ProjectTypeId", typeof(int));
            dtTR.Columns.Add("StatusId", typeof(int));
            dtTR.Columns.Add("DepartmentId", typeof(int));
            dtTR.Columns.Add("IsActive");
            dtTR.Columns.Add("RequestedDate");
            dtTR.Columns.Add("CreatedDate");
            dtTR.Columns.Add("CreatedUser");
            dtTR.Columns.Add("SystemInfo");
            dtTR.Columns.Add("BulkInsertSession");

            long bulkInsertSession = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmss"));

            foreach (var dr in drList)
            {
                DataRow drTR = dtTR.NewRow();
                drTR["ProjectId"] = dr.ProjectId;
                drTR["ProjectTypeId"] = dr.ProjectGroupId;
                drTR["StatusId"] = 47;
                drTR["DepartmentId"] = 9;
                drTR["IsActive"] = true;
                drTR["RequestedDate"] = DateTime.Today;
                drTR["CreatedDate"] = DateTime.Now;
                drTR["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                drTR["SystemInfo"] = Commons.GetClientIPAddress();
                drTR["BulkInsertSession"] = bulkInsertSession;
                dtTR.Rows.Add(drTR);
            }

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {
                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[TalentRequisition]";
                        bulkCopy.ColumnMappings.Add("ProjectId", "ProjectId");
                        bulkCopy.ColumnMappings.Add("ProjectTypeId", "ProjectTypeId");
                        bulkCopy.ColumnMappings.Add("StatusId", "StatusId");
                        bulkCopy.ColumnMappings.Add("DepartmentId", "DepartmentId");
                        bulkCopy.ColumnMappings.Add("IsActive", "IsActive");
                        bulkCopy.ColumnMappings.Add("RequestedDate", "RequestedDate");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");
                        bulkCopy.ColumnMappings.Add("BulkInsertSession", "BulkInsertSession");

                        bulkCopy.WriteToServer(dtTR);
                    }

                    tran.Commit();

                    SqlDataAdapter da = new SqlDataAdapter();
                    string query = "select * from TalentRequisition where BulkInsertSession = " + bulkInsertSession;
                    SqlCommand cmd = new SqlCommand(query, conn);
                    da.SelectCommand = cmd;
                    dsReport.Tables.Add("TalentRequisition");
                    da.Fill(dsReport.Tables["TalentRequisition"]);
                }

                foreach (DataRow dr in dsReport.Tables["Master Sheet"].Rows)
                {
                    dr["TRId"] = dsReport.Tables["TalentRequisition"].AsEnumerable().Where(pa => pa.Field<int>("ProjectId") == (int)dr["ProjectId"] && pa.Field<int>("ProjectTypeId") == (int)dr["ProjectTypeId"]).Select(pa => pa.Field<int>("TRId")).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Adding role details for requisition from report
        /// </summary>
        /// <param name="dsReport"></param>
        private void AddRequisitionRoleDetails(DataSet dsReport)
        {
            var drList = (from dr in dsReport.Tables["Master Sheet"].AsEnumerable()
                          where dr.Field<int>("ProjectId") != 0 && dr.Field<int>("ProjectTypeId") != 0 && dr.Field<int>("RoleId") != 0
                          select new
                          {
                              ProjectId = dr.Field<int>("ProjectId"),
                              ProjectGroupId = dr.Field<int>("ProjectTypeId"),
                              RoleId = dr.Field<int>("RoleId"),
                              TRId = dr.Field<int>("TRId")
                          }).Distinct().ToList();

            DataTable dtRRD = new DataTable();
            dtRRD.Columns.Add("TRId", typeof(int));
            dtRRD.Columns.Add("RoleId", typeof(int));
            dtRRD.Columns.Add("NoOfBillablePositions", typeof(int));
            dtRRD.Columns.Add("NoOfNonBillablePositions", typeof(int));
            dtRRD.Columns.Add("CreatedDate");
            dtRRD.Columns.Add("CreatedUser");
            dtRRD.Columns.Add("SystemInfo");
            string listTR = "(";
            foreach (var dr in drList)
            {
                DataRow drRRD = dtRRD.NewRow();

                drRRD["TRId"] = dr.TRId;
                drRRD["RoleId"] = dr.RoleId;
                drRRD["NoOfBillablePositions"] = (from report in dsReport.Tables["Master Sheet"].AsEnumerable()
                                                  where report.Field<int>("ProjectId") == dr.ProjectId
                                                  && report.Field<int>("ProjectTypeId") == dr.ProjectGroupId
                                                  && report.Field<int>("RoleId") == dr.RoleId
                                                  && report.Field<string>("Billing") == "Billable"
                                                  select report).Count();
                drRRD["NoOfNonBillablePositions"] = (from report in dsReport.Tables["Master Sheet"].AsEnumerable()
                                                     where report.Field<int>("ProjectId") == dr.ProjectId
                                                     && report.Field<int>("ProjectTypeId") == dr.ProjectGroupId
                                                     && report.Field<int>("RoleId") == dr.RoleId
                                                     && report.Field<string>("Billing") == "Non-Billable"
                                                     select report).Count();
                drRRD["CreatedDate"] = DateTime.Now;
                drRRD["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                drRRD["SystemInfo"] = Commons.GetClientIPAddress();

                dtRRD.Rows.Add(drRRD);
                listTR = listTR + drRRD["TRId"] + ",";
            }
            listTR = listTR.Remove(listTR.Length - 1, 1) + ")";
            listTR = drList.Count > 0 ? listTR : "(0)";
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {
                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[RequisitionRoleDetails]";
                        bulkCopy.ColumnMappings.Add("TRId", "TRId");
                        bulkCopy.ColumnMappings.Add("RoleId", "RoleId");
                        bulkCopy.ColumnMappings.Add("NoOfBillablePositions", "NoOfBillablePositions");
                        bulkCopy.ColumnMappings.Add("NoOfNonBillablePositions", "NoOfNonBillablePositions");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");

                        bulkCopy.WriteToServer(dtRRD);
                    }

                    tran.Commit();

                    SqlDataAdapter da = new SqlDataAdapter();
                    string query = "select * from RequisitionRoleDetails where TRId in " + listTR;
                    SqlCommand cmd = new SqlCommand(query, conn);
                    da.SelectCommand = cmd;
                    dsReport.Tables.Add("RequisitionRoleDetails");
                    da.Fill(dsReport.Tables["RequisitionRoleDetails"]);
                }
            }

            foreach (DataRow dr in dsReport.Tables["Master Sheet"].Rows)
            {
                dr["RequisitionRoleDetailId"] = dsReport.Tables["RequisitionRoleDetails"].AsEnumerable().Where(pa => pa.Field<int>("TRId") == (int)dr["TRId"] && pa.Field<int>("RoleId") == (int)dr["RoleId"]).Select(pa => pa.Field<int>("RequisitionRoleDetailId")).FirstOrDefault();
            }
        }

        /// <summary>
        /// Adding skills details for requisition role from report
        /// </summary>
        /// <param name="dsReport"></param>
        private void AddRequisitionRoleSkills(DataSet dsReport)
        {
            var drList = (from dr in dsReport.Tables["Master Sheet"].AsEnumerable()
                          where dr.Field<int>("ProjectId") != 0 && dr.Field<int>("ProjectTypeId") != 0 && dr.Field<int>("RoleId") != 0
                          group dr by new
                          {
                              ProjectId = dr.Field<int>("ProjectId"),
                              ProjectGroupId = dr.Field<int>("ProjectTypeId"),
                              RequisitionRoleDetailId = dr.Field<int>("RequisitionRoleDetailId"),
                              TRId = dr.Field<int>("TRId"),
                              PrimarySkill = dr.Field<string>("PrimarySkill")
                          } into temp
                          select new
                          {
                              ProjectId = temp.Key.ProjectId,
                              ProjectGroupId = temp.Key.ProjectGroupId,
                              RequisitionRoleDetailId = temp.Key.RequisitionRoleDetailId,
                              TRId = temp.Key.TRId,
                              PrimarySkill = (temp.Key.PrimarySkill).Split(',')
                          }).Distinct().ToList();

            DataTable dtRRS = new DataTable();
            dtRRS.Columns.Add("TRId", typeof(int));
            dtRRS.Columns.Add("RequistiionRoleDetailID", typeof(int));
            dtRRS.Columns.Add("CompetencyAreaId", typeof(int));
            dtRRS.Columns.Add("SkillId", typeof(int));
            dtRRS.Columns.Add("IsPrimary", typeof(bool));
            dtRRS.Columns.Add("ProficiencyLevelId", typeof(int));
            dtRRS.Columns.Add("IsDefaultSkill", typeof(bool));
            dtRRS.Columns.Add("CreatedDate");
            dtRRS.Columns.Add("CreatedUser");
            dtRRS.Columns.Add("SystemInfo");

            int CompetencyAreaId = dsReport.Tables["CompetencyArea"].AsEnumerable().Select(pa => pa.Field<int>("CompetencyAreaId")).FirstOrDefault();

            foreach (var dr in drList)
            {
                foreach (string skill in dr.PrimarySkill)
                {
                    DataRow drRRS = dtRRS.NewRow();

                    drRRS["TRId"] = dr.TRId;
                    drRRS["RequistiionRoleDetailID"] = dr.RequisitionRoleDetailId;
                    drRRS["CompetencyAreaId"] = CompetencyAreaId;
                    drRRS["SkillId"] = dsReport.Tables["Skills"].AsEnumerable().Where(s => s.Field<string>("SkillName").Trim().ToUpper() == skill.Trim().ToUpper()).Select(e => e.Field<int>("SkillID")).FirstOrDefault(); ;
                    drRRS["IsPrimary"] = true;
                    drRRS["ProficiencyLevelId"] = 1;
                    drRRS["IsDefaultSkill"] = true;
                    drRRS["CreatedDate"] = DateTime.Now;
                    drRRS["CreatedUser"] = HttpContext.Current.User.Identity.Name;
                    drRRS["SystemInfo"] = Commons.GetClientIPAddress();

                    dtRRS.Rows.Add(drRRS);
                }
            }

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                conn.Open();

                using (System.Data.SqlClient.SqlTransaction tran = conn.BeginTransaction())
                {
                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran))
                    {
                        bulkCopy.DestinationTableName = "[RequisitionRoleSkills]";
                        bulkCopy.ColumnMappings.Add("TRId", "TRId");
                        bulkCopy.ColumnMappings.Add("RequistiionRoleDetailID", "RequistiionRoleDetailID");
                        bulkCopy.ColumnMappings.Add("CompetencyAreaId", "CompetencyAreaId");
                        bulkCopy.ColumnMappings.Add("SkillId", "SkillId");
                        bulkCopy.ColumnMappings.Add("IsPrimary", "IsPrimary");
                        bulkCopy.ColumnMappings.Add("ProficiencyLevelId", "ProficiencyLevelId");
                        bulkCopy.ColumnMappings.Add("IsDefaultSkill", "IsDefaultSkill");
                        bulkCopy.ColumnMappings.Add("CreatedDate", "CreatedDate");
                        bulkCopy.ColumnMappings.Add("CreatedUser", "CreatedUser");
                        bulkCopy.ColumnMappings.Add("SystemInfo", "SystemInfo");

                        bulkCopy.WriteToServer(dtRRS);
                    }

                    tran.Commit();
                }
            }
        }
        #endregion
    }
}
