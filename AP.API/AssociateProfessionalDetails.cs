﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Web;
using System.Data.SqlClient;
using static AP.Utility.Enumeration;

namespace AP.API
{
    public class AssociateProfessionalDetails
    {
        #region Professional and Membership Tab Methods

        #region GetProfReferenceDetailsByID
        /// <summary>
        /// GetProfReferenceDetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public IEnumerable<object> GetProfReferenceDetailsByID(int empID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var getProfReferenceDetils = (from pr in hrmEntities.ProfessionalReferences
                                                  where pr.EmployeeId == empID && pr.IsActive == true
                                                  select new ProfRefDetails
                                                  {
                                                      ID = pr.ID,
                                                      name = pr.Name,
                                                      designation = pr.Designation,
                                                      companyName = pr.CompanyName,
                                                      companyAddress = pr.CompanyAddress,
                                                      officeEmailAddress = pr.OfficeEmailAddress,
                                                      mobileNo = pr.MobileNo

                                                  }).ToList();

                    return getProfReferenceDetils;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region AddCertificationDetails
        /// <summary>
        /// Add ceritification details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        public async Task<bool> AddCertificationDetails(ProfessionalDetails professionalDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_AddCertificationDetails] @EmployeeID, @CertificationID, @ValidFrom, @Institution, @Specialization, @ValidUpto, @CreatedUser, @CreatedDate, @SystemInfo, @SkillGroupID",
                                   new object[] {
                                        new SqlParameter ("EmployeeID", professionalDetails.EmployeeID),
                                        new SqlParameter ("CertificationID", professionalDetails.certificationID),
                                        new SqlParameter ("ValidFrom", professionalDetails.validFrom),
                                        new SqlParameter ("Institution", professionalDetails.institution),
                                        new SqlParameter ("Specialization", professionalDetails.specialization),
                                        new SqlParameter ("ValidUpto", professionalDetails.validUpto),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("SkillGroupID", professionalDetails.skillGroupID)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while adding a certification details");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region UpdateCertificationDetails
        /// <summary>
        /// Updates ceritification details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCertificationDetails(ProfessionalDetails professionalDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateCertificationDetails] @ID, @ValidFrom, @Institution, @Specialization, @ValidUpto, @ModifiedUser, @ModifiedDate, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ID", professionalDetails.ID),
                                        new SqlParameter ("ValidFrom", professionalDetails.validFrom),
                                        new SqlParameter ("Institution", professionalDetails.institution),
                                        new SqlParameter ("Specialization", professionalDetails.specialization),
                                        new SqlParameter ("ValidUpto", professionalDetails.validUpto),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while updating certification details.");
            }
            return rowsAffected > 0 ? true : false;

        }
        #endregion

        #region GetProfessionalDetailsByEmployeeID
        /// <summary>
        /// Get professional details by ID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<List<ProfessionalDetails>> GetProfessionalDetailsByEmployeeID(int employeeID)
        {
            List<ProfessionalDetails> lstProfessionalDetails;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstProfessionalDetails = await apEntities.Database.SqlQuery<ProfessionalDetails>
                              ("[usp_GetProfessionalDetailsByEmployeeID] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", employeeID)}
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get professional details.");
            }
            return lstProfessionalDetails;
        }
        #endregion

        #region GetSkillGroupsForCertificate
        /// <summary>
        /// Get skill groups for certificate competency
        /// </summary>
        /// <returns></returns>
        public async Task<List<SkillGroupDetails>> GetSkillGroupsByCertificate()
        {
            List<SkillGroupDetails> lstSkillGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkillGroups = await apEntities.Database.SqlQuery<SkillGroupDetails>
                              ("[usp_GetSkillGroupForCertificationDetails]"
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get skillgroups.");
            }
            return lstSkillGroups;
        }
        #endregion

        #region AddMembershipDetails
        /// <summary>
        /// Add membership details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        public async Task<bool> AddMembershipDetails(ProfessionalDetails professionalDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_AddMembershipDetails] @EmployeeID, @ProgramTitle, @ValidFrom, @Institution, @Specialization, @ValidUpto, @CreatedUser, @CreatedDate, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("EmployeeID", professionalDetails.EmployeeID),
                                        new SqlParameter ("ProgramTitle", professionalDetails.programTitle),
                                        new SqlParameter ("ValidFrom", professionalDetails.validFrom),
                                        new SqlParameter ("Institution", professionalDetails.institution),
                                        new SqlParameter ("Specialization", professionalDetails.specialization),
                                        new SqlParameter ("ValidUpto", professionalDetails.validUpto),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while adding membership details");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region UpdateMembershipDetails
        /// <summary>
        /// Updates membership details of an employee.
        /// </summary>
        /// <param name="professionalDetails"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMembershipDetails(ProfessionalDetails professionalDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateMembershipDetails] @ID, @ProgramTitle, @ValidFrom, @Institution, @Specialization, @ValidUpto, @ModifiedUser, @ModifiedDate, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ID", professionalDetails.ID),
                                        new SqlParameter ("ProgramTitle", professionalDetails.programTitle),
                                        new SqlParameter ("ValidFrom", professionalDetails.validFrom),
                                        new SqlParameter ("Institution", professionalDetails.institution),
                                        new SqlParameter ("Specialization", professionalDetails.specialization),
                                        new SqlParameter ("ValidUpto", professionalDetails.validUpto),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while updating membership details.");
            }
            return rowsAffected > 0 ? true : false;

        }
        #endregion

        #region DeleteProfessionalDetailsByID
        /// <summary>
        /// DeleteProfessionalDetailsByID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="programType"></param>
        /// <returns></returns>
        public async Task<bool> DeleteProfessionalDetailsByID(int id, int programType)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteProfessionalDetailsByID] @ID, @ProgramType",
                                   new object[] {
                                        new SqlParameter ("ID", id),
                                        new SqlParameter ("ProgramType", programType)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting professional details.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion
        
        #endregion

        #region public methods
        /// <summary>
        /// This method fetches associate professional details (like name and email) based on roles passed to it.
        /// </summary>
        /// <param name="hrmsEntities">DB context.</param>
        /// <param name="roleCodes">The comma seperated list of role codes.</param>
        /// <returns>The List of AssociateDetails.</returns>
        public static List<AssociateDetails> GetAssociatesByRole(APEntities hrmsEntities, string talentRequisitionMenuTitle)
        {

            try
            {
                var associates = (from e in hrmsEntities.Employees
                                  join user in hrmsEntities.Users on e.UserId equals user.UserId
                                  join userRole in hrmsEntities.UserRoles on e.UserId equals userRole.UserId
                                  join role in hrmsEntities.Roles on userRole.RoleId equals role.RoleId
                                  join mr in hrmsEntities.MenuRoles on role.RoleId equals mr.RoleId
                                  join m in hrmsEntities.MenuMasters on mr.MenuId equals m.MenuId
                                  where e.IsActive == true && userRole.IsActive == true && role.IsActive == true
                                        && m.Title == talentRequisitionMenuTitle
                                  orderby user.EmailAddress descending
                                  select new AssociateDetails()
                                  {
                                      empName = e.FirstName + " " + e.LastName,
                                      WorkEmail = user.EmailAddress,
                                      RoleName = role.RoleName
                                  }).ToList();
                return associates;
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        public static dynamic GetAssociatesDetailsByRoleId(APEntities hrmsEntities, string userEmail)
        {
            try
            {
                var associates = (from r in hrmsEntities.Roles
                                  join userrole in hrmsEntities.UserRoles on r.RoleId equals userrole.RoleId
                                  join u in hrmsEntities.Users on userrole.UserId equals u.UserId
                                  join ee in hrmsEntities.Employees on u.UserId equals ee.UserId
                                  join ee1 in hrmsEntities.Employees on ee.ReportingManager equals ee1.EmployeeId
                                  join usr1 in hrmsEntities.Users on ee1.UserId equals usr1.UserId
                                  join dept in hrmsEntities.Departments on ee.DepartmentId equals dept.DepartmentId
                                  join usr2 in hrmsEntities.Users on ee1.UserId equals usr2.UserId
                                  join ee3 in hrmsEntities.Employees on ee.ReportingManager equals ee3.EmployeeId
                                  join usr3 in hrmsEntities.Users on ee3.UserId equals usr3.UserId
                                  where u.EmailAddress == userEmail && userrole.IsActive == true && userrole.IsPrimary == true
                                  && u.UserId == ee.UserId
                                  select new
                                  {
                                      EmpName = ee.FirstName + " " + ee.LastName,
                                      EmployeeId = ee.EmployeeId,
                                      EmailAddress = u.EmailAddress,
                                      RoleName = r.RoleName,
                                      ReportingManagerId = ee.ReportingManager,
                                      UserId = ee.UserId,
                                      DepartmentId = ee.DepartmentId,
                                      DepartmentCode = dept.DepartmentCode,
                                      ProgramManagerId = ee.ProgramManager,
                                      ReportingManagerEmail = usr1.EmailAddress,
                                      DepartmentHeadEmail = usr2.EmailAddress,
                                      ProgramManagerEmail = usr3.EmailAddress,
                                      ReportingManagerName = ee1.FirstName + " " + ee1.LastName,
                                      ProgramManagerName = ee3.FirstName + " " + ee3.LastName
                                  }).ToList();
                return associates;
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }

        }
    }
}
