﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.Web;
using static AP.Utility.Enumeration;
using System.Data.SqlClient;

namespace AP.API
{

    public class UserRoles
    {

        #region GetRoles
        /// <summary>
        /// Gets roles details
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetRoles(bool isActive)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var roles = (from role in hrmsEntities.Roles
                                 select new
                                 {
                                     ID = role.RoleId,
                                     Name = role.RoleName,
                                     IsActive = role.IsActive
                                 }).OrderBy(r => r.Name).ToList();

                    if (isActive)
                        roles = roles.Where(i => i.IsActive == true).ToList();

                    return roles;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetUserRoleDetails
        /// <summary>
        /// Gets users roles details
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetUserRoleDetails()
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var getUserRoleDetails = (from ur in hrmEntities.UserRoles
                                              join usr in hrmEntities.Users on ur.UserId equals usr.UserId
                                              join r in hrmEntities.Roles on ur.RoleId equals r.RoleId
                                              select new { ur.UserRolesID, usr.UserId, usr.UserName, r.RoleId, r.RoleName, ur.IsActive }).ToList();

                    if (getUserRoleDetails.Count != 0)
                    {
                        var mergedResult = from row in getUserRoleDetails.ToList()
                                           group row.RoleName by new { row.UserId, row.UserName, row.IsActive } into grp
                                           select new
                                           {
                                               grp.Key.UserId,
                                               grp.Key.UserName,
                                               grp.Key.IsActive,
                                               RoleName = string.Join(",", grp),
                                               PrimaryRole = (from ur in hrmEntities.UserRoles
                                                              join r in hrmEntities.Roles on ur.RoleId equals r.RoleId
                                                              where ur.UserId == grp.Key.UserId && ur.IsPrimary == true
                                                              select r.RoleName
                                                              ).FirstOrDefault()
                                           };
                        return mergedResult.OrderBy(uName => uName.UserName).ToList();
                    }
                    else
                    {
                        Log.Write("User roles data is not available", Log.Severity.Warning, parms);
                        return Enumerable.Empty<object>().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }

        }
        #endregion

        #region GetUserRoleDetailsbyUserID
        /// <summary>
        /// Gets users roles details by User  ID
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetUserRoleDetailsbyUserID(int userID)
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var getUserRoleDetails = (from ur in hrmEntities.UserRoles
                                              join usr in hrmEntities.Users on ur.UserId equals usr.UserId
                                              join r in hrmEntities.Roles on ur.RoleId equals r.RoleId
                                              join emp in hrmEntities.Employees on ur.UserId equals emp.UserId into info
                                              where usr.UserId == userID && usr.IsActive == true && ur.IsActive == true
                                              select new
                                              {
                                                  ur.UserRolesID,
                                                  usr.UserId,
                                                  usr.UserName,
                                                  r.RoleId,
                                                  r.RoleName,
                                                  ur.IsActive,
                                                  IsPrimary = ur.IsPrimary == null ? false : ur.IsPrimary
                                              }).ToList();

                    if (getUserRoleDetails.Count > 0)
                        return getUserRoleDetails;

                    else
                    {
                        Log.Write("User roles data is not available", Log.Severity.Warning, parms);
                        return Enumerable.Empty<object>().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }

        }
        #endregion

        #region AssignRole
        /// <summary>
        /// Assigns role to an associate
        /// </summary>
        /// <param name="userRoleData"></param>
        /// <returns></returns>
        public bool AssignRole(UserRoleData userRoleData)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    UserRole userRole = new UserRole();

                    userRole.UserId = userRoleData.UserId;

                    var isRoleExists = (from ur in hrmsEntities.UserRoles
                                        where ur.UserId == userRole.UserId
                                        select ur).Count();

                    var isRoleAssigned = (from ur in hrmsEntities.UserRoles
                                          where ur.UserId == userRole.UserId && ur.RoleId == userRoleData.RoleId
                                          select ur).Count();

                    if (isRoleAssigned == 0)
                    {
                        userRole.RoleId = userRoleData.RoleId;
                        userRole.CreatedUser = userRoleData.CurrentUser;
                        userRole.CreatedDate = DateTime.Now;
                        userRole.IsActive = userRoleData.IsActive;
                        if (isRoleExists == 0)
                            userRole.IsPrimary = true;
                        else
                            userRole.IsPrimary = false;
                        userRole.SystemInfo = userRoleData.SystemInfo;
                        hrmsEntities.UserRoles.Add(userRole);
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                    }
                    else
                        throw new AssociatePortalException("Role already assigned to user");
                }
            }
            catch
            {
                throw;
            }

            return retValue;
        }
        #endregion

        #region UpdateRole
        /// <summary>
        /// Updates user role's details
        /// </summary>
        /// <param name="userRoleData"></param>
        /// <returns></returns>
        public bool UpdateRole(IEnumerable<UserRoleData> userRoleData)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    foreach (UserRoleData ur in userRoleData)
                    {


                        //User user = hrmsEntities.Users.Where(i => i.UserName == userRole.UserName).FirstOrDefault();
                        UserRole userRole = hrmsEntities.UserRoles.First(x => x.UserRolesID == ur.UserRolesID);

                        if (!Object.ReferenceEquals(ur, null))
                        {
                            userRole.RoleId = ur.RoleId;
                            userRole.IsActive = ur.IsActive;
                            userRole.IsPrimary = ur.IsPrimary;
                            userRole.ModifiedDate = DateTime.Now;
                            userRole.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntities.Entry(userRole).State = System.Data.Entity.EntityState.Modified;
                            retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return retValue;
        }
        #endregion

        #region UpdateUserRole
        /// <summary>
        /// Updates user role's details (implemented for Angular2 UI)
        /// </summary>
        /// <param name="userRoleData"></param>
        /// <returns></returns>
        public bool UpdateUserRole(UserRoleData userRoleData)
        {
            bool retValue = false;
            int assignedRolesCount = 0;

            using (APEntities hrmsEntities = new APEntities())
            {

                //UserRole userRole = hrmsEntities.UserRoles.FirstOrDefault(x => x.UserRolesID == userRoleData.UserRolesID && x.IsActive == true);
                IQueryable<UserRole> userRoleQuery = hrmsEntities.UserRoles.Where(
                   ur => ur.UserId == userRoleData.UserId &&
                   ur.RoleId == userRoleData.RoleId &&
                   ur.IsActive == true
                   );

                assignedRolesCount = userRoleQuery.Count();
                if (assignedRolesCount <= 0)
                {
                    int employeeId = (from employee in hrmsEntities.Employees
                                      where employee.UserId == userRoleData.UserId
                                      select employee.EmployeeId).FirstOrDefault();

                    var departmentDetails = (from employee in hrmsEntities.Employees
                                              join department in hrmsEntities.Departments on employee.DepartmentId equals department.DepartmentId
                                              where employee.EmployeeId == employeeId
                                              select new { employee.DepartmentId, department.DepartmentTypeId }).FirstOrDefault();
                    if (departmentDetails != null)
                    {
                        if (departmentDetails.DepartmentTypeId == 1)
                        {
                            retValue = DeliveryDepartmentRoles(userRoleData, employeeId) > 0 ? true : false;
                        }
                        else
                        {
                            retValue = NonDeliveryDepartmentRoles(userRoleData, employeeId) > 0 ? true : false;
                        }
                    }
                }
                else
                {
                    UserRole userRole = userRoleQuery.FirstOrDefault();
                    userRole.IsActive = userRoleData.IsActive;
                    userRole.ModifiedUser = HttpContext.Current.User.Identity.Name;
                    userRole.ModifiedDate = DateTime.Now;
                    userRole.SystemInfo = Commons.GetClientIPAddress();
                    hrmsEntities.Entry(userRole).State = System.Data.Entity.EntityState.Modified;
                    retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return retValue;
        }
        #endregion

        private bool AssignUserRole(int? RoleId, int? UserId, bool? IsActive)
        {
            bool retValue = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                UserRole userRole = new UserRole();
                userRole.RoleId = RoleId;
                userRole.CreatedUser = HttpContext.Current.User.Identity.Name;
                userRole.CreatedDate = DateTime.Now;
                userRole.IsActive = IsActive;
                userRole.UserId = UserId;
                userRole.SystemInfo = Commons.GetClientIPAddress();
                hrmsEntities.UserRoles.Add(userRole);
                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return retValue;
        }

        private int DeliveryDepartmentRoles(UserRoleData userRoleData, int EmployeeId)
        {
            int retValue = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                if (userRoleData.RoleName.Trim().ToLower() == "Lead".Trim().ToLower())
                {
                    int leadId = (from projectManager in hrmsEntities.ProjectManagers
                                  where projectManager.LeadID == EmployeeId && projectManager.IsActive == true
                                  select projectManager.ID).FirstOrDefault();
                    if (leadId > 0)
                        retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                    else
                        return -6; //Cannot assign lead role to associate
                }
                else if (userRoleData.RoleName.Trim().ToLower() == "Reporting Manager".Trim().ToLower())
                {
                    int projectManagerId = (from projectManager in hrmsEntities.ProjectManagers
                                            where projectManager.ReportingManagerID == EmployeeId && projectManager.IsActive == true
                                            select projectManager.ID).FirstOrDefault();
                    if (projectManagerId > 0)
                        retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                    else
                        return -6; //Cannot assign reporting manager role to associate
                }
                else if (userRoleData.RoleName.Trim().ToLower() == "Program Manager".Trim().ToLower())
                {
                    int projectManagerId = (from projectManager in hrmsEntities.ProjectManagers
                                            where projectManager.ProgramManagerID == EmployeeId && projectManager.IsActive == true
                                            select projectManager.ID).FirstOrDefault();
                    if (projectManagerId > 0)
                        retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                    else
                        return -6; //Cannot assign program manager role to associate
                }
                else if (userRoleData.RoleName.Trim().ToLower() == "Department Head".Trim().ToLower())
                {
                    retValue = AssignDepartmentHeadRole(userRoleData, EmployeeId);
                }
                else
                {
                    retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                }
            }
            return retValue;
        }

        private int NonDeliveryDepartmentRoles(UserRoleData userRoleData, int EmployeeId)
        {
            int retValue = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                if (userRoleData.RoleName.Trim().ToLower() == "Lead".Trim().ToLower() || userRoleData.RoleName.Trim().ToLower() == "Program Manager".Trim().ToLower() || userRoleData.RoleName.Trim().ToLower() == "Reporting Manager".Trim().ToLower())
                {
                    int leadId = (from employee in hrmsEntities.Employees
                                  where employee.ReportingManager == EmployeeId && employee.IsActive == true
                                  select employee.EmployeeId).FirstOrDefault();
                    if (leadId > 0)
                        retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                    else
                        return -6; //Cannot assign lead role to associate
                }
                else if (userRoleData.RoleName.Trim().ToLower() == "Department Head".Trim().ToLower())
                {
                    retValue = AssignDepartmentHeadRole(userRoleData, EmployeeId);
                }
                else
                {
                    retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                }
            }
            return retValue;
        }

        private int AssignDepartmentHeadRole(UserRoleData userRoleData, int EmployeeId)
        {
            int retValue = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                int departmentID = (from department in hrmsEntities.Departments
                                    where department.DepartmentHeadID == EmployeeId && department.IsActive == true
                                    select department.DepartmentId).FirstOrDefault();
                if (departmentID > 0)
                    retValue = AssignUserRole(userRoleData.RoleId, userRoleData.UserId, userRoleData.IsActive) == true ? 1 : 0;
                else
                    return -6; //Cannot assign department head role to associate
            }
            return retValue;
        }

        #region GetRoleMasterDetails
        /// <summary>
        /// GetRoleMasterDetails
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetRoleMasterDetails()
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var ddlRoleData = (from a in hrmEntities.Roles
                                       select new { a.RoleName, a.RoleDescription, a.KeyResponsibilities, a.IsActive }).ToList();
                    return ddlRoleData;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }

        }
        #endregion

        #region GetUserRoleOnLgin
        /// <summary>
        /// GetUserRoleOnLgin
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// 
        public List<LoginUserRole> GetUserRoleOnLgin(string userName)
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    List<LoginUserRole> roles = (from r in hrmsEntities.Roles
                                                 join usrrole in hrmsEntities.UserRoles on r.RoleId equals usrrole.RoleId
                                                 join usr in hrmsEntities.Users on usrrole.UserId equals usr.UserId
                                                 where usr.EmailAddress == userName && usrrole.IsActive == true
                                                 select new LoginUserRole
                                                 {
                                                     role = r.RoleName
                                                 }).Distinct().ToList();
                    return roles;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
        }

        public UserDetails GetEmployeeOnUserName(string userName)
        {
            UserDetails employee = null;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    employee = (from emp in hrmsEntities.Employees
                                join user in hrmsEntities.Users on emp.UserId equals user.UserId
                                where emp.IsActive == true
                                && user.EmailAddress == userName
                                select new UserDetails { empID = emp.EmployeeId, firstName = emp.FirstName, lastName = emp.LastName }).FirstOrDefault();

                    employee.name = $"{employee.firstName} {employee.lastName}";
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "[]");
                throw;
            }
            return employee;
        }
        #endregion

        #region Private Methods

        #region UserRoleSearch
        /// <summary>
        /// UserRoleSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> UserRoleSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getSearchData = (from ur in hrmEntities.UserRoles
                                     join r in hrmEntities.Roles on ur.RoleId equals r.RoleId
                                     join usr in hrmEntities.Users on ur.UserId equals usr.UserId
                                     select new
                                     {
                                         ur.UserId,
                                         ur.UserRolesID,
                                         r.RoleId,
                                         usr.UserName,
                                         ur.IsActive,
                                         r.RoleName,

                                         PrimaryRole = (from aur in hrmEntities.UserRoles
                                                        join role in hrmEntities.Roles on aur.RoleId equals role.RoleId
                                                        where aur.UserId == ur.UserId && aur.IsPrimary == true
                                                        select role.RoleName
                                                              ).FirstOrDefault()
                                     }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Role.ToString())
                    getSearchData = getSearchData.Where(r => r.RoleName != null && r.RoleName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.UserName.ToString())
                    getSearchData = getSearchData.Where(r => r.UserName != null && r.UserName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getSearchData;
            }
        }
        #endregion

        #region RoleSearch
        /// <summary>
        /// RoleSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> RoleSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getRoleSearchData = (from roleData in hrmEntities.Roles
                                         join department in hrmEntities.Departments on roleData.DepartmentId equals department.DepartmentId into info
                                         from d in info.DefaultIfEmpty()
                                         select new { roleData.RoleId, roleData.RoleName, roleData.RoleDescription, roleData.IsActive, roleData.DepartmentId, DepartmentCode = d.Description }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getRoleSearchData = getRoleSearchData.Where(r => r.RoleName != null && r.RoleName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getRoleSearchData = getRoleSearchData.Where(r => r.RoleDescription != null && r.RoleDescription.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.DepartmentCode.ToString())
                    getRoleSearchData = getRoleSearchData.Where(r => r.DepartmentCode != null && r.DepartmentCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getRoleSearchData;
            }
        }
        #endregion

        #region SkillSearch
        /// <summary>
        /// SkillSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> SkillSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getSkillSearchData = (from skillData in hrmEntities.Skills
                                          join ca in hrmEntities.CompetencyAreas on skillData.CompetencyAreaId equals ca.CompetencyAreaId
                                          join sg in hrmEntities.SkillGroups on skillData.SkillGroupId equals sg.SkillGroupId
                                          select new { skillData.SkillId, skillData.SkillCode, skillData.SkillDescription, skillData.IsActive, ca.CompetencyAreaCode, sg.SkillGroupName }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getSkillSearchData = getSkillSearchData.Where(r => r.SkillCode != null && r.SkillCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getSkillSearchData = getSkillSearchData.Where(r => r.SkillDescription != null && r.SkillDescription.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.CompetencyArea.ToString())
                    getSkillSearchData = getSkillSearchData.Where(r => r.CompetencyAreaCode != null && r.CompetencyAreaCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.SkillGroup.ToString())
                    getSkillSearchData = getSkillSearchData.Where(r => r.SkillGroupName != null && r.SkillGroupName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getSkillSearchData;
            }
        }
        #endregion

        #region StatusSearch
        /// <summary>
        /// StatusSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> StatusSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getStatusSearchData = (from statusData in hrmEntities.Status
                                           select new { statusData.StatusId, statusData.StatusCode, statusData.StatusDescription, statusData.IsActive, statusData.Category }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getStatusSearchData = getStatusSearchData.Where(r => r.StatusCode != null && r.StatusCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getStatusSearchData = getStatusSearchData.Where(r => r.StatusDescription != null && r.StatusDescription.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Category.ToString())
                    getStatusSearchData = getStatusSearchData.Where(r => r.Category != null && r.Category.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getStatusSearchData;
            }
        }
        #endregion

        #region DepartmentSearch
        /// <summary>
        /// DepartmentSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> DepartmentSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getDeptSearchData = (from dept in hrmEntities.Departments
                                         select new { dept.DepartmentId, dept.Description, dept.DepartmentCode, dept.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getDeptSearchData = getDeptSearchData.Where(r => r.DepartmentCode != null && r.DepartmentCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getDeptSearchData = getDeptSearchData.Where(r => r.Description != null && r.Description.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getDeptSearchData;
            }
        }
        #endregion

        #region GradeSearch
        /// <summary>
        /// GradeSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> GradeSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getGradeSearchData = (from grade in hrmEntities.Grades
                                          select new { grade.GradeId, grade.GradeCode, grade.GradeName, grade.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getGradeSearchData = getGradeSearchData.Where(r => r.GradeCode != null && r.GradeCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getGradeSearchData = getGradeSearchData.Where(r => r.GradeName != null && r.GradeName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getGradeSearchData;
            }
        }
        #endregion

        #region CompetencyAreaSearch
        /// <summary>
        /// CompetencyAreaSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> CompetencyAreaSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getCompetencySearchData = (from competencyArea in hrmEntities.CompetencyAreas
                                               select new { competencyArea.CompetencyAreaId, competencyArea.CompetencyAreaCode, competencyArea.CompetencyAreaDescription, competencyArea.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getCompetencySearchData = getCompetencySearchData.Where(r => r.CompetencyAreaCode != null && r.CompetencyAreaCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getCompetencySearchData = getCompetencySearchData.Where(r => r.CompetencyAreaDescription != null && r.CompetencyAreaDescription.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getCompetencySearchData;
            }
        }
        #endregion

        #region ProficiencyLevelSearch
        /// <summary>
        /// ProficiencyLevelSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> ProficiencyLevelSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getProficiencyLevelSearchData = (from proficiencyLevel in hrmEntities.ProficiencyLevels
                                                     select new { proficiencyLevel.ProficiencyLevelCode, proficiencyLevel.ProficiencyLevelDescription, proficiencyLevel.ProficiencyLevelId, proficiencyLevel.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getProficiencyLevelSearchData = getProficiencyLevelSearchData.Where(r => r.ProficiencyLevelCode != null && r.ProficiencyLevelCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getProficiencyLevelSearchData = getProficiencyLevelSearchData.Where(r => r.ProficiencyLevelDescription != null && r.ProficiencyLevelDescription.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getProficiencyLevelSearchData;
            }
        }
        #endregion

        #region PracticeAreaSearch
        /// <summary>
        /// PracticeAreaSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> PracticeAreaSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getPracticeAreaSearchData = (from practiceArea in hrmEntities.PracticeAreas
                                                 select new { practiceArea.PracticeAreaCode, practiceArea.PracticeAreaDescription, practiceArea.PracticeAreaId, practiceArea.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getPracticeAreaSearchData = getPracticeAreaSearchData.Where(r => r.PracticeAreaCode != null && r.PracticeAreaCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getPracticeAreaSearchData = getPracticeAreaSearchData.Where(r => r.PracticeAreaDescription != null && r.PracticeAreaDescription.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getPracticeAreaSearchData;
            }
        }
        #endregion

        #region ProjectTypeSearch
        /// <summary>
        /// Project Type Search
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> ProjectTypeSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getProjectTypeSearchData = (from projectType in hrmEntities.ProjectTypes
                                                select new { projectType.ProjectTypeId, projectType.ProjectTypeCode, projectType.Description, projectType.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getProjectTypeSearchData = getProjectTypeSearchData.Where(r => r.ProjectTypeCode != null && r.ProjectTypeCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getProjectTypeSearchData = getProjectTypeSearchData.Where(r => r.Description != null && r.Description.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getProjectTypeSearchData;
            }
        }
        #endregion

        #region DesignationSearch
        /// <summary>
        /// Designation Search
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> DesignationSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getDesignationSearchData = (from designation in hrmEntities.Designations
                                                select new { designation.DesignationId, designation.DesignationCode, designation.DesignationName, designation.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getDesignationSearchData = getDesignationSearchData.Where(r => r.DesignationCode != null && r.DesignationCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getDesignationSearchData = getDesignationSearchData.Where(r => r.DesignationName != null && r.DesignationName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getDesignationSearchData;
            }
        }
        #endregion

        //#region KraAspectSearch
        ///// <summary>
        ///// KraAspect Search
        ///// </summary>
        ///// <param name="searchFilter"></param>
        ///// <returns></returns>
        //private IEnumerable<object> KraAspectSearch(SearchFilter searchFilter)
        //{
        //   using (APEntities hrmEntities = new APEntities())
        //   {
        //      var getKraAspectData = (from kra in hrmEntities.KRAAspects
        //                              select new { kra.KRAAspectId, kra.KRAAspectCode, kra.Description, kra.IsActive }).ToList();

        //      if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
        //         getKraAspectData = getKraAspectData.Where(r => r.KRAAspectCode != null && r.KRAAspectCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

        //      if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
        //         getKraAspectData = getKraAspectData.Where(r => r.Description != null && r.Description.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

        //      return getKraAspectData;
        //   }
        //}
        //#endregion


        #region BusinessRuleSearch
        /// <summary>
        /// Business Rule Search 
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> BusinessRuleSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getBusinessRuleData = (from br in hrmEntities.BusinessRules
                                           select new { br.BusinessRuleId, br.BusinessRuleCode, br.BusinessRuleValue, br.Description, br.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getBusinessRuleData = getBusinessRuleData.Where(br => br.BusinessRuleCode != null && br.BusinessRuleCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getBusinessRuleData = getBusinessRuleData.Where(br => br.Description != null && br.Description.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.BusinessRuleValue.ToString())
                    getBusinessRuleData = getBusinessRuleData.Where(br => br.BusinessRuleValue != null && br.BusinessRuleValue.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getBusinessRuleData;
            }
        }
        #endregion

        #region ClientBillingRoleSearch
        /// <summary>
        /// Client Billing Role Search
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> ClientBillingRoleSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getClientBillingRoles = (from cbr in hrmEntities.ClientBillingRoles
                                             select new { cbr.ClientBillingRoleId, cbr.ClientBillingRoleName, cbr.IsActive }).ToList();

                //if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                //    getClientBillingRoles = getClientBillingRoles.Where(br => br.ClientBillingRoleCode != null && br.ClientBillingRoleCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getClientBillingRoles = getClientBillingRoles.Where(br => br.ClientBillingRoleName != null && br.ClientBillingRoleName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getClientBillingRoles;
            }
        }
        #endregion

        #region InternalBillingRoleSearch
        /// <summary>
        /// Internal Billing Role Search
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> InternalBillingRoleSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getInternalBillingRoles = (from cbr in hrmEntities.InternalBillingRoles
                                               select new { cbr.InternalBillingRoleId, cbr.InternalBillingRoleCode, cbr.InternalBillingRoleName, cbr.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getInternalBillingRoles = getInternalBillingRoles.Where(br => br.InternalBillingRoleCode != null && br.InternalBillingRoleCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getInternalBillingRoles = getInternalBillingRoles.Where(br => br.InternalBillingRoleName != null && br.InternalBillingRoleName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getInternalBillingRoles;
            }
        }
        #endregion

        #endregion

        #region GetUsers
        /// <summary>
        /// Retrieves active users names
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetUsers()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var usersList = (from users in hrmsEntities.Users
                                     join emp in hrmsEntities.Employees on users.UserId equals emp.UserId into ur
                                     from dep in ur.DefaultIfEmpty()
                                     where users.IsActive == true && dep.UserId != null
                                     orderby users.UserName
                                     select new { users.UserName, users.UserId, users.EmailAddress, dep.DepartmentId }).ToList();

                    if (usersList.Count > 0)
                        return usersList;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region ClientSearch
        /// <summary>
        /// ClientSearch
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        private IEnumerable<object> ClientSearch(SearchFilter searchFilter)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                var getClientSearchData = (from client in hrmEntities.Clients
                                           select new { client.ClientId, client.ClientCode, client.ClientName, client.IsActive }).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Code.ToString())
                    getClientSearchData = getClientSearchData.Where(r => r.ClientCode != null && r.ClientCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                if (searchFilter.SearchType == Enumeration.SearchType.Desc.ToString())
                    getClientSearchData = getClientSearchData.Where(r => r.ClientName != null && r.ClientName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                return getClientSearchData;
            }
        }
        #endregion

        #region GetNames
        /// <summary>
        /// GetNames
        /// </summary>
        /// <returns></returns>
        public List<UserDetails> GetNames()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<UserDetails> associateNames = hrmsEntities.Employees.Where(assosciate => assosciate.IsActive == true).AsQueryable()
                                                    .Select(namesData => new UserDetails
                                                    {
                                                        ID = namesData.EmployeeId,
                                                        name = namesData.FirstName + " " + namesData.LastName
                                                    }).OrderBy(names => names.name);

                return associateNames.ToList<UserDetails>();

            }
        }
        #endregion

        #region GetEmployeeResignStatus
        /// <summary>
        /// GetEmployeeResignStatus
        /// </summary>
        /// <returns></returns>
        public int GetEmployeeResignStatus()
        {
            return new Common().GetStatusId(StatusCategory.AssociateExit.ToString(), AssociateExitStatusCodes.Resigned.ToString());
        }
        #endregion

        #region MapAssociateId
        /// <summary>
        /// Maps an associate's name to its corresponding email adddress
        /// </summary>
        /// <returns></returns>
        public bool MapAssociateId(AssociateDetails associateDetails)
        {
            bool mapped = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    Employee employee = hrmsEntities.Employees.Where(emp => emp.EmployeeId == associateDetails.empID).FirstOrDefault();
                    if (employee != null)
                    {
                        employee.UserId = associateDetails.UserId;
                        employee.ModifiedUser = associateDetails.CurrentUser;
                        employee.ModifiedDate = DateTime.Now;
                        employee.SystemInfo = associateDetails.SystemInfo;
                        hrmsEntities.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                        mapped = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Employee does not exist");
                }
            }
            catch
            {
                throw;
            }
            return mapped;
        }
        #endregion

        #region UpdateEmployeeStatus
        /// <summary>
        /// Updates the employee status
        /// </summary>
        /// <returns></returns>
        public bool UpdateEmployeeStatus(AssociateDetails associateDetails)
        {
            bool mapped = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    Employee employee = hrmsEntities.Employees.Where(emp => emp.EmployeeId == associateDetails.empID).FirstOrDefault();
                    if (employee != null)
                    {
                        List<ProjectManager> isAllocated = hrmsEntities.ProjectManagers.Where(a => (a.ReportingManagerID == associateDetails.empID || a.ProgramManagerID == associateDetails.empID || a.LeadID == associateDetails.empID) && a.IsActive == true).ToList();
                        if(isAllocated !=null && isAllocated.Count > 0)
                        {
                            throw new AssociatePortalException("This associate has allocated as Lead or Reporting Manager or Program Manager.So you can't inactive him");
                        }
                        if (associateDetails.StatusId == null)
                        {
                            var resignStatusId = new Common().GetStatusId(StatusCategory.AssociateExit.ToString(), AssociateExitStatusCodes.Resigned.ToString());
                            var approvedStatusId = new StatusMaster().GetStatusIdByCode(AssociateExitStatusCodes.ApprovedByPM.ToString());
                            employee.IsActive = associateDetails.IsActive;
                            if (associateDetails.IsActive == false)
                            {
                                List<AssociateAllocation> associateAllocations = hrmsEntities.AssociateAllocations.Where(a => a.EmployeeId == associateDetails.empID && a.IsActive == true ).ToList();
                                //foreach (var a in associateAllocations)
                                //{
                                //    a.ReleaseDate = Commons.GetDateTimeInIST(associateDetails.lastWorkingDate);
                                //    a.IsActive = false;
                                //    employee.UserId = null;
                                //}
                                if (associateAllocations != null && associateAllocations.Count == 1)
                                {
                                    int projectTypeId = getProjectType(associateAllocations.SingleOrDefault().ProjectId);
                                    if (projectTypeId != 6)
                                        throw new AssociatePortalException("This associate has an allocation(s). Release associate from project(s)");
                                    else if (projectTypeId == 6)
                                    {
                                        associateAllocations.SingleOrDefault().ReleaseDate = Commons.GetDateTimeInIST(associateDetails.lastWorkingDate);
                                        associateAllocations.SingleOrDefault().IsActive = false;
                                        employee.UserId = null;
                                    }
                                }
                                else
                                {
                                    throw new AssociatePortalException("This associate has an allocation(s). Release associate from project(s)");
                                }


                            }
                            if (employee.StatusId == resignStatusId)
                            {
                                employee.StatusId = approvedStatusId;

                            }

                        }
                        else
                        {
                            employee.StatusId = associateDetails.StatusId;
                            employee.ResignationDate = DateTime.Now;
                        }

                        employee.ModifiedDate = DateTime.Now;
                        employee.ModifiedUser = HttpContext.Current.User.Identity.Name;

                        employee.SystemInfo = associateDetails.SystemInfo;
                        hrmsEntities.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                        mapped = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Employee does not exist");
                }
            }
            catch
            {
                throw;
            }
            return mapped;
        }
        #endregion
        private int getProjectType(int? projectId)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    int projectTypeId = apEntities.Database.SqlQuery<int>
                              ("[usp_GetProjectType] @ProjectId",

                               new object[] {
                                        new SqlParameter ("ProjectId", projectId)
                             }).SingleOrDefault();
                    return projectTypeId;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Project Type Id");
            }
        }
        #region GetUnMappedUsers
        /// <summary>
        /// GetUnMappedUsers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetUnMappedUsers()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var usersList = (from users in hrmsEntities.Users
                                     join emp in hrmsEntities.Employees on users.UserId equals emp.UserId into ur
                                     from dep in ur.DefaultIfEmpty()
                                     where users.IsActive == true && dep.UserId == null
                                     orderby users.UserName
                                     select new { users.UserName, users.UserId, users.EmailAddress, dep.DepartmentId }).ToList();

                    if (usersList.Count > 0)
                        return usersList;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
