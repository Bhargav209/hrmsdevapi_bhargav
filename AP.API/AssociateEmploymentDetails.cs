﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Web;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Core.Objects;

namespace AP.API
{
    public class AssociateEmploymentDetails
    {
        #region Employment Tab Methods

        #region SaveEmployementDetails
        /// <summary>
        /// Add/Update Employement Details
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool SaveEmployementDetails(UserDetails details)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var duplicateEmpDetails = hrmsEntities.PreviousEmploymentDetails.Where(d => d.EmployeeId == details.empID).ToList();

                    foreach (PreviousEmploymentDetail detail in duplicateEmpDetails)
                    {
                        detail.IsActive = false;
                        hrmsEntities.SaveChanges();
                    }

                    foreach (EmploymentDetails ed in details.prevEmployerdetails)
                    {
                        if (!string.IsNullOrEmpty(ed.name))
                        {
                            PreviousEmploymentDetail prevEmpDetails = hrmsEntities.PreviousEmploymentDetails.SingleOrDefault(i => i.ID == ed.ID);
                            if (prevEmpDetails != null)
                            {
                                prevEmpDetails.Name = ed.name;
                                prevEmpDetails.Address = ed.address;
                                prevEmpDetails.Designation = ed.designation;
                                prevEmpDetails.ServiceFrom = Convert.ToDateTime(ed.fromYear);
                                prevEmpDetails.ServiceTo = Convert.ToDateTime(ed.toYear);
                                prevEmpDetails.LeavingReson = ed.leavingReson;
                                prevEmpDetails.IsActive = true;
                                prevEmpDetails.ModifiedUser = HttpContext.Current.User.Identity.Name;
                                prevEmpDetails.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntities.Entry(prevEmpDetails).State = EntityState.Modified;
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                            else
                            {
                                PreviousEmploymentDetail newPrevEmpDetails = new PreviousEmploymentDetail();
                                newPrevEmpDetails.EmployeeId = details.empID;
                                newPrevEmpDetails.Name = ed.name;
                                newPrevEmpDetails.Address = ed.address;
                                newPrevEmpDetails.Designation = ed.designation;
                                newPrevEmpDetails.ServiceFrom = Convert.ToDateTime(ed.fromYear);
                                newPrevEmpDetails.ServiceTo = Convert.ToDateTime(ed.toYear);
                                newPrevEmpDetails.LeavingReson = ed.leavingReson;
                                newPrevEmpDetails.IsActive = true;
                                newPrevEmpDetails.CreatedUser = HttpContext.Current.User.Identity.Name;
                                newPrevEmpDetails.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntities.PreviousEmploymentDetails.Add(newPrevEmpDetails);
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                        }
                    }

                    var duplicateReference = hrmsEntities.ProfessionalReferences.Where(p => p.EmployeeId == details.empID).ToList();

                    foreach (ProfessionalReference detail in duplicateReference)
                    {
                        detail.IsActive = false;
                        hrmsEntities.SaveChanges();
                    }

                    foreach (ProfRefDetails prd in details.profReference)
                    {
                        if (!string.IsNullOrEmpty(prd.name))
                        {
                            ProfessionalReference profRef = hrmsEntities.ProfessionalReferences.SingleOrDefault(i => i.ID == prd.ID);
                            if (profRef != null)
                            {
                                profRef.Name = prd.name;
                                profRef.Designation = prd.designation;
                                profRef.CompanyName = prd.companyName;
                                profRef.CompanyAddress = prd.companyAddress;
                                profRef.OfficeEmailAddress = prd.officeEmailAddress;
                                profRef.MobileNo = prd.mobileNo;
                                profRef.IsActive = true;
                                profRef.ModifiedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                                profRef.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntities.Entry(profRef).State = EntityState.Modified;
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                            else
                            {
                                ProfessionalReference newProfRefrnce = new ProfessionalReference();
                                newProfRefrnce.EmployeeId = details.empID;
                                newProfRefrnce.Name = prd.name;
                                newProfRefrnce.Designation = prd.designation;
                                newProfRefrnce.CompanyName = prd.companyName;
                                newProfRefrnce.CompanyAddress = prd.companyAddress;
                                newProfRefrnce.OfficeEmailAddress = prd.officeEmailAddress;
                                newProfRefrnce.MobileNo = prd.mobileNo;
                                newProfRefrnce.IsActive = true;
                                newProfRefrnce.CreatedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                                newProfRefrnce.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntities.ProfessionalReferences.Add(newProfRefrnce);
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save employee details.");
            }

            return retValue;
        }
        #endregion

        #region GetEmploymentDetailsByID
        /// <summary>
        /// Get Employement Details by ID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public IEnumerable<object> GetEmploymentDetailsByID(int empID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var getEmploymentDetails = (from ed in hrmEntities.PreviousEmploymentDetails.AsEnumerable()
                                                where ed.EmployeeId == empID && ed.IsActive == true
                                                select new EmploymentDetails
                                                {
                                                    ID = ed.ID,
                                                    name = ed.Name,
                                                    address = ed.Address,
                                                    designation = ed.Designation,
                                                    serviceFrom = ed.ServiceFrom,
                                                    serviceTo = ed.ServiceTo,
                                                    leavingReson = ed.LeavingReson,
                                                    fromYear = string.Format("{0:MM/yyyy}", ed.ServiceFrom),
                                                    toYear = string.Format("{0:MM/yyyy}", ed.ServiceTo)
                                                }).ToList();

                    if (getEmploymentDetails.Count > 0)
                        return getEmploymentDetails;

                    else
                        return Enumerable.Empty<object>().ToList();

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get employee details.");
            }
        }
        #endregion

        #region UpdateDesignationToAssociate
        /// <summary>
        /// Updates the designation for a associate
        /// </summary>
        /// <param name="designationData"></param>
        /// <returns>bool</returns>
        public bool UpdateDesignationToAssociate(AssociateDesignationData designationData)
        {
            bool status = false;

            using (APEntities hrmsEntity = new APEntities())
            {

                using (var dbContext = hrmsEntity.Database.BeginTransaction())
                {
                    try
                    {
                        AssociateDesignation designationDetails = hrmsEntity.AssociateDesignations.Where(designations => designations.EmployeeId == designationData.EmployeeID).OrderByDescending(designations => designations.ID).FirstOrDefault();
                        Employee employeeData = hrmsEntity.Employees.Where(employee => employee.EmployeeId == designationData.EmployeeID).FirstOrDefault();
                        DateTime? dateTo = designationData.ToDate.Value.AddDays(-1);
                        AssociateDesignation designation = new AssociateDesignation();
                        if (designationDetails == null)
                        {
                            designation.EmployeeId = employeeData.EmployeeId;
                            designation.DesignationId = employeeData.Designation;
                            designation.GradeId = employeeData.GradeId;
                            designation.FromDate = employeeData.JoinDate;
                            designation.ToDate = dateTo;
                            designation.CreatedBy = HttpContext.Current.User.Identity.Name;
                            designation.CreatedDate = DateTime.Now;
                            designation.SystemInfo = Commons.GetClientIPAddress();

                            hrmsEntity.AssociateDesignations.Add(designation);
                        }
                        else
                        {
                            DateTime? fromDate = designationDetails.ToDate.Value.AddDays(1);
                            designation.EmployeeId = employeeData.EmployeeId;
                            designation.DesignationId = employeeData.Designation;
                            designation.GradeId = employeeData.GradeId;
                            designation.FromDate = fromDate;
                            designation.ToDate = dateTo;
                            designation.CreatedBy = HttpContext.Current.User.Identity.Name;
                            designation.CreatedDate = DateTime.Now;
                            designation.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntity.AssociateDesignations.Add(designation);
                        }

                        if (employeeData != null)
                        {
                            employeeData.Designation = designationData.DesignationID;
                            employeeData.GradeId = designationData.GradeID;
                            employeeData.ModifiedUser = HttpContext.Current.User.Identity.Name;
                            employeeData.ModifiedDate = DateTime.Now;
                            employeeData.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntity.Entry(employeeData).State = EntityState.Modified;
                        }
                        status = hrmsEntity.SaveChanges() > 0 ? true : false;
                        dbContext.Commit();
                        return status;
                    }
                    catch (Exception ex)
                    {
                        dbContext.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region GetDesignationDetailsByID
        /// <summary>
        /// Get DesignationDetails by ID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public object GetDesignationDetailsByID(int employeeID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var getEmploymentDetails = (from employee in hrmEntities.Employees
                                                where employee.EmployeeId == employeeID && employee.IsActive == true
                                                select new
                                                {
                                                    employee.EmployeeId,
                                                    employee.EmployeeCode,
                                                    Name = employee.FirstName + " " + employee.LastName,
                                                    JoinDate = employee.JoinDate ==  null ? null : SqlFunctions.DateName("day", employee.JoinDate) + "/" + SqlFunctions.StringConvert((double)employee.JoinDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", employee.JoinDate),
                                                    DesignationHistory = (from designationData in hrmEntities.AssociateDesignations
                                                                          join employeeDesignation in hrmEntities.Employees on employee.EmployeeId equals employeeDesignation.EmployeeId
                                                                          join designation in hrmEntities.Designations on designationData.DesignationId equals designation.DesignationId
                                                                          where designationData.EmployeeId == employeeID
                                                                          select new
                                                                          {
                                                                              designation.DesignationName,
                                                                              FromDate = designationData.FromDate == null ? null : SqlFunctions.DateName("day", designationData.FromDate) + "/" + SqlFunctions.StringConvert((double)designationData.FromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", designationData.FromDate),
                                                                              ToDate = designationData.ToDate == null ? null : SqlFunctions.DateName("day", designationData.ToDate) + "/" + SqlFunctions.StringConvert((double)designationData.ToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", designationData.ToDate),
                                                                          }).ToList()
                                                }).ToList();


                    return getEmploymentDetails;

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get employee details.");
            }
        }
        #endregion

        #endregion

        #region Assign Reporting Manager

        public bool AssignReportingManager(EmploymentDetails employeeData)
        {
            bool retValue = false;
            //
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    Employee emp = hrmsEntities.Employees.Where(e => e.EmployeeId == employeeData.empID).Where(e => e.IsActive == true).FirstOrDefault();
                    if (emp != null)
                    {
                        emp.ReportingManager = employeeData.ReportingManagerId;
                        emp.ModifiedUser = HttpContext.Current.User.Identity.Name;
                        emp.ModifiedDate = DateTime.Now;
                        hrmsEntities.Entry(emp).State = EntityState.Modified;
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "[]");
                throw new AssociatePortalException("Failed to assign reporting manager.");
            }
            return retValue;
        }

        public IEnumerable<object> GetReportingManagers()
        {
            IEnumerable<object> returnObject = null;

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var query = (from emp in hrmsEntity.Employees
                                 join projectmgrs in hrmsEntity.ProjectManagers on emp.EmployeeId equals projectmgrs.ReportingManagerID
                                 where emp.IsActive == true && projectmgrs.IsActive == true
                                 select new EmploymentDetails
                                 {
                                     empID = emp.EmployeeId,
                                     FirstName = emp.FirstName,
                                     LastName = emp.LastName
                                 });

                    var leads = query.ToList<EmploymentDetails>();

                    query = (from emp in hrmsEntity.Employees
                             join projectmgrs in hrmsEntity.ProjectManagers on emp.EmployeeId equals projectmgrs.ReportingManagerID
                             where emp.IsActive == true && projectmgrs.IsActive == true
                             select new EmploymentDetails
                             {
                                 empID = emp.EmployeeId,
                                 FirstName = emp.FirstName,
                                 LastName = emp.LastName
                             });
                    var managers = query.ToList<EmploymentDetails>();

                    if (leads == null) leads = new List<EmploymentDetails>();
                    if (managers != null) leads.AddRange(managers);
                    var list = leads.GroupBy(e => new { e.empID, e.FirstName, e.LastName }).Select(e => new { empID = e.Key.empID, FirstName = e.Key.FirstName, LastName = e.Key.LastName, name = e.Key.FirstName + " " + e.Key.LastName }).ToList();
                    //
                    returnObject = list.OrderBy(e => e.name).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get reporting manager details.");
            }

            return returnObject;
        }

        #endregion
    }
}
