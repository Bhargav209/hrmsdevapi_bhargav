﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.DataStorage;
using AP.Utility;

namespace AP.API
{
    public class ProjectTrainingMaster
    {
        #region ProjectTrainingMasterMethods

        #region GetTrainingData
        /// <summary>
        /// Get Training Data from system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetTrainingData()
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var trainingList = (from pt in hrmEntities.ProjectTrainings                                        
                                        join pr in hrmEntities.Projects on pt.ProjectId equals pr.ProjectId into prgrp
                                        from prlist in prgrp.DefaultIfEmpty()
                                        where prlist.IsActive == true && pt.IsActive == true
                                        select new
                                        {
                                            pt.ProjectTrainingId,
                                            pt.ProjectTrainingCode,
                                            pt.ProjectTrainingName,
                                            pt.ProjectTrainingType,
                                            pt.IsActive,
                                            prlist.ProjectId,
                                            prlist.ProjectName
                                        }).OrderBy(x => x.ProjectTrainingName).ToList();

                    //var mergedResult = from row in trainingList.ToList()
                    //                   group row.ProjectName by new { row.ProjectTrainingId, row.ProjectTrainingCode, row.ProjectTrainingName, row.IsActive } into grp
                    //                   select new { grp.Key.TrainingId, grp.Key.TrainingCode, grp.Key.ProjectTrainingName, grp.Key.IsActive, Projects = string.Join(",", grp) };

                    return trainingList.ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region GetProjects
        /// <summary>
        /// Get Projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetProjects()
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var projectsList = (from p in hrmEntities.Projects
                                        where p.IsActive == true
                                        select new
                                        {
                                            p.ProjectId,
                                            p.ProjectName
                                        }).OrderBy(x => x.ProjectName).ToList();

                    return projectsList;
                }
            }
            catch
            {
                throw new AssociatePortalException("Faile to get projects");
            }
        }
        #endregion        

        #region GetTrainingDataById
        /// <summary>
        /// Get Training Data from system
        /// </summary>
        /// <returns></returns>
        public object GetTrainingDataById(int trainingId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var trainingDetails = (from pt in hrmEntities.ProjectTrainings
                                           join pr in hrmEntities.Projects on pt.ProjectId equals pr.ProjectId into prgrp
                                           from prlist in prgrp.DefaultIfEmpty()
                                           where prlist.IsActive == true && pt.ProjectTrainingId == trainingId
                                           select new
                                           {
                                               pt.ProjectTrainingId,
                                               pt.ProjectTrainingCode,
                                               pt.ProjectTrainingName,
                                               pt.ProjectTrainingType,
                                               pt.IsActive,
                                               prlist.ProjectId,
                                               prlist.ProjectName
                                           }).FirstOrDefault();

                    return trainingDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get trailning details");
            }
        }
        #endregion        

        #region CreateNewTraining
        /// <summary>
        /// Method to Create New Training
        /// </summary>
        /// <param name="newTraining"></param>
        /// <returns></returns>
        public bool CreateNewTraining(ProjectTrainingMasterData newTraining)
        {
            bool result = false;
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var isExists = (from t in hrmEntities.ProjectTrainings
                                    where t.ProjectTrainingCode == newTraining.ProjectTrainingCode && t.IsActive == true
                                    select t).Count();

                    if (isExists == 0)
                    {
                        ProjectTraining projectTraining = new ProjectTraining();
                        projectTraining.ProjectTrainingCode = newTraining.ProjectTrainingCode;
                        projectTraining.ProjectTrainingName = newTraining.ProjectTrainingName;
                        projectTraining.ProjectTrainingType = newTraining.ProjectTrainingType;
                        projectTraining.ProjectId = newTraining.ProjectId;
                        // projectTraining.IsActive = newTraining.IsActive; 
                        projectTraining.IsActive = true;
                        projectTraining.CreatedUser = newTraining.CurrentUser;                        
                        projectTraining.CreatedDate = DateTime.Now;
                        projectTraining.SystemInfo = newTraining.SystemInfo;
                        hrmEntities.ProjectTrainings.Add(projectTraining);
                        result = hrmEntities.SaveChanges() > 0 ? true : false;                       
                    }
                    else
                        throw new AssociatePortalException("Training code already exists");
                }

            }
            catch
            {
                throw new AssociatePortalException("Failed to save training details");
            }

            return result;
        }
        #endregion        

        #region UpdateTrainingDetails
        /// <summary>
        /// Update Training details
        /// </summary>
        /// <param name="TrainingData"></param>
        /// <returns></returns>
        public bool UpdateTrainingDetails(ProjectTrainingMasterData trainingData)
        {
            bool IsValid = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from t in hrmsEntities.ProjectTrainings
                                    where t.ProjectTrainingCode == trainingData.ProjectTrainingCode && t.ProjectTrainingId != trainingData.ProjectTrainingId
                                    select t).ToList().Count();

                    if (isExists == 0)
                    {
                        ProjectTraining projectTraining = hrmsEntities.ProjectTrainings.FirstOrDefault(i => i.ProjectTrainingId == trainingData.ProjectTrainingId);
                        projectTraining.ProjectTrainingCode = trainingData.ProjectTrainingCode;
                        projectTraining.ProjectTrainingName = trainingData.ProjectTrainingName;
                        projectTraining.ProjectTrainingType = trainingData.ProjectTrainingType;
                        projectTraining.ProjectId = trainingData.ProjectId;
                        projectTraining.IsActive = trainingData.IsActive;
                        projectTraining.ModifiedUser = trainingData.CurrentUser;
                        projectTraining.ModifiedDate = DateTime.Now;
                        projectTraining.SystemInfo = trainingData.SystemInfo;
                        IsValid = hrmsEntities.SaveChanges() > 0 ? true : false;                      
                    }
                    else
                        throw new AssociatePortalException("Training code already exists");
                }

            }
            catch
            {
                throw new AssociatePortalException("Failed to update training details");
            }

            return IsValid;
        }
        #endregion

        #region GetTrainingDataByProjectId
        /// <summary>
        /// Get Training Data from system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetTrainingDataByProjectId(int projectId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var trainingList = (from t in hrmEntities.ProjectTrainings                                         
                                        where t.IsActive == true && t.ProjectId == projectId
                                        select new
                                        {
                                            t.ProjectTrainingId,
                                            t.ProjectTrainingCode,
                                            t.ProjectTrainingName
                                        }).ToList();

                    if (trainingList.Count > 0)
                        return trainingList;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get training details");
            }
        }
        #endregion        

        #endregion
    }
}
