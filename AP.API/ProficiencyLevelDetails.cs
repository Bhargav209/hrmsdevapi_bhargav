﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.DataStorage;
using AP.Utility;

namespace AP.API
{
    public class ProficiencyLevelDetails
    {
        #region CreateProficiencyLevel
        /// <summary>
        /// CreateProficiencyLevel
        /// </summary>
        /// <param name="proficiencyLevelData"></param>
        /// <returns></returns>
        public bool CreateProficiencyLevel(ProficiencyLevelData proficiencyLevelData)
        {
            bool isCreated = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from p in hrmsEntities.ProficiencyLevels
                                    where p.ProficiencyLevelCode == proficiencyLevelData.ProficiencyLevelCode
                                    select p).Count();
                    if (isExists == 0)
                    {

                        ProficiencyLevel proficiencyLevel = new ProficiencyLevel();
                        proficiencyLevel.ProficiencyLevelCode = proficiencyLevelData.ProficiencyLevelCode;
                        proficiencyLevel.ProficiencyLevelDescription = proficiencyLevelData.ProficiencyLevelDescription;
                        proficiencyLevel.IsActive = true;
                        proficiencyLevel.CreatedUser = proficiencyLevelData.CurrentUser;
                        proficiencyLevel.CreatedDate = DateTime.Now;
                        proficiencyLevel.SystemInfo = proficiencyLevelData.SystemInfo;
                        hrmsEntities.ProficiencyLevels.Add(proficiencyLevel);
                        isCreated = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Proficiency level code already exists");
                }
            }
            catch
            {
                throw;
            }

            return isCreated;
        }
        #endregion

        #region UpdateProficiencyLevel
        /// <summary>
        /// UpdateProficiencyLevel
        /// </summary>
        /// <param name="proficiencyLevelData"></param>
        /// <returns></returns>
        public bool UpdateProficiencyLevel(ProficiencyLevelData proficiencyLevelData)
        {
            bool isUpdated = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from p in hrmsEntities.ProficiencyLevels
                                    where p.ProficiencyLevelCode == proficiencyLevelData.ProficiencyLevelCode && p.ProficiencyLevelId != proficiencyLevelData.ProficiencyLevelId
                                    select p).Count();
                    if (isExists == 0)
                    {
                        ProficiencyLevel proficiencyLevel = hrmsEntities.ProficiencyLevels.FirstOrDefault(pid => pid.ProficiencyLevelId == proficiencyLevelData.ProficiencyLevelId);
                        proficiencyLevel.ProficiencyLevelCode = proficiencyLevelData.ProficiencyLevelCode;
                        proficiencyLevel.ProficiencyLevelDescription = proficiencyLevelData.ProficiencyLevelDescription;
                        proficiencyLevel.IsActive = true;
                        proficiencyLevel.ModifiedUser = proficiencyLevelData.CurrentUser;
                        proficiencyLevel.ModifiedDate = DateTime.Now;
                        proficiencyLevel.SystemInfo = proficiencyLevelData.SystemInfo;
                        hrmsEntities.Entry(proficiencyLevel).State = System.Data.Entity.EntityState.Modified;
                        isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Proficiency level code already exists");
                }
            }
            catch
            {
                throw;
            }

            return isUpdated;
        }
        #endregion

        #region GetProficiencyLevels
        /// <summary>
        /// GetProficiencyLevels
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetProficiencyLevels(bool isActive)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var proficiencyLevels = hrmsEntities.ProficiencyLevels.Select(p =>
                        new
                        {
                            ID = p.ProficiencyLevelId,
                            Name = p.ProficiencyLevelCode,
                            p.ProficiencyLevelId,
                            p.ProficiencyLevelCode,
                            p.ProficiencyLevelDescription,
                            IsActive = p.IsActive == true ? "Yes" : "No"
                        }).OrderBy(plevel => plevel.ProficiencyLevelCode).ToList();

                    if (isActive)
                        proficiencyLevels = proficiencyLevels.Where(i => i.IsActive == "Yes").ToList();

                    if (proficiencyLevels.Count > 0)
                        return proficiencyLevels;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
