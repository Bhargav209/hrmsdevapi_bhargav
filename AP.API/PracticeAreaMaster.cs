﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;

namespace AP.API
{
    public class PracticeAreaMaster
    {
        #region GetPracticeAreas
        /// <summary>
        /// To get the Practice area list
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetPracticeAreas()
        {           
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {                    
                        var  practiceAreas= hrmEntities.Database.SqlQuery<PracticeAreaDetails>
                               ("[usp_GetPracticeAreas] ",
                                   new object[] { }
                                   ).ToList();                        
                        return practiceAreas;                                      
                }
            }
            catch
            {
                throw;
            }           
        }
        #endregion

        #region Practice Area
        /// <summary>
        /// this method will insert the data for practice according to new design
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool AddPracticeArea(PracticeAreaDetails practiceAreaDetails)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from p in hrmsEntities.PracticeAreas
                                    where p.PracticeAreaCode == practiceAreaDetails.PracticeAreaCode
                                    select p).Count();
                    if (isExists == 0)
                    {
                        PracticeArea practiceArea = new PracticeArea();
                        practiceArea.PracticeAreaCode = practiceAreaDetails.PracticeAreaCode;
                        practiceArea.PracticeAreaDescription = practiceAreaDetails.PracticeAreaDescription;
                        practiceArea.IsActive = true;
                        practiceArea.CreatedUser = practiceAreaDetails.CurrentUser;
                        practiceArea.CreatedDate = DateTime.Now;
                        practiceArea.SystemInfo = practiceAreaDetails.SystemInfo;
                        hrmsEntities.PracticeAreas.Add(practiceArea);
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Practice area code already exists");
                }
            }
            catch
            {
                throw;
            }

            return retValue;
        }
        #endregion

        #region UpdatePracticeArea
        /// <summary>
        /// this method will update the data for practice area
        /// </summary>
        /// <param name="practiceAreaDetails"></param>
        /// <returns></returns>
        public bool UpdatePracticeArea(PracticeAreaDetails practiceAreaDetails)
        {
            bool isUpdated = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from p in hrmsEntities.PracticeAreas
                                    where p.PracticeAreaCode == practiceAreaDetails.PracticeAreaCode && p.PracticeAreaId!=practiceAreaDetails.PracticeAreaId
                                    select p).Count();
                    if (isExists == 0)
                    {

                        PracticeArea practiceArea = hrmsEntities.PracticeAreas.FirstOrDefault(cid => cid.PracticeAreaId == practiceAreaDetails.PracticeAreaId);
                    practiceArea.PracticeAreaCode = practiceAreaDetails.PracticeAreaCode;
                    practiceArea.PracticeAreaDescription = practiceAreaDetails.PracticeAreaDescription;
                    practiceArea.IsActive = true;
                    practiceArea.ModifiedUser = practiceAreaDetails.CurrentUser;
                    practiceArea.ModifiedDate = DateTime.Now;
                    practiceArea.SystemInfo = practiceAreaDetails.SystemInfo;
                    hrmsEntities.Entry(practiceArea).State = System.Data.Entity.EntityState.Modified;
                    isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Practice area code already exists");
                }
            }
            catch
            {
                throw;
            }

            return isUpdated;
        } 
        #endregion

    }
}
