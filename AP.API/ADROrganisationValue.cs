﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;


namespace AP.API
{
    public class ADROrganisationValue
    {
        #region GetADROrganisationValue
        /// <summary>
        /// Gets GetADROrganisationValue Master
        /// </summary>
        /// <returns></returns>
        public async Task<List<ADROrganisationValueData>> GetADROrganisationValues(int financialYearId)
        {
            List<ADROrganisationValueData> lstOrgValues;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstOrgValues = await apEntities.Database.SqlQuery<ADROrganisationValueData>
                              ("[usp_GetADROrganisationValues] @FinancialYearId",
                                   new object[] {
                                        new SqlParameter ("FinancialYearId", financialYearId),
                                   }
                                   ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstOrgValues;
        }
        #endregion

        #region CreateADROrganisationValue
        /// <summary>
        /// Create a new ADROrganisationValue
        /// </summary>
        /// <param name="organisationValueData"></param>
        /// <returns></returns>
        public async Task<int> CreateADROrganisationValue(ADROrganisationValueData organisationValueData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateADROrganisationValue] @ADROrganisationValue,@FinancialYearId, @CreatedDate, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ADROrganisationValue", organisationValueData.ADROrganisationValue),
                                        new SqlParameter ("FinancialYearId", organisationValueData.FinancialYearId),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region Update ADROrganisationValue
        /// <summary>
        /// Update ADROrganisationValue
        /// </summary>
        /// <param name="organisationValueData"></param>
        /// <returns></returns>
        public async Task<int> UpdateADROrganisationValue(ADROrganisationValueData organisationValueData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateADROrganisationValue] @ADROrganisationValueID, @ADROrganisationValue, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("ADROrganisationValueID", organisationValueData.ADROrganisationValueID ),
                                        new SqlParameter ("ADROrganisationValue", organisationValueData.ADROrganisationValue),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
