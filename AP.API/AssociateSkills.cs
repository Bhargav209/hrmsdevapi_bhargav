﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.Web;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Threading.Tasks;
using AP.Notification;
using System.Net;

namespace AP.API
{
    public class AssociateSkills
    {
        #region SkillMasterMethods

        #region CreateSkillsMaster
        /// <summary>
        /// method to create Skill
        /// </summary>
        /// <param name="skillData"></param>
        /// <returns></returns>
        public bool CreateSkillsMaster(SkillData skillData)
        {
            bool skillRetValue = false;
            skillData.IsApproved = true;
            using (APEntities hrmEntities = new APEntities())
            {

                IQueryable<Skill> skillQuery = (from s in hrmEntities.Skills
                                                where s.CompetencyAreaId == skillData.CompetencyAreaID && s.SkillCode.ToLower().Trim() == skillData.SkillCode.ToLower().Trim()
                                                select s);
                int isSkillCodeExist = skillQuery.Count();
                if (isSkillCodeExist > 0)
                {
                    throw new AssociatePortalException(skillQuery.FirstOrDefault().SkillCode + "Skill code already exists");
                }
                Skill skill = new Skill();
                skill.SkillCode = skillData.SkillCode;
                skill.SkillName = skillData.SkillCode;
                skill.SkillDescription = skillData.SkillDescription;
                skill.CompetencyAreaId = skillData.CompetencyAreaID;
                skill.SkillGroupId = skillData.SkillGroupID;
                skill.IsApproved = skillData.IsApproved;
                skill.CreatedUser = skillData.CurrentUser;
                skill.CreatedDate = DateTime.Now;
                skill.IsActive = true;
                skill.SystemInfo = skillData.SystemInfo;
                hrmEntities.Skills.Add(skill);
                skillRetValue = hrmEntities.SaveChanges() > 0 ? true : false;

            }

            return skillRetValue;
        }
        #endregion

        #region GetSkillsData
        /// <summary>
        /// Get Skill details from system
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetSkillsData(bool isActive)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skillData = (from a in hrmEntities.Skills
                                     join c in hrmEntities.CompetencyAreas
                                     on a.CompetencyAreaId equals c.CompetencyAreaId into skill
                                     from data in skill.DefaultIfEmpty()
                                     join sg in hrmEntities.SkillGroups on a.SkillGroupId equals sg.SkillGroupId into skillgroups
                                     from skillgroup in skillgroups.DefaultIfEmpty()
                                     select new { a.SkillId, a.SkillCode, a.SkillDescription, a.CompetencyAreaId, a.IsActive, a.IsApproved, data.CompetencyAreaCode, skillgroup.SkillGroupName, a.SkillGroupId }).OrderBy(s => s.SkillCode).ToList();

                    if (isActive)
                        skillData = skillData.Where(i => i.IsActive == true).ToList();

                    if (skillData.Count > 0)
                        return skillData;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion
        #region GetProjectDetailByEmployeeId
        /// <summary>
        /// Get Project Detail By EmployeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetProjectDetailByEmployeeId(int employeeId)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    var lstProjects = apEntities.Database.SqlQuery<AssociateAllocationDetails>
                              ("[usp_GetProjectDetailByEmployeeId] @EmployeeId",

                               new object[] {
                                        new SqlParameter ("EmployeeId", employeeId)
                             }).ToList();
                    return lstProjects;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Project details");
            }

        }
        #endregion
        #region GetSkillsData
        /// <summary>
        /// Get active and approved Skill details from system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetActiveSkillsData()
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skillData = (from s in hrmEntities.Skills
                                     where s.IsActive == true && s.IsApproved == true
                                     select new { s.SkillId, s.SkillCode, s.SkillDescription, s.IsActive, s.IsApproved, s.SkillName }).ToList();

                    if (skillData.Count > 0)
                        return skillData;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region UpdateSkillMasterDetails
        /// <summary>
        /// Update Skill details
        /// </summary>
        /// <param name="skillData"></param>
        /// <returns></returns>
        public bool UpdateSkillMasterDetails(SkillData skillData)
        {
            bool modifiedSkillRetValue = false;
            skillData.IsApproved = true;

            using (APEntities hrmEntities = new APEntities())
            {

                //Check whether the skill code already exist for the competency
                IQueryable<Skill> skillQuery = (from s in hrmEntities.Skills
                                                where s.SkillId != skillData.SkillId && s.SkillCode == skillData.SkillCode && s.CompetencyAreaId == skillData.CompetencyAreaID
                                                select s);
                int isSkillExists = skillQuery.Count();
                if (isSkillExists > 0 && (bool)skillData.IsActive)
                {
                    throw new AssociatePortalException("Skill code " + skillData.SkillCode + " already exists in the competency " + skillData.Name);
                }

                Skill skills = (from s in hrmEntities.Skills
                                where s.SkillId == skillData.SkillId
                                select s).FirstOrDefault();

                skills.SkillCode = skillData.SkillCode;
                skills.SkillName = skillData.SkillCode;
                skills.SkillDescription = skillData.SkillDescription;
                skills.CompetencyAreaId = skillData.CompetencyAreaID;
                skills.SkillGroupId = skillData.SkillGroupID;
                skills.IsActive = true;
                skills.IsApproved = skillData.IsApproved;
                skills.ModifiedUser = skillData.CurrentUser;
                skills.ModifiedDate = DateTime.Now;
                skills.SystemInfo = skillData.SystemInfo;
                hrmEntities.Entry(skills).State = System.Data.Entity.EntityState.Modified;
                modifiedSkillRetValue = hrmEntities.SaveChanges() > 0 ? true : false;
            }

            return modifiedSkillRetValue;
        }
        #endregion

        #endregion

        #region Project Tab Methods

        #region AddAssociateProjects
        /// <summary>
        /// AddAssociateProjects
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool AddAssociateProjects(UserDetails details)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    EmployeeProject empProject = new EmployeeProject();

                    foreach (EmployeeProjectDetails employeeProjectDetails in details.projects)
                    {
                        empProject.EmployeeId = details.empID;
                        empProject.OrganizationName = employeeProjectDetails.organizationName;
                        empProject.ProjectName = employeeProjectDetails.projectName;
                        empProject.RoleMasterId = employeeProjectDetails.RoleMasterId;
                        empProject.Duration = employeeProjectDetails.duration;
                        empProject.KeyAchievements = employeeProjectDetails.keyAchievement;
                        empProject.DomainID = employeeProjectDetails.DomainID;
                        empProject.IsActive = employeeProjectDetails.IsActive;
                        empProject.CreatedUser = details.CurrentUser;
                        empProject.CreatedDate = DateTime.Now;
                        empProject.SystemInfo = details.SystemInfo;
                        hrmsEntities.EmployeeProjects.Add(empProject);
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to add project details.");
            }

            return retValue;
        }
        #endregion

        #region GetAssociateSkillsByID
        /// <summary>
        /// Get Associate Skills Details By EmpID
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssociateSkillsByID(int empID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skills = (from es in hrmEntities.EmployeeSkills
                                  where es.EmployeeId == empID
                                  select new EmployeeSkillDetails
                                  {
                                      ID = es.ID,
                                      empID = es.EmployeeId,
                                      skillID = es.SkillId,
                                      //domain = es.Domain,
                                      experience = es.Experience,
                                      //proficiencyLevel = es.ProficiencyLevel,
                                      //isPrimary = es.PrimarySkillFlag
                                  }).ToList();

                    return skills;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetAssociateProjectDetailsByID
        /// <summary>
        /// Get Associate Project Details ByID
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssociateProjectDetailsByID(int empID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var projects = (from ep in hrmEntities.EmployeeProjects
                                    join domain in hrmEntities.Domains on ep.DomainID equals domain.DomainID
                                    where ep.EmployeeId == empID && ep.IsActive == true
                                    select new EmployeeProjectDetails
                                    {
                                        ID = ep.ID,
                                        empID = ep.EmployeeId,
                                        organizationName = ep.OrganizationName,
                                        projectName = ep.ProjectName,
                                        duration = ep.Duration,
                                        RoleMasterId = ep.RoleMasterId,
                                        DomainID = domain.DomainID,
                                        keyAchievement = ep.KeyAchievements
                                    }).ToList();

                    return projects;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region UpdateAssociateProjectDetails
        /// <summary>
        /// Update Associate Project Details
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool UpdateAssociateProjectDetails(UserDetails details)
        {
            bool modifiedRetValue = false;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    IEnumerable<object> existingProjects = hrmEntities.EmployeeProjects.Where(id => id.EmployeeId == details.empID).ToList();
                    foreach (EmployeeProject employeeProject in existingProjects)
                    {
                        hrmEntities.EmployeeProjects.Remove(employeeProject);
                        hrmEntities.SaveChanges();
                    }
                    foreach (EmployeeProjectDetails employeeProjectDetails in details.projects)
                    {
                        EmployeeProject empProjectDetails = hrmEntities.EmployeeProjects.Where(emp => emp.EmployeeId == details.empID && emp.DomainID == employeeProjectDetails.DomainID && emp.OrganizationName.Trim().ToLower() == employeeProjectDetails.organizationName.Trim().ToLower() && emp.ProjectName.Trim().ToLower() == employeeProjectDetails.projectName.Trim().ToLower()).FirstOrDefault();

                        if (empProjectDetails == null)
                        {
                            EmployeeProject empProject = new EmployeeProject();
                            empProject.EmployeeId = details.empID;
                            empProject.OrganizationName = employeeProjectDetails.organizationName;
                            empProject.ProjectName = employeeProjectDetails.projectName;
                            empProject.RoleMasterId = employeeProjectDetails.RoleMasterId;
                            empProject.Duration = employeeProjectDetails.duration;
                            empProject.KeyAchievements = employeeProjectDetails.keyAchievement;
                            empProject.DomainID = employeeProjectDetails.DomainID;
                            empProject.IsActive = true;
                            empProject.CreatedUser = details.CurrentUser;
                            empProject.ModifiedDate = DateTime.Now;
                            empProject.CreatedDate = DateTime.Now;
                            empProject.SystemInfo = details.SystemInfo;
                            hrmEntities.EmployeeProjects.Add(empProject);
                            modifiedRetValue = hrmEntities.SaveChanges() > 0 ? true : false;
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to upadate project details.");
            }

            return modifiedRetValue;
        }
        #endregion

        #endregion

        #region GetCompetencyArea
        /// <summary>
        /// to get the competency area from CompetencyAreas context
        /// </summary>
        /// <returns></returns>

        public IEnumerable<object> GetCompetencyArea()
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var competencyAreaData = (from ca in hrmEntities.CompetencyAreas
                                              select new { ca.CompetencyAreaId, ca.CompetencyAreaCode, ca.CompetencyAreaDescription, ca.IsActive }).Where(id => id.IsActive == true);


                    return competencyAreaData.ToList();

                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);

                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetCompetencySkils
        /// <summary>
        /// to get the skills related to roles and competency area 
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="competencyAreaID"></param>
        /// <returns></returns>

        public IEnumerable<object> GetCompetencySkils(int roleID, int competencyAreaID)
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var GetCompetencySkilsData = (from skills in hrmEntities.Skills
                                                  join competencySkill in hrmEntities.CompetencySkills on skills.SkillId equals competencySkill.SkillId
                                                  join roles in hrmEntities.Roles on competencySkill.RoleMasterID equals roles.RoleId
                                                  join competencyArea in hrmEntities.CompetencyAreas on competencySkill.CompetencyAreaId equals competencyArea.CompetencyAreaId
                                                  where skills.IsActive == true && roles.RoleId == roleID && competencyArea.CompetencyAreaId == competencyAreaID
                                                  select new { skills.SkillId, skills.SkillCode, skills.SkillDescription, skills.IsActive, skills.IsApproved, skills }).Where(id => id.IsActive == true && id.IsApproved == true);


                    return GetCompetencySkilsData.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);

                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region ProficiencyLevel
        public IEnumerable<object> GetProficiencyLevel()
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var GetProficiencyLevelData = (from proficiencyLevel in hrmEntities.ProficiencyLevels
                                                   select new { proficiencyLevel.ProficiencyLevelId, proficiencyLevel.ProficiencyLevelCode, proficiencyLevel.ProficiencyLevelDescription, proficiencyLevel.IsActive }).Where(id => id.IsActive == true);


                    return GetProficiencyLevelData.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);

                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region Add employee skills
        /// <summary>
        /// this method will insert the data for skills according to new design
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public int AddAssociateSkills(EmployeeSkillDetails skillDetails)
        {
            int rowsAffected = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                using (var dbContext = hrmsEntities.Database.BeginTransaction())
                {
                    try
                    {

                        if (skillDetails.RoleId == 2) //for Associate and Lead work flow
                        {
                            rowsAffected = hrmsEntities.Database.SqlQuery<int>
                          ("[usp_ManageAssociateWorkFlow]  @EmployeeId, @CompetencyAreaId, @SkillId, @ProficiencyLevelId, @Experience, @LastUsed, @IsPrimary, @CreatedUser, @CreatedDate, @SystemInfo, @SkillGroupId",
                          new object[]
                          {
                                new SqlParameter("EmployeeId", skillDetails.empID),
                                new SqlParameter ("CompetencyAreaId", skillDetails.CompetencyAreaID),
                                new SqlParameter ("SkillId", skillDetails.skillID),
                                new SqlParameter ("ProficiencyLevelId", skillDetails.proficiencyLevelId),
                                new SqlParameter ("Experience", (object) skillDetails.experience ?? DBNull.Value ),
                                new SqlParameter ("LastUsed", (object) skillDetails.LastUsed ?? DBNull.Value),
                                new SqlParameter ("IsPrimary", skillDetails.isPrimary),  
                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                new SqlParameter ("SkillGroupId", skillDetails.SkillGroupID),
                        }).SingleOrDefault();
                            dbContext.Commit();

                        }
                        else  //For all Rest of Associate flow
                        {
                            rowsAffected = hrmsEntities.Database.SqlQuery<int>
                            ("[usp_ManageAssociateSkills] @ID,@EmployeeId, @CompetencyAreaId, @SkillId, @ProficiencyLevelId, @Experience, @LastUsed, @IsPrimary, @CreatedUser, @CreatedDate, @SystemInfo, @SkillGroupId",
                            new object[]
                            {
                                new SqlParameter ("ID", skillDetails.ID),
                                new SqlParameter("EmployeeId", skillDetails.empID),
                                new SqlParameter ("CompetencyAreaId", skillDetails.CompetencyAreaID),
                                new SqlParameter ("SkillId", skillDetails.skillID),
                                new SqlParameter ("ProficiencyLevelId", skillDetails.proficiencyLevelId),
                                new SqlParameter ("Experience", (object) skillDetails.experience ?? DBNull.Value ),
                                new SqlParameter ("LastUsed", (object) skillDetails.LastUsed ?? DBNull.Value),
                                //new SqlParameter ("Experience", skillDetails.experience),
                                //new SqlParameter ("LastUsed", skillDetails.LastUsed),
                                new SqlParameter ("IsPrimary", skillDetails.isPrimary),
                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                new SqlParameter ("SkillGroupId", skillDetails.SkillGroupID)
                            }).SingleOrDefault();
                            dbContext.Commit();
                        }
                    }
                    catch(Exception ex)
                    {
                        dbContext.Rollback();
                        throw;
                    }
                    return rowsAffected;
                }


            }
        }
        #endregion

        #region Get Skills
        public IEnumerable<object> GetSkills(int competenctAreaID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var SkillList = (from skill in hrmEntities.Skills
                                     join compArea in hrmEntities.CompetencyAreas on skill.CompetencyAreaId equals compArea.CompetencyAreaId
                                     where compArea.IsActive == true && skill.CompetencyAreaId == competenctAreaID
                                     && skill.IsActive == true && skill.IsApproved == true
                                     select new
                                     {
                                         SkillId = skill.SkillId,
                                         SkillName = skill.SkillCode
                                     }).OrderBy(s => s.SkillName);


                    return SkillList.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, null);

                throw new AssociatePortalException("Failed to get skills.");
            }
        }
        #endregion

        #region Get Skills by Employee ID
        public IEnumerable<object> GetSkillsById(int empId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var SkillList = (from employeeSkill in hrmEntities.EmployeeSkills
                                     where employeeSkill.EmployeeId == empId && employeeSkill.IsActive == true
                                     select new
                                     {
                                         CompetencyAreaID = employeeSkill.CompetencyAreaId,
                                         PracticeAreaID = employeeSkill.SkillGroupId,
                                         skills = (from skill in hrmEntities.EmployeeSkills
                                                   join s in hrmEntities.Skills on skill.SkillId equals s.SkillId
                                                   where skill.EmployeeId == empId && skill.CompetencyAreaId == employeeSkill.CompetencyAreaId && skill.IsActive == true
                                                   select new
                                                   {
                                                       SkillId = skill.SkillId,
                                                       SkillName = s.SkillCode,
                                                       proficiencyLevelId = skill.ProficiencyLevelId,
                                                       experience = skill.Experience,
                                                       LastUsed = skill.LastUsed,
                                                       isPrimary = skill.IsPrimary,
                                                       IsApproved = s.IsApproved
                                                   }),

                                         skillList = (from skill in hrmEntities.Skills
                                                      where skill.CompetencyAreaId == employeeSkill.CompetencyAreaId && skill.IsApproved == true
                                                      select new { skill.SkillId, SkillName = skill.SkillCode }),

                                     }).ToList().Distinct();

                    return SkillList.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, null);
                throw;
            }
        }
        #endregion

        #region GetSkills
        /// <summary>
        /// Get Skill details from system
        /// </summary>
        /// <returns></returns>
        /// This service was written for Angular2 application and is being consumed in Angular2 application only.
        public IEnumerable<object> GetSkills()
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skillData = (from skills in hrmEntities.Skills
                                     join skillGroup in hrmEntities.SkillGroups
                                     on skills.SkillGroupId equals skillGroup.SkillGroupId
                                     join competency in hrmEntities.CompetencyAreas
                                     on skills.CompetencyAreaId equals competency.CompetencyAreaId
                                     select new { skills.SkillId, skills.SkillCode, skills.SkillDescription, skills.CompetencyAreaId, IsActive = skills.IsActive == true ? "Yes" : "No", IsApproved = skills.IsApproved == true ? "Yes" : "No", Name = competency.CompetencyAreaCode, SkillGroupName = skillGroup.SkillGroupName, SkillGroupId = skills.SkillGroupId }).OrderBy(s => s.SkillCode).ToList();

                    if (skillData.Count > 0)
                        return skillData;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get skills.");
            }
        }
        #endregion

        #region GetAssociateSkills
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EmployeeSkillDetails>> GetAssociateSkills(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkill = await apEntities.Database.SqlQuery<EmployeeSkillDetails>
                              ("[usp_GetAssociateSkills] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", empID) }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get associate skills");
            }
            return lstSkill;
        }
        #endregion

        #region GetSkillsByID
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EmployeeSkillDetails>> GetSkillsByID(int skillGroupID, int competencyAreaID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkill = await apEntities.Database.SqlQuery<EmployeeSkillDetails>
                              ("[usp_GetSkillsByCompetencySkillGroupID] @CompetencyAreaId, @SkillGroupId",
                              new object[] {
                                        new SqlParameter ("CompetencyAreaId", competencyAreaID),
                                        new SqlParameter ("SkillGroupId", skillGroupID)}).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get associate skills");
            }
            return lstSkill;
        }
        #endregion

        #region DeleteAssociateSkill
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAssociateSkill(int id)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteAssociateSkill] @ID",
                                   new object[] {
                                        new SqlParameter ("ID", id)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting an employee skill.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetSkillsBySearchString
        /// <summary>
        /// Gets Skills based on the search string from system 
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<List<GenericType>> GetSkillsBySearchString(string searchString)
        {
            List<GenericType> lstAssociates;
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    lstAssociates = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetSkillsBySearchString] @SearchString",
                                new object[] {
                                    new SqlParameter("SearchString", searchString)
                                        }).ToListAsync();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get skills.");
            }
            return lstAssociates;
        }
        #endregion

        #region GetDraftApproveStatusSkills
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EmployeeSkillDetails>> GetDraftApproveStatusSkills(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkill = await apEntities.Database.SqlQuery<EmployeeSkillDetails>
                              ("[usp_GetApprovedSkills] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", empID) }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get associate skills");
            }
            return lstSkill;
        }
        #endregion

        #region GetPendingStatusSkills
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EmployeeSkillDetails>> GetPendingStatusSkills(int empID)
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkill = await apEntities.Database.SqlQuery<EmployeeSkillDetails>
                              ("[usp_GetPendingSkillsforApproval] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", empID) }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get associate skills");
            }
            return lstSkill;
        }
        #endregion


        #region SubmitAssociateSkills
        /// <summary>
        /// this method will Submits Associate skills to  Lead for approval
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public int SubmitAssociateSkills(EmployeeSkillDetails empskillDetails)
        {
            int rowsAffected = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                using (var dbContext = hrmsEntities.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (SkillDetails ed in empskillDetails.skillDetails)
                        {
                           

                            rowsAffected = hrmsEntities.Database.SqlQuery<int>
                          ("[usp_UpdateSkillStatusAssociateWorkFlow]  @EmployeeId, @SubmittedRequisitionId",
                          new object[]
                          {
                                new SqlParameter("EmployeeId", ed.empID),
                                new SqlParameter ("SubmittedRequisitionId", ed.ID)
                        }).SingleOrDefault();
                           
                        }
                        dbContext.Commit();
                        if (rowsAffected == 1)
                        {
                            EmailNotificationConfiguration emailNotificationConfig = new EmailNotificationConfiguration();
                            EmployeeSkillDetails employeeSkillDetails = new EmployeeSkillDetails();

                            //Send notification email to Lead for approval
                            emailNotificationConfig = hrmsEntities.Database.SqlQuery<EmailNotificationConfiguration>
                                        ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                          new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.SkillNotificationStatus.Pending)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.Skills)),
                                         }
                                        ).FirstOrDefault();

                            employeeSkillDetails = hrmsEntities.Database.SqlQuery<EmployeeSkillDetails>
                                         ("[usp_GetAssociateSkillsPendingApproval] @EmployeeID",
                                           new object[]  {
                                    new SqlParameter ("EmployeeID",  empskillDetails.empID),
                                          }
                                         ).FirstOrDefault();



                            if (emailNotificationConfig != null)
                            {
                                if (employeeSkillDetails != null)
                                {
                                    NotificationDetail notificationDetail = new NotificationDetail();
                                    StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

                                    emailContent = emailContent.Replace("AssociateName", employeeSkillDetails.empName)
                                                               .Replace("SkillName", employeeSkillDetails.SkillName)
                                                               .Replace("SkillGroupName", employeeSkillDetails.SkillGroupName)
                                                               .Replace("CompetencyArea", employeeSkillDetails.CompetencyArea)
                                                               .Replace("ProficiencyLevel", employeeSkillDetails.ProficiencyLevel);

                                    if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                                        throw new AssociatePortalException("Email From cannot be blank");
                                    notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                                    if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                                        throw new AssociatePortalException("Email To cannot be blank");
                                    notificationDetail.ToEmail = employeeSkillDetails.ToEmailAddress;

                                    notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                                    notificationDetail.Subject = emailNotificationConfig.EmailSubject + " " + employeeSkillDetails.empCode;

                                    notificationDetail.EmailBody = emailContent.ToString();
                                    NotificationManager.SendEmail(notificationDetail);
                                }
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        dbContext.Rollback();
                        throw;
                    }
                    return rowsAffected;
                }
            }
        }
        #endregion
    }
}
