﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using static AP.Utility.Enumeration;
using System;
using System.Web;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using AP.Notification;
using System.Text;

namespace AP.API
{
    public class TalentRequisitionDetails
    {
        #region Create Requisition 
        /// <summary>
        /// This method is used to create requisition
        /// </summary>
        /// <param name="talentRequistionData"></param>
        /// <returns></returns>
        public int CreateRequisition(TalentRequistionData talentRequisitionData)
        {
            bool returnValue = false;
            int? raisedBy = null;
            TalentRequisition talentRequisition = null;
            int trId = 0;

            try
            {
                //int reportingManagerId = GetReportingManager(talentRequisitionData);

                //if (reportingManagerId <= 0)
                //    throw new AssociatePortalException("You are not authorized to raise this Requisition.");

                using (APEntities hrmsEntities = new APEntities())
                {
                    if (talentRequisitionData.TalentRequisitionId > 0)
                    {                        
                        talentRequisition = AssignRequisitionData(talentRequisitionData);
                        talentRequisition.TRId = talentRequisitionData.TalentRequisitionId;
                        talentRequisition.ModifiedUser = talentRequisitionData.CurrentUser;
                        talentRequisition.ModifiedDate = DateTime.Now;
                        hrmsEntities.Entry(talentRequisition).State = EntityState.Modified;
                    }
                    else
                    {
                        //Generate TRCode
                        int trCount = hrmsEntities.TalentRequisitions.Where(month => month.CreatedDate.Value.Month == DateTime.Now.Month && month.CreatedDate.Value.Year == DateTime.Now.Year).Count();
                        trCount += 1;
                        talentRequisitionData.TRCode = "TR-" + DateTime.Now.Month.ToString("D2") + DateTime.Now.Year.ToString() + "-" + trCount.ToString("D4");

                        //TalentRequisition oldTalentRequisition = hrmsEntities.TalentRequisitions.AsNoTracking().Where(t => t.TRCode == talentRequisitionData.TRCode && t.IsActive == true).FirstOrDefault();
                        //if (oldTalentRequisition != null)
                        //    throw new AssociatePortalException("The Requisition Code {0} already exists!", oldTalentRequisition.TRCode);

                        talentRequisition = AssignRequisitionData(talentRequisitionData);
                        //Lead cannot raise the requistion, lead can only draft the requisition
                        ProjectData project = (from p in hrmsEntities.Projects.AsNoTracking()
                                               join projectmgrs in hrmsEntities.ProjectManagers.AsNoTracking()
                                               on p.ProjectId equals projectmgrs.ProjectID
                                               join dep in hrmsEntities.Departments.AsNoTracking()
                                               on p.DepartmentId equals dep.DepartmentId into dept
                                               from deptHead in dept.DefaultIfEmpty()
                                               where p.ProjectId == talentRequisitionData.ProjectId
                                               select new ProjectData
                                               {
                                                   ReportingManagerId = projectmgrs.ReportingManagerID,
                                                   ManagerId = (int)projectmgrs.ProgramManagerID
                                               }).FirstOrDefault();
                        if (project != null)
                        {
                            if (project.ManagerId == talentRequisitionData.EmployeeId)
                                raisedBy = project.ManagerId;
                            else if (project.DepartmentHeadId == talentRequisitionData.EmployeeId)
                                raisedBy = project.DepartmentHeadId;
                        }
                        talentRequisition.RaisedBy = raisedBy;
                        talentRequisition.CreatedUser = talentRequisitionData.CurrentUser;
                        talentRequisition.CreatedDate = DateTime.Now;
                        hrmsEntities.TalentRequisitions.Add(talentRequisition);
                    }
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    trId = talentRequisition.TRId;
                }
            }
            catch
            {
                throw;
            }

            return trId;
        }
        #endregion

        #region AssignRequisitionData
        /// <summary>
        /// AssignRequisitionData
        /// </summary>
        /// <param name="talentRequisitionData"></param>
        /// <returns></returns>
        private TalentRequisition AssignRequisitionData(TalentRequistionData talentRequisitionData)
        {
            int? defaultValue = null;
            using (APEntities hrmsEntities = new APEntities())
            {
                TalentRequisition talentRequisition = new TalentRequisition()
                {
                    DepartmentId = talentRequisitionData.DepartmentId,
                    PracticeAreaId = talentRequisitionData.PracticeAreaId.HasValue ? talentRequisitionData.PracticeAreaId.Value : defaultValue,
                    ProjectId = talentRequisitionData.ProjectId.HasValue ? talentRequisitionData.ProjectId.Value : defaultValue,
                    TRCode = talentRequisitionData.TRCode,
                    StatusId = Convert.ToInt32(Enumeration.TalentRequisitionStatusCodes.Draft),
                    IsActive = true,
                    SystemInfo = talentRequisitionData.SystemInfo,
                    RaisedBy = talentRequisitionData.RaisedBy,
                    DraftedBy = talentRequisitionData.EmployeeId,
                    RequisitionType = talentRequisitionData.RequisitionType,
                    ClientID = talentRequisitionData.ClientId,
                    ProjectDuration = talentRequisitionData.ProjectDuration,
                    CreatedDate=talentRequisitionData.CreatedDate
                };

                string[] dateFormate = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy" };
                if (!string.IsNullOrWhiteSpace(talentRequisitionData.RequestedDate))
                {
                    if (talentRequisitionData.RequestedDate.Contains("T"))
                        talentRequisitionData.RequestedDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequisitionData.RequestedDate));
                    talentRequisition.RequestedDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequisitionData.RequestedDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None));
                }

                if (!string.IsNullOrWhiteSpace(talentRequisitionData.TargetFulfillmentDate))
                {
                    if (talentRequisitionData.TargetFulfillmentDate.Contains("T"))
                        talentRequisitionData.TargetFulfillmentDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequisitionData.TargetFulfillmentDate));
                    talentRequisition.TargetFulfillmentDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequisitionData.TargetFulfillmentDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None));
                }

                return talentRequisition;
            }
        }
        #endregion

        #region GetReportingManager
        /// <summary>
        /// 
        /// </summary>
        /// <param name="talentRequisitionData"></param>
        /// <returns></returns>
        private int GetReportingManager(TalentRequistionData talentRequisitionData)
        {
            //int reportingManagerId = 0;
            //using (APEntities hrmsEntities = new APEntities())
            //{
            //    IQueryable<ProjectData> query = (from project in hrmsEntities.Projects.AsNoTracking()
            //                                     join projectmgrs in hrmsEntities.ProjectManagers.AsNoTracking()
            //                                     on project.ProjectId equals projectmgrs.ProjectID
            //                                     join dept in hrmsEntities.Departments.AsNoTracking()
            //                                     on project.DepartmentId equals dept.DepartmentId into departments
            //                                     from dep in departments.DefaultIfEmpty()
            //                                     where project.ProjectId == talentRequisitionData.ProjectId && project.IsActive == true & dep.IsActive == true
            //                                     select new ProjectData
            //                                     {
            //                                         ProjectId = project.ProjectId,
            //                                         ProjectName = project.ProjectName,
            //                                         LeadId = projectmgrs.LeadID,
            //                                         ManagerId = (int)projectmgrs.ReportingManagerID,
            //                                         DepartmentHeadId = dep.DepartmentHead,
            //                                     });
            //    ProjectData projectData = query.FirstOrDefault();

            //    lkValue lkValue = hrmsEntities.lkValues.Where(l => l.ValueKey == talentRequisitionData.RequisitionType).FirstOrDefault();

            //    if (talentRequisitionData.EmployeeId == projectData.LeadId)
            //        reportingManagerId = projectData.ManagerId;
            //    else if (talentRequisitionData.EmployeeId == projectData.ManagerId)
            //        reportingManagerId = (int)projectData.DepartmentHeadId;
            //    else if (talentRequisitionData.EmployeeId == projectData.DepartmentHeadId && lkValue.ValueID == RequisitionType.New.ToString())
            //        reportingManagerId = (int)hrmsEntities.Departments.Where(d => d.DepartmentCode == DepartmentCodes.FD.ToString()).FirstOrDefault().DepartmentHead;
            //    else if (talentRequisitionData.EmployeeId == projectData.DepartmentHeadId && lkValue.ValueID == RequisitionType.Replacement.ToString())
            //        reportingManagerId = (int)projectData.DepartmentHeadId;
            //    return reportingManagerId;
            //}
            return 0;
        }
        #endregion

        #region GetUserProjectTypes
        /// <summary>
        /// GetUserProjectTypes
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public List<ProjectTypeDetails> GetUserProjectTypes(int employeeId)
        {
            //using (APEntities hrmsEntities = new APEntities())
            //{
            //    List<ProjectTypeDetails> projectTypes = (from p in hrmsEntities.Projects.AsNoTracking()
            //                                             join projectmgrs in hrmsEntities.ProjectManagers.AsNoTracking()
            //                                             on p.ProjectId equals projectmgrs.ProjectID
            //                                             join pt in hrmsEntities.ProjectTypes.AsNoTracking()
            //                                             on p.ProjectTypeId equals pt.ProjectTypeId
            //                                             where (projectmgrs.LeadID == employeeId || projectmgrs.ReportingManagerID == employeeId) && p.IsActive == true && pt.IsActive == true
            //                                             select new ProjectTypeDetails
            //                                             {
            //                                                 ProjectTypeId = pt.ProjectTypeId,
            //                                                 ProjectTypeCode = pt.ProjectTypeCode
            //                                             }).Distinct().ToList<ProjectTypeDetails>();

            //    if (projectTypes.Count < 0)
            //        projectTypes = (from project in hrmsEntities.Projects.AsNoTracking()
            //                        join ptype in hrmsEntities.ProjectTypes.AsNoTracking()
            //                        on project.ProjectTypeId equals ptype.ProjectTypeId
            //                        join dep in hrmsEntities.Departments.AsNoTracking()
            //                        on project.DepartmentId equals dep.DepartmentId into departments
            //                        from dept in departments.DefaultIfEmpty()
            //                        where dept.DepartmentHead == employeeId && project.IsActive == true && ptype.IsActive == true
            //                        select new ProjectTypeDetails
            //                        {
            //                            ProjectTypeId = ptype.ProjectTypeId,
            //                            ProjectTypeCode = ptype.ProjectTypeCode
            //                        }).Distinct().ToList<ProjectTypeDetails>();

            //    return projectTypes;
            //}
            return null;
        }
        #endregion

        #region GetUserProjects
        /// <summary>
        /// GetUserProjects
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public List<ProjectData> GetUserProjects(int employeeId, int departmentId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                List<ProjectData> projects = (from p in hrmsEntities.Projects.AsNoTracking()
                                              join projectmgrs in hrmsEntities.ProjectManagers.AsNoTracking()
                                              on p.ProjectId equals projectmgrs.ProjectID
                                              join pt in hrmsEntities.PracticeAreas.AsNoTracking()
                                              on p.PracticeAreaId equals pt.PracticeAreaId
                                              join dep in hrmsEntities.Departments.AsNoTracking()
                                              on p.DepartmentId equals dep.DepartmentId into departments
                                              from depts in departments.DefaultIfEmpty()
                                              where ((projectmgrs.ReportingManagerID == employeeId || projectmgrs.ProgramManagerID == employeeId) ||
                                              (depts.IsActive == true))
                                              && p.IsActive == true && depts.DepartmentId == departmentId
                                              select new ProjectData
                                              {
                                                  ProjectId = p.ProjectId,
                                                  ProjectName = p.ProjectName,
                                                  ProjectCode = p.ProjectCode,
                                                  PracticeAreaId = (int)p.PracticeAreaId,
                                                  PracticeAreaCode = pt.PracticeAreaCode,
                                                  ClientName = p.Client.ClientName,
                                                  CustomerId = p.ClientId
                                              }).Distinct().OrderBy(p => p.ProjectName).ToList();
                return projects;
            }
        }
        #endregion

        #region GetRequistion
        /// <summary>
        ///GetRequistion 
        /// </summary>
        /// <param name="requisitionId"></param>
        /// <returns></returns>
        public TalentRequistionData GetRequisition(int requisitionId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                TalentRequistionData talentRequistionData = (from tr in hrmsEntities.TalentRequisitions.AsNoTracking().AsEnumerable()
                                                             join department in hrmsEntities.Departments on tr.DepartmentId equals department.DepartmentId
                                                             where tr.TRId == requisitionId && tr.IsActive == true
                                                             select new TalentRequistionData
                                                             {
                                                                 TalentRequisitionId = tr.TRId,
                                                                 TRCode = tr.TRCode,
                                                                 DepartmentId = tr.DepartmentId,
                                                                 PracticeAreaId = tr.PracticeAreaId,
                                                                 RequestedDate = string.Format("{0:yyyy/MM/dd}", tr.RequestedDate),
                                                                 TargetFulfillmentDate = string.Format("{0:yyyy/MM/dd}", tr.TargetFulfillmentDate),
                                                                 RequisitionType = tr.RequisitionType,
                                                                 ProjectId = tr.ProjectId,
                                                                 ProjectDuration = tr.ProjectDuration,
                                                                 CreatedDate=tr.CreatedDate,
                                                                 DepartmentTypeId = department.DepartmentTypeId
                                                             }).FirstOrDefault();

                return talentRequistionData;
            }
        }
        #endregion

        #region DeleteRequistion
        /// <summary>
        /// DeleteRequistion
        /// </summary>
        /// <param name="requisitionId"></param>
        /// <returns></returns>
        public bool DeleteRequistion(int requisitionId)
        {
            bool deleted = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                TalentRequisition talentRequisiton = hrmsEntities.TalentRequisitions.Where(tr => tr.TRId == requisitionId && tr.IsActive == true).FirstOrDefault();
                talentRequisiton.IsActive = false;
                talentRequisiton.ModifiedUser = HttpContext.Current.User.Identity.Name;
                talentRequisiton.ModifiedDate = DateTime.Now;
                talentRequisiton.SystemInfo = Commons.GetClientIPAddress();
                hrmsEntities.Entry(talentRequisiton).State = EntityState.Modified;
                deleted = hrmsEntities.SaveChanges() > 0 ? true : false;

                return deleted;
            }
        }
        #endregion

        #region GetRequisitionsByEmpId
        /// <summary>
        /// This method is used to get existing all requisitions of an associate
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TalentRequisitionHistoryData> GetRequisitionsByEmpId(int employeeId)
        {
            try
            {

                using (APEntities hrmEntities = new APEntities())
                {

                    var categoryies = hrmEntities.CategoryMasters.Where(cat => cat.CategoryName == "TalentRequisition").FirstOrDefault();

                    List<TalentRequisitionHistoryData> requisitions = (from tr in hrmEntities.TalentRequisitions
                                                                       join s in hrmEntities.Status on tr.StatusId equals s.StatusId
                                                                       join trs in hrmEntities.TalentRequisitionWorkFlows on tr.StatusId equals trs.StatusID into trStatus
                                                                       from trss in trStatus.Where(f => f.TalentRequisitionID == tr.TRId).DefaultIfEmpty()
                                                                       join lkValue in hrmEntities.lkValues on tr.RequisitionType equals lkValue.ValueKey
                                                                       //join e in hrmEntities.Employees on trss.ToEmployeeID equals e.EmployeeId into ee
                                                                       // from emp in ee.DefaultIfEmpty()
                                                                       join e1 in hrmEntities.Employees on trss.FromEmployeeID equals e1.EmployeeId into ee1
                                                                       from emp1 in ee1.DefaultIfEmpty()
                                                                       join draft in hrmEntities.Employees.AsNoTracking() on tr.DraftedBy equals draft.EmployeeId into drafts
                                                                       from dr in drafts.DefaultIfEmpty()
                                                                       join raised in hrmEntities.Employees.AsNoTracking() on tr.RaisedBy equals raised.EmployeeId into raise
                                                                       from raisedby in raise.DefaultIfEmpty()
                                                                       join approved in hrmEntities.Employees.AsNoTracking() on tr.ApprovedBy equals approved.EmployeeId into appr
                                                                       from apr in appr.DefaultIfEmpty()
                                                                       where tr.IsActive == true && s.CategoryID == categoryies.CategoryID && (tr.DraftedBy == employeeId || tr.RaisedBy == employeeId || tr.ApprovedBy == employeeId)
                                                                       // || (trss.ToEmployeeID == employeeId)
                                                                       select new TalentRequisitionHistoryData
                                                                       {
                                                                           //TRId = tr.TRId,
                                                                           TalentRequisitionId = tr.TRId,
                                                                           TRCode = tr.TRCode,
                                                                           DepartmentName = tr.Department.Description,
                                                                           DepartmentId=tr.Department.DepartmentId,
                                                                           //ProjectType = tr.ProjectType.Description,
                                                                           ProjectName = tr.Project.ProjectName,
                                                                           Status = s.StatusCode,
                                                                           StatusId = tr.StatusId,
                                                                           TRSelect = false,
                                                                           Remarks = tr.Remarks,
                                                                           TRStatusUpdatedBy = emp1.FirstName + " " + emp1.LastName,
                                                                           RaisedUser = raisedby.FirstName + " " + raisedby.LastName,
                                                                           DraftedUser = dr.FirstName + " " + dr.LastName,
                                                                           ApprovedUser = apr.FirstName + " " + apr.LastName,
                                                                           RequisitionName = lkValue.ValueName,
                                                                           NoOfPositions = tr.RequisitionRoleDetails.Sum(t => t.NoOfBillablePositions!= null?t.NoOfBillablePositions:0) + tr.RequisitionRoleDetails.Sum(t => t.NoOfNonBillablePositions != null ? t.NoOfNonBillablePositions : 0),
                                                                       }).Distinct().OrderByDescending(t => t.TalentRequisitionId).ToList();
                    return requisitions;
                }
                //return null;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region BehaviourAreaMethods

        #region AddBehaviorArea
        /// <summary>
        /// AddBehaviorArea
        /// </summary>
        /// <param name="behaviorData"></param>
        /// <returns>bool</returns>
        public bool AddBehaviorArea(BehaviorAreas behaviorData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                BehaviorArea behaviorArea = new BehaviorArea()
                {
                    BehaviorAreaDescription = behaviorData.BehaviorArea,
                    IsActive = behaviorData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name
                };
                hrmsEntities.BehaviorAreas.Add(behaviorArea);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region UpdateBehaviorArea
        /// <summary>
        /// UpdateBehaviorArea
        /// </summary>
        /// <param name="behaviorData"></param>
        /// <returns>bool</returns>
        public bool UpdateBehaviorArea(BehaviorAreas behaviorData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                BehaviorArea behaviorArea = hrmsEntities.BehaviorAreas.Where(behavior => behavior.BehaviorAreaId == behaviorData.BehaviorAreaId).FirstOrDefault();
                if (behaviorArea != null)
                {
                    behaviorArea.BehaviorAreaId = behaviorData.BehaviorAreaId;
                    behaviorArea.BehaviorAreaDescription = behaviorData.BehaviorArea;
                    behaviorArea.IsActive = behaviorData.IsActive;
                    behaviorArea.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    behaviorArea.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(behaviorArea).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region DeleteBehaviorArea
        /// <summary>
        /// DeleteBehaviorArea
        /// </summary>
        /// <param name="behaviorAreaId"></param>
        /// <returns>bool</returns>
        public bool DeleteBehaviorArea(int behaviorAreaId)
        {
            bool deleted = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                BehaviorArea behaviorArea = hrmsEntities.BehaviorAreas.Where(behavior => behavior.BehaviorAreaId == behaviorAreaId && behavior.IsActive == true).FirstOrDefault();
                if (behaviorArea != null)
                {
                    behaviorArea.IsActive = false;
                    behaviorArea.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    behaviorArea.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(behaviorArea).State = EntityState.Modified;
                    deleted = hrmsEntities.SaveChanges() > 0 ? true : false;
                }

                return deleted;
            }
        }
        #endregion

        #region GetBehaviorArea
        /// <summary>
        /// GetBehaviorArea
        /// </summary>
        /// <returns>List</returns>
        public List<BehaviorAreas> GetBehaviorArea()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<BehaviorAreas> behaviorAreaList = hrmsEntities.BehaviorAreas.Where(behavior => behavior.IsActive == true).AsQueryable()
                                                                             .Select(behaviorAreaData => new BehaviorAreas
                                                                             {
                                                                                 BehaviorAreaId = behaviorAreaData.BehaviorAreaId,
                                                                                 BehaviorArea = behaviorAreaData.BehaviorAreaDescription,
                                                                                 IsActive = behaviorAreaData.IsActive
                                                                             }).OrderBy(area => area.BehaviorArea);
                return behaviorAreaList.ToList<BehaviorAreas>();
            }
        }
        #endregion

        #endregion

        #region BehaviourRatingMethods

        #region AddBehaviorRating
        /// <summary>
        /// AddBehaviorRating
        /// </summary>
        /// <param name="behaviorRatingData"></param>
        /// <returns>bool</returns>
        public bool AddBehaviorRating(BehaviorRatings behaviorRatingData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                BehaviorRating behaviorRating = new BehaviorRating()
                {
                    BehaviorRatingDescription = behaviorRatingData.Rating,
                    IsActive = behaviorRatingData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name
                };
                hrmsEntities.BehaviorRatings.Add(behaviorRating);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region UpdateBehaviorRating
        /// <summary>
        /// UpdateBehaviorRating
        /// </summary>
        /// <param name="behaviorRatingData"></param>
        /// <returns>bool</returns>
        public bool UpdateBehaviorRating(BehaviorRatings behaviorRatingData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                BehaviorRating behaviorRating = hrmsEntities.BehaviorRatings.Where(behavior => behavior.RatingId == behaviorRatingData.BehaviorRatingId).FirstOrDefault();
                if (behaviorRating != null)
                {
                    behaviorRating.BehaviorRatingDescription = behaviorRatingData.Rating;
                    behaviorRating.IsActive = behaviorRatingData.IsActive;
                    behaviorRating.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    behaviorRating.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(behaviorRating).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region DeleteBehaviorRating
        /// <summary>
        /// DeleteBehaviorRating
        /// </summary>
        /// <param name="ratingId"></param>
        /// <returns>bool</returns>
        public bool DeleteBehaviorRating(int ratingId)
        {
            bool deleted = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                BehaviorRating behaviorRating = hrmsEntities.BehaviorRatings.Where(rating => rating.RatingId == ratingId && rating.IsActive == true).FirstOrDefault();
                if (behaviorRating != null)
                {
                    behaviorRating.IsActive = false;
                    behaviorRating.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    behaviorRating.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(behaviorRating).State = EntityState.Modified;
                    deleted = hrmsEntities.SaveChanges() > 0 ? true : false;
                }

                return deleted;
            }
        }
        #endregion

        #region GetBehaviorRating
        /// <summary>
        /// GetBehaviorRating
        /// </summary>
        /// <returns>List</returns>
        public List<BehaviorRatings> GetBehaviorRating()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<BehaviorRatings> ratingList = hrmsEntities.BehaviorRatings.Where(rating => rating.IsActive == true).AsQueryable()
                                                                             .Select(ratingData => new BehaviorRatings
                                                                             {
                                                                                 BehaviorRatingId = ratingData.RatingId,
                                                                                 Rating = ratingData.BehaviorRatingDescription,
                                                                                 IsActive = ratingData.IsActive
                                                                             }).OrderBy(rating => rating.Rating);
                return ratingList.ToList<BehaviorRatings>();
            }
        }
        #endregion

        #endregion

        #region LearningAptitudeMethods

        #region AddLearningAptitude
        /// <summary>
        /// AddLearningAptitude
        /// </summary>
        /// <param name="learningAptitudeData"></param>
        /// <returns>bool</returns>
        public bool AddLearningAptitude(LearningAptitudeData learningAptitudeData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                LearningAptitude learningAptitude = new LearningAptitude()
                {
                    LearningAptitudeDescription = learningAptitudeData.LearningAptitude,
                    IsActive = learningAptitudeData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name
                };
                hrmsEntities.LearningAptitudes.Add(learningAptitude);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region UpdateLearningAptitude
        /// <summary>
        /// UpdateLearningAptitude
        /// </summary>
        /// <param name="learningAptitudeData"></param>
        /// <returns>bool</returns>
        public bool UpdateLearningAptitude(LearningAptitudeData learningAptitudeData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                LearningAptitude learningAptitude = hrmsEntities.LearningAptitudes.Where(learning => learning.LearningAptitudeId == learningAptitudeData.LearningAptitudeId).FirstOrDefault();
                if (learningAptitude != null)
                {
                    learningAptitude.LearningAptitudeDescription = learningAptitudeData.LearningAptitude;
                    learningAptitude.IsActive = learningAptitudeData.IsActive;
                    learningAptitude.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    learningAptitude.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(learningAptitude).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region DeleteLearningAptitude
        /// <summary>
        /// DeleteLearningAptitude
        /// </summary>
        /// <param name="learningAptitudeId"></param>
        /// <returns>bool</returns>
        public bool DeleteLearningAptitude(int learningAptitudeId)
        {
            bool deleted = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                LearningAptitude learningAptitude = hrmsEntities.LearningAptitudes.Where(learning => learning.LearningAptitudeId == learningAptitudeId && learning.IsActive == true).FirstOrDefault();
                if (learningAptitude != null)
                {
                    learningAptitude.IsActive = false;
                    learningAptitude.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    learningAptitude.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(learningAptitude).State = EntityState.Modified;
                    deleted = hrmsEntities.SaveChanges() > 0 ? true : false;
                }

                return deleted;
            }
        }
        #endregion

        #region GetLearningAptitude
        /// <summary>
        /// GetLearningAptitude
        /// </summary>
        /// <returns>List</returns>
        public List<LearningAptitudeData> GetLearningAptitude()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<LearningAptitudeData> learningAptitudeList = hrmsEntities.LearningAptitudes.Where(learning => learning.IsActive == true).AsQueryable()
                                                                             .Select(aptitudeData => new LearningAptitudeData
                                                                             {
                                                                                 LearningAptitudeId = aptitudeData.LearningAptitudeId,
                                                                                 LearningAptitude = aptitudeData.LearningAptitudeDescription,
                                                                                 IsActive = aptitudeData.IsActive
                                                                             }).OrderBy(aptitude => aptitude.LearningAptitude);
                return learningAptitudeList.ToList<LearningAptitudeData>();
            }
        }
        #endregion

        #region GetLearningAptitudeByTrId
        /// <summary>
        /// GetLearningAptitudeByTrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        public RequisitionLearningAptitudeData GetLearningAptitudeByTrId(int trId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<RequisitionLearningAptitudeData> reqLearningAptitudeList = (from rla in hrmsEntities.RequisitionLearningAptitudes
                                                                                       join tr in hrmsEntities.TalentRequisitions on rla.TRId equals tr.TRId
                                                                                       where tr.IsActive == true && rla.TRId == trId
                                                                                       select new RequisitionLearningAptitudeData
                                                                                       {
                                                                                           RequisitionLearningAptitudeId = rla.RequisitionLearningAptitudeId,
                                                                                           TalentRequisitionId = rla.TRId,
                                                                                           LearningAptitudeId = rla.LearningAptitudeId
                                                                                       });
                return reqLearningAptitudeList.FirstOrDefault<RequisitionLearningAptitudeData>();
            }
        }
        #endregion



        #endregion

        #region ExpertiseAreaMethods

        #region AddExpertiseArea
        /// <summary>
        /// AddExpertiseArea
        /// </summary>
        /// <param name="expertiseServiceData"></param>
        /// <returns>bool</returns>
        public bool AddExpertiseArea(ExpertiseAreaData expertiseAreaData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                ExpertiseArea expertiseArea = new ExpertiseArea()
                {
                    ExpertiseAreaDescription = expertiseAreaData.ExpertiseAreaDescription,
                    IsActive = expertiseAreaData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name
                };
                hrmsEntities.ExpertiseAreas.Add(expertiseArea);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region UpdateExpertiseArea
        /// <summary>
        /// UpdateExpertiseArea
        /// </summary>
        /// <param name="expertiseAreaData"></param>
        /// <returns>bool</returns>
        public bool UpdateExpertiseArea(ExpertiseAreaData expertiseAreaData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                ExpertiseArea expertiseArea = hrmsEntities.ExpertiseAreas.Where(expertise => expertise.ExpertiseId == expertiseAreaData.ExpertiseId).FirstOrDefault();
                if (expertiseArea != null)
                {
                    expertiseArea.ExpertiseAreaDescription = expertiseAreaData.ExpertiseAreaDescription;
                    expertiseArea.IsActive = expertiseAreaData.IsActive;
                    expertiseArea.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    expertiseArea.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(expertiseArea).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region DeleteExpertiseArea
        /// <summary>
        /// DeleteExpertiseArea
        /// </summary>
        /// <param name="expertiseId"></param>
        /// <returns>bool</returns>
        public bool DeleteExpertiseArea(int expertiseId)
        {
            bool deleted = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                ExpertiseArea expertiseArea = hrmsEntities.ExpertiseAreas.Where(expertise => expertise.ExpertiseId == expertiseId && expertise.IsActive == true).FirstOrDefault();
                if (expertiseArea != null)
                {
                    expertiseArea.IsActive = false;
                    expertiseArea.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    expertiseArea.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(expertiseArea).State = EntityState.Modified;
                    deleted = hrmsEntities.SaveChanges() > 0 ? true : false;
                }

                return deleted;
            }
        }
        #endregion

        #region GetExpertiseArea
        /// <summary>
        /// GetExpertiseArea
        /// </summary>
        /// <returns>List</returns>
        public List<ExpertiseAreaData> GetExpertiseArea()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<ExpertiseAreaData> expertiseList = hrmsEntities.ExpertiseAreas.Where(expertise => expertise.IsActive == true).AsQueryable()
                                                                             .Select(expertiseData => new ExpertiseAreaData
                                                                             {
                                                                                 ExpertiseId = expertiseData.ExpertiseId,
                                                                                 ExpertiseAreaDescription = expertiseData.ExpertiseAreaDescription,
                                                                                 IsActive = expertiseData.IsActive
                                                                             }).OrderBy(expertise => expertise.ExpertiseAreaDescription);
                return expertiseList.ToList<ExpertiseAreaData>();
            }
        }
        #endregion

        #endregion

        #region RequisitionRolesandSkillsMethods

        #region SaveRequisitionRoleDetails
        /// <summary>
        /// SaveRequisitionRoleDetails
        /// </summary>
        /// <param name="requisitionRoleDetails"></param>
        /// <returns></returns>
        public bool SaveRequisitionRoleDetails(RequisitionRoleDetails requisitionRoleDetails)
        {
            bool returnValue = false;
            RequisitionRoleDetail requisitionRole = null;
            using (APEntities hrmsEntities = new APEntities())
            {
                if (requisitionRoleDetails.RequisitionRoleDetailID <= 0)
                {
                    requisitionRole = AssignRequisitionRoleData(requisitionRoleDetails);
                    requisitionRole.CreatedUser = requisitionRoleDetails.CurrentUser;
                    requisitionRole.CreatedDate = DateTime.Now;
                    hrmsEntities.RequisitionRoleDetails.Add(requisitionRole);
                }
                else
                {
                    requisitionRole = AssignRequisitionRoleData(requisitionRoleDetails);
                    requisitionRole.RequisitionRoleDetailID = requisitionRoleDetails.RequisitionRoleDetailID;
                    requisitionRole.ModifiedUser = requisitionRoleDetails.CurrentUser;
                    requisitionRole.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(requisitionRole).State = EntityState.Modified;
                }
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return returnValue;
        }
        #endregion

        #region AssignRequisitionRoleDetail
        /// <summary>
        /// AssignRequisitionRoleDetail
        /// </summary>
        /// <returns></returns>
        private RequisitionRoleDetail AssignRequisitionRoleData(RequisitionRoleDetails requisitionRoleDetails)
        {
            RequisitionRoleDetail requisitionRole = new RequisitionRoleDetail
            {
                TRId = requisitionRoleDetails.TalentRequisitionId,
                RoleMasterId = requisitionRoleDetails.RoleMasterId,
                NoOfBillablePositions = requisitionRoleDetails.NoOfBillablePositions,
                NoOfNonBillablePositions = requisitionRoleDetails.NoOfNonBillablePositions,
                MinimumExperience = requisitionRoleDetails.MinimumExperience,
                MaximumExperience = requisitionRoleDetails.MaximumExperience,
                RoleDescription = requisitionRoleDetails.RoleDescription,
                KeyResponsibilities = requisitionRoleDetails.KeyResponsibilities,
                EssentialEducationQualification = requisitionRoleDetails.EssentialEducationQualification,
                DesiredEducationQualification = requisitionRoleDetails.DesiredEducationQualification,
                ProjectSpecificResponsibilities = requisitionRoleDetails.ProjectSpecificResponsibilities,
                Expertise = requisitionRoleDetails.Expertise,
                SystemInfo = requisitionRoleDetails.SystemInfo
            };
            return requisitionRole;
        }
        #endregion

        #region GetRequisitionRoleDetailsByTrId
        /// <summary>
        /// GetRequisitionRoleDetailsByTrId
        /// </summary>
        /// <param name="TrId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetRequisitionRoleDetailsByTrId(int trId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                var requisitionRoleDetails = (from rrd in hrmsEntities.RequisitionRoleDetails.AsNoTracking()
                                              join role in hrmsEntities.RoleMasters.AsNoTracking() on rrd.RoleMasterId equals role.RoleMasterID
                                              join suffix in hrmsEntities.SGRoleSuffixes on role.SuffixID equals suffix.SuffixID into suffixtemp
                                              from sufflist in suffixtemp.DefaultIfEmpty()
                                              join prefix in hrmsEntities.SGRolePrefixes on role.PrefixID equals prefix.PrefixID into prefixtemp
                                              from preflist in prefixtemp.DefaultIfEmpty()
                                              join sgrole in hrmsEntities.SGRoles on role.SGRoleID equals sgrole.SGRoleID
                                              where rrd.TRId == trId
                                              select new
                                              {
                                                  RequisitionRoleDetailID = rrd.RequisitionRoleDetailID,
                                                  TRId = rrd.TRId,
                                                  RoleMasterId = rrd.RoleMasterId,
                                                  RoleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName,
                                                  NoOfBillablePositions = rrd.NoOfBillablePositions,
                                                  NoOfNonBillablePositions = rrd.NoOfNonBillablePositions,
                                                  TotalPositions = (rrd.NoOfBillablePositions != null ? rrd.NoOfBillablePositions : 0) + (rrd.NoOfNonBillablePositions != null ? rrd.NoOfNonBillablePositions : 0),
                                                  MinimumExperience = rrd.MinimumExperience,
                                                  MaximumExperience = rrd.MaximumExperience,
                                                  TAPositions = rrd.TAPositions
                                              }).ToList();
                return requisitionRoleDetails;
            }
        }
        #endregion

        #region GetRequisitionRoleDataById
        /// <summary>
        /// GetRequisitionRoleDataById
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <returns></returns>
        public RequisitionRoleDetails GetRequisitionRoleDataById(int requisitionRoleDetailId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                RequisitionRoleDetails requisitionRoleDetails = (from rrd in hrmsEntities.RequisitionRoleDetails
                                                                 where rrd.RequisitionRoleDetailID == requisitionRoleDetailId
                                                                 select new RequisitionRoleDetails
                                                                 {
                                                                     RequisitionRoleDetailID = rrd.RequisitionRoleDetailID,
                                                                     TalentRequisitionId = rrd.TRId,
                                                                     RoleMasterId = rrd.RoleMasterId,
                                                                     NoOfBillablePositions = rrd.NoOfBillablePositions,
                                                                     NoOfNonBillablePositions = rrd.NoOfNonBillablePositions,
                                                                     MinimumExperience = rrd.MinimumExperience,
                                                                     MaximumExperience = rrd.MaximumExperience,
                                                                     RoleDescription = rrd.RoleDescription,
                                                                     Expertise = rrd.Expertise,
                                                                     KeyResponsibilities = rrd.KeyResponsibilities,
                                                                     ProjectSpecificResponsibilities = rrd.ProjectSpecificResponsibilities,
                                                                     EssentialEducationQualification = rrd.EssentialEducationQualification,
                                                                     DesiredEducationQualification = rrd.DesiredEducationQualification,
                                                                     TAPositions = rrd.TAPositions
                                                                 }).FirstOrDefault();
                return requisitionRoleDetails;
            }
        }
        #endregion

        #region SaveRequisitionRoleSkills
        /// <summary>
        /// SaveRequisitionRoleSkills
        /// </summary>
        /// <param name="requisitionRoleSkills"></param>
        /// <returns></returns>
        public bool SaveRequisitionRoleSkill(List<RequisitionRoleSkills> requisitionRoleSkills)
        {
            bool returnValue = false;
            //string currentUser = HttpContext.Current.User.Identity.Name;
            //string systemInfo = Commons.GetClientIPAddress();
            //using (APEntities hrmsEntities = new APEntities())
            //{
            //    using (var dbTransaction = hrmsEntities.Database.BeginTransaction())
            //    {
            //        try
            //        {
            //            List<RequisitionRoleSkill> requisitionRoleSkillList = new List<RequisitionRoleSkill>();
            //            foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills)
            //            {
            //                RequisitionRoleSkill requisitionRoleSkill = new RequisitionRoleSkill()
            //                {
            //                    TRId = roleSkill.TRId,
            //                    RequistiionRoleDetailID = roleSkill.RequisitionRoleDetailID,
            //                    CompetencyAreaId = roleSkill.CompetencyAreaId,
            //                    SkillId = roleSkill.SkillId,
            //                    IsPrimary = roleSkill.IsPrimary,
            //                    ProficiencyLevelId = roleSkill.ProficiencyLevelId,
            //                    SystemInfo = systemInfo
            //                };

            //                if (roleSkill.RRSkillId <= 0)
            //                {
            //                    requisitionRoleSkill.CreatedUser = currentUser;
            //                    requisitionRoleSkill.CreatedDate = DateTime.Now;
            //                    requisitionRoleSkillList.Add(requisitionRoleSkill);
            //                }
            //                else
            //                {
            //                    requisitionRoleSkill.RRSkillId = roleSkill.RRSkillId;
            //                    requisitionRoleSkill.ModifiedUser = currentUser;
            //                    requisitionRoleSkill.ModifiedDate = DateTime.Now;
            //                    hrmsEntities.Entry(requisitionRoleSkill).State = EntityState.Modified;
            //                }
            //            }
            //            if (requisitionRoleSkillList.Count > 0)
            //                hrmsEntities.RequisitionRoleSkills.AddRange(requisitionRoleSkillList);

            //            int reqRoleDetailId = requisitionRoleSkills.Select(i => i.RequisitionRoleDetailID).FirstOrDefault();
            //            List<RequisitionRoleSkill> reqRoleSkills = hrmsEntities.RequisitionRoleSkills.AsEnumerable().
            //                Where(rs => (!requisitionRoleSkills.Any(b => b.SkillId == rs.SkillId) && rs.RequistiionRoleDetailID == reqRoleDetailId)).ToList();
            //            if (reqRoleSkills.Count > 0)
            //                hrmsEntities.RequisitionRoleSkills.RemoveRange(reqRoleSkills);

            //            returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            //            dbTransaction.Commit();
            //        }
            //        catch
            //        {
            //            dbTransaction.Rollback();
            //            throw;
            //        }
            //    }
            //}
            return returnValue;
        }
        #endregion

        #region SaveRequisitionRoleSkills
        /// <summary>
        /// SaveRequisitionRoleSkills
        /// </summary>
        /// <param name="requisitionRoleSkills"></param>
        /// <returns></returns>
        //public bool SaveRequisitionRoleSkills(RequisitionRoleSkillList requisitionRoleSkills)
        //{
        //    bool returnValue = false;
        //string currentUser = HttpContext.Current.User.Identity.Name;
        //string systemInfo = Commons.GetClientIPAddress();
        //using (APEntities hrmsEntities = new APEntities())
        //{
        //    using (var dbTransaction = hrmsEntities.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            List<ProficiencyLevel> proficiencyLevels = hrmsEntities.ProficiencyLevels.AsNoTracking().Where(i => i.IsActive == true).ToList();
        //            List<RequisitionRoleSkill> requisitionSkillListAdd = new List<RequisitionRoleSkill>();
        //            List<RequisitionRoleSkills> existingReqRoleSkills = new List<RequisitionRoleSkills>();

        //            if (requisitionRoleSkills.BasicLevel != null && requisitionRoleSkills.BasicLevel.Count > 0)
        //            {
        //                int basicLevelId = proficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Basic.ToString().ToLower()))
        //                                                   .Select(id => id.ProficiencyLevelId).FirstOrDefault();

        //                foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills.BasicLevel)
        //                {
        //                    roleSkill.ProficiencyLevelId = basicLevelId;
        //                    existingReqRoleSkills.Add(roleSkill);
        //                }
        //            }

        //            if (requisitionRoleSkills.BeginnerLevel != null && requisitionRoleSkills.BeginnerLevel.Count > 0)
        //            {
        //                int beginnerLevelId = proficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Beginner.ToString().ToLower()))
        //                       .Select(id => id.ProficiencyLevelId).FirstOrDefault();

        //                foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills.BeginnerLevel)
        //                {
        //                    roleSkill.ProficiencyLevelId = beginnerLevelId;
        //                    existingReqRoleSkills.Add(roleSkill);
        //                }
        //            }

        //            if (requisitionRoleSkills.IntermediateLevel != null && requisitionRoleSkills.IntermediateLevel.Count > 0)
        //            {
        //                int intermediateLevelId = proficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Intermediate.ToString().ToLower()))
        //                       .Select(id => id.ProficiencyLevelId).FirstOrDefault();

        //                foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills.IntermediateLevel)
        //                {
        //                    roleSkill.ProficiencyLevelId = intermediateLevelId;
        //                    existingReqRoleSkills.Add(roleSkill);
        //                }
        //            }

        //            if (requisitionRoleSkills.AdvanceLevel != null && requisitionRoleSkills.AdvanceLevel.Count > 0)
        //            {
        //                int advanceLevelId = proficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Advanced.ToString().ToLower()))
        //                       .Select(id => id.ProficiencyLevelId).FirstOrDefault();

        //                foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills.AdvanceLevel)
        //                {
        //                    roleSkill.ProficiencyLevelId = advanceLevelId;
        //                    existingReqRoleSkills.Add(roleSkill);
        //                }

        //            }
        //            if (requisitionRoleSkills.ExpertLevel != null && requisitionRoleSkills.ExpertLevel.Count > 0)
        //            {
        //                int expertLevelId = proficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Expert.ToString().ToLower()))
        //                       .Select(id => id.ProficiencyLevelId).FirstOrDefault();

        //                foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills.ExpertLevel)
        //                {
        //                    roleSkill.ProficiencyLevelId = expertLevelId;
        //                    existingReqRoleSkills.Add(roleSkill);
        //                }
        //            }

        //            foreach (RequisitionRoleSkills roleSkill in existingReqRoleSkills)
        //            {
        //                RequisitionRoleSkill requisitionRoleSkill;

        //                if (roleSkill.RRSkillId <= 0)
        //                {
        //                    requisitionRoleSkill = new RequisitionRoleSkill();
        //                    requisitionRoleSkill.TRId = roleSkill.TRId;
        //                    requisitionRoleSkill.RequistiionRoleDetailID = roleSkill.RequisitionRoleDetailID;
        //                    requisitionRoleSkill.CompetencyAreaId = roleSkill.CompetencyAreaId;
        //                    requisitionRoleSkill.SkillId = roleSkill.SkillId;
        //                    requisitionRoleSkill.IsPrimary = roleSkill.IsPrimary;
        //                    requisitionRoleSkill.ProficiencyLevelId = roleSkill.ProficiencyLevelId;
        //                    requisitionRoleSkill.SystemInfo = systemInfo;
        //                    requisitionRoleSkill.CreatedUser = currentUser;
        //                    requisitionRoleSkill.CreatedDate = DateTime.Now;
        //                    requisitionSkillListAdd.Add(requisitionRoleSkill);
        //                }
        //                else
        //                {
        //                    requisitionRoleSkill = hrmsEntities.RequisitionRoleSkills.Where(rrs => rrs.RRSkillId == roleSkill.RRSkillId).FirstOrDefault();
        //                    requisitionRoleSkill.TRId = roleSkill.TRId;
        //                    requisitionRoleSkill.RequistiionRoleDetailID = roleSkill.RequisitionRoleDetailID;
        //                    requisitionRoleSkill.CompetencyAreaId = roleSkill.CompetencyAreaId;
        //                    requisitionRoleSkill.SkillId = roleSkill.SkillId;
        //                    requisitionRoleSkill.IsPrimary = roleSkill.IsPrimary;
        //                    requisitionRoleSkill.ProficiencyLevelId = roleSkill.ProficiencyLevelId;
        //                    requisitionRoleSkill.SystemInfo = systemInfo;
        //                    requisitionRoleSkill.ModifiedUser = currentUser;
        //                    requisitionRoleSkill.ModifiedDate = DateTime.Now;
        //                    hrmsEntities.Entry(requisitionRoleSkill).State = EntityState.Modified;
        //                }
        //            }


        //            if (requisitionSkillListAdd.Count > 0)
        //                hrmsEntities.RequisitionRoleSkills.AddRange(requisitionSkillListAdd);

        //            //Removing the old skills
        //            int reqRoleDetailId = existingReqRoleSkills.Select(i => i.RequisitionRoleDetailID).FirstOrDefault();
        //            List<RequisitionRoleSkill> reqRoleSkills = hrmsEntities.RequisitionRoleSkills.AsEnumerable().
        //                Where(rs => (!existingReqRoleSkills.Any(b => b.SkillId == rs.SkillId) && rs.RequistiionRoleDetailID == reqRoleDetailId)).ToList();
        //            if (reqRoleSkills.Count > 0)
        //                hrmsEntities.RequisitionRoleSkills.RemoveRange(reqRoleSkills);

        //            returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //            dbTransaction.Commit();
        //        }
        //        catch
        //        {
        //            dbTransaction.Rollback();
        //            throw;
        //        }
        //    }
        //}
        //    return returnValue;
        //}
        #endregion

        #region GetRequisitionRolesByTrId
        /// <summary>
        /// GetRequisitionRolesByTrId
        /// </summary>
        /// <param name="TrId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetRequisitionRolesByTrId(int trId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                var requisitionRoleDetails = (from rrd in hrmsEntities.RequisitionRoleDetails.AsNoTracking()
                                              join role in hrmsEntities.Roles.AsNoTracking()
                                              on rrd.RoleMasterId equals role.RoleId
                                              where rrd.TRId == trId
                                              select new
                                              {
                                                  RequisitionRoleDetailID = rrd.RequisitionRoleDetailID,
                                                  TRId = rrd.TRId,
                                                  RoleId = rrd.RoleMasterId,
                                                  RoleName = role.RoleName,
                                              }).ToList();
                return requisitionRoleDetails;
            }
        }
        #endregion

        #region DeleteRequisitionRoleSkill
        /// <summary>
        /// 
        /// </summary>
        /// <param name="talentRequisitionID"></param>
        /// <param name="requisitionRoleDetaildID"></param>
        /// <param name="skillGroupID"></param>
        /// <returns></returns>
        public bool DeleteRequisitionRoleSkill(int talentRequisitionID, int requisitionRoleDetailID, int skillGroupID)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteRequisitionRoleSkills] @TalentRequisitionID, @RequisitionRoleDetailID, @SkillGroupID",
                                   new object[] {
                                        new SqlParameter ("TalentRequisitionID", talentRequisitionID),
                                        new SqlParameter ("RequisitionRoleDetailID", requisitionRoleDetailID),
                                        new SqlParameter ("SkillGroupID",skillGroupID)
                                   }
                                   ).SingleOrDefault();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting skills.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetReqRoleSkillsByRRId
        /// <summary>
        /// GetReqRoleSkillsByRRId
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <returns></returns>
        public List<RequisitionRoleSkills> GetReqRoleSkillsByRRId(int requisitionRoleDetailId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                //List<RequisitionRoleSkills> requisitionRoleDetails = (from rrs in hrmsEntities.RequisitionRoleSkills
                //                                                      join skill in hrmsEntities.Skills
                //                                                      on rrs.SkillId equals skill.SkillId
                //                                                      join carea in hrmsEntities.CompetencyAreas
                //                                                      on rrs.CompetencyAreaId equals carea.CompetencyAreaId
                //                                                      join plevel in hrmsEntities.ProficiencyLevels
                //                                                      on rrs.ProficiencyLevelId equals plevel.ProficiencyLevelId
                //                                                      where rrs.RequistiionRoleDetailID == requisitionRoleDetailId
                //                                                      select new RequisitionRoleSkills
                //                                                      {
                //                                                          RRSkillId = rrs.RRSkillId,
                //                                                          RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
                //                                                          TRId = rrs.TRId,
                //                                                          SkillId = rrs.SkillId,
                //                                                          OtherSkillCode = skill.SkillCode,
                //                                                          CompetencyAreaId = (int)rrs.CompetencyAreaId,
                //                                                          CompetencyArea = carea.CompetencyAreaCode,
                //                                                          //ProficiencyLevelId=rrs.ProficiencyLevelId,
                //                                                          ProficiencyLevel = plevel.ProficiencyLevelCode,
                //                                                          SkillGroupId = skill.SkillGroupId
                //                                                      }).ToList();

                //return requisitionRoleDetails;
                return null;
            }
        }
        #endregion

        #region GetReqRoleSkills
        /// <summary>
        /// GetReqRoleSkills
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <param name="competencyAreaId"></param>
        /// <returns></returns>
        //public RequisitionRoleSkillList GetReqRoleSkills(int requisitionRoleDetailId, int competencyAreaId)
        //{
        //    using (APEntities hrmsEntities = new APEntities())
        //    {
        //List<RequisitionRoleSkills> reqRoleSkills = (from rrs in hrmsEntities.RequisitionRoleSkills.AsNoTracking()
        //                                             where rrs.RequistiionRoleDetailID == requisitionRoleDetailId && rrs.CompetencyAreaId == competencyAreaId
        //                                             select new RequisitionRoleSkills
        //                                             {
        //                                                 SkillId = (int)rrs.SkillId,
        //                                                 SkillName = rrs.Skill.SkillCode,
        //                                                 RRSkillId = rrs.RRSkillId,
        //                                                 RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                                 TRId = rrs.TRId,
        //                                                 skillID = (int)rrs.SkillId,
        //                                                 skillName = rrs.Skill.SkillCode
        //                                             }).ToList();

        //List<RequisitionRoleSkills> skills = (from s in hrmsEntities.Skills
        //                                      where s.IsActive == true && s.CompetencyAreaId == competencyAreaId
        //                                      select new RequisitionRoleSkills
        //                                      {
        //                                          SkillId = s.SkillId,
        //                                          SkillName = s.SkillCode,
        //                                          skillID = (int)s.SkillId,
        //                                          skillName = s.SkillCode
        //                                      }).ToList();

        //RequisitionRoleSkillList requisitionRoleSkillList = new RequisitionRoleSkillList();
        //requisitionRoleSkillList.AvailableSkills = skills.Where(s => (!reqRoleSkills.Any(rrs => rrs.SkillId == s.SkillId))).ToList();

        //int basic = hrmsEntities.ProficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Basic.ToString().ToLower())).Select(i => i.ProficiencyLevelId).FirstOrDefault();
        //requisitionRoleSkillList.BasicLevel = (from rrs in hrmsEntities.RequisitionRoleSkills.AsNoTracking()
        //                                       where rrs.RequistiionRoleDetailID == requisitionRoleDetailId &&
        //                                       rrs.CompetencyAreaId == competencyAreaId && rrs.ProficiencyLevelId == basic
        //                                       select new RequisitionRoleSkills
        //                                       {
        //                                           SkillId = (int)rrs.SkillId,
        //                                           SkillName = rrs.Skill.SkillCode,
        //                                           RRSkillId = rrs.RRSkillId,
        //                                           RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                           TRId = rrs.TRId,
        //                                           skillID = (int)rrs.SkillId,
        //                                           skillName = rrs.Skill.SkillCode
        //                                       }).ToList();

        //int beginner = hrmsEntities.ProficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Beginner.ToString().ToLower())).Select(i => i.ProficiencyLevelId).FirstOrDefault();
        //requisitionRoleSkillList.BeginnerLevel = (from rrs in hrmsEntities.RequisitionRoleSkills.AsNoTracking()
        //                                          where rrs.RequistiionRoleDetailID == requisitionRoleDetailId &&
        //                                          rrs.CompetencyAreaId == competencyAreaId && rrs.ProficiencyLevelId == beginner
        //                                          select new RequisitionRoleSkills
        //                                          {
        //                                              SkillId = (int)rrs.SkillId,
        //                                              SkillName = rrs.Skill.SkillCode,
        //                                              RRSkillId = rrs.RRSkillId,
        //                                              RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                              TRId = rrs.TRId,
        //                                              skillID = (int)rrs.SkillId,
        //                                              skillName = rrs.Skill.SkillCode
        //                                          }).ToList();

        //int intermediate = hrmsEntities.ProficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Intermediate.ToString().ToLower())).Select(i => i.ProficiencyLevelId).FirstOrDefault();
        //requisitionRoleSkillList.IntermediateLevel = (from rrs in hrmsEntities.RequisitionRoleSkills.AsNoTracking()
        //                                              where rrs.RequistiionRoleDetailID == requisitionRoleDetailId &&
        //                                              rrs.CompetencyAreaId == competencyAreaId && rrs.ProficiencyLevelId == intermediate
        //                                              select new RequisitionRoleSkills
        //                                              {
        //                                                  SkillId = (int)rrs.SkillId,
        //                                                  SkillName = rrs.Skill.SkillCode,
        //                                                  RRSkillId = rrs.RRSkillId,
        //                                                  RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                                  TRId = rrs.TRId,
        //                                                  skillID = (int)rrs.SkillId,
        //                                                  skillName = rrs.Skill.SkillCode
        //                                              }).ToList();

        //int advance = hrmsEntities.ProficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Advanced.ToString().ToLower())).Select(i => i.ProficiencyLevelId).FirstOrDefault();
        //requisitionRoleSkillList.AdvanceLevel = (from rrs in hrmsEntities.RequisitionRoleSkills.AsNoTracking()
        //                                         where rrs.RequistiionRoleDetailID == requisitionRoleDetailId &&
        //                                         rrs.CompetencyAreaId == competencyAreaId && rrs.ProficiencyLevelId == advance
        //                                         select new RequisitionRoleSkills
        //                                         {
        //                                             SkillId = (int)rrs.SkillId,
        //                                             SkillName = rrs.Skill.SkillCode,
        //                                             RRSkillId = rrs.RRSkillId,
        //                                             RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                             TRId = rrs.TRId,
        //                                             skillID = (int)rrs.SkillId,
        //                                             skillName = rrs.Skill.SkillCode
        //                                         }).ToList();

        //int expert = hrmsEntities.ProficiencyLevels.Where(p => p.ProficiencyLevelCode.ToLower().Equals(Enumeration.ProficiencyLevels.Expert.ToString().ToLower())).Select(i => i.ProficiencyLevelId).FirstOrDefault();
        //requisitionRoleSkillList.ExpertLevel = (from rrs in hrmsEntities.RequisitionRoleSkills.AsNoTracking()
        //                                        where rrs.RequistiionRoleDetailID == requisitionRoleDetailId &&
        //                                        rrs.CompetencyAreaId == competencyAreaId && rrs.ProficiencyLevelId == expert
        //                                        select new RequisitionRoleSkills
        //                                        {
        //                                            SkillId = (int)rrs.SkillId,
        //                                            SkillName = rrs.Skill.SkillCode,
        //                                            RRSkillId = rrs.RRSkillId,
        //                                            RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                            TRId = rrs.TRId,
        //                                            skillID = (int)rrs.SkillId,
        //                                            skillName = rrs.Skill.SkillCode
        //                                        }).ToList();
        //return requisitionRoleSkillList;
        //        return null;
        //    }
        //}
        #endregion

        #region DeleteRequisitionRoleDetails
        /// <summary>
        /// DeleteRequisitionRoleDetails
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRequisitionRoleDetails(int requisitionRoleDetailId, int talentRequisitionId, int roleMasterId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteRequisitionRoleDetails] @RequisitionRoleDetailId, @TalentRequisitionId, @RoleMasterId",
                                   new object[] {
                                        new SqlParameter ("RequisitionRoleDetailId", requisitionRoleDetailId),
                                        new SqlParameter ("TalentRequisitionId", talentRequisitionId),
                                        new SqlParameter ("RoleMasterId", roleMasterId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting a requisition role.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region AddRequisitionRoleSkills
        /// <summary>
        /// Add Requisition Role Skills.
        /// </summary>
        /// <param name="requisitionRoleSkills"></param>
        /// <returns></returns>
        public async Task<bool> AddRequisitionRoleSkills(List<RequisitionRoleSkills> requisitionRoleSkills)
        {
            int rowsAffected = 0;
            int RoleSkillMappingID;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        RequisitionRoleSkills requisitonRoleSkill = (from requisition in requisitionRoleSkills
                                                                     select new RequisitionRoleSkills
                                                                     {
                                                                         TalentRequisitionId = requisition.TalentRequisitionId,
                                                                         RequistionRoleDetailID = requisition.RequistionRoleDetailID,
                                                                         SkillGroupId = requisition.SkillGroupId

                                                                     }).First();

                        RoleSkillMappingID = await apEntities.Database.SqlQuery<int>
                          ("[usp_AddRequisitionRoleSkillMapping] @TRID, @RequistiionRoleDetailID, @SkillGroupID",
                              new object[] {
                                         new SqlParameter ("TRID", requisitonRoleSkill.TalentRequisitionId),
                                         new SqlParameter ("RequistiionRoleDetailID", requisitonRoleSkill.RequistionRoleDetailID),
                                         new SqlParameter ("SkillGroupID", requisitonRoleSkill.SkillGroupId)
                              }
                              ).SingleOrDefaultAsync();

                        foreach (RequisitionRoleSkills roleSkill in requisitionRoleSkills)
                        {
                            //Contains atleast one skill
                            if (roleSkill.SkillId != 0)
                            {
                                rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_AddRequisitionRoleSkills] @RoleSkillMappingID, @CompetencyAreaID, @SkillGroupID, @SkillID, @ProficiencyLevelID, @CreatedUser, @CreatedDate, @SystemInfo",
                                       new object[] {
                                        new SqlParameter ("RoleSkillMappingID", RoleSkillMappingID),
                                        new SqlParameter ("CompetencyAreaID", roleSkill.CompetencyAreaId),
                                        new SqlParameter ("SkillGroupID", roleSkill.SkillGroupId),
                                        new SqlParameter ("SkillID", roleSkill.SkillId),
                                        new SqlParameter ("ProficiencyLevelID", roleSkill.ProficiencyLevelId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                       }
                                       ).SingleOrDefaultAsync();
                            }
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while adding requisition role skills.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetRequisitionRoleSkillsByID
        /// <summary>
        ///  Get Requisition Role Skills.
        /// </summary>
        /// <param name="TRID"></param>
        /// <param name="RequistionRoleDetailID"></param>
        /// <returns>List</returns>
        public async Task<List<RequisitionRoleSkills>> GetRequisitionRoleSkillsByID(int TRID, int RequistionRoleDetailID)
        {
            List<RequisitionRoleSkills> lstSkills;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkills = await apEntities.Database.SqlQuery<RequisitionRoleSkills>
                              ("[usp_GetRequisitionRoleSkillsByID] @TRID, @RequistionRoleDetailID",
                              new object[] {
                                        new SqlParameter ("TRID", TRID),
                                        new SqlParameter ("RequistionRoleDetailID", RequistionRoleDetailID)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get skills.");
            }
            return lstSkills;
        }
        #endregion

        #region GetRoleDetailsByTrID
        /// <summary>
        ///  Get Role Details by TrID.
        /// </summary>
        /// <param name="TRID"></param>
        /// <returns>List</returns>
        public async Task<List<RequisitionRoleDetails>> GetRoleDetailsByTrID(int TRID)
        {
            List<RequisitionRoleDetails> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<RequisitionRoleDetails>
                              ("[usp_GetRoleDetailsByTrID] @TRID",
                              new object[] {
                                        new SqlParameter ("TRID", TRID)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles.");
            }
            return lstRoles;
        }
        #endregion

        #region GetSkillDetailsByTrRoleID
        /// <summary>
        ///  Get Skill Details by Tr and RoleID.
        /// </summary>
        /// <param name="TRID"></param>
        /// <param name="RoleID"></param>
        /// <returns>List</returns>
        public async Task<List<RequisitionRoleDetails>> GetSkillDetailsByTrRoleID(int TRID, int RoleID)
        {
            List<RequisitionRoleDetails> lstSkills;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkills = await apEntities.Database.SqlQuery<RequisitionRoleDetails>
                              ("[usp_GetSkillDetailsByTrRoleID] @TRID, @RoleID",
                              new object[] {
                                        new SqlParameter ("TRID", TRID),
                                        new SqlParameter ("RoleID", RoleID)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get skills.");
            }
            return lstSkills;
        }
        #endregion

        #region GetExperienceByTrID
        /// <summary>
        ///  Get Experience by TrID.
        /// </summary>
        /// <param name="TRID"></param>
        /// <returns>List</returns>
        public async Task<List<RequisitionRoleDetails>> GetExperienceByTrID(int TRID)
        {
            List<RequisitionRoleDetails> lstExperiences;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstExperiences = await apEntities.Database.SqlQuery<RequisitionRoleDetails>
                              ("[usp_GetExperienceByTrID] @TRID",
                              new object[] {
                                        new SqlParameter ("TRID", TRID)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get experiences.");
            }
            return lstExperiences;
        }
        #endregion

        //#region GetReqRoleSkillsByRRId
        ///// <summary>
        ///// GetReqRoleSkillsByRRId
        ///// </summary>
        ///// <param name="requisitionRoleDetailId"></param>
        ///// <returns></returns>
        //public var GetReqRoleSkillsDetailsByRRId(int requisitionRoleDetailId)
        //{
        //    using (APEntities hrmsEntities = new APEntities())
        //    {
        //       var requisitionRoleDetails = (from rrs in hrmsEntities.RequisitionRoleSkills
        //                                                              join skill in hrmsEntities.Skills
        //                                                              on rrs.SkillId equals skill.SkillId
        //                                                              where rrs.RequistiionRoleDetailID == requisitionRoleDetailId
        //                                                              select new 
        //                                                              {
        //                                                                  RRSkillId = rrs.RRSkillId,
        //                                                                  RequisitionRoleDetailID = rrs.RequistiionRoleDetailID,
        //                                                                  TRId = rrs.TRId,
        //                                                                  SkillId = rrs.SkillId,
        //                                                                  IsPrimary = (bool)rrs.IsPrimary,
        //                                                                  OtherSkillCode = skill.SkillCode
        //                                                              }).OrderByDescending(rr => rr.IsPrimary).ToList();
        //        return requisitionRoleDetails;
        //    }
        //}
        //#endregion

        #endregion

        #region WorkPlaceBehaviorMethods
        #region GetWorkPlaceBehaviorByTrId
        /// <summary>
        /// GetWorkPlaceBehaviorByTrId
        /// </summary>
        /// <param name="trId"></param>
        /// <param name="roleMasterID"></param>
        /// <returns></returns>
        public List<RequisitionWorkPlaceBehavior> GetWorkPlaceBehaviorByTrId(int trId, int roleMasterId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<RequisitionWorkPlaceBehavior> reqWorkPlaceBehavior = (from rba in hrmsEntities.RequisitionBehaviorAreas
                                                                                 join ba in hrmsEntities.BehaviorAreas on rba.BehaviorAreaId equals ba.BehaviorAreaId
                                                                                 join tr in hrmsEntities.TalentRequisitions on rba.TRId equals tr.TRId
                                                                                 where tr.IsActive == true && rba.TRId == trId && rba.RoleMasterId == roleMasterId
                                                                                 select new RequisitionWorkPlaceBehavior
                                                                                 {
                                                                                     RequisitionBehaviorAreaId = rba.RequisitionBehaviorAreaId,
                                                                                     TalentRequisitionId = rba.TRId,
                                                                                     RoleMasterId = rba.RoleMasterId,
                                                                                     BehaviorAreaId = rba.BehaviorAreaId,
                                                                                     BehaviorArea = ba.BehaviorAreaDescription,
                                                                                     BehaviorCharacteristics = rba.BehaviorCharacteristics,
                                                                                     BehaviorRatingId = rba.BehaviorRatingId
                                                                                 });
                return reqWorkPlaceBehavior.ToList<RequisitionWorkPlaceBehavior>();
            }
        }
        #endregion

        #region SaveRequisitionBehaviorArea
        /// <summary>
        /// SaveRequisitionBehaviorArea
        /// </summary>
        /// <param name="behaviorAreaDataList"></param>
        /// <returns></returns>
        public bool SaveRequisitionBehaviorArea(List<RequisitionWorkPlaceBehavior> behaviorAreaDataList)
        {
            bool status = false;
            string currentUser = HttpContext.Current.User.Identity.Name;
            string systemInfo = Commons.GetClientIPAddress();
            using (APEntities hrmsEntities = new APEntities())
            {
                if (behaviorAreaDataList.Count > 0)
                {
                    foreach (RequisitionWorkPlaceBehavior behaviorAreaData in behaviorAreaDataList)
                    {
                        RequisitionBehaviorArea requisitionBehaviorArea = hrmsEntities.RequisitionBehaviorAreas.Where(behaviorArea => behaviorArea.RequisitionBehaviorAreaId == behaviorAreaData.RequisitionBehaviorAreaId).FirstOrDefault();
                        if (requisitionBehaviorArea != null)
                        {
                            requisitionBehaviorArea.TRId = behaviorAreaData.TalentRequisitionId;
                            requisitionBehaviorArea.RoleMasterId = behaviorAreaData.RoleMasterId;
                            requisitionBehaviorArea.BehaviorAreaId = behaviorAreaData.BehaviorAreaId;
                            requisitionBehaviorArea.BehaviorCharacteristics = behaviorAreaData.BehaviorCharacteristics;
                            requisitionBehaviorArea.BehaviorRatingId = behaviorAreaData.BehaviorRatingId == null ? null : behaviorAreaData.BehaviorRatingId;
                            requisitionBehaviorArea.SystemInfo = systemInfo;
                            requisitionBehaviorArea.ModifiedBy = currentUser;
                            requisitionBehaviorArea.ModifiedDate = DateTime.Now;
                            hrmsEntities.Entry(requisitionBehaviorArea).State = EntityState.Modified;
                        }
                        else
                        {
                            requisitionBehaviorArea = new RequisitionBehaviorArea()
                            {
                                TRId = behaviorAreaData.TalentRequisitionId,
                                RoleMasterId = behaviorAreaData.RoleMasterId,
                                BehaviorAreaId = behaviorAreaData.BehaviorAreaId,
                                BehaviorCharacteristics = behaviorAreaData.BehaviorCharacteristics,
                                BehaviorRatingId = behaviorAreaData.BehaviorRatingId == null ? null : behaviorAreaData.BehaviorRatingId,
                                SystemInfo = systemInfo,
                                CreatedBy = currentUser,
                                CreatedDate = DateTime.Now
                            };
                            hrmsEntities.RequisitionBehaviorAreas.Add(requisitionBehaviorArea);
                        }
                    }
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #endregion

        #region GetExpertiseAreas
        /// <summary>
        /// Get Expertise Areas
        /// </summary>
        public async Task<List<ExpertiseAreaData>> GetExpertiseAreas()
        {
            List<ExpertiseAreaData> lstExpertiseAreas;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstExpertiseAreas = await apEntities.Database.SqlQuery<ExpertiseAreaData>
                              ("[usp_GetExpertiseArea]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get expertise areas");
            }
            return lstExpertiseAreas;
        }
        #endregion

        #region GetExpertiseAreaByTrId
        /// <summary>
        /// Get Expertise Area by TrId
        /// </summary>
        /// <param name="trId"></param>
        /// <returns></returns>
        public async Task<List<RequisitionExpertiseArea>> GetExpertiseAreaByTrId(int trId, int roleMasterId)
        {
            List<RequisitionExpertiseArea> lstReqExpertiseAreas;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    lstReqExpertiseAreas = await hrmsEntities.Database.SqlQuery<RequisitionExpertiseArea>
                                  ("[usp_GetExpertiseAreaByTrId] @TalentRequisitionId, @RoleMasterId",
                                    new object[]  {
                                        new SqlParameter ("TalentRequisitionId", trId),
                                        new SqlParameter ("RoleMasterId", roleMasterId)
                                   }
                                   ).ToListAsync();
                }
            }
            catch
            {
                throw;
            }
            return lstReqExpertiseAreas;

        }
        #endregion

        #region SaveRequisitionExpertareas
        /// <summary>
        /// Save Requisition Expert areas
        /// </summary>
        /// <param name="requisitionExpertiseAreaDetails"></param>
        /// <returns></returns>
        public async Task<bool> SaveRequisitionExpertiseAreas(RequisitionExpertiseArea requisitionExpertiseAreaDetails)
        {
            int rowsAffected;

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_SaveRequisitionExpertiseArea] @TalentRequisitionId, @RoleMasterId, @ExpertiseAreaId, @ExpertiseDescription, @CreatedUser, @CreatedDate, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("TalentRequisitionId", requisitionExpertiseAreaDetails.TalentRequisitionId),
                                        new SqlParameter ("RoleMasterId", requisitionExpertiseAreaDetails.RoleMasterId),
                                        new SqlParameter ("ExpertiseAreaId",requisitionExpertiseAreaDetails.ExpertiseAreaId),
                                        new SqlParameter ("ExpertiseDescription", requisitionExpertiseAreaDetails.ExpertiseDescription),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch
            {
                throw;
            }

            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region SubmitRequisitionForApproval
        /// <summary>
        /// Submit Requisition for approval
        /// </summary>
        /// <param name="submitRequisitionForApproval"></param>
        /// <returns></returns>
        public async Task<bool> SubmitRequisitionForApproval(SubmitRequisitionForApproval submitRequisitionForApproval)
        {
            int statusId = Convert.ToInt32(Enumeration.TalentRequisitionStatusCodes.SubmittedForApproval);
            int rowsAffected;
            try
            {
                //Tag employees for requisition.
                EmployeeTag(submitRequisitionForApproval.employeeTagDetails, submitRequisitionForApproval.TalentRequisitionID);

                using (var apEntities = new APEntities())
                {
                    string ApprovedByID = string.Join(",", submitRequisitionForApproval.ApprovedByID);

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        //Update requisition and add workflow.
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_SubmitRequisitionByTrId] @Talentrequisitionid, @StatusId, @FromEmployeeId, @ToEmployeeId, @Modifieduser, @Modifieddate, @Systeminfo",
                                   new object[] {
                                        new SqlParameter ("Talentrequisitionid", submitRequisitionForApproval.TalentRequisitionID),
                                        new SqlParameter ("StatusId", statusId),
                                        new SqlParameter ("FromEmployeeId", submitRequisitionForApproval.SubmittedByID),
                                        new SqlParameter ("ToEmployeeId", ApprovedByID),
                                        new SqlParameter ("Modifieduser",  HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Modifieddate", DateTime.UtcNow),
                                        new SqlParameter ("Systeminfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                    if (rowsAffected > 0)
                    {
                        List<int> employeeIds = ApprovedByID.ToString().Split(',').Select(int.Parse).ToList();
                        string EmailId = "";
                        foreach (var employee in employeeIds)
                        {
                            EmailId = EmailId + (from employe in apEntities.Employees
                                                 join user in apEntities.Users
                                                 on employe.UserId equals user.UserId
                                                 where employe.EmployeeId == employee
                                                 select new
                                                 {
                                                     user.EmailAddress
                                                 }).FirstOrDefault().EmailAddress + ";";

                        }
                        EmailId = EmailId.TrimEnd(';');
                        NotificationForApproval(submitRequisitionForApproval.TalentRequisitionID, submitRequisitionForApproval.SubmittedByID, EmailId);

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetLearningAptitudesByTrId
        /// <summary>
        /// Get Expertise Area by TrId
        /// </summary>
        /// <param name="trId"></param>
        /// <param name="roleMasterId"></param>
        /// <returns></returns>
        public async Task<List<RequisitionLearningAptitudeData>> GetLearningAptitudesByTrId(int trId, int roleMasterId)
        {
            try
            {
                List<RequisitionLearningAptitudeData> lstReqExpertiseAreas;

                using (APEntities hrmsEntities = new APEntities())
                {
                    lstReqExpertiseAreas = await hrmsEntities.Database.SqlQuery<RequisitionLearningAptitudeData>
                                  ("[usp_GetLearningAptitudeByTrId] @TalentRequisitionId, @RoleMasterId",
                                   new object[]  {
                                        new SqlParameter ("TalentRequisitionId", trId),
                                        new SqlParameter ("RoleMasterId", roleMasterId)
                                   }
                                   ).ToListAsync();


                }
                return lstReqExpertiseAreas;
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region SaveRequisitionLearningAptitude
        /// <summary>
        /// Save Requisition Learning Aptitude Details
        /// </summary>
        /// <param name="requisitionExpertiseAreaDetails"></param>
        /// <returns></returns>
        public async Task<bool> SaveRequisitionLearningAptitude(RequisitionLearningAptitudeData requisitionLearningAptitudeDetails)
        {
            int rowsAffected;

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_SaveRequisitionLearningAptitude] @TalentRequisitionId, @RoleMasterId, @LearningAptiudeID, @CreatedUser, @CreatedDate, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("TalentRequisitionId", requisitionLearningAptitudeDetails.TalentRequisitionId),
                                        new SqlParameter ("RoleMasterId", requisitionLearningAptitudeDetails.RoleMasterId),
                                        new SqlParameter ("LearningAptiudeID",requisitionLearningAptitudeDetails.LearningAptitudeId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch
            {
                throw;
            }

            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region ChecksDuplicateRoleAgainstTrId
        /// <summary>
        /// Checks whether it's dupllicate role against TRID or not.
        /// </summary>
        /// <param name="requisitionRoleDetails"></param>
        /// <returns></returns>
        public bool ChecksDuplicateRoleAgainstTrId(RequisitionRoleDetails requisitionRoleDetails)
        {
            bool returnValue = false;
            RequisitionRoleDetail requisitionRole = null;
            using (APEntities hrmsEntities = new APEntities())
            {
                if (requisitionRoleDetails.RequisitionRoleDetailID <= 0)
                {
                    requisitionRole = hrmsEntities.RequisitionRoleDetails.Where(rr => rr.TRId == requisitionRoleDetails.TalentRequisitionId && rr.RoleMasterId == requisitionRoleDetails.RoleMasterId).FirstOrDefault();
                    if (requisitionRole != null)
                    {
                        returnValue = true;
                    }
                }
            }
            return returnValue;
        }
        #endregion


        #region GetRolesByTalentRequisitionId
        /// <summary>
        ///  Get Roles by Talent Requisition ID.
        /// </summary>
        /// <param name="TalentRequisitionId"></param>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetRolesByTalentRequisitionId(int TalentRequisitionId)
        {
            List<GenericType> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetRolesByTalentRequisitionId] @TalentRequisitionId",
                              new object[] {
                                        new SqlParameter ("TalentRequisitionId", TalentRequisitionId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles.");
            }
            return lstRoles;
        }
        #endregion

        #region GetSkillDetailsByTalentRequisitionRoleId
        /// <summary>
        ///  Get Skill Details by Talent requisition and RoleId.
        /// </summary>
        /// <param name="TalentRequisitionId"></param>
        /// <param name="RoleId"></param>
        /// <returns>List</returns>
        public async Task<RequisitionSearchFilter> GetSkillDetailsByTalentRequisitionRoleId(int TalentRequisitionId, int RoleMasterId)
        {
            RequisitionSearchFilter searchFilter = new RequisitionSearchFilter();
            try
            {
                using (var apEntities = new APEntities())
                {
                    int? noOfBillablePositions = await apEntities.RequisitionRoleDetails.Where(x => x.TRId == TalentRequisitionId && x.RoleMasterId == RoleMasterId).Select(x => x.NoOfBillablePositions).SingleOrDefaultAsync();
                    int? noOfNonBillablePositions = await apEntities.RequisitionRoleDetails.Where(x => x.TRId == TalentRequisitionId && x.RoleMasterId == RoleMasterId).Select(x => x.NoOfNonBillablePositions).SingleOrDefaultAsync();

                    if (noOfBillablePositions == null)
                        noOfBillablePositions = 0;

                    if (noOfNonBillablePositions == null)
                        noOfNonBillablePositions = 0;

                    searchFilter = await apEntities.Database.SqlQuery<RequisitionSearchFilter>
                              ("[usp_GetExperienceByTalentRequistionId] @TalentRequistionId, @RoleMasterId",
                              new object[] {
                                        new SqlParameter ("TalentRequistionId", TalentRequisitionId),
                                        new SqlParameter ("RoleMasterId", RoleMasterId)
                              }).FirstOrDefaultAsync();

                    searchFilter.Skills = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetSkillDetailsByTalentRequisitionRoleId] @TalentRequisitionId, @RoleMasterId",
                              new object[] {
                                        new SqlParameter ("TalentRequisitionId", TalentRequisitionId),
                                        new SqlParameter ("RoleMasterId", RoleMasterId)
                              }).ToListAsync();
                    searchFilter.TotalPositions = noOfBillablePositions.Value + noOfNonBillablePositions.Value;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get skills.");
            }
            return searchFilter;
        }
        #endregion

        #region GetRequisitionDetailsBySearch
        /// <summary>
        ///  Get Requisition details by search criteria.
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns>List</returns>
        public SkillSearchDetails GetRequisitionDetailsBySearch(RequisitionSearchFilter searchFilter)
        {
            SkillSearchDetails searchResult = new SkillSearchDetails();
            try
            {
                using (var apEntities = new APEntities())
                {
                    string skills = string.Join(",", searchFilter.Skills.Select(x => x.Id));
                    string proficiency = string.Join(",", searchFilter.Proficiencies.Select(x => x.Id));

                    using (var connection = apEntities.Database.Connection)
                    {
                        connection.Open();
                        var command = connection.CreateCommand();
                        command.CommandText = "EXEC [dbo].[usp_GetMatchingProfiles] @RoleId, @SkillId, @ProficiencyLevelId, @MinimumExperience, @MaximumExperience, @TalentRequisitionId";

                        SqlParameter param = new SqlParameter();
                        param.ParameterName = "@RoleId";
                        param.Value = searchFilter.RoleId;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@SkillId";
                        param.Value = skills;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@ProficiencyLevelId";
                        param.Value = proficiency;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@MinimumExperience";
                        param.Value = searchFilter.MinimumExperience;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@MaximumExperience";
                        param.Value = searchFilter.MaximumExperience;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@TalentRequisitionId";
                        param.Value = searchFilter.TalentRequisitionId;
                        command.Parameters.Add(param);

                        using (var reader = command.ExecuteReader())
                        {
                            searchResult.talentRequisitionEmployeeDetails = ((IObjectContextAdapter)apEntities)
                                        .ObjectContext
                                        .Translate<TalentRequisitionEmployeeDetails>(reader)
                                        .ToList();

                            reader.NextResult();
                            searchResult.talentRequisitionProjectDetails = ((IObjectContextAdapter)apEntities)
                                        .ObjectContext
                                        .Translate<TalentRequisitionProjectDetails>(reader)
                                         .ToList();

                            reader.NextResult();
                            searchResult.talentRequisitionSkillDetails = ((IObjectContextAdapter)apEntities)
                                          .ObjectContext
                                          .Translate<TalentRequisitionSkillDetails>(reader)
                                           .ToList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get matching profiles.");
            }
            return searchResult;
        }
        #endregion

        #region EmployeeTag
        /// <summary>
        /// Tag an employee for a requisition
        /// </summary>
        /// <param name="employeeTag"></param>
        /// <returns></returns>
        private void EmployeeTag(List<EmployeeTagDetails> employeeTag, int trId)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        var DistinctItems = employeeTag.GroupBy(x => x.EmployeeId).Select(y => y.First());

                        foreach (var employee in DistinctItems)
                        {
                            rowsAffected = apEntities.Database.SqlQuery<int>
                               ("[usp_TalentRequisitionEmployeeTag] @TalentRequisitionID, @EmployeeId, @RoleMasterID, @CreatedUser, @CreatedDate, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("TalentRequisitionID", trId),
                                        new SqlParameter ("EmployeeId", employee.EmployeeId),
                                        new SqlParameter ("RoleMasterID", employee.RoleMasterID),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefault();

                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while adding tagged employee.");
            }
        }
        #endregion

        #region GetTalentRequisitionApprovers
        /// <summary>
        ///  Get talent requisition approvers list.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetTalentRequisitionApprovers()
        {
            List<GenericType> lstApprovers;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstApprovers = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetTalentRequisitionApprovers]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get approvers.");
            }
            return lstApprovers;
        }
        #endregion

        #region NotificationForApproval
        /// <summary>
        /// Notification To Submit Requisition For Approval
        /// </summary>
        /// <param name="employeeTag"></param>
        /// <returns></returns>
        public void NotificationForApproval(int TalentRequisitionId, int SubmittedByID, string ToEmailID)
        {
            using (var hrmEntities = new APEntities())
            {

                EmailNotificationConfiguration emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                          ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                            new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.NotificationType.TRSbmitForApproval)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.TalentRequisition))
                           }
                          ).FirstOrDefault();

                if (emailNotificationConfig != null)
                {
                    List<string> roles = new List<string>();
                    roles = hrmEntities.Database.SqlQuery<string>
                                 ("[usp_GetNotificationDetailsToSubmitTR] @TalentRequisitionId",
                                   new object[]  {
                                    new SqlParameter ("TalentRequisitionId", TalentRequisitionId),
                                  }
                                 ).ToList();

                    if (roles != null)
                    {
                        int? ProjectId = hrmEntities.TalentRequisitions.Where(project => project.TRId == TalentRequisitionId).Select(id => id.ProjectId).FirstOrDefault();
                        string ProjectName = hrmEntities.Projects.Where(project => project.ProjectId == ProjectId).Select(name => name.ProjectName).FirstOrDefault();
                        string TrCode = hrmEntities.TalentRequisitions.Where(trcode => trcode.TRId == TalentRequisitionId).Select(code => code.TRCode).FirstOrDefault();
                        string SubmittedByName = hrmEntities.Employees.Where(submittedby => submittedby.EmployeeId == SubmittedByID).Select(name => name.FirstName + " " + name.LastName).FirstOrDefault();

                        NotificationDetail notificationDetail = new NotificationDetail();
                        StringBuilder emailContent = new StringBuilder(emailNotificationConfig.EmailContent);


                        emailContent = emailContent.Replace("{TRCode}", TrCode)
                                                   .Replace("{ProjectName}", ProjectName)
                                                   .Replace("{RoleName}", string.Join(",", roles))
                                                   .Replace("{SubmittedBy}", SubmittedByName);

                        if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                            throw new AssociatePortalException("Email From cannot be blank");
                        notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                        if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                            throw new AssociatePortalException("Email To cannot be blank");

                        notificationDetail.ToEmail = ToEmailID;

                        notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                        StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
                        Subject = Subject.Replace("{TRCode}", TrCode)
                                         .Replace("{ProjectName}", ProjectName);
                        notificationDetail.Subject = Subject.ToString();

                        notificationDetail.EmailBody = emailContent.ToString();
                        //Send Email Notification.
                        NotificationManager.SendEmail(notificationDetail);
                    }

                }
            }
        }
        #endregion

        #region GetTaggedEmployeesByTalentRequisitionId
        /// <summary>
        ///  Get Tagged Employees by Talent Requisition ID.
        /// </summary>
        /// <param name="talentRequisitionId"></param>
        /// <returns>List</returns>
        public async Task<List<EmployeeTagDetails>> GetTaggedEmployeesByTalentRequisitionId(int talentRequisitionId)
        {
            List<EmployeeTagDetails> lstTaggedEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstTaggedEmployees = await apEntities.Database.SqlQuery<EmployeeTagDetails>
                              ("[usp_GetTaggedEmployeesByTalentRequisitionId] @TalentRequisitionId",
                              new object[] {
                                        new SqlParameter ("TalentRequisitionId", talentRequisitionId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get tagged employees.");
            }
            return lstTaggedEmployees;
        }
        #endregion

        #region DeleteRequisitionRoleDetails
        /// <summary>
        /// DeleteRequisitionRoleDetails
        /// </summary>
        /// <param name="requisitionRoleDetailId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteTaggedEmployees(List<int> Ids)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        Parallel.ForEach(Ids, taggedEmployeeID =>
                        rowsAffected = apEntities.Database.SqlQuery<int>
                          ("[usp_DeleteTaggedEmployees] @Id",
                              new object[] {
                                        new SqlParameter ("Id", taggedEmployeeID)
                              }
                              ).SingleOrDefault()
                        );
                        trans.Commit();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting tagged employee.");
            }
            return true;
        }
        #endregion

        #region RejectTalentRequisition
        /// <summary>
        /// Reject talent requisition
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns>bool</returns>
        public async Task<bool> RejectTalentRequisition(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        int? ToEmployeeId;
                        ToEmployeeId = apEntities.TalentRequisitions.Where(requisition => requisition.TRId == talentRequisitionDetails.TalentRequisitionId).Select(employee => employee.RaisedBy).FirstOrDefault();

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_RejectTalentRequisition] @TalentrequisitionId, @RejectedByID, @ToEmployeeId, @StatusId, @Comments, @Modifieduser, @Modifieddate, @Createduser, @Createddate, @Systeminfo",
                                   new object[] {
                                        new SqlParameter ("TalentrequisitionId", talentRequisitionDetails.TalentRequisitionId),
                                        new SqlParameter ("RejectedByID", talentRequisitionDetails.FromEmployeeID),
                                        new SqlParameter ("ToEmployeeId", ToEmployeeId),
                                        new SqlParameter ("StatusId", talentRequisitionDetails.StatusId),
                                        new SqlParameter ("Comments", talentRequisitionDetails.Comments),
                                        new SqlParameter ("Modifieduser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Modifieddate", DateTime.UtcNow),
                                        new SqlParameter ("Createduser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Createddate", DateTime.UtcNow),
                                        new SqlParameter ("Systeminfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();

                        if (rowsAffected > 0)
                        {
                            //Send notification email to finance head
                            //List<int> employeeIds = ApprovedByID.Split(',').Select(int.Parse).ToList();
                            string EmailId = "";
                            EmailId = EmailId + (from employe in apEntities.Employees
                                                 join user in apEntities.Users
                                                 on employe.UserId equals user.UserId
                                                 where employe.EmployeeId == ToEmployeeId
                                                 select new
                                                 {
                                                     user.EmailAddress
                                                 }).FirstOrDefault().EmailAddress + ";";


                            EmailId = EmailId.TrimEnd(';');
                            new TalentRequisitionDetails().NotificationForRejection(talentRequisitionDetails.TalentRequisitionId, talentRequisitionDetails.Comments, EmailId);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while approving talent requisiton.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region NotificationForRejection
        /// <summary>
        /// Notification To Submit Requisition For Rejection
        /// </summary>
        /// <returns></returns>
        public void NotificationForRejection(int TalentRequisitionId, string Comments, string ToEmailID)
        {
            using (var hrmEntities = new APEntities())
            {

                EmailNotificationConfiguration emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                          ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                            new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.NotificationType.TRRejected)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.TalentRequisition))
                           }
                          ).FirstOrDefault();

                if (emailNotificationConfig != null)
                {
                    //List<string> roles = new List<string>();
                    //roles = hrmEntities.Database.SqlQuery<string>
                    //             ("[usp_GetNotificationDetailsToSubmitTR] @TalentRequisitionId",
                    //               new object[]  {
                    //                new SqlParameter ("TalentRequisitionId", TalentRequisitionId),
                    //              }
                    //             ).ToList();

                    //if (roles != null)
                    //{
                        int? ProjectId = hrmEntities.TalentRequisitions.Where(project => project.TRId == TalentRequisitionId).Select(id => id.ProjectId).FirstOrDefault();
                        string ProjectName = hrmEntities.Projects.Where(project => project.ProjectId == ProjectId).Select(name => name.ProjectName).FirstOrDefault();
                        string TrCode = hrmEntities.TalentRequisitions.Where(trcode => trcode.TRId == TalentRequisitionId).Select(code => code.TRCode).FirstOrDefault();
                        //string Comments = hrmEntities.TalentRequisitionWorkFlows.Where(TRcomment => TRcomment.TalentRequisitionID == TalentRequisitionId).Select(comment => comment.Comments).FirstOrDefault();
                        
                        NotificationDetail notificationDetail = new NotificationDetail();
                        StringBuilder emailContent = new StringBuilder(emailNotificationConfig.EmailContent);


                        emailContent = emailContent.Replace("{TRCode}", TrCode)
                                                   .Replace("{ProjectName}", ProjectName)
                                                   //.Replace("{RoleName}", string.Join(",", roles))
                                                   .Replace("{Comments}", Comments);

                        if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                            throw new AssociatePortalException("Email From cannot be blank");
                        notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                        if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                            throw new AssociatePortalException("Email To cannot be blank");

                        notificationDetail.ToEmail = ToEmailID;

                        notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                        StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
                        Subject = Subject.Replace("{TRCode}", TrCode)
                                         .Replace("{ProjectName}", ProjectName);
                        notificationDetail.Subject = Subject.ToString();

                        notificationDetail.EmailBody = emailContent.ToString();
                        //Send Email Notification.
                        NotificationManager.SendEmail(notificationDetail);
                   // }

                }
            }
        }
        #endregion

    }
}
