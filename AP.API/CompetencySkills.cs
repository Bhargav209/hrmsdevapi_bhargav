﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;

namespace AP.API
{
    public class CompetencySkills
    {
        #region CompetencySkillsMasterMethods       

        #region GetCompetencyArea
        /// <summary>
        /// GetCompetencyArea
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetCompetencyArea()
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var competencyAreaData = (from a in hrmEntities.CompetencyAreas
                                              select new { a.CompetencyAreaId, a.CompetencyAreaCode, a.CompetencyAreaDescription, a.IsActive }).Where(id => id.IsActive == true);

                    return competencyAreaData.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw new AssociatePortalException("Failed to get details.");
            }
        }

        #endregion

        #region GetSkillIDList
        /// <summary>
        /// GetSkillIDList
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetSkillIDList(ICollection<string> skillNameList)
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skillIDList = (from a in hrmEntities.Skills
                                       where a.IsActive == true && skillNameList.Contains(a.SkillName)
                                       select new { a.SkillId }).ToList();

                    return skillIDList;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw new AssociatePortalException("Failed to get details.");
            }
        }

        #endregion

        #region GetCompetencySkillsMappedData
        /// <summary>
        /// GetCompetencySkillsMappedData
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetCompetencySkillsMappedData()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var result = (from competencySkill in hrmsEntities.CompetencySkills
                                  join role in hrmsEntities.Roles on competencySkill.RoleMasterID equals role.RoleId
                                  join competencyArea in hrmsEntities.CompetencyAreas on competencySkill.CompetencyAreaId equals competencyArea.CompetencyAreaId
                                  join skill in hrmsEntities.Skills on competencySkill.SkillId equals skill.SkillId
                                  where competencySkill.IsActive == true
                                  select new
                                  {
                                      competencySkill.CompetencySkillsId,
                                      role.RoleId,
                                      role.RoleName,
                                      competencyArea.CompetencyAreaId,
                                      competencyArea.CompetencyAreaCode,
                                      skill.SkillId,
                                      skill.SkillName,
                                      skill.SkillDescription
                                  });

                    var mergedResult = from row in result.ToList()
                                       group row.SkillName by new { row.RoleName, row.RoleId, row.CompetencyAreaCode, row.CompetencyAreaId } into grp
                                       select new { grp.Key.RoleName, grp.Key.RoleId, grp.Key.CompetencyAreaCode, grp.Key.CompetencyAreaId, Skills = string.Join(",", grp) };

                    return mergedResult.ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region AddUpdateCompetencySkills
        /// <summary>
        /// AddUpdateCompetencySkills
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool AddUpdateCompetencySkills(CompetencySkillDetails details)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var existingRows = (from cs in hrmsEntities.CompetencySkills
                                        where cs.CompetencyAreaId == details.CompetencyID && cs.RoleMasterID == details.RoleID
                                        select new { cs.SkillId }).ToList();

                    var notinInputList = (from dblist in existingRows
                                          where !details.Skills.Any(x => x == dblist.SkillId)
                                          select dblist);

                    foreach (var skill in notinInputList)
                    {
                        CompetencySkill competencySkill = hrmsEntities.CompetencySkills
                                                            .Where(i => i.SkillId == skill.SkillId
                                                                        && i.CompetencyAreaId == details.CompetencyID && i.RoleMasterID == details.RoleID
                                                                    ).FirstOrDefault();
                        if (competencySkill != null)
                        {
                            competencySkill.IsActive = false;
                            hrmsEntities.SaveChanges();
                        }

                    }

                    foreach (var inputSkill in details.Skills)
                    {
                        CompetencySkill skillexits = (from skill in hrmsEntities.CompetencySkills
                                                      where skill.SkillId == inputSkill && skill.CompetencyAreaId == details.CompetencyID && skill.RoleMasterID == details.RoleID
                                                      select skill).FirstOrDefault();
                        if (skillexits == null)
                        {
                            skillexits = new CompetencySkill();
                            skillexits.RoleMasterID = details.RoleID;
                            skillexits.CompetencyAreaId = details.CompetencyID;
                            skillexits.SkillId = inputSkill;
                            skillexits.IsActive = true;
                            skillexits.SystemInfo = details.SystemInfo;
                            skillexits.CreatedUser = details.CurrentUser;
                            skillexits.CreatedDate = DateTime.Now;
                            hrmsEntities.CompetencySkills.Add(skillexits);
                            hrmsEntities.SaveChanges();
                        }
                        else
                        {
                            skillexits.IsActive = true;
                            hrmsEntities.SaveChanges();
                        }
                    }

                    retValue = true;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save/update competency skill details."); ;
            }

            return retValue;
        }
        #endregion

        #region CompetencySkillSearch
        /// <summary>
        /// CompetencySkillSearch
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> CompetencySkillSearch(SearchFilter searchFilter)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var result = (from competencySkill in hrmsEntities.CompetencySkills
                                  join role in hrmsEntities.Roles on competencySkill.RoleMasterID equals role.RoleId
                                  join competencyArea in hrmsEntities.CompetencyAreas on competencySkill.CompetencyAreaId equals competencyArea.CompetencyAreaId
                                  join skill in hrmsEntities.Skills on competencySkill.SkillId equals skill.SkillId
                                  select new
                                  {
                                      competencySkill.CompetencySkillsId,
                                      role.RoleId,
                                      role.RoleName,
                                      competencyArea.CompetencyAreaId,
                                      competencyArea.CompetencyAreaCode,
                                      skill.SkillId,
                                      skill.SkillName
                                  }).ToList();

                    if (searchFilter.SearchData == null)
                        searchFilter.SearchData = "";

                    if (searchFilter != null && searchFilter.SearchType == "RoleName")
                        result = result.Where(i => i.RoleName.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                    if (searchFilter != null && searchFilter.SearchType == "CompetencyArea")
                        result = result.Where(i => i.CompetencyAreaCode.ToUpper().Contains(searchFilter.SearchData.ToUpper())).ToList();

                    var mergedResult = from row in result.ToList()
                                       group row.SkillName by new { row.RoleName, row.RoleId, row.CompetencyAreaCode, row.CompetencyAreaId } into grp
                                       select new { grp.Key.RoleName, grp.Key.RoleId, grp.Key.CompetencyAreaCode, grp.Key.CompetencyAreaId, Skills = string.Join(", ", grp) };

                    return mergedResult.ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetCompetencySkillsByRoleId
        /// <summary>
        /// CompetencySkillSearch
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetCompetencySkillsByRoleId(int roleId)
        {
            string parms = "[]";
            try
            {

                using (APEntities hrmsEntities = new APEntities())
                {
                    var result = (from competencySkill in hrmsEntities.CompetencySkills
                                  join role in hrmsEntities.Roles on competencySkill.RoleMasterID equals role.RoleId
                                  join competencyArea in hrmsEntities.CompetencyAreas on competencySkill.CompetencyAreaId equals competencyArea.CompetencyAreaId
                                  join skill in hrmsEntities.Skills on competencySkill.SkillId equals skill.SkillId
                                  where competencySkill.RoleMasterID == roleId && competencySkill.IsActive == true
                                  select new
                                  {
                                      role.RoleId,
                                      competencySkill.CompetencySkillsId,
                                      competencyArea.CompetencyAreaId,
                                      competencyArea.CompetencyAreaCode,
                                      competencySkill.ProficiencyLevelId,
                                      competencySkill.ProficiencyLevel.ProficiencyLevelCode,
                                      competencySkill.IsPrimary,
                                      skill.SkillId,
                                      skill.SkillCode,
                                      IsDefaultSkill = true
                                  }).ToList();
                    return result;

                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #endregion
    }
}
