﻿using System;
using System.Collections.Generic;
using System.Linq;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.Web;
using System.Data.SqlClient;

namespace AP.API
{
    public class Clients
    {
        #region CreateClient
        /// <summary>
        /// Method to add new client data to database
        /// </summary>
        /// <param name="clientDetails"></param>
        /// <returns></returns>
        public int CreateClient(ClientDetails clientDetails)
        {
            int isCreated ;
            try
            {

                using (APEntities hrmsEntities = new APEntities())
                {
                    

                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {

                        isCreated = hrmsEntities.Database.SqlQuery<int>
                               ("[usp_CreateClient] @ClientCode,@ClientName, @ClientRegisterName,@IsActive,@CreatedUser,@CreatedDate,@SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ClientCode", clientDetails.ClientCode.Trim()),
                                        new SqlParameter ("ClientName", clientDetails.ClientName.Trim()),
                                        new SqlParameter ("ClientRegisterName", (object)clientDetails.ClientRegisterName ?? DBNull.Value),
                                        new SqlParameter ("IsActive", clientDetails.IsActive),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }).SingleOrDefault();
                        trans.Commit();
                    }
                 
                }
            }
            catch(Exception ex)
            {
                throw;
            }

            return isCreated;
        }
        #endregion

        #region UpdateClient
        /// <summary>
        /// Method to update existing client data
        /// </summary>
        /// <param name="clientDetails"></param>
        /// <returns></returns>
        public int UpdateClient(ClientDetails clientDetails)
        {
            int isUpdated;
            using (APEntities hrmsEntities = new APEntities())
            {

                
                if (!(bool)clientDetails.IsActive)
                {
                    //IQueryable<ClientBillingRole> clientBillingRoleQuery = (from clientBillingRole in hrmsEntities.ClientBillingRoles
                    //                                                        where clientBillingRole.ClientId == clientDetails.ClientId
                    //                                                        && clientBillingRole.IsActive == true
                    //                                                        select clientBillingRole);
                    //int isClientBillingRoleExist = clientBillingRoleQuery.Count();

                    //if (isClientBillingRoleExist > 0)
                    //    throw new AssociatePortalException("Client cannot be inactive");
                }
                using (var trans = hrmsEntities.Database.BeginTransaction())
                {
                    isUpdated = hrmsEntities.Database.SqlQuery<int>
                           ("[usp_UpdateClient] @ClientId,@ClientCode,@ClientName, @ClientRegisterName,@IsActive,@ModifiedUser,@ModifiedDate,@SystemInfo",
                               new object[] {
                                        new SqlParameter ("ClientId", clientDetails.ClientId),
                                        new SqlParameter ("ClientCode", clientDetails.ClientCode.Trim()),
                                        new SqlParameter ("ClientName", clientDetails.ClientName.Trim()),
                                        new SqlParameter ("ClientRegisterName",  (object)clientDetails.ClientRegisterName ?? DBNull.Value),
                                        new SqlParameter ("IsActive", clientDetails.IsActive),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                               }
                               ).SingleOrDefault();
                    trans.Commit();
                }
           

            }
            return isUpdated;
        }
        #endregion

        #region GetClientsDetails
        /// <summary>
        /// GetClientsDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetClientsDetails(bool isActive)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var clientsDetails = apEntities.Database.SqlQuery<ClientDetails>
                                  ("[usp_GetClients]").ToList();

                    return clientsDetails.OrderBy(cs => cs.ClientName);
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get client details.");
            }

        }
        #endregion

        #region GetClientsById
        /// <summary>
        /// Gets client details by id
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetClientsById(int clientId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    List<ClientDetails> clientDetailsById;

                    clientDetailsById = hrmsEntities.Database.SqlQuery<ClientDetails>
                          ("[usp_GetClientDetailsById] @ClientId",
                              new object[] {
                                        new SqlParameter ("ClientId", clientId) }).ToList();

                    return clientDetailsById;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get client details.");
            }
        }
        #endregion

    }
}
