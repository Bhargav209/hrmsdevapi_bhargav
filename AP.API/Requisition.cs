﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static AP.Utility.Enumeration;
using System.Text.RegularExpressions;

namespace AP.API
{
    public class Requisition
    {
        #region GetAllRequisitions
        /// <summary>
        /// this method is used to get existing all requisitions
        /// </summary>
        /// <returns></returns>

        //public IEnumerable<TalentRequisitionHistoryData> GetAllRequisitions()
        //{
        //    try
        //    {
        //        using (APEntities hrmEntities = new APEntities())
        //        {
        //            var employee = (from emp in hrmEntities.Employees
        //                            where emp.User.EmailAddress == HttpContext.Current.User.Identity.Name
        //                            select new { EmpId = emp.EmployeeId }).FirstOrDefault();

        //            var empId = employee?.EmpId ?? 0;
        //            if (HttpContext.Current.User.IsInRole("HRM"))
        //            {
        //                var statusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.Draft.ToString());
        //                return (from tr in hrmEntities.TalentRequisitions
        //                        where tr.IsActive == true && tr.StatusId != statusId
        //                        select new TalentRequisitionHistoryData()
        //                        {
        //                            TRId = tr.TRId,
        //                            TRCode = tr.TRCode,
        //                            DepartmentName = tr.Department.Description,
        //                            ProjectType = tr.ProjectType.Description,
        //                            ProjectName = tr.Project.ProjectName,
        //                            //Status = tr.Status.StatusDescription,
        //                            TRSelect = false,
        //                            Remarks = tr.Remarks,
        //                            NoOfPositions = tr.RequisitionRoleDetails.Sum(t => t.NoOfBillablePositions) + tr.RequisitionRoleDetails.Sum(t => t.NoOfNonBillablePositions),
        //                            TalentRequisitionHistoryRoleDetails = tr.RequisitionRoleDetails.Select(t => new TalentRequisitionHistoryRoleDetails()
        //                            {
        //                                RequisitionRoleDetailID = t.RequisitionRoleDetailID,
        //                                RoleName = t.Role.RoleName,
        //                                NoOfBillablePositions = t.NoOfBillablePositions,
        //                                NoOfNonBillablePositions = t.NoOfNonBillablePositions,
        //                                TAPositions = t.TAPositions,
        //                            })
        //                        }).ToList().Union(from tr in hrmEntities.TalentRequisitions
        //                                          where tr.IsActive == true && tr.CreatedUser == HttpContext.Current.User.Identity.Name && tr.StatusId == statusId
        //                                          select new TalentRequisitionHistoryData()
        //                                          {
        //                                              TRId = tr.TRId,
        //                                              TRCode = tr.TRCode,
        //                                              DepartmentName = tr.Department.Description,
        //                                              ProjectType = tr.ProjectType.Description,
        //                                              ProjectName = tr.Project.ProjectName,
        //                                              //Status = tr.Status.StatusDescription,
        //                                              TRSelect = false,
        //                                              Remarks = tr.Remarks,
        //                                              NoOfPositions = tr.RequisitionRoleDetails.Sum(t => t.NoOfBillablePositions) + tr.RequisitionRoleDetails.Sum(t => t.NoOfNonBillablePositions),
        //                                              TalentRequisitionHistoryRoleDetails = tr.RequisitionRoleDetails.Select(t => new TalentRequisitionHistoryRoleDetails()
        //                                              {
        //                                                  RequisitionRoleDetailID = t.RequisitionRoleDetailID,
        //                                                  RoleName = t.Role.RoleName,
        //                                                  NoOfBillablePositions = t.NoOfBillablePositions,
        //                                                  NoOfNonBillablePositions = t.NoOfNonBillablePositions,
        //                                                  TAPositions = t.TAPositions,
        //                                              })
        //                                          }).Distinct().OrderByDescending(i => i.TRId).ToList();
        //            }

        //            //var approvalDueStatusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.ApprovalDue.ToString());
        //            var requisitions = (from tr in hrmEntities.TalentRequisitions
        //                                where tr.IsActive == true && tr.CreatedUser == HttpContext.Current.User.Identity.Name
        //                                select new TalentRequisitionHistoryData()
        //                                {
        //                                    TRId = tr.TRId,
        //                                    TRCode = tr.TRCode,
        //                                    DepartmentName = tr.Department.Description,
        //                                    ProjectType = tr.ProjectType.Description,
        //                                    ProjectName = tr.Project.ProjectName,
        //                                    //Status = tr.Status.StatusDescription,
        //                                    TRSelect = false,
        //                                    Remarks = tr.Remarks,
        //                                    NoOfPositions = tr.RequisitionRoleDetails.Sum(t => t.NoOfBillablePositions) + tr.RequisitionRoleDetails.Sum(t => t.NoOfNonBillablePositions),
        //                                    TalentRequisitionHistoryRoleDetails = tr.RequisitionRoleDetails.Select(t => new TalentRequisitionHistoryRoleDetails()
        //                                    {
        //                                        RequisitionRoleDetailID = t.RequisitionRoleDetailID,
        //                                        RoleName = t.Role.RoleName,
        //                                        NoOfBillablePositions = t.NoOfBillablePositions,
        //                                        NoOfNonBillablePositions = t.NoOfNonBillablePositions,
        //                                        TAPositions = t.TAPositions,
        //                                    })
        //                                }).ToList().Union(from tr in hrmEntities.TalentRequisitions
        //                                                  where tr.IsActive == true && tr.ApprovedBy == empId && tr.StatusId == approvalDueStatusId
        //                                                  select new TalentRequisitionHistoryData()
        //                                                  {
        //                                                      TRId = tr.TRId,
        //                                                      TRCode = tr.TRCode,
        //                                                      DepartmentName = tr.Department.Description,
        //                                                      ProjectType = tr.ProjectType.Description,
        //                                                      ProjectName = tr.Project.ProjectName,
        //                                                      //Status = tr.Status.StatusDescription,
        //                                                      TRSelect = false,
        //                                                      Remarks = tr.Remarks,
        //                                                      NoOfPositions = tr.RequisitionRoleDetails.Sum(t => t.NoOfBillablePositions) + tr.RequisitionRoleDetails.Sum(t => t.NoOfNonBillablePositions),
        //                                                      TalentRequisitionHistoryRoleDetails = tr.RequisitionRoleDetails.Select(t => new TalentRequisitionHistoryRoleDetails()
        //                                                      {
        //                                                          RequisitionRoleDetailID = t.RequisitionRoleDetailID,
        //                                                          RoleName = t.Role.RoleName,
        //                                                          NoOfBillablePositions = t.NoOfBillablePositions,
        //                                                          NoOfNonBillablePositions = t.NoOfNonBillablePositions,
        //                                                          TAPositions = t.TAPositions,
        //                                                      })
        //                                                  }).OrderByDescending(i => i.TRId).ToList();

        //            return requisitions;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        #endregion

        #region GetRequisitionById
        /// <summary>
        /// this method is used to get existing requisition by id
        /// </summary>
        /// <returns></returns>

        public object GetRequisitionById(int id)
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var requisition = (from tr in hrmEntities.TalentRequisitions
                                       where tr.IsActive == true && tr.TRId == id //&& tr.CreatedUser == HttpContext.Current.User.Identity.Name
                                       select new
                                       {
                                           TRId = tr.TRId,
                                           DepartmentId = tr.Department.DepartmentId,
                                           ProjectTypeId = tr.ProjectType.ProjectTypeId,
                                           ProjectId = tr.Project.ProjectId,
                                           TRCode = tr.TRCode,
                                           RequestedDate = tr.RequestedDate,
                                           RequiredDate = tr.RequiredDate,
                                           TargetFulfillmentDate = tr.TargetFulfillmentDate,
                                           CurrentUser = tr.CreatedUser,
                                           Remarks = tr.Remarks,
                                           RequisitionRoleDetails = tr.RequisitionRoleDetails.Select(t => new
                                           {
                                               RequisitionRoleDetailID = t.RequisitionRoleDetailID,
                                               RoleId = t.RoleMasterId,
                                               NoOfBillablePositions = t.NoOfBillablePositions,
                                               NoOfNonBillablePositions = t.NoOfNonBillablePositions,
                                               MinimumExperience = t.MinimumExperience,
                                               MaximumExperience = t.MaximumExperience,
                                               EssentialEducationQualification = t.EssentialEducationQualification,
                                               DesiredEducationQualification = t.DesiredEducationQualification,
                                               ProjectSpecificResponsibilities = t.ProjectSpecificResponsibilities,
                                               RoleDescription = t.RoleDescription,
                                               KeyResponsibilities = t.KeyResponsibilities,
                                               TAPositions = t.TAPositions,
                                               Expertise = t.Expertise,
                                               DeltedRRSkillIds = string.Empty,
                                               //RequisitionRoleSkills = t.RequisitionRoleSkills.Select(s => new
                                               //{
                                               //    RRSkillId = s.RRSkillId,
                                               //    CompetencyAreaId = s.CompetencyAreaId,
                                               //    CompetencyAreaCode = s.CompetencyArea.CompetencyAreaCode,
                                               //    SkillId = s.SkillId,
                                               //    SkillCode = s.Skill.SkillCode,
                                               //    IsPrimary = s.IsPrimary.HasValue ? s.IsPrimary.Value : false,
                                               //    IsDefaultSkill = s.IsDefaultSkill.HasValue ? s.IsDefaultSkill.Value : false,
                                               //    ProficiencyLevelId = s.ProficiencyLevelId,
                                               //    // ProficiencyLevelCode = s.ProficiencyLevel.ProficiencyLevelCode,
                                               //    SkillsList = string.Empty,
                                               //    OtherSkillCode = string.Empty
                                               //})
                                           })

                                       }).FirstOrDefault();


                    return requisition;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
        }
        #endregion

        #region GetRequisitionById
        /// <summary>
        /// this method is used to get existing requisition by id
        /// </summary>
        /// <returns></returns>

        public object GetRequisitionViewDetailsById(int id)
        {
            string parms = "[]";

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var role = hrmEntities.Database.SqlQuery<RoleData>
                              ("[usp_GetKRAHistory]").FirstOrDefault();

                    var requisition = (from tr in hrmEntities.TalentRequisitions
                                       where tr.IsActive == true && tr.TRId == id //&& tr.CreatedUser == HttpContext.Current.User.Identity.Name
                                       select new
                                       {
                                           TRId = tr.TRId,
                                           TRCode = tr.TRCode,
                                           DepartmentName = tr.Department.Description,
                                           ProjectTypeName = tr.ProjectType.Description,
                                           ProjectName = tr.Project.ProjectName,
                                           ProjectCode = tr.Project.ProjectCode,
                                           RequestedDate = tr.RequestedDate,
                                           RequiredDate = tr.RequiredDate,
                                           TargetFulfillmentDate = tr.TargetFulfillmentDate,
                                           CurrentUser = tr.CreatedUser,
                                           Remarks = tr.Remarks,
                                           RequisitionRoleDetails = tr.RequisitionRoleDetails.Select(t => new
                                           {
                                               RequisitionRoleDetailID = t.RequisitionRoleDetailID,
                                               RoleName = role.RoleName,
                                               NoOfBillablePositions = t.NoOfBillablePositions,
                                               NoOfNonBillablePositions = t.NoOfNonBillablePositions,
                                               MinimumExperience = t.MinimumExperience,
                                               MaximumExperience = t.MaximumExperience,
                                               EssentialEducationQualification = t.EssentialEducationQualification,
                                               DesiredEducationQualification = t.DesiredEducationQualification,
                                               ProjectSpecificResponsibilities = t.ProjectSpecificResponsibilities,
                                               RoleDescription = role.RoleDescription,
                                               KeyResponsibilities = role.KeyResponsibilities,
                                               TAPositions = t.TAPositions,
                                               Expertise = t.Expertise,
                                               //RequisitionRoleSkills = t.RequisitionRoleSkills.Select(s => new
                                               //{
                                               //    RRSkillId = s.RRSkillId,
                                               //    CompetencyAreaCode = s.CompetencyArea.CompetencyAreaCode,
                                               //    SkillCode = s.Skill.SkillCode,
                                               //    IsPrimary = s.IsPrimary.HasValue ? s.IsPrimary.Value : false,
                                               //    //ProficiencyLevelDescription = s.ProficiencyLevel.ProficiencyLevelDescription
                                               //})
                                           })

                                       }).FirstOrDefault();


                    return requisition;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
        }
        #endregion

        #region Create Requisition 
        /// <summary>
        /// this method is used to create requisitions
        /// </summary>
        /// <param name="talentRequistionData"></param>
        /// <returns></returns>
        public bool CreateRequisition(TalentRequistionData talentRequistionData)
        {
            bool returnValue = false;
            try
            {
                int reportingManagerId = GetReportingManager(talentRequistionData.CurrentUser);

                if (reportingManagerId <= 0)
                    throw new AssociatePortalException("You are not allowed to create Requisition due to Reporting Manager is not assigned.");

                using (APEntities hrmsEntities = new APEntities())
                {

                    TalentRequisition talentRequisition = new TalentRequisition()
                    {
                        DepartmentId = talentRequistionData.DepartmentId,
                        PracticeAreaId = talentRequistionData.PracticeAreaId.Value,
                        ProjectId = talentRequistionData.ProjectId.Value,
                        TRCode = talentRequistionData.TRCode,
                        StatusId = Convert.ToInt32(Enumeration.TalentRequisitionStatusCodes.Draft),
                        IsActive = true,
                        CreatedUser = talentRequistionData.CurrentUser,
                        SystemInfo = talentRequistionData.SystemInfo,
                        CreatedDate = DateTime.Now,
                        ApprovedBy = reportingManagerId
                    };

                    string[] dateFormate = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy" };
                    if (!string.IsNullOrWhiteSpace(talentRequistionData.RequestedDate))
                    {
                        if (talentRequistionData.RequestedDate.Contains("T"))
                            talentRequistionData.RequestedDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequestedDate));
                        talentRequisition.RequestedDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequestedDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None));
                    }

                    if (!string.IsNullOrWhiteSpace(talentRequistionData.RequiredDate))
                    {
                        if (talentRequistionData.RequiredDate.Contains("T"))
                            talentRequistionData.RequiredDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequiredDate));
                        talentRequisition.RequiredDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequiredDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None));
                    }

                    if (!string.IsNullOrWhiteSpace(talentRequistionData.TargetFulfillmentDate))
                    {
                        if (talentRequistionData.TargetFulfillmentDate.Contains("T"))
                            talentRequistionData.TargetFulfillmentDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.TargetFulfillmentDate));
                        talentRequisition.TargetFulfillmentDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.TargetFulfillmentDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None));
                    }

                    hrmsEntities.TalentRequisitions.Add(talentRequisition);
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    if (talentRequistionData.RequisitionRoleDetails != null && talentRequistionData.RequisitionRoleDetails.Count() > 0)
                    {
                        foreach (var roleDetails in talentRequistionData.RequisitionRoleDetails)
                        {
                            returnValue = AddRequistiionRoleDetail(roleDetails, talentRequisition.TRId, talentRequistionData.CurrentUser, talentRequistionData.SystemInfo);
                        }
                    }
                }

            }
            catch
            {
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// Creates a TR and approves it automatically.
        /// </summary>
        /// <param name="talentRequistionData">TalentRequistionData</param>
        /// <returns>bool</returns>
        private bool CreateAndApproveRequisition(TalentRequistionData talentRequistionData)
        {
            bool returnValue = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    int empId = hrmsEntities.Employees.Where(e => e.User.EmailAddress == talentRequistionData.CurrentUser)
                                                     .Select(e => e.EmployeeId).FirstOrDefault();

                    TalentRequisition talentRequisition = new TalentRequisition()
                    {
                        DepartmentId = talentRequistionData.DepartmentId,
                        PracticeAreaId = talentRequistionData.PracticeAreaId.Value,
                        ProjectId = talentRequistionData.ProjectId.Value,
                        TRCode = talentRequistionData.TRCode,
                        StatusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.Approved.ToString()),
                        IsActive = true,
                        CreatedUser = talentRequistionData.CurrentUser,
                        SystemInfo = talentRequistionData.SystemInfo,
                        CreatedDate = DateTime.Now,
                        ApprovedBy = empId
                    };

                    string[] dateFormates = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy" };
                    if (!string.IsNullOrWhiteSpace(talentRequistionData.RequestedDate))
                    {
                        if (talentRequistionData.RequestedDate.Contains("T"))
                            talentRequistionData.RequestedDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequestedDate));
                        talentRequisition.RequestedDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequestedDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
                    }

                    if (!string.IsNullOrWhiteSpace(talentRequistionData.RequiredDate))
                    {
                        if (talentRequistionData.RequiredDate.Contains("T"))
                            talentRequistionData.RequiredDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequiredDate));
                        talentRequisition.RequiredDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequiredDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
                    }

                    if (!string.IsNullOrWhiteSpace(talentRequistionData.TargetFulfillmentDate))
                    {
                        if (talentRequistionData.TargetFulfillmentDate.Contains("T"))
                            talentRequistionData.TargetFulfillmentDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.TargetFulfillmentDate));
                        talentRequisition.TargetFulfillmentDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.TargetFulfillmentDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
                    }

                    hrmsEntities.TalentRequisitions.Add(talentRequisition);
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    if (talentRequistionData.RequisitionRoleDetails != null && talentRequistionData.RequisitionRoleDetails.Count() > 0)
                    {
                        foreach (var roleDetails in talentRequistionData.RequisitionRoleDetails)
                        {
                            returnValue = AddRequistiionRoleDetail(roleDetails, talentRequisition.TRId, talentRequistionData.CurrentUser, talentRequistionData.SystemInfo);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Creates a TR and approves it automatically.
        /// </summary>
        /// <param name="talentRequistionData">TalentRequistionData</param>
        /// <returns>bool</returns>
        //private bool CreateRequisitionNG2(TalentRequistionData talentRequistionData)
        //{
        //    bool returnValue = false;
        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            int approvalDueWith = GetReportingManager(talentRequistionData.CurrentUser);

        //            if (approvalDueWith <= 0)
        //                throw new AssociatePortalException("You are not allowed to create Requisition due to Reporting Manager is not assigned.");
        //            int _statusId = 0;
        //            if (HttpContext.Current.User.IsInRole(Enumeration.ApplicationRoles.Lead.ToString()) || HttpContext.Current.User.IsInRole(Enumeration.ApplicationRoles.Manager.ToString()))
        //                _statusId = GetRequistionStatsId(Enumeration.ApprovalDueWithPM);
        //            else if (HttpContext.Current.User.IsInRole(Enumeration.ApplicationRoles.HRA.ToString()))
        //                _statusId = GetRequistionStatsId(Enumeration.ApprovalDueWithHRM);

        //            TalentRequisition talentRequisition = new TalentRequisition()
        //            {
        //                DepartmentId = talentRequistionData.DepartmentId,
        //                ProjectTypeId = talentRequistionData.ProjectTypeId.Value,
        //                ProjectId = talentRequistionData.ProjectId.Value,
        //                TRCode = talentRequistionData.TRCode,
        //                StatusId = _statusId != 0 ? _statusId : GetRequistionStatsId(talentRequistionData.Status),
        //                RequisitionType = talentRequistionData.RequisitionType,
        //                IsActive = true,
        //                CreatedUser = talentRequistionData.CurrentUser,
        //                SystemInfo = talentRequistionData.SystemInfo,
        //                CreatedDate = DateTime.Now
        //            };                    

        //            string[] dateFormates = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy" };
        //            if (!string.IsNullOrWhiteSpace(talentRequistionData.RequestedDate))
        //            {
        //                if (talentRequistionData.RequestedDate.Contains("T"))
        //                    talentRequistionData.RequestedDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequestedDate));
        //                talentRequisition.RequestedDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequestedDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //            }

        //            if (!string.IsNullOrWhiteSpace(talentRequistionData.RequiredDate))
        //            {
        //                if (talentRequistionData.RequiredDate.Contains("T"))
        //                    talentRequistionData.RequiredDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequiredDate));
        //                talentRequisition.RequiredDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequiredDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //            }

        //            if (!string.IsNullOrWhiteSpace(talentRequistionData.TargetFulfillmentDate))
        //            {
        //                if (talentRequistionData.TargetFulfillmentDate.Contains("T"))
        //                    talentRequistionData.TargetFulfillmentDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.TargetFulfillmentDate));
        //                talentRequisition.TargetFulfillmentDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.TargetFulfillmentDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //            }
        //            hrmsEntities.TalentRequisitions.Add(talentRequisition);                    
        //            returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //            hrmsEntities.Entry(talentRequisition).GetDatabaseValues();
        //            int _returnValue = talentRequisition.TRId;

        //            UserDetails userDetails = new UserRoles().GetEmployeeOnUserName(talentRequistionData.CurrentUser);
        //            TalentRequisitionWorkFlow trStatus = new TalentRequisitionWorkFlow()
        //            {
        //                TalentRequisitionID = _returnValue,
        //                StatusID = _statusId != 0 ? _statusId : GetRequistionStatsId(talentRequistionData.Status),
        //                FromEmployeeID = userDetails != null ? userDetails.empID : 0,
        //                //ToEmployeeID = approvalDueWith
        //            };
        //            hrmsEntities.TalentRequisitionWorkFlows.Add(trStatus);
        //            returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //            if (talentRequistionData.RequisitionRoleDetails != null && talentRequistionData.RequisitionRoleDetails.Count() > 0)
        //            {
        //                foreach (var roleDetails in talentRequistionData.RequisitionRoleDetails)
        //                {
        //                    returnValue = AddRequistiionRoleDetail(roleDetails, talentRequisition.TRId, talentRequistionData.CurrentUser, talentRequistionData.SystemInfo);
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return returnValue;
        //}

        #endregion

        //#region Add Or Update Requisition 
        ///// <summary>
        ///// this method is used to update requisitions
        ///// </summary>
        ///// <param name="talentRequistionData"></param>
        ///// <returns></returns>
        //public bool AddOrUpdateRequisition(TalentRequistionData talentRequistionData)
        //{
        //    bool returnValue = false;
        //    bool readyToApprove = (HttpContext.Current.User.IsInRole("HRM") || HttpContext.Current.User.IsInRole("HRA"))
        //                               && talentRequistionData.Status.Equals(ConstantResources.ResourceManager.GetString("Requisitionapprovaldue"), StringComparison.OrdinalIgnoreCase);

        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            var ProjectName = (from p in hrmsEntities.Projects
        //                               where p.ProjectId == talentRequistionData.ProjectId
        //                               select new
        //                               {
        //                                   Name = p.ProjectName

        //                               }).FirstOrDefault();


        //            TalentRequisition talentRequisition = hrmsEntities.TalentRequisitions.FirstOrDefault(i => i.TRId == talentRequistionData.TRId);
        //            if (talentRequisition == null)
        //            {
        //                if (readyToApprove)
        //                    returnValue = CreateAndApproveRequisition(talentRequistionData);
        //                else
        //                    returnValue = CreateRequisition(talentRequistionData);
        //            }
        //            else
        //            {
        //                talentRequisition.DepartmentId = talentRequistionData.DepartmentId;
        //                talentRequisition.ProjectTypeId = talentRequistionData.ProjectTypeId.Value;
        //                talentRequisition.ProjectId = talentRequistionData.ProjectId.Value;
        //                talentRequisition.TRCode = talentRequistionData.TRCode;

        //                if (readyToApprove)
        //                {
        //                    int empId = hrmsEntities.Employees.Where(e => e.User.EmailAddress == talentRequistionData.CurrentUser)
        //                                             .Select(e => e.EmployeeId).FirstOrDefault();
        //                    //
        //                    talentRequisition.StatusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.Approved.ToString());
        //                    talentRequisition.ApprovedBy = empId;
        //                }
        //                else
        //                {
        //                    talentRequisition.StatusId = GetRequistionStatsId(talentRequistionData.Status);
        //                }
        //                string[] dateFormates = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy", "dd-MM-yyyy" };
        //                if (!string.IsNullOrWhiteSpace(talentRequistionData.RequestedDate))
        //                {
        //                    if (talentRequistionData.RequestedDate.Contains("T"))
        //                        talentRequistionData.RequestedDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequestedDate));
        //                    talentRequisition.RequestedDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequestedDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //                }

        //                if (!string.IsNullOrWhiteSpace(talentRequistionData.RequiredDate))
        //                {
        //                    if (talentRequistionData.RequiredDate.Contains("T"))
        //                        talentRequistionData.RequiredDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequiredDate));
        //                    talentRequisition.RequiredDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequiredDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //                }

        //                if (!string.IsNullOrWhiteSpace(talentRequistionData.TargetFulfillmentDate))
        //                {
        //                    if (talentRequistionData.TargetFulfillmentDate.Contains("T"))
        //                        talentRequistionData.TargetFulfillmentDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.TargetFulfillmentDate));
        //                    talentRequisition.TargetFulfillmentDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.TargetFulfillmentDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //                }
        //                else
        //                    talentRequisition.TargetFulfillmentDate = null;
        //                talentRequisition.ModifiedDate = DateTime.Now;
        //                talentRequisition.ModifiedUser = talentRequistionData.CurrentUser;
        //                talentRequisition.SystemInfo = talentRequistionData.SystemInfo;
        //                hrmsEntities.Entry(talentRequisition).State = EntityState.Modified;
        //                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

        //                foreach (var roleDetails in talentRequistionData.RequisitionRoleDetails)
        //                {
        //                    #region Add/Update RequistiionRoleDetail 

        //                    var existRequistiionRoleDetail = hrmsEntities.RequisitionRoleDetails.FirstOrDefault(r => r.TRId == talentRequisition.TRId && r.RequisitionRoleDetailID == roleDetails.RequisitionRoleDetailID);
        //                    if (existRequistiionRoleDetail == null)
        //                    {

        //                        returnValue = AddRequistiionRoleDetail(roleDetails, talentRequisition.TRId, talentRequistionData.CurrentUser, talentRequistionData.SystemInfo);
        //                    }
        //                    else
        //                    {
        //                        existRequistiionRoleDetail.TRId = talentRequisition.TRId;
        //                        existRequistiionRoleDetail.RoleId = roleDetails.RoleId;
        //                        existRequistiionRoleDetail.NoOfBillablePositions = roleDetails.NoOfBillablePositions ?? 0;
        //                        existRequistiionRoleDetail.NoOfNonBillablePositions = roleDetails.NoOfNonBillablePositions ?? 0;
        //                        existRequistiionRoleDetail.MinimumExperience = roleDetails.MinimumExperience;
        //                        existRequistiionRoleDetail.MaximumExperience = roleDetails.MaximumExperience;
        //                        existRequistiionRoleDetail.EssentialEducationQualification = roleDetails.EssentialEducationQualification;
        //                        existRequistiionRoleDetail.DesiredEducationQualification = roleDetails.DesiredEducationQualification;
        //                        existRequistiionRoleDetail.KeyResponsibilities = roleDetails.KeyResponsibilities;
        //                        existRequistiionRoleDetail.RoleDescription = roleDetails.RoleDescription;
        //                        existRequistiionRoleDetail.Expertise = roleDetails.Expertise;
        //                        existRequistiionRoleDetail.ProjectSpecificResponsibilities = roleDetails.ProjectSpecificResponsibilities;
        //                        existRequistiionRoleDetail.ModifiedDate = DateTime.Now;
        //                        existRequistiionRoleDetail.ModifiedUser = talentRequistionData.CurrentUser;
        //                        existRequistiionRoleDetail.SystemInfo = talentRequistionData.SystemInfo;
        //                        hrmsEntities.Entry(existRequistiionRoleDetail).State = EntityState.Modified;
        //                        returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

        //                        foreach (var requisitionRoleSkills in roleDetails.RequisitionRoleSkills)
        //                        {
        //                            #region Add/Update RequisitionRoleSkills 

        //                            //var existRequisitionRoleSkill = hrmsEntities.RequisitionRoleSkillMappings.FirstOrDefault(r => r.TRID == talentRequisition.TRId && r.RequistiionRoleDetailID == existRequistiionRoleDetail.RequisitionRoleDetailID && r.RRSkillId == requisitionRoleSkills.RRSkillId);
        //                            //if (existRequisitionRoleSkill == null)
        //                            //{
        //                            //    returnValue = AddRequisitionRoleSkill(requisitionRoleSkills, talentRequisition.TRId, existRequistiionRoleDetail.RequisitionRoleDetailID, talentRequistionData.CurrentUser, roleDetails.RoleId, talentRequistionData.SystemInfo);
        //                            //}
        //                            //else
        //                            //{
        //                            //    existRequisitionRoleSkill.TRId = talentRequisition.TRId;
        //                            //    existRequisitionRoleSkill.RequistiionRoleDetailID = existRequistiionRoleDetail.RequisitionRoleDetailID;
        //                            //    existRequisitionRoleSkill.CompetencyAreaId = requisitionRoleSkills.CompetencyAreaId;
        //                            //    existRequisitionRoleSkill.SkillId = requisitionRoleSkills.SkillId;
        //                            //    existRequisitionRoleSkill.IsPrimary = requisitionRoleSkills.IsPrimary;
        //                            //    existRequisitionRoleSkill.ProficiencyLevelId = requisitionRoleSkills.ProficiencyLevelId;
        //                            //    existRequisitionRoleSkill.ModifiedUser = talentRequistionData.CurrentUser;
        //                            //    existRequisitionRoleSkill.ModifiedDate = DateTime.Now;
        //                            //    existRequisitionRoleSkill.SystemInfo = talentRequistionData.SystemInfo;
        //                            //    hrmsEntities.Entry(existRequistiionRoleDetail).State = EntityState.Modified;
        //                            //    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //                            //}
        //                            #endregion
        //                        }
        //                        #region Delete RequisitionRoleSkills

        //                        if (roleDetails.DeltedRRSkillIds != null && roleDetails.DeltedRRSkillIds.Count > 0)
        //                            returnValue = DeleteRequisitionRoleSkills(roleDetails.DeltedRRSkillIds);
        //                        #endregion
        //                    }
        //                    #endregion
        //                }

        //                #region Delete RequisitionRoleDetails

        //                if (talentRequistionData.DeltedRRdetailsIds != null && talentRequistionData.DeltedRRdetailsIds.Count > 0)
        //                    returnValue = DeleteRequisitionRoleDetails(talentRequistionData.DeltedRRdetailsIds);
        //                #endregion
        //            }

        //            IEnumerable<string> roleNames = talentRequistionData.RequisitionRoleDetails.Select(e => e.RoleDescription).Distinct().ToList();
        //            int? reportingManagerId = GetReportingManager(talentRequistionData.CurrentUser);
        //            talentRequistionData.ApprovedBy = reportingManagerId;

        //            string talentRequisitionMenuTitle = ConstantResources.ResourceManager.GetString("TalentRequisitionMenuTitle");
        //            List<AssociateDetails> associates = AssociateProfessionalDetails.GetAssociatesByRole(hrmsEntities, talentRequisitionMenuTitle);
        //            var associateTRDetails = AssociateProfessionalDetails.GetAssociatesDetailsByRoleId(hrmsEntities, talentRequistionData.CurrentUser);
        //            string toMailName = "", toMailids = "", ccMailids = "";
        //            if (associates != null)
        //            {
        //                var head = associates.Where(e => e.RoleName == Enumeration.DeliveryHead).FirstOrDefault();
        //                var hrm = associates.Where(e => e.RoleName == Enumeration.ApplicationRoles.HRM.ToString()).FirstOrDefault();
        //                var finance = associates.Where(e => e.RoleName == Enumeration.ApplicationRoles.FinanceHead.ToString()).FirstOrDefault();
        //                var prgrmMngr = associates.Where(e => e.RoleName == Enumeration.ProgramManager).FirstOrDefault();

        //                if (!talentRequistionData.IsDoNotSendNotifications && associateTRDetails !=null && associateTRDetails.Count > 0)
        //                {
        //                    if (associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.Lead.ToString() || associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.Manager.ToString()
        //                        || associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.HRA.ToString())
        //                    {
        //                        toMailName = associateTRDetails[0].ReportingManagerName;
        //                        toMailids = associateTRDetails[0].ReportingManagerEmail;
        //                        ccMailids = associateTRDetails[0].DepartmentHeadEmail + ";" + hrm?.WorkEmail + ";" + talentRequistionData.CurrentUser;
        //                    }
        //                    else if (associateTRDetails[0].RoleName == Enumeration.ProgramManager)
        //                    {
        //                        toMailName = associateTRDetails[0].DepartmentHeadName;
        //                        toMailids = associateTRDetails[0].DepartmentHeadEmail;
        //                        ccMailids = hrm?.WorkEmail + ";" + talentRequistionData.CurrentUser;
        //                    }
        //                }
        //            }

        //            if (readyToApprove)
        //            {
        //                if (associates != null)
        //                {
        //                    if (!talentRequistionData.IsDoNotSendNotifications && associateTRDetails != null && associateTRDetails.Count > 0)
        //                    {
        //                        lkValue lkValue = hrmsEntities.lkValues.Where(l => l.ValueKey == talentRequistionData.RequisitionType).FirstOrDefault();
        //                        if (lkValue.ValueID == Enumeration.RequisitionType.New.ToString())
        //                        {
        //                            //SendTRApprovedNotifications(toMailName, toMailids, ccMailids, talentRequistionData.TRCode, ProjectName.Name, (roleNames != null ? string.Join(",", roleNames) : string.Empty));
        //                        }
        //                        else
        //                        {
        //                            //SendTRApprovedNotifications(toMailName, toMailids, ccMailids, talentRequistionData.TRCode, ProjectName.Name, (roleNames != null ? string.Join(",", roleNames) : string.Empty));
        //                        }
        //                    }
        //                }
        //            }
        //            else if (!talentRequistionData.IsDoNotSendNotifications && talentRequistionData.Status.Equals(ConstantResources.ResourceManager.GetString("Requisitionapprovaldue"), StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                //SendTalentRequisitionSubmittedNotifications(talentRequistionData.CurrentUser, toMailName, toMailids, ccMailids, talentRequistionData.TRCode, talentRequistionData.ApprovedBy, ProjectName.Name, (roleNames != null ? string.Join(",", roleNames) : string.Empty));
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to add/modify requisition details.");
        //    }
        //    return returnValue;
        //}

        //#endregion


        //#region Add Or Update Requisition from NG2
        ///// <summary>
        ///// this method is used to update requisitions
        ///// </summary>
        ///// <param name="talentRequistionData"></param>
        ///// <returns></returns>
        //public bool AddOrUpdateRequisitionNG2(TalentRequistionData talentRequistionData)
        //{
        //    bool returnValue = false;
        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            var ProjectName = (from p in hrmsEntities.Projects
        //                               where p.ProjectId == talentRequistionData.ProjectId
        //                               select new
        //                               {
        //                                   Name = p.ProjectName
        //                               }).FirstOrDefault();


        //            TalentRequisition talentRequisition = hrmsEntities.TalentRequisitions.FirstOrDefault(i => i.TRId == talentRequistionData.TRId);

        //            if (talentRequisition == null)
        //            {
        //                TalentRequisition talentRequisitionTRCodeDetails = hrmsEntities.TalentRequisitions.FirstOrDefault(i => i.TRCode == talentRequistionData.TRCode);
        //                if (talentRequisitionTRCodeDetails != null)
        //                    throw new AssociatePortalException("The given TR Code is already exists..");
        //                else
        //                    returnValue = CreateRequisitionNG2(talentRequistionData);
        //            }
        //            else
        //            {
        //                talentRequisition.DepartmentId = talentRequistionData.DepartmentId;
        //                talentRequisition.ProjectTypeId = talentRequistionData.ProjectTypeId.Value;
        //                talentRequisition.ProjectId = talentRequistionData.ProjectId.Value;
        //                talentRequisition.TRCode = talentRequistionData.TRCode;
        //                talentRequisition.RequisitionType = talentRequistionData.RequisitionType;
        //                int _statusId = 0;
        //                if (HttpContext.Current.User.IsInRole(Enumeration.ApplicationRoles.Lead.ToString()) || HttpContext.Current.User.IsInRole(Enumeration.ApplicationRoles.Manager.ToString()))
        //                    _statusId = GetRequistionStatsId(Enumeration.ApprovalDueWithPM);
        //                else if (HttpContext.Current.User.IsInRole(Enumeration.ApplicationRoles.HRA.ToString()))
        //                    _statusId = GetRequistionStatsId(Enumeration.ApprovalDueWithHRM);

        //                talentRequisition.StatusId = _statusId !=0 ? _statusId : GetRequistionStatsId(talentRequistionData.Status);

        //                UserDetails userDetails = new UserRoles().GetEmployeeOnUserName(talentRequistionData.CurrentUser);
        //                TalentRequisitionWorkFlow trStatus = new TalentRequisitionWorkFlow()
        //                {
        //                    TalentRequisitionID = talentRequistionData.TRId,
        //                    StatusID = _statusId != 0 ? _statusId : GetRequistionStatsId(talentRequistionData.Status),
        //                    FromEmployeeID = userDetails != null ? userDetails.empID : 0,
        //                    //ToEmployeeID = GetReportingManager(talentRequistionData.CurrentUser)
        //                };
        //                hrmsEntities.TalentRequisitionWorkFlows.Add(trStatus);

        //                string[] dateFormates = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy", "dd-MM-yyyy" };
        //                if (!string.IsNullOrWhiteSpace(talentRequistionData.RequestedDate))
        //                {
        //                    if (talentRequistionData.RequestedDate.Contains("T"))
        //                        talentRequistionData.RequestedDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequestedDate));
        //                    talentRequisition.RequestedDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequestedDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //                }

        //                if (!string.IsNullOrWhiteSpace(talentRequistionData.RequiredDate))
        //                {
        //                    if (talentRequistionData.RequiredDate.Contains("T"))
        //                        talentRequistionData.RequiredDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.RequiredDate));
        //                    talentRequisition.RequiredDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.RequiredDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //                }

        //                if (!string.IsNullOrWhiteSpace(talentRequistionData.TargetFulfillmentDate))
        //                {
        //                    if (talentRequistionData.TargetFulfillmentDate.Contains("T"))
        //                        talentRequistionData.TargetFulfillmentDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(talentRequistionData.TargetFulfillmentDate));
        //                    talentRequisition.TargetFulfillmentDate = Commons.GetDateTimeInIST(DateTime.ParseExact(talentRequistionData.TargetFulfillmentDate, dateFormates, CultureInfo.InvariantCulture, DateTimeStyles.None));
        //                }
        //                else
        //                    talentRequisition.TargetFulfillmentDate = null;
        //                talentRequisition.ModifiedDate = DateTime.Now;
        //                talentRequisition.ModifiedUser = talentRequistionData.CurrentUser;
        //                talentRequisition.SystemInfo = talentRequistionData.SystemInfo;
        //                hrmsEntities.Entry(talentRequisition).State = EntityState.Modified;
        //                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

        //                foreach (var roleDetails in talentRequistionData.RequisitionRoleDetails)
        //                {
        //                    #region Add/Update RequistiionRoleDetail 

        //                    var existRequistiionRoleDetail = hrmsEntities.RequisitionRoleDetails.FirstOrDefault(r => r.TRId == talentRequisition.TRId && r.RequisitionRoleDetailID == roleDetails.RequisitionRoleDetailID);
        //                    if (existRequistiionRoleDetail == null)
        //                    {

        //                        returnValue = AddRequistiionRoleDetail(roleDetails, talentRequisition.TRId, talentRequistionData.CurrentUser, talentRequistionData.SystemInfo);
        //                    }
        //                    else
        //                    {
        //                        existRequistiionRoleDetail.TRId = talentRequisition.TRId;
        //                        existRequistiionRoleDetail.RoleId = roleDetails.RoleId;
        //                        existRequistiionRoleDetail.NoOfBillablePositions = roleDetails.NoOfBillablePositions ?? 0;
        //                        existRequistiionRoleDetail.NoOfNonBillablePositions = roleDetails.NoOfNonBillablePositions ?? 0;
        //                        existRequistiionRoleDetail.MinimumExperience = roleDetails.MinimumExperience;
        //                        existRequistiionRoleDetail.MaximumExperience = roleDetails.MaximumExperience;
        //                        existRequistiionRoleDetail.EssentialEducationQualification = roleDetails.EssentialEducationQualification;
        //                        existRequistiionRoleDetail.DesiredEducationQualification = roleDetails.DesiredEducationQualification;
        //                        existRequistiionRoleDetail.KeyResponsibilities = roleDetails.KeyResponsibilities;
        //                        existRequistiionRoleDetail.RoleDescription = roleDetails.RoleDescription;
        //                        existRequistiionRoleDetail.Expertise = roleDetails.Expertise;
        //                        existRequistiionRoleDetail.ProjectSpecificResponsibilities = roleDetails.ProjectSpecificResponsibilities;
        //                        existRequistiionRoleDetail.ModifiedDate = DateTime.Now;
        //                        existRequistiionRoleDetail.ModifiedUser = talentRequistionData.CurrentUser;
        //                        existRequistiionRoleDetail.SystemInfo = talentRequistionData.SystemInfo;
        //                        hrmsEntities.Entry(existRequistiionRoleDetail).State = EntityState.Modified;
        //                        returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

        //                        foreach (var requisitionRoleSkills in roleDetails.RequisitionRoleSkills)
        //                        {
        //                            #region Add/Update RequisitionRoleSkills 

        //                            //var existRequisitionRoleSkill = hrmsEntities.RequisitionRoleSkills.FirstOrDefault(r => r.TRId == talentRequisition.TRId && r.RequistiionRoleDetailID == existRequistiionRoleDetail.RequisitionRoleDetailID && r.RRSkillId == requisitionRoleSkills.RRSkillId);
        //                            //if (existRequisitionRoleSkill == null)
        //                            //{
        //                            //    returnValue = AddRequisitionRoleSkill(requisitionRoleSkills, talentRequisition.TRId, existRequistiionRoleDetail.RequisitionRoleDetailID, talentRequistionData.CurrentUser, roleDetails.RoleId, talentRequistionData.SystemInfo);
        //                            //}
        //                            //else
        //                            //{
        //                            //    existRequisitionRoleSkill.TRId = talentRequisition.TRId;
        //                            //    existRequisitionRoleSkill.RequistiionRoleDetailID = existRequistiionRoleDetail.RequisitionRoleDetailID;
        //                            //    existRequisitionRoleSkill.CompetencyAreaId = requisitionRoleSkills.CompetencyAreaId;
        //                            //    existRequisitionRoleSkill.SkillId = requisitionRoleSkills.SkillId;
        //                            //    existRequisitionRoleSkill.IsPrimary = requisitionRoleSkills.IsPrimary;
        //                            //    existRequisitionRoleSkill.ProficiencyLevelId = requisitionRoleSkills.ProficiencyLevelId;
        //                            //    existRequisitionRoleSkill.ModifiedUser = talentRequistionData.CurrentUser;
        //                            //    existRequisitionRoleSkill.ModifiedDate = DateTime.Now;
        //                            //    existRequisitionRoleSkill.SystemInfo = talentRequistionData.SystemInfo;
        //                            //    hrmsEntities.Entry(existRequistiionRoleDetail).State = EntityState.Modified;
        //                            //    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //                            //}
        //                            #endregion
        //                        }
        //                        #region Delete RequisitionRoleSkills

        //                        if (roleDetails.DeltedRRSkillIds != null && roleDetails.DeltedRRSkillIds.Count > 0)
        //                            returnValue = DeleteRequisitionRoleSkills(roleDetails.DeltedRRSkillIds);
        //                        #endregion
        //                    }
        //                    #endregion
        //                }

        //                #region Delete RequisitionRoleDetails

        //                if (talentRequistionData.DeltedRRdetailsIds != null && talentRequistionData.DeltedRRdetailsIds.Count > 0)
        //                    returnValue = DeleteRequisitionRoleDetails(talentRequistionData.DeltedRRdetailsIds);
        //                #endregion
        //            }

        //            IEnumerable<string> roleNames = talentRequistionData.RequisitionRoleDetails.Select(e => e.RoleDescription).Distinct().ToList();
        //            int? reportingManagerId = GetReportingManager(talentRequistionData.CurrentUser);
        //            talentRequistionData.ApprovedBy = reportingManagerId;

        //            string talentRequisitionMenuTitle = ConstantResources.ResourceManager.GetString("TalentRequisitionMenuTitle");
        //            List<AssociateDetails> associates = AssociateProfessionalDetails.GetAssociatesByRole(hrmsEntities, talentRequisitionMenuTitle);
        //            var associateTRDetails = AssociateProfessionalDetails.GetAssociatesDetailsByRoleId(hrmsEntities, talentRequistionData.CurrentUser);
        //            string toMailName = "", toMailids = "", ccMailids = "";
        //            if (associates != null)
        //            {
        //                List<AssociateDetails> hrmDetailsList = associates.Where(e => e.RoleName == Enumeration.ApplicationRoles.HRM.ToString() && !string.IsNullOrEmpty(e.WorkEmail) != false).ToList();
        //                IEnumerable<string> hrmEmailList = hrmDetailsList.Select(e => e.WorkEmail).Distinct().ToList();

        //                if (!talentRequistionData.IsDoNotSendNotifications && associateTRDetails != null)
        //                {
        //                    if (associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.Lead.ToString() || associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.Manager.ToString()
        //                        || associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.HRA.ToString())
        //                    {
        //                        toMailName = associateTRDetails[0].ReportingManagerName;
        //                        toMailids = associateTRDetails[0].ReportingManagerEmail;
        //                        ccMailids = associateTRDetails[0].DepartmentHeadEmail + ";" + talentRequistionData.CurrentUser + ";" + (hrmEmailList != null ? string.Join(",", hrmEmailList) : string.Empty);
        //                    }
        //                    else if (associateTRDetails[0].RoleName == Enumeration.ProgramManager || associateTRDetails[0].RoleName == Enumeration.ApplicationRoles.HRM.ToString())
        //                    {
        //                        toMailName = associateTRDetails[0].DepartmentHeadName;
        //                        toMailids = associateTRDetails[0].DepartmentHeadEmail;
        //                        ccMailids = talentRequistionData.CurrentUser + ";" + (hrmEmailList != null ? string.Join(",", hrmEmailList) : string.Empty);
        //                    }
        //                }
        //            }
        //            if (!talentRequistionData.IsDoNotSendNotifications && talentRequistionData.Status.Equals(ConstantResources.ResourceManager.GetString("Requisitionapprovaldue"), StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                //SendTalentRequisitionSubmittedNotifications(talentRequistionData.CurrentUser, toMailName, toMailids, ccMailids, talentRequistionData.TRCode, talentRequistionData.ApprovedBy, ProjectName.Name, (roleNames != null ? string.Join(",", roleNames) : string.Empty));
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to add/modify requisition details.");
        //    }
        //    return returnValue;
        //}

        //#endregion

        //#region Approve Or Rejected Requisition 
        //public bool ApproveRequisition(ApproveRequisitionData approveRequisitionData)
        //{

        //    bool returnValue = false;
        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            var talentRequisitions = (from tr in hrmsEntities.TalentRequisitions
        //                                      join p in hrmsEntities.Projects on tr.ProjectId equals p.ProjectId
        //                                      where approveRequisitionData.TRIds.Contains(tr.TRId)
        //                                      select new
        //                                      {
        //                                          Name = p.ProjectName,
        //                                          TRId = tr.TRId,
        //                                          TRCode = tr.TRCode,
        //                                          RoleNames = tr.RequisitionRoleDetails.Select(r => r.RoleDescription),
        //                                          CreatedUser = tr.CreatedUser,
        //                                      }).ToList();

        //            IEnumerable<int> trIds = talentRequisitions.Select(e => e.TRId).Distinct().ToList();
        //            List<TalentRequisition> requisitions = hrmsEntities.TalentRequisitions.Where(e => trIds.Contains(e.TRId)).ToList();

        //            if (approveRequisitionData.Type.Equals("Approved", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                foreach (var talentRequisition in talentRequisitions)
        //                {
        //                    TalentRequisition tr = requisitions.Where(x => x.TRId == talentRequisition.TRId).FirstOrDefault();
        //                    tr.StatusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.Approved.ToString());
        //                    hrmsEntities.Entry(tr).State = EntityState.Modified;
        //                    //sending emails when requisition is approved
        //                    //SendTalentRequisitionApprovedNotifications(talentRequisition.CreatedUser, approveRequisitionData.CurrentUser, talentRequisition.TRCode, talentRequisition.Name, (talentRequisition.RoleNames != null ? string.Join(",", talentRequisition.RoleNames) : string.Empty));
        //                }
        //                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //            }

        //            else if (approveRequisitionData.Type.Equals("Rejected", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                foreach (var talentRequisition in talentRequisitions)
        //                {
        //                    TalentRequisition tr = requisitions.Where(x => x.TRId == talentRequisition.TRId).FirstOrDefault();
        //                    tr.StatusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.Rejected.ToString());
        //                    tr.Remarks = approveRequisitionData.Remarks;
        //                    hrmsEntities.Entry(tr).State = EntityState.Modified;
        //                    //sending emails when requisition is rejected
        //                    //SendTalentRequisitionRejectedNotifications(talentRequisition.CreatedUser, approveRequisitionData.CurrentUser, talentRequisition.TRCode, approveRequisitionData.Remarks, talentRequisition.Name, (talentRequisition.RoleNames != null ? string.Join(",", talentRequisition.RoleNames) : string.Empty));
        //                }
        //                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return returnValue;
        //}
        //#endregion

        #region Tag TA Positions 
        public bool TagTAPositions(int roledetailId, int noOfTAPostions)
        {
            bool returnValue = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    string toMailids = "", ccMailids = "";
                    var existRequistiionRoleDetail = hrmsEntities.RequisitionRoleDetails.FirstOrDefault(r => r.RequisitionRoleDetailID == roledetailId);

                    //AssociateDetails departmentHeadDetails = GetDepartmentHeadDetails(Enumeration.DepartmentCodes.HR.ToString());
                    var associateTRDetails = AssociateProfessionalDetails.GetAssociatesDetailsByRoleId(hrmsEntities, existRequistiionRoleDetail.CreatedUser);
                    string talentRequisitionMenuTitle = ConstantResources.ResourceManager.GetString("TalentRequisitionMenuTitle");
                    List<AssociateDetails> associates = AssociateProfessionalDetails.GetAssociatesByRole(hrmsEntities, talentRequisitionMenuTitle);
                    List<AssociateDetails> hrmDetailsList = associates.Where(e => e.RoleName == Enumeration.ApplicationRoles.HRM.ToString() && !string.IsNullOrEmpty(e.WorkEmail) != false).ToList();
                    IEnumerable<string> hrmEmailList = hrmDetailsList.Select(e => e.WorkEmail).Distinct().ToList();

                    toMailids = hrmEmailList != null ? string.Join(",", hrmEmailList) : string.Empty;
                   if(associateTRDetails != null && associateTRDetails.Count  > 0) 
                    ccMailids = associateTRDetails[0].ReportingManagerEmail + ";" + associateTRDetails[0].DepartmentHeadEmail;

                    var ProjectName = (from p in hrmsEntities.Projects
                                       where p.ProjectId == existRequistiionRoleDetail.TalentRequisition.ProjectId
                                       select new
                                       {
                                           Name = p.ProjectName
                                       }).FirstOrDefault();

                    if (existRequistiionRoleDetail != null)
                    {
                        existRequistiionRoleDetail.TAPositions = noOfTAPostions;
                        hrmsEntities.Entry(existRequistiionRoleDetail).State = EntityState.Modified;
                        //SendTagTAPositionsNotification(toMailids, ccMailids, existRequistiionRoleDetail.TRId, existRequistiionRoleDetail.TalentRequisition.TRCode, noOfTAPostions, ProjectName.Name, existRequistiionRoleDetail.Role.RoleName);
                        returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to Tag TA position.");
            }
            return returnValue;
        }
        #endregion

        //#region Get Requisition Details By Search
        ///// <summary>
        ///// Get Requisition Details By Search
        ///// </summary>
        ///// <returns></returns>
        //public IEnumerable<TalentRequisitionHistoryData> GetRequisitionDetailsBySearch(SearchFilter searchFilter)
        //{
        //    using (APEntities hrmsEntities = new APEntities())
        //    {
        //        var result = GetAllRequisitions();

        //        if (searchFilter.SearchData == null)
        //            searchFilter.SearchData = "";

        //        if (searchFilter != null && searchFilter.SearchType == "TRNumber")
        //            result = result.Where(i => i.TRId.ToString().IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

        //        if (searchFilter != null && searchFilter.SearchType == "Department")
        //            result = result.Where(i => i.DepartmentName.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

        //        if (searchFilter != null && searchFilter.SearchType == "ProjectType")
        //            result = result.Where(i => i.ProjectType.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

        //        if (searchFilter != null && searchFilter.SearchType == "Project")
        //            result = result.Where(i => i.ProjectName.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

        //        if (searchFilter != null && searchFilter.SearchType == "Status")
        //            result = result.Where(i => i.Status.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

        //        return result;
        //    }
        //}
        //#endregion

        #region Private Methods

        //private void SendTagTAPositionsNotification(string emailTo, string emailCC, int tRId, string tRCode, int taPositions, string projectName, string roleName)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString("TagTAPositions");
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //                       string htmlContent = notificationConfiguration.emailContent;
        //                       string htmlSubject = notificationConfiguration.emailSubject;

                    //var trCreatedUserDetails = (from e in hrmEntities.Employees
                    //                            join usr in hrmEntities.Users on e.UserId equals usr.UserId
                    //                            join tr in hrmEntities.TalentRequisitions on usr.EmailAddress equals tr.CreatedUser
                    //                            where tr.TRId == tRId
                    //                            select new
                    //                            {
                    //                                Name = e.FirstName + " " + e.LastName,
                    //                                EmailAddress = usr.EmailAddress
                    //                            }).FirstOrDefault();

                    //if (trCreatedUserDetails != null)
                    //{
                    //htmlContent = htmlContent.Replace("@TRCode", tRCode).Replace("@TAPositions", taPositions.ToString()).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
                    //    htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);

                    //    Email email = new Email();
                    //    int notificationConfigID = new BaseEmail().BuildEmailObject(email, emailTo, notificationConfiguration.emailFrom, emailCC, htmlSubject,
                    //                                   htmlContent, Enumeration.NotificationStatus.TagTAPositions.ToString());
                    //    new BaseEmail().SendEmail(email, notificationConfigID);
                    //}
        //        }
        //    }
        //}

        //private void SendTalentRequisitionSubmittedNotifications(string currentUser, string toMailName, string toMailids, string ccMailids, string tRCode, int? reportingManagerId, string projectName, string roleName)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString("TRSubmitForApproval");
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //            string htmlContent = notificationConfiguration.emailContent;
        //            string htmlSubject = notificationConfiguration.emailSubject;

        //            var reportingManagerDetails = (from e in hrmEntities.Employees
        //                                           join usr in hrmEntities.Users on e.UserId equals usr.UserId
        //                                           where e.EmployeeId == reportingManagerId
        //                                           select new
        //                                           {
        //                                               Name = e.FirstName + " " + e.LastName,
        //                                               EmailAddress = usr.EmailAddress
        //                                           }).FirstOrDefault();

        //            if (reportingManagerDetails != null)
        //            {
        //                string hrMangerEmailAddress = GetHRManagerEmailAddress();
        //                //if (!string.IsNullOrEmpty(hrMangerEmailAddress))
        //                //    emailCC += ";" + hrMangerEmailAddress;
        //                //emailCC = emailCC.Replace(";;", ";");
        //                htmlContent = htmlContent.Replace("@CreatedBy", toMailName).Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
        //                htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);

        //                Email email = new Email();
        //                int notificationConfigID = new BaseEmail().BuildEmailObject(email, toMailids, notificationConfiguration.emailFrom, ccMailids, htmlSubject, htmlContent, Enumeration.NotificationStatus.TRSubmitForApproval.ToString());
        //                new BaseEmail().SendEmail(email, notificationConfigID);
        //            }
        //        }
        //    }
        //}

        //private void SendTalentRequisitionApprovedNotifications(string emailTo, string emailCC, string tRCode, string projectName, string roleName)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString("TRApproved");
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //            string htmlContent = notificationConfiguration.emailContent;
        //            string htmlSubject = notificationConfiguration.emailSubject;

        //            var reportingManagerDetails = (from e in hrmEntities.Employees
        //                                           join usr in hrmEntities.Users on e.UserId equals usr.UserId
        //                                           where usr.EmailAddress == emailTo
        //                                           select new
        //                                           {
        //                                               Name = e.FirstName + " " + e.LastName,
        //                                               EmailAddress = usr.EmailAddress
        //                                           }).FirstOrDefault();

        //            if (reportingManagerDetails != null)
        //            {
        //                string hrMangerEmailAddress = GetHRManagerEmailAddress();
        //                if (!string.IsNullOrEmpty(hrMangerEmailAddress))
        //                    emailCC += ";" + hrMangerEmailAddress;
        //                emailCC = emailCC.Replace(";;", ";");
        //                htmlContent = htmlContent.Replace("@CreatedBy", reportingManagerDetails.Name).Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
        //                htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);

        //                Email email = new Email();
        //                int notificationConfigID = new BaseEmail().BuildEmailObject(email, emailTo, notificationConfiguration.emailFrom, emailCC, htmlSubject,
        //                                               htmlContent, Enumeration.NotificationStatus.TRApproved.ToString());
        //                new BaseEmail().SendEmail(email, notificationConfigID);
        //            }
        //        }
        //    }

        //}

        //private void SendTalentRequisitionApprovedNotificationsNG2(string toMailName, string emailTo, string emailCC, string tRCode, string projectName, string roleName, string TRApprovalStatus)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString(TRApprovalStatus);
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //            string htmlContent = notificationConfiguration.emailContent;
        //            string htmlSubject = notificationConfiguration.emailSubject;

        //            var reportingManagerDetails = (from e in hrmEntities.Employees
        //                                           join usr in hrmEntities.Users on e.UserId equals usr.UserId
        //                                           where usr.EmailAddress == emailTo
        //                                           select new
        //                                           {
        //                                               Name = e.FirstName + " " + e.LastName,
        //                                               EmailAddress = usr.EmailAddress
        //                                           }).FirstOrDefault();

        //            if (reportingManagerDetails != null)
        //            {
                        //string hrMangerEmailAddress = GetHRManagerEmailAddress();
                        //if (!string.IsNullOrEmpty(hrMangerEmailAddress))
                        //    emailCC += ";" + hrMangerEmailAddress;
                        //emailCC = emailCC.Replace(";;", ";");
        //                htmlContent = htmlContent.Replace("@CreatedBy", toMailName).Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
        //                htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);

        //                Email email = new Email();
        //                int notificationConfigID = new BaseEmail().BuildEmailObject(email, emailTo, notificationConfiguration.emailFrom, emailCC, htmlSubject,
        //                                               htmlContent, Enumeration.NotificationStatus.TRApproved.ToString());
        //                new BaseEmail().SendEmail(email, notificationConfigID);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Sends TR approved notification to Deliveray Head and HRM.
        /// </summary>
        /// <param name="name">The name of the Delivery Head; if not present, the HRM name.</param>
        /// <param name="emailTo">The Email address of Delivery Head; if not present, the HRM's email.</param>
        /// <param name="emailCC">The Email address of HRM.</param>
        /// <param name="tRId">TR Number.</param>
        //private void SendTRApprovedNotifications(string name, string emailTo, string emailCC, string tRCode, string projectName, string roleName)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString("TRApproved");
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //            string htmlContent = notificationConfiguration.emailContent;
        //            string htmlSubject = notificationConfiguration.emailSubject;
        //            htmlContent = htmlContent.Replace("@CreatedBy", name).Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
        //            htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
        //            Email email = new Email();
        //            int notificationConfigID = new BaseEmail().BuildEmailObject(email, emailTo, notificationConfiguration.emailFrom, emailCC, htmlSubject, htmlContent, Enumeration.NotificationStatus.TRApproved.ToString());
        //            new BaseEmail().SendEmail(email, notificationConfigID);
        //        }
        //    }
        //}

        //private void SendTalentRequisitionRejectedNotifications(string emailTo, string emailCC, string tRCode, string comments, string projectName, string roleName)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString("TRRejected");
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //            string htmlContent = notificationConfiguration.emailContent;
        //            string htmlSubject = notificationConfiguration.emailSubject;

        //            var reportingManagerDetails = (from e in hrmEntities.Employees
        //                                           join usr in hrmEntities.Users on e.UserId equals usr.UserId
        //                                           where usr.EmailAddress == emailTo
        //                                           select new
        //                                           {
        //                                               Name = e.FirstName + " " + e.LastName,
        //                                               EmailAddress = usr.EmailAddress
        //                                           }).FirstOrDefault();

        //            if (reportingManagerDetails != null)
        //            {
        //                string hrMangerEmailAddress = GetHRManagerEmailAddress();

        //                if (!string.IsNullOrEmpty(hrMangerEmailAddress))
        //                    emailCC += ";" + hrMangerEmailAddress;

        //                emailCC = emailCC.Replace(";;", ";");

        //                htmlContent = htmlContent.Replace("@CreatedBy", reportingManagerDetails.Name).Replace("@TRCode", tRCode).Replace("@Comments", comments).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);
        //                htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);

        //                //new BaseEmail().SendEmail(emailTo, notificationConfiguration.emailFrom, emailCC, htmlSubject, htmlContent);
        //            }
        //        }
        //    }

        //}

        //private void SendTalentRequisitionRejectedNotificationsNG2(string toMailName, string emailTo, string emailCC, string tRCode, string comments, string projectName, string roleName, string rejectedReason)
        //{
        //    using (APEntities hrmEntities = new APEntities())
        //    {
        //        string adminNotificationCode = ConstantResources.ResourceManager.GetString("TRRejected");
        //        NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

        //        if (notificationConfiguration != null)
        //        {
        //            string htmlContent = notificationConfiguration.emailContent;
        //            string htmlSubject = notificationConfiguration.emailSubject;

        //            var reportingManagerDetails = (from e in hrmEntities.Employees
        //                                           join usr in hrmEntities.Users on e.UserId equals usr.UserId
        //                                           where usr.EmailAddress == emailTo
        //                                           select new
        //                                           {
        //                                               Name = e.FirstName + " " + e.LastName,
        //                                               EmailAddress = usr.EmailAddress
        //                                           }).FirstOrDefault();

        //            if (reportingManagerDetails != null)
        //            {
        //                string hrMangerEmailAddress = GetHRManagerEmailAddress();

        //                //if (!string.IsNullOrEmpty(hrMangerEmailAddress))
        //                //    emailCC += ";" + hrMangerEmailAddress;

        //                //emailCC = emailCC.Replace(";;", ";");

        //                htmlContent = htmlContent.Replace("@CreatedBy", toMailName).Replace("@TRCode", tRCode).Replace("@Comments", comments).Replace("@ProjectName", projectName).Replace("@RoleName", roleName).Replace("@Reason", rejectedReason);
        //                htmlSubject = htmlSubject.Replace("@TRCode", tRCode).Replace("@ProjectName", projectName).Replace("@RoleName", roleName);

        //                Email email = new Email();
        //                int notificationConfigID = new BaseEmail().BuildEmailObject(email, emailTo, notificationConfiguration.emailFrom, emailCC, htmlSubject,
        //                                               htmlContent, Enumeration.NotificationStatus.TRRejected.ToString());
        //                new BaseEmail().SendEmail(email, notificationConfigID);
        //            }
        //        }
        //    }
        //}
        private bool AddRequistiionRoleDetail(RequisitionRoleDetails roleDetails, int tRId, string currentUser, string systemInfo)
        {
            bool returnValue = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                RequisitionRoleDetail requistiionRoleDetail = new RequisitionRoleDetail()
                {
                    TRId = tRId,
                    RoleMasterId = roleDetails.RoleMasterId,
                    NoOfBillablePositions = roleDetails.NoOfBillablePositions ?? 0,
                    NoOfNonBillablePositions = roleDetails.NoOfNonBillablePositions ?? 0,
                    MinimumExperience = roleDetails.MinimumExperience,
                    MaximumExperience = roleDetails.MaximumExperience,
                    EssentialEducationQualification = roleDetails.EssentialEducationQualification,
                    DesiredEducationQualification = roleDetails.DesiredEducationQualification,
                    ProjectSpecificResponsibilities = roleDetails.ProjectSpecificResponsibilities,
                    RoleDescription = roleDetails.RoleDescription,
                    Expertise = roleDetails.Expertise,
                    CreatedUser = currentUser,
                    SystemInfo = systemInfo,
                    KeyResponsibilities = roleDetails.KeyResponsibilities,
                    CreatedDate = DateTime.Now
                };
                hrmsEntities.RequisitionRoleDetails.Add(requistiionRoleDetail);
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                returnValue = AddRequisitionRoleSkills(roleDetails.RequisitionRoleSkills, tRId, requistiionRoleDetail.RequisitionRoleDetailID, currentUser, roleDetails.RoleMasterId, systemInfo);
            }

            return returnValue;
        }
        private bool AddRequisitionRoleSkills(IEnumerable<RequisitionRoleSkills> requisitionRoleSkills, int tRId, int requistiionRoleDetailID, string currentUser, int roleId, string systemInfo)
        {
            bool returnValue = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                List<RequisitionRoleSkill> RequisitionRoleSkillList = new List<RequisitionRoleSkill>();
                foreach (var rrskill in requisitionRoleSkills)
                {
                    RequisitionRoleSkillMapping roleSkillMapping = new RequisitionRoleSkillMapping()
                    {
                        TRID = tRId,
                        RequistiionRoleDetailID = requistiionRoleDetailID
                    };

                    RequisitionRoleSkill requisitionRoleSkill = new RequisitionRoleSkill()
                    {
                        RoleSkillMappingID = rrskill.RoleSkillMappingID,
                        CompetencyAreaID = rrskill.CompetencyAreaId,
                        SkillID = rrskill.SkillId,
                        ProficiencyLevelID = rrskill.ProficiencyLevelId,
                        CreatedUser = currentUser,
                        SystemInfo = systemInfo,
                        CreatedDate = DateTime.Now

                    };

                    if (rrskill.SkillId == -1)
                    {
                        requisitionRoleSkill.SkillID = AddNewSkill(rrskill, currentUser, roleId, systemInfo);
                    }

                    RequisitionRoleSkillList.Add(requisitionRoleSkill);
                }
                hrmsEntities.RequisitionRoleSkills.AddRange(RequisitionRoleSkillList);
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return returnValue;
        }

        private bool AddRequisitionRoleSkill(RequisitionRoleSkills requisitionRoleSkills, int tRId, int requistiionRoleDetailID, string currentUser, int roleId, string systemInfo)
        {
            bool returnValue = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                List<RequisitionRoleSkill> RequisitionRoleSkillList = new List<RequisitionRoleSkill>();

                RequisitionRoleSkillMapping roleSkillMapping = new RequisitionRoleSkillMapping()
                {
                    TRID = tRId,
                    RequistiionRoleDetailID = requistiionRoleDetailID
                };

                RequisitionRoleSkill requisitionRoleSkill = new RequisitionRoleSkill()
                {
                    RoleSkillMappingID = requisitionRoleSkills.RoleSkillMappingID,
                    CompetencyAreaID = requisitionRoleSkills.CompetencyAreaId,
                    SkillID = requisitionRoleSkills.SkillId,
                    ProficiencyLevelID = requisitionRoleSkills.ProficiencyLevelId,
                    CreatedUser = currentUser,
                    SystemInfo = systemInfo,
                    CreatedDate = DateTime.Now
                };

                if (requisitionRoleSkill.SkillID == -1)
                {
                    requisitionRoleSkill.SkillID = AddNewSkill(requisitionRoleSkills, currentUser, roleId, systemInfo);
                }

                RequisitionRoleSkillList.Add(requisitionRoleSkill);
                hrmsEntities.RequisitionRoleSkills.Add(requisitionRoleSkill);
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return returnValue;
        }

        private int AddNewSkill(RequisitionRoleSkills requisitionRoleSkills, string currentUser, int roleId, string systemInfo)
        {
            bool returnValue = false;
            int newSkillId = 0;

            using (APEntities hrmsEntities = new APEntities())
            {
                if (requisitionRoleSkills.SkillId == -1)
                {
                    Skill newSkill = new Skill();
                    newSkill.SkillCode = requisitionRoleSkills.OtherSkillCode;
                    newSkill.SkillName = requisitionRoleSkills.OtherSkillCode;
                    newSkill.SkillDescription = requisitionRoleSkills.OtherSkillCode;
                    newSkill.CompetencyAreaId = requisitionRoleSkills.CompetencyAreaId;
                    newSkill.IsActive = true;
                    newSkill.IsApproved = false;
                    newSkill.CreatedUser = currentUser;
                    newSkill.CreatedDate = DateTime.Now;
                    newSkill.ModifiedUser = currentUser;
                    newSkill.SystemInfo = systemInfo;
                    newSkill.ModifiedDate = DateTime.Now;
                    hrmsEntities.Skills.Add(newSkill);
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                    if (returnValue == true)
                    {
                        CompetencySkill cSkill = new CompetencySkill();
                        cSkill.RoleMasterID = roleId;
                        cSkill.CompetencyAreaId = requisitionRoleSkills.CompetencyAreaId;
                        cSkill.SkillId = newSkill.SkillId;
                        cSkill.IsActive = true;
                        cSkill.CreatedUser = currentUser;
                        cSkill.ModifiedUser = currentUser;
                        cSkill.SystemInfo = systemInfo;
                        cSkill.ModifiedDate = DateTime.Now;
                        cSkill.CreatedDate = DateTime.Now;
                        cSkill.IsPrimary = false;
                        cSkill.ProficiencyLevelId = requisitionRoleSkills.ProficiencyLevelId;
                        hrmsEntities.CompetencySkills.Add(cSkill);
                        returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                        newSkillId = newSkill.SkillId;
                    }
                }
            }
            return newSkillId;
        }

        //private int GetRequistionStatsId(string status)
        //{
        //    int statusId = 0;
        //    if (status.Equals(ConstantResources.ResourceManager.GetString("Requisitionsaveasdraft"), StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        statusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.Draft.ToString());
        //    }
        //    else if (status.Equals(ConstantResources.ResourceManager.GetString("Requisitionapprovaldue"), StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        statusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), TalentRequisitionStatusCodes.ApprovalDue.ToString());
        //    }
        //    else
        //        statusId = new Common().GetStatusId(StatusCategory.TalentRequisition.ToString(), status);
        //    return statusId;
        //}

        private bool DeleteRequisitionRoleSkills(List<int> deleteids)
        {
            bool returnValue = false;

            using (APEntities hrmsEntities = new APEntities())
            {
                var existRequisitionRoleSkill = hrmsEntities.RequisitionRoleSkills.Where(r => deleteids.Contains(r.RoleSkillMappingID)).ToList();
                hrmsEntities.RequisitionRoleSkills.RemoveRange(existRequisitionRoleSkill);
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return returnValue;
        }

        private bool DeleteRequisitionRoleDetails(List<int> deleteids)
        {
            bool returnValue = false;

            using (APEntities hrmsEntities = new APEntities())
            {
                var requisitionRoleDetails = hrmsEntities.RequisitionRoleDetails.Where(r => deleteids.Contains(r.RequisitionRoleDetailID)).ToList();
                var skills = hrmsEntities.RequisitionRoleSkillMappings.Where(r => deleteids.Contains(r.RequistiionRoleDetailID)).ToList();
                hrmsEntities.RequisitionRoleSkillMappings.RemoveRange(skills);
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                hrmsEntities.RequisitionRoleDetails.RemoveRange(requisitionRoleDetails);
                returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return returnValue;
        }

        private int GetReportingManager(string userName)
        {
            int reportingManagerId = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                var reportingManager = (from emp in hrmsEntities.Employees
                                        join usr in hrmsEntities.Users on emp.UserId equals usr.UserId
                                        where usr.EmailAddress == userName
                                        select new
                                        {
                                            ReportingManager = emp.ReportingManager
                                        }).FirstOrDefault();

                if (reportingManager != null)
                    reportingManagerId = Convert.ToInt32(reportingManager.ReportingManager);

                return reportingManagerId;
            }
        }
        private AssociateDetails GetDepartmentHeadDetails(string departmentCode)
        {
            int reportingManagerId = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                var reportingManagerDetails = (from dept in hrmsEntities.Departments
                                        join emp in hrmsEntities.Employees on dept.DepartmentId equals emp.DepartmentId
                                        join usr in hrmsEntities.Users on emp.UserId equals usr.UserId
                                        where dept.DepartmentCode == departmentCode && emp.IsActive == true
                                        && usr.IsActive == true && dept.IsActive == true
                                        select new AssociateDetails
                                        {
                                            empName = emp.FirstName + " " + emp.LastName,
                                            EmailAddress = usr.EmailAddress

                                        }).FirstOrDefault();

                return reportingManagerDetails;
            }
        }

        private string GetHRManagerEmailAddress()
        {
            string hrManagerEmail = string.Empty;
            using (APEntities hrmsEntities = new APEntities())
            {
                var hrManagerRoleName = ConstantResources.ResourceManager.GetString("HRManagerRoleName");
                var hrManagers = (from usrrole in hrmsEntities.UserRoles
                                  join usr in hrmsEntities.Users on usrrole.UserId equals usr.UserId
                                  join role in hrmsEntities.Roles on usrrole.RoleId equals role.RoleId
                                  where role.RoleName == hrManagerRoleName && usrrole.IsActive == true && usr.IsActive == true
                                  select new
                                  {
                                      HRManagerEmail = usr.EmailAddress
                                  }).ToList();

                if (hrManagers?.Count > 0)
                {
                    string hrm = string.Join(";", hrManagers.Select(i => i.HRManagerEmail));
                    hrManagerEmail = Regex.Replace(hrm, "([;][;]+)", ";");
                }
                return hrManagerEmail;
            }
        }
      
        #endregion
    }
}
