﻿
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.API
{
    public class FinancialYearDetails
    {
        #region GetFinancialYear
        /// <summary>
        /// Gets list of Financial Years
        /// </summary>
        /// <returns></returns>
        public async Task<List<FinancialYearData>> GetFinancialYears()
        {
            List<FinancialYearData> lstFinancialYears;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstFinancialYears = await apEntities.Database.SqlQuery<FinancialYearData>
                              ("[usp_GetAllFinancialYears]").ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstFinancialYears.OrderBy(f=>f.FromYear).ToList();
        }
        #endregion

        #region GetCurrentFinancialYearByMonth
        /// <summary>
        /// Gets Current FinancialYear By Month
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetCurrentFinancialYearByMonth()
        {
            int currentFinancialYear;
            try
            {
                using (var apEntities = new APEntities())
                {
                    currentFinancialYear = await apEntities.Database.SqlQuery<int>
                              ("[usp_GetCurrentFinancialYearByMonth]").SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return currentFinancialYear;
        }
        #endregion

        #region CreateFinancialYear
        /// <summary>
        /// Create Financial Year
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateFinancialYear(FinancialYearData financialYearData)
        {
            int rowsAffected = 0;

            if (financialYearData.FromYear > financialYearData.ToYear || financialYearData.FromYear < 2015 || financialYearData.ToYear < 2015 || financialYearData.ToYear > 9999 || financialYearData.FromYear > 9999)
            {
                return -11; //Nothing to save
            }
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateFinancialYearAndStatus] @FromYear,@ToYear, @IsActive",
                                   new object[] {
                                        new SqlParameter ("FromYear", financialYearData.FromYear),
                                        new SqlParameter ("ToYear", financialYearData.ToYear),
                                        new SqlParameter ("IsActive", financialYearData.IsActive)
                                   }
                                   ).SingleOrDefaultAsync();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while creating a New Financial Year");
            }
            return rowsAffected;
        }
        #endregion


        #region UpdateFinancialYearStatus
        /// <summary>
        /// Update FinancialYear Status
        /// </summary>
        /// <param name="financialYearData"></param>
        /// <returns></returns>
        public async Task<int> UpdateFinancialYearStatus(FinancialYearData financialYearData)
        {
            int rowsAffected;

            if (financialYearData.FromYear > financialYearData.ToYear || financialYearData.FromYear < 2015 || financialYearData.ToYear < 2015 || financialYearData.ToYear > 9999 || financialYearData.FromYear > 9999)
            {
                return -11; //Nothing to save
            }

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateFinancialYearAndStatus] @ID, @FromYear,@ToYear, @IsActive",
                               new object[] {
                                        new SqlParameter ("ID", financialYearData.Id),
                                        new SqlParameter ("FromYear", financialYearData.FromYear),
                                        new SqlParameter ("ToYear", financialYearData.ToYear),
                                        new SqlParameter ("IsActive", financialYearData.IsActive)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

    }
}
