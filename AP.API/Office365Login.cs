﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using AP.DomainEntities;
using unirest_net.http;
using AP.Utility;

namespace AP.API
{
    public class Office365Login : ILogin
    {
        #region LoginUser
        /// <summary>
        /// Login User
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool LoginUser(string userName,string password)
        {
            bool result = false;

            if (userName != null && password != null)
            {
                try
                {
                    HttpResponse<string> response = Unirest.get("https://outlook.office365.com/api/v1.0/me/").basicAuth(userName, Commons.DecryptStringAES(password)).asJson<string>();

                    if (response.Code == 200)
                        result = true;
                }
                catch
                {
                    throw;
                }
            }

            return result;
        }

        private string decrypt(string password)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
