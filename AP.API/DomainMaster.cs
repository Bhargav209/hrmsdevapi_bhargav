﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class DomainMaster
    {
        #region GetDomains
        /// <summary>
        /// Gets Domain Master
        /// </summary>
        /// <returns></returns>
        public async Task<List<DomainMasterData>> GetDomains()
        {
            List<DomainMasterData> lstDomains;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstDomains = await apEntities.Database.SqlQuery<DomainMasterData>
                              ("[usp_GetDomain]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDomains;
        }
        #endregion

        #region CreateDomain
        /// <summary>
        /// Create a new Domain
        /// </summary>
        /// <param name="domainMasterData"></param>
        /// <returns></returns>
        public async Task<int> CreateDomain(DomainMasterData domainMasterData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateDomain] @DomainName, @CreatedDate, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("DomainName", domainMasterData.DomainName),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region UpdateDomain
        /// <summary>
        /// Update a Domain based on Domain ID
        /// </summary>
        /// <param name="domainMasterData"></param>
        /// <returns></returns>
        public async Task<int> UpdateDomain(DomainMasterData domainMasterData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateDomain] @DomainID, @DomainName, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("DomainID", domainMasterData.DomainID),
                                        new SqlParameter ("DomainName", domainMasterData.DomainName),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
