﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.IO;
using AP.DataStorage;
using AP.DomainEntities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using AP.Utility;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Web;
using static AP.Utility.Enumeration;
using AP.Notification;
using System.Text;
using System.Net;

namespace AP.API
{
    public class DashboardDetails
    {
        #region Notification Methods

        #region GetAssociateCodeByEmpID
        /// <summary>
        /// Get Associate Code By EmpID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public string GetAssociateCodeByEmpID(int empID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {

                    var employeeCode = (from e in hrmEntities.Employees
                                        where e.EmployeeId == empID
                                        select new { e.EmployeeCode }).FirstOrDefault();

                    if (employeeCode != null)
                        return employeeCode.EmployeeCode;
                    else
                        return "";
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetHRHeadDetails
        /// <summary>
        /// GetHRHeadDetails
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetHRHeadDetails()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAssociateDetails = (from e in hrmsEntities.Employees
                                               where e.StatusId == (int)(Enumeration.EPCStatus.Pending)
                                               select new
                                               {
                                                   notificationType = "EPC",
                                                   empID = e.EmployeeId,
                                                   empCode = e.EmployeeCode,
                                                   empName = e.FirstName + " " + e.LastName,
                                                   hrAdvisor = e.HRAdvisor,
                                                   //remarks = notification.Remarks
                                               }).Distinct().OrderByDescending(o => o.empID).ToList();
                    return getAssociateDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion
        
        public async Task<List<EmployeeSkillDetails>> GetSkillDetails()
        {
            List<EmployeeSkillDetails> lstSkill;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkill = await apEntities.Database.SqlQuery<EmployeeSkillDetails>
                              ("[usp_GetEmployeeSkills]").ToListAsync();
                    
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get associate skills");
            }
            return lstSkill;
        }

        public async Task<bool> CreateEmployeeSkill(EmployeeSkillDetails employeeSkill)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateEmployeeSkill] @SkillsSubmittedForApprovalId,@EmployeeId,@CompetencyAreaId,@SkillId,@ProficiencyLevelId,@Experience,@LastUsed,@IsPrimary,@CreatedUser,@CreatedDate,@SystemInfo,@SkillGroupId,@WorkFlowId,@SubmittedBy,@SubmittedTo",
                               new object[] {
                                        new SqlParameter ("SkillsSubmittedForApprovalId", employeeSkill.SkillsSubmittedForApprovalId),
                                        new SqlParameter ("EmployeeId", employeeSkill.empID),
                                        new SqlParameter ("CompetencyAreaId", employeeSkill.CompetencyAreaID),
                                        new SqlParameter ("SkillId",employeeSkill.skillID),
                                        new SqlParameter ("ProficiencyLevelId", employeeSkill.proficiencyLevelId),
                                        new SqlParameter ("Experience", employeeSkill.experience),
                                        new SqlParameter ("LastUsed", employeeSkill.LastUsed),
                                        new SqlParameter ("IsPrimary",(object)employeeSkill.isPrimary ?? DBNull.Value),
                                        new SqlParameter ("CreatedUser",  HttpContext.Current.User.Identity.Name),
                                        new SqlParameter("CreatedDate",DateTime.UtcNow),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("SkillGroupId", employeeSkill.SkillGroupID),
                                        new SqlParameter ("WorkFlowId", employeeSkill.WorkFlowId),
                                        new SqlParameter ("SubmittedBy", employeeSkill.empID),
                                        new SqlParameter ("SubmittedTo", employeeSkill.SubmittedTo),
                                   }
                                   ).SingleOrDefaultAsync();

                        trans.Commit();
                    }

                    //Send notification email to RM for approval
                    using (var hrmEntities = new APEntities())
                    {
                        EmailNotificationConfiguration emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                                  ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                    new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.NotificationType.ESU)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.ESU)),
                                   }
                                  ).FirstOrDefault();

                        if (emailNotificationConfig != null)
                        {
                            EmployeeSkillDetails employeeSkillDetails = hrmEntities.Database.SqlQuery<EmployeeSkillDetails>
                                         ("[usp_GetEmployeeskillsForNotification] @SkillId,@empID",
                                           new object[]  {
                                    new SqlParameter ("SkillId", employeeSkill.skillID),
                                    new SqlParameter ("empID", employeeSkill.empID)
                                          }
                                         ).FirstOrDefault();

                            if (employeeSkillDetails != null)
                            {
                                NotificationDetail notificationDetail = new NotificationDetail();
                                StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

                                emailContent = emailContent.Replace("{SkillGroupName}", employeeSkillDetails.SkillGroupName)
                                                           .Replace("{SkillName}", employeeSkillDetails.SkillName)
                                                           .Replace("{EmployeeName}", employeeSkillDetails.EmployeeName)
                                                           .Replace("{ProficiencyLevel}", employeeSkillDetails.ProficiencyLevel)
                                                           .Replace("{experience}",Convert.ToString(employeeSkillDetails.experience))
                                                           .Replace("{LastUsed}", Convert.ToString(employeeSkillDetails.LastUsed))
                                                           .Replace("{StatusCode}", employeeSkillDetails.StatusCode)
                                                           .Replace("{isPrimary}", Convert.ToString( employeeSkillDetails.isPrimary));
                                                                 

                                if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                                    throw new AssociatePortalException("Email From cannot be blank");
                                notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                                if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                                    throw new AssociatePortalException("Email To cannot be blank");
                                notificationDetail.ToEmail = employeeSkillDetails.ToEmailAddress;

                                notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                                notificationDetail.Subject = emailNotificationConfig.EmailSubject + " " + employeeSkillDetails.SkillName;
                                notificationDetail.EmailBody = emailContent.ToString();
                                NotificationManager.SendEmail(notificationDetail);
                                
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a  Work station");
            }
            return rowsAffected > 0 ? true : false;
        }

        #region GetHRADetails
        /// <summary>
        /// HRA details
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetHRADetails()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAssociateDetails = (from e in hrmsEntities.Employees
                                               where e.StatusId == (int)(Enumeration.EPCStatus.Rejected)
                                               select new
                                               {
                                                   notificationType = "EPC",
                                                   empID = e.EmployeeId,
                                                   empCode = e.EmployeeCode,
                                                   empName = e.FirstName + " " + e.LastName,
                                                   remarks = e.Remarks
                                               }).Distinct().OrderByDescending(o => o.empID).ToList();

                    return getAssociateDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetHRANotificationCount
        /// <summary>
        /// HRA notification count
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns></returns>
        public int GetHRANotificationCount(int notificationTypeId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    //var hrcnt = (from notification in hrmsEntities.Notifications
                    //             where notification.StatusId == 3 && notification.NotificationTypeID == notificationTypeId
                    //             select new
                    //             {
                    //                empCode = notification.EmployeeCode,
                    //             }).Distinct().ToList();

                    //return hrcnt.Count;
                    return 0;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get notification count.");
            }
        }
        #endregion

        #region GetNotificationCount
        /// <summary>
        /// GetNotificationCount
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns></returns>
        /// 
        public int GetNotificationCount(int notificationTypeId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    //var hrcnt = (from notification in hrmsEntities.Notifications
                    //             where notification.StatusId == 2 && notification.NotificationTypeID == notificationTypeId
                    //             select new
                    //             {
                    //                empCode = notification.EmployeeCode,
                    //             }).Distinct().ToList();

                    //return hrcnt.Count;
                    return 0;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region PDFGeneration

        #region GeneratePDFReport
        /// <summary>
        /// Generate PDF
        /// </summary>
        /// <param name="empID"></param>
        public byte[] GeneratePDFReport(int empID)
        {
            Paragraph para = null;
            Phrase pharse = null;
            PdfPTable table = null;
            PdfPCell cell = null;
            Document document = new Document(PageSize.A4, 30, 30, 30, 130); // PageSize.A4, left, right, top , bottom
            Font contentFont = FontFactory.GetFont(FontFactory.HELVETICA, 11f, BaseColor.BLACK);
            Font contentFontBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11f, BaseColor.BLACK);

            string pdfPassword = ConfigurationManager.AppSettings["pdfPassword"];
            APEntities hrmsEntities = new APEntities();
            try
            {
                Employee empexits = hrmsEntities.Employees.Where(i => i.EmployeeId == empID).FirstOrDefault();

                if (empexits != null)
                {
                    PersonalDetails getEmployeeDetails = GetEmployeeDetailsbyEmpId(empID);

                    List<ContactDetails> getContactDetails = GetContactDetailsbyEmpId(empID);

                    List<EmergencyContactData> emergencyPrimaryContactDetails = GetEmergencyDetailsbyEmpId(empID);

                    List<RelationDetails> familyDetails = GetFamilyDetailsbyEmpId(empID);

                    List<EducationDetails> educationDetails = GetEducationalDetailsbyEmpId(empID);

                    // List<CertificateDetails> profcertificationdetails = GetCertificateDetailsbyEmpId(empID);

                    // List<MembershipDetails> getMembershipDetails = GetMembershipDetailsbyEmpId(empID);

                    List<EmploymentDetails> getPrevEmploymentDetails = GetEmploymentDetailsbyEmpId(empID);

                    List<ProfRefDetails> getProfReferenceDetils = GetProfRefDetailsbyEmpId(empID);

                    List<EmployeeSkillDetails> skills = GetSkillDetailsbyEmpId(empID);

                    List<EmployeeProjectDetails> projects = GetProjectDetailsbyEmpId(empID);

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        PdfWriter.GetInstance(document, memoryStream);
                        document.Open();

                        table = PDFHRSection(document, para, contentFontBold, contentFont, table, cell, pharse, getEmployeeDetails);
                        document.Add(table);

                        table = PDFPersonalSection(document, para, contentFontBold, contentFont, table, cell, pharse, getEmployeeDetails, getContactDetails);
                        document.Add(table);

                        table = PDFEmergencyContactSection(document, para, contentFontBold, contentFont, table, cell, pharse, emergencyPrimaryContactDetails);
                        document.Add(table);

                        table = PDFFamilySection(document, para, contentFontBold, contentFont, table, cell, pharse, familyDetails);
                        document.Add(table);

                        table = PDFEducationalSection(document, para, contentFontBold, contentFont, table, cell, pharse, educationDetails);
                        document.Add(table);

                        //table = PDFCertificationSection(document, para, contentFontBold, contentFont, table, cell, pharse, profcertificationdetails);
                        // document.Add(table);

                        // table = PDFMemberShipSection(document, para, contentFontBold, contentFont, table, cell, pharse, getMembershipDetails);
                        // document.Add(table);

                        table = PDFEmploymentSection(document, para, contentFontBold, contentFont, table, cell, pharse, getPrevEmploymentDetails);
                        document.Add(table);

                        table = PDFSkillSection(document, para, contentFontBold, contentFont, table, cell, pharse, skills);
                        document.Add(table);

                        table = PDFProjectSection(document, para, contentFontBold, contentFont, table, cell, pharse, projects);
                        document.Add(table);

                        table = PDFProfessionalReferencesSection(document, para, contentFontBold, contentFont, table, cell, pharse, getProfReferenceDetils);
                        document.Add(table);

                        document = PDFDeclarationSection(document, para, contentFontBold, contentFont, table, cell, pharse);

                        document.Close();
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();

                        using (MemoryStream input = new MemoryStream(bytes))
                        {
                            using (MemoryStream output = new MemoryStream())
                            {
                                PdfReader reader = new PdfReader(input);
                                PdfEncryptor.Encrypt(reader, output, true, pdfPassword, pdfPassword, PdfWriter.ALLOW_SCREENREADERS);
                                bytes = output.ToArray();
                                return bytes;
                            }
                        }
                    }
                }
                return null;
            }
            catch
            {
                throw new AssociatePortalException("Failed to generate PDF.");
            }
        }
        #endregion

        #region GetEmployeeDetailsbyEmpId
        /// <summary>
        /// Get Employee Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private PersonalDetails GetEmployeeDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    PersonalDetails personalDetails = (from emp in hrmsEntities.Employees
                                                       where emp.EmployeeId == empID
                                                       select new PersonalDetails
                                                       {
                                                           empCode = emp.EmployeeCode,
                                                           workEmailID = emp.WorkEmailAddress,
                                                           firstName = emp.FirstName,
                                                           middleName = emp.MiddleName,
                                                           lastName = emp.LastName,
                                                           EncryptedPhoneNumber = emp.TelephoneNo,
                                                           EncryptedMobileNo = emp.MobileNo,
                                                           personalEmail = emp.PersonalEmailAddress,
                                                           Birthdate = SqlFunctions.DateName("day", emp.DateofBirth) + "/" + SqlFunctions.StringConvert((double)emp.DateofBirth.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", emp.DateofBirth),
                                                           gender = emp.Gender,
                                                           maritalStatus = emp.MaritalStatus,
                                                           bloodGroup = emp.BloogGroup,
                                                           nationality = emp.Nationality,
                                                           EncryptedPanNumber = emp.PANNumber,
                                                           EncryptedPFNumber = emp.PFNumber,
                                                           EncryptedAadharNumber = emp.AadharNumber,
                                                           EncryptedUANNumber = emp.UANNumber,
                                                           EncryptedPassportNumber = emp.PassportNumber
                                                       }).FirstOrDefault();

                    return personalDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get employee details.");
            }

        }
        #endregion

        #region GetContactDetailsbyEmpId
        /// <summary>
        /// Get Contact Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<ContactDetails> GetContactDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    List<ContactDetails> contactDetails = (from contact in hrmsEntities.Contacts
                                                           where contact.EmployeeId == empID && (contact.AddressType == "CurrentAddress" || contact.AddressType == "PermanentAddress")
                                                           select new ContactDetails
                                                           {
                                                               currentAddress1 = contact.AddressLine1,
                                                               currentAddress2 = contact.AddressLine2,
                                                               currentAddCity = contact.City,
                                                               currentAddState = contact.State,
                                                               currentAddZip = contact.PostalCode,
                                                               currentAddCountry = contact.Country,
                                                               addressType = contact.AddressType
                                                           }
                                           ).ToList();

                    return contactDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get contact details.");

            }

        }
        #endregion

        #region GetEmergencyDetailsbyEmpId
        /// <summary>
        /// Get Emergency Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<EmergencyContactData> GetEmergencyDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    List<EmergencyContactData> emergencyContactData = (from ePrimeContact in hrmsEntities.EmergencyContactDetails
                                                                       where ePrimeContact.EmployeeId == empID && (ePrimeContact.ContactType == "PrimaryContact" || ePrimeContact.ContactType == "")
                                                                       select new EmergencyContactData
                                                                       {
                                                                           ID = ePrimeContact.EmergencyContactId,
                                                                           contactName = ePrimeContact.ContactName,
                                                                           relationship = ePrimeContact.Relationship,
                                                                           addressLine1 = ePrimeContact.AddressLine1,
                                                                           addressLine2 = ePrimeContact.AddressLine2,
                                                                           city = ePrimeContact.City,
                                                                           state = ePrimeContact.State,
                                                                           zip = ePrimeContact.PostalCode,
                                                                           country = ePrimeContact.Country,
                                                                           EncryptedMobileNo = ePrimeContact.MobileNo,
                                                                           EncryptedTelePhoneNo = ePrimeContact.TelephoneNo,
                                                                           emailAddress = ePrimeContact.EmailAddress,
                                                                           contactType = ePrimeContact.ContactType
                                                                       }).OrderByDescending(e => e.contactType).ToList();

                    return emergencyContactData;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get emergency contact details.");
            }

        }
        #endregion

        #region GetFamilyDetailsbyEmpId
        /// <summary>
        /// Get Family Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<RelationDetails> GetFamilyDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    return (from family in hrmsEntities.FamilyDetails
                            where family.EmployeeId == empID
                            select new RelationDetails
                            {
                                EncryptedName = family.Name,
                                relationship = family.Relationship,
                                birthDate = SqlFunctions.DateName("day", family.DateofBirth) + "/" + SqlFunctions.StringConvert((double)family.DateofBirth.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", family.DateofBirth),
                                occupation = family.Occupation
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get family details.");
            }
        }
        #endregion

        #region GetEducationalDetailsbyEmpId
        /// <summary>
        /// Get Educational Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<EducationDetails> GetEducationalDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    return (from ed in hrmsEntities.EducationDetails
                            where ed.EmployeeId == empID & ed.IsActive == true 
                            select new EducationDetails
                            {
                                qualificationName = ed.EducationalQualification,                               
                                completedYear = SqlFunctions.DateName("day", ed.AcadamicCompletedYear) + "/" + SqlFunctions.StringConvert((double)ed.AcadamicCompletedYear.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", ed.AcadamicCompletedYear),
                                institution = ed.NameofInstitution,
                                specialization = ed.Specialization,
                                programType = ed.ProgramType,
                                grade = ed.Grade,
                                marks = ed.Marks_
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get education details.");
            }
        }
        #endregion

        #region GetCertificateDetailsbyEmpId
        /// <summary>
        /// Get Certificate Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        //private List<CertificateDetails> GetCertificateDetailsbyEmpId(int empID)
        //{
        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            return (from pc in hrmsEntities.ProfessionalDetails
        //                    where (pc.EmployeeId == empID && pc.ProgramType == "Certification")
        //                    select new CertificateDetails
        //                    {
        //                        certificationTitle = pc.ProgramTitle,
        //                        year = pc.Year,
        //                        institution = pc.Institution,
        //                        specialization = pc.Specialization,
        //                        currentValidity = pc.CurrentValidity
        //                    }).ToList();
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to get certificaton details.");
        //    }
        //}
        #endregion

        #region GetMembershipDetailsbyEmpId
        /// <summary>
        /// Get Member ship Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        //private List<MembershipDetails> GetMembershipDetailsbyEmpId(int empID)
        //{
        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            return (from ms in hrmsEntities.ProfessionalDetails
        //                    where (ms.EmployeeId == empID && ms.ProgramType == "MemberShip")
        //                    select new MembershipDetails
        //                    {
        //                        membershipTitle = ms.ProgramTitle,
        //                        year = ms.Year,
        //                        institution = ms.Institution,
        //                        specialization = ms.Specialization,
        //                        currentValidity = ms.CurrentValidity
        //                    }).ToList();
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to get membership details.");
        //    }
        //}
        #endregion

        #region GetEmploymentDetailsbyEmpId
        /// <summary>
        /// Get Employment Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<EmploymentDetails> GetEmploymentDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    return (from ed in hrmsEntities.PreviousEmploymentDetails
                            where ed.EmployeeId == empID
                            select new EmploymentDetails
                            {
                                name = ed.Name,
                                address = ed.Address,
                                designation = ed.Designation,
                                fromYear = SqlFunctions.DateName("day", ed.ServiceFrom) + "/" + SqlFunctions.StringConvert((double)ed.ServiceFrom.Month).TrimStart() + "/" + SqlFunctions.DateName("year", ed.ServiceFrom),
                                toYear = SqlFunctions.DateName("day", ed.ServiceTo) + "/" + SqlFunctions.StringConvert((double)ed.ServiceTo.Month).TrimStart() + "/" + SqlFunctions.DateName("year", ed.ServiceTo),
                                leavingReson = ed.LeavingReson
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get employement details.");
            }
        }
        #endregion

        #region GetProfRefDetailsbyEmpId
        /// <summary>
        /// Get ProfRef Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<ProfRefDetails> GetProfRefDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    return (from pr in hrmsEntities.ProfessionalReferences
                            where pr.EmployeeId == empID
                            select new ProfRefDetails
                            {
                                ID = pr.ID,
                                name = pr.Name,
                                designation = pr.Designation,
                                companyName = pr.CompanyName,
                                companyAddress = pr.CompanyAddress,
                                officeEmailAddress = pr.OfficeEmailAddress,
                                mobileNo = pr.MobileNo
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get professional references.");

            }
        }
        #endregion

        #region GetSkillDetailsbyEmpId
        /// <summary>
        /// Get Skill Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<EmployeeSkillDetails> GetSkillDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    return (from es in hrmsEntities.EmployeeSkills.AsNoTracking()
                            join skill in hrmsEntities.Skills.AsNoTracking() on es.SkillId equals skill.SkillId
                            join pl in hrmsEntities.ProficiencyLevels.AsNoTracking() on es.ProficiencyLevelId equals pl.ProficiencyLevelId
                            join ca in hrmsEntities.CompetencyAreas.AsNoTracking() on es.CompetencyAreaId equals ca.CompetencyAreaId
                            where es.EmployeeId == empID
                            select new EmployeeSkillDetails
                            {
                                ID = es.ID,
                                empID = es.EmployeeId,
                                skillID = es.SkillId,
                                SkillName = skill.SkillName,
                                experience = es.Experience,
                                proficiencyLevelId = es.ProficiencyLevel.ProficiencyLevelId,
                                ProficiencyLevel = pl.ProficiencyLevelCode,
                                CompetencyArea = ca.CompetencyAreaCode,
                                isPrimary = es.IsPrimary
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get skill details.");
            }
        }
        #endregion

        #region GetProjectDetailsbyEmpId
        /// <summary>
        /// Get Project Details by EmpId
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        private List<EmployeeProjectDetails> GetProjectDetailsbyEmpId(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    return (from ep in hrmsEntities.EmployeeProjects
                                /*join r in hrmsEntities.RoleMasters.AsNoTracking() on ep.RoleMasterId equals r.RoleMasterID
                                join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                from sufflist in suffixtemp.DefaultIfEmpty()
                                join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                from preflist in prefixtemp.DefaultIfEmpty()
                                join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID*/
                            where ep.EmployeeId == empID
                            select new EmployeeProjectDetails
                            {
                                organizationName = ep.OrganizationName,
                                projectName = ep.ProjectName,
                                duration = ep.Duration,
                                // roleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName,
                                keyAchievement = ep.KeyAchievements
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get project details.");
            }
        }
        #endregion

        #region PDFHRSection
        /// <summary>
        /// PDF HR Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="personalDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFHRSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, PersonalDetails personalDetails)
        {
            para = ParagraphContent("1.TO BE FILLED BY HR", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(2);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            pharse.Add(new Chunk("Associate ID No:", contentFontBold));
            cell = PhraseCell(pharse);
            table.AddCell(cell);

            pharse = new Phrase();
            pharse.Add(new Chunk(personalDetails.empCode, contentFont));
            cell = PhraseCell(pharse);
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Associate Email Address", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase(personalDetails.workEmailID, contentFont));
            table.AddCell(cell);
            table.AddCell(cell);

            return table;
        }
        #endregion

        #region PDFPersonalSection
        /// <summary>
        /// PDFPersonalSection
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="personalDetails"></param>
        /// <param name="contactDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFPersonalSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, PersonalDetails personalDetails, List<ContactDetails> contactDetails)
        {
            para = ParagraphContent("2.	PERSONAL DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(5);
            table.WidthPercentage = 100;

            cell = PhraseCell(new Phrase("First Name:", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.firstName, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Middle Name:", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.middleName, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Last Name:", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.lastName, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Gender", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.gender, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            string address = string.Empty;
            foreach (var addr in contactDetails)
            {
                if (addr.addressType.ToLower().Contains("current"))
                {
                    if (addr.currentAddress1 != null && addr.currentAddress1 != "")
                        address = addr.currentAddress1;
                    if (addr.currentAddress2 != null && addr.currentAddress2 != "")
                        address = address + "," + addr.currentAddress2;
                    if (addr.currentAddCity != null && addr.currentAddCity != "")
                        address = address + "," + addr.currentAddCity;
                    if (addr.currentAddState != null && addr.currentAddState != "")
                        address = address + "," + addr.currentAddState;
                    if (addr.currentAddZip != null && addr.currentAddZip != "")
                        address = address + "," + addr.currentAddZip;
                    if (addr.currentAddCountry != null && addr.currentAddCountry != "")
                        address = address + "," + addr.currentAddCountry;
                    address = address.TrimStart(',');
                    cell = PhraseCell(new Phrase("Current  Address:\n ", contentFontBold));
                    table.AddCell(cell);
                    cell = PhraseCell(new Phrase(address, contentFont));
                    cell.Colspan = 2;
                    table.AddCell(cell);
                }
            }
            cell = PhraseCell(new Phrase("Telephone No \n(with STD code)", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.phoneNumber, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            foreach (var addr in contactDetails)
            {
                if (addr.addressType.ToLower().Contains("permanent"))
                {
                    address = string.Empty;
                    cell = PhraseCell(new Phrase("Permanent Address:\n ", contentFontBold));
                    table.AddCell(cell);
                    if (addr.currentAddress1 != null && addr.currentAddress1 != "")
                        address = addr.currentAddress1;
                    if (addr.currentAddress2 != null && addr.currentAddress2 != "")
                        address = address + "," + addr.currentAddress2;
                    if (addr.currentAddCity != null && addr.currentAddCity != "")
                        address = address + "," + addr.currentAddCity;
                    if (addr.currentAddState != null && addr.currentAddState != "")
                        address = address + "," + addr.currentAddState;
                    if (addr.currentAddZip != null && addr.currentAddZip != "")
                        address = address + "," + addr.currentAddZip;
                    if (addr.currentAddCountry != null && addr.currentAddCountry != "")
                        address = address + "," + addr.currentAddCountry;
                    address = address.TrimStart(',');
                    cell = PhraseCell(new Phrase(address, contentFont));
                    cell.Colspan = 2;
                    table.AddCell(cell);
                }
            }
            cell = PhraseCell(new Phrase("Mobile No  ", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.mobileNo, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Personal Email Address", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.personalEmail, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Date of Birth", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.Birthdate, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Marital Status", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.maritalStatus, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Blood Group", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.bloodGroup, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Nationality", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.nationality, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("PAN Number", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.panNumber, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("PF Number", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.pfNumber, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Aadhar Number", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.aadharNumber, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("Passport Number", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.passportNumber, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);
            cell = PhraseCell(new Phrase("UAN Number", contentFontBold));
            table.AddCell(cell);
            cell = PhraseCell(new Phrase(personalDetails.uanNumber, contentFont));
            cell.Colspan = 2;
            table.AddCell(cell);

            return table;
        }
        #endregion

        #region PDFEmergencyContactSection
        /// <summary>
        /// PDF Emergency Contact Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="contactDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFEmergencyContactSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<EmergencyContactData> contactDetails)
        {
            para = ParagraphContent("3.	EMERGENCY CONTACT DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(2);
            table.WidthPercentage = 100;
            foreach (var contact in contactDetails)
            {
                if (contact.contactType == "PrimaryContact")
                {
                    pharse = new Phrase();
                    cell = PhraseCell(new Phrase("Primary Contact", contentFontBold));
                    cell.Colspan = 2;
                    table.AddCell(cell);
                }
                else
                {
                    pharse = new Phrase();
                    cell = PhraseCell(new Phrase("", contentFontBold));
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    pharse = new Phrase();
                    cell = PhraseCell(new Phrase("Other Contact", contentFontBold));
                    cell.Colspan = 2;
                    table.AddCell(cell);
                }
                pharse = new Phrase();
                cell = PhraseCell(new Phrase("Name :", contentFontBold));
                table.AddCell(cell);
                pharse.Add(new Chunk(contact.contactName, contentFont));
                cell = PhraseCell(pharse);
                table.AddCell(cell);

                pharse = new Phrase();
                cell = PhraseCell(new Phrase("Relationship :", contentFontBold));
                table.AddCell(cell);
                pharse.Add(new Chunk(contact.relationship, contentFont));
                cell = PhraseCell(pharse);
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Current Address :", contentFontBold));
                table.AddCell(cell);
                string emergencyPrimaryContactAddress = string.Empty;
                //contact.addressLine1 + ',' + contact.addressLine2 + ',' + contact.city + ',' + contact.state + ',' + contact.country;
                if (contact.addressLine1 != null && contact.addressLine1 != "")
                    emergencyPrimaryContactAddress = contact.addressLine1;
                if (contact.addressLine2 != null && contact.addressLine2 != "")
                    emergencyPrimaryContactAddress = emergencyPrimaryContactAddress + "," + contact.addressLine2;
                if (contact.city != null && contact.city != "")
                    emergencyPrimaryContactAddress = emergencyPrimaryContactAddress + "," + contact.city;
                if (contact.state != null && contact.state != "")
                    emergencyPrimaryContactAddress = emergencyPrimaryContactAddress + "," + contact.state;
                if (contact.zip != null && contact.zip != "")
                    emergencyPrimaryContactAddress = emergencyPrimaryContactAddress + "," + contact.zip;
                if (contact.country != null && contact.country != "")
                    emergencyPrimaryContactAddress = emergencyPrimaryContactAddress + "," + contact.country;

                cell = PhraseCell(new Phrase(emergencyPrimaryContactAddress, contentFont));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Mobile No. :", contentFontBold));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(contact.mobileNo, contentFont));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Telephone No. :", contentFontBold));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(contact.telephoneNo, contentFont));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Email Address:", contentFontBold));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(contact.emailAddress, contentFont));
                cell.Colspan = 2;
                table.AddCell(cell);
            }
            return table;
        }
        #endregion

        #region PDFFamilySection
        /// <summary>
        /// PDFFamilySection
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="familyDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFFamilySection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<RelationDetails> familyDetails)
        {
            para = ParagraphContent("4.	FAMILY DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(4);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            pharse.Add(new Chunk("Relationship", contentFontBold));
            cell = PhraseCell(pharse);
            table.AddCell(cell);

            pharse = new Phrase();
            pharse.Add(new Chunk("Name ", contentFontBold));
            cell = PhraseCell(pharse);
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Date of Birth", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Occupation", contentFontBold));
            table.AddCell(cell);

            foreach (var familyDetail in familyDetails)
            {
                cell = PhraseCell(new Phrase(familyDetail.relationship, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(familyDetail.name, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(familyDetail.birthDate, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(familyDetail.occupation, contentFont));
                table.AddCell(cell);
            }

            return table;
        }
        #endregion

        #region PDFEducationalSection
        /// <summary>
        /// PDF Educational Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="educationDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFEducationalSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<EducationDetails> educationDetails)
        {
            para = ParagraphContent("5.	EDUCATION DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(7);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            pharse.Add(new Chunk("Qualification", contentFontBold));

            cell = PhraseCell(pharse);
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Program Type", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Specialization ", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Completed Year", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Institute/ University", contentFontBold));
            table.AddCell(cell);           

            cell = PhraseCell(new Phrase("Grade", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Marks/Percentage", contentFontBold));
            table.AddCell(cell);

            foreach (var ed in educationDetails)
            {
                cell = PhraseCell(new Phrase(ed.qualificationName, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(ed.programType, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(ed.specialization, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(ed.completedYear, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(ed.institution, contentFont));
                table.AddCell(cell);               
                cell = PhraseCell(new Phrase(ed.grade, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(ed.marks, contentFont));
                table.AddCell(cell);
            }

            return table;
        }
        #endregion

        #region PDFCertificationSection
        /// <summary>
        /// PDF Certification Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="certificateDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFCertificationSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<ProfessionalDetails> certificateDetails)
        {
            para = ParagraphContent("6.	PROFESSIONAL CERTIFICATIONS DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(5);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            cell = PhraseCell(new Phrase("Certification Title", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Certification Year", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Certification Institution ", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Specialization", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Current Validity", contentFontBold));
            table.AddCell(cell);

            //foreach (var profCert in certificateDetails)
            //{
            //    cell = PhraseCell(new Phrase(profCert.certificationTitle, contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(profCert.year.ToString(), contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(profCert.institution, contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(profCert.specialization, contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(profCert.currentValidity, contentFont));
            //    table.AddCell(cell);
            //}

            return table;
        }
        #endregion

        #region PDFMemberShipSection
        /// <summary>
        /// PDF MemberShip Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="membershipDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFMemberShipSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse) //List<MembershipDetails> membershipDetails -- need to be passed as parameter
        {
            para = ParagraphContent("7.	PROFESSIONAL MEMBERSHIP DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(5);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            cell = PhraseCell(new Phrase("Membership Title", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Membership Year", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Institution", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Specialization", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Current Validity", contentFontBold));
            table.AddCell(cell);

            //foreach (var memData in membershipDetails)
            //{
            //    cell = PhraseCell(new Phrase(memData.membershipTitle, contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(memData.year.ToString(), contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(memData.institution, contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(memData.specialization, contentFont));
            //    table.AddCell(cell);
            //    cell = PhraseCell(new Phrase(memData.currentValidity, contentFont));
            //    table.AddCell(cell);
            //}

            return table;
        }
        #endregion

        #region PDFEmploymentSection
        /// <summary>
        /// PDF Employment Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="employmentDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFEmploymentSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<EmploymentDetails> employmentDetails)
        {
            para = ParagraphContent("6.	EMPLOYMENT DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(5);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            pharse.Add(new Chunk("Name & Address of Previous Employer", contentFontBold));

            cell = PhraseCell(pharse);
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Designation", contentFontBold));
            table.AddCell(cell);

            PdfPTable nested = new PdfPTable(2);
            nested.WidthPercentage = 100;
            cell = PhraseCell(new Phrase("Service Duration", contentFontBold));
            cell.Colspan = 2;
            nested.AddCell(cell);
            cell = PhraseCell(new Phrase("From", contentFontBold));
            nested.AddCell(cell);
            cell = PhraseCell(new Phrase("To", contentFontBold));
            nested.AddCell(cell);
            cell = new PdfPCell(nested);
            cell.Colspan = 2;
            table.AddCell(cell);

            //cell = PhraseCell(new Phrase("Last Drawn Salary", contentFontBold));
            //table.AddCell(cell);

            cell = PhraseCell(new Phrase("Reason for Leaving", contentFontBold));
            table.AddCell(cell);

            foreach (var prevEmpDetails in employmentDetails)
            {
                cell = PhraseCell(new Phrase(prevEmpDetails.name + "\n" + prevEmpDetails.address, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(prevEmpDetails.designation, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(prevEmpDetails.fromYear, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(prevEmpDetails.toYear, contentFont));
                table.AddCell(cell);
                //cell = PhraseCell(new Phrase(prevEmpDetails.lastDrawnSalary.ToString(), contentFont));
                //table.AddCell(cell);
                cell = PhraseCell(new Phrase(prevEmpDetails.leavingReson, contentFont));
                table.AddCell(cell);
            }

            return table;
        }
        #endregion

        #region PDFSkillSection
        /// <summary>
        /// PDF Skill Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="employeeSkillDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFSkillSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<EmployeeSkillDetails> employeeSkillDetails)
        {
            para = ParagraphContent("7.	SKILL DETAILS", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(4);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            cell = PhraseCell(new Phrase("Skill / Expertize Title", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Domain", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Experience (In months)", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Proficiency Level", contentFontBold));
            table.AddCell(cell);

            foreach (var skill in employeeSkillDetails)
            {
                cell = PhraseCell(new Phrase(skill.SkillName.ToString(), contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(skill.CompetencyArea.ToString(), contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(skill.experience.ToString(), contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(skill.ProficiencyLevel.ToString(), contentFont));
                table.AddCell(cell);
            }

            return table;
        }
        #endregion

        #region PDFProjectSection
        /// <summary>
        /// PDF Project Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="projectDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFProjectSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<EmployeeProjectDetails> projectDetails)
        {
            para = ParagraphContent("8.	PROJECT DETAILS (Worked in last 4 years)", contentFontBold, spacingAfter: 20);
            document.Add(para);

            table = new PdfPTable(4);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            cell = PhraseCell(new Phrase("Organization Name", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Project Name", contentFontBold));
            table.AddCell(cell);

            //cell = PhraseCell(new Phrase("Role", contentFontBold));
            //table.AddCell(cell);

            cell = PhraseCell(new Phrase("Duration (Months)", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Key Achievements", contentFontBold));
            table.AddCell(cell);

            foreach (var projcts in projectDetails)
            {
                cell = PhraseCell(new Phrase(projcts.organizationName, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(projcts.projectName, contentFont));
                table.AddCell(cell);
                //cell = PhraseCell(new Phrase(projcts.roleName, contentFont));
                //table.AddCell(cell);
                cell = PhraseCell(new Phrase(projcts.duration.ToString(), contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(projcts.keyAchievement, contentFont));
                table.AddCell(cell);
            }

            return table;
        }
        #endregion

        #region PDFProfessionalReferencesSection
        /// <summary>
        /// PDF Professional References Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <param name="profRefDetails"></param>
        /// <returns></returns>
        private PdfPTable PDFProfessionalReferencesSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse, List<ProfRefDetails> profRefDetails)
        {
            para = ParagraphContent("9.	PROFESSIONAL REFERENCES", contentFontBold);
            document.Add(para);

            para = ParagraphContent(@"Please provide details of two professional references from the previous organization where you were employed:", contentFont);
            document.Add(para);

            para = ParagraphContent(@"1. Your Reporting Manager or Department Head      2. HR Manager", contentFont, spacingBefore: 0, spacingAfter: 10);
            document.Add(para);

            table = new PdfPTable(5);
            table.WidthPercentage = 100;

            pharse = new Phrase();
            cell = PhraseCell(new Phrase("Name", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Designation Name", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Company Name & Address", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Official Email Address", contentFontBold));
            table.AddCell(cell);

            cell = PhraseCell(new Phrase("Contact Number", contentFontBold));
            table.AddCell(cell);

            foreach (var profRef in profRefDetails)
            {
                cell = PhraseCell(new Phrase(profRef.name, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(profRef.designation, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(profRef.companyName + "\n" + profRef.companyAddress, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(profRef.officeEmailAddress, contentFont));
                table.AddCell(cell);
                cell = PhraseCell(new Phrase(profRef.mobileNo, contentFont));
                table.AddCell(cell);
            }

            return table;
        }
        #endregion

        #region PDFDeclarationSection
        /// <summary>
        /// PDF Declaration Section
        /// </summary>
        /// <param name="document"></param>
        /// <param name="para"></param>
        /// <param name="contentFontBold"></param>
        /// <param name="contentFont"></param>
        /// <param name="table"></param>
        /// <param name="cell"></param>
        /// <param name="pharse"></param>
        /// <returns></returns>
        private Document PDFDeclarationSection(Document document, Paragraph para, Font contentFontBold, Font contentFont, PdfPTable table, PdfPCell cell, Phrase pharse)
        {
            para = ParagraphContent(@"10.	DECLARATION AND AUTHORIZATION:", contentFontBold);
            document.Add(para);

            para = ParagraphContent(@"I hereby authorize Seneca Global (or a third party agent appointed by the organization) to carry out Pre-employment Screening and permission to access any appropriate databases. I authorize former employers, agencies, education institutes etc. to release any information pertaining to my employment / education and I release them from any liability in doing so.", contentFont);
            document.Add(para);

            para = ParagraphContent(@"I confirm that the information furnished above is correct and I understand that any misrepresentation of information in this Associates On boarding form may result in my employment being terminated.", contentFont, spacingAfter: 20);
            document.Add(para);

            para = ParagraphContent("Signature: ________________________", contentFontBold);
            document.Add(para);

            para = ParagraphContent("Name (in Capital Letters): ____________________________________________", contentFontBold);
            document.Add(para);

            para = ParagraphContent("Date: ____________________________", contentFontBold);
            document.Add(para);

            return document;
        }
        #endregion

        #region PhraseCell
        /// <summary>
        /// PhraseCell
        /// </summary>
        /// <param name="phrase"></param>
        /// <param name="align"></param>
        /// <returns></returns>
        private static PdfPCell PhraseCell(Phrase phrase, int align = Element.ALIGN_LEFT)
        {
            PdfPCell cell = new PdfPCell(phrase);
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingLeft = 7f;
            cell.PaddingRight = 7f;
            cell.PaddingBottom = 7f;
            cell.PaddingTop = 7f;
            return cell;
        }
        #endregion

        #region ParagraphContent
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="contentFont"></param>
        /// <param name="align"></param>
        /// <param name="spacingBefore"></param>
        /// <param name="spacingAfter"></param>
        /// <returns></returns>
        private static Paragraph ParagraphContent(string content, Font contentFont, int align = Element.ALIGN_JUSTIFIED, int spacingBefore = 10, int spacingAfter = 0)
        {
            Paragraph paragraph = new Paragraph();
            paragraph.SpacingBefore = spacingBefore;
            paragraph.SpacingAfter = spacingAfter;
            paragraph.Font = contentFont;
            paragraph.Alignment = align;
            paragraph.Add(content);
            return paragraph;
        }
        #endregion

        #endregion

        #endregion

        #region Reports

        #region GetUtilizeReports
        /// <summary>
        /// GetUtilizeReports
        /// </summary>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        public IEnumerable<object> GetUtilizeReports(SearchFilter searchFilter)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var result = (from allocation in hrmsEntities.AssociateAllocations.AsNoTracking()
                                  join allocationPercentage in hrmsEntities.AllocationPercentages.AsNoTracking() on allocation.AllocationPercentage equals allocationPercentage.AllocationPercentageID
                                  join rrd in hrmsEntities.RequisitionRoleDetails.AsNoTracking() on allocation.TRId equals rrd.TRId into rrdetails
                                  from rdetails in rrdetails.DefaultIfEmpty()
                                  join project in hrmsEntities.Projects.AsNoTracking() on allocation.ProjectId equals project.ProjectId
                                  join projectmgrs in hrmsEntities.ProjectManagers.AsNoTracking() on project.ProjectId equals projectmgrs.ProjectID
                                  join emp in hrmsEntities.Employees.AsNoTracking() on allocation.EmployeeId equals emp.EmployeeId
                                  join rmgr in hrmsEntities.Employees.AsNoTracking() on allocation.ReportingManagerId equals rmgr.EmployeeId
                                  join pmgr in hrmsEntities.Employees.AsNoTracking() on projectmgrs.ProgramManagerID equals pmgr.EmployeeId
                                  join lead in hrmsEntities.Employees.AsNoTracking() on projectmgrs.ReportingManagerID equals lead.EmployeeId into techlead
                                  from tl in techlead.DefaultIfEmpty()
                                  join client in hrmsEntities.Clients.AsNoTracking() on project.ClientId equals client.ClientId
                                  join es in hrmsEntities.EmployeeSkills.AsNoTracking() on emp.EmployeeId equals es.EmployeeId into tempempSkills
                                  from empskills in tempempSkills.DefaultIfEmpty()
                                  join skills in hrmsEntities.Skills.AsNoTracking() on empskills.SkillId equals skills.SkillId
                                  join prtype in hrmsEntities.SkillGroups.AsNoTracking() on skills.SkillGroupId equals prtype.SkillGroupId
                                  join ca in hrmsEntities.CompetencyAreas.AsNoTracking() on empskills.CompetencyAreaId equals ca.CompetencyAreaId
                                  join role in hrmsEntities.RoleMasters.AsNoTracking() on allocation.RoleMasterId equals role.RoleMasterID
                                  join grade in hrmsEntities.Grades.AsNoTracking() on emp.GradeId equals grade.GradeId into tempgrades
                                  from Employees in tempgrades.DefaultIfEmpty()
                                  join designation in hrmsEntities.Designations.AsNoTracking() on emp.Designation equals designation.DesignationId into designations
                                  from desig in designations.DefaultIfEmpty()
                                  where allocation.IsActive == true && emp.IsActive == true && project.IsActive == true && client.IsActive == true && empskills.IsActive == true
                                  && ca.IsActive == true && role.IsActive == true
                                  select new
                                  {
                                      AllocationId = allocation.AssociateAllocationId,
                                      AssociateCode = emp.EmployeeCode,
                                      AssociateId = emp.EmployeeId,
                                      AssociateName = emp.FirstName + " " + emp.LastName,
                                      RMFirstName = rmgr.FirstName,
                                      RMLastName = rmgr.LastName,
                                      PMFirstName = pmgr.FirstName,
                                      PMLastName = pmgr.LastName,
                                      LeadFirstName = tl.FirstName,
                                      LeadLastName = tl.LastName,
                                      Experience = emp.Experience,
                                      JoinDate = emp.JoinDate,
                                      //Designation = role.RoleDescription,
                                      Designation = desig.DesignationName,
                                      roleID = (int?)role.RoleMasterID,
                                      Grade = Employees.GradeName,
                                      GradeID = (int?)emp.GradeId,
                                      CompetencyGroup = emp.CompetencyGroup,
                                      projectTypeId = (int?)prtype.SkillGroupId,
                                      TechnicalCompetency = prtype.SkillGroupName,
                                      Client = client.ClientName,
                                      ClientID = (int?)client.ClientId,
                                      ProjectId = (int?)project.ProjectId,
                                      ProjectName = project.ProjectName,
                                      PercentUtilization = allocationPercentage.Percentage,
                                      Billable = (allocation.IsBillable == null) ? false : allocation.IsBillable,
                                      Critical = (allocation.IsCritical == null) ? false : allocation.IsCritical,
                                      SkillId = (int?)skills.SkillId,
                                      PrimarySkill = skills.SkillName,
                                      CompetencyArea = ca.CompetencyAreaCode,
                                      competencyAreaId = (int?)ca.CompetencyAreaId,
                                      leadId = projectmgrs.ReportingManagerID,
                                      managerId = projectmgrs.ProgramManagerID,
                                      reportingManagerId = emp.ReportingManager
                                  }).OrderBy(x => x.AssociateName).ToList();

                    if (searchFilter.AssociateId > 0)
                        result = result.Where(i => i.AssociateId == searchFilter.AssociateId).ToList();
                    if (!string.IsNullOrEmpty(searchFilter.AssociateName))
                        result = result.Where(i => i.AssociateName.ToUpper().Contains(searchFilter.AssociateName.ToUpper())).ToList();
                    if (searchFilter.ProjectId != 0)
                        result = result.Where(i => i.ProjectId == searchFilter.ProjectId).ToList();
                    if (searchFilter.GradeId != 0)
                        result = result.Where(i => i.GradeID == searchFilter.GradeId).ToList();
                    if (searchFilter.RoleId != 0)
                        result = result.Where(i => i.roleID == searchFilter.RoleId).ToList();
                    if (searchFilter.CompetencyId != 0)
                        result = result.Where(i => i.projectTypeId == searchFilter.CompetencyId).ToList();
                    if (searchFilter.SkillId != 0)
                        result = result.Where(i => i.SkillId == searchFilter.SkillId).ToList();
                    if (searchFilter.ClientId != 0)
                        result = result.Where(i => i.ClientID == searchFilter.ClientId).ToList();
                    if (searchFilter.ManagerId != 0)
                        result = result.Where(i => i.managerId == searchFilter.ManagerId).ToList();
                    if (searchFilter.Utilization != 0)
                        result = result.Where(i => i.PercentUtilization == searchFilter.Utilization).ToList();

                    if (searchFilter.IsBillable == null)
                    {
                        result = result.Where(i => (i.Billable.Equals(true) || (i.Billable.Equals(false)) || (i.Billable.Equals(null)))).ToList();
                    }
                    else if (searchFilter.IsBillable.Equals("True"))
                        result = result.Where(i => i.Billable == true).ToList();
                    else if (searchFilter.IsBillable.Equals("False"))
                        result = result.Where(i => i.Billable == false).ToList();
                    if (searchFilter.IsCritical == null)
                    {
                        result = result.Where(i => (i.Critical.Equals(true) || (i.Critical.Equals(false)) || (i.Critical.Equals(null)))).ToList();
                    }
                    else if (searchFilter.IsCritical.Equals("True"))
                        result = result.Where(i => i.Critical == true).ToList();
                    else if (searchFilter.IsCritical.Equals("False"))
                        result = result.Where(i => i.Critical == false).ToList();


                    var asssociatesUtilizaton = (from row in result.ToList()
                                                 group row by new
                                                 {
                                                     row.AllocationId,
                                                     row.AssociateId,
                                                     row.AssociateCode,
                                                     row.AssociateName,
                                                     row.managerId,
                                                     row.leadId,
                                                     row.Experience,
                                                     row.JoinDate,
                                                     row.Designation,
                                                     row.Grade,
                                                     row.CompetencyArea,
                                                     row.TechnicalCompetency,
                                                     row.Client,
                                                     row.ProjectName,
                                                     row.PercentUtilization,
                                                     row.Billable,
                                                     row.Critical,
                                                     row.reportingManagerId,
                                                     row.PMFirstName,
                                                     row.PMLastName,
                                                     row.RMFirstName,
                                                     row.RMLastName,
                                                     row.LeadFirstName,
                                                     row.LeadLastName
                                                 } into tempGroup
                                                 select new AssociateReport
                                                 {
                                                     AllocationId = tempGroup.Key.AllocationId,
                                                     AssociateCode = tempGroup.Key.AssociateCode,
                                                     AssociateId = tempGroup.Key.AssociateId,
                                                     AssociateName = tempGroup.Key.AssociateName,
                                                     ProgramManager = tempGroup.Key.PMFirstName + " " + tempGroup.Key.PMLastName,
                                                     ReportingManager = tempGroup.Key.RMFirstName + " " + tempGroup.Key.RMLastName,
                                                     TechnicalLead = tempGroup.Key.LeadFirstName + " " + tempGroup.Key.LeadLastName,
                                                     JoinDate = tempGroup.Key.JoinDate,
                                                     DateofJoining = string.Format("{0:dd/MM/yyyy}", tempGroup.Key.JoinDate),
                                                     Experience = tempGroup.Key.Experience,
                                                     Designation = string.IsNullOrEmpty(tempGroup.Key.Designation) ? string.Empty : tempGroup.Key.Designation,
                                                     Grade = tempGroup.Key.Grade,
                                                     PrimarySkill = string.Join(";", (tempGroup.Select(i => i.PrimarySkill).Distinct())),
                                                     CompetencyArea = tempGroup.Key.CompetencyArea,
                                                     TechnicalCompetency = tempGroup.Key.TechnicalCompetency,
                                                     Client = tempGroup.Key.Client,
                                                     ProjectName = string.Join(";", tempGroup.Key.ProjectName),
                                                     Utilization = (int)tempGroup.Key.PercentUtilization,
                                                     Billable = tempGroup.Key.Billable == true ? true : false,
                                                     Critical = tempGroup.Key.Critical == true ? true : false,
                                                     IsBillable = tempGroup.Key.Billable == true ? "Yes" : "No",
                                                     IsCritical = tempGroup.Key.Critical == true ? "Yes" : "No",
                                                 }).OrderBy(x => x.AssociateName).ToList();

                    return CalculateExperience(asssociatesUtilizaton.ToList(), searchFilter.Experience);
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }

        }
        #endregion

        #region CalculateExperience
        /// <summary>
        /// Calculate Experience
        /// </summary>
        /// <param name="asssociatesUtilizaton"></param>
        /// <param name="Searchexperience"></param>
        /// <returns></returns>
        public List<AssociateReport> CalculateExperience(IEnumerable<AssociateReport> asssociatesUtilizaton, string Searchexperience)
        {
            List<AssociateReport> finalList = new List<AssociateReport>();
            DateTime endDate = DateTime.Now;
            decimal? totalexp = 0, prevExperience = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    foreach (var s in asssociatesUtilizaton)
                    {
                        if (s.JoinDate.HasValue)
                        {
                            double expericeAsofNow = ((endDate - s.JoinDate).Value.TotalDays) / 365;
                            prevExperience = (decimal)expericeAsofNow;
                            totalexp = s.Experience != null ? (prevExperience + s.Experience.Value) : prevExperience;
                            string result = String.Format("{0:0.0}", Math.Round((decimal)totalexp, 1));
                            totalexp = decimal.Parse(result);
                        }

                        AssociateReport associateReport = new AssociateReport();
                        associateReport.AllocationId = s.AllocationId;
                        associateReport.AssociateId = s.AssociateId;
                        associateReport.AssociateCode = s.AssociateCode;
                        associateReport.AssociateName = s.AssociateName;
                        associateReport.ProgramManager = s.ProgramManager;
                        associateReport.ReportingManager = s.ReportingManager;
                        associateReport.TechnicalLead = s.TechnicalLead;
                        associateReport.JoinDate = s.JoinDate;
                        associateReport.DateofJoining = s.DateofJoining;
                        associateReport.Experience = totalexp != 0 ? totalexp : s.Experience == null ? 0 : decimal.Parse(String.Format("{0:0.0}", s.Experience));
                        var Exp = totalexp != 0 ? totalexp : s.Experience == null ? 0 : decimal.Parse(String.Format("{0:0.0}", s.Experience));
                        var expRange = Exp > 0 ? Convert.ToInt32(Exp.ToString().Split('.')[1]) > 0 ? Exp.ToString().Split('.')[0] + "-" + (Convert.ToInt32(Exp.ToString().Split('.')[0]) + 1) : Exp.ToString() : Exp.ToString();
                        associateReport.ExpRange = expRange;
                        associateReport.Designation = s.Designation;
                        associateReport.Grade = s.Grade;
                        associateReport.PrimarySkill = s.PrimarySkill;
                        associateReport.CompetencyArea = s.CompetencyArea;
                        associateReport.Client = s.Client;
                        associateReport.ProjectName = s.ProjectName;
                        associateReport.Utilization = s.Utilization;
                        associateReport.Billable = s.Billable;
                        associateReport.Critical = s.Critical;
                        associateReport.IsBillable = s.IsBillable;
                        associateReport.IsCritical = s.IsCritical;
                        associateReport.TechnicalCompetency = s.TechnicalCompetency;
                        finalList.Add(associateReport);
                    }

                    if (Searchexperience != "" && Searchexperience != null)
                        finalList = finalList.Where(i => i.ExpRange == (Searchexperience == "0-1" ? "0" : Searchexperience)).ToList();
                }
            }
            catch
            {
                throw;
            }
            return finalList;
        }
        #endregion

        #region SaveReportData
        /// <summary>
        /// SaveReportData
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        public bool SaveReportData(List<AssociateReport> associateAllocationList)
        {
            bool isValid = false;

            try
            {
                string projectTypeCode = new ProjectDetails().GetEnumDescription(Enumeration.ProjectTypes.TalentPool);
                var poolProjects = (from a in associateAllocationList
                                    where a.ProjectName.Contains(projectTypeCode)
                                    select new { empId = a.AssociateId }).FirstOrDefault();

                if (poolProjects != null)
                    throw new AssociatePortalException("Talent pool allocation of the associate with ID: {0} cannot be updated directly", poolProjects.empId);

                //Allocate associate 
                //
                using (APEntities hrmsEntities = new APEntities())
                {
                    foreach (AssociateReport objAssociateAllocation in associateAllocationList)
                    {
                        AssociateAllocation associateAllocation = hrmsEntities.AssociateAllocations.Where(id => id.AssociateAllocationId == objAssociateAllocation.AllocationId).FirstOrDefault();

                        //To fetch Talent Pool project based on the associate's competency group (domain)
                        var project = (from emp in hrmsEntities.Employees.AsNoTracking()
                                       join parea in hrmsEntities.TalentPools on emp.CompetencyGroup equals parea.PracticeAreaId into pareagroup
                                       from pgroup in pareagroup.DefaultIfEmpty()
                                       join proj in hrmsEntities.Projects on pgroup.ProjectId equals proj.ProjectId
                                       join projectmgrs in hrmsEntities.ProjectManagers on proj.ProjectId equals projectmgrs.ProjectID
                                       where (emp.EmployeeId == associateAllocation.EmployeeId && pgroup.IsActive == true
                                       && proj.IsActive == true && emp.IsActive == true)
                                       select new
                                       {
                                           EmployeeCode = emp.EmployeeCode,
                                           ProjectId = pgroup.ProjectId,
                                           ProjectName = proj.ProjectName,
                                           ReportingManagerId = projectmgrs.ReportingManagerID
                                       }).FirstOrDefault();

                        //Check whether an associate's total PercentUtilization exceeding 100
                        decimal? recievedPercentage = associateAllocationList.Where(i => i.AssociateCode == associateAllocation.Employee.EmployeeCode && i.ProjectName != project.ProjectName).Sum(total => total.Utilization);
                        if (recievedPercentage > 100)
                            throw new AssociatePortalException("This allocation can not be done, over all utilization percentage exceeding 100 for the associate with ID: {0}", associateAllocation.Employee.EmployeeCode);

                        if (objAssociateAllocation.ProjectName != project.ProjectName)
                        {
                            if (objAssociateAllocation.Billable != associateAllocation.IsBillable || objAssociateAllocation.Utilization != associateAllocation.AllocationPercentage || objAssociateAllocation.Critical != associateAllocation.IsCritical)
                            {
                                var existingAllocation = (from allocation in hrmsEntities.AssociateAllocations.AsNoTracking()
                                                          where (allocation.ProjectId != associateAllocation.ProjectId && allocation.ProjectId != project.ProjectId
                                                          && allocation.EmployeeId == associateAllocation.EmployeeId && allocation.IsActive == true)
                                                          group allocation by new { allocation.EmployeeId } into temp
                                                          select new
                                                          {
                                                              percentage = temp.Sum(a => a.AllocationPercentage)
                                                          }).FirstOrDefault();

                                if (existingAllocation != null)
                                {
                                    var pids = (from a in associateAllocationList
                                                join p in hrmsEntities.Projects.AsNoTracking().AsEnumerable() on a.ProjectName equals p.ProjectName
                                                select new { projectId = p.ProjectId, projectName = p.ProjectName }).ToList();

                                    var check = (from a in hrmsEntities.AssociateAllocations.AsEnumerable()
                                                 where !pids.Any(p => p.projectId == a.ProjectId)
                                                 && a.IsActive == true && a.EmployeeId == associateAllocation.EmployeeId && a.ProjectId != project.ProjectId
                                                 select new { Id = a.AssociateAllocationId, ProjectId = a.ProjectId }).ToList();

                                    if ((objAssociateAllocation.Utilization + existingAllocation.percentage) > 100 && check.Count > 0)
                                        throw new AssociatePortalException("This allocation can not be done, over all allocation percentage exceeding 100 for the associate with ID: {0}", project.EmployeeCode);
                                }

                                AssociateAllocation oldPoolAllocation = hrmsEntities.AssociateAllocations.Where(i => i.EmployeeId == associateAllocation.EmployeeId && i.ProjectId == project.ProjectId && i.IsActive == true).FirstOrDefault();
                                if (oldPoolAllocation != null && objAssociateAllocation.Utilization != associateAllocation.AllocationPercentage)
                                {
                                    oldPoolAllocation.ModifiedBy = objAssociateAllocation.CurrentUser;
                                    oldPoolAllocation.ModifiedDate = DateTime.Now;
                                    oldPoolAllocation.SystemInfo = objAssociateAllocation.SystemInfo;
                                    oldPoolAllocation.IsActive = false;
                                    oldPoolAllocation.ReleaseDate = DateTime.Now;
                                    hrmsEntities.Entry(oldPoolAllocation).State = System.Data.Entity.EntityState.Modified;
                                }
                                decimal? newPoolPercentage = objAssociateAllocation.Utilization + (existingAllocation != null ? existingAllocation.percentage : 0);
                                if ((newPoolPercentage < 100 || newPoolPercentage + (oldPoolAllocation == null ? 0 : oldPoolAllocation.AllocationPercentage) < 100) && (100 - newPoolPercentage != 0) && recievedPercentage < 100)
                                {
                                    //For Talent Pool allocation
                                    AssociateAllocation poolAllocation = new AssociateAllocation();
                                    poolAllocation.TRId = associateAllocation.TRId;
                                    poolAllocation.ProjectId = project.ProjectId;
                                    poolAllocation.ReportingManagerId = project.ReportingManagerId;
                                    poolAllocation.EmployeeId = associateAllocation.EmployeeId;
                                    poolAllocation.RoleMasterId = associateAllocation.RoleMasterId;
                                    poolAllocation.IsActive = true;
                                    poolAllocation.AllocationDate = DateTime.Now;
                                    poolAllocation.CreatedBy = objAssociateAllocation.CurrentUser;
                                    poolAllocation.CreateDate = DateTime.Now;
                                    poolAllocation.SystemInfo = objAssociateAllocation.SystemInfo;
                                    poolAllocation.EffectiveDate = DateTime.Now.AddDays(1);
                                    poolAllocation.AllocationDate = DateTime.Now;
                                    poolAllocation.AllocationPercentage = existingAllocation != null ? (100 - (objAssociateAllocation.Utilization + existingAllocation.percentage)) : (100 - objAssociateAllocation.Utilization);
                                    poolAllocation.ClientBillingPercentage = poolAllocation.ClientBillingPercentage;
                                    poolAllocation.IsCritical = false;
                                    poolAllocation.IsBillable = false;
                                    hrmsEntities.AssociateAllocations.Add(poolAllocation);
                                }
                            }
                            else
                            {
                                throw new AssociatePortalException("Nothing was mentioned to update for the associate with ID: {0} - project {1}, Please mention what to update!", project.EmployeeCode, objAssociateAllocation.ProjectName);
                            }
                            AssociateAllocation newAllocation = new AssociateAllocation();
                            newAllocation.TRId = associateAllocation.TRId;
                            newAllocation.ProjectId = associateAllocation.ProjectId;
                            newAllocation.EmployeeId = associateAllocation.EmployeeId;
                            newAllocation.RoleMasterId = associateAllocation.RoleMasterId;
                            newAllocation.IsActive = true;
                            newAllocation.AllocationDate = DateTime.Now;
                            newAllocation.CreatedBy = objAssociateAllocation.CurrentUser;
                            newAllocation.CreateDate = DateTime.Now;
                            newAllocation.SystemInfo = objAssociateAllocation.SystemInfo;
                            newAllocation.ReportingManagerId = associateAllocation.ReportingManagerId;
                            newAllocation.EffectiveDate = DateTime.Now.AddDays(1);
                            newAllocation.AllocationDate = DateTime.Now;
                            newAllocation.AllocationPercentage = objAssociateAllocation.Utilization;
                            newAllocation.InternalBillingPercentage = objAssociateAllocation.Utilization;
                            newAllocation.IsPrimary = associateAllocation.IsPrimary;
                            newAllocation.IsCritical = objAssociateAllocation.Billable == true ? true : objAssociateAllocation.Critical == null ? false : objAssociateAllocation.Critical;
                            newAllocation.IsBillable = objAssociateAllocation.Billable == null ? false : objAssociateAllocation.Billable;
                            hrmsEntities.AssociateAllocations.Add(newAllocation);

                            associateAllocation.IsActive = false;
                            associateAllocation.ReleaseDate = DateTime.Now;
                            associateAllocation.ModifiedBy = objAssociateAllocation.CurrentUser;
                            associateAllocation.ModifiedDate = DateTime.Now;
                            isValid = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                        else
                        {
                            throw new AssociatePortalException("Talent pool allocation of the associate with ID: {0} cannot be updated directly", project.EmployeeCode);
                        }
                    }
                }
                return isValid;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetActiveSkillsData
        /// <summary>
        /// Get active and approved Skill details from system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetActiveSkillsData()
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skillData = (from s in hrmEntities.Skills
                                     where s.IsActive == true && s.IsApproved == true
                                     select new { s.SkillId, s.SkillCode, s.SkillDescription, s.IsActive, s.IsApproved, s.SkillName }).OrderBy(x => x.SkillName).ToList();

                    if (skillData.Count > 0)
                        return skillData;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetFinanceReport
        /// <summary>
        /// GetFinanceReport
        /// </summary>
        /// <param name="searchFilter"></param>
        public IEnumerable<object> GetFinanceReport(SearchFilter searchFilter)
        {
            try
            {
                if (searchFilter.DateFrom != null && searchFilter.DateTo != null)
                {
                    DateTime fromDate = Commons.GetDateTimeInIST(searchFilter.DateFrom);
                    DateTime toDate = Commons.GetDateTimeInIST(searchFilter.DateTo);

                    if (fromDate > toDate)
                        throw new AssociatePortalException("From Date should be less than or equal to To Date!");

                    using (APEntities hrmsEntities = new APEntities())
                    {
                        var result = (from allocation in hrmsEntities.AssociateAllocations.AsNoTracking()
                                      join clientrole in hrmsEntities.ClientBillingRoles on allocation.ClientBillingRoleId equals clientrole.ClientBillingRoleId into crolegrp
                                      from clientrolelist in crolegrp.DefaultIfEmpty()
                                      join internalrole in hrmsEntities.InternalBillingRoles on allocation.InternalBillingRoleId equals internalrole.InternalBillingRoleId into Irolegrp
                                      from internalrolelist in Irolegrp.DefaultIfEmpty()
                                      join role in hrmsEntities.RoleMasters.AsNoTracking() on allocation.RoleMasterId equals role.RoleMasterID
                                      join project in hrmsEntities.Projects.AsNoTracking() on allocation.ProjectId equals project.ProjectId
                                      join projectmgrs in hrmsEntities.ProjectManagers.AsNoTracking() on project.ProjectId equals projectmgrs.ProjectID
                                      join prtype in hrmsEntities.PracticeAreas.AsNoTracking() on project.PracticeAreaId equals prtype.PracticeAreaId
                                      join client in hrmsEntities.Clients.AsNoTracking() on project.ClientId equals client.ClientId
                                      join emp in hrmsEntities.Employees.AsNoTracking() on allocation.EmployeeId equals emp.EmployeeId
                                      join rmgr in hrmsEntities.Employees.AsNoTracking() on allocation.ReportingManagerId equals rmgr.EmployeeId
                                      join pmgr in hrmsEntities.Employees.AsNoTracking() on projectmgrs.ReportingManagerID equals pmgr.EmployeeId
                                      join parea in hrmsEntities.PracticeAreas.AsNoTracking() on emp.CompetencyGroup equals parea.PracticeAreaId into practiceareagrp
                                      from pgroup in practiceareagrp.DefaultIfEmpty()
                                      join grade in hrmsEntities.Grades.AsNoTracking() on emp.GradeId equals grade.GradeId into tempgrades
                                      from g in tempgrades.Where(i => i.IsActive == true).DefaultIfEmpty()
                                      join dept in hrmsEntities.Departments.AsNoTracking() on emp.DepartmentId equals dept.DepartmentId into tempdept
                                      from deptlist in tempdept.Where(j => j.IsActive == true).DefaultIfEmpty()
                                      join designation in hrmsEntities.Designations.AsNoTracking() on emp.Designation equals designation.DesignationId into designations
                                      from desig in designations.DefaultIfEmpty()
                                      where role.IsActive == true && project.IsActive == true
                                     && (DbFunctions.TruncateTime(allocation.EffectiveDate) <= DbFunctions.TruncateTime(toDate))
                                    && ((DbFunctions.TruncateTime(allocation.ReleaseDate) >= fromDate && DbFunctions.TruncateTime(allocation.ReleaseDate) <= toDate)
                                    || (allocation.ReleaseDate == null || allocation.IsActive != false))
                                      select new
                                      {
                                          AssociateAllocationId = allocation.AssociateAllocationId,
                                          AssociateID = emp.EmployeeCode,
                                          FirstName = emp.FirstName,
                                          LastName = emp.LastName,
                                          RMFirstName = rmgr.FirstName,
                                          RMLastName = rmgr.LastName,
                                          PMFirstName = pmgr.FirstName,
                                          PMLastName = pmgr.LastName,
                                          //Designation = role.RoleDescription,
                                          Designation = desig.DesignationName,
                                          roleID = (int?)role.RoleMasterID,
                                          Grade = g.GradeName,
                                          GradeID = (int?)emp.GradeId,
                                          CompetencyGroup = pgroup.PracticeAreaCode,
                                          //ProjectGroup = prtype.ProjectTypeCode,
                                          Client = client.ClientName,
                                          ClientID = (int?)client.ClientId,
                                          ProjectId = (int?)project.ProjectId,
                                          ProjectName = project.ProjectName,
                                          Billable = allocation.IsBillable,
                                          IsBillable = allocation.IsBillable,
                                          IsCritical = allocation.IsCritical,
                                          BillablePercentage = allocation.ClientBillingPercentage,
                                          PercentUtilization = allocation.AllocationPercentage,
                                          Critical = allocation.IsCritical,
                                          managerId = projectmgrs.ProgramManagerID,
                                          reportingManagerId = emp.ReportingManager,
                                          leadId = projectmgrs.ReportingManagerID,
                                          DateFrom = DbFunctions.TruncateTime(fromDate) > DbFunctions.TruncateTime(allocation.EffectiveDate) ? fromDate.ToString() : allocation.EffectiveDate.ToString(),
                                          FromDate = DbFunctions.TruncateTime(fromDate) > DbFunctions.TruncateTime(allocation.EffectiveDate) ? fromDate : allocation.EffectiveDate,
                                          ToDate = DbFunctions.TruncateTime(toDate) > DbFunctions.TruncateTime(allocation.ReleaseDate) ? allocation.ReleaseDate : toDate,
                                          ClientBillingRoleId = (int?)allocation.ClientBillingRoleId,
                                          //ClientBillingRoleCode = clientrolelist.ClientBillingRoleCode,
                                          ClientBillingRoleName = clientrolelist.ClientBillingRoleName,
                                          InternalBillingRoleId = (int?)allocation.InternalBillingRoleId,
                                          InternalBillingRoleCode = internalrolelist.InternalBillingRoleCode,
                                          InternalBilling = string.Empty,
                                          InternalBillingPercentage = allocation.InternalBillingPercentage
                                      }).ToList();

                        if (!string.IsNullOrEmpty(searchFilter.AssociateCode))
                            result = result.Where(i => i.AssociateID == searchFilter.AssociateCode).ToList();

                        if (searchFilter.ProjectId != 0)
                            result = result.Where(i => i.ProjectId == searchFilter.ProjectId).ToList();

                        var asssociatesUtilizaton = (from row in result.ToList()
                                                     group row by new
                                                     {
                                                         row.AssociateAllocationId,
                                                         row.AssociateID,
                                                         row.FirstName,
                                                         row.LastName,
                                                         row.Designation,
                                                         row.Grade,
                                                         row.CompetencyGroup,
                                                         //row.ProjectGroup,
                                                         row.Client,
                                                         row.ProjectName,
                                                         row.Billable,
                                                         row.IsBillable,
                                                         row.IsCritical,
                                                         row.BillablePercentage,
                                                         row.Critical,
                                                         row.PercentUtilization,
                                                         row.managerId,
                                                         row.reportingManagerId,
                                                         row.FromDate,
                                                         row.ToDate,
                                                         row.ClientBillingRoleId,
                                                         //row.ClientBillingRoleCode,
                                                         row.ClientBillingRoleName,
                                                         row.InternalBillingRoleId,
                                                         row.InternalBillingRoleCode,
                                                         row.RMFirstName,
                                                         row.RMLastName,
                                                         row.PMFirstName,
                                                         row.PMLastName,
                                                         row.InternalBilling,
                                                         row.InternalBillingPercentage
                                                     } into tempGroup
                                                     select new FinanceReport
                                                     {
                                                         AllocationId = tempGroup.Key.AssociateAllocationId,
                                                         AssociateID = tempGroup.Key.AssociateID,
                                                         AssociateName = tempGroup.Key.FirstName + " " + tempGroup.Key.LastName,
                                                         Designation = string.IsNullOrEmpty(tempGroup.Key.Designation) ? string.Empty : tempGroup.Key.Designation,
                                                         Grade = tempGroup.Key.Grade,
                                                         CompetencyGroup = tempGroup.Key.CompetencyGroup != null ? tempGroup.Key.CompetencyGroup : string.Empty,
                                                         //ProjectGroup = tempGroup.Key.ProjectGroup != null ? tempGroup.Key.ProjectGroup : string.Empty,
                                                         Client = tempGroup.Key.Client,
                                                         ProjectName = tempGroup.Key.ProjectName,
                                                         Billable = (tempGroup.Key.IsBillable == null) ? false : tempGroup.Key.IsBillable,
                                                         IsBillable = tempGroup.Key.IsBillable == true ? "Yes" : "No",
                                                         IsCritical = tempGroup.Key.IsCritical == true ? "Yes" : "No",
                                                         BillablePercentage = tempGroup.Key.BillablePercentage,
                                                         AllocationPercentage = tempGroup.Key.PercentUtilization,
                                                         Critical = tempGroup.Key.IsCritical,
                                                         ProgramManager = tempGroup.Key.PMFirstName + " " + tempGroup.Key.PMLastName,
                                                         ReportingManager = tempGroup.Key.RMFirstName + " " + tempGroup.Key.RMLastName,
                                                         DateFrom = string.Format("{0:dd/MMM/yy}", tempGroup.Key.FromDate.HasValue ? tempGroup.Key.FromDate : fromDate),
                                                         DateTo = string.Format("{0:dd/MMM/yy}", tempGroup.Key.ToDate.HasValue ? tempGroup.Key.ToDate : toDate),
                                                         FromDate = tempGroup.Key.FromDate,
                                                         ToDate = tempGroup.Key.ToDate.HasValue ? tempGroup.Key.ToDate : toDate,
                                                         ClientBillingRoleId = (int?)tempGroup.Key.ClientBillingRoleId,
                                                         InternalBillingRoleId = (int?)tempGroup.Key.InternalBillingRoleId,
                                                         InternalBillingRoleCode = tempGroup.Key.InternalBillingRoleCode != null ? tempGroup.Key.InternalBillingRoleCode : string.Empty,
                                                         //ClientBilling = tempGroup.Key.ClientBillingRoleCode != null ? tempGroup.Key.ClientBillingRoleCode : string.Empty,
                                                         ClientBilling = tempGroup.Key.ClientBillingRoleName != null ? tempGroup.Key.ClientBillingRoleName : string.Empty,
                                                         BillablePercent = (tempGroup.Key.BillablePercentage.ToString()) + "%",
                                                         AllocationPercent = tempGroup.Key.PercentUtilization.ToString() + "%",
                                                         InternalBillingPercentage = tempGroup.Key.InternalBillingPercentage,
                                                         InternalBilling = (tempGroup.Key.InternalBillingPercentage.ToString()) + "%"
                                                     }).OrderBy(x => x.FromDate).ToList();

                        return asssociatesUtilizaton;
                    }
                }
                else
                    throw new AssociatePortalException("From Date and To date should not be empty.");
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region SaveFinanceReportData
        /// <summary>
        /// SaveFinanceReportData
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        public bool SaveFinanceReportData(List<FinanceReport> associateAllocationList)
        {
            bool isValid = false;

            foreach (FinanceReport objAssociateAllocation in associateAllocationList)
            {
                try
                {
                    using (APEntities hrmsEntities = new APEntities())
                    {
                        AssociateAllocation associateAllocation = hrmsEntities.AssociateAllocations.Where(id => id.AssociateAllocationId == objAssociateAllocation.AllocationId).FirstOrDefault();
                        //associateAllocation.ModifiedDate = DateTime.Now;
                        associateAllocation.ModifiedBy = objAssociateAllocation.CurrentUser;
                        associateAllocation.InternalBillingRoleId = objAssociateAllocation.InternalBillingRoleId;
                        associateAllocation.ClientBillingRoleId = objAssociateAllocation.ClientBillingRoleId;
                        hrmsEntities.SaveChanges();
                        isValid = true;
                    }
                }
                catch
                {
                    throw new AssociatePortalException("Failed to save data.");
                }
            }
            return isValid;
        }
        #endregion

        #region GetSkillGroups
        /// <summary>
        /// GetSkillGroups
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetSkillGroups()
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var skillgroupsData = (from s in hrmEntities.SkillGroups
                                           where s.IsActive == true
                                           select new { s.SkillGroupId, s.SkillGroupName }).OrderBy(x => x.SkillGroupName).ToList();

                    if (skillgroupsData.Count > 0)
                        return skillgroupsData;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #endregion

        #region TalentRequisitionDeliveryHeadDashboard

        #region GetPendingRequisitionsForApproval
        /// <summary>
        ///  Get pending for approval requisitions in delivery head dashboard.
        /// </summary>
        /// <returns>List</returns>

        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetPendingRequisitionsForApproval(int employeeId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitions;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var connection = apEntities.Database.Connection)
                    {
                        connection.Open();
                        var command = connection.CreateCommand();
                        command.CommandText = "EXEC [dbo].[usp_GetPendingRequisitionsForApproval] @EmployeeId";

                        SqlParameter param = new SqlParameter();
                        param.ParameterName = "@EmployeeId";
                        param.Value = employeeId;
                        command.Parameters.Add(param);

                        using (var reader = command.ExecuteReader())
                        {
                            reader.NextResult();
                            lstRequisitions = ((IObjectContextAdapter)apEntities)
                                        .ObjectContext
                                        .Translate<DeliveryHeadTalentRequisitionDetails>(reader)
                                        .ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get pending for approval requisitions.");
            }
            return lstRequisitions;
        }
        #endregion

        #region GetRolesAndPositionsByRequisitionId
        /// <summary>
        ///  Get roles and positions details by trid.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetRolesAndPositionsByRequisitionId(int talentRequisitionId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitions;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRequisitions = await apEntities.Database.SqlQuery<DeliveryHeadTalentRequisitionDetails>
                              ("[usp_GetRolesAndPositionsByRequisitionId] @TalentRequisitionId",
                              new object[] {
                                        new SqlParameter ("TalentRequisitionId", talentRequisitionId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles and positions.");
            }
            return lstRequisitions;
        }
        #endregion

        #region GetTaggedEmployeeByTRAndRoleId
        /// <summary>
        ///  Get tagged employee details by trId and roleId.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetTaggedEmployeeByTRAndRoleId(int talentRequisitionId, int RoleMasterId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = await apEntities.Database.SqlQuery<DeliveryHeadTalentRequisitionDetails>
                              ("[usp_GetTaggedEmployeeByTRAndRoleId] @TalentRequisitionId, @RoleMasterId",
                              new object[] {
                                        new SqlParameter ("TalentRequisitionId", talentRequisitionId),
                                        new SqlParameter ("RoleMasterId", RoleMasterId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get tagged employees.");
            }
            return lstEmployees;
        }
        #endregion

        #region GetFinanceHeadList
        /// <summary>
        /// Gets list of finance heads.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetFinanceHeadList()
        {
            List<GenericType> lstFinanceHeads;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstFinanceHeads = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetFinanceHeadList]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get finance heads.");
            }
            return lstFinanceHeads;
        }
        #endregion

        #region ApproveTalentRequisition
        /// <summary>
        /// Approve talent requisition by delivery head
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns>bool</returns>
        public async Task<bool> ApproveTalentRequisition(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        int requisitionTypeNew = Convert.ToInt32(RequisitionType.New);
                        int statusId = 0;
                        string ApprovedByID = "";
                        if (talentRequisitionDetails.RequisitionType == requisitionTypeNew)  //requisition type  :  new
                        {
                            statusId = Convert.ToInt32(TalentRequisitionStatusCodes.SubmittedForFinanceApproval);
                            ApprovedByID = string.Join(",", talentRequisitionDetails.ApprovedByID.Select(ID => ID.Id));
                        }
                        else
                        {
                            statusId = Convert.ToInt32(TalentRequisitionStatusCodes.Approved);  //Final approval (requisition type : replacement)
                        }

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_ApproveRequisition] @TalentrequisitionId, @FromEmployeeID, @ApprovedByID, @StatusId, @Comments, @Modifieduser, @Modifieddate, @Createduser, @Createddate, @Systeminfo",
                                   new object[] {
                                        new SqlParameter ("TalentrequisitionId", talentRequisitionDetails.TalentRequisitionId),
                                        new SqlParameter ("FromEmployeeID", talentRequisitionDetails.FromEmployeeID),
                                        new SqlParameter ("ApprovedByID", ApprovedByID),
                                        new SqlParameter ("StatusId", statusId),
                                        new SqlParameter ("Comments", talentRequisitionDetails.Comments),
                                        new SqlParameter ("Modifieduser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Modifieddate", DateTime.UtcNow),
                                        new SqlParameter ("Createduser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Createddate", DateTime.UtcNow),
                                        new SqlParameter ("Systeminfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();

                        if (rowsAffected > 0)
                        {
                            if (talentRequisitionDetails.RequisitionType == requisitionTypeNew)  //requisition type  :  new
                            {
                                //Send notification email to finance head
                                List<int> employeeIds = ApprovedByID.Split(',').Select(int.Parse).ToList();
                                string EmailId = "";
                                foreach (var employee in employeeIds)
                                {
                                    EmailId = EmailId + (from employe in apEntities.Employees
                                                         join user in apEntities.Users
                                                         on employe.UserId equals user.UserId
                                                         where employe.EmployeeId == employee
                                                         select new
                                                         {
                                                             user.EmailAddress
                                                         }).FirstOrDefault().EmailAddress + ";";

                                }
                                EmailId = EmailId.TrimEnd(';');
                                new TalentRequisitionDetails().NotificationForApproval(talentRequisitionDetails.TalentRequisitionId, talentRequisitionDetails.FromEmployeeID, EmailId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while approving talent requisiton.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region ApproveTalentRequisitionByFinance
        /// <summary>
        /// Approve or Reject talent requisition by finance head
        /// </summary>
        /// <param name="talentRequisitionDetails"></param>
        /// <returns>bool</returns>
        public async Task<bool> ApproveTalentRequisitionByFinance(DeliveryHeadTalentRequisitionDetails talentRequisitionDetails)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        int statusId = Convert.ToInt32(TalentRequisitionStatusCodes.Approved);  //Final approval (requisition type : replacement)


                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_ApproveRequisition] @TalentrequisitionId, @FromEmployeeID, @ApprovedByID, @StatusId, @Comments, @Modifieduser, @Modifieddate, @Createduser, @Createddate, @Systeminfo",
                                   new object[] {
                                        new SqlParameter ("TalentrequisitionId", talentRequisitionDetails.TalentRequisitionId),
                                        new SqlParameter ("FromEmployeeID", talentRequisitionDetails.FromEmployeeID),
                                        new SqlParameter ("ApprovedByID", ""),  //No further approval required.
                                        new SqlParameter ("StatusId", statusId),
                                        new SqlParameter ("Comments", talentRequisitionDetails.Comments),
                                        new SqlParameter ("Modifieduser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Modifieddate", DateTime.UtcNow),
                                        new SqlParameter ("Createduser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("Createddate", DateTime.UtcNow),
                                        new SqlParameter ("Systeminfo", Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();

                        //TO DO : TBD

                        //if (rowsAffected > 0)
                        //{
                        //    if (talentRequisitionDetails.RequisitionType == requisitionTypeNew)  //requisition type  :  new
                        //    {
                        //        //Send notification email to finance head
                        //        List<int> employeeIds = ApprovedByID.Split(',').Select(int.Parse).ToList();
                        //        string EmailId = "";
                        //        foreach (var employee in employeeIds)
                        //        {
                        //            EmailId = EmailId + (from employe in apEntities.Employees
                        //                                 join user in apEntities.Users
                        //                                 on employe.UserId equals user.UserId
                        //                                 where employe.EmployeeId == employee
                        //                                 select new
                        //                                 {
                        //                                     user.EmailAddress
                        //                                 }).FirstOrDefault().EmailAddress + ";";

                        //        }
                        //        EmailId = EmailId.TrimEnd(';');
                        //        new TalentRequisitionDetails().NotificationForApproval(talentRequisitionDetails.TalentrequisitionId, talentRequisitionDetails.FromEmployeeID, EmailId);
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while approving talent requisition.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetRequisitionWorkflowByTRId
        /// <summary>
        ///  Get requisition workflow by trid.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetRequisitionWorkflowByTRId(int talentRequisitionId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitionHistory;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRequisitionHistory = await apEntities.Database.SqlQuery<DeliveryHeadTalentRequisitionDetails>
                              ("[usp_GetRequisitionHistoryByRequisitionId] @TalentRequisitionId",
                              new object[] {
                                        new SqlParameter ("TalentRequisitionId", talentRequisitionId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get requisition workflow.");
            }
            return lstRequisitionHistory;
        }
        #endregion

        #endregion

        #region TalentRequisitionFinanceHeadDashboard

        #region GetRequisitionsForApprovalForFinance
        /// <summary>
        ///  Get pending for approval requisitions in finance head dashboard.
        /// </summary>
        /// <returns>List</returns>

        public async Task<List<DeliveryHeadTalentRequisitionDetails>> GetRequisitionsForApprovalForFinance(int EmployeeId)
        {
            List<DeliveryHeadTalentRequisitionDetails> lstRequisitions;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var connection = apEntities.Database.Connection)
                    {

                        lstRequisitions = await apEntities.Database.SqlQuery<DeliveryHeadTalentRequisitionDetails>
                              ("[usp_GetRequisitionsForApprovalForFinance] @EmployeeId",
                                  new object[] {
                                        new SqlParameter ("EmployeeId", EmployeeId)
                                        }
                                   ).ToListAsync();
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get pending for approval requisitions.");
            }
            return lstRequisitions;
        }
        #endregion

        #endregion
    }
}
