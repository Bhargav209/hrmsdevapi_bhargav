﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class JDtemplateDetails
    {
        #region GetJDTemplateTitles
        /// <summary>
        /// Gets list of JD TemplateTitles
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetJDTemplateTitles()
        {
            List<GenericType> lstJDTemplateTitles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstJDTemplateTitles = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetJDTemplateTitles]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstJDTemplateTitles;
        }
        #endregion

        #region GetTemplateSkills
        /// <summary>
        /// Gets list of JD TemplateSkills
        /// </summary>
        /// <returns></returns>
        public async Task<List<JDTemplateMasterData>> GetTemplateSkills(int templateTitleId)
        {
            List<JDTemplateMasterData> lstJDTemplateMaster;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstJDTemplateMaster = await apEntities.Database.SqlQuery<JDTemplateMasterData>
                              ("[USP_GetSkillsByJDTemplateTitleId] @JDTemplateTitleId", new object[] {
                                    new SqlParameter("JDTemplateTitleId", templateTitleId)
                                       }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstJDTemplateMaster;
        }
        #endregion    

        #region AddJDTemplateSkills
        /// <summary>
        /// Create a JDTemplateMasterData
        /// </summary>
        /// <param name="JDTemplateDetailsData"></param>
        /// <returns></returns>
        public async Task<int> AddJDTemplateSkills(JDTemplateDetailsData JDTemplateDetailsData)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    //Created for SQLBulkCopy
                    SqlConnection sConn = (SqlConnection)apEntities.Database.Connection;

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        //Create datatable to store bulk records
                        DataTable dt = new DataTable("JDTemplateDetail");

                        dt.Columns.Add(new DataColumn("TemplateTitleId", typeof(int)));
                        dt.Columns.Add(new DataColumn("CompetencyAreaId", typeof(int)));
                        dt.Columns.Add(new DataColumn("SkillGroupId", typeof(int)));
                        dt.Columns.Add(new DataColumn("SkillId", typeof(int)));
                        dt.Columns.Add(new DataColumn("ProficiencyLevelId", typeof(int)));

                        var DuplicateItems = await apEntities.Database.SqlQuery<int>
                              ("[usp_GetJDTemplateDuplicates]").SingleOrDefaultAsync();

                        if (DuplicateItems > 0)
                            return -1; //duplicate Entries for TemplateDetails

                        //copy data into Datatable
                        foreach (JDTemplateMasterData JdTemplate in JDTemplateDetailsData.JDTemplateSkills)
                        {
                            DataRow newTemplateDetail = dt.NewRow();
                            newTemplateDetail["TemplateTitleId"] = JDTemplateDetailsData.TemplateTitleId;
                            newTemplateDetail["CompetencyAreaId"] = JdTemplate.CompetencyAreaId;
                            newTemplateDetail["SkillGroupId"] = JdTemplate.SkillGroupId;
                            newTemplateDetail["SkillId"] = JdTemplate.SkillId;
                            newTemplateDetail["ProficiencyLevelId"] = JdTemplate.ProficiencyLevelId;
                            dt.Rows.Add(newTemplateDetail);
                        }
                        // Bulk copy
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sConn, SqlBulkCopyOptions.Default, (SqlTransaction)trans.UnderlyingTransaction))
                        {
                            bulkCopy.ColumnMappings.Add("TemplateTitleId", "TemplateTitleId");
                            bulkCopy.ColumnMappings.Add("CompetencyAreaId", "CompetencyAreaId");
                            bulkCopy.ColumnMappings.Add("SkillGroupId", "SkillGroupId");
                            bulkCopy.ColumnMappings.Add("SkillId", "SkillId");
                            bulkCopy.ColumnMappings.Add("ProficiencyLevelId", "ProficiencyLevelId");

                            bulkCopy.DestinationTableName = "JDTemplateDetail";

                            bulkCopy.WriteToServer(dt);
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
        #endregion


    }
}
