﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using System.Configuration;

namespace AP.API
{ 
    public class FileUploadAndDownloadDetails
    {
        #region Upload Tab Methods

        #region UploadFile
        /// <summary>
        /// UploadFile
        /// </summary>
        /// <returns></returns>
        public bool UploadFile()
        {
            bool returnValue = false;

            try
            {
                var httpRequest = HttpContext.Current.Request;
                string filepath = ConfigurationManager.AppSettings["HRMSRepositoryPath"].ToString();
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        string extension = Path.GetExtension(httpRequest.Files[file].FileName);
                        var postedFile = httpRequest.Files[file];

                        int empID = Convert.ToInt32(httpRequest.Form["empID"]);
                        string fileName = httpRequest.Form["documentName"].ToString().Split('.')[0];
                        using (APEntities hrmsEntities = new APEntities())
                        {
                            string employeeCode = new DashboardDetails().GetAssociateCodeByEmpID(empID);
                            UploadFile uploadFile = new UploadFile();
                            uploadFile.EmployeeID = empID;
                            uploadFile.FileName = fileName + extension;
                            uploadFile.IsActive = true;
                            uploadFile.CreatedDate = DateTime.Now;
                            uploadFile.CreatedUser = HttpContext.Current.User.Identity.Name;
                            uploadFile.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntities.UploadFiles.Add(uploadFile);
                            hrmsEntities.SaveChanges();

                            var userName = (from e in hrmsEntities.Employees
                                            where e.EmployeeId == empID
                                            select new { e.FirstName }).FirstOrDefault();

                            string path = string.Empty;
                            path = @filepath + employeeCode + @"\OnBoarding";

                            // Checks folder with that employee code exits or not.
                            bool isExists = Directory.Exists(path);

                            if (!isExists)                                                           
                                throw new AssociatePortalException(employeeCode + " folder not exists");

                            //Whatever we want to give fileName and serverpath to save the file/files.
                            
                            var filePath1 = path + @"\" + fileName + extension;
                            postedFile.SaveAs(filePath1);
                        }

                        returnValue = true;
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to upload file.");
            }

            return returnValue;
        }
        #endregion

        #region GetFileUploadedData
        /// <summary>
        /// Gets the Uploaded file data as per employee id.
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public IEnumerable<object> GetFileUploadedData(int empID)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                var getFileUploadedData = (from fu in hrmsEntities.UploadFiles
                                           where (fu.EmployeeID == empID && fu.IsActive == true)
                                           select new { fu.FileName, fu.ID, fu.EmployeeID }
                                          ).ToList();

                return getFileUploadedData;
            }

        }
        #endregion

        #region DeleteFileUploadedData
        /// <summary>
        /// DeleteFileUploadedData
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="iD"></param>
        /// <returns></returns>
        public bool DeleteFileUploadedData(int empID, int iD)
        {
            bool returnValue = false;

            using (APEntities hrmsEntities = new APEntities())
            {
                UploadFile uploadFile = hrmsEntities.UploadFiles.Where(i => i.ID == iD && i.EmployeeID == empID).FirstOrDefault();

                if (uploadFile != null)
                {
                    uploadFile.IsActive = false;
                    uploadFile.CreatedDate = DateTime.Now;
                    uploadFile.CreatedUser = HttpContext.Current.User.Identity.Name;
                    uploadFile.SystemInfo = Commons.GetClientIPAddress();
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                    var userName = (from e in hrmsEntities.Employees
                                    where e.EmployeeId == empID
                                    select new { e.FirstName }).FirstOrDefault();

                    if (File.Exists(HttpContext.Current.Server.MapPath("~/Files/" + uploadFile.FileName.Split('.')[0] + "_" + userName.FirstName + "." + uploadFile.FileName.Split('.')[1])))
                    {
                        // Use a try block to catch IOExceptions, to
                        // handle the case of the file already being
                        // opened by another process.
                        try
                        {
                            File.Delete(HttpContext.Current.Server.MapPath("~/Files/" + uploadFile.FileName.Split('.')[0] + "_" + userName.FirstName + "." + uploadFile.FileName.Split('.')[1]));
                        }
                        catch
                        {
                            throw new AssociatePortalException("Failed to get the details.");
                        }
                    }
                }

                return returnValue;
            }
        }
        #endregion

        #endregion        
    }
}
