﻿using System;
using System.Collections.Generic;
using System.Linq;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;

namespace AP.API
{
    public class UserGrades
    {
        #region CreateGrade
        /// <summary>
        /// Method to add new grade data to database
        /// </summary>
        /// <param name="gradeData"></param>
        /// <returns></returns>
        public bool CreateGrade(GradeData gradeData)
        {

          
            bool isCreated = false;

            using (APEntities hrmsEntities = new APEntities())
            {
                try
                {
                    var isExists = (from g in hrmsEntities.Grades
                                    where g.GradeCode == gradeData.GradeCode
                                    select g).Count();
                    if (isExists == 0)
                    { 
                    Grade grade = new Grade();
                    grade.GradeCode = gradeData.GradeCode;
                    grade.GradeName = gradeData.GradeName;
                    grade.IsActive = true;
                    grade.CreatedDate = DateTime.Now;
                    grade.CreatedUser = gradeData.CurrentUser;
                    grade.SystemInfo = gradeData.SystemInfo;
                    hrmsEntities.Grades.Add(grade);
                    isCreated = hrmsEntities.SaveChanges() > 0 ? true : false;
                  }
                    else
                        throw new AssociatePortalException("Grade code already exists");

                }
                catch
                {                   
                    throw;
                }
            }

            return isCreated;
        }
        #endregion

        #region UpdateGrade
        /// <summary>
        /// Method to update existing grade data
        /// </summary>
        /// <param name="gradeData"></param>
        /// <returns></returns>
        public bool UpdateGrade(GradeData gradeData)
        {
            bool isUpdated = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from g in hrmsEntities.Grades
                                    where g.GradeCode == gradeData.GradeCode && g.GradeId != gradeData.GradeId
                                    select g).Count();
                    if (isExists == 0)
                    {
                        Grade grade = hrmsEntities.Grades.FirstOrDefault(g => g.GradeId == gradeData.GradeId);
                        grade.GradeCode = gradeData.GradeCode;
                        grade.GradeName = gradeData.GradeName;
                        grade.IsActive = true;
                        grade.ModifiedDate = DateTime.Now;
                        grade.ModifiedUser =gradeData.CurrentUser;
                        grade.SystemInfo = gradeData.SystemInfo;
                        hrmsEntities.Entry(grade).State = System.Data.Entity.EntityState.Modified;
                        isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Grade code already exists");
                }
            }
            catch
            {               
                throw;
            }

            return isUpdated;
        }
        #endregion

        #region GetGradesDetails
        /// <summary>
        /// GetGradesDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetGradesDetails(bool isActive)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var gradesDetails = apEntities.Grades.Select(grade =>
                                        new { ID = grade.GradeId, Name = grade.GradeCode, grade.GradeId, grade.GradeCode, grade.GradeName, IsActive = grade.IsActive == true ? "Yes" : "No" })
                                        .OrderBy(x => x.GradeName).ToList();

                    if (isActive == true)
                        gradesDetails = gradesDetails.Where(i => i.IsActive == "Yes").ToList();

                    if (gradesDetails.Count > 0)
                        return gradesDetails;
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region GetGradesById
        /// <summary>
        /// Gets grade details by id
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetGradesById(int gradeId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var gradesListById = hrmsEntities.Grades.Where(grade => grade.GradeId == gradeId).Select(grade =>
                                             new { grade.GradeCode, grade.GradeName, grade.IsActive }).ToList();

                    if (gradesListById.Count > 0)
                        return gradesListById;
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
}
