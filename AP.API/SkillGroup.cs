﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class SkillGroup
    {
        #region CreateSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDetails"></param>
        /// <returns></returns>
        public async Task<bool> CreateSkillGroup(SkillGroupDetails skillGroupDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_AddSkillGroup] @SkillGroupName, @Description,@CreatedUser, @SystemInfo, @CompetencyAreaId, @CreatedDate",
                                   new object[] {
                                        new SqlParameter ("SkillGroupName", skillGroupDetails.SkillGroupName),
                                        new SqlParameter ("Description",(object)skillGroupDetails.Description?? DBNull.Value),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("CompetencyAreaId", skillGroupDetails.CompetencyAreaId),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a skillgroup");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region UpdateSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <param name="skillGroupDetails"></param>
        /// <returns></returns>
        public async Task<bool> UpdateSkillGroup(SkillGroupDetails skillGroupDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateSkillGroup] @SkillGroupId, @SkillGroupName, @Description,@ModifiedUser, @SystemInfo, @ModifiedDate, @CompetencyAreaId",
                                   new object[] {
                                        new SqlParameter ("SkillGroupId", skillGroupDetails.SkillGroupId),
                                        new SqlParameter ("SkillGroupName", skillGroupDetails.SkillGroupName),
                                        new SqlParameter ("Description", (object)skillGroupDetails.Description?? DBNull.Value),                                       
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("CompetencyAreaId", skillGroupDetails.CompetencyAreaId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Error while updating a skillgroup");
            }
            return rowsAffected > 0 ? true : false;

        }
        #endregion

        #region GetsSkillGroup
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<SkillGroupDetails>> GetsSkillGroup()
        {
            List<SkillGroupDetails> lstSkillGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkillGroups = await apEntities.Database.SqlQuery<SkillGroupDetails>
                              ("[usp_GetSkillGroup]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get skillgroups");
            }
            return lstSkillGroups;
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<List<SkillGroupDetails>> GetSkillGroupByCompetencyAreaID(int competencyAreaID)
        {
            List<SkillGroupDetails> lstSkillGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkillGroups = await apEntities.Database.SqlQuery<SkillGroupDetails>
                              ("[usp_GetSkillGroupByCompetencyAreaId] @CompetencyAreaID",
                                new object[]  {
                                    new SqlParameter ("CompetencyAreaID", competencyAreaID)
                               }
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Failed to get skillgroups by CompetencyAreaId");
            }
            return lstSkillGroups;
        }
        #endregion

        #region DeleteSkillGroup
        /// <summary>
        /// 
        /// </summary>
        /// <param name="skillGroupDetails"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSkillGroup(SkillGroupDetails skillGroupDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteSkillGroup] @SkillGroupId, @ModifiedUser, @SystemInfo, @ModifiedDate",
                                   new object[] {
                                        new SqlParameter ("SkillGroupId", skillGroupDetails.SkillGroupId),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, ex.Message);
                throw new AssociatePortalException("Error while deleting a skillgroup");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion
    }
}
