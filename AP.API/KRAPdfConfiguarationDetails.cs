﻿using AP.DataStorage;
using AP.DomainEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.API
{
    public class KRAPdfConfiguarationDetails
    {
        #region GetKRAPdfConfiguarationDetails
        /// <summary>
        /// Get KRA Pdf Configuaration Details
        /// </summary>
        /// <returns>List</returns>
        public KRAPdfConfiguarationData GetKRAPdfConfiguarationDetails()
        {
            KRAPdfConfiguarationData lstdetails;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstdetails = apEntities.Database.SqlQuery<KRAPdfConfiguarationData>
                              ("[dbo].[usp_GetKRAPdfConfiguarationDetails]",
                               new object[] { }
                               ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstdetails;
        }
        #endregion
    }
}
