﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using static AP.Utility.Enumeration;
using System.ComponentModel;
using System.Globalization;
using System.Data.Entity;
using System.Web;
using System.Threading.Tasks;
using System.Data.SqlClient;
using AP.Notification;
using System.Text;
using System.Net;

namespace AP.API
{
    public class ProjectDetails
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region GetProject
        /// <summary>
        /// Retrieves project details based on ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetProject(int projectId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var projectData = hrmsEntity.Database.SqlQuery<ProjectData>
                            ("[usp_GetProjectById] @ProjectId",
                                new object[] {
                                        new SqlParameter ("ProjectId", projectId)
                                }
                                ).ToList();
                    return projectData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get project details");
            }
        }
        #endregion

        #region GetProjectsList
        /// <summary>
        /// Retrives all the projects details
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetProjectsList(string userRole, int employeeId, string dashboard)
        {
            try
            {
                string projectState = "SubmittedForApproval";
                using (var apEntities = new APEntities())
                {
                    var projectData = apEntities.Database.SqlQuery<ProjectData>
                             ("[usp_GetProjectDetail] @RoleName, @EmployeeId",
                                 new object[] {
                                        new SqlParameter ("RoleName", userRole),
                                        new SqlParameter ("EmployeeId", employeeId)
                                 }
                                 ).ToList();
                    if (dashboard == "PMDashboard" || dashboard == "DHDashboard")
                    {
                        return projectData.Where(i => i.ProjectState == projectState).ToList();
                    }
                    if (dashboard == "ProjectDashboard")
                    {
                        return projectData.Where(i => i.ProjectState != projectState).ToList();
                    }

                    return projectData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get projects list");
            }
        }
        #endregion

        #region GetProjectsStatuses
        /// <summary>
        /// Get Projects Statuses
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetProjectsStatuses()
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    var statusData = apEntities.Database.SqlQuery<StatusData>
                             ("[usp_GetProjectStatuses] ",
                                 new object[] { }
                                 ).ToList();

                    return statusData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get projects list");
            }
        }
        #endregion

        #region GetProjectsListByTypeId
        /// <summary>
        /// Retrives all the projects details
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetProjectsListByTypeId(int projectTypeId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var projects = (from p in hrmsEntity.Projects
                                    join client in hrmsEntity.Clients on p.ClientId equals client.ClientId
                                    join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                                    where pt.PracticeAreaId == projectTypeId
                                    select new
                                    {
                                        ProjectId = p.ProjectId,
                                        ProjectCode = p.ProjectCode,
                                        ProjectName = p.ProjectName,
                                        IsActive = p.IsActive,
                                        ActualStartDate = p.ActualStartDate,
                                        ActualEndDate = p.ActualEndDate,
                                        PlannedEndDate = p.PlannedEndDate,
                                        PlannedStartDate = p.PlannedStartDate,
                                        CustomerName = client.ClientName,
                                        PracticeArea = pt.PracticeAreaId,
                                        PracticeAreaDescription = pt.PracticeAreaDescription,
                                        ModifiedDate = p.ModifiedDate
                                    }).OrderByDescending(x => x.ModifiedDate).ToList();

                    return projects;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get project");
            }
        }
        #endregion

        #region GetEmpList
        /// <summary>
        /// Retrieves all the employees id and names
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetEmpList()
        {
            IEnumerable<object> returnObject = null;

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var empData = (from e in hrmsEntity.Employees
                                   where e.IsActive == true
                                   select new
                                   {
                                       EmpCode = e.EmployeeId,
                                       EmpName = e.FirstName + " " + e.LastName,
                                       Code = e.EmployeeCode
                                   }).ToList();

                    empData = empData.OrderBy(x => x.EmpName).ThenBy(x => x.EmpCode).ToList();

                    returnObject = empData;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }

            return returnObject;
        }
        #endregion

        #region GetRolesList
        /// <summary>
        /// Get all active Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetRolesList()
        {
            IEnumerable<object> returnObject = null;

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var role = (from r in hrmsEntity.Roles
                                where r.IsActive == true
                                select new
                                {
                                    RoleCode = r.RoleId,
                                    RoleName = r.RoleName,
                                    RoleDescription = r.RoleDescription
                                }).OrderBy(x => x.RoleName).ToList();

                    returnObject = role;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get roles");
            }

            return returnObject;
        }
        /// <summary>
        /// Gets roles by ProjectId
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetRolesList(int projectId)
        {
            IEnumerable<object> returnObject = null;

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    List<int> roleId = new List<int>() { (int)Enumeration.Roles.TL, (int)Enumeration.Roles.Dev, (int)Enumeration.Roles.T, (int)Enumeration.Roles.AT, (int)Enumeration.Roles.PGM, (int)Enumeration.Roles.CDM };
                    var TLAvail = (from au in hrmsEntity.AssociateUtilizes
                                   where au.ProjectId == projectId && au.RoleMasterId == (int)Enumeration.Roles.TL && au.IsActive == true
                                   select au.EmployeeId).ToList();

                    if (TLAvail.Count != 0)
                    {
                        roleId.Remove((int)Enumeration.Roles.TL);
                    }

                    var CDMAvail = (from au in hrmsEntity.AssociateUtilizes
                                    where au.ProjectId == projectId && au.RoleMasterId == (int)Enumeration.Roles.CDM && au.IsActive == true
                                    select au.EmployeeId).ToList();

                    if (CDMAvail.Count != 0)
                    {
                        roleId.Remove((int)Enumeration.Roles.CDM);
                    }

                    var PGMAvail = (from au in hrmsEntity.AssociateUtilizes
                                    where au.ProjectId == projectId && au.RoleMasterId == (int)Enumeration.Roles.PGM && au.IsActive == true
                                    select au.EmployeeId).ToList();

                    if (PGMAvail.Count != 0)
                    {
                        roleId.Remove((int)Enumeration.Roles.PGM);
                    }

                    var role = (from r in hrmsEntity.Roles
                                where r.IsActive == true && roleId.Contains(r.RoleId)
                                select new
                                {
                                    RoleCode = r.RoleId,
                                    RoleName = r.RoleName,
                                    RoleDescription = r.RoleDescription
                                }).ToList();

                    returnObject = role;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }

            return returnObject;
        }
        #endregion

        #region GetReportingManager
        /// <summary>
        /// Gets the Reporting Manager of an employee based on id
        /// </summary>
        /// <param name="associateTypeId"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetReportingManager(int associateTypeId)
        {
            try
            {
                if (associateTypeId == (int)Enumeration.Roles.PGM || associateTypeId == (int)Enumeration.Roles.CDM)
                {
                    return null;
                }

                int[] roleId = new int[3] { (int)Enumeration.Roles.TL, (int)Enumeration.Roles.PGM, (int)Enumeration.Roles.CDM };

                using (APEntities hrmsEntity = new APEntities())
                {
                    if (associateTypeId == (int)Enumeration.Roles.TL)
                    {
                        return (from r in hrmsEntity.Roles
                                where r.IsActive == true && r.RoleId == (int)Enumeration.Roles.PGM
                                select new
                                {
                                    RoleCode = r.RoleId,
                                    RoleName = r.RoleName,
                                    RoleDescription = r.RoleDescription
                                }).ToList();
                    }
                    else
                    {
                        return (from r in hrmsEntity.Roles
                                where r.IsActive == true && roleId.Contains(r.RoleId)
                                select new
                                {
                                    RoleCode = r.RoleId,
                                    RoleName = r.RoleName,
                                    RoleDescription = r.RoleDescription
                                }).ToList();
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get reporting manager");
            }

        }
        #endregion

        #region GetAssociates
        /// <summary>
        /// Retrieves the basic details of associates
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetAssociates()
        {
            try
            {
                string projectType = new ProjectDetails().GetEnumDescription(Enumeration.ProjectTypes.TalentPool);
                using (APEntities hrmsEntity = new APEntities())
                {
                    var associates = (from e in hrmsEntity.Employees
                                      join au in hrmsEntity.AssociateAllocations on e.EmployeeId equals au.EmployeeId
                                      join p in hrmsEntity.Projects on au.ProjectId equals p.ProjectId
                                      join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                                      join department in hrmsEntity.Departments on e.DepartmentId equals department.DepartmentId
                                      where au.IsActive == true && pt.PracticeAreaCode != projectType
                                      orderby e.FirstName
                                      select new
                                      {
                                          AssociateName = e.FirstName + " " + e.LastName,
                                          EmployeeId = e.EmployeeId,
                                          DepartmentId = department.DepartmentId
                                      }).ToList().Distinct();
                    return associates;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region ReleaseAssociate
        /// <summary>
        /// Code to release an associate from a project
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        public bool ReleaseAssociate(AssociateAllocationDetails associateDetails)
        {
            int rowsAffected;

            using (APEntities hrmsEntity = new APEntities())
            {
                using (var dbContext = hrmsEntity.Database.BeginTransaction())
                {
                    try
                    {
                        rowsAffected = hrmsEntity.Database.SqlQuery<int>
                             ("[usp_ReleaseAssociate] @ProjectId, @EmployeeId, @TalentPoolProjectId,@ReleasingPercentage,@ReportingManagerId,@ReleaseDate, @ModifiedDate, @ModifiedBy, @SystemInfo",
                               new object[] {
                                    new SqlParameter("ProjectId", associateDetails.ProjectId),
                                    new SqlParameter("EmployeeId", associateDetails.EmployeeId),
                                    new SqlParameter("TalentPoolProjectId",associateDetails.ReleaseProjectId),
                                    new SqlParameter("ReleasingPercentage",associateDetails.ReleasingPercentage),
                                    new SqlParameter("ReportingManagerId",associateDetails.ReportingManagerId),
                                    new SqlParameter("ReleaseDate",associateDetails.ReleaseDate),
                                    new SqlParameter("ModifiedDate", DateTime.UtcNow),
                                    new SqlParameter("ModifiedBy",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("SystemInfo",  Commons.GetClientIPAddress())
                               }
                               ).SingleOrDefault();
                        dbContext.Commit();
                        return rowsAffected > 0 ? true : false;

                    }
                    catch (Exception ex)
                    {
                        dbContext.Rollback();
                        throw;
                    }
                }
            }

        }
        #endregion       

        #region ResourceAllocate
        /// <summary>
        /// Code to allocate a associate to project
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        public bool ResourceAllocate(AssociateAllocationDetails associateDetails)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    List<AssociateAllocationDetails> associateAllocationDetailsList = new List<AssociateAllocationDetails>();
                    AssociateAllocationDetails associateAllocation = new AssociateAllocationDetails();
                    associateAllocation.AllocationPercentage = associateDetails.AllocationPercentage;
                    associateAllocation.InternalBillingPercentage = associateDetails.InternalBillingPercentage;
                    associateAllocation.ClientBillingPercentage = associateDetails.ClientBillingPercentage;
                    associateAllocation.EffectiveDate = associateDetails.EffectiveDate;
                    associateAllocation.EmployeeId = associateDetails.EmployeeId;
                    associateAllocation.IsActive = true;
                    associateAllocation.isCritical = associateDetails.isCritical;
                    associateAllocation.Billable = associateDetails.Billable;
                    associateAllocation.ProjectId = associateDetails.ProjectId;
                    associateAllocation.RoleMasterId = associateDetails.RoleMasterId;
                    associateAllocation.TalentRequisitionId = associateDetails.TalentRequisitionId; //this value belongs to Talent Requisition,so its getting null value
                    associateAllocation.RequisitionRoleDetailID = associateDetails.RequisitionRoleDetailID;//this value belongs to Talent Requisition,so its getting null value
                    associateAllocation.ClientBillingRoleId = associateDetails.ClientBillingRoleId;
                    associateAllocation.InternalBillingRoleId = associateDetails.InternalBillingRoleId;
                    associateAllocation.ReportingManagerId = associateDetails.ReportingManagerId;
                    associateAllocation.IsPrimary = associateDetails.IsPrimary;
                    associateAllocation.CurrentUser = associateDetails.CurrentUser;
                    associateAllocation.SystemInfo = Commons.GetClientIPAddress();
                    associateAllocation.NotifyAll = associateDetails.NotifyAll;
                    associateAllocationDetailsList.Add(associateAllocation);

                    var result = new Allocation().AllocateAssociates(associateAllocationDetailsList);

                    return result;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "-2")
                    throw new AssociatePortalException("-2");
                else
                    throw new AssociatePortalException("Resource allocation failed."); ;
            }

        }
        #endregion

        #region GetProjectDetails
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetProjectDetails(int employeeId)
        {
            try
            {
                string projectTypeCode = GetEnumDescription(ProjectTypes.TalentPool);

                using (APEntities hrmsEntity = new APEntities())
                {
                    int projectTypeId = hrmsEntity.ProjectTypes.Where(pt => pt.ProjectTypeCode == projectTypeCode).FirstOrDefault().ProjectTypeId;
                    var projects = (from p in hrmsEntity.Projects.AsNoTracking()
                                    join aut in hrmsEntity.AssociateAllocations.AsNoTracking() on p.ProjectId equals aut.ProjectId
                                    join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                                    //join ap in hrmsEntity.AllocationPercentages on aut.AllocationPercentage equals ap.AllocationPercentageID
                                    where aut.EmployeeId == employeeId && aut.IsActive == true && p.ProjectTypeId != projectTypeId
                                    select new
                                    {
                                        ProjectId = p.ProjectId,
                                        ProjectCode = p.ProjectCode,
                                        ProjectName = p.ProjectName,
                                        PracticeAreaCode = pt.PracticeAreaCode,
                                        //AllocationPercentage = ap.Percentage,
                                        AllocationPercentage = aut.AllocationPercentage,
                                        EffectiveDate = aut.EffectiveDate
                                    }).ToList().Distinct();

                    return projects;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region TalentPools
        /// <summary>
        /// Retrieves various talent pools
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetTalentPools()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var projectTypeCode = GetEnumDescription(ProjectTypes.TalentPool);

                    var talentPools = (from pt in hrmsEntities.PracticeAreas
                                       join p in hrmsEntities.Projects on pt.PracticeAreaId equals p.PracticeAreaId
                                       join projectmgrs in hrmsEntities.ProjectManagers on p.ProjectId equals projectmgrs.ProjectID
                                       where pt.PracticeAreaCode == projectTypeCode
                                       select new
                                       {
                                           ReleaseProjectId = p.ProjectId,
                                           ProjectCode = p.ProjectCode,
                                           ProjectName = p.ProjectName,
                                           ReportingManagerId = projectmgrs.ReportingManagerID,
                                       }).ToList();

                    if (talentPools.Count > 0)
                        return talentPools;

                    else
                        return Enumerable.Empty<object>().ToList();

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }

        public string GetEnumDescription(Enum value)
        {
            var fieldName = value.ToString();
            var fieldInfo = value.GetType().GetField(fieldName);
            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : fieldName;
        }
        #endregion

        #region GetPercentUtilization
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetPercentUtilization(int projectId, int employeeId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var percentUtilize = (from au in hrmsEntity.AssociateUtilizes
                                          where (au.ProjectId == projectId && au.EmployeeId == employeeId) && (au.IsActive == true)
                                          select new
                                          {
                                              PercentUtilization = au.PercentUtilization
                                          }).ToList();

                    return percentUtilize;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get utilization percentage");
            }
        }
        #endregion

        #region GetProjectTypes
        /// <summary>
        /// Project types
        /// </summary>
        /// 
        public IEnumerable<object> GetProjectTypes()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var projectType = hrmsEntity.Database.SqlQuery<ProjectData>
                             ("[usp_GetProjectTypes] ",
                              new object[] { }
                              ).ToList();
                    return projectType;
                }
            }
            catch
            {
                throw new AssociatePortalException("failed to get project type");
            }
        }
        #endregion       

        #region GetCustomers
        /// <summary>
        /// Customers
        /// </summary>
        /// 
        public IEnumerable<object> GetCustomers()
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    var projectData = apEntities.Database.SqlQuery<ProjectData>
                           ("[usp_GetClients] ",
                               new object[] { }
                               ).ToList();

                    return projectData.OrderBy(o=>o.ClientName).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region AssignProgramManagerToProject
        /// <summary>
        /// Assign Program Manager To Project
        /// </summary>
        /// 
        public IEnumerable<object> AssignProgramManagerToProject(string userRole, int employeeId)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        var projectData = apEntities.Database.SqlQuery<ProjectData>
                               ("[usp_AssignProgramManagerToProject] @UserRole,@EmployeeId",
                                   new object[] {
                                       new SqlParameter ("UserRole", userRole),
                                       new SqlParameter ("EmployeeId", employeeId)
                                   }
                                   ).ToList();
                        trans.Commit();
                        return projectData;
                    }
                }
            }
            catch
            {

                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region AddProjectDetails
        /// <summary>
        /// Add project details
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public async Task<int> AddProjectDetails(ProjectData projectData)
        {
            int rowsAffected = 0;
            try
            {
                if (string.IsNullOrWhiteSpace(projectData.ProjectCode))
                {
                    throw new InvalidOperationException(resourceManager.GetString("Project Code Should not be null"));
                }
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateProject] @ProjectCode, @ProjectName, @ProjectTypeId, @ClientId, @DomainId, @ProgramManagerId,@PracticeAreaId,@ActualStartDate,@ActualEndDate,@CreatedUser,@CreatedDate,@SystemInfo,@DepartmentId",
                                   new object[] {
                                    new SqlParameter ("ProjectCode", projectData.ProjectCode.Trim()),
                                    new SqlParameter ("ProjectName", projectData.ProjectName.Trim()),
                                    new SqlParameter ("ProjectTypeId", projectData.ProjectTypeId),
                                    new SqlParameter ("ClientId", projectData.ClientId),
                                    new SqlParameter ("DomainId", (object)projectData.DomainId ?? DBNull.Value),
                                    new SqlParameter ("ProgramManagerId", projectData.ManagerId),
                                    new SqlParameter ("PracticeAreaId", (int)projectData.PracticeAreaId),
                                    new SqlParameter ("ActualStartDate", (object) projectData.ActualStartDate ?? DBNull.Value ),
                                    new SqlParameter ("ActualEndDate", (object) projectData.ActualEndDate ?? DBNull.Value ),
                                    new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                    new SqlParameter ("CreatedDate", DateTime.Now),
                                    new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                    new SqlParameter ("DepartmentId", (object) projectData.DepartmentId ?? DBNull.Value )
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.ToUpper() == "SQLEXCEPTION")
                {
                    if (((System.Data.SqlClient.SqlException)ex).Number == 2627)
                        throw new AssociatePortalException(projectData.ProjectName + " Or " + projectData.ProjectCode + " already exist...");
                }
                else
                    throw new AssociatePortalException("Error while creating Project.");
            }
            return rowsAffected;
        }
        #endregion

        #region SubmitForApproval
        /// <summary>
        /// Submit For Approval
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public async Task<int> SubmitForApproval(int projectId, string userRole, int employeeId)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        if (userRole == "Program Manager")
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_SubmitForApproval] @ProjectId,@EmployeeId, @CreatedUser,@CreatedDate,@SystemInfo",
                                       new object[] {
                                        new SqlParameter ("ProjectId", projectId),
                                        new SqlParameter ("EmployeeId", employeeId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.Now),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())

                                       }
                                       ).SingleOrDefaultAsync();
                            trans.Commit();
                            NotificationConfiguration(projectId);
                        }
                        else
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                  ("[usp_ProjectApproved] @ProjectId, @EmployeeId, @CreatedUser,@CreatedDate,@SystemInfo",
                                       new object[] {
                                        new SqlParameter ("ProjectId", projectId),
                                        new SqlParameter ("EmployeeId", employeeId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.Now),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())

                                       }
                                      ).SingleOrDefaultAsync();
                            trans.Commit();
                            NotificationConfiguration(projectId);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while Project submitting for approval.");
            }
            return rowsAffected;
        }
        #endregion
        #region ApproveOrRejectByDH
        /// <summary>
        /// Approve Or Reject By DH
        /// </summary>
        /// <param name="projectId,status"></param>
        /// <returns></returns>
        public async Task<int> ApproveOrRejectByDH(int projectId, string status, int employeeId)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        if (status == "Approve")
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_ProjectApproved] @ProjectId,@EmployeeId, @CreatedUser,@CreatedDate,@SystemInfo",
                                       new object[] {
                                        new SqlParameter ("ProjectId", projectId),
                                        new SqlParameter ("EmployeeId", employeeId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.Now),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())

                                       }
                                       ).SingleOrDefaultAsync();
                            trans.Commit();
                            ApprovedNotificationConfiguration(projectId);
                        }
                        else
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                  ("[usp_ProjectRejected] @ProjectId,@EmployeeId, @CreatedUser,@CreatedDate,@SystemInfo",
                                       new object[] {
                                        new SqlParameter ("ProjectId",projectId),
                                        new SqlParameter ("EmployeeId", employeeId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.Now),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())

                                       }
                                      ).SingleOrDefaultAsync();
                            trans.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while approving or rejecting Project.");
            }
            return rowsAffected;
        }
        #endregion
        #region NotificationConfiguration       
        public void NotificationConfiguration(int projectId)
        {
            try
            {
                //Send notification email to Delivery Head for approval
                using (var hrmEntities = new APEntities())
                {
                    EmailNotificationConfiguration emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                              ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.NotificationType.PPC)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.PPC)),
                               }
                              ).FirstOrDefault();

                    if (emailNotificationConfig != null)
                    {
                        ProjectData projectDetails = hrmEntities.Database.SqlQuery<ProjectData>
                                     ("[usp_GetProjectById] @ProjectId",
                                       new object[]  {
                                       new SqlParameter ("ProjectId", projectId),
                                      }
                                     ).FirstOrDefault();

                        if (projectDetails != null)
                        {
                            NotificationDetail notificationDetail = new NotificationDetail();
                            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

                            emailContent = emailContent.Replace("{ProjectCode}", projectDetails.ProjectCode)
                                                       .Replace("{ProjectName}", projectDetails.ProjectName)
                                                       .Replace("{ManagerName}", projectDetails.ManagerName)
                                                       .Replace("{ProjectState}", projectDetails.ProjectState)
                                                       .Replace("{WorkFlowStatus}", projectDetails.WorkFlowStatus);

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                                throw new AssociatePortalException("Email From cannot be blank");
                            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                                throw new AssociatePortalException("Email To cannot be blank");
                            notificationDetail.ToEmail = emailNotificationConfig.EmailTo;

                            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                            notificationDetail.Subject = emailNotificationConfig.EmailSubject + " " + projectDetails.ProjectName;
                            notificationDetail.EmailBody = emailContent.ToString();
                            NotificationManager.SendEmail(notificationDetail);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Error while sending email notification.");
            }
        }
        #endregion

        public void ApprovedNotificationConfiguration(int projectId)
        {
            try
            {
                //Send notification email from Delivery Head to Program Manager
                using (var hrmEntities = new APEntities())
                {
                    EmailNotificationConfiguration emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                              ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.NotificationType.PPCApproved)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.PPCApproved)),
                               }
                              ).FirstOrDefault();

                    if (emailNotificationConfig != null)
                    {
                        ProjectData projectDetails = hrmEntities.Database.SqlQuery<ProjectData>
                                     ("[usp_GetProjectById] @ProjectId",
                                       new object[]  {
                                       new SqlParameter ("ProjectId", projectId),
                                      }
                                     ).FirstOrDefault();

                        if (projectDetails != null)
                        {
                            NotificationDetail notificationDetail = new NotificationDetail();
                            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

                            emailContent = emailContent.Replace("{ProjectCode}", projectDetails.ProjectCode)
                                                       .Replace("{ProjectName}", projectDetails.ProjectName)
                                                       .Replace("{ManagerName}", projectDetails.ManagerName)
                                                       .Replace("{ProjectState}", projectDetails.ProjectState)
                                                       .Replace("{WorkFlowStatus}", projectDetails.WorkFlowStatus);

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                                throw new AssociatePortalException("Email From cannot be blank");
                            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                                throw new AssociatePortalException("Email To cannot be blank");
                            notificationDetail.ToEmail = emailNotificationConfig.EmailTo;

                            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                            notificationDetail.Subject = emailNotificationConfig.EmailSubject + " " + projectDetails.ProjectName;
                            notificationDetail.EmailBody = emailContent.ToString();
                            NotificationManager.SendEmail(notificationDetail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Error while sending email notification.");
            }
        }

        #region UpdateProject
        /// <summary>
        /// Updates the project details
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<int> UpdateProject(ProjectData project)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {

                        rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                             ("[usp_UpdateProject] @ProjectId,@ProjectName, @ProjectTypeId, @ClientId,@DomainId, @ProjectStateId,@ProgramManagerId, @PracticeAreaId,@ActualStartDate, @ActualEndDate, @ModifiedUser, @ModifiedDate, @SystemInfo, @DepartmentId,@UserRole",
                               new object[] {
                               new SqlParameter ("ProjectId", (object) project.ProjectId ?? DBNull.Value),
                               new SqlParameter ("ProjectName", (object) project.ProjectName.Trim() ?? DBNull.Value),
                               new SqlParameter ("ProjectTypeId", (object) project.ProjectTypeId ?? DBNull.Value),
                               new SqlParameter ("ClientId", (object) project.ClientId ?? DBNull.Value ),
                               new SqlParameter ("DomainId", (object) project.DomainId ?? DBNull.Value),
                               new SqlParameter ("ProjectStateId", (object) project.ProjectStateId ?? DBNull.Value),
                               new SqlParameter ("ProgramManagerId",(object) project.ManagerId ?? DBNull.Value),
                               new SqlParameter ("PracticeAreaId",  (object) project.PracticeAreaId ?? DBNull.Value),
                               new SqlParameter ("ActualStartDate", (object)  project.ActualStartDate ?? DBNull.Value),
                               new SqlParameter ("ActualEndDate", (object)  project.ActualEndDate ?? DBNull.Value),
                               new SqlParameter ("ModifiedUser", (object) HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                               new SqlParameter ("ModifiedDate", (object) DateTime.Now ?? DBNull.Value),
                               new SqlParameter ("SystemInfo", (object) Commons.GetClientIPAddress() ?? DBNull.Value),
                               new SqlParameter ("DepartmentId", (object) project.DepartmentId ?? DBNull.Value),
                               new SqlParameter ("UserRole", (object) project.UserRole ?? DBNull.Value)

                             }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
                if (rowsAffected == 2627)
                    throw new AssociatePortalException("Project Name already exists");
            }
            catch (Exception ex)
            {
                throw;
            }
            return rowsAffected;
        }
        #endregion
        #region DeleteProjectDetails
        /// <summary>
        /// Delete Project Details
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<bool> DeleteProjectDetails(int projectId)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                             ("[usp_DeleteProjectDetails] @ProjectId",
                               new object[] {
                               new SqlParameter ("ProjectId", projectId ),
                             }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to delete project details.");
            }
            return (rowsAffected > 0 ? true : false); ;
        }
        #endregion
        #region GetManagersList
        /// <summary>
        /// Gets the managers name with Ids
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetManagersList()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var managersList = (from manager in hrmsEntities.Projects
                                        join projectmgrs in hrmsEntities.ProjectManagers on manager.ProjectId equals projectmgrs.ProjectID
                                        join emp in hrmsEntities.Employees on projectmgrs.ProgramManagerID equals emp.EmployeeId
                                        into managers
                                        from m in managers.DefaultIfEmpty()
                                        where m.IsActive == true
                                        orderby m.FirstName
                                        select new
                                        {
                                            ManagerId = projectmgrs.ProgramManagerID,
                                            ManagerName = m.FirstName + " " + m.LastName
                                        }).Distinct().ToList();

                    if (managersList.Count > 0)
                        return managersList;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get manager details"); ;
            }

        }
        #endregion

        #region Get Managers and Leads list
        /// <summary>
        /// Get Managers and Leads list
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetManagersAndLeads()
        {
            try
            {
                List<object> resultList = new List<object>();
                using (APEntities hrmsEntities = new APEntities())
                {
                    var managersList = (from manager in hrmsEntities.Projects
                                        join projectmgrs in hrmsEntities.ProjectManagers on manager.ProjectId equals projectmgrs.ProjectID into pmtemp
                                        from pmlist in pmtemp.Where(i => i.IsActive == true).DefaultIfEmpty()
                                        join emp in hrmsEntities.Employees on pmlist.ProgramManagerID equals emp.EmployeeId
                                        into managers
                                        from m in managers.DefaultIfEmpty()
                                        where m.IsActive == true
                                        orderby m.FirstName
                                        select new
                                        {
                                            ManagerId = pmlist.ProgramManagerID,
                                            ManagerName = m.FirstName + " " + m.LastName
                                        }).Distinct().ToList();

                    var leadersList = (from lead in hrmsEntities.Projects
                                       join projectmgrs in hrmsEntities.ProjectManagers on lead.ProjectId equals projectmgrs.ProjectID into pmtemp
                                       from pmlist in pmtemp.Where(i => i.IsActive == true).DefaultIfEmpty()
                                       join emp in hrmsEntities.Employees on pmlist.ReportingManagerID equals emp.EmployeeId
                                       into leads
                                       from m in leads.DefaultIfEmpty()
                                       where m.IsActive == true
                                       orderby m.FirstName
                                       select new
                                       {
                                           ManagerId = pmlist.ReportingManagerID,
                                           ManagerName = m.FirstName + " " + m.LastName
                                       }).Distinct().ToList();

                    if (managersList.Count > 0)
                        resultList.AddRange(managersList);

                    if (leadersList.Count > 0)
                        resultList.AddRange(leadersList);

                    if (resultList.Count > 0)
                        return resultList.Distinct().ToList();
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }

        }
        #endregion

        #region GetReportingDetailsByProjectId
        /// <summary>
        /// Get Reporting Details By ProjectId
        /// </summary>
        /// <returns></returns>
        public ManagersData GetReportingDetailsByProjectId(int projectId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var managersList = (from project in hrmsEntities.Projects
                                        join projectmanagers in hrmsEntities.ProjectManagers on project.ProjectId equals projectmanagers.ProjectID into projects
                                        from projectmgrs in projects.Where(i => i.IsActive == true).DefaultIfEmpty()
                                        join programmanager in hrmsEntities.Employees on projectmgrs.ProgramManagerID equals programmanager.EmployeeId
                                        join reportingmanager in hrmsEntities.Employees on projectmgrs.ReportingManagerID equals reportingmanager.EmployeeId into managers
                                        from manager in managers.DefaultIfEmpty()
                                        join lead in hrmsEntities.Employees on projectmgrs.LeadID equals lead.EmployeeId into leads
                                        from lead in leads.DefaultIfEmpty()
                                        where project.ProjectId == projectId
                                        orderby programmanager.FirstName
                                        select new ManagersData
                                        {
                                            ProgramManagerName = programmanager.FirstName + " " + programmanager.LastName,
                                            ReportingManagerName = manager.FirstName + " " + manager.LastName,
                                            LeadName = lead.FirstName + " " + lead.LastName,
                                            ProgramManagerId = programmanager.EmployeeId,
                                            ReportingManagerId = manager.EmployeeId,
                                            LeadId = lead.EmployeeId
                                        }).Distinct().FirstOrDefault();

                    return managersList;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get details");
            }

        }
        #endregion

        #region GetProjectsList
        /// <summary>
        /// Retrives all the projects details
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> ProjectDataSearch(SearchFilter searchFilter)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var result = (from p in hrmsEntities.Projects
                                  join projectmgrs in hrmsEntities.ProjectManagers on p.ProjectId equals projectmgrs.ProjectID
                                  join client in hrmsEntities.Clients on p.ClientId equals client.ClientId
                                  join pt in hrmsEntities.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                                  join manager in hrmsEntities.Employees on projectmgrs.ReportingManagerID equals manager.EmployeeId

                                  select new
                                  {
                                      ProjectId = p.ProjectId,
                                      ProjectCode = p.ProjectCode,
                                      ProjectName = p.ProjectName,
                                      IsActive = p.IsActive.ToString(),
                                      CustomerName = client.ClientName,
                                      PracticeArea = pt.PracticeAreaCode,
                                      PracticeAreaDescription = pt.PracticeAreaDescription,
                                      ModifiedDate = p.ModifiedDate,
                                      Manager = manager.FirstName + " " + manager.LastName,
                                      ActualStartDate = p.ActualStartDate,
                                      ActualEndDate = p.ActualEndDate,
                                      PlannedEndDate = p.PlannedEndDate,
                                      PlannedStartDate = p.PlannedStartDate
                                  }).OrderByDescending(x => x.ModifiedDate).ToList();

                    if (searchFilter.SearchData == null)
                        searchFilter.SearchData = "";

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "Code")
                        result = result.Where(i => i.ProjectCode.ToLower().Contains(searchFilter.SearchData.ToLower()) || i.ProjectCode.Contains(searchFilter.SearchData.ToLower())).ToList();

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "Name")
                        result = result.Where(i => i.ProjectName.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "Type")
                        result = result.Where(i => i.PracticeArea.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "Manager")
                        result = result.Where(i => i.Manager.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "Customer")
                        result = result.Where(i => i.CustomerName.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();

                    return result;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region GetEmpTalentPoolId
        /// <summary>
        /// Gets employee's talent pool project Id
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public object GetEmpTalentPool(int employeeId)
        {
            string empName = string.Empty;
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var project = (from emp in hrmsEntity.Employees.AsNoTracking()
                                   join parea in hrmsEntity.TalentPools on emp.CompetencyGroup equals parea.PracticeAreaId into pareagroup
                                   from pgroup in pareagroup.DefaultIfEmpty()
                                   join proj in hrmsEntity.Projects on pgroup.ProjectId equals proj.ProjectId
                                   join projectmgrs in hrmsEntity.ProjectManagers on proj.ProjectId equals projectmgrs.ProjectID
                                   join p in hrmsEntity.PracticeAreas on emp.CompetencyGroup equals p.PracticeAreaId
                                   where (emp.EmployeeId == employeeId && pgroup.IsActive == true && projectmgrs.IsActive == true
                                   //&& proj.IsActive == true //as we removed isactive column from project table for now we are commenting it
                                   && emp.IsActive == true && p.IsActive == true)
                                   select new
                                   {
                                       EmployeeCode = emp.EmployeeCode,
                                       ProjectId = pgroup.ProjectId,
                                       ProjectName = proj.ProjectName,
                                       ReportingManagerId = projectmgrs.ReportingManagerID,
                                       EmployeeId = employeeId,
                                       FirstName = emp.FirstName,
                                       LastName = emp.LastName,
                                       PracticeArea = p.PracticeAreaCode
                                   }).FirstOrDefault();
                    empName = project.FirstName + project.LastName;
                    return project;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to fetch {0}'s talent pool project", empName);
            }
        }
        #endregion

        #region GetEmpAllocationHistory
        /// <summary>
        /// GetEmpAllocationHistory
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public List<AssociateAllocationDetails> GetEmpAllocationHistory(int employeeId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    List<AssociateAllocationDetails> associateAllocationDetails = (from al in hrmsEntity.AssociateAllocations.AsNoTracking()
                                                                                   join e in hrmsEntity.Employees.AsNoTracking() on al.EmployeeId equals e.EmployeeId
                                                                                   join p in hrmsEntity.Projects.AsNoTracking() on al.ProjectId equals p.ProjectId
                                                                                   join ap in hrmsEntity.AllocationPercentages.AsNoTracking() on al.AllocationPercentage equals ap.AllocationPercentageID
                                                                                   where (al.EmployeeId == employeeId && al.IsActive == true)
                                                                                   select new AssociateAllocationDetails
                                                                                   {
                                                                                       EmployeeId = al.EmployeeId,
                                                                                       ProjectId = (int)al.ProjectId,
                                                                                       Project = p.ProjectName,
                                                                                       //AllocationPercentage = al.AllocationPercentage,
                                                                                       AllocationPercentage = ap.Percentage,
                                                                                       Billable = (bool)(al.IsBillable == null ? false : al.IsBillable),
                                                                                       isCritical = al.IsCritical == null ? false : al.IsCritical,
                                                                                       IsPrimary = (bool)(al.IsPrimary == null ? false : al.IsPrimary),
                                                                                       EffectiveDate = al.EffectiveDate,
                                                                                       AssociateName = e.FirstName + " " + e.LastName
                                                                                   }).OrderBy(i => i.EffectiveDate).ThenBy(i => i.Project).ToList();
                    return associateAllocationDetails;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetManagers
        /// <summary>
        /// Gets the managers name with Ids
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetManagerList()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var managersList = (from employee in hrmsEntities.Employees
                                        join designation in hrmsEntities.Designations on employee.Designation equals designation.DesignationId
                                        join grade in hrmsEntities.Grades on designation.GradeId equals grade.GradeId
                                        where employee.IsActive == true && grade.GradeCode == "G5-B" && designation.DesignationCode == "PM"
                                        select new
                                        {
                                            ManagerId = employee.EmployeeId,
                                            ManagerName = employee.FirstName + " " + employee.LastName
                                        }).Distinct().ToList();

                    if (managersList.Count > 0)
                        return managersList;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get manager details"); ;
            }

        }
        #endregion

        #region SaveManagersToProject
        /// <summary>
        /// Add and updates managers for a project
        /// </summary>
        /// <param name="projectData"></param>
        /// <returns></returns>
        public bool SaveManagersToProject(ProjectData projectData)
        {
            bool status = false;

            using (APEntities hrmsEntity = new APEntities())
            {
                using (var dbContext = hrmsEntity.Database.BeginTransaction())
                {
                    try
                    {
                        ProjectManager projectManagerDetails = hrmsEntity.ProjectManagers.Where(projects => projects.ProjectID == projectData.ProjectId && projects.IsActive == true).FirstOrDefault();

                        if (projectManagerDetails != null)
                        {

                            if (projectManagerDetails.ProjectID == projectData.ProjectId && projectManagerDetails.ProgramManagerID == projectData.ManagerId && projectManagerDetails.ReportingManagerID == projectData.ReportingManagerId && projectManagerDetails.LeadID == projectData.LeadId)
                            {
                                throw new AssociatePortalException("Record already exists.");
                            }

                            else
                            {
                                projectManagerDetails.IsActive = false;
                                projectManagerDetails.ModifiedDate = DateTime.Now;
                                projectManagerDetails.ModifiedBy = HttpContext.Current.User.Identity.Name;
                                projectManagerDetails.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntity.Entry(projectManagerDetails).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                        ProjectManager projectManagerData = new ProjectManager()
                        {
                            ProjectID = projectData.ProjectId,
                            IsActive = true,
                            ProgramManagerID = projectData.ManagerId,
                            ReportingManagerID = projectData.ReportingManagerId,
                            LeadID = projectData.LeadId,
                            CreatedBy = HttpContext.Current.User.Identity.Name,
                            CreatedDate = DateTime.Now,
                            SystemInfo = Commons.GetClientIPAddress()
                        };

                        hrmsEntity.ProjectManagers.Add(projectManagerData);


                        List<AssociateAllocation> allocations = hrmsEntity.AssociateAllocations.Where(allocation => allocation.ProjectId == projectData.ProjectId && allocation.EmployeeId != projectData.ManagerId && allocation.IsActive == true).ToList();

                        if (allocations != null)
                        {
                            foreach (var allocation in allocations)
                            {
                                allocation.ReportingManagerId = projectData.ReportingManagerId;
                                allocation.ProgramManagerID = projectData.ManagerId;
                                allocation.LeadID = projectData.LeadId;
                                allocation.ModifiedDate = DateTime.Now;
                                allocation.ModifiedBy = HttpContext.Current.User.Identity.Name;
                                allocation.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntity.Entry(allocation).State = System.Data.Entity.EntityState.Modified;

                                Employee employees = hrmsEntity.Employees.Where(employee => employee.EmployeeId == allocation.EmployeeId && employee.IsActive == true).FirstOrDefault();

                                if (employees != null)
                                {
                                    employees.ReportingManager = allocation.ReportingManagerId;
                                    employees.ModifiedDate = DateTime.Now;
                                    employees.ModifiedUser = HttpContext.Current.User.Identity.Name;
                                    employees.SystemInfo = Commons.GetClientIPAddress();
                                    hrmsEntity.Entry(employees).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                        }
                        status = hrmsEntity.SaveChanges() > 0 ? true : false;
                        dbContext.Commit();
                        return status;
                    }
                    catch
                    {
                        dbContext.Rollback();
                        throw;
                    }
                }
            }

        }
        #endregion

        #region UpdateReportingManagerToAssociate
        /// <summary>
        /// Updates the reporting manager for a associate
        /// </summary>
        /// <param name="projectData"></param>
        /// <returns></returns>
        public bool UpdateReportingManagerToAssociate(ProjectData projectData, bool isDelivery)
        {
            bool status = false;
            using (APEntities hrmsEntity = new APEntities())
            {
                using (var dbContext = hrmsEntity.Database.BeginTransaction())
                {
                    if (isDelivery == true)
                    {
                        AssociateAllocation allocationData = hrmsEntity.AssociateAllocations.Where(allocation => allocation.EmployeeId == projectData.EmployeeId && allocation.ProjectId == projectData.ProjectId && allocation.IsActive == true).FirstOrDefault();

                        if (allocationData != null)
                        {
                            allocationData.ReportingManagerId = projectData.ReportingManagerId;
                            allocationData.ProgramManagerID = projectData.ManagerId;
                            allocationData.LeadID = projectData.LeadId;
                            allocationData.ModifiedBy = HttpContext.Current.User.Identity.Name;
                            allocationData.ModifiedDate = DateTime.Now;
                            allocationData.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntity.Entry(allocationData).State = EntityState.Modified;

                            status = UpdateReportingManagerToEmployee(projectData.EmployeeId, projectData.ReportingManagerId);
                        }
                    }
                    else
                    {
                        status = UpdateReportingManagerToEmployee(projectData.EmployeeId, projectData.ReportingManagerId);
                    }

                    status = hrmsEntity.SaveChanges() > 0 ? true : false;
                    dbContext.Commit();
                    return status;
                }
            }
        }
        #endregion

        #region UpdateReportingManagerToEmployee
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        private bool UpdateReportingManagerToEmployee(int? employeeID, int? reportingManagerID)
        {
            bool status = false;
            using (var hrmEntities = new APEntities())
            {
                Employee employeeData = hrmEntities.Employees.Where(employee => employee.EmployeeId == employeeID && employee.IsActive == true).FirstOrDefault();

                if (employeeData != null)
                {
                    employeeData.ReportingManager = reportingManagerID;
                    employeeData.ModifiedUser = HttpContext.Current.User.Identity.Name;
                    employeeData.ModifiedDate = DateTime.Now;
                    employeeData.SystemInfo = Commons.GetClientIPAddress();
                    hrmEntities.Entry(employeeData).State = EntityState.Modified;
                    status = hrmEntities.SaveChanges() > 0 ? true : false;
                }
                return status;
            }
        }
        #endregion

        /// <summary>
        /// Get Active Projects
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProjectData>> GetActiveProjects()
        {
            List<ProjectData> lstProjects;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstProjects = await apEntities.Database.SqlQuery<ProjectData>
                              ("[usp_GetProjects]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get active projects");
            }
            return lstProjects;
        }

        /// <summary>
        /// Get Employees By ProjectsID
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetEmployeesByProjectID(int projectID)
        {
            List<GenericType> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetEmployeeDetailsByProjectID] @ProjectID",

                               new object[] {
                                        new SqlParameter ("ProjectID", projectID)
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get employee details");
            }
            return lstEmployees;
        }

        #region GetEmployees
        /// <summary> 
        /// Gets list of associates by search string
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetEmployees(string searchString)
        {
            List<GenericType> lstAssociates;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAssociates = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetEmployees] @SearchString",
                                new object[] {
                                    new SqlParameter("SearchString", searchString)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAssociates;
        }
        #endregion

        #region Get All Leads and Managers
        /// <summary> 
        /// Get list of Leads and managers by search string
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetAllLeadsManagers(string searchString)
        {
            List<GenericType> allLeadsManagers;
            try
            {
                using (var apEntities = new APEntities())
                {
                    allLeadsManagers = await apEntities.Database.SqlQuery<GenericType>
                                  ("[usp_GetAllLeadManagers] @SearchString",
                                    new object[] {
                                    new SqlParameter("SearchString", searchString)
                                            }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return allLeadsManagers;
        }
        #endregion

        #region GetManagerandLeadByProjectID
        /// <summary>
        /// To get the manager and lead name by project DI
        /// </summary>
        /// <returns></returns>
        public ManagersData GetManagerandLeadByProjectID(int projectID, int employeeID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    ManagersData getNames = (from allocation in hrmsEntities.AssociateAllocations
                                             join programmanager in hrmsEntities.Employees on allocation.ProgramManagerID equals programmanager.EmployeeId into pm
                                             from programmanager in pm.DefaultIfEmpty()
                                             join reportingmanager in hrmsEntities.Employees on allocation.ReportingManagerId equals reportingmanager.EmployeeId into rm
                                             from reportingmanager in rm.DefaultIfEmpty()
                                             join lead in hrmsEntities.Employees on allocation.LeadID equals lead.EmployeeId into leads
                                             from lead in leads.DefaultIfEmpty()
                                             where allocation.ProjectId == projectID && allocation.IsActive == true && allocation.EmployeeId == employeeID
                                             select new ManagersData
                                             {
                                                 ProgramManagerName = programmanager.FirstName + " " + programmanager.LastName,
                                                 ReportingManagerName = reportingmanager.FirstName + " " + reportingmanager.LastName,
                                                 LeadName = lead.FirstName + " " + lead.LastName,
                                                 ProgramManagerId = programmanager.EmployeeId,
                                                 ReportingManagerId = reportingmanager.EmployeeId,
                                                 LeadId = lead.EmployeeId
                                             }).FirstOrDefault();

                    return getNames;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetSowsByProjectId
        /// <summary>
        /// GetSowsByProjectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetSowsByProjectId(int projectId)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var SOWDetails = apEntities.Database.SqlQuery<SOWSignData>
                                  ("[usp_GetSowsByProjectId] @ProjectId",
                                    new object[] {
                                        new SqlParameter ("ProjectId", projectId)
                                    }).ToList();

                    return SOWDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get SOW details.");
            }
        }
        #endregion
        #region GetAddendumDetailsById
        /// <summary>
        /// GetAddendumDetailsById
        /// </summary>
        /// <param name="Id,projectId,roleName"></param>
        /// <returns></returns>
        public IEnumerable<object> GetAddendumDetailsById(int id, int projectId, string roleName)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var addendumDetails = apEntities.Database.SqlQuery<SOWSignData>
                                  ("[usp_GetAddendumsBySOWId] @Id,@ProjectId,@RoleName",
                                    new object[] {
                                        new SqlParameter ("Id", id),
                                         new SqlParameter ("ProjectId", projectId),
                                         new SqlParameter ("RoleName", roleName)
                                    }).ToList();

                    return addendumDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Addendum details.");
            }
        }
        #endregion
        #region GetAddendums
        /// <summary>
        /// GetAddendums
        /// </summary>
        /// <param name="Id,projectid"></param>
        /// <returns></returns>
        public IEnumerable<object> GetAddendums(int Id, int projectId)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var addendumDetails = apEntities.Database.SqlQuery<SOWSignData>
                                  ("[usp_GetAddendums] @Id,@ProjectId",
                                    new object[] {
                                        new SqlParameter ("Id", Id),
                                         new SqlParameter ("ProjectId", projectId),

                                    }).ToList();

                    return addendumDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Addendum details.");
            }
        }
        #endregion
        #region GetSOWDetails
        /// <summary>
        /// GetSOWDetails
        /// </summary>
        /// <param name="Id,projectId,roleName"></param>
        /// <returns></returns>
        public IEnumerable<object> GetSOWDetails(int id, int projectId, string roleName)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var addendumDetails = apEntities.Database.SqlQuery<SOWSignData>
                                  ("[usp_GetSOWDetails] @Id,@ProjectId,@RoleName",
                                    new object[] {
                                        new SqlParameter ("Id", id),
                                         new SqlParameter ("ProjectId", projectId),
                                         new SqlParameter ("RoleName", roleName)
                                    }).ToList();

                    return addendumDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get SOW details.");
            }
        }
        #endregion

        #region PostSOW
        /// <summary>
        /// Add SOW details
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public int PostSOW(SOWSignData sowSignData)
        {
            int isCreated = 0;

            using (APEntities hrmsEntities = new APEntities())
            {
                using (var trans = hrmsEntities.Database.BeginTransaction())
                {

                    if (sowSignData.SOW)
                    {
                        isCreated = hrmsEntities.Database.SqlQuery<int>
                               ("[usp_CreateSOW] @ProjectId,@SOWId,@SOWFileName, @SignedDate,@IsActive,@CreatedDate,@CreatedUser,@SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ProjectId", sowSignData.ProjectId),
                                        new SqlParameter ("SOWId", sowSignData.SOWId.Trim()),
                                        new SqlParameter ("SOWFileName", (object)sowSignData.SOWFileName ?? DBNull.Value),
                                        new SqlParameter ("SignedDate",sowSignData.SignedDate),
                                        new SqlParameter ("IsActive",true),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }).SingleOrDefault();
                        trans.Commit();
                    }
                    else
                    {
                        isCreated = hrmsEntities.Database.SqlQuery<int>
                               ("[usp_CreateAddendum] @ProjectId,@Id,@AddendumNo, @RecipientName,@AddendumDate,@Note,@CreatedDate,@CreatedUser,@SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ProjectId", sowSignData.ProjectId),
                                        new SqlParameter ("Id", sowSignData.Id),
                                        new SqlParameter ("AddendumNo", (object)sowSignData.AddendumNo ?? DBNull.Value),
                                        new SqlParameter ("RecipientName", (object)sowSignData.RecipientName ?? DBNull.Value),
                                        new SqlParameter ("AddendumDate", (object)sowSignData.AddendumDate ?? DBNull.Value),
                                        new SqlParameter ("Note", (object)sowSignData.Note ?? DBNull.Value),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                   }).SingleOrDefault();
                        trans.Commit();
                    }
                }
            }

            return isCreated;
        }

        #endregion

        #region DeleteSOW
        /// <summary>
        /// Delete SOW
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<bool> DeleteSOW(int Id)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                             ("[usp_DeleteSOW] @Id",
                               new object[] {
                               new SqlParameter ("Id",Id ),
                             }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to delete SOW Details.");
            }
            return (rowsAffected > 0 ? true : false); ;
        }
        #endregion

        #region UpdateSOWAndAddendumDetails
        /// <summary>
        /// Updates the project details
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<int> UpdateSowAndAddendumDetails(SOWSignData sowSign)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        if (sowSign.SOW)
                        {
                            rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                                 ("[usp_UpdateSOWDetails] @Id, @SOWId,@ProjectId,@SOWFileName, @SOWSignedDate, @RoleName, @ModifiedDate, @ModifiedUser, @SystemInfo",
                                   new object[] {
                                       new SqlParameter ("Id", sowSign.Id ),
                               new SqlParameter ("SOWId", (object) sowSign.SOWId.Trim() ?? DBNull.Value),
                               new SqlParameter ("ProjectId", (object) sowSign.ProjectId ?? DBNull.Value),
                               new SqlParameter ("SOWFileName", (object) sowSign.SOWFileName ?? DBNull.Value),
                               new SqlParameter ("SOWSignedDate",sowSign.SignedDate),
                               new SqlParameter ("RoleName", (object) sowSign.RoleName.Trim() ?? DBNull.Value),
                               new SqlParameter ("ModifiedDate", (object) DateTime.Now ?? DBNull.Value),
                               new SqlParameter ("ModifiedUser", (object) HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                               new SqlParameter ("SystemInfo", (object) Commons.GetClientIPAddress() ?? DBNull.Value)

                                 }).SingleOrDefaultAsync();
                            trans.Commit();
                        }
                        else
                        {
                            rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                                ("[usp_UpdateAddendumDetails] @Id,@ProjectId, @RoleName, @AddendumNo,@RecipientName, @AddendumDate, @Note,@ModifiedUser, @ModifiedDate, @SystemInfo",
                                  new object[] {

                               new SqlParameter ("Id", (object) sowSign.AddendumId ?? DBNull.Value),
                               new SqlParameter ("ProjectId", (object) sowSign.ProjectId ?? DBNull.Value),
                               new SqlParameter ("RoleName", (object) sowSign.RoleName.Trim() ?? DBNull.Value),
                               new SqlParameter ("AddendumNo",(object) sowSign.AddendumNo ?? DBNull.Value),
                               new SqlParameter ("RecipientName",  (object) sowSign.RecipientName ?? DBNull.Value),
                               new SqlParameter ("AddendumDate", (object)  sowSign.AddendumDate ?? DBNull.Value),
                               new SqlParameter ("Note", (object)  sowSign.Note ?? DBNull.Value),
                               new SqlParameter ("ModifiedUser", (object) HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                               new SqlParameter ("ModifiedDate", (object) DateTime.Now ?? DBNull.Value),
                               new SqlParameter ("SystemInfo", (object) Commons.GetClientIPAddress() ?? DBNull.Value)

                                }).SingleOrDefaultAsync();
                            trans.Commit();
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to update Sow And Addendum details.");
            }
            return rowsAffected;
        }
        #endregion


        #region Close Project
        ///<summary>
        ///close project
        /// <param name="projectId"></param>
        /// <returns> integer value</returns>
        ///</summary>
        public async Task<int> HasActiveClientBillingRoles(int projectId)
        {
            int isClosed = 0, canClose = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        canClose = hrmsEntities.Database.SqlQuery<int>("[usp_HasActiveClientBillingRoles] @ProjectId",
                                      new object[] {
                                        new SqlParameter ("ProjectId",projectId),
                                      }).SingleOrDefault();
                        trans.Commit();

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return canClose;

        }

        #endregion


        #region Close Project
        ///<summary>
        ///close project
        /// <param name="projectId"></param>
        /// <returns> integer value</returns>
        ///</summary>
        public async Task<int> CloseProject(ProjectData projectData)
        {
            int isClosed = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        isClosed = hrmsEntities.Database.SqlQuery<int>("[usp_CloseProject] @ProjectId, @EndDate, @ModifiedUser, @ModifiedDate, @SystemInfo",
                                      new object[] {
                                        new SqlParameter ("ProjectId",projectData.ProjectId),
                                        new SqlParameter ("EndDate",projectData.ActualEndDate),
                                        new SqlParameter ("ModifiedUser", (object) HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                        new SqlParameter ("ModifiedDate", (object) DateTime.Now ?? DBNull.Value),
                                        new SqlParameter ("SystemInfo", (object) Commons.GetClientIPAddress() ?? DBNull.Value),
                                      }).SingleOrDefault();
                        trans.Commit();

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return isClosed;

        }
    }

        #endregion
    }



