﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.API
{
    public partial class Footer : PdfPageEventHelper
    {

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            PdfPTable table = null;
            PdfPCell cell = null;
            table = new PdfPTable(2);

            table.HorizontalAlignment = 0;
            table.TotalWidth = 470f;
            table.LockedWidth = true;

            string imagePath = ConfigurationManager.AppSettings["SenecaLogo"].ToString();
            Image image = Image.GetInstance(imagePath);
            image.ScaleToFit(150f, 150f);
            image.Alignment = Element.ALIGN_RIGHT;

            imagePath = ConfigurationManager.AppSettings["MidSizeWorkspace"].ToString();
            Image WorkSpaceLogo = Image.GetInstance(imagePath);
           // WorkSpaceLogo.ScaleToFit(90f, 110f);


            cell = new PdfPCell(WorkSpaceLogo);
            cell.Border = 0;
            cell.FixedHeight = 40;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            WorkSpaceLogo.ScaleAbsolute(120, 40);
           // cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;            
            table.AddCell(cell);

            cell = new PdfPCell(image);
            cell.Border = 0;           
            cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
            table.AddCell(cell);
            table.SpacingAfter = 20;
            document.Add(table);

        }

        public override void OnEndPage(PdfWriter writer, Document doc)
        {
            base.OnEndPage(writer, doc);
            PdfPTable footerTbl = new PdfPTable(1);
            footerTbl.TotalWidth = 500;

            string data = "SENECAGLOBAL IT SERVICES PRIVATE LIMITED, SURVEY NUMBER 13,  WESTERN PEARL, 7TH FLOOR, KONDAPUR,";
            Paragraph footer = new Paragraph(data, FontFactory.GetFont(BaseFont.HELVETICA, 7, new BaseColor(186, 184, 108)));
            footer.Alignment = Element.ALIGN_RIGHT;
            PdfPCell cell = new PdfPCell(footer);
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.PaddingLeft = 0;
            footerTbl.AddCell(cell);
            data = "HYDERABAD 500084, TELANGANA, INDIA.O + 91.40.66073500, +91.40.23113300 F + 91.40.23119900 WWW.SENECAGLOBAL.COM";
            footer = new Paragraph(data, FontFactory.GetFont(BaseFont.HELVETICA, 7, new BaseColor(186, 184, 108)));
            footer.Alignment = Element.ALIGN_RIGHT;
            cell = new PdfPCell(footer);
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.PaddingLeft = 0;
            footerTbl.AddCell(cell);
            data=  "COMPANY IDENTITY NUMBER -U72200TG2007PTC055624 ";
            footer = new Paragraph(data, FontFactory.GetFont(BaseFont.HELVETICA, 7, new BaseColor(186, 184, 108)));
            footer.Alignment = Element.ALIGN_RIGHT;
            cell = new PdfPCell(footer);
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.PaddingLeft = 0;
            footerTbl.AddCell(cell);





            footerTbl.WriteSelectedRows(0, -1, 50, (doc.BottomMargin - 5), writer.DirectContent);


        }
    }
}
