﻿using System;
using System.Linq;
using System.Resources;
using AP.DataStorage;
using AP.Utility;
using Quartz;
using AP.DomainEntities;
using System.Collections.Generic;
using System.Web;

namespace AP.API
{
    public class ProfileCreationNotificationJob : IJob
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region AddJob
        /// <summary>
        /// Add the profile creation job to the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <param name="interval"></param>
        /// 
        public bool AddJob(string jobId, string groupId)
        {
            bool retValue = true;

            try
            {
                //using (APEntities hrmEntities = new APEntities())
                //{
                //    string profileCode = resourceManager.GetString("ProfileCreationNotification");
                //    //NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(n => n.notificationCode == profileCode).FirstOrDefault();

                //    //if (notificationConfiguration.SLA.GetValueOrDefault() == 0) throw new AssociatePortalException("Employee Profile Creation Notification's SLA can not be zero.");

                //    //retValue = NotificationScheduler.AddJob<ProfileCreationNotificationJob>(jobId, groupId, notificationConfiguration.SLA.GetValueOrDefault());

                //    if (retValue)
                //    {
                //        string statusStarted = resourceManager.GetString("Started");
                //        var jobExist = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId && j.Status == statusStarted).FirstOrDefault();

                //        // Insert record to the JobDetail table 
                //        //
                //        //if (jobExist == null)
                //        //{
                //        //    JobDetail jobDetail = new JobDetail() { JobCode = jobId, GroupID = groupId, Status = statusStarted, JobInterval = notificationConfiguration.SLA.GetValueOrDefault() };
                //        //    jobDetail = hrmEntities.JobDetails.Add(jobDetail);
                //        //}

                //        string notificationTypeId = resourceManager.GetString("ProfileCreationNotificationTypeID");
                //        string statusPendingId = resourceManager.GetString("StatusPendingID");
                //        var notType = Convert.ToInt32(notificationTypeId);
                //        var pending = Convert.ToInt32(statusPendingId);
                //        var notificationExist = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == notType).FirstOrDefault();

                //        // Insert record in to Notification table  with NotificationTypeID = "EmployeeProfileCreation" and  StatusId = "Pending"
                //        //
                //        if (notificationExist == null)
                //        {
                //            //Notification notification = new Notification() { EmployeeCode = jobId, NotificationTypeID = notType, StatusId = pending };
                //            //notification = hrmEntities.Notifications.Add(notification);
                //        }
                //        else
                //        {
                //            notificationExist.StatusId = pending;
                //        }

                //        hrmEntities.SaveChanges();
                //    }
                //}
            }
            catch
            {
                throw new AssociatePortalException("Failed to save details.");
            }

            return retValue;
        }
        #endregion

        #region RemoveJob
        /// <summary>
        /// Remove job from the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <param name="remarks"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        /// 
        public bool RemoveJob(string jobId, string groupId, string remarks, string status)
        {
            bool isDelete = false;

            try
            {
                isDelete = NotificationScheduler.RemoveJob(jobId, groupId);

                using (APEntities hrmEntities = new APEntities())
                {
                    ///  Delete Job from JobDetail table ///

                    JobDetail jobToBeDeleted = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId).FirstOrDefault();

                    if (jobToBeDeleted != null)
                    {
                        hrmEntities.JobDetails.Remove(jobToBeDeleted);
                    }

                    int profileCreationNotificationTypeID = Convert.ToInt32(resourceManager.GetString("ProfileCreationNotificationTypeID"));
                    //Notification notification = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == profileCreationNotificationTypeID).FirstOrDefault();

                    //if (notification != null)
                    //{
                    //    notification.Remarks = remarks;
                    //    int approvedId = Convert.ToInt32(resourceManager.GetString("StatusApprovedID"));
                    //    int rejectedId = Convert.ToInt32(resourceManager.GetString("StatusRejectedID"));

                    //    if (status == resourceManager.GetString("Approve"))
                    //    {
                    //        notification.StatusId = approvedId;
                    //    }
                    //    else // for reject
                    //    {
                    //        notification.StatusId = rejectedId;
                    //    }
                    //}

                    hrmEntities.SaveChanges();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to delete details.");
            }

            return isDelete;
        }
        #endregion

        #region Execute
        /// <summary>
        /// Execution of the job for sending email
        /// </summary>
        /// <param name="context"></param>
        /// 
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (context != null)
                {
                    JobDataMap jobDataMap = context.JobDetail.JobDataMap;
                    string associateId = jobDataMap.GetString("AssociateId");
                    string name = jobDataMap.GetString("AssociateName");
                    string designation = jobDataMap.GetString("Designation");
                    string department = jobDataMap.GetString("Department");
                    string mobileNo = jobDataMap.GetString("Mobile");

                    using (APEntities hrmEntities = new APEntities())
                    {
                        string profileNotificationCode = resourceManager.GetString("ProfileCreationNotification");
                        //NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == profileNotificationCode).FirstOrDefault();

                        //if (notificationConfiguration != null && name != null && department != null)
                        //{
                        //    string htmlContent = notificationConfiguration.emailContent;

                        //    htmlContent = htmlContent.Replace("@AssociateName", name).Replace("@Designation", designation).Replace("@Department", department).Replace("@Mobile", mobileNo);
                        //    Email email = new Email();
                        //    int notificationConfigID = new BaseEmail().BuildEmailObject(email, notificationConfiguration.emailTo, notificationConfiguration.emailFrom, notificationConfiguration.emailCC,
                        //                                    notificationConfiguration.emailSubject + associateId, htmlContent, Enumeration.NotificationStatus.EPC.ToString());
                        //    new BaseEmail().SendEmail(email, notificationConfigID);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion
    }
}
