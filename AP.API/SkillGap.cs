﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Resources;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;
using static AP.Utility.Enumeration;
using System.ComponentModel;
using System.Data.SqlClient;

namespace AP.API
{
    public class SkillGap
    {
        #region GetCurrentProficiencyLevel
        /// <summary>
        /// Retrieves the current proficiency level of employee based on employeeId and skillId
        /// </summary>
        /// <returns></returns>
        /// 
        public object GetCurrentProficiencyLevel(int employeeId, int skillId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {


                    var CurrentProficiencyLevel = (from es in hrmsEntity.EmployeeSkills.AsNoTracking()
                                                   join skill in hrmsEntity.Skills.AsNoTracking() on es.SkillId equals skill.SkillId
                                                   join pl in hrmsEntity.ProficiencyLevels.AsNoTracking() on es.ProficiencyLevelId equals pl.ProficiencyLevelId
                                                   where es.EmployeeId == employeeId && es.SkillId == skillId && es.IsActive==true &&skill.IsActive==true&&pl.IsActive==true
                                                   select new
                                                   { CurrProficiencyLevelDescription=pl.ProficiencyLevelDescription, CurrentProficiencyLevelId=pl.ProficiencyLevelId }).FirstOrDefault();

                    return CurrentProficiencyLevel;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion


        #region GetRoleByIDs
        /// <summary>
        /// Retrieves the current proficiency level of employee based on employeeId and skillId
        /// </summary>
        /// <returns></returns>
        /// 
        public object GetRoleByIDs(int employeeId, int projectId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {


                    var currentRole = hrmsEntity.Database.SqlQuery<string>
                                        ("[usp_GetRolesByEmployeeAndProjectId] @ProjectId, @EmployeeId",
                                        new object[]  {
                                            new SqlParameter ("ProjectId", projectId),
                                            new SqlParameter ("EmployeeId", employeeId)
                                        }
                                        ).FirstOrDefault();

                    return currentRole;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetProjectsByEmpID
        /// <summary>
        /// Retrieves the projects of employee based on employeeId and role
        /// </summary>
        /// <returns></returns>
        /// 
        public object GetProjectsByEmpID(int employeeId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {


                    var projects = (from a in hrmsEntity.AssociateAllocations
                                    join pr in hrmsEntity.Projects on a.ProjectId equals pr.ProjectId
                                    join projectmgrs in hrmsEntity.ProjectManagers on pr.ProjectId equals projectmgrs.ProjectID
                                    where a.EmployeeId == employeeId && (projectmgrs.ReportingManagerID == employeeId || projectmgrs.ReportingManagerID == employeeId) && a.IsActive == true && pr.IsActive == true
                                     select new
                                     {
                                         ProjectId = a.ProjectId,
                                         ProjectName = pr.ProjectName
                                     }).Distinct().OrderBy(i => i.ProjectName).ToList();

                    return projects;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAssociateSkillGapList
        /// <summary>
        /// Retrieves the skill gap details of all associates
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetAssociateSkillGapList(int projectID)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var skillGapList = (from s in hrmsEntity.AssociateSkillGaps
                                        join c in hrmsEntity.CompetencyAreas on s.CompetencyAreaId equals c.CompetencyAreaId
                                        join e in hrmsEntity.Employees on s.EmployeeId equals e.EmployeeId
                                        join p in hrmsEntity.ProficiencyLevels on s.RequiredProficiencyLevelId equals p.ProficiencyLevelId
                                        join pl in hrmsEntity.ProficiencyLevels on s.CurrentProficiencyLevelId equals pl.ProficiencyLevelId
                                        join ps in hrmsEntity.ProjectSkills on s.ProjectSkillId equals ps.ProjectSkillId
                                        join sk in hrmsEntity.Skills on ps.SkillId equals sk.SkillId
                                        join st in hrmsEntity.Status on s.StatusId equals st.StatusId
                                        where s.IsActive ==true && ps.ProjectId== projectID&&c.IsActive==true&&e.IsActive==true&&p.IsActive==true
                                        && pl.IsActive==true && ps.IsActive==true && sk.IsActive==true && st.IsActive==true
                                        select new SkillGapData
                                        {
                                           AssociateSkillGapId=s.AssociateSkillGapId,                                           
                                           CompetencyAreaCode=c.CompetencyAreaCode,
                                            empFirstName = e.FirstName,
                                            empLastName= e.LastName,
                                           RequiredProficiencyLevel =p.ProficiencyLevelCode,
                                           CurrentProficiencyLevel=pl.ProficiencyLevelCode,
                                           SkillCode=sk.SkillCode,
                                           StatusCode=st.StatusCode,
                                          }).ToList();
                    skillGapList.Select(e => { e.employeeName = e.empFirstName + " " + e.empLastName; return e; }).ToList();
                    return skillGapList;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetRequiredProficiencyLevel
        /// <summary>
        /// Retrieves the required proficiency level of skill in project based on projectId and skillId
        /// </summary>
        /// <returns></returns>
        /// 
        public object GetRequiredProficiencyLevel(int projectId, int skillId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {


                    var RequiredProficiencyLevel = (from ps in hrmsEntity.ProjectSkills.AsNoTracking()
                                                    join skill in hrmsEntity.Skills.AsNoTracking() on ps.SkillId equals skill.SkillId
                                                    join pl in hrmsEntity.ProficiencyLevels.AsNoTracking() on ps.ProficiencyId equals pl.ProficiencyLevelId
                                                    where ps.ProjectId == projectId && ps.SkillId == skillId && ps.IsActive==true && skill.IsActive==true && pl.IsActive==true
                                                    select new { ReqProficiencyLevelDescription=pl.ProficiencyLevelDescription, RequiredProficiencyLevelId=pl.ProficiencyLevelId }).FirstOrDefault();

                    return RequiredProficiencyLevel;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region AddUpdateSkillGapAssesment
        /// <summary>
        /// Adds and updates the skill gap assesment details
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        /// 
        public int AddUpdateSkillGapAssesment(SkillGapData skillGapAssesment)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    
                    AssociateSkillGap asp = hrmsEntity.AssociateSkillGaps.Where(asg => asg.AssociateSkillGapId == skillGapAssesment.AssociateSkillGapId).FirstOrDefault();
                    if (asp == null)
                        asp = new AssociateSkillGap();
                    asp.EmployeeId = skillGapAssesment.EmpId;                                    
                    asp.ProjectSkillId =(from ps in hrmsEntity.ProjectSkills.Where(ps => ps.ProjectId == skillGapAssesment.ProjectId && ps.SkillId == skillGapAssesment.SkillId && ps.IsActive==true)select ps.ProjectSkillId).FirstOrDefault();
                    asp.CompetencyAreaId = skillGapAssesment.CompetencyAreaId;
                    asp.StatusId = (from s in hrmsEntity.Status.Where(st=> st.StatusCode == skillGapAssesment.Status && st.Category=="skillgapassesment" && st.IsActive==true) select s.StatusId).FirstOrDefault(); 
                    asp.CurrentProficiencyLevelId = (from p in hrmsEntity.ProficiencyLevels.Where(pl => pl.ProficiencyLevelDescription.ToLower() == skillGapAssesment.currProficiencyLevelDescription.ToLower()) select p.ProficiencyLevelId).FirstOrDefault();
                    asp.RequiredProficiencyLevelId = skillGapAssesment.RequiredProficiencyLevelId;
                    asp.IsActive = skillGapAssesment.IsActive;
                    asp.SystemInfo = skillGapAssesment.SystemInfo;
                    asp.IsActive = true;
                    if (skillGapAssesment.AssociateSkillGapId == 0)
                    {
                        var IsExist = hrmsEntity.AssociateSkillGaps.Where(asg => asg.EmployeeId == skillGapAssesment.EmpId && asg.ProjectSkillId == asp.ProjectSkillId && asg.RequiredProficiencyLevelId == skillGapAssesment.RequiredProficiencyLevelId).ToList().Count();
                        if (IsExist == 0)
                        {
                            asp.CreatedUser = skillGapAssesment.CurrentUser;
                            asp.CreatedDate = DateTime.Now;
                            hrmsEntity.AssociateSkillGaps.Add(asp);
                        }
                        else
                        {
                            throw new AssociatePortalException("Required skill gap is already assigned to associate.");
                        }
                    }
                    else
                    {
                        AssociateSkillGap asgexists = hrmsEntity.AssociateSkillGaps.Where(asg => asg.EmployeeId == skillGapAssesment.EmpId && asg.ProjectSkillId == asp.ProjectSkillId && asg.RequiredProficiencyLevelId == skillGapAssesment.RequiredProficiencyLevelId && asg.AssociateSkillGapId != skillGapAssesment.AssociateSkillGapId).FirstOrDefault();
                        if (asgexists != null)
                            throw new AssociatePortalException("Required skill gap is already assigned to associate.");
                        else
                        {
                            asp.ModifiedUser = skillGapAssesment.CurrentUser;
                            asp.ModifiedDate = DateTime.Now;
                            hrmsEntity.Entry(asp).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    var result = hrmsEntity.SaveChanges();
                    return result;
                }
            }
            catch
            {
                throw;
            }

        }
        #endregion
    }
}
