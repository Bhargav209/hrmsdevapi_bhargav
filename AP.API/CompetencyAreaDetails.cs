﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.DataStorage;
using AP.Utility;

namespace AP.API
{
   public class CompetencyAreaDetails
   {
      #region CreateCompetencyArea
      /// <summary>
      /// CreateCompetencyArea
      /// </summary>
      /// <param name="competencyAreaData"></param>
      /// <returns></returns>
      public bool CreateCompetencyArea(CompetencyAreaData competencyAreaData)
      {
         bool isCreated = false;
         using (APEntities hrmsEntities = new APEntities())
         {
            var isExists = (from c in hrmsEntities.CompetencyAreas
                            where c.CompetencyAreaCode == competencyAreaData.CompetencyAreaCode
                            select c).Count();
            if (isExists == 0)
            {
               CompetencyArea competencyArea = new CompetencyArea();
               competencyArea.CompetencyAreaCode = competencyAreaData.CompetencyAreaCode;
               competencyArea.CompetencyAreaDescription = competencyAreaData.CompetencyAreaDescription;
               competencyArea.IsActive = true;
               competencyArea.CreatedUser = competencyAreaData.CurrentUser;
               competencyArea.CreatedDate = DateTime.Now;
               competencyArea.SystemInfo = competencyAreaData.SystemInfo;
               hrmsEntities.CompetencyAreas.Add(competencyArea);
               isCreated = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            else
               throw new AssociatePortalException("Competency area code already exists");
         }

         return isCreated;
      }
      #endregion

      #region UpdateCompetencyArea
      /// <summary>
      /// UpdateCompetencyArea
      /// </summary>
      /// <param name="competencyAreaData"></param>
      /// <returns></returns>
      public bool UpdateCompetencyArea(CompetencyAreaData competencyAreaData)
      {
         bool isUpdated = false;
         using (APEntities hrmsEntities = new APEntities())
         {
            CompetencyArea competencyArea = hrmsEntities.CompetencyAreas.FirstOrDefault(cid => cid.CompetencyAreaId == competencyAreaData.CompetencyAreaId);

            //check for duplicate CompetencyAreaCode
            int isExists = (from c in hrmsEntities.CompetencyAreas
                            where c.CompetencyAreaCode == competencyAreaData.CompetencyAreaCode
                            && c.CompetencyAreaId != competencyAreaData.CompetencyAreaId
                            select c).Count();
            if (isExists > 0)
               throw new AssociatePortalException("Competency area code already exists");               

                //check for whether any skills are being mapped
                var skillExists = (from skill in hrmsEntities.Skills
                               where skill.CompetencyAreaId == competencyAreaData.CompetencyAreaId
                               && skill.IsActive == true
                               select skill).Count();

            if (skillExists > 0 && !(bool)competencyAreaData.IsActive)
               throw new AssociatePortalException("Skills exists under this competency area");

             //check for whether any skillgroups are being mapped
               var skillGroupExists = (from skillGroup in hrmsEntities.SkillGroups
                                        where skillGroup.CompetencyAreaId == competencyAreaData.CompetencyAreaId
                                        && skillGroup.IsActive == true
                                        select skillGroup).Count();

                if (skillGroupExists > 0 && !(bool)competencyAreaData.IsActive)
                    throw new AssociatePortalException("Skill Groups exists under this competency area");


            competencyArea.CompetencyAreaCode = competencyAreaData.CompetencyAreaCode;
            competencyArea.CompetencyAreaDescription = competencyAreaData.CompetencyAreaDescription;
            competencyArea.ModifiedUser = competencyAreaData.CurrentUser;
            competencyArea.ModifiedDate = DateTime.Now;
            competencyArea.SystemInfo = competencyAreaData.SystemInfo;
            competencyArea.IsActive = true;
            hrmsEntities.Entry(competencyArea).State = System.Data.Entity.EntityState.Modified;
            isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;
         }

         return isUpdated;
      }
      #endregion

      #region GetCompetencyAreaDetails
      /// <summary>
      /// GetCompetencyAreaDetails
      /// </summary>
      /// <param name="isActive"></param>
      /// <returns></returns>
      public IEnumerable<object> GetCompetencyAreaDetails(bool isActive)
      {
         try
         {
            using (APEntities hrmsEntities = new APEntities())
            {
               var competencyAreasList = hrmsEntities.CompetencyAreas.Select(c =>
                 new { ID = c.CompetencyAreaId, Name = c.CompetencyAreaCode, c.CompetencyAreaId, c.CompetencyAreaCode, c.CompetencyAreaDescription, IsActive = c.IsActive == true ? "Yes" : "No" })
                 .OrderBy(x => x.CompetencyAreaCode).ToList();

               if (isActive == true)
                  competencyAreasList = competencyAreasList.Where(i => i.IsActive == "Yes").ToList();

               if (competencyAreasList.Count > 0)
                  return competencyAreasList;

               else
                  return Enumerable.Empty<object>().ToList();

            }
         }
         catch
         {
            throw new AssociatePortalException("Failed to get competency details.");
         }
      }
      #endregion
   }
}
