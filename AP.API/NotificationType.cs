﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class NotificationType
    {
        #region CreateNotificationType
        /// <summary>
        /// Create a new notification type
        /// </summary>
        /// <param name="notificationTypeDetails"></param>
        /// <returns></returns>
        public async Task<int> CreateNotificationType(NotificationTypeDetails notificationTypeDetails)
        {
            int rowsAffected;
            try
            {
                if (!string.IsNullOrEmpty(notificationTypeDetails.NotificationType) && notificationTypeDetails.NotificationType.Length > 50 && !System.Text.RegularExpressions.Regex.IsMatch(notificationTypeDetails.NotificationType, "^[a-zA-Z0-9_ ]*$"))
                {
                    return 0;
                }

                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateNotificationType] @NotificationCode, @NotificationDescription, @CategoryId, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("NotificationCode", notificationTypeDetails.NotificationType),
                                        new SqlParameter ("NotificationDescription", notificationTypeDetails.Description),
                                        new SqlParameter ("CategoryId", notificationTypeDetails.CategoryId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region UpdateNotificationType
        /// <summary>
        /// Update a NotificationType
        /// </summary>
        /// <param name="notificationTypeDetails"></param>
        /// <returns></returns>
        public async Task<int> UpdateNotificationType(NotificationTypeDetails notificationTypeDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateNotificationType] @NotificationTypeID, @NotificationCode, @NotificationDescription, @CategoryId, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("NotificationTypeID", notificationTypeDetails.NotificationTypeID),
                                        new SqlParameter ("NotificationCode", notificationTypeDetails.NotificationType),
                                        new SqlParameter ("NotificationDescription", notificationTypeDetails.Description),
                                        new SqlParameter ("CategoryId", notificationTypeDetails.CategoryId),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region GetNotificationType
        /// <summary>
        /// Gets list of notification types
        /// </summary>
        /// <returns></returns>
        public async Task<List<NotificationTypeDetails>> GetNotificationType()
        {
            List<NotificationTypeDetails> lstNotificationType;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstNotificationType = await apEntities.Database.SqlQuery<NotificationTypeDetails>
                              ("[usp_GetNotificationType]").ToListAsync();

                    lstNotificationType.OrderBy(notification => notification.NotificationType);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstNotificationType;
        }
        #endregion

        #region DeleteNotificationTypeByTypeId
        /// <summary>
        /// Delete Notification type by notification type id
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteNotificationTypeByTypeId(int notificationTypeId, int categoryId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteNotificationType] @NotificationTypeID, @CategoryId",
                                   new object[] {
                                        new SqlParameter ("NotificationTypeID", notificationTypeId),
                                        new SqlParameter ("CategoryId", categoryId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting notification type.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion
    }
}
