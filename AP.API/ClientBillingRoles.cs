﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class ClientBillingRoles
    {
        #region Create ClientBillingRole
        /// <summary>
        /// Method to add new client billing role data to database
        /// </summary>
        /// <param name="clientBillingRoleDetails"></param>
        /// <returns></returns>
        public async Task<int> CreateClientBillingRole(ClientBillingRoleDetails clientBillingRoleDetails)
        {
            int rowsAffected;
            try
            {
                //if (!string.IsNullOrEmpty(clientBillingRoleDetails.ClientBillingRoleCode) && clientBillingRoleDetails.ClientBillingRoleCode.Length > 50 && !System.Text.RegularExpressions.Regex.IsMatch(clientBillingRoleDetails.ClientBillingRoleCode, "^[a-zA-Z-&, ]*$"))
                //{
                //    return 0;
                //}

                if (!string.IsNullOrEmpty(clientBillingRoleDetails.ClientBillingRoleName) && clientBillingRoleDetails.ClientBillingRoleName.Length > 70 && !System.Text.RegularExpressions.Regex.IsMatch(clientBillingRoleDetails.ClientBillingRoleName, "^[a-zA-Z-&, ]*$"))
                {
                    return 0;
                }

                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateClientBillingRole]  @ClientBillingRoleName, @ProjectId, @IsActive,@NoOfPositions, @CreatedDate, @CreatedUser, @SystemInfo,@StartDate,@ClientBillingPercentage",
                                   new object[] {
                                        new SqlParameter ("ClientBillingRoleName", clientBillingRoleDetails.ClientBillingRoleName.Trim()),
                                        new SqlParameter ("ProjectId", clientBillingRoleDetails.ProjectId),
                                        new SqlParameter ("IsActive", 1),
                                        new SqlParameter ("NoOfPositions", clientBillingRoleDetails.NoOfPositions),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("StartDate", (Object)clientBillingRoleDetails.StartDate ?? DBNull.Value),
                                        new SqlParameter ("ClientBillingPercentage", clientBillingRoleDetails.ClientBillingPercentage),
                                        
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region Update ClientBillingRole
        /// <summary>
        /// Method to update existing client billing role data
        /// </summary>
        /// <param name="clientBillingRoleDetails"></param>
        /// <returns></returns>

        public async Task<int> UpdateClientBillingRole(ClientBillingRoleDetails clientBillingRoleDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateClientBillingRole] @ClientBillingRoleId, @ClientBillingRoleName, @ProjectId, @NoOfPositions, @StartDate,@EndDate, @ModifiedDate, @ModifiedUser, @SystemInfo,@ClientBillingPercentage",
                               new object[] {
                                        new SqlParameter ("ClientBillingRoleId", clientBillingRoleDetails.ClientBillingRoleId),
                                        new SqlParameter ("ClientBillingRoleName", clientBillingRoleDetails.ClientBillingRoleName.Trim()),
                                        new SqlParameter ("ProjectId", clientBillingRoleDetails.ProjectId),
                                        new SqlParameter ("NoOfPositions", clientBillingRoleDetails.NoOfPositions),
                                        new SqlParameter ("StartDate",(object)clientBillingRoleDetails.StartDate?? DBNull.Value),
                                        new SqlParameter ("EndDate",(object)clientBillingRoleDetails.EndDate??DBNull.Value),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("ClientBillingPercentage", clientBillingRoleDetails.ClientBillingPercentage),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region Get ClientBillingRoles
        /// <summary>
        /// Get Client Billing Roles
        /// </summary>        
        /// <returns>ClientBillingRoles</returns>

        public async Task<List<ClientBillingRoleDetails>> GetClientBillingRoles()
        {
            List<ClientBillingRoleDetails> clientBillingRoles;

            try
            {
                using (var apEntities = new APEntities())
                {
                    clientBillingRoles = await apEntities.Database.SqlQuery<ClientBillingRoleDetails>
                              ("[usp_GetClientBillingRoles]").ToListAsync();

                    //clientBillingRoles.OrderBy(clientBillingRole => clientBillingRole.ClientBillingRoleCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clientBillingRoles;
        }
        #endregion

        #region Get ClientBillingRolesByProject
        /// <summary>
        /// Get Client Billing Roles By Project
        /// </summary>      
        /// <param name="projectId"></param>
        /// <returns>ClientBillingRolesByProject</returns>

        public async Task<List<ClientBillingRoleDetails>> GetClientBillingRolesByProjectId(int projectId)
        {
            List<ClientBillingRoleDetails> clientBillingRoles;

            try
            {
                using (var apEntities = new APEntities())
                {
                    clientBillingRoles = await apEntities.Database.SqlQuery<ClientBillingRoleDetails>
                              ("[usp_GetClientBillingRolesByProjectId] @ProjectId",
                              new object[] {
                                        new SqlParameter ("ProjectId", projectId)
                                   }
                              ).ToListAsync();
                    foreach (ClientBillingRoleDetails percentage in clientBillingRoles)
                    {
                        percentage.ClientBillingPercentage= Convert.ToInt32(percentage.Percentage);
                    }
                    clientBillingRoles.OrderBy(clientBillingRole => clientBillingRole.ClientBillingRoleName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clientBillingRoles;
        }
        #endregion

        #region GetEmployeesByClientBillingRoleId
        /// <summary>
        /// Get Employees By Client Billing Role Id
        /// </summary>      
        /// <param name="clientbillingroleid,projectId"></param>
        /// <returns>GetEmployeesByClientBillingRoleId</returns>

        public async Task<List<ClientBillingRoleDetails>> GetEmployeesByClientBillingRoleId(int clientBillingRoleId,int projectId)
        {
            List<ClientBillingRoleDetails> employeeName;

            try
            {
                using (var apEntities = new APEntities())
                {
                    employeeName = await apEntities.Database.SqlQuery<ClientBillingRoleDetails>
                              ("[usp_GetEmployeesByClientBillingRoleId] @ClientBillingRoleId,@ProjectId",
                              new object[] {
                                        new SqlParameter ("ClientBillingRoleId", clientBillingRoleId),
                                        new SqlParameter ("ProjectId", projectId)
                                   }
                              ).ToListAsync();                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return employeeName;
        }
        #endregion

        #region DeleteClientBillingRole
        /// <summary>
        /// Delete ClientBilling Role
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<bool> DeleteClientBillingRole(int clientBillingRoleId)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                             ("[usp_DeleteClientBillingRole] @ClientBillingRoleId",
                               new object[] {
                               new SqlParameter ("ClientBillingRoleId", clientBillingRoleId ),
                             }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch(Exception ex)
            {
                throw new AssociatePortalException("Failed to delete Client Billing Role.");
            }
            return (rowsAffected > 0 ? true : false); ;
        }
        #endregion
        #region CloseClientBillingRole
        /// <summary>
        /// Close Client Billing Role
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<int> CloseClientBillingRole(int clientBillingRoleId,DateTime endDate)
        {
            int rowsAffected = 0;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    using (var trans = hrmsEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await hrmsEntities.Database.SqlQuery<int>
                             ("[usp_CloseClientBillingRole] @ClientBillingRoleId,@EndDate,@ModifiedUser,@ModifiedDate,@SystemInfo",
                               new object[] {
                               new SqlParameter ("ClientBillingRoleId", clientBillingRoleId ),
                               new SqlParameter ("EndDate", endDate ),
                               new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                               new SqlParameter ("ModifiedDate",  DateTime.UtcNow ),
                               new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                             }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to delete Client Billing Role.");
            }
            return rowsAffected ;
        }
        #endregion
    }
}
