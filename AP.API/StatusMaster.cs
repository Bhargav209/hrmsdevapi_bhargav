﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;

namespace AP.API
{
    public class StatusMaster
    {
        #region CreateStatusMaster
        /// <summary>
        /// Method to add status data to database
        /// </summary>
        /// <param name="statusData"></param>
        /// <returns></returns>
        public bool CreateStatusMaster(StatusData statusData)
        {
            bool statusRetValue = false;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var isExists = (from s in hrmEntities.Status
                                    where s.StatusCode == statusData.StatusCode
                                    select s).Count();

                    int categoryID = (from cm in hrmEntities.CategoryMasters
                                      where cm.CategoryName == statusData.CategoryName && cm.IsActive == true
                                      select cm.CategoryID).FirstOrDefault();

                    if (isExists == 0)
                    {

                        Status status = new Status();
                        status.StatusCode = statusData.StatusCode;
                        status.StatusDescription = statusData.StatusDescription;
                        //status.Category = statusData.Category;
                        status.CategoryID = categoryID;
                        status.IsActive = statusData.IsActive;
                        status.CreatedUser = statusData.CurrentUser;
                        status.CreatedDate = DateTime.Now;
                        status.SystemInfo = statusData.SystemInfo;
                        hrmEntities.Status.Add(status);
                        statusRetValue = hrmEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Status code already exists");
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return statusRetValue;
        }
        #endregion

        #region GetStatusMasterDetails
        /// <summary>
        /// Get Status Master Details
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetStatusMasterDetails(bool isActive)
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var statusMasterDetails = (from a in hrmEntities.Status
                                               join cm in hrmEntities.CategoryMasters on a.CategoryID equals cm.CategoryID
                                               select new { a.StatusCode, a.StatusDescription, IsActive = a.IsActive == true ? "Yes" : "No", a.StatusId, a.CategoryID, cm.CategoryName}).OrderBy(s => s.StatusCode).ToList();

                    if (isActive)
                        statusMasterDetails = statusMasterDetails.Where(i => i.IsActive == "Yes").ToList();

                    if (statusMasterDetails.Count > 0)
                        return statusMasterDetails;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

       
        #region GetStatusIdByCode
        /// <summary>
        /// Get Status Master details from DB
        /// </summary>
        /// <returns></returns>
        public int GetStatusIdByCode(string statusCode)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var statusId = hrmEntities.Status.Where(s => s.StatusCode.ToLower() == statusCode.ToLower()).FirstOrDefault().StatusId;
                    if (statusId == null)
                        return 0;
                    else
                        return statusId;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion


        #region UpdateStatusMasterDetails
        /// <summary>
        /// Method to update status data
        /// </summary>
        /// <param name="statusData"></param>
        /// <returns></returns>
        public bool UpdateStatusMasterDetails(StatusData statusData)
        {
            bool modifiedRetValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from s in hrmsEntities.Status
                                    where s.StatusCode == statusData.StatusCode && s.StatusId != statusData.StatusId
                                    select s).Count();

                    int categoryID = (from cm in hrmsEntities.CategoryMasters
                                      where cm.CategoryName == statusData.CategoryName && cm.IsActive == true
                                      select cm.CategoryID).FirstOrDefault();

                    if (isExists == 0)
                    {
                        Status status = hrmsEntities.Status.FirstOrDefault(i => i.StatusId == statusData.StatusId);
                        status.StatusCode = statusData.StatusCode;
                        status.StatusDescription = statusData.StatusDescription;
                        //status.Category = statusData.Category;
                        status.CategoryID = categoryID;
                        status.IsActive = statusData.IsActive;
                        status.ModifiedDate = DateTime.Now;
                        status.ModifiedUser = statusData.CurrentUser;
                        status.SystemInfo = statusData.SystemInfo;
                        hrmsEntities.Entry(status).State = System.Data.Entity.EntityState.Modified;
                        modifiedRetValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Status code already exists");
                }

            }
            catch
            {
                throw;
            }

            return modifiedRetValue;
        }
        #endregion

        //#region GetStatusMasterDDL
        ///// <summary>
        ///// Get Status Master DDL 
        ///// </summary>
        ///// <returns></returns>
        //public IEnumerable<object> GetStatusMasterDDL()
        //{
        //    string parms = "[]";

        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            var statusMasterDetails = (from status in hrmsEntities.Status
        //                                       select new
        //                                       {
        //                                           ID = status.StatusCode,
        //                                           Name = status.StatusDescription
        //                                       }).ToList();
        //            return statusMasterDetails;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(ex, Log.Severity.Error, parms);
        //        throw;
        //    }
        //}
        //#endregion
    }
}
