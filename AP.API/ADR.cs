﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.DataStorage;
using System.Data.SqlClient;
using AP.Utility;
using System.Web;

namespace AP.API
{
    public class ADR
    {
        #region Create ADR Cycle
        /// <summary>
        /// Method to add new ADR Cycle data to database
        /// </summary>
        /// <param name="ADR Cycle"></param>
        /// <returns></returns>
        public async Task<int> CreateADRCycle(ADRCycleDetail adrCycleDetail)
        {
            int rowsAffected;
            try
            {
                if (!string.IsNullOrEmpty(adrCycleDetail.ADRCycle))
                {
                    return 0;
                }
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>("[usp_CreateADRCycle] @ADRCycle,@FinancialYearId, @IsActive, @DateCreated, @CreatedUser, @SystemInfo",
                            new object[] {
                                new SqlParameter("ADRCycle",adrCycleDetail.ADRCycle),
                                new SqlParameter("FinancialYearId",adrCycleDetail.FinancialYearId),
                                new SqlParameter ("IsActive", 0),
                                new SqlParameter ("DateCreated", DateTime.UtcNow),
                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),

                            }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region CreateADRSection
        /// <summary>
        /// Create ADR Section
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        public async Task<int> CreateADRSection(ADRData adrData)
        {
            int rowsAffected=0;
            try
            {                
                using (var apEntities = new APEntities())
                {
                    string[] departments = adrData.DepartmentId.Split(',');
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        foreach (string department in departments)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                ("[usp_CreateADRSection] @ADRSectionName, @ADRMeasurementAreaId,@DepartmentId, @CreatedDate, @CreatedUser, @SystemInfo",
                                new object[] {
                                new SqlParameter("ADRSectionName",adrData.ADRSectionName),
                                new SqlParameter("ADRMeasurementAreaId",adrData.ADRMeasurementAreaId),
                                 new SqlParameter("DepartmentId",Convert.ToInt32(department)),
                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),

                                }).SingleOrDefaultAsync();
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region CreateAssociateADRDetail
        /// <summary>
        /// Create Associate ADR Detail
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        public async Task<int> CreateAssociateADRDetail(ADRData adrData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {                   
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                ("[usp_CreateAssociateADRDetail] @AssociateKRAMapperId, @ADRCycleID,@KRADefinitionId,@Contribution,@SelfRating,@ManagerComments,@Rating, @CreatedDate, @CreatedUser, @SystemInfo",
                                new object[] {
                                new SqlParameter("AssociateKRAMapperId",adrData.AssociateKRAMapperId),
                                new SqlParameter("ADRCycleID",adrData.ADRCycleID),
                                new SqlParameter("KRADefinitionId",adrData.KRADefinitionId),
                                new SqlParameter("Contribution",adrData.Contribution),
                                new SqlParameter("SelfRating",adrData.SelfRating),
                                new SqlParameter("ManagerComments",adrData.ManagerComments),
                                new SqlParameter("Rating",adrData.Rating),
                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),

                                }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region GetAssociateADRDetail
        /// <summary>
        /// Get Associate ADR Detail
        /// </summary>
        /// <returns></returns>
        public async Task<List<ADRData>> GetAssociateADRDetail(int EmployeeID,int FinancialYearID,int AdrCycleId)
        {
            List<ADRData> getAdrSections;
            try
            {
                using (var apEntities = new APEntities())
                {
                    getAdrSections = await apEntities.Database.SqlQuery<ADRData>
                              ("[usp_GetAssociateADRDetail]  @EmployeeID,@FinancialYearID,@AdrCycleId",
                             new object[] {
                                  new SqlParameter("EmployeeID",EmployeeID),
                                  new SqlParameter("FinancialYearID",FinancialYearID),
                                  new SqlParameter("ADRCycleID",AdrCycleId),

                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getAdrSections;
        }
        #endregion

        #region UpdateAssociateADRDetail
        /// <summary>
        /// Update Associate ADR Detail
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        public async Task<int> UpdateAssociateADRDetail(ADRData adrData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateAssociateADRDetail] @AssociateADRMasterId,@AssociateKRAMapperId, @ADRCycleID,@KRADefinitionId,@Contribution,@SelfRating,@ManagerComments,@Rating, @ModifiedDate, @ModifiedUser, @SystemInfo",
                               new object[] {
                                         new SqlParameter("AssociateADRMasterId",adrData.AssociateADRMasterId),
                                         new SqlParameter("AssociateKRAMapperId",adrData.AssociateKRAMapperId),
                                         new SqlParameter("ADRCycleID",adrData.ADRCycleID),
                                         new SqlParameter("KRADefinitionId",adrData.KRADefinitionId),
                                         new SqlParameter("Contribution",adrData.Contribution),
                                         new SqlParameter("SelfRating",adrData.SelfRating),
                                         new SqlParameter("ManagerComments",adrData.ManagerComments),
                                         new SqlParameter("Rating",adrData.Rating),
                                         new SqlParameter ("ModifiedDate", adrData.ModifiedDate),
                                         new SqlParameter ("ModifiedUser", adrData.ModifiedBy),
                                         new SqlParameter ("SystemInfo", adrData.SystemInfo),
                                   }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region GetADRSection
        /// <summary>
        /// Get ADR Section
        /// </summary>
        /// <returns></returns>
        public async Task<List<ADRData>> GetADRSection()
        {
            List<ADRData> getAdrSections;
            try
            {
                using (var apEntities = new APEntities())
                {
                    getAdrSections = await apEntities.Database.SqlQuery<ADRData>
                              ("[usp_GetADRSection] ",
                             new object[] {
                                       
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getAdrSections;
        }
        #endregion
        #region UpdateADRSection
        /// <summary>
        /// Update ADR Section
        /// </summary>
        /// <param name="adrData"></param>
        /// <returns></returns>
        public async Task<int> UpdateADRSection(ADRData adrData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateADRSection] @ADRSectionId, @ADRSectionName, @ADRMeasurementAreaId,@DepartmentId, @ModifiedDate, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("ADRSectionId", adrData.ADRSectionId),
                                        new SqlParameter ("ADRSectionName", adrData.ADRSectionName),
                                        new SqlParameter ("ADRMeasurementAreaId", adrData.ADRMeasurementAreaId),
                                        new SqlParameter ("DepartmentId", Convert.ToInt32(adrData.DepartmentId)),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        /// <summary>
        /// Set ADR cycle active
        /// </summary>
        /// <param name="adrCycleDetail">kraSetData data model</param>
        /// <returns></returns>
        public async Task<bool> SetADRCycleActive(ADRCycleDetail adrCycleDetail)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_SetADRCycleActive] @ADRCycleID, @IsActive, @DateModified, @ModifiedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("ADRCycleID", adrCycleDetail.ADRCycleID),
                                        new SqlParameter ("IsActive", adrCycleDetail.IsActive),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }

        #region GetEmployeeKRAByEmployeeId
        /// <summary>
        /// Gets Employee KRA's by employeeId, Financial year and ADRCycleID
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAAspectData>> GetEmployeeKRAByEmployeeId(int EmployeeId, int FinancialYearId, int ADRCycleID)
        {
            List<KRAAspectData> lstKRA;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRA = await apEntities.Database.SqlQuery<KRAAspectData>
                              ("[usp_GetADRAssociateReviewData] @EmployeeID, @ADRCycleID, @FinancialYearID",
                             new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeId),
                                        new SqlParameter ("ADRCycleID", ADRCycleID),
                                        new SqlParameter ("FinancialYearID", FinancialYearId)
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRA;
        }
        #endregion

        #region GetCommentsByEmployeeId
        /// <summary>
        /// Gets all the comments of KRA Metrics by employeeId, Financial year and ADRCycleID
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAAspectData>> GetCommentsByEmployeeId(int EmployeeId, int FinancialYearId, int ADRCycleID)
        {
            List<KRAAspectData> lstComments;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstComments = await apEntities.Database.SqlQuery<KRAAspectData>
                              ("[usp_GetCommentsByEmployeeId] @EmployeeID, @ADRCycleID, @FinancialYearID",
                             new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeId),
                                        new SqlParameter ("ADRCycleID", ADRCycleID),
                                        new SqlParameter ("FinancialYearID", FinancialYearId)
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstComments;
        }
        #endregion

        #region GetMetricsAndCommentsByAspectId
        /// <summary>
        /// Gets KRA Metrics and Comments by employeeId, Financial year and ADRCycleID
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAAspectData>> GetMetricsAndCommentsByAspectId(int EmployeeId, int FinancialYearId, int ADRCycleID, int KRAAspectId)
        {
            List<KRAAspectData> lstMetrics;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstMetrics = await apEntities.Database.SqlQuery<KRAAspectData>
                              ("[usp_GetMetricsAndCommentsByAspectId] @EmployeeID, @ADRCycleID, @FinancialYearID, @KRAAspectID",
                             new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeId),
                                        new SqlParameter ("ADRCycleID", ADRCycleID),
                                        new SqlParameter ("FinancialYearID", FinancialYearId),
                                        new SqlParameter ("KRAAspectID", KRAAspectId)
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstMetrics;
        }
        #endregion

        #region GetCommentsByAspectAndMetricId
        /// <summary>
        /// Gets Comments by AspectId, MetricId, EmployeeId, Financial year and ADRCycleID.
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAAspectData>> GetCommentsByAspectAndMetricId(int EmployeeId, int FinancialYearId, int ADRCycleID, int KRAAspectId, int KRAMetricId)
        {
            List<KRAAspectData> lstComments;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstComments = await apEntities.Database.SqlQuery<KRAAspectData>
                              ("[usp_GetCommentsByAspectAndMetricId] @EmployeeID, @ADRCycleID, @FinancialYearID, @KRAAspectID, @KRAMetricID",
                             new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeId),
                                        new SqlParameter ("ADRCycleID", ADRCycleID),
                                        new SqlParameter ("FinancialYearID", FinancialYearId),
                                        new SqlParameter ("KRAAspectID", KRAAspectId),
                                        new SqlParameter ("KRAMetricID", KRAMetricId)
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstComments;
        }
        #endregion

        #region GetADRSection
        /// <summary>
        /// Get ADR Section
        /// </summary>
        /// <returns></returns>
        public async Task<List<ADRData>> GetCurrentYearADRMeasurementAreas()
        {
            List<ADRData> getAdrMeasurementAreas;
            try
            {
                using (var apEntities = new APEntities())
                {
                    getAdrMeasurementAreas = await apEntities.Database.SqlQuery<ADRData>
                              ("[usp_GetADRMeasurementAreasByCurrentFinancialYear] ",
                             new object[] {

                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getAdrMeasurementAreas;
        }
        #endregion





        
    }
}
