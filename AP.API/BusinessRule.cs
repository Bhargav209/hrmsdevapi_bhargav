﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.API
{
    public class BusinessRule
    {
        #region CreateBusinessRule
        /// <summary>
        /// CreateBusinessRule
        /// </summary>
        /// <param name="businessRuleData"></param>
        /// <returns></returns>
        public bool CreateBusinessRule(BusinessRuleData businessRuleData)
        {
            bool isCreated = false;

            using (APEntities hrmsEntities = new APEntities())
            {
                var isExists = (from br in hrmsEntities.BusinessRules
                                where br.BusinessRuleCode == businessRuleData.BusinessRuleCode
                                select br).Count();
                if (isExists == 0)
                {
                    DataStorage.BusinessRule businessRule = new DataStorage.BusinessRule();
                    businessRule.BusinessRuleCode = businessRuleData.BusinessRuleCode;
                    businessRule.Description = businessRuleData.Description;
                    businessRule.BusinessRuleValue = businessRuleData.BusinessRuleValue;
                    businessRule.IsActive = (bool)businessRuleData.IsActive;
                    businessRule.CreatedBy = businessRuleData.CurrentUser;
                    businessRule.CreatedDate = DateTime.Now;
                    businessRule.SystemInfo = businessRuleData.SystemInfo;
                    hrmsEntities.BusinessRules.Add(businessRule);
                    isCreated = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
                else
                    throw new AssociatePortalException("Business rule code already exists");
            }
            
            return isCreated;
        }
        #endregion

        #region UpdateBusinessRule
        /// <summary>
        /// UpdateBusinessRule
        /// </summary>
        /// <param name="businessRuleData"></param>
        /// <returns></returns>
        public bool UpdateBusinessRule(BusinessRuleData businessRuleData)
        {
            bool isUpdated = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    var isExists = (from br in hrmsEntities.BusinessRules
                                    where br.BusinessRuleCode == businessRuleData.BusinessRuleCode
                                    && br.BusinessRuleId != businessRuleData.BusinessRuleId
                                    select br).Count();

                    if (isExists == 0)
                    {
                        DataStorage.BusinessRule businessRule = hrmsEntities.BusinessRules.FirstOrDefault(bid => bid.BusinessRuleId == businessRuleData.BusinessRuleId);
                        businessRule.BusinessRuleCode = businessRuleData.BusinessRuleCode;
                        businessRule.Description = businessRuleData.Description;
                        businessRule.BusinessRuleValue = businessRuleData.BusinessRuleValue;
                        businessRule.IsActive = (bool)businessRuleData.IsActive;
                        businessRule.ModifiedBy = businessRuleData.CurrentUser;
                        businessRule.ModifiedDate = DateTime.Now;
                        businessRule.SystemInfo = businessRuleData.SystemInfo;
                        hrmsEntities.Entry(businessRule).State = System.Data.Entity.EntityState.Modified;
                        isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Business rule code already exists");
                }
            }
            catch
            {
                throw;
            }

            return isUpdated;
        }
        #endregion

        #region GetBusinessRuleDetails
        /// <summary>
        /// GetBusinessRuleDetails
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="businessRuleCode"></param>
        /// <returns></returns>
        public IEnumerable<object> GetBusinessRules(bool isActive, string businessRuleCode = null)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var query = (from br in hrmsEntities.BusinessRules
                                 select new
                                 {
                                     br.BusinessRuleId,
                                     br.BusinessRuleCode,
                                     br.Description,
                                     br.BusinessRuleValue,
                                     IsActive = br.IsActive == true ? "Yes" : "No"
                                 });

                    if (!string.IsNullOrWhiteSpace(businessRuleCode))
                        query = query.Where(e => e.BusinessRuleCode == businessRuleCode);

                    if (isActive)
                        query = query.Where(i => i.IsActive == "Yes");

                    return query.ToList().OrderBy(e => e.BusinessRuleCode);
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get business rule.");
            }
        }

        #endregion
    }
}
