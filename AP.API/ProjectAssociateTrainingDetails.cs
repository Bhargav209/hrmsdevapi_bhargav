﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.DataStorage;
using AP.Utility;


namespace AP.API
{
    public class ProjectAssociateTrainingDetails
    {
        #region ProjectAssociateTrainingMethods

        #region GetAssociateSkillGapByEmpId
        /// <summary>
        /// Get Associate Skill Gap details By EmpId
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetAssociateSkillGapByEmpId(int empId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    var getSkillGapforAssociate = (from assSkillgap in hrmsEntities.AssociateSkillGaps
                                                   join prskills in hrmsEntities.ProjectSkills on assSkillgap.ProjectSkillId equals prskills.ProjectSkillId
                                                   join skills in hrmsEntities.Skills on prskills.SkillId equals skills.SkillId
                                                   join emp in hrmsEntities.Employees on assSkillgap.EmployeeId equals emp.EmployeeId
                                                   join st in hrmsEntities.Status on assSkillgap.StatusId equals st.StatusId
                                                   where emp.EmployeeId == empId && st.StatusCode!="Closed"
                                                   select new
                                                   {
                                                       skills.SkillCode,
                                                       skills.SkillId,
                                                       assSkillgap.AssociateSkillGapId
                                                   }).OrderBy(i=>i.SkillCode).ToList();
                    return getSkillGapforAssociate;

                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region GetProjectTrainingsByProjectId
        /// <summary>
        /// Get Project Trainings By ProjectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetProjectTrainingsByProjectId(int projectId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getProjectTrainings = (from pt in hrmsEntities.ProjectTrainings
                                               join pr in hrmsEntities.Projects on pt.ProjectId equals pr.ProjectId
                                               where pt.ProjectId == projectId && pr.IsActive == true && pt.IsActive == true
                                               select new
                                               {
                                                   pt.ProjectTrainingId,
                                                   pt.ProjectTrainingName
                                               }).OrderBy(x => x.ProjectTrainingName).ToList();
                    return getProjectTrainings;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region GetAssessedByProjectID
        /// <summary>
        /// Get AssessedBy details by ProjectID
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        public IEnumerable<object> GetAssessedByProjectID(int projectID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var getManager = (from p in hrmEntities.Projects
                                      join projectmgrs in hrmEntities.ProjectManagers on p.ProjectId equals projectmgrs.ProjectID
                                      join e in hrmEntities.Employees on projectmgrs.ReportingManagerID equals e.EmployeeId
                                      where p.ProjectId == projectID && p.IsActive == true
                                      select new UserDetails
                                      {
                                          empID =(int) projectmgrs.ReportingManagerID,
                                          name = e.FirstName + " " + e.LastName                                                 
                                      }).Distinct().FirstOrDefault();

                    var getLead = (from p in hrmEntities.Projects
                                   join projectmgrs in hrmEntities.ProjectManagers on p.ProjectId equals projectmgrs.ProjectID
                                   join e in hrmEntities.Employees on projectmgrs.ReportingManagerID equals e.EmployeeId
                                   where p.ProjectId == projectID && p.IsActive == true
                                   select new UserDetails
                                   {
                                       empID =(int) projectmgrs.ReportingManagerID,
                                       name = e.FirstName + " " + e.LastName
                                   }).Distinct().FirstOrDefault();


                    var emps = (from aa in hrmEntities.AssociateAllocations
                                join emp in hrmEntities.Employees on aa.EmployeeId equals emp.EmployeeId
                                where aa.ProjectId == projectID && aa.IsActive == true && emp.IsActive == true                                
                                select new UserDetails
                                {
                                    empID = (int)aa.EmployeeId,
                                    name = emp.FirstName + " " + emp.LastName

                                }).Distinct().ToList();

                    int[] roleIds = new int[1] { (int)Enumeration.Roles.CompetencytLead };

                    var getCompetencyLead = (from emp in hrmEntities.Employees
                                             join ur in hrmEntities.UserRoles on emp.UserId equals ur.UserId
                                             where emp.IsActive == true && ur.IsActive == true
                                             && roleIds.Contains(ur.RoleId ?? 0)
                                             select new UserDetails
                                             {
                                                 empID = emp.EmployeeId,
                                                 name = emp.FirstName + " " + emp.LastName
                                             }).Distinct().ToList();

                    List<UserDetails> result = new List<UserDetails>();
                    result.AddRange(getCompetencyLead);
                    result.Add(getManager);
                    result.Add(getLead);
                    result.AddRange(emps);                    

                    return result.Select(i=>new { empID=i.empID,name=i.name }).Distinct().OrderBy(i=>i.name).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region GetProjectAssociateTrainings
        /// <summary>
        /// GetProjectAssociateTrainings
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="projectId"></param>
        /// <param name="projectAssociateTrainingId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetProjectAssociateTrainings(int empId, int projectId, int? projectAssociateTrainingId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAssociateTrainings = (from asstra in hrmsEntities.ProjectAssociateTrainings
                                                 join emp in hrmsEntities.Employees on asstra.EmployeeId equals emp.EmployeeId
                                                 join assskillgap in hrmsEntities.AssociateSkillGaps on asstra.AssociateSkillGapId equals assskillgap.AssociateSkillGapId
                                                 join prskill in hrmsEntities.ProjectSkills on assskillgap.ProjectSkillId equals prskill.ProjectSkillId
                                                 join sk in hrmsEntities.Skills on prskill.SkillId equals sk.SkillId
                                                 join prt in hrmsEntities.ProjectTrainings on asstra.ProjectTrainingId equals prt.ProjectTrainingId                                               
                                                 join p in hrmsEntities.Projects on prt.ProjectId equals p.ProjectId
                                                 join pl in hrmsEntities.ProficiencyLevels on asstra.ProficiencyLevelId equals pl.ProficiencyLevelId
                                                 join assedby in hrmsEntities.Employees on asstra.AssessedById equals assedby.EmployeeId
                                                 join st in hrmsEntities.Status on asstra.StatusId equals st.StatusId
                                                 where asstra.IsActive == true
                                                 && assskillgap.IsActive == true && sk.IsActive == true && prskill.IsActive == true && prt.IsActive == true
                                                 && pl.IsActive == true
                                                 select new ProjectAssociateTrainingData
                                                 {
                                                     ProjectAssociateTrainingId = asstra.ProjectAssociateTrainingId,
                                                     AssociateSkillGapId = assskillgap.AssociateSkillGapId,
                                                     SkillName = sk.SkillCode,
                                                     ProjectId = p.ProjectId,
                                                     ProjectName = p.ProjectName,
                                                     ProjectTrainingId = prt.ProjectTrainingId,
                                                     EmployeeId = emp.EmployeeId,
                                                     FirstName=emp.FirstName,
                                                     LastName=emp.LastName,                                                     
                                                     TrainingId = prt.ProjectTrainingId,
                                                     TrainingName = prt.ProjectTrainingName,
                                                     FromDate = asstra.FromDate,
                                                     ToDate = asstra.ToDate,
                                                     IsSkillApplied = asstra.IsSkillApplied==null?false:asstra.IsSkillApplied,
                                                     AssessedById = assedby.EmployeeId,
                                                     AssessedBy = assedby.FirstName + " " + assedby.LastName,
                                                     AssessedFirstName = assedby.FirstName,
                                                     AssessedLastName = assedby.LastName,
                                                     AssessedDate = asstra.AssessedDate,
                                                     ProficiencyLevelCode = pl.ProficiencyLevelCode,
                                                     ProficiencyLevelId = pl.ProficiencyLevelId,
                                                     StatusId = asstra.StatusId,
                                                     StatusCode=st.StatusCode,
                                                     Remarks=asstra.Remarks
                                                 }).OrderBy(i=>i.ProjectAssociateTrainingId).ToList();

                    getAssociateTrainings.Select(e => e. AssociateName = e.FirstName + " " + e.LastName).ToList();
                    if (empId !=0)
                        getAssociateTrainings = getAssociateTrainings.Where(i => i.EmployeeId == empId).ToList();

                    if (projectId != 0)
                        getAssociateTrainings = getAssociateTrainings.Where(i => i.ProjectId == projectId).ToList();

                    if (projectAssociateTrainingId != 0)
                        getAssociateTrainings = getAssociateTrainings.Where(i => i.ProjectAssociateTrainingId == projectAssociateTrainingId).ToList();


                    return getAssociateTrainings;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }

        #endregion

        #region SaveProjectAssociateTrainings
        /// <summary>
        /// Save Project Associate Trainings
        /// </summary>
        /// <param name="projectAssociateTraining"></param>
        /// <returns></returns>
        public bool SaveProjectAssociateTrainings(List<ProjectAssociateTrainingData> projectAssociateTraining)
        {
            bool IsValid = true;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    foreach (ProjectAssociateTrainingData data in projectAssociateTraining)
                    {                       

                        int associateSkillGapId = (from skill in hrmsEntities.Skills
                                                   join pskill in hrmsEntities.ProjectSkills on skill.SkillId equals pskill.SkillId
                                                   join asskill in hrmsEntities.AssociateSkillGaps on pskill.ProjectSkillId equals asskill.ProjectSkillId
                                                   where skill.SkillCode == data.SkillName && pskill.ProjectId == data.ProjectId && asskill.EmployeeId == data.EmployeeId
                                                   select asskill.AssociateSkillGapId).FirstOrDefault();
                        int ProjectTrainingId = (from pt in hrmsEntities.ProjectTrainings                                                  
                                                   where pt.ProjectId == data.ProjectId && pt.ProjectTrainingId == data.ProjectTrainingId
                                                 select pt.ProjectTrainingId).FirstOrDefault();

                        ProjectAssociateTraining pat = hrmsEntities.ProjectAssociateTrainings
                            .Where(i=>i.AssociateSkillGapId == associateSkillGapId
                            && i.EmployeeId == data.EmployeeId && i.ProjectTrainingId==ProjectTrainingId).FirstOrDefault();

                        if (pat == null)
                        {                            
                            pat = new ProjectAssociateTraining();
                            pat.AssociateSkillGapId = associateSkillGapId;
                            pat.ProjectTrainingId = ProjectTrainingId;
                            pat.EmployeeId = data.EmployeeId;
                            pat.FromDate = Commons.GetDateTimeInIST(data.FromDate);
                            pat.ToDate = Commons.GetDateTimeInIST(data.ToDate);
                            pat.AssessedById = data.AssessedById;
                            pat.AssessedDate = Commons.GetDateTimeInIST(data.AssessedDate);                           
                            pat.StatusId = GetStatusIDByStatusCode(data.Status);
                            pat.IsSkillApplied = data.IsSkillApplied;                            
                            if (data.ProficiencyLevelId != 0)
                                pat.ProficiencyLevelId = data.ProficiencyLevelId;
                            pat.IsActive = true;                            
                            pat.CreatedUser = data.CurrentUser;
                            pat.CreatedDate = DateTime.Now;
                            pat.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntities.ProjectAssociateTrainings.Add(pat);
                            if (associateSkillGapId == 0)
                            {
                                throw new AssociatePortalException("There is no training assigned for this skill gap.");
                            }
                            IsValid = hrmsEntities.SaveChanges() > 0 ? true : false;
                           
                        }
                        else
                        {
                            throw new AssociatePortalException("Same Training is already assigned for this Associate.");
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save details");
            }

            return IsValid;
        }
        #endregion

        #region UpdateProjectAssociateTrainings
        /// <summary>
        /// Update Project Associate Trainings
        /// </summary>
        /// <param name="projectAssociateTraining"></param>
        /// <returns></returns>
        public bool UpdateProjectAssociateTrainings(List<ProjectAssociateTrainingData> projectAssociateTraining)
        {
            bool IsValid = true;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    foreach (ProjectAssociateTrainingData data in projectAssociateTraining)
                    {
                        ProjectAssociateTraining pat = hrmsEntities.ProjectAssociateTrainings.Where(i => i.ProjectAssociateTrainingId == data.ProjectAssociateTrainingId).FirstOrDefault();

                        int associateSkillGapId = (from skill in hrmsEntities.Skills
                                                   join pskill in hrmsEntities.ProjectSkills on skill.SkillId equals pskill.SkillId
                                                   join asskill in hrmsEntities.AssociateSkillGaps on pskill.ProjectSkillId equals asskill.ProjectSkillId
                                                   where skill.SkillCode == data.SkillName && pskill.ProjectId == data.ProjectId && asskill.EmployeeId == data.EmployeeId
                                                   select asskill.AssociateSkillGapId).FirstOrDefault();

                        int ProjectTrainingId = (from pt in hrmsEntities.ProjectTrainings
                                                 where pt.ProjectId == data.ProjectId && pt.ProjectTrainingId == data.ProjectTrainingId
                                                 select pt.ProjectTrainingId).FirstOrDefault();

                        if (pat != null)
                        {
                            pat.AssociateSkillGapId = associateSkillGapId;
                            pat.ProjectTrainingId = ProjectTrainingId;
                            pat.EmployeeId = data.EmployeeId;
                            pat.FromDate = Commons.GetDateTimeInIST(data.FromDate);
                            pat.ToDate = Commons.GetDateTimeInIST(data.ToDate);
                            pat.AssessedById = data.AssessedById;
                            pat.AssessedDate = Commons.GetDateTimeInIST(data.AssessedDate);
                            pat.StatusId = GetStatusIDByStatusCode(data.Status);
                            pat.IsSkillApplied = data.IsSkillApplied;
                            if (data.IsSkillApplied == true && data.Status== "Assigned")
                            {
                                pat.StatusId = GetStatusIDByStatusCode("Closed");
                                ChangeAssociateSkillGapStatus(associateSkillGapId, "Closed");
                            }
                           
                            pat.Remarks = data.Remarks;
                            if (data.ProficiencyLevelId != 0)
                                pat.ProficiencyLevelId = data.ProficiencyLevelId;
                            pat.IsActive = true;
                            pat.ModifiedUser = data.CurrentUser;
                            pat.ModifiedDate = DateTime.Now;
                            pat.SystemInfo = Commons.GetClientIPAddress();
                            IsValid = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                        else
                        {
                            throw new AssociatePortalException("No Training details are available.");
                        }                        
                    }
                }
            }
            catch
            {
                throw;
            }

            return IsValid;
        }
        #endregion

        #region GetStatusIDByStatusCode
        public int GetStatusIDByStatusCode(string status)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    int statusId = (from s in hrmsEntities.Status
                                    where s.StatusCode == status && s.Category == "ProjectManagement"
                                    select s.StatusId).FirstOrDefault();
                    return statusId;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region GetProficiencyLevels
        /// <summary>
        /// GetProficiencyLevels
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetProficiencyLevels()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var proficiencyLevels = hrmsEntities.ProficiencyLevels.Select(p =>
                        new {
                            ID = p.ProficiencyLevelId,
                            Name = p.ProficiencyLevelCode,
                            p.IsActive                            
                        }).Where(i=>i.IsActive== true).OrderBy(plevel => plevel.Name).ToList();

                    if (proficiencyLevels.Count > 0)
                        return proficiencyLevels;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion

        #region ChangeAssociateSkillGapStatus
        /// <summary>
        /// ChangeAssociateSkillGapStatus
        /// </summary>
        /// <param name="associateSkillGapId"></param>
        public bool ChangeAssociateSkillGapStatus(int associateSkillGapId,string status)
        {
            bool result = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    AssociateSkillGap objAssociateSkillGap = hrmsEntities.AssociateSkillGaps.Where(i => i.AssociateSkillGapId == associateSkillGapId).FirstOrDefault();
                    objAssociateSkillGap.StatusId = GetStatusIDByStatusCode(status);
                    result = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to change details");
            }
            return result;
        }
        #endregion

        #endregion
    }
}
