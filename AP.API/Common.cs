﻿using AP.DataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using AP.Utility;
using System.Threading.Tasks;
using AP.DomainEntities;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace AP.API
{
    public class Common
    {
        #region GetBusinessValues
        /// <summary>
        /// GetBusinessValues
        /// </summary>
        /// <param name="valueKey"></param>
        /// <returns></returns>
        public IEnumerable<object> GetBusinessValues(string valueKey)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getBusinessValues = (from lkv in hrmsEntities.lkValues
                                             join vt in hrmsEntities.ValueTypes on lkv.ValueTypeKey equals vt.ValueTypeKey
                                             where vt.ValueTypeID == valueKey && lkv.IsActive == true
                                             select new
                                             {
                                                 valueID = lkv.ValueID,
                                                 valueName = lkv.ValueName,
                                                 valueKey = lkv.ValueKey
                                             }).ToList();

                    return getBusinessValues;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get business values.");
            }
        }
        #endregion

        #region GetStatusId
        /// <summary>
        /// GetStatusId
        /// </summary>
        /// <param name="category"></param>
        /// <param name="statusCode"></param>
        /// <returns>int</returns>
        public int GetStatusId(string category, string statusCode)
        {
            using (APEntities hrmEntities = new APEntities())
            {
                int query = (from Status in hrmEntities.Status
                             join Category in hrmEntities.CategoryMasters on Status.CategoryID equals Category.CategoryID
                             where Status.StatusCode == statusCode && Category.CategoryName == category && Status.IsActive == true
                             select new
                             {
                                 statusID = Status.StatusId
                             }).FirstOrDefault().statusID;
                return query;
            }
        }
        #endregion

        #region GetSkillsbySkillGroupID
        /// <summary>
        /// GetSkillsbySkillGroupID
        /// </summary>
        /// <param name="SkillGroupID"></param>
        /// <returns>List</returns>
        public async Task<List<SkillData>> GetSkillsbySkillGroupID(int SkillGroupID)
        {
            List<SkillData> lstSkills;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkills = await apEntities.Database.SqlQuery<SkillData>
                              ("[usp_GetSkillsbySkillGroupID] @SkillGroupID",
                              new object[] {
                                        new SqlParameter("SkillGroupID", SkillGroupID)}
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get skills.");
            }
            return lstSkills;
        }
        #endregion

        #region GetAllocationPercentages
        /// <summary>
        /// GetAllocationPercentages
        /// </summary>       
        public List<AllocationPercentageType> GetAllocationPercentages()
        {
            using (APEntities hrmEntities = new APEntities())
            {
                List<AllocationPercentageType> percentages = (from allpercentage in hrmEntities.AllocationPercentages
                             select new AllocationPercentageType
                             {
                                 Id = allpercentage.AllocationPercentageID,
                                 Percentage = allpercentage.Percentage

                             }).ToList();
                return percentages;
            }
        }
        #endregion

        public void CreateHRMSRepository(string EmployeeCode)
        {
            try
            {
                string filepath = ConfigurationManager.AppSettings["HRMSRepositoryPath"].ToString();

                //Checks root folder available or not
                bool isExists = Directory.Exists(filepath);

                if (!isExists)
                    Directory.CreateDirectory(filepath);

                // Checks folder with that employee code exits or not.
                //isExists = Directory.Exists(filepath + "/" + EmployeeCode);

                //if (!isExists)
                //    Directory.CreateDirectory(filepath + "/" + EmployeeCode);

                // Checks Onboarding folder with that employee code exits or not.
                isExists = Directory.Exists(filepath + "/" + EmployeeCode + "/OnBoarding/");

                if (!isExists)
                    Directory.CreateDirectory(filepath + "/" + EmployeeCode + "/OnBoarding/");

                // Checks KRA folder with that employee code exits or not.
                isExists = Directory.Exists(filepath + "/" + EmployeeCode + "/KRA/");

                if (!isExists)
                    Directory.CreateDirectory(filepath + "/" + EmployeeCode + "/KRA/");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
