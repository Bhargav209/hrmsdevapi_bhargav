﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class MasterData
    {
        #region GetProjectsList
        /// <summary>
        /// Retrives all the projects details
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<List<ProjectData>> GetProjectsList()
        {
            List<ProjectData> lstProjects;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstProjects = await apEntities.Database.SqlQuery<ProjectData>
                              ("[usp_GetProjects]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get active projects");
            }
            return lstProjects;
        }
        
        #endregion
        #region Get Associate Functions

        /// <summary>
        /// Gets associates names by department id
        /// </summary>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetAssociatesByDepartmentId(int departmentID)
        {
            List<GenericType> lstAssociates;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAssociates = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetAssociatesByDepartmentId] @DepartmentID",
                                new object[] {
                                    new SqlParameter("DepartmentID", departmentID)
                                        }).ToListAsync();

                    lstAssociates.OrderBy(employeName => employeName.Name);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAssociates;
        }

        /// <summary>
        /// Get Employees By projectid and departmentid
        /// </summary>
        /// <param name="projectID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        public async Task<List<AssociateRoleMappingData>> GetEmployeesByDepartmentAndProjectID(int? projectId, int departmentId, bool isNew)
        {
            List<AssociateRoleMappingData> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = await apEntities.Database.SqlQuery<AssociateRoleMappingData>
                              ("[usp_GetEmployeesByDepartmentAndProjectIDForKRA] @ProjectID, @DepartmentID, @IsNew",

                               new object[] {
                                        new SqlParameter ("ProjectID", (object)projectId??DBNull.Value),
                                        new SqlParameter ("DepartmentID", departmentId),
                                        new SqlParameter ("IsNew", isNew),
                             }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get employee details");
            }
            return lstEmployees.OrderBy(name => name.AssociateName).ToList();
        }

        /// <summary>
        /// Get Employees By projectid and departmentid
        /// </summary>
        /// <param name="projectID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        public async Task<List<AssociateRoleMappingData>> GetEmployeesForViewKraInformation(int? projectId, int departmentId,int financialYearId)
        {
            List<AssociateRoleMappingData> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = await apEntities.Database.SqlQuery<AssociateRoleMappingData>
                              ("[usp_GetEmployeesForViewKRAInformation] @ProjectID, @DepartmentID, @FinancialYear",

                               new object[] {
                                        new SqlParameter ("ProjectID", (object)projectId??DBNull.Value),
                                        new SqlParameter ("DepartmentID", departmentId),
                                        new SqlParameter ("FinancialYear", financialYearId),
                             }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get employee details");
            }
            return lstEmployees.OrderBy(name => name.AssociateName).ToList();
        }

        /// <summary>
        /// Get Employees By employeeID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<string> GetEmployeeNameByEmployeeId(int employeeID)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<string>
                              ("[usp_GetEmployeeNameByEmployeeId] @EmployeeId",

                               new object[] {
                                        new SqlParameter ("EmployeeId", employeeID)
                             }).FirstOrDefaultAsync();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get employee name");
            }
        }      

        #endregion

        #region Get Managers And Competency Leads

        /// <summary>
        /// Gets reporting managers, program managers and competency leads list
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetManagersAndCompetencyLeads()
        {
            List<GenericType> lstAssociates;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAssociates = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetManagersLeadsAndHeads]").ToListAsync();

                    lstAssociates.OrderBy(employeName => employeName.Name);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAssociates;
        }
        #endregion        

        #region Get Program Managers

        /// <summary>
        /// Gets program managers list
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetProgramManagers()
        {
            List<GenericType> lstAssociates;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAssociates = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetProgramManagers]").ToListAsync();

                    lstAssociates.OrderBy(employeName => employeName.Name);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAssociates;
        }
        #endregion      

        #region Get Financial Year Information
        /// <summary>
        /// Gets current financial year
        /// </summary>
        /// <returns></returns>
        public async Task<FinancialYearData> GetCurrentFinancialYear()
        {
            FinancialYearData lstFinancialYear = new FinancialYearData();
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    lstFinancialYear = await apEntities.Database.SqlQuery<FinancialYearData>
                              ("[usp_GetCurrentFinancialYear]", new object[] { }
                              ).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstFinancialYear;
        }

        /// <summary>
        /// Gets financial year list
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetFinancialYearList()
        {
            List<GenericType> lstFinancialYear;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstFinancialYear = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetFinancialYear]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstFinancialYear.OrderBy(n=>n.Name).ToList();
        }
        
        #endregion

        #region Get ADR Cycle
        /// <summary>
        /// Gets ADR Cycle
        /// </summary>
        /// <returns></returns>
        public async Task<List<ADRCycleDetail>> GetADRCycle()
        {
            List<ADRCycleDetail> lstADRCycle;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstADRCycle = await apEntities.Database.SqlQuery<ADRCycleDetail>
                              ("[usp_GetADRCycle]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstADRCycle;
        }
        #endregion

        #region Get Department Types

        /// <summary>
        /// Gets department types list
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetDepartmentTypes()
        {
            List<GenericType> lstDepartmentTypes;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstDepartmentTypes = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetDepartmentTypes]").ToListAsync();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDepartmentTypes;
        }
        #endregion    

        #region Get Department By Department Type Id

        /// <summary>
        /// Gets department by department type id
        /// </summary>
        /// <param name="departmentTypeId"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetDepartmentByDepartmentTypeId(int departmentTypeId)
        {
            List<GenericType> lstDepartments;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstDepartments = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetDepartmentByDepartmentTypeId] @DepartmentTypeId",
                                new object[] {
                                    new SqlParameter("DepartmentTypeId", departmentTypeId)
                                        }).ToListAsync();
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDepartments.OrderBy(departments => departments.Name).ToList();
        }
        #endregion

        #region GetDesignations
        /// <summary> 
        /// Gets list of designations by search string
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetDesignations(string searchString)
        {
            List<GenericType> lstDesignations;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstDesignations = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetDesignations] @SearchString",
                                new object[] {
                                    new SqlParameter("SearchString", searchString)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDesignations;
        }
        #endregion

    }
}
