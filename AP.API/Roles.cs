﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class Roles
    {

        #region CreateRole
        /// <summary>
        /// Create a new Role
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        public async Task<int> CreateRole(RoleData roleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateRoleMaster] @SGRoleID, @PrefixID, @SuffixID, @DepartmentID, @RoleDescription, @KeyResponsibilities, @EducationQualification, @CreatedDate, @CreatedUser, @SystemInfo, @KRAGroupID",
                                   new object[] {
                                        new SqlParameter ("SGRoleID", roleData.SGRoleID),
                                        new SqlParameter ("PrefixID", (object)roleData.PrefixID ?? DBNull.Value),
                                        new SqlParameter ("SuffixID", (object)roleData.SuffixID ?? DBNull.Value),
                                        new SqlParameter ("DepartmentID", roleData.DepartmentId),
                                        new SqlParameter ("RoleDescription",(object) roleData.RoleDescription??DBNull.Value),
                                        new SqlParameter ("KeyResponsibilities",(object) roleData.KeyResponsibilities??DBNull.Value),
                                        new SqlParameter ("EducationQualification",(object) roleData.EducationQualification??DBNull.Value),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("KRAGroupID", (object)roleData.KRAGroupID ?? DBNull.Value),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// Gets Roles
        /// </summary>
        /// <returns></returns>
        public async Task<List<RoleData>> GetRoles()
        {
            List<RoleData> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<RoleData>
                              ("[usp_GetRoles]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstRoles;
        }
        #endregion

        #region GetRoleByRoleID
        /// <summary>
        /// Gets Roles
        /// </summary>
        /// <returns></returns>
        public async Task<RoleData> GetRoleByRoleID(int roleID)
        {
            RoleData roleDetail;
            try
            {
                using (var apEntities = new APEntities())
                {
                    roleDetail = await apEntities.Database.SqlQuery<RoleData>
                              ("[usp_GetRolesByRoleID] @RoleID",
                              new object[] {
                                        new SqlParameter ("RoleID", roleID)
                                            }
                              ).FirstOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return roleDetail;
        }
        #endregion

        #region GetRoleSuffixAndPrefix
        /// <summary>
        /// Gets roles, suffixes and prefixes.
        /// </summary>
        /// <returns></returns>
        public async Task<SGEntity> GetRoleSuffixAndPrefix(int departmentId)
        {
            SGEntity entity = new SGEntity();
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var connection = apEntities.Database.Connection)
                    {
                        connection.Open();
                        var command = connection.CreateCommand();
                        command.CommandText = "EXEC [dbo].[usp_GetRoleSuffixAndPrefix] @DepartmentId";

                        SqlParameter param = new SqlParameter();
                        param.ParameterName = "@DepartmentId";
                        param.Value = departmentId;
                        command.Parameters.Add(param);

                        using (var reader = command.ExecuteReader())
                        {
                            entity.Roles = ((IObjectContextAdapter)apEntities)
                                        .ObjectContext
                                        .Translate<GenericType>(reader)
                                        .ToList();

                            reader.NextResult();
                            entity.Prefix = ((IObjectContextAdapter)apEntities)
                                        .ObjectContext
                                        .Translate<GenericType>(reader)
                                         .ToList();

                            reader.NextResult();
                            entity.Suffix = ((IObjectContextAdapter)apEntities)
                                          .ObjectContext
                                          .Translate<GenericType>(reader)
                                           .ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return entity;
        }
        #endregion

        /// <summary>
        /// Update a Role
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        public async Task<int> UpdateRole(RoleData roleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateRoleMaster] @RoleMasterID, @RoleDescription, @KeyResponsibilities, @EducationQualification, @ModifiedDate, @ModifiedUser, @SystemInfo, @KRAGroupID",
                                   new object[] {
                                        new SqlParameter ("RoleMasterID", roleData.RoleMasterId),
                                        new SqlParameter ("RoleDescription", roleData.RoleDescription),
                                        new SqlParameter ("KeyResponsibilities", roleData.KeyResponsibilities),
                                        new SqlParameter ("EducationQualification", roleData.EducationQualification),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("KRAGroupID", (object)roleData.KRAGroupID ?? DBNull.Value),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }

        #region GetRoleDetailsByDepartmentID
        /// <summary>
        ///  Get Roles by department ID.
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>List</returns>
        public async Task<List<RoleData>> GetRoleDetailsByDepartmentID(int DepartmentId)
        {
            List<RoleData> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<RoleData>
                              ("[usp_GetRoleDetailsByDepartmentID] @DepartmentID",
                              new object[] {
                                        new SqlParameter ("DepartmentID", DepartmentId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles.");
            }
            return lstRoles;
        }
        #endregion

        #region GetRolesByDepartmentID
        /// <summary>
        ///  Get Roles by department ID.
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetRolesByDepartmentID(int DepartmentId)
        {
            List<GenericType> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetRolesByDepartmentID] @DepartmentID",
                              new object[] {
                                        new SqlParameter ("DepartmentID", DepartmentId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles.");
            }
            return lstRoles;
        }
        #endregion

        #region GetKRARolesByDepartmentID
        /// <summary>
        ///  Get KRARoles By DepartmentID
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetKRARolesByDepartmentID(int DepartmentId)
        {
            List<GenericType> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKraGroupsByApprovedStatus] @DepartmentID",
                              new object[] {
                                        new SqlParameter ("DepartmentID", DepartmentId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles.");
            }
            return lstRoles;
        }
        #endregion

        #region AddSkillsToRole
        /// <summary>
        /// Create Role Skills
        /// </summary>
        /// <param name="roleData"></param>
        /// <returns></returns>
        public async Task<bool> AddSkillsToRole(RoleData roleData)
        {
            bool userRetValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    if (roleData.RoleCompetencySkills != null && roleData.RoleCompetencySkills.Count() > 0)
                    {
                        userRetValue = await AddSkillsToRole(roleData.RoleCompetencySkills, roleData.RoleMasterId, roleData.CurrentUser, roleData.SystemInfo);
                    }
                }
            }
            catch
            {
                throw;
            }

            return userRetValue;
        }
        #endregion

        public async Task<List<RoleCompetencySkills>> GetSkillsByRole(int RoleMasterID)
        {
            List<RoleCompetencySkills> lstSkills;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstSkills = await apEntities.Database.SqlQuery<RoleCompetencySkills>
                              ("[usp_GetSkillsByRole] @RoleMasterID",
                              new object[] {
                                        new SqlParameter ("RoleMasterID", RoleMasterID)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstSkills;
        }

        //#region UpdateRoleSkills
        ///// <summary>
        ///// Update Role Skills
        ///// </summary>
        ///// <param name="roleData"></param>
        ///// <returns></returns>
        //public bool UpdateRoleSkills(RoleData roleData)
        //{
        //    bool modifiedRetValue = false;

        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            foreach (var roleCompetencySkill in roleData.RoleCompetencySkills)
        //            {
        //                #region Add/Update RequisitionRoleSkills 

        //                var existRoleCompetencySkill = hrmsEntities.CompetencySkills.FirstOrDefault(r => r.RoleMasterID == roleData.RoleId && r.CompetencySkillsId == roleCompetencySkill.CompetencySkillsId);
        //                if (existRoleCompetencySkill == null)
        //                {
        //                    CompetencySkill competencySkill = new CompetencySkill()
        //                    {

        //                        RoleMasterID = roleData.RoleId,
        //                        CompetencyAreaId = roleCompetencySkill.CompetencyAreaId,
        //                        SkillId = roleCompetencySkill.SkillId,
        //                        ProficiencyLevelId = roleCompetencySkill.ProficiencyLevelId,
        //                        SkillGroupID = roleCompetencySkill.SkillGroupId,
        //                        CreatedUser = roleData.CurrentUser,
        //                        IsPrimary = roleCompetencySkill.IsPrimary,
        //                        IsActive = true,
        //                        CreatedDate = DateTime.Now,
        //                        SystemInfo = roleData.SystemInfo
        //                    };
        //                    hrmsEntities.CompetencySkills.Add(competencySkill);
        //                    modifiedRetValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //                }

        //                else
        //                {
        //                    existRoleCompetencySkill.CompetencyAreaId = roleCompetencySkill.CompetencyAreaId;
        //                    existRoleCompetencySkill.SkillId = roleCompetencySkill.SkillId;
        //                    existRoleCompetencySkill.IsPrimary = roleCompetencySkill.IsPrimary;
        //                    existRoleCompetencySkill.ProficiencyLevelId = roleCompetencySkill.ProficiencyLevelId;
        //                    existRoleCompetencySkill.SkillGroupID = roleCompetencySkill.SkillGroupId;
        //                    existRoleCompetencySkill.ModifiedUser = roleData.CurrentUser;
        //                    existRoleCompetencySkill.ModifiedDate = DateTime.Now;
        //                    hrmsEntities.Entry(existRoleCompetencySkill).State = EntityState.Modified;
        //                    modifiedRetValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //                }

        //                #endregion
        //            }

        //            #region Delete RequisitionRoleSkills

        //            if (roleData.DeltedSkillIds != null && roleData.DeltedSkillIds.Count > 0)
        //                modifiedRetValue = DeleteRoleCompetencySkills(roleData.DeltedSkillIds);

        //            #endregion
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return modifiedRetValue;
        //}
        //#endregion

        #region GetKRATitleByDepartmentID
        /// <summary>
        ///  Get Roles by department ID.
        /// </summary>
        /// <param name="departmentID"></param>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetKRATitleByDepartmentID(int departmentID)
        {
            List<GenericType> lstRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstRoles = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKRATitleByStatus] @DepartmentID",
                              new object[] {
                                        new SqlParameter ("DepartmentID", departmentID)
                              }).ToListAsync();
                    
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get roles.");
            }
            return lstRoles;
        }
        #endregion

        #region Get Status By Role
        /// <summary>
        ///  Get Status By Role.
        /// </summary>
        /// <param name="RoleName"></param>
        /// /// <param name="CategoryId"></param>
        /// <returns>List</returns>
        public async Task<List<GenericType>> GetStatusByRole(string RoleName, int CategoryId)
        {
            List<GenericType> lstStatus;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstStatus = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetStatusByRole] @RoleName, @CategoryId",
                              new object[] {
                                        new SqlParameter ("RoleName", RoleName),
                                        new SqlParameter ("CategoryId", CategoryId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get status by roles.");
            }
            return lstStatus;
        }
        #endregion

        #region PrivateMehtods
        private async Task<bool> AddSkillsToRole(IEnumerable<RoleCompetencySkills> roleCompetencySkills, int roleMasterID, string currentUser, string systemInfo)
        {
            bool returnValue = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                foreach (var skill in roleCompetencySkills)
                {
                    CompetencySkill roleCompetencySkill = new CompetencySkill()
                    {
                        RoleMasterID = roleMasterID,
                        CompetencyAreaId = skill.CompetencyAreaId,
                        SkillId = skill.SkillId,
                        ProficiencyLevelId = skill.ProficiencyLevelId,
                        SkillGroupID = skill.SkillGroupId,
                        CreatedUser = currentUser,
                        IsPrimary = skill.IsPrimary,
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        SystemInfo = systemInfo
                    };
                    hrmsEntities.CompetencySkills.Add(roleCompetencySkill);
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return returnValue;
        }

        //private bool DeleteRoleCompetencySkills(List<int> deleteids)
        //{
        //    bool returnValue = false;

        //    using (APEntities hrmsEntities = new APEntities())
        //    {
        //        var existCompetencySkills = hrmsEntities.CompetencySkills.Where(r => deleteids.Contains(r.CompetencySkillsId)).ToList();
        //        hrmsEntities.CompetencySkills.RemoveRange(existCompetencySkills);
        //        returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
        //    }

        //    return returnValue;
        //}
        #endregion

    }
}
