﻿using AP.DataStorage;
using AP.DomainEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class AssociateTransitionPlan
    {
        #region SaveTransitionPlan
        /// <summary>
        /// Save Transition Plan
        /// </summary>
        /// <param name="planData"></param>
        /// <returns>bool</returns>
        public bool SaveTransitionPlan(TransitionPlanData planData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                TransitionPlan transitionPlan = hrmsEntities.TransitionPlans.Where(i => i.TransitionId == planData.TransitionId).FirstOrDefault();
                if (transitionPlan == null)
                {
                    transitionPlan = new TransitionPlan();
                    transitionPlan.RoleMasterId = planData.RoleMasterId;
                    transitionPlan.ProjectId = planData.ProjectId;
                    transitionPlan.EmployeeId = planData.EmployeeId;
                    transitionPlan.StartDate = planData.StartDate;
                    transitionPlan.EndDate = planData.EndDate;
                    transitionPlan.StatusId = planData.StatusId;
                    transitionPlan.CreatedBy = HttpContext.Current.User.Identity.Name;
                    transitionPlan.CreatedDate = DateTime.Now;
                    hrmsEntities.TransitionPlans.Add(transitionPlan);
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
                else
                {
                    transitionPlan.RoleMasterId = planData.RoleMasterId;
                    transitionPlan.ProjectId = planData.ProjectId;
                    transitionPlan.EmployeeId = planData.EmployeeId;
                    transitionPlan.StartDate = planData.StartDate;
                    transitionPlan.EndDate = planData.EndDate;
                    transitionPlan.StatusId = planData.StatusId;
                    transitionPlan.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    transitionPlan.ModifiedDate = DateTime.Now;
                    hrmsEntities.Entry(transitionPlan).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region GetProjectByReportingManager
        /// <summary>
        /// Get Projects By ReportingManager
        /// </summary>
        /// <param name="reportingManagerId"></param>
        /// <returns>List</returns>
        public List<ProjectData> GetProjectByReportingManager(int reportingManagerId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                List<ProjectData> projectList = (from projects in hrmsEntities.Projects
                                                 join projectmgrs in hrmsEntities.ProjectManagers on projects.ProjectId equals projectmgrs.ProjectID
                                                 where projectmgrs.ReportingManagerID == reportingManagerId && projects.IsActive == true
                                                 select new ProjectData
                                                 {
                                                     ProjectId = projects.ProjectId,
                                                     ProjectName = projects.ProjectName
                                                 }).OrderBy(project => project.ProjectName).ToList();
                return projectList;
            }
        }
        #endregion

        #region GetRoleByEmployeeID
        /// <summary>
        /// Get Role By EmployeeID
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="projectId"></param>
        /// <returns>string</returns>
        public string GetRoleByEmployeeID(int employeeId, int projectId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                string roleName = (from associateAllocation in hrmsEntities.AssociateAllocations
                                   join r in hrmsEntities.RoleMasters.AsNoTracking() on associateAllocation.RoleMasterId equals r.RoleMasterID
                                   join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                   from sufflist in suffixtemp.DefaultIfEmpty()
                                   join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                   from preflist in prefixtemp.DefaultIfEmpty()
                                   join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID
                                   where associateAllocation.EmployeeId == employeeId && associateAllocation.ProjectId == projectId && associateAllocation.IsActive == true && r.IsActive == true
                                   select new
                                   {
                                       roleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName
                                   }).FirstOrDefault().ToString();
                return roleName;
            }
        }
        #endregion

        #region GetEmployeeByProjectIDAndReportingManagerID
        /// <summary>
        /// Get Employee By ProjectID and Reporting ManagerID
        /// </summary>
        /// <param name="reportingManagerId"></param>
        /// <param name="projectId"></param>
        /// <returns>List</returns>
        public List<AssociateAllocationDetails> GetEmployeeById(int reportingManagerId, int projectId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                List<AssociateAllocationDetails> associateAllocationList = (from associateAllocation in hrmsEntities.AssociateAllocations
                                                                            join employee in hrmsEntities.Employees on associateAllocation.EmployeeId equals employee.EmployeeId
                                                                            where associateAllocation.ReportingManagerId == reportingManagerId && associateAllocation.ProjectId == projectId && associateAllocation.IsActive == true
                                                                            select new AssociateAllocationDetails
                                                                            {
                                                                                EmployeeId = associateAllocation.EmployeeId,
                                                                                AssociateName = employee.FirstName + " " + employee.LastName,
                                                                            }).OrderBy(employee => employee.AssociateName).ToList();
                return associateAllocationList;
            }
        }
        #endregion

        #region GetTasksById
        /// <summary>
        /// GetTasksById
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>List</returns>
        public List<TransitionPlanDetailsData> GetTasksById(int categoryId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<TransitionPlanDetailsData> categoriesList = hrmsEntities.TransitionPlanTasks.Where(category => category.TransitionPlanCategoryId == categoryId).AsQueryable()
                                                                             .Select(categoryData => new TransitionPlanDetailsData
                                                                             {
                                                                                 TaskId = categoryData.TransitionPlanCategoryId,
                                                                                 TaskName = categoryData.Task
                                                                             });
                return categoriesList.ToList<TransitionPlanDetailsData>();
            }
        }
        #endregion

        #region GetCategories
        /// <summary>
        /// GetCategories
        /// </summary>
        /// <returns>List</returns>
        public List<TransitionPlanDetailsData> GetCategories()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<TransitionPlanDetailsData> categoriesList = hrmsEntities.TransitionPlanCategories.Where(category => category.IsActive == true).AsQueryable()
                                                                             .Select(categoryData => new TransitionPlanDetailsData
                                                                             {
                                                                                 CategoryId = categoryData.CategoryId,
                                                                                 CategoryName = categoryData.CategoryName
                                                                             });
                return categoriesList.ToList<TransitionPlanDetailsData>();
            }
        }
        #endregion

        #region SaveTransitionPlanDetails
        /// <summary>
        /// Save Transition Plan Details
        /// </summary>
        /// <param name="planDetailsData"></param>
        /// <returns>bool</returns>
        public bool SaveTransitionPlanDetails(List<TransitionPlanDetailsData> planDetails)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                List<TransitionPlanDetail> transitionPlanList = new List<TransitionPlanDetail>();
                List<TransitionPlanDetail> transitionPlanListUpdate = new List<TransitionPlanDetail>();
                foreach (var planDetail in planDetails)
                {
                    TransitionPlanDetail transitionPlan = hrmsEntities.TransitionPlanDetails.Where(i => i.ID == planDetail.ID).FirstOrDefault();
                    if (transitionPlan == null)
                    {
                        transitionPlan = new TransitionPlanDetail()
                        {
                            TransitionId = planDetail.TransitionId,
                            CategoryId = planDetail.CategoryId,
                            TaskId = planDetail.TaskId,
                            StartDate = planDetail.StartDate,
                            EndDate = planDetail.EndDate,
                            StatusId = planDetail.StatusId,
                            Remarks = planDetail.Remarks,
                            CreatedBy = HttpContext.Current.User.Identity.Name,
                            CreatedDate = DateTime.Now,
                        };
                        transitionPlanList.Add(transitionPlan);
                    }
                    else
                    {
                        transitionPlan.TransitionId = planDetail.TransitionId;
                        transitionPlan.CategoryId = planDetail.CategoryId;
                        transitionPlan.TaskId = planDetail.TaskId;
                        transitionPlan.StartDate = planDetail.StartDate;
                        transitionPlan.EndDate = planDetail.EndDate;
                        transitionPlan.StatusId = planDetail.StatusId;
                        transitionPlan.Remarks = planDetail.Remarks;
                        transitionPlan.ModifiedBy = HttpContext.Current.User.Identity.Name;
                        transitionPlan.ModifiedDate = DateTime.Now;

                        transitionPlanListUpdate.Add(transitionPlan);
                    }
                }
                if (transitionPlanList.Count > 0)
                    hrmsEntities.TransitionPlanDetails.AddRange(transitionPlanList);
                if (transitionPlanListUpdate.Count > 0)
                    transitionPlanListUpdate.ForEach(result => hrmsEntities.Entry(result).State = EntityState.Modified);

                status = hrmsEntities.SaveChanges() > 0 ? true : false;

            }
            return status;
        }
        #endregion

    }
}
