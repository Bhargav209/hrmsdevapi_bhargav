﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Notification;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class KRA
    {
        #region GetRoleCategory
        /// <summary>
        /// Gets KRA role category
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetRoleCategory()
        {
            List<GenericType> lstKRARoleCategories;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRARoleCategories = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKRARoleCategory]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRARoleCategories;
        }
        #endregion             

        #region SubmitKRA
        /// <summary>
        /// Submit KRAs for furthur action.
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        public async Task<int> SubmitKRA(KRAWorkFlowData kraWorkFlowData)
        {
            int rowsAffected = 0;
            string KRAGroupIDs = "";
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        KRAGroupIDs = string.Join(",", kraWorkFlowData.KRAGroupIDs.Select(ID => ID.Id));

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_SubmitKRA] @FinancialYearID, @StatusID, @FromEmployeeID, @ToEmployeeID, @DepartmentID, @KRAGroupID, @CreatedBy, @CreatedDate, @Systeminfo, @Comments",
                               new object[] {
                                        new SqlParameter ("FinancialYearID", kraWorkFlowData.FinancialYearID),
                                        new SqlParameter ("StatusID", kraWorkFlowData.StatusID),
                                        new SqlParameter ("FromEmployeeID", kraWorkFlowData.FromEmployeeID),
                                        new SqlParameter ("ToEmployeeID", (object)kraWorkFlowData.ToEmployeeID?? DBNull.Value),
                                        new SqlParameter ("DepartmentID", (object)kraWorkFlowData.DepartmentID?? DBNull.Value),
                                        new SqlParameter ("KRAGroupID", KRAGroupIDs),
                                        new SqlParameter ("CreatedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("Systeminfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("Comments", (object)kraWorkFlowData.Comments?? DBNull.Value),
                                   }
                                   ).SingleOrDefaultAsync();

                        trans.Commit();

                        if (rowsAffected == 1)
                        {
                            if (kraWorkFlowData.StatusID == Convert.ToInt32(Enumeration.KRA.SubmittedForDepartmentHeadReview))
                            {
                                EmailNotificationConfiguration emailNotificationConfig = getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.KRASubmittedForDHReview), Convert.ToInt32(Enumeration.CategoryMaster.KRA));
                                if (emailNotificationConfig != null)
                                {
                                    kRASubmittedForDepartmentHeadReviewNotification(kraWorkFlowData, emailNotificationConfig);
                                }
                            }

                            else if (kraWorkFlowData.StatusID == Convert.ToInt32(Enumeration.KRA.Approved))
                            {
                                EmailNotificationConfiguration emailNotificationConfig = getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.KRAApproved), Convert.ToInt32(Enumeration.CategoryMaster.KRA));
                                if (emailNotificationConfig != null)
                                {
                                    kRAAcceptedNotification(kraWorkFlowData, emailNotificationConfig);
                                }
                            }
                            else if (kraWorkFlowData.StatusID == Convert.ToInt32(Enumeration.KRA.SendBackForHRMReview))
                            {
                                EmailNotificationConfiguration emailNotificationConfig = getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.KRASendBackForHRMReview), Convert.ToInt32(Enumeration.CategoryMaster.KRA));
                                if (emailNotificationConfig != null)
                                {
                                    kRASendBackForHRMReview(kraWorkFlowData, emailNotificationConfig);
                                }
                            }
                            else if (kraWorkFlowData.StatusID == Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview))
                            {
                                EmailNotificationConfiguration emailNotificationConfig = getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.SubmittedForHRHeadReview), Convert.ToInt32(Enumeration.CategoryMaster.KRA));
                                if (emailNotificationConfig != null)
                                {
                                    kRASubmittedForHRHeadReview(kraWorkFlowData, emailNotificationConfig);
                                }
                            }
                            else if (kraWorkFlowData.StatusID == Convert.ToInt32(Enumeration.KRA.SendBackForDepartmentHeadReview))
                            {
                                EmailNotificationConfiguration emailNotificationConfig = getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.SendBackForDepartmentHeadReview), Convert.ToInt32(Enumeration.CategoryMaster.KRA));
                                if (emailNotificationConfig != null)
                                {
                                    kRASendBackForDepartmentHeadReview(kraWorkFlowData, emailNotificationConfig);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region GetKRARolesForReview
        /// <summary>
        /// Get KRA Roles For Review.
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        public async Task<List<KRASetData>> GetKRARolesForReview(int financialYearID, int departmentID)
        {
            List<KRASetData> roleList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        roleList = await apEntities.Database.SqlQuery<KRASetData>
                           ("[usp_GetKRARolesForReview] @FinancialYearID, @DepartmentID",
                                new object[] {
                                        new SqlParameter ("FinancialYearID", financialYearID),
                                        new SqlParameter ("DepartmentID", departmentID),
                               }
                               ).ToListAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return roleList;
        }
        #endregion

        #region GetKRARoles
        /// <summary>
        /// Get KRA Roles on dashboard by statusid.
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        public async Task<List<KRASetData>> GetKRARoles(int financialYearID, int departmentID, int statusID)
        {
            List<KRASetData> roleList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        roleList = await apEntities.Database.SqlQuery<KRASetData>
                           ("[usp_GetKRARoles] @FinancialYearID, @DepartmentID, @StatusID",
                                new object[] {
                                        new SqlParameter ("FinancialYearID", financialYearID),
                                        new SqlParameter ("DepartmentID", departmentID),
                                        new SqlParameter ("StatusID", statusID)
                               }
                               ).ToListAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return roleList;
        }
        #endregion

        /// <summary>
        /// Get KRA History
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRASetData>> GetKRAHistory()
        {
            List<KRASetData> lstKRAHistory;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAHistory = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_GetKRAHistory]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAHistory;
        }

        /// <summary>
        /// Get KRA WorkFlow History
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        public async Task<List<KRASetData>> GetKRAWorkFlowHistory(int financialYearID)
        {
            List<KRASetData> lstKRAWorkFlowHistory;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAWorkFlowHistory = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_GetkraWorkFlowHistory] @FinancialYearID",
                                new object[] {
                                    new SqlParameter("FinancialYearID", financialYearID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAWorkFlowHistory;
        }

        #region CloneKRAs
        /// <summary>
        /// Clone KRAs
        /// </summary>
        /// <param name="cloneKRAData"></param>
        /// <returns></returns>
        public async Task<int> CloneKRAs(CloneKRAData cloneKRAData)
        {
            int result;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        if (cloneKRAData.CloneType == 1)
                            result = await CloneKRAByFinancialYearId(cloneKRAData);
                        else if (cloneKRAData.CloneType == 2)
                            result = await CloneKRAByDepartmentId(cloneKRAData);
                        else
                            result = await CloneKRAByDepartmentAndGroupId(cloneKRAData);
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region CloneKRAByFinancialYearId
        /// <summary>
        /// Clone KRAs by FinancialYearId
        /// </summary>
        /// <param name="cloneKRAData"></param>
        /// <returns></returns>
        private async Task<int> CloneKRAByFinancialYearId(CloneKRAData cloneKRAData)
        {
            int result = 0;
            using (var hrmEntities = new APEntities())
            {
                using (var trans = hrmEntities.Database.BeginTransaction())
                {
                    result = await hrmEntities.Database.SqlQuery<int>
                              ("[usp_CloneKRAByFinancialYearId] @FromFinancialYearId, @ToFinancialYearId, @CreatedUser, @CreatedDate, @SystemInfo",
                                new object[] {
                                    new SqlParameter("FromFinancialYearId", cloneKRAData.FromFinancialYearId),
                                    new SqlParameter("ToFinancialYearId", cloneKRAData.ToFinancialYearId),
                                    new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("CreatedDate", DateTime.UtcNow),
                                    new SqlParameter("SystemInfo",  Commons.GetClientIPAddress())
                                }
                                ).SingleOrDefaultAsync();
                    trans.Commit();
                }
            }
            return result;
        }
        #endregion

        #region CloneKRAByDepartmentId
        /// <summary>
        /// Clone Kras by departmentIds
        /// </summary>
        /// <param name="cloneKRAData"></param>
        /// <returns></returns>
        private async Task<int> CloneKRAByDepartmentId(CloneKRAData cloneKRAData)
        {
            int rowsAffected = 0;
            using (var hrmEntities = new APEntities())
            {
                using (var trans = hrmEntities.Database.BeginTransaction())
                {
                    rowsAffected = await hrmEntities.Database.SqlQuery<int>
                                ("[usp_CloneKRAByDepartmentId] @FromFinancialYearId, @ToFinancialYearId, @DepartmentIds, @CreatedUser, @CreatedDate, @SystemInfo",
                                  new object[] {
                                    new SqlParameter("FromFinancialYearId", cloneKRAData.FromFinancialYearId),
                                    new SqlParameter("ToFinancialYearId", cloneKRAData.ToFinancialYearId),
                                    new SqlParameter("DepartmentIds", string.Join(",",cloneKRAData.DepartmentIds)),
                                    new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("CreatedDate", DateTime.UtcNow),
                                    new SqlParameter("SystemInfo",  Commons.GetClientIPAddress())
                                  }
                                  ).SingleOrDefaultAsync();
                    trans.Commit();
                }
            }
            return rowsAffected;
        }
        #endregion

        #region CloneKRAByDepartmentAndGroupId
        /// <summary>
        /// Clone Kras by departmentId and group ids
        /// </summary>
        /// <param name="cloneKRAData"></param>
        /// <returns></returns>
        private async Task<int> CloneKRAByDepartmentAndGroupId(CloneKRAData cloneKRAData)
        {
            int rowsAffected = 0;
            using (var hrmEntities = new APEntities())
            {
                using (var trans = hrmEntities.Database.BeginTransaction())
                {
                    foreach (var departmentId in cloneKRAData.DepartmentIds)
                    {
                        foreach (var groupId in cloneKRAData.GroupIds)
                        {
                            rowsAffected = await hrmEntities.Database.SqlQuery<int>
                                      ("[usp_CloneKRAByDepartmentAndGroupId] @FromFinancialYearId, @ToFinancialYearId, @DepartmentIds, @GroupIds, @CreatedUser, @CreatedDate, @SystemInfo",
                                        new object[] {
                                    new SqlParameter("FromFinancialYearId", cloneKRAData.FromFinancialYearId),
                                    new SqlParameter("ToFinancialYearId", cloneKRAData.ToFinancialYearId),
                                    new SqlParameter("DepartmentIds", string.Join(",",cloneKRAData.DepartmentIds)),
                                    new SqlParameter("GroupIds", string.Join(",",cloneKRAData.GroupIds)),
                                    new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("CreatedDate", DateTime.UtcNow),
                                    new SqlParameter("SystemInfo",  Commons.GetClientIPAddress())
                                        }
                                        ).SingleOrDefaultAsync();
                        }
                    }
                    trans.Commit();
                }
                return rowsAffected;
            }
        }
        #endregion

        #region DeleteKRARoleByFinancialYearAndRoleId
        /// <summary>
        /// DeleteKRARoleByFinancialYearAndRoleId
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteKRARoleByFinancialYearAndRoleId(int roleID, int financialYearId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteKRARoleByFinancialYearAndRoleId] @RoleID, @FinancialYearID",
                                   new object[] {
                                        new SqlParameter ("RoleID", roleID),
                                        new SqlParameter ("FinancialYearID", financialYearId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting kra role aspect.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region KRAPendingForReviewByHOD
        /// <summary>
        /// KRAPendingForReviewByHOD
        /// </summary>
        /// <param name="ToEmployeeID"></param>
        /// <returns></returns>
        public async Task<List<KRASetData>> KRAPendingForReviewByHOD(int toEmployeeID)
        {
            List<KRASetData> lstPendingForReviewKRA;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstPendingForReviewKRA = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_KRAPendingForReviewByHOD] @ToEmployeeID",
                                new object[] {
                                    new SqlParameter("ToEmployeeID", toEmployeeID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstPendingForReviewKRA;
        }
        #endregion

        #region KRAPendingForReviewByHRHead
        /// <summary>
        /// KRAPendingForReviewByHRHead
        /// </summary>
        /// <param name="ToEmployeeID"></param>
        /// <returns></returns>
        public async Task<List<KRASetData>> KRAPendingForReviewByHRHead(int toEmployeeID)
        {
            List<KRASetData> lstPendingForReviewKRA;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstPendingForReviewKRA = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_KRAPendingForReviewByHOD] @ToEmployeeID",
                                new object[] {
                                    new SqlParameter("ToEmployeeID", toEmployeeID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstPendingForReviewKRA;
        }
        #endregion

        #region GetKRADepartmentStatus
        /// <summary>
        /// GetKRADepartmentStatus
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRASetData>> GetKRADepartmentStatus()
        {
            List<KRASetData> lstKRADepartmentStatus;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRADepartmentStatus = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_GetKRADepartmentStatus]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRADepartmentStatus;
        }
        #endregion

        #region GetKRADepartmentWorkFlow
        /// <summary>
        /// GetKRADepartmentWorkFlow
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRASetData>> GetKRADepartmentWorkFlow(int financialYearID, int departmentID)
        {
            List<KRASetData> lstKRADepartmentWorkFlow;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRADepartmentWorkFlow = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_GetKRADepartmentWorkFlow] @FinancialYearID, @DepartmentID",
                                new object[] {
                                    new SqlParameter ("FinancialYearID", financialYearID),
                                    new SqlParameter ("DepartmentID", departmentID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRADepartmentWorkFlow;
        }
        #endregion

        #region KRAPendingForReviewByMD
        /// <summary>
        /// KRAPendingForReviewByMD
        /// </summary>
        /// <param name="toEmployeeID"></param>
        /// <returns></returns>
        public async Task<List<KRASetData>> KRAPendingForReviewByMD(int employeeID)
        {
            List<KRASetData> lstPendingForReviewKRA;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstPendingForReviewKRA = await apEntities.Database.SqlQuery<KRASetData>
                              ("[usp_KRAPendingForReviewByMD] @EmployeeID",
                                new object[] {
                                    new SqlParameter("EmployeeID", employeeID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstPendingForReviewKRA;
        }
        #endregion

        #region getEmailTemplate
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationTypeID"></param>
        /// <returns></returns>
        public EmailNotificationConfiguration getEmailTemplate(int notificationTypeID, int categoryId)
        {
            using (var hrmEntities = new APEntities())
            {
                return hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                           ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                             new object[]  {
                                    new SqlParameter ("NotificationTypeID", notificationTypeID),
                                    new SqlParameter ("CategoryId", categoryId)
                            }
                           ).FirstOrDefault();
            }
        }
        #endregion

        #region kRASubmittedForDepartmentHeadReviewNotification
        /// <summary>
        /// Notification To Submit kra 
        /// </summary>
        /// <returns></returns>
        private void kRASubmittedForDepartmentHeadReviewNotification(KRAWorkFlowData kraWorkFlowData, EmailNotificationConfiguration emailNotificationConfig)
        {
            int statusID = Convert.ToInt32(Enumeration.KRA.SubmittedForDepartmentHeadReview);
            int toEmployeeId = getToEmployeeID(kraWorkFlowData, statusID, kraWorkFlowData.DepartmentID);

            string toEmailID = getEmployeeEmail(toEmployeeId);
            string submittedToName = getEmployeeName(toEmployeeId);

            string submittedByName = getEmployeeName(kraWorkFlowData.FromEmployeeID);
            string departmentName = getDepartmentName(kraWorkFlowData.DepartmentID);
            string financialYear = getFinancialYear(kraWorkFlowData.FinancialYearID);

            NotificationDetail notificationDetail = new NotificationDetail();
            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

            emailContent = emailContent.Replace("{ToEmployeeName}", submittedToName)
                                       .Replace("{DepartmentName}", departmentName)
                                       .Replace("{FromEmployeeName}", submittedByName)
                                       .Replace("{FinancialYear}", financialYear);

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                throw new AssociatePortalException("Email From cannot be blank");
            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                throw new AssociatePortalException("Email To cannot be blank");

            notificationDetail.ToEmail = toEmailID;

            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

            StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
            Subject = Subject.Replace("{FinancialYear}", financialYear);
            notificationDetail.Subject = Subject.ToString();

            notificationDetail.EmailBody = emailContent.ToString();
            //Send Email Notification.
            NotificationManager.SendEmail(notificationDetail);
        }
        #endregion

        #region kRAAcceptedNotification
        /// <summary>
        /// Notification To accept kra
        /// </summary>
        /// <returns></returns>
        private void kRAAcceptedNotification(KRAWorkFlowData kraWorkFlowData, EmailNotificationConfiguration emailNotificationConfig)
        {
            int statusID = Convert.ToInt32(Enumeration.KRA.Approved);

            int toEmployeeId = getToEmployeeID(kraWorkFlowData, statusID, kraWorkFlowData.DepartmentID);
            List<int> toHRMEmployeeId = getHRMEmployeeID(kraWorkFlowData, kraWorkFlowData.DepartmentID);

            List<EmailList> toEmployee = new List<EmailList>();
            //DEpartment head
            string toEmailID = getEmployeeEmail(toEmployeeId);
            string submittedToName = getEmployeeName(toEmployeeId);

            EmailList objEmailList = new EmailList();
            objEmailList.EmailId = toEmailID;
            objEmailList.Username = submittedToName;
            toEmployee.Add(objEmailList);

            foreach (int hrmEmployee in toHRMEmployeeId)
            {
                //HRM 
                string toHRMEmailID = getEmployeeEmail(hrmEmployee);
                string submittedToHRMName = getEmployeeName(hrmEmployee);

                objEmailList = new EmailList();
                objEmailList.EmailId = toHRMEmailID;
                objEmailList.Username = submittedToHRMName;
                toEmployee.Add(objEmailList);
            }

            string submittedByName = getEmployeeName(kraWorkFlowData.FromEmployeeID);
            string departmentName = getDepartmentName(kraWorkFlowData.DepartmentID);
            string financialYear = getFinancialYear(kraWorkFlowData.FinancialYearID);

            NotificationDetail notificationDetail = new NotificationDetail();

            foreach (var employee in toEmployee)
            {
                StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));
                emailContent = emailContent.Replace("{ToEmployeeName}", employee.Username)
                                       .Replace("{DepartmentName}", departmentName)
                                       .Replace("{FromEmployeeName}", submittedByName)
                                       .Replace("{FinancialYear}", financialYear);

                if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                    throw new AssociatePortalException("Email From cannot be blank");
                notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;


                if (string.IsNullOrEmpty(employee.EmailId))
                    throw new AssociatePortalException("Email To cannot be blank");

                notificationDetail.ToEmail = employee.EmailId;

                notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
                Subject = Subject.Replace("{FinancialYear}", financialYear);
                notificationDetail.Subject = Subject.ToString();

                notificationDetail.EmailBody = emailContent.ToString();
                //Send Email Notification.
                NotificationManager.SendEmail(notificationDetail);
            }


        }
        #endregion

        #region kRASendBackForHRMReview
        /// <summary>
        /// Notification To accept kra
        /// </summary>
        /// <returns></returns>
        private void kRASendBackForHRMReview(KRAWorkFlowData kraWorkFlowData, EmailNotificationConfiguration emailNotificationConfig)
        {
            int statusID = Convert.ToInt32(Enumeration.KRA.SendBackForHRMReview);
            int toEmployeeId = getToEmployeeID(kraWorkFlowData, statusID, kraWorkFlowData.DepartmentID);

            string toEmailID = getEmployeeEmail(toEmployeeId);
            string submittedToName = getEmployeeName(toEmployeeId);

            string submittedByName = getEmployeeName(kraWorkFlowData.FromEmployeeID);
            string departmentName = getDepartmentName(kraWorkFlowData.DepartmentID);
            string financialYear = getFinancialYear(kraWorkFlowData.FinancialYearID);

            NotificationDetail notificationDetail = new NotificationDetail();
            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

            emailContent = emailContent.Replace("{ToEmployeeName}", submittedToName)
                                       .Replace("{DepartmentName}", departmentName)
                                       .Replace("{FromEmployeeName}", submittedByName)
                                       .Replace("{FinancialYear}", financialYear)
                                       .Replace("{Comments}", kraWorkFlowData.Comments);

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                throw new AssociatePortalException("Email From cannot be blank");
            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                throw new AssociatePortalException("Email To cannot be blank");

            notificationDetail.ToEmail = toEmailID;

            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

            StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
            Subject = Subject.Replace("{FinancialYear}", financialYear);
            notificationDetail.Subject = Subject.ToString();

            notificationDetail.EmailBody = emailContent.ToString();
            //Send Email Notification.
            NotificationManager.SendEmail(notificationDetail);
        }
        #endregion

        #region kRASubmittedForHRHeadReview
        /// <summary>
        /// Notification To accept kra
        /// </summary>
        /// <returns></returns>
        private void kRASubmittedForHRHeadReview(KRAWorkFlowData kraWorkFlowData, EmailNotificationConfiguration emailNotificationConfig)
        {
            int statusID = Convert.ToInt32(Enumeration.KRA.SubmittedForHRHeadReview);
            int toEmployeeId = getToEmployeeID(kraWorkFlowData, statusID, kraWorkFlowData.DepartmentID);

            string toEmailID = getEmployeeEmail(toEmployeeId);
            string submittedToName = getEmployeeName(toEmployeeId);

            string submittedByName = getEmployeeName(kraWorkFlowData.FromEmployeeID);
            string departmentName = getDepartmentName(kraWorkFlowData.DepartmentID);
            string financialYear = getFinancialYear(kraWorkFlowData.FinancialYearID);

            NotificationDetail notificationDetail = new NotificationDetail();
            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

            emailContent = emailContent.Replace("{ToEmployeeName}", submittedToName)
                                       .Replace("{DepartmentName}", departmentName)
                                       .Replace("{FromEmployeeName}", submittedByName)
                                       .Replace("{FinancialYear}", financialYear);

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                throw new AssociatePortalException("Email From cannot be blank");
            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                throw new AssociatePortalException("Email To cannot be blank");

            notificationDetail.ToEmail = toEmailID;

            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

            StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
            Subject = Subject.Replace("{FinancialYear}", financialYear);
            notificationDetail.Subject = Subject.ToString();

            notificationDetail.EmailBody = emailContent.ToString();
            //Send Email Notification.
            NotificationManager.SendEmail(notificationDetail);
        }
        #endregion

        #region kRASendBackForDepartmentHeadReview
        /// <summary>
        /// Notification To accept kra
        /// </summary>
        /// <returns></returns>
        private void kRASendBackForDepartmentHeadReview(KRAWorkFlowData kraWorkFlowData, EmailNotificationConfiguration emailNotificationConfig)
        {
            int statusID = Convert.ToInt32(Enumeration.KRA.SendBackForDepartmentHeadReview);
            int toEmployeeId = getToEmployeeID(kraWorkFlowData, statusID, kraWorkFlowData.DepartmentID);

            string toEmailID = getEmployeeEmail(toEmployeeId);
            string submittedToName = getEmployeeName(toEmployeeId);

            string submittedByName = getEmployeeName(kraWorkFlowData.FromEmployeeID);
            string departmentName = getDepartmentName(kraWorkFlowData.DepartmentID);
            string financialYear = getFinancialYear(kraWorkFlowData.FinancialYearID);

            NotificationDetail notificationDetail = new NotificationDetail();
            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

            emailContent = emailContent.Replace("{ToEmployeeName}", submittedToName)
                                       .Replace("{DepartmentName}", departmentName)
                                       .Replace("{FromEmployeeName}", submittedByName)
                                       .Replace("{FinancialYear}", financialYear);

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                throw new AssociatePortalException("Email From cannot be blank");
            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                throw new AssociatePortalException("Email To cannot be blank");

            notificationDetail.ToEmail = toEmailID;

            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

            StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
            Subject = Subject.Replace("{FinancialYear}", financialYear);
            notificationDetail.Subject = Subject.ToString();

            notificationDetail.EmailBody = emailContent.ToString();
            //Send Email Notification.
            NotificationManager.SendEmail(notificationDetail);
        }
        #endregion

        #region getEmployeeName
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        private string getEmployeeName(int employeeID)
        {
            using (var hrmEntities = new APEntities())
            {
                return hrmEntities.Employees.Where(submittedto => submittedto.EmployeeId == employeeID).Select(name => name.FirstName + " " + name.LastName).FirstOrDefault();
            }
        }
        #endregion

        #region getEmployeeEmail
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        private string getEmployeeEmail(int employeeID)
        {
            using (var hrmEntities = new APEntities())
            {
                return (from employe in hrmEntities.Employees
                        join user in hrmEntities.Users
                        on employe.UserId equals user.UserId
                        where employe.EmployeeId == employeeID
                        select new
                        {
                            user.EmailAddress
                        }).FirstOrDefault().EmailAddress;
            }
        }
        #endregion

        #region getDepartmentName
        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentID"></param>
        /// <returns></returns>
        private string getDepartmentName(int departmentID)
        {
            using (var hrmEntities = new APEntities())
            {
                return hrmEntities.Departments.Where(department => department.DepartmentId == departmentID).Select(code => code.Description).FirstOrDefault();
            }
        }
        #endregion

        #region getFinancialYear
        /// <summary>
        /// 
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        private string getFinancialYear(int financialYearID)
        {
            using (var hrmEntities = new APEntities())
            {
                return hrmEntities.FinancialYears.Where(year => year.ID == financialYearID).Select(financialYear => financialYear.FromYear + " - " + financialYear.ToYear).FirstOrDefault();
            }
        }
        #endregion

        #region getToEmployeeID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <param name="statusID"></param>
        /// <returns></returns>
        private int getToEmployeeID(KRAWorkFlowData kraWorkFlowData, int statusID, int departmentID)
        {
            using (var hrmEntities = new APEntities())
            {
                return hrmEntities.KRAWorkFlows.Where(kra => kra.FinancialYearID == kraWorkFlowData.FinancialYearID &&
                                                                           kra.FromEmployeeID == kraWorkFlowData.FromEmployeeID &&
                                                                           kra.DepartmentID == departmentID &&
                                                                           kra.StatusID == statusID).OrderByDescending(workflowid => workflowid.WorkFlowID)
                                                                           .Select(id => id.ToEmployeeID)
                                                                           .FirstOrDefault().Value;
            }
        }
        #endregion

        #region getHRMEmployeeID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <param name="statusID"></param>
        /// <returns></returns>
        private List<int> getHRMEmployeeID(KRAWorkFlowData kraWorkFlowData, int departmentID)
        {
            using (var hrmEntities = new APEntities())
            {
                int statusID = Convert.ToInt32(Enumeration.KRA.SubmittedForDepartmentHeadReview);
                return hrmEntities.KRAWorkFlows.Where(kra => kra.FinancialYearID == kraWorkFlowData.FinancialYearID &&
                                                                           kra.DepartmentID == kraWorkFlowData.DepartmentID &&
                                                                           kra.StatusID == statusID).OrderBy(workflowid => workflowid.WorkFlowID)
                                                                           .Select(id => id.FromEmployeeID)
                                                                           .Distinct().ToList();
            }
        }
        #endregion

        #region CreateKRAForRole
        /// <summary>
        /// Create KRA for role
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        public async Task<bool> CreateKRAForRole(KRARoleData kraRoleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateKRAForRoleByHRHead] @RoleID, @FinancialYearID, @StatusID, @KRAAspectID, @KRAAspectMetric, @KRAAspectTarget, @DateCreated, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("RoleID", kraRoleData.RoleID),
                                        new SqlParameter ("FinancialYearID", kraRoleData.FinancialYearID),
                                        new SqlParameter ("StatusID", kraRoleData.StatusID),
                                        new SqlParameter ("KRAAspectID", kraRoleData.KRAAspectID),
                                        new SqlParameter ("KRAAspectMetric", kraRoleData.KRAAspectMetric),
                                        new SqlParameter ("KRAAspectTarget", kraRoleData.KRAAspectTarget),
                                        new SqlParameter ("DateCreated", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }

        /// <summary>
        /// Create KRA for role
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        public async Task<bool> CreateKRAForRoleByDepartmentHead(KRARoleData kraRoleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateKRAForRoleByDepartmentHead] @RoleID, @FinancialYearID, @StatusID, @KRAAspectID, @KRAAspectMetric, @KRAAspectTarget, @DateCreated, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("RoleID", kraRoleData.RoleID),
                                        new SqlParameter ("FinancialYearID", kraRoleData.FinancialYearID),
                                        new SqlParameter ("StatusID", kraRoleData.StatusID),
                                        new SqlParameter ("KRAAspectID", kraRoleData.KRAAspectID),
                                        new SqlParameter ("KRAAspectMetric", kraRoleData.KRAAspectMetric),
                                        new SqlParameter ("KRAAspectTarget", kraRoleData.KRAAspectTarget),
                                        new SqlParameter ("DateCreated", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetDepartmentsByDepartmentHeadID
        /// <summary>
        /// GetDepartmentsByDepartmentHeadID
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetDepartmentsByDepartmentHeadID(int employeeID)
        {
            List<GenericType> lstDepartmentDetails;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstDepartmentDetails = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetDepartmentsByDepartmentHeadID] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", employeeID)
                               }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDepartmentDetails.OrderBy(dept => dept.Name).ToList();
        }
        #endregion

        #region UpdateKRARoleMetric
        /// <summary>
        /// Update a KRA Role Metric
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateKRARoleMetricByDepartmentHead(KRARoleData kraRoleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateKRARoleMetricByDepartmentHead] @ID, @KRAAspectMetric, @KRAAspectTarget, @StatusID, @RoleMasterID, @FinancialYearID, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("ID", kraRoleData.ID),
                                        new SqlParameter ("KRAAspectMetric", kraRoleData.KRAAspectMetric),
                                        new SqlParameter ("KRAAspectTarget", kraRoleData.KRAAspectTarget),
                                        new SqlParameter ("StatusID", kraRoleData.StatusID),
                                        new SqlParameter ("RoleMasterID", kraRoleData.RoleID),
                                        new SqlParameter ("FinancialYearID", kraRoleData.FinancialYearID),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                               }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }

        /// <summary>
        /// Update a KRA Role Metric
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateKRARoleMetricByHRHead(KRARoleData kraRoleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateKRARoleMetricByHRHead] @ID, @KRAAspectMetric, @KRAAspectTarget, @StatusID, @RoleMasterID, @FinancialYearID, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("ID", kraRoleData.ID),
                                        new SqlParameter ("KRAAspectMetric", kraRoleData.KRAAspectMetric),
                                        new SqlParameter ("KRAAspectTarget", kraRoleData.KRAAspectTarget),
                                        new SqlParameter ("StatusID", kraRoleData.StatusID),
                                        new SqlParameter ("RoleMasterID", kraRoleData.RoleID),
                                        new SqlParameter ("FinancialYearID", kraRoleData.FinancialYearID),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                               }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetLoggedInUserRoles
        /// <summary>
        /// GetLoggedInUserRoles
        /// </summary>
        /// <param name="financialYearID"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetLoggedInUserRoles(int employeeID, int finanicalYearId)
        {
            List<GenericType> roleList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        roleList = await apEntities.Database.SqlQuery<GenericType>
                           ("[usp_GetAssociateKRARoles] @FinancialYearID, @EmployeeID",
                                new object[] {
                                        new SqlParameter ("EmployeeID", employeeID),
                                        new SqlParameter ("FinancialYearID", finanicalYearId)
                               }
                               ).ToListAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return roleList;
        }
        #endregion

        #region GetKRAWorkFlowPendingWithEmployeeID
        /// <summary>
        /// Returns the employee ID with whom KRA Work Flow is pending.
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <param name="financialYearID"></param>
        /// <returns>employee ID</returns>
        public int GetKRAWorkFlowPendingWithEmployeeID(int financialYearID, int departmentID)
        {
            using (var apEntities = new APEntities())
            {
                return apEntities.KRAWorkFlows.Where(kraWorkFlow => kraWorkFlow.DepartmentID == departmentID && kraWorkFlow.FinancialYearID == financialYearID)
                                     .OrderByDescending(kraWorkFlow => kraWorkFlow.WorkFlowID)
                                     .Select(kraWorkFlow => kraWorkFlow.ToEmployeeID).FirstOrDefault().Value;
            }
        }
        #endregion

        #region GetOrganizationAndCustomKRAsForPMDH
        /// <summary>
        /// This method is used to get organization and custom KRAs when program manager or DH logs in. Displayed in dashboard.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        public async Task<AssociateKRAs> GetOrganizationAndCustomKRAsForEmployee(int employeeID, int groupId, int financialYearID)
        {
            AssociateKRAs associateKRAs = new AssociateKRAs();
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var connection = apEntities.Database.Connection)
                    {
                        connection.Open();
                        var command = connection.CreateCommand();
                        command.CommandText = "EXEC [dbo].[usp_GetOrganizationAndCustomKRAsForEmployee] @EmployeeID,@KRAGroupID, @FinancialYearID";

                        SqlParameter param = new SqlParameter();
                        param.ParameterName = "@EmployeeID";
                        param.Value = employeeID;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@KRAGroupID";
                        param.Value = groupId;
                        command.Parameters.Add(param);

                        param = new SqlParameter();
                        param.ParameterName = "@FinancialYearID";
                        param.Value = financialYearID;
                        command.Parameters.Add(param);

                        List<OrganizationKRAs> organizationKRAs = new List<OrganizationKRAs>();
                        List<CustomKRAs> customKRAs = new List<CustomKRAs>();

                        using (var reader = command.ExecuteReader())
                        {
                            associateKRAs.OrganizationKRAs = ((IObjectContextAdapter)apEntities)
                                                        .ObjectContext
                                                        .Translate<OrganizationKRAs>(reader)
                                                        .ToList();
                             reader.NextResult();

                            //Go to next result set.
                            //reader.NextResult();
                            //associateKRAs.CustomKRAs = ((IObjectContextAdapter)apEntities)
                            //                      .ObjectContext
                            //                      .Translate<CustomKRAs>(reader)
                            //                      .ToList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return associateKRAs;
        }
        #endregion

        #region GetOrganizationAndCustomKRAsForPMDH
        /// <summary>
        /// This method is used to get organization and custom KRAs when program manager or DH logs in. Displayed in dashboard.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        public async Task<AssociateKRAs> GetViewKRAByEmployee(int employeeID, int financialYearID)
        {
            AssociateKRAs associateKRAs = new AssociateKRAs();
            List<OrganizationKRAs> organizationKRAs = new List<OrganizationKRAs>();
            try
            {
                using (var apEntities = new APEntities())
                {
                    associateKRAs.OrganizationKRAs = await apEntities.Database.SqlQuery<OrganizationKRAs>
                              ("[usp_GetViewKRAByEmployee] @EmployeeID, @FinancialYearID",
                                new object[] {
                                    new SqlParameter("EmployeeID", employeeID),
                                    new SqlParameter("FinancialYearID", financialYearID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return associateKRAs;
        }
        #endregion

        //#region SaveCustomKRAs
        ///// <summary>
        ///// Save KRAs added by PM (CustomKRA)
        ///// </summary>
        ///// <param name="kraSetData"></param>
        ///// <returns></returns>
        //public bool SaveCustomKRAs(CustomKRAs customKRAs)
        //{
        //    int rowsAffected = 0;
        //    string createdUser = HttpContext.Current.User.Identity.Name;
        //    string systemInfo = Commons.GetClientIPAddress();
        //    try
        //    {
        //        using (var apEntities = new APEntities())
        //        {
        //            using (var trans = apEntities.Database.BeginTransaction())
        //            {
        //                Parallel.ForEach(customKRAs.EmployeeIDs, employee =>
        //                {
        //                    rowsAffected = apEntities.Database.SqlQuery<int>
        //                   ("[usp_SaveCustomKRAs] @EmployeeID, @KRAAspectID, @KRAAspectMetric, @KRAAspectTarget, @KRATypeID, @FinancialYearID, @CreatedDate, @CreatedUser, @SystemInfo",
        //                        new object[] {
        //                                new SqlParameter ("EmployeeID", employee),
        //                                new SqlParameter ("KRAAspectID", customKRAs.KRAAspectID),
        //                                new SqlParameter ("KRAAspectMetric", customKRAs.KRAAspectMetric),
        //                                new SqlParameter ("KRAAspectTarget", customKRAs.KRAAspectTarget),
        //                                new SqlParameter ("KRATypeID", customKRAs.KRATypeID),
        //                                new SqlParameter ("FinancialYearID", customKRAs.FinancialYearID),
        //                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
        //                                new SqlParameter ("CreatedUser", createdUser),
        //                                new SqlParameter ("SystemInfo", systemInfo)
        //                       }
        //                       ).SingleOrDefault();
        //                });
        //                trans.Commit();
        //            }
        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return rowsAffected > 0 ? true : false;
        //}
        //#endregion

        #region GetProjectsByProjectManagerID
        /// <summary>
        /// This method is used to get all the projects under a reporting manager.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetProjectsByProjectManagerID(int projectManagerID)
        {
            List<GenericType> lstProjects;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstProjects = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetProjectsByProjectManagerID] @ProjectManagerID",
                                new object[] {
                                    new SqlParameter("ProjectManagerID", projectManagerID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstProjects;
        }
        #endregion

       

        #region GetEmployeesByProjectID
        /// <summary>
        /// This method is used to get all the employees under a project.
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetEmployeesByProjectID(int projectID)
        {
            List<GenericType> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetEmployeesByPrimaryProjectID] @ProjectID",
                                new object[] {
                                    new SqlParameter("ProjectID", projectID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstEmployees;
        }
        #endregion

        #region GetEmployeesForDepartment
        /// <summary>
        /// This method is used to get all the employees under a department.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<List<GenericType>> GetEmployeesForDepartment(int employeeID)
        {
            List<GenericType> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetEmployeesForDepartment] @EmployeeID",
                                new object[] {
                                    new SqlParameter("EmployeeID", employeeID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstEmployees;
        }
        #endregion

        //#region EditCustomKRAs
        ///// <summary>
        ///// Update custom KRA
        ///// </summary>
        ///// <param name="kraSetData"></param>
        ///// <returns></returns>
        //public async Task<bool> EditCustomKRAs(CustomKRAs customKRAs)
        //{
        //    int rowsAffected;
        //    try
        //    {
        //        using (var apEntities = new APEntities())
        //        {
        //            using (var trans = apEntities.Database.BeginTransaction())
        //            {
        //                rowsAffected = await apEntities.Database.SqlQuery<int>
        //                       ("[usp_EditCustomKRAs] @CustomKRAID, @KRAAspectID, @KRAAspectMetric, @KRAAspectTarget, @ModifiedDate, @ModifiedUser, @SystemInfo",
        //                       new object[] {
        //                                new SqlParameter ("CustomKRAID", customKRAs.CustomKRAID),
        //                                new SqlParameter ("KRAAspectID", customKRAs.KRAAspectID),
        //                                new SqlParameter ("KRAAspectMetric", customKRAs.KRAAspectMetric),
        //                                new SqlParameter ("KRAAspectTarget", customKRAs.KRAAspectTarget),
        //                                new SqlParameter ("ModifiedDate", DateTime.UtcNow),
        //                                new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
        //                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
        //                       }
        //                       ).SingleOrDefaultAsync();
        //                trans.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return rowsAffected > 0 ? true : false;
        //}
        //#endregion

        //#region DeleteCustomKRA
        ///// <summary>
        ///// Delete customKRA
        ///// </summary>
        ///// <param name="customKRAID"></param>
        ///// <returns></returns>
        //public async Task<bool> DeleteCustomKRA(int customKRAID)
        //{
        //    int rowsAffected;
        //    try
        //    {
        //        using (var apEntities = new APEntities())
        //        {
        //            using (var trans = apEntities.Database.BeginTransaction())
        //            {
        //                rowsAffected = await apEntities.Database.SqlQuery<int>
        //                       ("[usp_DeleteCustomKRA] @CustomKRAID",
        //                           new object[] {
        //                                new SqlParameter ("CustomKRAID", customKRAID)
        //                           }
        //                           ).SingleOrDefaultAsync();
        //                trans.Commit();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new AssociatePortalException("Error while deleting customKRA.");
        //    }
        //    return rowsAffected > 0 ? true : false;
        //}
        //#endregion

        #region CreateKRADefinition
        /// <summary>
        /// creates kra definition
        /// </summary>
        /// <param name="kraDefinitionData"></param>
        /// <returns></returns>
        public int CreateKRADefinition(KRADefinitionData kraDefinitionData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        if (kraDefinitionData != null)
                        {           
                            if (string.IsNullOrEmpty(kraDefinitionData.lstMetricAndTarget[0].KRATargetText))
                            {
                                kraDefinitionData.lstMetricAndTarget[0].KRATargetText = null;
                            }
                            string[] dateFormate = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy" };
                            if (!string.IsNullOrWhiteSpace(kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate))
                            {
                                if (kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate.Contains("T"))
                                    kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate));
                            }

                            rowsAffected = apEntities.Database.SqlQuery<int>
                           ("[usp_CreateKRADefinition] @KRAGroupId, @KRAAspectId, @Metric, @KRAMeasurementTypeID, @FinancialYearID, @KRAOperatorID, @KRAScaleMasterID, @KRATargetValue, @KRATargetValeAsDate, @KRATargetText, @KRATargetPeriodID, @CreatedUser, @SystemInfo",
                                new object[] {
                                        new SqlParameter ("KRAGroupId", kraDefinitionData.KRAGroupId),
                                        new SqlParameter ("KRAAspectId", kraDefinitionData.KRAAspectId),
                                        new SqlParameter ("Metric", kraDefinitionData.lstMetricAndTarget[0].Metric),
                                        new SqlParameter ("KRAMeasurementTypeID", kraDefinitionData.lstMetricAndTarget[0].KRAMeasurementTypeId),
                                        new SqlParameter ("FinancialYearID", kraDefinitionData.FinancialYearId),
                                        new SqlParameter ("KRAOperatorID", kraDefinitionData.lstMetricAndTarget[0].KRAOperatorId),
                                        new SqlParameter ("KRAScaleMasterID", (object)kraDefinitionData.lstMetricAndTarget[0].KRAScaleMasterId??DBNull.Value),
                                        new SqlParameter ("KRATargetValue", (object)kraDefinitionData.lstMetricAndTarget[0].KRATargetValue??DBNull.Value),
                                        new SqlParameter ("KRATargetValeAsDate", kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate!=null?(object)Commons.GetDateTimeInIST(DateTime.ParseExact(kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None)):DBNull.Value),
                                        new SqlParameter ("KRATargetText", (object)kraDefinitionData.lstMetricAndTarget[0].KRATargetText??DBNull.Value),
                                        new SqlParameter ("KRATargetPeriodID", kraDefinitionData.lstMetricAndTarget[0].KRATargetPeriodId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                }
                               ).SingleOrDefault();

                            trans.Commit();
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region CreateKRAGroup
        /// <summary>
        /// creates kra group
        /// </summary>
        /// <param name="kraGroupData"></param>
        /// <returns></returns>
        public async Task<int> CreateKRAGroup(KRAGroupData kraGroupData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = apEntities.Database.SqlQuery<int>
                       ("[usp_CreateKRAGroup] @DepartmentId, @RoleCategoryId, @ProjectTypeId, @KRATitle, @FinancialYearId, @CreatedUser, @SystemInfo",
                            new object[] {
                                        new SqlParameter ("DepartmentId", kraGroupData.DepartmentId),
                                        new SqlParameter ("RoleCategoryId", kraGroupData.RoleCategoryId),
                                        new SqlParameter ("ProjectTypeId", (object)kraGroupData.ProjectTypeId?? DBNull.Value),
                                        new SqlParameter ("KRATitle", kraGroupData.KRATitle),
                                        new SqlParameter ("FinancialYearId", kraGroupData.FinancialYearId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                            }
                           ).SingleOrDefault();
                        trans.Commit();
                    }
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region DeleteKRADefinition
        /// <summary>
        /// Delete KRADefinition
        /// </summary>
        /// <param name="kraDefinitionId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteKRADefinition(KRADefinitionData kraDefinition)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteKRAAspectByDefinitionId] @KRAGroupId,@FinancialYearId,@KRAAspectId,@StatusId, @RoleName",
                                   new object[] {
                                        new SqlParameter ("KRAGroupId", kraDefinition.KRAGroupId),
                                        new SqlParameter ("FinancialYearId", kraDefinition.FinancialYearId),
                                        new SqlParameter ("KRAAspectId", kraDefinition.KRAAspectId),
                                        new SqlParameter ("StatusId", kraDefinition.StatusId),
                                        new SqlParameter ("RoleName", kraDefinition.RoleName)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting KRA definition.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region DeleteKRAMetricByKRADefinitionId
        /// <summary>
        /// Delete KRAMetric By KRADefinitionId
        /// </summary>
        /// <param name="kraDefinitionId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteKRAMetricByKRADefinitionId(KRADefinitionData kraDefinition)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteKRARoleMetric] @KRADefinitionId,@StatusId,@RoleName",
                                   new object[] {
                                        new SqlParameter ("KRADefinitionId", kraDefinition.lstMetricAndTarget[0].KRADefinitionId),
                                        new SqlParameter ("StatusId", kraDefinition.StatusId),
                                        new SqlParameter ("RoleName", kraDefinition.RoleName)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting KRA metric.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetKRADefinitionById
        /// <summary>
        /// This method is used to get all the kra definitions under a kra group.
        /// </summary>
        /// <param name="kraGroupId"></param
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        public async Task<List<KRADefinitionData>> GetKRADefinitionById(int kraGroupId, int financialYearId)
        {
            List<KRADefinitionData> lstKRADefinitions;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRADefinitions = await apEntities.Database.SqlQuery<KRADefinitionData>
                              ("[usp_GetKRAAspectForKRAGroup] @KRAGroupId, @FinancialYearId",
                                new object[] {
                                    new SqlParameter("KRAGroupId", kraGroupId),
                                    new SqlParameter("FinancialYearId", financialYearId)
                                        }).ToListAsync();

                    foreach (KRADefinitionData kraDefinitionData in lstKRADefinitions)
                    {
                        List<MetricAndTarget> lstMetricAndTarget = await apEntities.Database.SqlQuery<MetricAndTarget>
                                  ("[usp_GetKRADefinitionByKRAGroupId] @KRAGroupId, @FinancialYearId, @KRAAspectId",
                                    new object[] {
                                    new SqlParameter("KRAGroupId", kraGroupId),
                                    new SqlParameter("FinancialYearId", financialYearId),
                                    new SqlParameter("KRAAspectId", kraDefinitionData.KRAAspectId)
                                            }).ToListAsync();
                        kraDefinitionData.lstMetricAndTarget = lstMetricAndTarget.OrderBy(a => a.Metric).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRADefinitions;
        }
        #endregion

        #region GetKRAGroups
        /// <summary>
        /// Gets list of KRA Groups.
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAGroupData>> GetKRAGroups()
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAGroups = await apEntities.Database.SqlQuery<KRAGroupData>
                              ("[usp_GetKRAGroups]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAGroups;
        }
        #endregion

        #region GetKRAGroups
        /// <summary>
        /// Gets list of KRA Groups by financialyearid.
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAGroupData>> GetKRAGroups(int financialYearId)
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAGroups = await apEntities.Database.SqlQuery<KRAGroupData>
                              ("[usp_GetKRAGroups] @FinancialYearId",
                                new object[] {
                                        new SqlParameter ("FinancialYearId", financialYearId)
                               }
                              ).ToListAsync();

                    lstKRAGroups = lstKRAGroups.Where(year => year.FinancialYearId == financialYearId).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAGroups;
        }
        #endregion

        #region GetKRAGroups
        /// <summary>
        /// Gets list of KRA Groups by financialyearid.
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAGroupData>> GetKRAGroupsByFinancial(int financialYearId)
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAGroups = await apEntities.Database.SqlQuery<KRAGroupData>
                              ("[usp_GetKraGroupsByFinancialYear] @FinancialYearId",
                                new object[] {
                                        new SqlParameter ("FinancialYearId", financialYearId)
                               }
                              ).ToListAsync();

                    lstKRAGroups = lstKRAGroups.Where(year => year.FinancialYearId == financialYearId).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAGroups;
        }
        #endregion

        #region ViewKRAGroups
        /// <summary>
        /// View KRA Groups by DepartmentID and FinancialYearID
        /// </summary>
        /// <param name="departmentID"></param>
        /// <param name="financialYearID"></param>
        /// <returns></returns>
        public async Task<List<KRAGroupData>> ViewKRAGroups(int departmentID, int financialYearID)
        {
            List<KRAGroupData> lstKRAGroups;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAGroups = await apEntities.Database.SqlQuery<KRAGroupData>
                              ("[usp_ViewKRAGroups] @DepartmentID, @FinancialYearID",
                                new object[] {
                                    new SqlParameter("DepartmentID", departmentID),
                                    new SqlParameter("FinancialYearID", financialYearID)
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAGroups;
        }
        #endregion

        #region GetKRADefinitionByDepartmentID
        /// <summary>
        /// Get KRA Definitions By Department ID
        /// </summary>
        /// <param name="kraGroupID"></param>
        /// <param name="financialYearID"></param>
        /// <param name="departmentID"></param>
        public async Task<List<KRADefinitionData>> GetKRADefinitionByDepartmentID(int kraGroupID, int financialYearID, int departmentID)
        {
            List<KRADefinitionData> lstKRADefinitions;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRADefinitions = await apEntities.Database.SqlQuery<KRADefinitionData>
                              ("[usp_GetKRADefinitionByDepartmentID] @KRAGroupID, @FinancialYearID, @DepartmentID",
                                new object[] {
                                    new SqlParameter("KRAGroupId", kraGroupID),
                                    new SqlParameter("FinancialYearId", financialYearID),
                                     new SqlParameter("DepartmentID", departmentID)
                                        }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRADefinitions;
        }
        #endregion

        //#region GetKRATitle
        ///// <summary>
        ///// Get KRA Title
        ///// </summary>
        ///// <param name="kraGroupID"></param>
        ///// <returns></returns>
        //public async Task<string> GetKRATitle(int kraGroupID)
        //{
        //    try
        //    {
        //        using (var apEntities = new APEntities())
        //        {
        //            return await apEntities.Database.SqlQuery<string>
        //                       ("[usp_GetKRATitle] @KRAGroupID",
        //                         new object[] {
        //                            new SqlParameter("KRAGroupId", kraGroupID),
        //                                 }).FirstOrDefaultAsync();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion

        #region UpdateKRADefinition
        /// <summary>
        /// Update KRA definition
        /// </summary>
        /// <param name="kraDefinitionData"></param>
        /// <returns></returns>
        public async Task<int> UpdateKRADefinition(KRADefinitionData kraDefinitionData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {

                        if (kraDefinitionData != null)
                        {
                            if (string.IsNullOrEmpty(kraDefinitionData.lstMetricAndTarget[0].KRATargetText))
                            {
                                kraDefinitionData.lstMetricAndTarget[0].KRATargetText = null;
                            }
                            string[] dateFormate = new string[] { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy" };
                            if (!string.IsNullOrWhiteSpace(kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate))
                            {
                                if (kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate.Contains("T"))
                                    kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate));
                            }

                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_UpdateKRADefinition] @KRADefinitionId, @Metric, @KRAMeasurementTypeID, @KRAOperatorID, @KRAScaleMasterID, @KRATargetValue, @KRATargetValeAsDate, @KRATargetText, @KRATargetPeriodID, @RoleName, @StatusId, @ModifiedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("KRADefinitionId", kraDefinitionData.lstMetricAndTarget[0].KRADefinitionId),
                                        new SqlParameter ("Metric", kraDefinitionData.lstMetricAndTarget[0].Metric),
                                        new SqlParameter ("KRAMeasurementTypeID", kraDefinitionData.lstMetricAndTarget[0].KRAMeasurementTypeId),
                                        new SqlParameter ("KRAOperatorID", kraDefinitionData.lstMetricAndTarget[0].KRAOperatorId),
                                        new SqlParameter ("KRAScaleMasterID", (object)kraDefinitionData.lstMetricAndTarget[0].KRAScaleMasterId??DBNull.Value),
                                        new SqlParameter ("KRATargetValue", (object)kraDefinitionData.lstMetricAndTarget[0].KRATargetValue??DBNull.Value),
                                        new SqlParameter ("KRATargetValeAsDate", kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate!=null?(object)Commons.GetDateTimeInIST(DateTime.ParseExact(kraDefinitionData.lstMetricAndTarget[0].KRATargetValueAsDate, dateFormate, CultureInfo.InvariantCulture, DateTimeStyles.None)):DBNull.Value),
                                        new SqlParameter ("KRATargetText", (object)kraDefinitionData.lstMetricAndTarget[0].KRATargetText??DBNull.Value),
                                        new SqlParameter ("KRATargetPeriodID", kraDefinitionData.lstMetricAndTarget[0].KRATargetPeriodId),
                                        new SqlParameter ("RoleName", kraDefinitionData.RoleName),
                                        new SqlParameter ("StatusId", kraDefinitionData.StatusId),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();

                            trans.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region InitiateKRAs
        /// <summary>
        /// Initiate KRAs by HR Head
        /// </summary>
        /// <param name="FinancialYearID"></param>
        /// <returns></returns>
        public async Task<bool> InitiateKRAs(int FinancialYearId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_InitiateKRAs] @FinancialYearId",
                               new object[] {
                                        new SqlParameter ("FinancialYearId", FinancialYearId)
                               }
                               ).SingleOrDefaultAsync();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetKRAOperators
        /// <summary>
        /// Gets list of KRA Operators.
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetKRAOperators()
        {
            List<GenericType> lstKRAOperators;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAOperators = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKRAOperators]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAOperators;
        }
        #endregion

        #region GetKRAMeasurementType
        /// <summary>
        /// Gets list of KRA measurement type.
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetKRAMeasurementType()
        {
            List<GenericType> lstKRAOperators;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAOperators = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKRAMeasurementType]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAOperators;
        }
        #endregion

        #region CreateKRAMeasurementType
        /// <summary>
        /// Create KRA MeasurementType
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateKRAMeasurementType(KRAMeasurementTypeData KRAMeasurementTypeData)
        {
            int rowsAffected = 0;

            try
            {
                using (var apEntities = new APEntities())
                {
                    if (string.IsNullOrEmpty(KRAMeasurementTypeData.KRAMeasurementType))
                    {
                        return -11; //Nothing to save
                    }
                    //Should not allow Numerics and some special characters
                    var regexItem = new Regex(@"[0-9\|!#$%/()=?»«@£§€{}.;`~'<>_]");
                    if (regexItem.IsMatch(KRAMeasurementTypeData.KRAMeasurementType))
                        return -11;

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateKRAMeasurementType] @KRAMeasurementType",
                                   new object[] {
                                        new SqlParameter ("KRAMeasurementType", KRAMeasurementTypeData.KRAMeasurementType)
                                   }
                                   ).SingleOrDefaultAsync();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while creating a New Measurement Type");
            }
            return rowsAffected;
        }
        #endregion

        #region UpdateKRAMeasurementType
        /// <summary>
        /// Update KRA MeasurementType
        /// </summary>
        /// <param name="KRAMeasurementTypeData"></param>
        /// <returns></returns>
        public async Task<int> UpdateKRAMeasurementType(KRAMeasurementTypeData KRAMeasurementTypeData)
        {
            int rowsAffected;

            try
            {
                using (var apEntities = new APEntities())
                {
                    if (string.IsNullOrEmpty(KRAMeasurementTypeData.KRAMeasurementType))
                    {
                        return -11; //Nothing to save
                    }

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateKRAMeasurementType] @KraMeasurementTypeId, @KRAMeasurementType",
                               new object[] {
                                        new SqlParameter ("KraMeasurementTypeId", KRAMeasurementTypeData.KRAMeasurementTypeId),
                                        new SqlParameter ("KRAMeasurementType", KRAMeasurementTypeData.KRAMeasurementType)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region DeleteKRAMeasurementType
        /// <summary>
        /// Delete KRAMeasurementType
        /// </summary>
        /// <param name="KRAMeasurementTypeId"></param>
        /// <returns></returns>
        public async Task<int> DeleteKRAMeasurementType(int KRAMeasurementTypeId)
        {
            int rowsAffected;

            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteKRAMeasurementType] @KraMeasurementTypeId",
                               new object[] {
                                        new SqlParameter ("KraMeasurementTypeId", KRAMeasurementTypeId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion


        #region GetKRAScales
        /// <summary>
        /// Gets list of KRA scales.
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAScaleData>> GetKRAScales()
        {
            List<KRAScaleData> lstKRAScales;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAScales = await apEntities.Database.SqlQuery<KRAScaleData>
                              ("[usp_GetKRAScales]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAScales;
        }
        #endregion

        #region GetKRATargetPeriods
        /// <summary>
        /// Gets list of KRA target periods.
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetKRATargetPeriods()
        {
            List<GenericType> lstKRATargetPeriods;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRATargetPeriods = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKRATargetPeriods]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRATargetPeriods;
        }
        #endregion

        #region GetKRAScalesValues
        /// <summary>
        /// Gets list of KRA scale values.
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetKRAScaleValues()
        {
            List<GenericType> lstKRAScaleValues;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAScaleValues = await apEntities.Database.SqlQuery<GenericType>
                              ("[usp_GetKRAScaleValues]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAScaleValues;
        }
        #endregion

        #region GetKRAComments
        /// <summary>
        /// Get KRA comments.
        /// </summary>
        /// <param name="financialYearId"></param>
        /// <param name="kRAGroupId"></param>
        /// <returns></returns>
        public async Task<List<KRAComments>> GetKRAComments(int financialYearId, int kraGroupId)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<KRAComments>
                              ("[usp_GetKRAComments] @FinancialYearId, @KRAGroupId",
                                    new SqlParameter("FinancialYearId", financialYearId),
                                    new SqlParameter("KRAGroupId", kraGroupId)
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddKRAComments
        /// <summary>
        /// Adds KRA Comments.
        /// </summary>
        /// <param name="kraWorkFlowData"></param>
        /// <returns></returns>
        public async Task<int> AddKRAComments(KRAWorkFlowData kraWorkFlowData)
        {
            int rowsAffected = 0;
            int KRAGroupID;
            try
            {
                using (var apEntities = new APEntities())
                {
                    if (kraWorkFlowData.StatusID == Convert.ToInt32(Enumeration.KRA.Approved))
                        return -12; //Cannot add comments as KRAs are already approved.

                    if (string.IsNullOrEmpty(kraWorkFlowData.Comments) || (kraWorkFlowData.KRAGroupIDs.Any() && kraWorkFlowData.KRAGroupIDs == null))
                        return -11;  //Nothing to save

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        KRAGroupID = kraWorkFlowData.KRAGroupIDs[0].Id;

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_AddKRAComments] @KRAGroupId, @FinancialYearId, @FromEmployeeId, @Comments, @CommentedDate, @StatusID",
                               new object[] {
                                        new SqlParameter ("KRAGroupId", KRAGroupID),
                                        new SqlParameter ("FinancialYearId", kraWorkFlowData.FinancialYearID),
                                        new SqlParameter ("FromEmployeeId", kraWorkFlowData.FromEmployeeID),
                                        new SqlParameter ("Comments", kraWorkFlowData.Comments),
                                        new SqlParameter ("CommentedDate", DateTime.UtcNow),
                                        new SqlParameter ("StatusID", kraWorkFlowData.StatusID)

                                   }
                                   ).SingleOrDefaultAsync();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion

        #region DeleteKRAGroup
        /// <summary>
        /// Delete KRAGroup
        /// </summary>
        /// <param name="kraSubmittedGroup"></param>
        /// <returns></returns>
        public async Task<int> DeleteKRAGroup(KRASubmittedGroup kraSubmittedGroup)
        {
            KRASubmittedGroup notificationData = new KRASubmittedGroup();
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        foreach (var kraGroupId in kraSubmittedGroup.KRAGroupIds)
                        {
                            notificationData = apEntities.Database.SqlQuery<KRASubmittedGroup>
                               ("[usp_DeleteKRAGroup] @KRAGroupId, @FinancialYearId, @RoleName",
                                   new object[] {
                                        new SqlParameter ("KRAGroupId", kraGroupId),
                                        new SqlParameter ("FinancialYearId", kraSubmittedGroup.FinancialYearId),
                                        new SqlParameter ("RoleName", kraSubmittedGroup.RoleName)
                                   }
                                   ).FirstOrDefault();

                            if (notificationData != null)
                            {
                                if (notificationData.StatusId == -2)  //unauthorized access.
                                {
                                    trans.Rollback();
                                    return -2;
                                }

                                EmailNotificationConfiguration emailNotificationConfig = getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.DeleteKRAGroup), Convert.ToInt32(Enumeration.CategoryMaster.KRA));
                                if (emailNotificationConfig != null)
                                {
                                    DeleteKRAGroupNotification(notificationData, emailNotificationConfig, kraSubmittedGroup.fromEmployeeId);
                                }
                            }

                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting KRA group.");
            }
            return 1;
        }
        #endregion

        #region DeleteKRAGroupNotification
        /// <summary>
        /// Notification To delete kra group
        /// </summary>
        /// <returns></returns>
        private void DeleteKRAGroupNotification(KRASubmittedGroup notificationData, EmailNotificationConfiguration emailNotificationConfig, int FromEmployeeId)
        {
            string submittedByName = getEmployeeName(FromEmployeeId);

            NotificationDetail notificationDetail = new NotificationDetail();
            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

            emailContent = emailContent.Replace("{KRAGroup}", notificationData.KRATitle)
                                       .Replace("{ToEmployeeName}", notificationData.ToEmployeeName)
                                       .Replace("{DepartmentName}", notificationData.Description)
                                       .Replace("{FromEmployeeName}", submittedByName)
                                       .Replace("{FinancialYear}", notificationData.FinancialYear);

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                throw new AssociatePortalException("Email From cannot be blank");
            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                throw new AssociatePortalException("Email To cannot be blank");
            notificationDetail.ToEmail = notificationData.EmailAddress;

            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

            StringBuilder Subject = new StringBuilder(emailNotificationConfig.EmailSubject);
            Subject = Subject.Replace("{FinancialYear}", notificationData.FinancialYear);
            notificationDetail.Subject = Subject.ToString();

            notificationDetail.EmailBody = emailContent.ToString();
            //Send Email Notification.
            NotificationManager.SendEmail(notificationDetail);
        }
        #endregion

    }

}

