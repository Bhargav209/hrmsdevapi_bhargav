﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using Quartz;
using System.Web;

namespace AP.API
{
    public class FinanceNotificationJob : IJob
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region AddJob
        /// <summary>
        /// Add job to the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <param name="interval"></param>
        /// 
        public bool AddJob(string jobId, string groupId)
        {
            bool returnValue = false;

            try
            {
            //using (APEntities hrmEntities = new APEntities())
            //{
            //    // Insert record to the JobDetail table //
            //    //
            //    string financeNotificationCode = resourceManager.GetString("FinanceNotification");
            //   // NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == financeNotificationCode).FirstOrDefault();

            //   // if (notificationConfiguration.SLA == 0) throw new AssociatePortalException("Employee Profile Creation Notification's SLA can not be zero.");

            //   // returnValue = NotificationScheduler.AddJob<FinanceNotificationJob>(jobId, groupId, notificationConfiguration.SLA.GetValueOrDefault());

            //    if (returnValue)
            //    {
            //        string started = resourceManager.GetString("Started");
            //        var jobExist = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId && j.Status == started).FirstOrDefault();

            //        // Insert record to the JobDetail table 
            //        //
            //        if (jobExist == null)
            //        {
            //           // JobDetail jobDetail = new JobDetail() { JobCode = jobId, GroupID = groupId, Status = started, JobInterval = notificationConfiguration.SLA.GetValueOrDefault() };
            //           // jobDetail = hrmEntities.JobDetails.Add(jobDetail);
            //        }

            //        int financeDepartmentNotificationTypeID = Convert.ToInt32(resourceManager.GetString("FinanceDepartmentNotificationTypeID"));
            //        string statusPendingID = resourceManager.GetString("StatusPendingID");
            //        var notType = Convert.ToInt32(financeDepartmentNotificationTypeID);
            //        var pending = Convert.ToInt32(statusPendingID);
            //        var notificationExist = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == notType && n.StatusId == pending).FirstOrDefault();

            //        // Insert record in to Notification table  
            //        //
            //        if (notificationExist == null)
            //        {
            //            //Notification notification = new Notification() { EmployeeCode = jobId, NotificationTypeID = financeDepartmentNotificationTypeID, StatusId = Convert.ToInt32(statusPendingID) };
            //            //notification = hrmEntities.Notifications.Add(notification);
            //        }

            //        //Insert the task details into FinanceTask table
            //        //
            //        IEnumerable financeTasks = GetFinanceTasks();

            //        foreach (FinanceTaskMaster task in financeTasks)
            //        {
            //            FinanceTask financeTask = new FinanceTask() { EmployeeCode = jobId, TaskID = task.FinanceTaskID, StatusId = Convert.ToInt32(statusPendingID) };
            //            financeTask = hrmEntities.FinanceTasks.Add(financeTask);
            //        }

            //        hrmEntities.SaveChanges();
            //    }
            //}
             
            }
            catch
            {
                throw new AssociatePortalException("Failed to save the data.");
            }

            return returnValue;
        }
        #endregion

        #region RemoveJob
        /// <summary>
        /// Remove job from the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// 
        public bool RemoveJob(string jobId, string groupId)
        {
            bool isDelete = false;
            APEntities hrmEntities = null;

            try
            {
                isDelete = NotificationScheduler.RemoveJob(jobId, groupId);

                using (hrmEntities = new APEntities())
                {
                    ///  Delete Job from JobDetail table ///

                    JobDetail jobToBeDeleted = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId).FirstOrDefault();

                    if (jobToBeDeleted != null)
                    {
                        hrmEntities.JobDetails.Remove(jobToBeDeleted);
                    }

                    int financeNotificationType = Convert.ToInt32(resourceManager.GetString("FinanceDepartmentNotificationTypeID"));
                    //Notification notification = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == financeNotificationType).FirstOrDefault();

                    //if (notification != null)
                    //{
                    //    int statusApprovedID = Convert.ToInt32(resourceManager.GetString("StatusApprovedID"));
                    //    notification.StatusId = statusApprovedID;
                    //}

                    hrmEntities.SaveChanges();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to remove the details.");
            }

            return isDelete;
        }
        #endregion

        #region Execute
        /// <summary>
        /// Execution of the job for sending email
        /// </summary>
        /// <param name="context"></param>
        /// 
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                JobDataMap jobDataMap = context.JobDetail.JobDataMap;
                string associateId = jobDataMap.GetString("AssociateId");
                string name = jobDataMap.GetString("AssociateName");
                string designation = jobDataMap.GetString("Designation");
                string department = jobDataMap.GetString("Department");
                string mobileNo = jobDataMap.GetString("Mobile");  
                
                using (APEntities hrmEntities = new APEntities())
                {
                    string financeNotificationCode = resourceManager.GetString("FinanceNotification");
                   // NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == financeNotificationCode).FirstOrDefault();

                    //if (notificationConfiguration != null && name != null && department != null)
                    //{
                    //   // string htmlContent = notificationConfiguration.emailContent;

                    //    htmlContent = htmlContent.Replace("@AssociateName", name).Replace("@Designation", designation).Replace("@Department", department).Replace("@Mobile", mobileNo);

                    //    Email email = new Email();
                    //    int notificationConfigID = new BaseEmail().BuildEmailObject(email, notificationConfiguration.emailTo, notificationConfiguration.emailFrom, notificationConfiguration.emailCC, notificationConfiguration.emailSubject + associateId, htmlContent, Enumeration.NotificationStatus.Finance.ToString());
                    //    new BaseEmail().SendEmail(email, notificationConfigID);
                    //}
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region GetFinanceNotifications
        /// <summary>
        /// Method to get Finance notification lists
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetFinanceNotifications()
        {
            APEntities apEntities = null;

            try
            {
            //int notificationType = Convert.ToInt32(resourceManager.GetString("FinanceDepartmentNotificationTypeID"));
            //int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));

            //using (apEntities = new APEntities())
            //{
            //    var financeNotifications = (from n in apEntities.Notifications
            //                                join nm in apEntities.NotificationTypeMasters on n.NotificationTypeID equals nm.NotificationTypeID
            //                                join e in apEntities.Employees on n.EmployeeCode equals e.EmployeeCode
            //                                join f in apEntities.FinanceTasks on n.EmployeeCode equals f.EmployeeCode
            //                                join fm in apEntities.FinanceTaskMasters on f.TaskID equals fm.FinanceTaskID
            //                                where (f.StatusId == pendingStatusID && nm.NotificationTypeID == notificationType)
            //                                select new NotificationData
            //                                {
            //                                    notificationTypeId = n.NotificationTypeID,
            //                                    notificationCode = nm.NotificationCode,
            //                                    notificationDesc = nm.NotificationDesc,
            //                                    employeeCode = n.EmployeeCode,
            //                                    firstName = e.FirstName,
            //                                    lastName = e.LastName,
            //                                    statusId = f.StatusId,
            //                                    taskName = fm.TaskName,
            //                                    taskId = f.TaskID
            //                                });

            //    var Notification = financeNotifications.Distinct().ToList();

            //    if (Notification.Count > 0)
            //        return Notification;
            //    else
            //        return Enumerable.Empty<object>().ToList();
            //}
            return null;
            }
            catch
            {
                throw new AssociatePortalException("Failed to get notification");
            }
        }
        #endregion

        #region CountFinanceNotifications
        /// <summary>
        /// Method to get Finance notification count
        /// </summary>
        /// <returns></returns>
        /// 
        public int CountFinanceNotifications()
        {
            APEntities apEntities = null;

            try
            {
                using (apEntities = new APEntities())
                {

                    int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
                    var financeTasksCount = (from finance in apEntities.FinanceTasks
                                             where finance.StatusId == pendingStatusID
                                             select new { finance.EmployeeCode }).Distinct().Count();

                    return financeTasksCount;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }

        }
        #endregion

        #region UpdateFinanceNotifications
        /// <summary>
        /// Method to update Finance notification status
        /// </summary>
        /// <param name="notificationData"></param>
        /// <returns></returns>
        /// 
        public bool UpdateFinanceNotifications(string employeeCode, int taskId)
        {
            bool isUpdated = false;
            APEntities hrmsEntities = null;

            try
            {
                using (hrmsEntities = new APEntities())
                {
                    FinanceTask financeTask = hrmsEntities.FinanceTasks.Where(e => e.EmployeeCode == employeeCode && e.TaskID == taskId).ToList().FirstOrDefault();

                    int statusApprovedID = Convert.ToInt32(resourceManager.GetString("StatusApprovedID"));
                    financeTask.StatusId = statusApprovedID;
                    hrmsEntities.Entry(financeTask).State = System.Data.Entity.EntityState.Modified;
                    isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;

                    //Remove the finance notification job if there is no more pending task for this employee
                    int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
                    FinanceTask pendingFinanceTask = hrmsEntities.FinanceTasks.Where(e => e.EmployeeCode == employeeCode && e.StatusId == pendingStatusID).ToList().FirstOrDefault();

                    if (pendingFinanceTask == null)
                    {
                        RemoveJob(employeeCode, resourceManager.GetString("FinanceNotification"));
                    }

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to update notification details.");
            }

            return isUpdated;
        }
        #endregion

        #region GetFinanceTasks
        /// <summary>
        /// Method to get Finance tasks
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<FinanceTaskMaster> GetFinanceTasks()
        {
            APEntities hrmsEntities = null;

            try
            {
                using (hrmsEntities = new APEntities())
                {
                    IEnumerable<FinanceTaskMaster> getFinanceTasks = (from e in hrmsEntities.FinanceTaskMasters
                                                                      select e).ToList();
                    return getFinanceTasks;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion
    }

}

