﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AP.API
{
    public static class DomainUtility
    {
        /// <summary>
        /// This method prepares the entity modal object using domain entity object provided as a parameter.
        /// </summary>
        /// <typeparam name="T">Entity Modal object for AP.DataStorage library.</typeparam>
        /// <param name="source">Domain Entity object constructed by UI.</param>
        /// <returns></returns>
        public static T CreateObject<T>(object source)
           where T : class
        {
            T result = Activator.CreateInstance<T>();
            PrepareEntity(source, result);
            //
            result.SetPropertyValue("IsActive", true);
            result.SetPropertyValue("CreatedBy", HttpContext.Current.User.Identity.Name);
            result.SetPropertyValue("CreatedUser", HttpContext.Current.User.Identity.Name);
            result.SetPropertyValue("CreatedDate", DateTime.Now);
            //
            return result;
        }

        /// <summary>
        /// Updates the "result" object using the data is passed in "source" object.
        /// </summary>
        /// <param name="result">The object of domain entity that gest populated by client.</param>
        /// /// <param name="source">The object of API modal that needs to be updated.</param>
        /// <returns></returns>
        public static object UpdateObject(object result, object source)
        {
            PrepareEntity(source, result);

            result.SetPropertyValue("IsActive", true);
            result.SetPropertyValue("ModifiedBy", HttpContext.Current.User.Identity.Name);
            result.SetPropertyValue("ModifiedUser", HttpContext.Current.User.Identity.Name);
            result.SetPropertyValue("ModifiedDate", DateTime.Now);
            return result;
        }

        private static void PrepareEntity(object source, object result)
        {
            string[] ignoreColumns = new[] { "IsActive", "CreatedBy", "CreatedUser", "CreatedDate", "ModifiedBy", "ModifiedUser", "ModifiedDate" };
            //
            var properties = from PropertyInfo property in source.GetType().GetProperties()
                             where !ignoreColumns.Contains(property.Name) //!property.Name.Contains("Id")
                             select property;

            foreach (PropertyInfo property in properties)
                result.SetPropertyValue(property.Name, source.GetPropertyValue(property.Name));
        }

        private static Object GetPropertyValue(this object entity, string property)
        {
            try { return entity.GetType().GetProperty(property).GetValue(entity, null); }
            catch { return null; }
        }

        private static void SetPropertyValue(this object entity, string property, object value)
        {
            try
            {
                PropertyInfo info = entity.GetType().GetProperty(property);
                if (info != null) info.SetValue(entity, value, null);
            }
            catch { }
        }

    }
}
