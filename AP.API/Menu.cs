﻿using System;
using System.Linq;
using System.Resources;
using AP.DataStorage;
using AP.Utility;
using Quartz;
using AP.DomainEntities;
using System.Collections.Generic;
using System.Web;
using System.Data.Entity;
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.MappingAPI.Extensions;
using System.Transactions;
using System.ComponentModel.DataAnnotations;
using EntityFramework.MappingAPI;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace AP.API
{
    public class Menu
    {
        public IEnumerable<MenuData> GetMenuDetails(string roleName)
        {
            try
            {
                return GetMenuDetailsByRole(roleName);
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
            return null;
        }

        private IEnumerable<MenuData> GetMenuDetailsByRole(string roles)
        {
            IEnumerable<MenuData> _menuParentNodesData;

            using (APEntities hrmsEntities = new APEntities())
            {
                List<MenuData> menuData = hrmsEntities.Database.SqlQuery<MenuData>
                          ("[usp_GetMenuDetailsByRoles] @Roles",
                             new object[] {
                                        new SqlParameter ("Roles", roles)
                             }
                             ).OrderBy(m=>m.DisplayOrder).ToList();

                if (menuData != null && menuData.Count() > 0)
                {
                    _menuParentNodesData = menuData.Where(menu => menu.ParentId == 0);

                    foreach (var menuItem in _menuParentNodesData)
                    {
                        buildTreeviewMenu(menuItem, menuData);
                    }
                }
                else
                    _menuParentNodesData = new MenuData[] { };
            }
            return _menuParentNodesData;
        }

        private void buildTreeviewMenu(MenuData menuItem, IEnumerable<MenuData> menudata)
        {
            IEnumerable<MenuData> _menuItems;

            _menuItems = menudata.Where(menu => menu.ParentId == menuItem.MenuId);

            if (_menuItems != null && _menuItems.Count() > 0)
            {
                foreach (var item in _menuItems)
                {
                    menuItem.Categories.Add(item);
                    buildTreeviewMenu(item, menudata);
                }
            }
        }

        #region GetParentMenus
        /// <summary>
        /// GetParentMenus
        /// </summary>
        /// <returns>List</returns>
        public List<MenuData> GetParentMenus()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<MenuData> parentList = hrmsEntities.MenuMasters.Where(parent => parent.ParentId == 0 && parent.IsActive == true).AsQueryable()
                                                                             .Select(parentData => new MenuData
                                                                             {
                                                                                 ParentId = parentData.ParentId,
                                                                                 Title = parentData.Title,
                                                                                 IsActive = parentData.IsActive
                                                                             });
                return parentList.ToList<MenuData>();
            }
        }
        #endregion

        #region AddMenu
        /// <summary>
        /// AddMenu
        /// </summary>
        /// <param name="menuData"></param>
        /// <returns>bool</returns>
        public bool AddMenu(MenuData menuData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                MenuMaster menu = new MenuMaster()
                {
                    Title = menuData.Title,
                    Path = menuData.Path,
                    ParentId = menuData.ParentId,
                    DisplayOrder = GetDisplayOrder(menuData.ParentId),
                    Parameter = menuData.Parameter,
                    NodeId = menuData.NodeId,
                    Style = menuData.Style,
                    IsActive = menuData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name,
                    SystemInfo = menuData.SystemInfo
                };
                hrmsEntities.MenuMasters.Add(menu);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region GetDisplayOrder
        /// <summary>
        /// Gets the maximum value of display order.
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns>int</returns>
        public int GetDisplayOrder(int parentId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                int maxOrderCount = hrmsEntities.MenuMasters.Where(id => id.ParentId == parentId).Max(order => order.DisplayOrder);
                return maxOrderCount + 1;
            }
        }
        #endregion

        #region UpdateMenu
        /// <summary>
        /// UpdateMenu
        /// </summary>
        /// <param name="menuData"></param>
        /// <returns>bool</returns>
        public bool UpdateMenu(MenuData menuData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                MenuMaster menu = hrmsEntities.MenuMasters.Where(id => id.MenuId == menuData.MenuId).FirstOrDefault();
                if (menu != null)
                {
                    menu.Title = menuData.Title;
                    menu.IsActive = menuData.IsActive;
                    menu.ParentId = menuData.ParentId;
                    menu.Style = menuData.Style;
                    menu.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    hrmsEntities.Entry(menu).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region GetMenus
        /// <summary>
        /// GetMenus
        /// </summary>
        /// <returns>List</returns>
        public List<MenuData> GetMenus()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<MenuData> parentList = (from parents in hrmsEntities.MenuMasters
                                                   from menus in hrmsEntities.MenuMasters
                                                   where parents.IsActive == true && parents.ParentId == 0 && menus.IsActive == true && ((menus.ParentId == 0 && menus.MenuId == parents.MenuId) || menus.ParentId == parents.MenuId)
                                                   select new MenuData
                                                   {
                                                       MenuId = menus.MenuId,
                                                       Title = menus.Title,
                                                       DisplayOrder = menus.DisplayOrder,
                                                       IsActive = menus.IsActive
                                                   });
                return parentList.ToList<MenuData>();
            }
        }
        #endregion

        #region AddMenuRole
        /// <summary>
        /// AddMenuRole
        /// </summary>
        /// <param name="menuData"></param>
        /// <returns>bool</returns>
        public bool AddMenuRole(MenuRoleData menuRoleData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                MenuRole menuRole = new MenuRole()
                {
                    MenuId = menuRoleData.MenuId,
                    RoleId = menuRoleData.RoleId,
                    IsActive = menuRoleData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name
                };
                hrmsEntities.MenuRoles.Add(menuRole);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region UpdateMenuRole
        /// <summary>
        /// UpdateMenuRole
        /// </summary>
        /// <param name="menuRoleData"></param>
        /// <returns>bool</returns>
        public bool UpdateMenuRole(MenuRoleData menuRoleData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                MenuRole menuRole = hrmsEntities.MenuRoles.Where(id => id.MenuRoleId == menuRoleData.MenuRoleId).FirstOrDefault();
                if (menuRole != null)
                {
                    menuRole.MenuId = menuRoleData.MenuId;
                    menuRole.RoleId = menuRoleData.RoleId;
                    menuRole.IsActive = menuRoleData.IsActive;
                    menuRole.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    hrmsEntities.Entry(menuRole).State = EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            return status;
        }
        #endregion

        #region GetMenuTitles
        /// <summary>
        /// GetMenuTitles
        /// </summary>
        /// <returns>List</returns>
        public List<MenuData> GetMenuTitles()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<MenuData> titleList = hrmsEntities.MenuMasters.Where(parent => parent.ParentId != 0 && parent.IsActive == true).AsQueryable()
                                                                             .Select(parentData => new MenuData
                                                                             {
                                                                                 MenuId = parentData.MenuId,
                                                                                 Title = parentData.Title
                                                                             });
                return titleList.ToList<MenuData>();
            }
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// GetRoles
        /// </summary>
        /// <returns>List</returns>
        public List<RoleData> GetRoles()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<RoleData> roleList = hrmsEntities.Roles.Where(role => role.IsActive == true).AsQueryable()
                                                                             .Select(roleData => new RoleData
                                                                             {
                                                                                 RoleName = roleData.RoleName
                                                                             });
                return roleList.ToList<RoleData>();
            }
        }
        #endregion

        #region GetTargetMenuRoles
        /// <summary>
        /// GetTargetMenuRoles
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<Menus>> GetTargetMenuRoles(int RoleId)
        {
            List<Menus> lstMenus;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstMenus = await apEntities.Database.SqlQuery<Menus>
                              ("[dbo].[usp_GetTargetMenusByRoleId] @RoleId",
                             new object[] {
                                        new SqlParameter ("RoleId", RoleId),
                                       
                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstMenus;
        }
        #endregion

        #region GetSourceMenuRoles
        /// <summary>
        /// GetSourceMenuRoles
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<Menus>> GetSourceMenuRoles(int RoleId)
        {
            List<Menus> lstMenus;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstMenus = await apEntities.Database.SqlQuery<Menus>
                              ("[dbo].[usp_GetSourceMenusByRoleId] @RoleId",
                             new object[] {
                                        new SqlParameter ("RoleId", RoleId),

                             }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstMenus.OrderBy(menu=>menu.MenuName).ToList();
        }
        #endregion

        #region UpdateTargetMenuRoles
        /// <summary>
        /// UpdateTargetMenuRoles
        /// </summary>
        /// <returns>List</returns>
        public async Task<bool> AddTargetMenuRoles(MenuRoles MenuRoles)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        //Delete Existing MenuRoles 
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                             ("[usp_DeleteMenuRoles] @RoleId",
                                       new object[] {
                                        new SqlParameter ("RoleId", MenuRoles.RoleId)
                                       }
                                       ).SingleOrDefaultAsync();
                
                    for (int i = 0; i < MenuRoles.MenuList.Count; i++)
                        {
                            rowsAffected = await apEntities.Database.SqlQuery<int>
                                   ("[usp_AddTargetMenuRoles]  @RoleId, @MenuId, @CreatedUser, @SystemInfo, @CreatedDate",
                                       new object[] {
                                        new SqlParameter ("RoleId", MenuRoles.RoleId),
                                        new SqlParameter ("MenuId", MenuRoles.MenuList[i].MenuId),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                       }
                                       ).SingleOrDefaultAsync();
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a Appreciation");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion
    }
}

