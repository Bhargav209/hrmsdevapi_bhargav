﻿using System;
using System.Globalization;
using System.Linq;
using System.Data.Entity;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Web;
using System.Resources;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace AP.API
{
    public class ProjectRoleAssignment
    {
        #region AssignProjectRole
        /// <summary>
        ///  Functionality for assigning an associate's project role
        /// </summary>
        /// <param name="roleAssignmentData"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public bool AssignProjectRole(ProjectRoleData roleAssignmentData, string statusCode)
        {
            bool isAssigned = false;
            string hrCode, programManagerCode;
            DateTime? fromDate, toDate;

            try
            {
                string currentUser = HttpContext.Current.User.Identity.Name;
                ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

                using (APEntities hrmsEntities = new APEntities())
                {
                    var userRole = (from emp in hrmsEntities.Employees
                                    join allocation in hrmsEntities.AssociateAllocations on emp.EmployeeId equals allocation.EmployeeId
                                    where (emp.IsActive == true && allocation.IsActive == true && emp.EmployeeId == roleAssignmentData.EmployeeId)
                                    select new PersonalDetails
                                    {
                                        empID = emp.EmployeeId,
                                        firstName = emp.FirstName,
                                        lastName = emp.LastName,
                                        //PrimaryRole = allocation.IsPrimary,
                                        //RoleId = allocation.RoleId,
                                        //EmployeeName = emp.FirstName + " " + emp.LastName
                                    }).FirstOrDefault();
                    //userRole.Select(i => { i.empName = i.firstName + " " + i.lastName; return i; }).OrderBy(n => n.firstName).ToList();

                    ProjectRoleDetail projectRoleDetail = null;

                    if (roleAssignmentData.IsPrimaryRole == null || roleAssignmentData.IsPrimaryRole == false)
                    {

                        if (roleAssignmentData.FromDate != null && roleAssignmentData.ToDate != null)
                        {
                            fromDate = Commons.GetDateTimeInIST(roleAssignmentData.FromDate);
                            toDate = Commons.GetDateTimeInIST(roleAssignmentData.ToDate);

                            //var previousData = hrmsEntities.ProjectRoleDetails.
                            //Where(p => p.ProjectId == roleAssignmentData.ProjectId && p.EmployeeId == roleAssignmentData.EmployeeId && p.IsActive == true &&
                            //p.RoleId == roleAssignmentData.RoleId && p.FromDate == fromDate && p.ToDate == toDate).FirstOrDefault();

                            var previousData = hrmsEntities.ProjectRoleDetails.
                           Where(p => p.ProjectId == roleAssignmentData.ProjectId && p.EmployeeId == roleAssignmentData.EmployeeId && p.IsActive == true &&
                           p.RoleMasterId == roleAssignmentData.RoleMasterId).ToList();
                            foreach (var date in previousData)
                            {
                                if (date.FromDate <= fromDate && fromDate <= date.ToDate)
                                {
                                    throw new AssociatePortalException("This allocation already exists, please check the role allocation history");
                                }

                            }

                            if (previousData != null) //== null)
                            {
                                projectRoleDetail = new ProjectRoleDetail();
                                projectRoleDetail.EmployeeId = roleAssignmentData.EmployeeId;
                                projectRoleDetail.RoleMasterId = roleAssignmentData.RoleMasterId;
                                projectRoleDetail.ProjectId = roleAssignmentData.ProjectId;
                                projectRoleDetail.IsActive = true;
                                projectRoleDetail.IsPrimaryRole = roleAssignmentData.IsPrimaryRole;
                                projectRoleDetail.FromDate = fromDate;
                                projectRoleDetail.ToDate = toDate;
                                projectRoleDetail.CreatedBy = roleAssignmentData.CurrentUser;
                                projectRoleDetail.CreatedDate = DateTime.Now;
                                projectRoleDetail.SystemInfo = roleAssignmentData.SystemInfo;
                                hrmsEntities.ProjectRoleDetails.Add(projectRoleDetail);

                                ProjectRole projectRole = hrmsEntities.ProjectRoles.Where(p => p.ProjectId == roleAssignmentData.ProjectId && p.RoleMasterId == roleAssignmentData.RoleMasterId && p.IsActive == true).FirstOrDefault();
                                if (projectRole != null)
                                {
                                    projectRole.Responsibilities = roleAssignmentData.KeyResponsibilities;
                                    projectRole.ModifiedDate = DateTime.Now;
                                    projectRole.ModifiedUser = roleAssignmentData.CurrentUser;
                                    projectRole.SystemInfo = roleAssignmentData.SystemInfo;
                                    hrmsEntities.Entry(projectRole).State = EntityState.Modified;
                                }

                                isAssigned = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                            //else
                            //    throw new AssociatePortalException("This allocation already exists, please check the role allocation history");
                        }
                        else
                            throw new AssociatePortalException("Please mention the duration for this allocation");
                    }
                    else if (roleAssignmentData.IsPrimaryRole == true)
                    {
                        var existingAllocation = hrmsEntities.AssociateAllocations.
                            Where(a => a.EmployeeId == roleAssignmentData.EmployeeId && a.RoleMasterId == roleAssignmentData.RoleMasterId && a.IsPrimary == true && a.IsActive == true).FirstOrDefault();

                        string roleName = hrmsEntities.Database.SqlQuery<string>
                                            ("[usp_GetRoleNameByRoleId] @RoleMasterID",
                                            new object[]  {
                                                new SqlParameter ("RoleMasterID", roleAssignmentData.RoleMasterId),
                                            }
                                            ).FirstOrDefault();
                        //hrmsEntities.Roles.Where(r => r.RoleId == roleAssignmentData.RoleId && r.IsActive == true).Select(r => r.RoleName).FirstOrDefault();

                        if (existingAllocation != null)
                            throw new AssociatePortalException("{0} has {1} as primary role already", userRole.firstName, roleName);

                        else
                        {
                            if (!string.IsNullOrWhiteSpace(statusCode))
                            {
                                var fromRole = (from au in hrmsEntities.AssociateAllocations
                                                join r in hrmsEntities.RoleMasters.AsNoTracking() on au.RoleMasterId equals r.RoleMasterID
                                                join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                                from sufflist in suffixtemp.DefaultIfEmpty()
                                                join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                                from preflist in prefixtemp.DefaultIfEmpty()
                                                join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID
                                                where r.IsActive == true && au.IsPrimary == true && au.EmployeeId == roleAssignmentData.EmployeeId && au.IsActive == true
                                                select new { RoleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName }).FirstOrDefault();
                                if (fromRole != null)
                                {
                                    int statusId = hrmsEntities.Status.Where(s => s.StatusCode == statusCode && s.IsActive == true).Select(id => id.StatusId).FirstOrDefault();

                                    if (statusId == 0)
                                        throw new AssociatePortalException($"Status - for this primary role change is not found.");

                                    var previousData = hrmsEntities.ProjectRoleDetails.
                                        Where(s => s.StatusId == statusId && s.EmployeeId == roleAssignmentData.EmployeeId && s.ProjectId == roleAssignmentData.ProjectId && s.RoleMasterId == roleAssignmentData.RoleMasterId && s.IsActive == true).FirstOrDefault();

                                    if (previousData != null)
                                        throw new AssociatePortalException("{0}'s {1} primary role change has already submitted for approval", userRole.firstName, roleName);

                                    else
                                    {
                                        if (resourceManager.GetString("RoleSubmittedForApproval").Equals(statusCode))
                                        {
                                            hrCode = resourceManager.GetString("HRManagerRoleName");
                                            var projectDetails = hrmsEntities.ProjectManagers.Where(i => i.ProjectID == roleAssignmentData.ProjectId && i.IsActive == true).FirstOrDefault();
                                            var projectData = hrmsEntities.Projects.Where(i => i.ProjectId == roleAssignmentData.ProjectId && i.IsActive == true).FirstOrDefault();
                                            if (projectDetails != null)
                                            {
                                                string pmMail = hrmsEntities.Employees.Where(i => i.EmployeeId == projectDetails.ReportingManagerID && i.IsActive == true).Select(i => i.WorkEmailAddress).FirstOrDefault();
                                                //string projectName = hrmsEntities.Projects.Where(i => i.ProjectId == roleAssignmentData.ProjectId && i.IsActive == true).Select(i => i.ProjectName).FirstOrDefault();
                                                //var hrMail = new KRA().GetAssociateByRole(hrmsEntities, hrCode);

                                                projectRoleDetail = new ProjectRoleDetail();
                                                projectRoleDetail.EmployeeId = roleAssignmentData.EmployeeId;
                                                projectRoleDetail.RoleMasterId = roleAssignmentData.RoleMasterId;
                                                projectRoleDetail.ProjectId = roleAssignmentData.ProjectId;
                                                projectRoleDetail.IsActive = false;
                                                projectRoleDetail.IsPrimaryRole = roleAssignmentData.IsPrimaryRole;
                                                projectRoleDetail.StatusId = statusId;
                                                projectRoleDetail.CreatedBy = roleAssignmentData.CurrentUser;
                                                projectRoleDetail.CreatedDate = DateTime.Now;
                                                projectRoleDetail.SystemInfo = roleAssignmentData.SystemInfo;
                                                hrmsEntities.ProjectRoleDetails.Add(projectRoleDetail);

                                                ProjectRole projectRole = hrmsEntities.ProjectRoles.Where(p => p.ProjectId == roleAssignmentData.ProjectId && p.RoleMasterId == roleAssignmentData.RoleMasterId && p.IsActive == true).FirstOrDefault();
                                                if (projectRole != null)
                                                {
                                                    projectRole.Responsibilities = roleAssignmentData.KeyResponsibilities;
                                                    projectRole.ModifiedDate = DateTime.Now;
                                                    projectRole.ModifiedUser = roleAssignmentData.CurrentUser;
                                                    projectRole.SystemInfo = roleAssignmentData.SystemInfo;
                                                    hrmsEntities.Entry(projectRole).State = EntityState.Modified;
                                                }
                                                isAssigned = hrmsEntities.SaveChanges() > 0 ? true : false;

                                                //SendApprovalNotification(currentUser, pmMail, hrMail.WorkEmail, statusCode, projectData.ProjectName, roleAssignmentData.EmployeeId, fromRole.RoleName, roleName);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new AssociatePortalException("{0} has no primary role previously, so primary role cannot be assigned", userRole.firstName);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return isAssigned;
        }
        #endregion

        #region SendApprovalNotification
        /// <summary>
        /// SendApprovalNotification
        /// </summary>
        /// <param name="fromMail"></param>
        /// <param name="toMail"></param>
        /// <param name="ccMail"></param>
        /// <param name="empName"></param>
        /// <param name="status"></param>
        /// <param name="roles"></param>
        //private static void SendApprovalNotification(string fromMail, string toMail, string ccMail, string status, string projectName, int? employeeId, string fromRole, string roles)
        //{
        //    string toName = string.Empty;
        //    int firstDotPosition, atPosition, length;
        //    try
        //    {
        //        using (APEntities hrmEntities = new APEntities())
        //        {
        //            //if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(empName) && !string.IsNullOrEmpty(roles))
        //            //{
        //            NotificationConfiguration configuration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == status).FirstOrDefault();
        //            var associateName = hrmEntities.Employees.Where(i => i.EmployeeId == employeeId && i.IsActive == true).Select(n => new { empName = n.FirstName + " " + n.LastName }).FirstOrDefault();
        //            if (configuration != null)
        //            {
        //                firstDotPosition = !string.IsNullOrEmpty(toMail) ? toMail.IndexOf('.') : configuration.emailTo.IndexOf('.');
        //                atPosition = !string.IsNullOrEmpty(toMail) ? toMail.IndexOf('@') : configuration.emailTo.IndexOf('@');
        //                string firstPart = !string.IsNullOrEmpty(toMail) ? toMail : configuration.emailTo;

        //                if (firstDotPosition < atPosition)
        //                {
        //                    firstPart = firstPart.Substring(0, firstDotPosition);
        //                    toName = char.ToUpper(firstPart[0]) + firstPart.Substring(1);
        //                }
        //                else
        //                {
        //                    firstPart = firstPart.Substring(0, atPosition);
        //                    toName = char.ToUpper(firstPart[0]) + firstPart.Substring(1);
        //                }

        //                string mailContent = configuration.emailContent;
        //                string mailSubject = configuration.emailSubject;

        //                if (!string.IsNullOrWhiteSpace(associateName.empName))
        //                    mailContent = mailContent.Replace("@AssociateName", associateName.empName);

        //                if (!string.IsNullOrWhiteSpace(fromRole))
        //                    mailContent = mailContent.Replace("@OldPrimaryRole", fromRole);

        //                if (!string.IsNullOrWhiteSpace(roles))
        //                {
        //                    mailContent = mailContent.Replace("@NewPrimaryRole", roles);
        //                    mailSubject = mailSubject.Replace("@NewPrimaryRole", roles);
        //                }

        //                if (!string.IsNullOrEmpty(toName))
        //                    mailContent = mailContent.Replace("@CreatedBy", toName);

        //                if (!string.IsNullOrEmpty(projectName))
        //                {
        //                    mailContent = mailContent.Replace("@ProjectName", projectName);
        //                    mailSubject = mailSubject.Replace("@ProjectName", projectName);
        //                }
        //                //
        //                BaseEmail objBaseEmail = new BaseEmail();
        //                //objBaseEmail.SendEmail(!string.IsNullOrWhiteSpace(toMail) ? toMail : configuration.emailTo,
        //                //             !string.IsNullOrWhiteSpace(fromMail) ? fromMail : configuration.emailFrom,
        //                //             !string.IsNullOrWhiteSpace(ccMail) ? ccMail : configuration.emailCC,
        //                //             configuration.emailSubject,
        //                //             mailContent);

        //                Email email = new Email();
        //                int notificationConfigID = new BaseEmail().BuildEmailObject(email, configuration.emailTo, configuration.emailFrom, configuration.emailCC, mailSubject,
        //                                               mailContent, Enumeration.NotificationStatus.PA.ToString());
        //                new BaseEmail().SendEmail(email, notificationConfigID);
        //            }
        //            // }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        #endregion

        #region RoleApproval
        /// <summary>
        /// RoleApproval
        /// </summary>
        /// <param name="roleAssignmentData"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public bool RoleApproval(ProjectRoleData roleAssignmentData, string statusCode)
        {
            bool isApproved = false;
            int statusId = 0;

            try
            {
                string currentUser = HttpContext.Current.User.Identity.Name;
                ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

                if (!string.IsNullOrEmpty(statusCode))
                {
                    using (APEntities hrmsEntities = new APEntities())
                    {
                        if (resourceManager.GetString("RoleApproved").Equals(statusCode))
                            statusId = hrmsEntities.Status.Where(i => i.StatusCode == statusCode && i.IsActive == true).Select(i => i.StatusId).FirstOrDefault();

                        if (statusId == 0)
                            throw new AssociatePortalException($"Status - for this primary role change is not found.");

                        var employee = (from epr in hrmsEntities.ProjectRoleDetails
                                        join e in hrmsEntities.Employees on epr.EmployeeId equals e.EmployeeId into associates
                                        from emp in associates.DefaultIfEmpty()
                                        join p in hrmsEntities.Projects on epr.ProjectId equals p.ProjectId into project
                                        from pt in project.DefaultIfEmpty()
                                        where emp.EmployeeId == roleAssignmentData.EmployeeId && emp.IsActive == true && pt.IsActive == true
                                        select new
                                        {
                                            Name = emp.FirstName,
                                            Employeecode = emp.EmployeeCode,
                                            ProjectName = pt.ProjectName
                                        }).FirstOrDefault();

                        AssociateAllocation existingAllocation = hrmsEntities.AssociateAllocations.Where(i => i.EmployeeId == roleAssignmentData.EmployeeId && i.IsPrimary == true).FirstOrDefault();
                        if (existingAllocation != null)
                        {
                            //For adding new primary role to an associate
                            AssociateAllocation associateAllocation = new AssociateAllocation();
                            associateAllocation.EmployeeId = roleAssignmentData.EmployeeId;
                            associateAllocation.RoleMasterId = roleAssignmentData.RoleMasterId;
                            associateAllocation.ProjectId = roleAssignmentData.ProjectId;
                            associateAllocation.IsActive = true;
                            associateAllocation.IsPrimary = roleAssignmentData.IsPrimaryRole;
                            associateAllocation.TRId = existingAllocation.TRId;
                            associateAllocation.AllocationPercentage = existingAllocation.AllocationPercentage;
                            associateAllocation.InternalBillingPercentage = existingAllocation.InternalBillingPercentage;
                            associateAllocation.ClientBillingPercentage = existingAllocation.ClientBillingPercentage;
                            associateAllocation.IsCritical = existingAllocation.IsCritical;
                            associateAllocation.IsBillable = existingAllocation.IsBillable;
                            associateAllocation.ReportingManagerId = existingAllocation.ReportingManagerId;
                            associateAllocation.AllocationDate = DateTime.Today;
                            associateAllocation.EffectiveDate = DateTime.Today;
                            associateAllocation.CreatedBy = roleAssignmentData.CurrentUser;
                            associateAllocation.CreateDate = DateTime.Now;
                            associateAllocation.SystemInfo = roleAssignmentData.SystemInfo;
                            hrmsEntities.AssociateAllocations.Add(associateAllocation);

                            existingAllocation.IsPrimary = false;
                            //existingAllocation.IsActive = false;
                            existingAllocation.ModifiedBy = roleAssignmentData.CurrentUser;
                            existingAllocation.ModifiedDate = DateTime.Now;
                            existingAllocation.SystemInfo = roleAssignmentData.SystemInfo;
                            hrmsEntities.Entry(existingAllocation).State = EntityState.Modified;
                        }
                        else
                        {
                            throw new AssociatePortalException("The associate {0} has no primary role earlier hence it cannot be changed ", employee.Name);
                        }

                        ProjectRoleDetail projectRoleDetail = hrmsEntities.ProjectRoleDetails.Where(i => i.RoleAssignmentId == roleAssignmentData.RoleAssignmentId).FirstOrDefault();
                        if (projectRoleDetail != null)
                        {
                            projectRoleDetail.StatusId = statusId;
                            projectRoleDetail.IsActive = true;
                            projectRoleDetail.ModifiedBy = roleAssignmentData.CurrentUser;
                            projectRoleDetail.ModifiedDate = DateTime.Now;
                            projectRoleDetail.SystemInfo = roleAssignmentData.SystemInfo;
                            hrmsEntities.Entry(projectRoleDetail).State = EntityState.Modified;
                        }
                        else
                        {
                            throw new AssociatePortalException("No role was assigned to the employee {0} for approval", employee.Name);
                        }
                        isApproved = hrmsEntities.SaveChanges() > 0 ? true : false;

                        if (isApproved)
                        {
                            string hrCode = resourceManager.GetString("HRManagerRoleName");
                            //var hrMail = new KRA().GetAssociateByRole(hrmsEntities, hrCode);

                            var fromRole = (from au in hrmsEntities.AssociateAllocations
                                            join r in hrmsEntities.RoleMasters.AsNoTracking() on au.RoleMasterId equals r.RoleMasterID
                                            join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                            from sufflist in suffixtemp.DefaultIfEmpty()
                                            join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                            from preflist in prefixtemp.DefaultIfEmpty()
                                            join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID
                                            where r.IsActive == true && au.IsPrimary == true && au.EmployeeId == roleAssignmentData.EmployeeId && au.IsActive == true
                                            select new { RoleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName }).FirstOrDefault();

                            string roleName = hrmsEntities.Roles.Where(r => r.RoleId == roleAssignmentData.RoleMasterId && r.IsActive == true).Select(r => r.RoleName).FirstOrDefault();

                            var projectDetails = hrmsEntities.ProjectManagers.Where(i => i.ProjectID == roleAssignmentData.ProjectId && i.IsActive == true).FirstOrDefault();
                            string leadMail = hrmsEntities.Employees.Where(i => i.EmployeeId == projectDetails.ReportingManagerID && i.IsActive == true).Select(i => i.WorkEmailAddress).FirstOrDefault();
                            var projectData = hrmsEntities.Projects.Where(i => i.ProjectId == roleAssignmentData.ProjectId && i.IsActive == true).FirstOrDefault();
                            //SendApprovalNotification(currentUser, leadMail, hrMail.WorkEmail, statusCode, projectData.ProjectName, roleAssignmentData.EmployeeId, fromRole.RoleName, roleName);

                            DateTime date = DateTime.Today;
                            var currentYear = $"{Convert.ToInt32(date.ToString("yyyy"))}-{Convert.ToInt32(Convert.ToInt32(date.ToString("yy")) + 1)}";
                           // new KRA().KRAAssignment(currentYear, employee.Employeecode, roleAssignmentData.RoleId);
                        }
                        else
                            throw new AssociatePortalException("Failed to approve the role change notication");
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to approve the role");
            }

            return isApproved;
        }
        #endregion

        #region GetRoleNotifications
        /// <summary>
        /// GetRoleNotifications
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetRoleNotifications(int employeeId)
        {
            ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

            try
            {
                string statusCode = resourceManager.GetString("RoleSubmittedForApproval");

                using (APEntities hrmsEntities = new APEntities())
                {
                    int statusId = hrmsEntities.Status.Where(i => i.StatusCode == statusCode && i.IsActive == true).Select(i => i.StatusId).FirstOrDefault();

                    var roleNotifications = (from notifications in hrmsEntities.ProjectRoleDetails
                                             join emp in hrmsEntities.Employees on notifications.EmployeeId equals emp.EmployeeId
                                             join pr in hrmsEntities.Projects on notifications.ProjectId equals pr.ProjectId
                                             join projectmgrs in hrmsEntities.ProjectManagers on pr.ProjectId equals projectmgrs.ProjectID
                                             join r in hrmsEntities.RoleMasters.AsNoTracking() on notifications.RoleMasterId equals r.RoleMasterID
                                             join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                             from sufflist in suffixtemp.DefaultIfEmpty()
                                             join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                             from preflist in prefixtemp.DefaultIfEmpty()
                                             join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID
                                             where notifications.StatusId == statusId && projectmgrs.ReportingManagerID == employeeId && notifications.IsPrimaryRole == true && emp.IsActive == true && r.IsActive == true && pr.IsActive == true//&& emp.IsActive == true && role.IsActive == true
                                             select new ProjectRoleData
                                             {
                                                 RoleAssignmentId = notifications.RoleAssignmentId,
                                                 EmployeeId = notifications.EmployeeId,
                                                 FirstName = emp.FirstName,
                                                 LastName = emp.LastName,
                                                 ProjectId = notifications.ProjectId,
                                                 ProjectName = pr.ProjectName,
                                                 RoleMasterId = notifications.RoleMasterId,
                                                 RoleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName,
                                                 IsPrimaryRole = notifications.IsPrimaryRole,
                                                 StatusId = notifications.StatusId
                                             }).OrderBy(n => n.FirstName).ToList();

                    roleNotifications.Select(i => { i.AssociateName = i.FirstName + " " + i.LastName; return i; }).OrderBy(n => n.FirstName).ToList();

                    return roleNotifications;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get the notification");
            }

        }
        #endregion


        #region GetAssignedRoles
        /// <summary>
        /// GetAssignedRoles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssignedRoles(int projectId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var roleNotification = (from prole in hrmsEntities.ProjectRoleDetails
                                            join emp in hrmsEntities.Employees on prole.EmployeeId equals emp.EmployeeId
                                            join r in hrmsEntities.RoleMasters.AsNoTracking() on prole.RoleMasterId equals r.RoleMasterID
                                            join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                            from sufflist in suffixtemp.DefaultIfEmpty()
                                            join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                            from preflist in prefixtemp.DefaultIfEmpty()
                                            join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID
                                            where (emp.IsActive == true && r.IsActive == true && (prole.IsActive == true || prole.IsPrimaryRole == true) && prole.ProjectId == projectId)
                                            select new ProjectRoleData
                                            {
                                                FirstName = emp.FirstName,
                                                LastName = emp.LastName,
                                                EmployeeId = emp.EmployeeId,
                                                RoleMasterId = r.RoleMasterID,
                                                RoleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName,
                                                FromDate = prole.FromDate,
                                                ToDate = prole.ToDate,
                                                IsPrimaryRole = prole.IsPrimaryRole
                                            }).OrderBy(n => n.FirstName).ToList();
                    roleNotification.Select(i => { i.AssociateName = i.FirstName + " " + i.LastName; return i; }).OrderBy(n => n.FirstName).ToList();
                    return roleNotification;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get the assigned role");
            }
        }
        #endregion

        #region RejectRole
        /// <summary>
        /// RejectRole
        /// </summary>
        /// <param name="roleAssignmentData"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public bool RejectRole(ProjectRoleData roleAssignmentData, string statusCode)
        {
            bool isRejected = false;
            int statusId;
            string leadMail, managerMail;

            try
            {
                string currentUser = HttpContext.Current.User.Identity.Name;
                ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

                if (resourceManager.GetString("RoleChangeRejected").Equals(statusCode))
                {
                    using (APEntities hrmsEntities = new APEntities())
                    {
                        statusId = hrmsEntities.Status.Where(i => i.StatusCode == statusCode && i.IsActive == true).Select(i => i.StatusId).FirstOrDefault();

                        if (statusId == 0)
                            throw new AssociatePortalException($"Status - for this primary role change is not found.");

                        ProjectRoleDetail projectRoleDetail = hrmsEntities.ProjectRoleDetails.Where(i => i.RoleAssignmentId == roleAssignmentData.RoleAssignmentId).FirstOrDefault();
                        if (projectRoleDetail != null)
                        {
                            int? leadId = hrmsEntities.ProjectManagers.Where(i => i.ProjectID == roleAssignmentData.ProjectId && i.IsActive == true).Select(i => i.ReportingManagerID).FirstOrDefault();
                            leadMail = hrmsEntities.Employees.Where(i => i.EmployeeId == leadId && i.IsActive == true).Select(i => i.WorkEmailAddress).FirstOrDefault();

                            int? managerId = hrmsEntities.ProjectManagers.Where(i => i.ProjectID == roleAssignmentData.ProjectId && i.IsActive == true).Select(i => i.ProgramManagerID).FirstOrDefault();
                            managerMail = hrmsEntities.Employees.Where(i => i.EmployeeId == managerId && i.IsActive == true).Select(i => i.WorkEmailAddress).FirstOrDefault();

                            var empData = hrmsEntities.Employees.Where(i => i.EmployeeId == roleAssignmentData.EmployeeId && i.IsActive == true).Select(e => new { eMail = e.WorkEmailAddress, eName = e.FirstName }).FirstOrDefault();
                            var projectDetails = hrmsEntities.Projects.Where(i => i.ProjectId == roleAssignmentData.ProjectId && i.IsActive == true).FirstOrDefault();

                            projectRoleDetail.StatusId = statusId;
                            projectRoleDetail.RejectReason = roleAssignmentData.RejectReason;
                            projectRoleDetail.ModifiedBy = roleAssignmentData.CurrentUser;
                            projectRoleDetail.ModifiedDate = DateTime.Now;
                            projectRoleDetail.SystemInfo = roleAssignmentData.SystemInfo;
                            hrmsEntities.Entry(projectRoleDetail).State = EntityState.Modified;

                            var fromRole = (from au in hrmsEntities.AssociateAllocations
                                            join r in hrmsEntities.RoleMasters.AsNoTracking() on au.RoleMasterId equals r.RoleMasterID
                                            join suffix in hrmsEntities.SGRoleSuffixes on r.SuffixID equals suffix.SuffixID into suffixtemp
                                            from sufflist in suffixtemp.DefaultIfEmpty()
                                            join prefix in hrmsEntities.SGRolePrefixes on r.PrefixID equals prefix.PrefixID into prefixtemp
                                            from preflist in prefixtemp.DefaultIfEmpty()
                                            join sgrole in hrmsEntities.SGRoles on r.SGRoleID equals sgrole.SGRoleID
                                            where r.IsActive == true && au.IsPrimary == true && au.EmployeeId == roleAssignmentData.EmployeeId && au.IsActive == true
                                            select new { RoleName = preflist.PrefixName + " " + sgrole.SGRoleName + " " + sufflist.SuffixName }).FirstOrDefault();

                           // SendApprovalNotification(managerMail, leadMail, empData.eMail, statusCode, projectDetails.ProjectName, roleAssignmentData.EmployeeId, fromRole.RoleName, roleAssignmentData.RoleName);
                        }
                        else
                        {
                            throw new AssociatePortalException("No data exists for rejection");
                        }

                        isRejected = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to reject details");
            }

            return isRejected;
        }
        #endregion

        #region CountRoleNotifications
        /// <summary>
        /// CountRoleNotifications
        /// </summary>
        /// <returns></returns>
        public int CountRoleNotifications()
        {
            ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);
            int roleNotifications;

            try
            {
                string statusCode = resourceManager.GetString("RoleSubmittedForApproval");

                using (APEntities hrmsEntities = new APEntities())
                {
                    int statusId = hrmsEntities.Status.Where(i => i.StatusCode == statusCode && i.IsActive == true).Select(i => i.StatusId).FirstOrDefault();

                    roleNotifications = hrmsEntities.ProjectRoleDetails.Where(n => n.StatusId == statusId && n.IsPrimaryRole == true).Count();

                    return roleNotifications;
                }
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region GetProjectManagerOrLeadId
        /// <summary>
        /// GetProjectManagerOrLeadId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public int? GetProjectManagerOrLeadId(int employeeId)
        {
            int? associateId;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    int? manager = hrmsEntities.ProjectManagers.Where(p => p.ProgramManagerID == employeeId && p.IsActive == true).Select(i => i.ProgramManagerID).FirstOrDefault();

                    int? lead = hrmsEntities.ProjectManagers.Where(p => p.ReportingManagerID == employeeId && p.IsActive == true).Select(i => i.ReportingManagerID).FirstOrDefault();

                    if (manager != null && manager != 0)
                        associateId = manager;
                    else
                        associateId = lead;
                }
            }
            catch
            {
                throw;
            }
            return associateId;
        }
        #endregion

        #region GetRolesByProjectId
        /// <summary>
        /// GetRolesByProjectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetRolesByProjectId(int projectId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var projectRoles = hrmsEntities.Database.SqlQuery<string>
                                        ("[usp_GetRolesByProjectId] @ProjectId",
                                        new object[]  {
                                            new SqlParameter ("ProjectId", projectId),
                                        }
                                        ).ToList();

                    return projectRoles;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetRoleMasterDetailByRoleId
        /// <summary>
        /// GetRoleMasterDetails
        /// </summary>
        /// <returns></returns>
        /// 
        public object GetRoleMasterDetailByRoleId(int roleid, int projectId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var ddlRoleData = (from a in hrmEntities.Roles
                                       join p in hrmEntities.ProjectRoles on a.RoleId equals p.RoleMasterId
                                       where a.RoleId == roleid && p.ProjectId == projectId && a.IsActive == true && p.IsActive == true
                                       select new
                                       {
                                           RoleId = a.RoleId,
                                           RoleName = a.RoleName,
                                           RoleDescription = a.RoleDescription,
                                           KeyResponsibilities = p.Responsibilities != null ? p.Responsibilities : a.KeyResponsibilities,
                                           //a.EducationQualification,
                                       }).FirstOrDefault();

                    return ddlRoleData;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
}
