﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Resources;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.IO;
using System.Data;
using Excel;
using System.Web;
using System.Text;

namespace AP.API
{
    public partial class AssociateUtility : IDisposable
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region GetAssociatesUtilizaton
        /// <summary>
        /// Gets Associate's utilization in a project
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetAssociatesUtilizaton()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var asssociatesUtilizaton = (from aa in hrmsEntity.AssociateAllocations
                                                 join em in hrmsEntity.Employees on aa.EmployeeId equals em.EmployeeId
                                                 join p in hrmsEntity.Projects on aa.ProjectId equals p.ProjectId
                                                 join r in hrmsEntity.RoleMasters on aa.RoleMasterId equals r.RoleMasterID
                                                 where aa.IsActive == true
                                                 select new
                                                 {
                                                     EmployeeId = em.FirstName + " " + em.LastName,
                                                     ProjectId = p.ProjectName,
                                                     PercentUtilization = aa.AllocationPercentage,
                                                     Billable = aa.ClientBillingPercentage,
                                                     Critical = aa.IsCritical,
                                                     Role = r.RoleDescription
                                                 }).OrderBy(x => x.EmployeeId).Distinct().OrderBy(i => i.EmployeeId).ToList();

                    return asssociatesUtilizaton;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetPercentUtilization
        /// <summary>
        /// Gets the percent of utilization of an associate in a project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetPercentUtilization(int projectId, int employeeId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var percentUtilize = (from au in hrmsEntity.AssociateUtilizes
                                          where (au.ProjectId == projectId && au.EmployeeId == employeeId) && (au.IsActive == true)
                                          select new
                                          {
                                              PercentUtilization = au.PercentUtilization
                                          }).ToList();

                    return percentUtilize;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose
        /// </summary>
        /// 
        public void Dispose()
        {
            APEntities hrmsEntity = new APEntities();
            hrmsEntity.Dispose();
            GC.SuppressFinalize(this);
        }
        #endregion

        #region AddAssociateUtilize
        /// <summary>
        /// Inserts associate's utilization percent into database
        /// </summary>
        /// <param name="associateUtilize"></param>
        /// <returns></returns>
        /// 
        public int AddAssociateUtilize(AssociateUsage associateUsage)
        {

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var PGMAvail = (from au in hrmsEntity.AssociateUtilizes
                                    where au.ProjectId == associateUsage.ProjectId && au.RoleMasterId == (int)Enumeration.Roles.PGM && au.IsActive == true
                                    select au.EmployeeId).ToList();

                    if (associateUsage.RoleMasterId != (int)Enumeration.Roles.PGM && PGMAvail.Count == 0)
                    {
                        throw new InvalidOperationException(resourceManager.GetString("PGMNotAvailable"));
                    }

                    var TLAvail = (from au in hrmsEntity.AssociateUtilizes
                                   where au.ProjectId == associateUsage.ProjectId && au.RoleMasterId == (int)Enumeration.Roles.TL && au.IsActive == true
                                   select au.EmployeeId).ToList();

                    if (associateUsage.RoleMasterId == (int)Enumeration.Roles.TL && TLAvail.Count > 0)
                    {
                        throw new InvalidOperationException(resourceManager.GetString("TLAvailable"));
                    }

                    if ((associateUsage.RoleMasterId != (int)Enumeration.Roles.TL && associateUsage.RoleMasterId != (int)Enumeration.Roles.PGM) && TLAvail.Count == 0)
                    {
                        throw new InvalidOperationException(resourceManager.GetString("TLNotAvailable"));
                    }

                    if (associateUsage.RoleMasterId != (int)Enumeration.Roles.PGM && associateUsage.RoleMasterId != (int)Enumeration.Roles.CDM)
                    {
                        var CheckReportingManager = (from au in hrmsEntity.AssociateUtilizes
                                                     where au.EmployeeId == associateUsage.EmployeeId && au.IsActive == true && au.ProjectId != associateUsage.ProjectId && au.RoleMasterId != (int)Enumeration.Roles.PGM && au.RoleMasterId != (int)Enumeration.Roles.CDM
                                                     select au.EmployeeId).ToList();

                        if (CheckReportingManager.Count > 0 && (associateUsage.ManagerId != (int)Enumeration.Roles.PGM && associateUsage.ManagerId != (int)Enumeration.Roles.CDM))
                        {
                            throw new InvalidOperationException(resourceManager.GetString("InvalidManager"));
                        }
                    }

                    AssociateUtilize oldAssociateUtilize = (from au in hrmsEntity.AssociateUtilizes
                                                            where (au.ProjectId == associateUsage.ProjectId && au.EmployeeId == associateUsage.EmployeeId) && (au.IsActive == true)
                                                            select au).SingleOrDefault();

                    var totalPercenatge = GetTotalPercentUtilize(associateUsage.EmployeeId);


                    int? total = 0;

                    if (associateUsage.RoleMasterId == (int)Enumeration.Roles.PGM || associateUsage.RoleMasterId == (int)Enumeration.Roles.CDM)
                    {
                        total = null;
                    }
                    else if (oldAssociateUtilize == null)
                    {
                        total = Convert.ToInt32(totalPercenatge) + associateUsage.PercentUtilization;
                    }
                    else
                    {
                        total = Convert.ToInt32(totalPercenatge) + associateUsage.PercentUtilization - oldAssociateUtilize.PercentUtilization;
                    }

                    if (total == null || (total > 0 && total <= 100))
                    {
                        if (oldAssociateUtilize != null)
                        {
                            //deactivate old record
                            oldAssociateUtilize.IsActive = false;
                        }
                        //insert
                        AssociateUtilize associateUtilize = new AssociateUtilize();
                        associateUtilize.ProjectId = associateUsage.ProjectId;
                        associateUtilize.EmployeeId = associateUsage.EmployeeId;
                        associateUtilize.IsActive = true;
                        associateUtilize.RoleMasterId = associateUsage.RoleMasterId;
                        associateUtilize.PercentUtilization = associateUsage.PercentUtilization;
                        associateUtilize.Billable = associateUsage.Billable;
                        associateUtilize.Critical = associateUsage.Critical;
                        associateUtilize.ManagerId = associateUsage.ManagerId;
                        associateUtilize.PrimaryManager = associateUsage.PrimaryManager;
                        associateUtilize.ActualStartDate = DateTime.Now;
                        associateUtilize.ActualEndDate = associateUsage.ActualEndDate;
                        associateUtilize.CreatedDate = DateTime.Now;
                        associateUtilize.CreatedUser = associateUsage.CurrentUser;
                        associateUtilize.SystemInfo = associateUsage.SystemInfo;
                        hrmsEntity.AssociateUtilizes.Add(associateUtilize);
                        var result = hrmsEntity.SaveChanges();
                        return result;
                    }
                    else
                    {
                        throw new InvalidOperationException(resourceManager.GetString("OverAllocation"));
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save details.");
            }
        }
        #endregion

        #region GetTotalPercentUtilize
        /// <summary>
        /// Gets the total percent of utilization associate
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        /// 
        internal object GetTotalPercentUtilize(int employeeId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var totalPercent = (from au in hrmsEntity.AssociateUtilizes
                                        where (au.EmployeeId == employeeId) && (au.IsActive == true)
                                        select new
                                        {
                                            TotalPer = au.PercentUtilization
                                        }).ToList();

                    return totalPercent.Select(c => c.TotalPer).Sum();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
             
        }
        #endregion

        #region Import excel data to the database
        /// <summary>
        /// Import Associate Utilize Report
        /// </summary>
        /// <returns></returns>
        public object ImportAssociateUtilizeReport()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    DataTable reports = GetDataFromExcel();

                    // Map skills
                    MapSkills(reports);

                    //Add employees
                    AddEmployeeForReport(reports);

                    //Add projects
                    AddProjectForReport(reports);
                    
                    //Add resource utilization
                    return AddAssociateUtilization(reports);
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");

                throw new AssociatePortalException("Failed to get details.");
            }
        }
        /// <summary>
        /// Get data from Excel
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private DataTable GetDataFromExcel()
        {
            DataTable dt = null;
            var httpRequest = HttpContext.Current.Request;

            if (httpRequest.Files.Count > 0)
            {
                string extension = Path.GetExtension(httpRequest.Files[0].FileName);
                var postedFile = httpRequest.Files[0];

                string fileName = httpRequest.Form["documentName"].ToString().Split('.')[0];

                bool isExists = Directory.Exists(HttpContext.Current.Server.MapPath("~/Files/"));

                if (!isExists)
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Files/"));

                //Whatever we want to give fileName and serverpath to save the file/files.

                var path = HttpContext.Current.Server.MapPath("~/Files/" + fileName + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + extension);
                postedFile.SaveAs(path);

                FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet reports = excelReader.AsDataSet();
                // Add Columns
                for (int i = 0; i <= 16; i++)
                {
                    reports.Tables[0].Columns[i].ColumnName = reports.Tables[0].Columns[i].ColumnName.Replace(" ", "");
                    reports.Tables[0].AcceptChanges();
                }
                dt = reports.Tables[0];

                //Remove empty rows
                List<DataRow> emptyRows = (from row in dt.AsEnumerable()
                                           where row.Field<string>("AssociateID") == ""
                                           || row.Field<string>("AssociateID") == null
                                           || row.Field<string>("AssociateID").StartsWith("Associate")
                                           select row).ToList();

                foreach (DataRow row in emptyRows)
                {
                    dt.Rows.Remove(row);
                    dt.AcceptChanges();
                }

                //Add Id colomns
                dt.Columns.Add("ProjectId");
                dt.Columns.Add("ClientId");
                dt.Columns.Add("ProjectTypeId");
                dt.Columns.Add("RoleId");
                dt.Columns.Add("CompetencyId");
                dt.Columns.Add("EmployeeId");
                dt.Columns.Add("SkillGroupId");
                dt.Columns.Add("ManagerId");

                using (APEntities hrmsEntity = new APEntities())
                {
                    List<AssociateDetails> employees = (from emp in hrmsEntity.Employees
                                                        where emp.IsActive == true
                                                        select new AssociateDetails
                                                        {
                                                            empCode = emp.EmployeeCode.ToUpper(),
                                                            empID = emp.EmployeeId,
                                                            empName = (emp.FirstName + " " + emp.LastName).Trim().ToUpper()
                                                        }).ToList();

                    List<ClientDetails> clients = (from client in hrmsEntity.Clients
                                                   select new ClientDetails
                                                   {
                                                       ClientId = client.ClientId,
                                                       ClientName = client.ClientName.Trim().ToUpper()
                                                   }).ToList();


                    List<ProjectTypeDetails> projectTypes = (from type in hrmsEntity.ProjectTypes
                                                             select new ProjectTypeDetails
                                                             {
                                                                 ProjectTypeId = type.ProjectTypeId,
                                                                 Description = type.Description.Trim().ToUpper()
                                                             }).ToList();

                    List<RoleData> roles = (from role in hrmsEntity.Roles
                                            select new RoleData
                                            {
                                                RoleId = role.RoleId,
                                                RoleName = role.RoleName.Trim().ToUpper()
                                            }).ToList();

                    int CompetencyID = (from com in hrmsEntity.CompetencyAreas
                                        where com.CompetencyAreaCode.ToUpper() == "TECHNOLOGY"
                                        select com.CompetencyAreaId).FirstOrDefault();

                    List<SkillGroupDetails> skillGroup = (from skill in hrmsEntity.SkillGroups
                                                               select new SkillGroupDetails
                                                               {
                                                                   SkillGroupId = skill.SkillGroupId,
                                                                   SkillGroupName = skill.SkillGroupName.ToUpper()
                                                               }).ToList();

                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["EmployeeId"] = employees.Where(e => e.empCode == dr["AssociateID"].ToString().Trim().ToUpper()).Select(e => e.empID).FirstOrDefault();
                        dr["ManagerId"] = employees.Where(e => e.empCode == dr["ReportingManagerCode"].ToString().Trim().ToUpper()).Select(e => e.empID).FirstOrDefault();
                        dr["ClientId"] = clients.Where(c => c.ClientName == dr["Client"].ToString().Trim().ToUpper()).Select(c => c.ClientId).FirstOrDefault();
                        dr["ProjectTypeId"] = projectTypes.Where(p => p.Description == dr["ProjectGroup"].ToString().Trim().ToUpper()).Select(p => p.ProjectTypeId).FirstOrDefault();
                        dr["RoleId"] = roles.Where(r => r.RoleName == dr["Role"].ToString().Trim().ToUpper()).Select(r => r.RoleId).FirstOrDefault();
                        dr["SkillGroupId"] = skillGroup.Where(skills => skills.SkillGroupName == dr["SkillGroup"].ToString().Trim().ToUpper()).Select(skill => skill.SkillGroupId).FirstOrDefault();
                        dr["CompetencyId"] = CompetencyID;
                    }
                }
            }

            return dt;
        }
        /// <summary>
        /// Add/Update employees from reports
        /// </summary>
        /// <param name="dtReport"></param>
        private void AddEmployeeForReport(DataTable dtReport)
        {
            Nullable<DateTime> myjoindate = new DateTime?();


            using (APEntities hrmsEntity = new APEntities())
            {
                foreach (DataRow dr in dtReport.Rows)
                {
                    try
                    {
                        string associateCode = dr["AssociateID"].ToString().Trim();

                        Employee oldEmployee = associateCode == "" ? null : (from employee in hrmsEntity.Employees
                                                                             where employee.EmployeeCode.Equals(associateCode, StringComparison.CurrentCultureIgnoreCase)
                                                                             select employee).FirstOrDefault();

                        if (oldEmployee == null)
                        {
                            Employee emp = new Employee();
                            emp.FirstName = dr["AssociateName"].ToString().Split(' ')[0];
                            emp.LastName = dr["AssociateName"].ToString().Split(' ').Length < 1 ? "" : dr["AssociateName"].ToString().Split(' ')[1];
                            emp.EmployeeTypeId = 1;
                            emp.EmployeeCode = dr["AssociateID"].ToString();
                            emp.BloogGroup = "O+";
                            emp.IsActive = true;
                            emp.StatusId = 8;
                            emp.Experience = dr["TotalExperience"].ToString() == null ? 0 : Convert.ToDecimal(dr["TotalExperience"].ToString());
                            emp.JoinDate = dr["DateOfJoining(DD-MM-YYYY)"].ToString() == "" ? myjoindate : Convert.ToDateTime(dr["DateOfJoining(DD-MM-YYYY)"]);
                            emp.CreatedDate = DateTime.Now;
                            emp.CreatedUser = HttpContext.Current.User.Identity.Name;
                            emp.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntity.Employees.Add(emp);
                            hrmsEntity.SaveChanges();
                        }
                        else
                        {
                            oldEmployee.FirstName = dr["AssociateName"].ToString().Split(' ')[0];
                            oldEmployee.LastName = dr["AssociateName"].ToString().Split(' ').Length < 2 ? "" : dr["AssociateName"].ToString().Split(' ')[1];
                            oldEmployee.EmployeeTypeId = 1;
                            oldEmployee.BloogGroup = "O+";
                            oldEmployee.IsActive = true;
                            oldEmployee.StatusId = 8;
                            oldEmployee.Experience = dr["TotalExperience"].ToString() == null ? 0 : Convert.ToDecimal(dr["TotalExperience"].ToString());
                            oldEmployee.JoinDate = dr["DateOfJoining(DD-MM-YYYY)"].ToString() == "" ? myjoindate : Convert.ToDateTime(dr["DateOfJoining(DD-MM-YYYY)"]);
                            oldEmployee.ModifiedDate = DateTime.Now;
                            oldEmployee.ModifiedUser = HttpContext.Current.User.Identity.Name;
                            hrmsEntity.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(ex, Log.Severity.Error, "");

                        throw new AssociatePortalException("Failed to add employee.");
                    }
                }
            }
        }
        /// <summary>
        /// Add/Update projects from reports
        /// </summary>
        /// <param name="dtReport"></param>
        private void AddProjectForReport(DataTable dtReport)
        {
            using (APEntities hrmsEntity = new APEntities())
            {
                var drList = (from dr in dtReport.AsEnumerable()
                              select new
                              {
                                  ProjectCode = dr.Field<string>("ProjectCode"),
                                  ProjectName = dr.Field<string>("ProjectName"),
                                  Client = dr.Field<string>("Client"),
                                  ClientID = Convert.ToInt32(dr.Field<string>("ClientId")),
                                  ProjectGroup = dr.Field<string>("ProjectGroup"),
                                  ProjectGroupId = Convert.ToInt32(dr.Field<string>("ProjectTypeId"))
                              }).Distinct().ToList();

                foreach (var dr in drList)
                {
                    try
                    {
                        if (dr.ProjectCode != null && dr.ProjectCode.ToString() != "")
                        {
                            string projectCode = dr.ProjectCode == null ? "" : dr.ProjectCode.ToString().Trim().ToUpper();

                            Project oldProject = (from pro in hrmsEntity.Projects
                                                  where pro.ProjectCode.Equals(projectCode, StringComparison.CurrentCultureIgnoreCase)
                                                  && pro.ProjectTypeId == dr.ProjectGroupId
                                                  && pro.ClientId == dr.ClientID
                                                  select pro).FirstOrDefault();

                            Project project = new Project();
                            project.ProjectName = dr.ProjectName;
                            project.ProjectCode = projectCode;
                            project.ProjectTypeId = dr.ProjectGroupId;
                            project.ClientId = dr.ClientID;

                            if (project.ClientId != 0 && project.ClientId != null && project.ProjectTypeId != 0 && project.ProjectTypeId != null)
                            {
                                if (oldProject == null)
                                {
                                    project.IsActive = true;
                                    project.CreatedDate = DateTime.Now;
                                    project.CreatedUser = HttpContext.Current.User.Identity.Name;
                                    project.SystemInfo = Commons.GetClientIPAddress();
                                    hrmsEntity.Projects.Add(project);
                                    hrmsEntity.SaveChanges();
                                }
                                else
                                {
                                    oldProject.ProjectName = project.ProjectName;
                                    oldProject.IsActive = true;
                                    oldProject.ModifiedDate = DateTime.Now;
                                    oldProject.ModifiedUser = HttpContext.Current.User.Identity.Name;
                                    hrmsEntity.SaveChanges();
                                }
                            }
                            else
                            {
                                string parms = new StringBuilder().AppendFormat("[AssociateUtility.AddProjectForReport : ProjectCode - {0},Client - {1},ProjectName - {2}]", projectCode, dr.Client, dr.ProjectName).ToString();
                                Log.Write(resourceManager.GetString("ListOfNotInsertedProjects"), Log.Severity.Warning, parms);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(ex, Log.Severity.Error, "");

                        throw new AssociatePortalException("Failed to add project.");
                    }
                }

                var projects = (from p in hrmsEntity.Projects
                                select new
                                {
                                    p.ProjectCode,
                                    p.ProjectId
                                }).ToList();
                foreach (DataRow dr in dtReport.Rows)
                {
                    dr["ProjectId"] = projects.Where(p => p.ProjectCode == dr["ProjectCode"].ToString().Trim().ToUpper()).Select(p => p.ProjectId).FirstOrDefault();

                }
            }
        }
        /// <summary>
        /// Map Skills
        /// </summary>
        /// <param name="dtReport"></param>
        private void MapSkills(DataTable dtReport)
        {
            using (APEntities hrmsEntity = new APEntities())
            {
                List<SkillData> skills = (from skill in hrmsEntity.Skills
                                          where skill.IsActive == true
                                          select new SkillData
                                          {
                                              SkillId = skill.SkillId,
                                              SkillCode = skill.SkillCode.ToUpper()
                                          }).ToList();

                int ProficiencyLevelId = (from pl in hrmsEntity.ProficiencyLevels select pl.ProficiencyLevelId).FirstOrDefault();

                var drList = (from dr in dtReport.AsEnumerable()
                              select new
                              {
                                  PrimarySkill = dr.Field<string>("PrimarySkill"),
                                  Role = dr.Field<string>("Role"),
                                  RoleId = Convert.ToInt32(dr.Field<string>("RoleId")),
                                  CompetencyId = Convert.ToInt32(dr.Field<string>("CompetencyID"))
                              }).Distinct().ToList();

                foreach (var dr in drList)
                {
                    try
                    {
                        string[] primarySkill = dr.PrimarySkill == null || dr.PrimarySkill == "" ? new string[] { } : dr.PrimarySkill.ToString().Trim().ToUpper().Split(',');
                        for (int i = 0; i < primarySkill.Length; i++)
                        {
                            primarySkill[i] = primarySkill[i].Trim();
                        }
                        string roleName = dr.Role == null || dr.Role == "" ? "" : dr.Role.ToString().Trim().ToUpper();

                        List<int> Skills = (from skill in skills
                                            where primarySkill.Contains(skill.SkillCode)
                                            select skill.SkillId).ToList();

                        if (dr.RoleId != 0 && dr.CompetencyId != 0 && skills.Count() > 0)
                        {
                            foreach (int skillId in Skills)
                            {
                                CompetencySkill skillexits = (from cs in hrmsEntity.CompetencySkills
                                                              where cs.SkillId == skillId && cs.CompetencyAreaId == dr.CompetencyId && cs.RoleMasterID == dr.RoleId
                                                              select cs).FirstOrDefault();
                                if (skillexits == null)
                                {
                                    skillexits = new CompetencySkill();
                                    skillexits.RoleMasterID = dr.RoleId;
                                    skillexits.CompetencyAreaId = dr.CompetencyId;
                                    skillexits.SkillId = skillId;
                                    skillexits.IsActive = true;
                                    skillexits.ProficiencyLevelId = ProficiencyLevelId;
                                    skillexits.SystemInfo = Commons.GetClientIPAddress();
                                    skillexits.CreatedUser = HttpContext.Current.User.Identity.Name;
                                    skillexits.CreatedDate = DateTime.Now;
                                    hrmsEntity.CompetencySkills.Add(skillexits);
                                    hrmsEntity.SaveChanges();
                                }
                                else
                                {
                                    skillexits.ModifiedUser = HttpContext.Current.User.Identity.Name;
                                    skillexits.ModifiedDate = DateTime.Now;
                                    skillexits.IsActive = true;
                                    hrmsEntity.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            string parms = new StringBuilder().AppendFormat("[MapSkills: primarySkill - {0},roleName - {1}]", dr.PrimarySkill, roleName).ToString();
                            Log.Write(resourceManager.GetString("ListOfNotMappedSkills"), Log.Severity.Warning, parms);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.LogError(ex, Log.Severity.Error, "");

                        throw new AssociatePortalException("Failed to map skills.");
                    }
                }
            }
        }
        /// <summary>
        /// Add Skills
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="skills"></param>
        /// <param name="roleId"></param>
        /// <param name="skillGroupId"></param>
        private void AddSkills(int empId, string skills, int roleId, int skillGroupId)
        {
            using (APEntities hrmsEntity = new APEntities())
            {
                try
                {
                    UserDetails userDetail = new UserDetails();
                    userDetail.empID = empId;
                    string[] primarySkill = skills.ToString().Trim().ToUpper().Split(',');
                    for (int i = 0; i < primarySkill.Length; i++)
                    {
                        primarySkill[i] = primarySkill[i].Trim();
                    }

                    IEnumerable<EmployeeSkillDetails> skillList = (from skill in hrmsEntity.Skills
                                                                   where primarySkill.Contains(skill.SkillCode.ToUpper())
                                                                   select new EmployeeSkillDetails
                                                                   {
                                                                       skillID = skill.SkillId,
                                                                       SkillName = skill.SkillName,
                                                                       RoleId = roleId,
                                                                       CompetencyAreaID = (from ca in hrmsEntity.CompetencyAreas
                                                                                           where ca.CompetencyAreaCode.Equals("Technology", StringComparison.CurrentCultureIgnoreCase)
                                                                                           select ca.CompetencyAreaId).FirstOrDefault(),
                                                                       SkillGroupID = skillGroupId,
                                                                       proficiencyLevelId = 1
                                                                   }).ToList();

                    userDetail.skills = skillList;
                    AssociateSkills obj = new AssociateSkills();
                    //obj.AddAssociateSkills(userDetail);
                }
                catch (Exception ex)
                {
                    Log.LogError(ex, Log.Severity.Error, "");

                    throw new AssociatePortalException("Failed to add skills.");
                }
            }
        }
        /// <summary>
        /// Add AssociateUtilization from reports
        /// </summary>
        /// <param name="reports"></param>
        private object AddAssociateUtilization(DataTable reports)
        {
            List<object> errList = new List<object>();
            int retVal = 0;

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    foreach (DataRow dr in reports.Rows)
                    {
                        AssociateUtilize associateUtilize = new AssociateUtilize();

                        associateUtilize.EmployeeId = Convert.ToInt32(dr["EmployeeId"].ToString());

                        associateUtilize.ProjectId = Convert.ToInt32(dr["ProjectId"].ToString());

                        associateUtilize.RoleMasterId = Convert.ToInt32(dr["RoleId"].ToString());
                        double pu;
                        associateUtilize.PercentUtilization = double.TryParse(dr["%Utilization"].ToString(), out pu) ? Convert.ToInt32(pu * 100) : 0;

                        associateUtilize.ManagerId = Convert.ToInt32(dr["ManagerId"]);

                        associateUtilize.Billable = dr["Billing"].ToString() == "Billable" ? true : false;
                        associateUtilize.Critical = dr["Billing"].ToString() == "Billable" ? true : false;
                        //associateUtilize.Critical = dr["RoleCritical"].ToString() == "Critical-YES" ? true : false;                    
                        DateTime defDate = DateTime.Today;
                        associateUtilize.CreatedDate = DateTime.TryParse(dr["DateofAllocation"].ToString().Trim(), out defDate) ? defDate : DateTime.Today;
                        associateUtilize.IsActive = dr["AllocationStatus"].ToString().Trim() == "YES" ? true : false;

                        //Add skills
                        AddSkills(associateUtilize.EmployeeId, dr["PrimarySkill"].ToString(), associateUtilize.RoleMasterId ?? 0, Convert.ToInt32(dr["SkillGroupId"]));

                        if (associateUtilize.EmployeeId != 0
                            && associateUtilize.ProjectId != 0
                            && associateUtilize.RoleMasterId != 0
                            && associateUtilize.ManagerId != 0)
                        {
                            AssociateUtilize oldAssignment = (from au in hrmsEntity.AssociateUtilizes
                                                              where au.EmployeeId == associateUtilize.EmployeeId && au.ProjectId == associateUtilize.ProjectId && au.RoleMasterId == associateUtilize.RoleMasterId && au.IsActive == true
                                                              select au).FirstOrDefault();

                            if (oldAssignment == null)
                            {
                                associateUtilize.CreatedDate = DateTime.Now;
                                associateUtilize.CreatedUser = HttpContext.Current.User.Identity.Name;
                                associateUtilize.SystemInfo = Commons.GetClientIPAddress();
                                hrmsEntity.AssociateUtilizes.Add(associateUtilize);
                                retVal = retVal + hrmsEntity.SaveChanges();
                            }
                            else
                            {
                                oldAssignment.ManagerId = associateUtilize.ManagerId;
                                oldAssignment.Billable = associateUtilize.Billable;
                                oldAssignment.Critical = associateUtilize.Critical;
                                oldAssignment.PercentUtilization = associateUtilize.PercentUtilization;
                                oldAssignment.IsActive = associateUtilize.IsActive;
                                retVal = retVal + hrmsEntity.SaveChanges();
                            }
                        }
                        else
                        {
                            var obj = new
                            {
                                AssociateCode = dr["AssociateID"].ToString().Trim().ToUpper(),
                                ProjectCode = dr["ProjectCode"].ToString().Trim().ToUpper(),
                                AssociateRole = dr["Role"].ToString().Trim().ToUpper(),
                                ReportingManager = dr["ReportingManagerCode"].ToString().Trim().ToUpper()

                            };

                            errList.Add(obj);
                            string parms = new StringBuilder().AppendFormat("[AssociateUtilize: EmployeeId - {0},ProjectId - {1},RoleId - {2},ManagerId - {3}]", associateUtilize.EmployeeId, associateUtilize.ProjectId, associateUtilize.RoleMasterId, associateUtilize.ManagerId).ToString();
                            Log.Write(resourceManager.GetString("ListOfNotInsertedAssociateUtilization"), Log.Severity.Warning, parms);
                        }
                    }

                    var errorReport = new
                    {
                        reportCount = retVal,
                        errList = errList
                    };
                    return errorReport;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save details.");
            }
        }
        #endregion
    }
}
