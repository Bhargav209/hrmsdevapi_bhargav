﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Web;
using System.Resources;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Threading.Tasks;
using System.Globalization;
using System.Data.SqlClient;
using AP.Notification;
using System.Net;

namespace AP.API
{
    public class AssociatePersonalDetails
    {
        StringBuilder stringBuilder = new StringBuilder();
        string parms = string.Empty;
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region Personal Tab Methods

        #region GetAssociateDetails
        /// <summary>
        /// GetAssociateDetails
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssociateDetails(int? departmentId = null)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    int approved = hrmsEntities.Status.Where(e => "Approved".Equals(e.StatusCode) && e.CategoryID == (int?)Enumeration.CategoryMaster.EPC).Select(e => e.StatusId).FirstOrDefault();
                    if (approved <= 0) throw new Exception("Approved Status is not found.");
                    //
                    var query = (from e in hrmsEntities.Employees
                                 join dep in hrmsEntities.Departments on e.DepartmentId equals dep.DepartmentId into employee
                                 from emp in employee.DefaultIfEmpty()
                                 where e.IsActive == true && e.StatusId == approved
                                 select new AssociateDetails()
                                 {
                                     empID = e.EmployeeId,
                                     empCode = e.EmployeeCode,
                                     empName = e.FirstName + " " + e.LastName,
                                     bgvStatus = e.BGVStatus,
                                     EncryptedMobileNumber = e.MobileNo,
                                     WorkEmail = e.WorkEmailAddress,
                                     PersonalEmail = e.PersonalEmailAddress,
                                     Dob = e.DateofBirth,
                                     DepartmentId = emp.DepartmentId,
                                     Department = emp.Description
                                 }); //.OrderByDescending(i => i.empID).ToList();

                    foreach (var q in query)
                    {
                        var mobileNo = Commons.DecryptStringAES(q.EncryptedMobileNumber);
                        q.MobileNo = mobileNo;
                    }

                    if (departmentId.HasValue)
                        query = query.Where(e => e.DepartmentId == departmentId);
                    var getAssociateDetails = query.ToList();

                    if (getAssociateDetails.Count > 0)
                        getAssociateDetails = getAssociateDetails.OrderByDescending(e => e.empID).ToList();

                    return getAssociateDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get associate details.");
            }
        }

        //public IEnumerable<AssociateDetails> GetDepartmentHeadDetails(int? departmentId = null)
        //{
        //    IEnumerable<AssociateDetails> groupHeads = null;
        //    try
        //    {

        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            int approved = hrmsEntities.Status.Where(e => "Approved".Equals(e.StatusCode)).Select(e => e.StatusId).FirstOrDefault();
        //            if (approved <= 0) throw new AssociatePortalException("Approved Status is not found.");
        //            //
        //            var departments = hrmsEntities.Departments.Where(e => e.IsActive == true)
        //                           .Where(e => e.DepartmentHead.HasValue)
        //                          .Select(e => new { DepartmentCode = e.DepartmentCode, DepartmentHead = e.DepartmentHead })
        //                          .ToList();
        //            List<int?> employeeIds = departments.Where(e => e.DepartmentHead.HasValue).Select(e => e.DepartmentHead).ToList<int?>();

        //            if (employeeIds != null && employeeIds.Count > 0)
        //            {
        //                groupHeads = hrmsEntities.Employees.Where(e => e.IsActive == true)
        //                                            .Where(e => e.StatusId == approved)
        //                                            .Where(e => employeeIds.Any(id => id == e.EmployeeId))
        //                                            .Select(e => new AssociateDetails
        //                                            {
        //                                                empID = e.EmployeeId,
        //                                                empCode = e.EmployeeCode,
        //                                                empName = e.FirstName + " " + e.LastName,
        //                                                WorkEmail = e.WorkEmailAddress,
        //                                                PersonalEmail = e.PersonalEmailAddress,
        //                                            }).ToList();
        //            }
        //            if (groupHeads != null) groupHeads = groupHeads.OrderByDescending(e => e.empName).ToList();
        //            return groupHeads;
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to get details.");
        //    }
        //}

        /// <summary>
        /// This Method fetches all active associates with associated roles. 
        /// If role is not assigned to the associate(s), then those associate(s) will be ignored.
        /// </summary>
        /// <param name="roleName">The Role that you want to retrieve the associates, associated to.</param>
        /// <returns>The list of AssociateDetails object.</returns>
        public IEnumerable<object> GetAssociatesWithRoles(string roleName = null)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var query = (from e in hrmsEntities.Employees
                                 join user in hrmsEntities.Users on e.UserId equals user.UserId
                                 join userRole in hrmsEntities.UserRoles on user.UserId equals userRole.UserId
                                 join role in hrmsEntities.Roles on userRole.RoleId equals role.RoleId
                                 where e.IsActive == true && user.IsActive == true && userRole.IsActive == true && role.IsActive == true
                                 select new AssociateDetails()
                                 {
                                     empID = e.EmployeeId,
                                     empCode = e.EmployeeCode,
                                     empName = e.FirstName + " " + e.LastName,
                                     RoleName = role.RoleName,
                                     WorkEmail = e.WorkEmailAddress,
                                     PersonalEmail = e.PersonalEmailAddress
                                 });

                    if (!string.IsNullOrWhiteSpace(roleName)) query = query.Where(e => e.RoleName == roleName);

                    var associates = query.ToList();
                    if (associates.Count > 0) associates = associates.OrderByDescending(e => e.empName).ToList();

                    return associates;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }

        #endregion

        #region GenerateEmployeeCode
        /// <summary>
        /// Generate Employee Code
        /// </summary>
        /// <param name="emptype"></param>
        /// <returns></returns>
        /// 
        public string GenerateEmployeeCode(string emptype)
        {
            string employeeCode = string.Empty;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var fteEmpType = (from e in hrmsEntities.Employees
                                      join et in hrmsEntities.EmployeeTypes on e.EmployeeTypeId equals et.EmployeeTypeId
                                      where e.EmployeeTypeId == 1 && e.EmployeeCode.StartsWith("N")
                                      select new { e.EmployeeId, e.EmployeeCode }
                                                   ).OrderByDescending(e => e.EmployeeId).FirstOrDefault();

                    var contractEmpType = (from e in hrmsEntities.Employees
                                           join et in hrmsEntities.EmployeeTypes on e.EmployeeTypeId equals et.EmployeeTypeId
                                           where e.EmployeeTypeId == 2 && e.EmployeeCode.StartsWith("C")
                                           select new { e.EmployeeId, e.EmployeeCode }
                                     ).OrderByDescending(e => e.EmployeeId).FirstOrDefault();

                    var traineeEmpType = (from e in hrmsEntities.Employees
                                          join et in hrmsEntities.EmployeeTypes on e.EmployeeTypeId equals et.EmployeeTypeId
                                          where e.EmployeeTypeId == 3 && e.EmployeeCode.StartsWith("T")
                                          select new { e.EmployeeId, e.EmployeeCode }
                                    ).OrderByDescending(e => e.EmployeeId).FirstOrDefault();

                    if (emptype != null)
                    {
                        if (emptype == "FTE" && fteEmpType != null)
                            employeeCode = "N0000".Substring(0, 5 - fteEmpType.EmployeeId.ToString().Length) + (fteEmpType.EmployeeId + 1).ToString();
                        else if (emptype == "FTE" && fteEmpType == null)
                            employeeCode = "N0001";

                        if (emptype == "Contractors" && contractEmpType != null)
                            employeeCode = "C0000".Substring(0, 5 - contractEmpType.EmployeeId.ToString().Length) + (contractEmpType.EmployeeId + 1).ToString();
                        else if (emptype == "Contractors" && contractEmpType == null)
                            employeeCode = "C0001";

                        if (emptype == "CSR Trainees" && traineeEmpType != null)
                            employeeCode = "T0000".Substring(0, 5 - traineeEmpType.EmployeeId.ToString().Length) + (traineeEmpType.EmployeeId + 1).ToString();
                        else if (emptype == "CSR Trainees" && traineeEmpType == null)
                            employeeCode = "T0001";
                    }
                }
            }
            catch
            {
                throw;
            }

            return employeeCode;
        }
        #endregion

        #region AddAssociatePersonalContacts
        /// <summary>
        /// Add Associate Personal Contacts
        /// </summary>
        /// <param name="personalDetails"></param>
        /// <param name="employeeId"></param>
        public void AddAssociatePersonalContacts(PersonalDetails personalDetails, int employeeId)
        {
            Contact contactOne = new Contact();
            Contact contactTwo = new Contact();

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    if (personalDetails.contacts.currentAddress1 != null || personalDetails.contacts.currentAddress2 != null
                                              || personalDetails.contacts.currentAddCity != null || personalDetails.contacts.currentAddCountry != null
                                              || personalDetails.contacts.currentAddState != null || personalDetails.contacts.currentAddZip != null)
                    {
                        contactOne.EmployeeId = employeeId;
                        contactOne.AddressType = "CurrentAddress";
                        contactOne.AddressLine1 = personalDetails.contacts.currentAddress1;
                        contactOne.AddressLine2 = personalDetails.contacts.currentAddress2;
                        contactOne.City = personalDetails.contacts.currentAddCity;
                        contactOne.Country = personalDetails.contacts.currentAddCountry;
                        contactOne.State = personalDetails.contacts.currentAddState;
                        contactOne.PostalCode = personalDetails.contacts.currentAddZip;
                        contactOne.CreatedUser = personalDetails.CurrentUser;
                        contactOne.CreatedDate = DateTime.Now;
                        contactOne.SystemInfo = personalDetails.SystemInfo;
                        hrmsEntities.Contacts.Add(contactOne);
                        hrmsEntities.SaveChanges();
                    }
                    if (personalDetails.contacts.permanentAddress1 != null || personalDetails.contacts.permanentAddress2 != null
                        || personalDetails.contacts.permanentAddCity != null || personalDetails.contacts.permanentAddCountry != null
                        || personalDetails.contacts.permanentAddState != null || personalDetails.contacts.permanentAddZip != null)
                    {
                        contactTwo.EmployeeId = employeeId;
                        contactTwo.AddressType = "PermanentAddress";
                        contactTwo.AddressLine1 = personalDetails.contacts.permanentAddress1;
                        contactTwo.AddressLine2 = personalDetails.contacts.permanentAddress2;
                        contactTwo.City = personalDetails.contacts.permanentAddCity;
                        contactTwo.Country = personalDetails.contacts.permanentAddCountry;
                        contactTwo.State = personalDetails.contacts.permanentAddState;
                        contactTwo.PostalCode = personalDetails.contacts.permanentAddZip;
                        contactTwo.SystemInfo = personalDetails.SystemInfo;
                        contactTwo.CreatedDate = DateTime.Now;
                        contactTwo.CreatedUser = personalDetails.CurrentUser;
                        hrmsEntities.Contacts.Add(contactTwo);
                        hrmsEntities.SaveChanges();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAssociateDetailsForNotification
        /// <summary>
        /// GetAssociateDetailsForNotification
        /// </summary>
        /// <param name="associateId"></param>
        /// <returns></returns>
        public UserDetails GetAssociateDetailsForNotification(string associateId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                var getAssociateDetails = (from e in hrmsEntities.Employees
                                           join dep in hrmsEntities.Departments on e.DepartmentId equals dep.DepartmentId into department
                                           from dept in department.DefaultIfEmpty()
                                           join desg in hrmsEntities.Designations on e.Designation equals desg.DesignationId into designation
                                           from desgn in designation.DefaultIfEmpty()
                                           where e.EmployeeCode == associateId
                                           select new UserDetails
                                           {

                                               name = e.FirstName + " " + e.LastName,
                                               mobileNo = e.MobileNo,
                                               department = dept.Description,
                                               designation = desgn.DesignationName

                                           }).FirstOrDefault();

                return getAssociateDetails;
            }
        }
        #endregion

        #region Validate Personal Details
        /// <summary>
        /// ValidatePersonalData
        /// </summary>
        /// <param name="personalDetails"></param>
        /// <returns></returns>
        public List<GenericType> ValidatePersonalData(PersonalDetails personalDetails)
        {
            List<GenericType> lstEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmployees = apEntities.Database.SqlQuery<GenericType>
                              ("[usp_ValidatePersonalData] @EmployeeId,@PersonalEmailAddress,@MobileNo,@AadharNumber,@PANNumber,@PFNumber,@UANNumber,@PassportNumber",

                               new object[] {
                                        new SqlParameter ("EmployeeId", personalDetails.empID),
                                        new SqlParameter ("PersonalEmailAddress", personalDetails.personalEmail),
                                        new SqlParameter ("MobileNo", personalDetails.EncryptedMobileNo),
                                        new SqlParameter ("AadharNumber", (object) personalDetails.EncryptedAadharNumber ?? DBNull.Value),
                                        new SqlParameter ("PANNumber", (object)personalDetails.EncryptedPanNumber ?? DBNull.Value),
                                        new SqlParameter ("PFNumber", (object)personalDetails.EncryptedPFNumber ?? DBNull.Value),
                                        new SqlParameter ("UANNumber", (object)personalDetails.EncryptedUANNumber ?? DBNull.Value),
                                        new SqlParameter ("PassportNumber", (object)personalDetails.EncryptedPassportNumber ?? DBNull.Value)
                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to validate employee details");
            }
            return lstEmployees;
        }
        #endregion

        #region AddPersonalDetails
        /// <summary>
        /// Method to Add Personal Details
        /// </summary>
        /// <param name="personalDetails"></param>
        /// <returns></returns>
        public int AddPersonalDetails(PersonalDetails personalDetails)
        {
            APEntities hrmsEntities = null;
            Employee emp = null;
            try
            {
                hrmsEntities = new APEntities();
                emp = new Employee();





                var empInfo = (from pra in hrmsEntities.ProspectiveAssociates
                               where (pra.ID == personalDetails.ID)
                               select new
                               {
                                   pra.EmployeeID,
                                   pra.EmploymentType,
                                   pra.HRAdvisorName,
                                   pra.DepartmentId,
                                   pra.Designation.DesignationCode,
                                   pra.GradeId,
                                   pra.JoinDate,
                                   pra.TechnologyID
                               }).FirstOrDefault();

                if (empInfo != null && empInfo.EmployeeID == null)
                {
                    //try
                    //{
                    parms = stringBuilder.AppendFormat("[PersonalDetails:{0}]", personalDetails).ToString();

                    string hrAdvisor = empInfo != null ? empInfo.HRAdvisorName : null;
                    string emptype = empInfo != null ? empInfo.EmploymentType : null;
                    EmployeeType employeeType = hrmsEntities.EmployeeTypes.Where(i => i.EmployeeTypeCode == emptype).FirstOrDefault();

                    Employee employeeCode = hrmsEntities.Employees.Where(code => code.EmployeeCode == personalDetails.empCode).FirstOrDefault();
                    if (employeeCode == null)
                    {
                        emp.DateofBirth = Commons.GetDateTimeInIST(personalDetails.dob);
                        emp.FirstName = personalDetails.firstName;
                        emp.MiddleName = personalDetails.middleName;
                        emp.LastName = personalDetails.lastName;
                        emp.TelephoneNo = personalDetails.EncryptedPhoneNumber;
                        emp.MobileNo = personalDetails.EncryptedMobileNo;
                        emp.PersonalEmailAddress = personalDetails.personalEmail;
                        emp.Gender = personalDetails.gender;
                        emp.MaritalStatus = personalDetails.maritalStatus;
                        emp.BloogGroup = personalDetails.bloodGroup;
                        emp.Nationality = personalDetails.nationality;
                        emp.PANNumber = personalDetails.EncryptedPanNumber;
                        emp.BGVStatusId = personalDetails.bgvStatusID;
                        emp.BGVStatus = personalDetails.bgvStatus;
                        emp.ReportingManager = personalDetails.ReportingManagerId;
                        emp.AadharNumber = personalDetails.EncryptedAadharNumber;
                        emp.PFNumber = personalDetails.EncryptedPFNumber;
                        emp.UANNumber = personalDetails.EncryptedUANNumber;
                        emp.BGVInitiatedDate = personalDetails.bgvStartDate;
                        emp.BGVCompletionDate = personalDetails.bgvCompletedDate;
                        emp.PassportNumber = personalDetails.EncryptedPassportNumber;
                        emp.PassportIssuingOffice = personalDetails.passportIssuingOffice;
                        emp.PassportDateValidUpto = personalDetails.passportValidDate;
                        //emp.EmployeeCode = GenerateEmployeeCode(employeeType.EmployeeTypeCode);
                        emp.EmployeeCode = personalDetails.empCode;
                        emp.HRAdvisor = hrAdvisor;
                        emp.EmployeeType = employeeType;
                        emp.DepartmentId = personalDetails.deptID == null ? empInfo.DepartmentId : personalDetails.deptID;
                        emp.Designation = personalDetails.designationID == null ? empInfo.DepartmentId : personalDetails.designationID;
                        emp.GradeId = personalDetails.gradeID == null ? empInfo.GradeId : personalDetails.gradeID;
                        emp.JoinDate = personalDetails.joiningDate == null ? empInfo.JoinDate : personalDetails.joiningDate;
                        emp.CompetencyGroup = personalDetails.technologyID == null ? empInfo.TechnologyID : personalDetails.technologyID; //Here TechnologyID refers to PracticeAreaId
                        emp.IsActive = true;
                        emp.CreatedDate = DateTime.Now;
                        emp.CreatedUser = personalDetails.CurrentUser;
                        emp.SystemInfo = personalDetails.SystemInfo;
                        emp.CareerBreak = (personalDetails.CareerBreak) * 30;
                        emp.EmploymentStartDate = Commons.GetDateTimeInIST(personalDetails.EmplStartDate);
                        emp.JoinDate = Commons.GetDateTimeInIST(personalDetails.doj);
                        hrmsEntities.Employees.Add(emp);
                        int rowInserted = hrmsEntities.SaveChanges();

                        personalDetails.empID = emp.EmployeeId;



                        if (rowInserted != 0)
                        {
                            ProspectiveAssociate pa = (from pra in hrmsEntities.ProspectiveAssociates
                                                       where (pra.ID == personalDetails.ID)
                                                       select pra).FirstOrDefault();

                            pa.IsActive = false;
                            hrmsEntities.Entry(pa).State = EntityState.Modified;
                            hrmsEntities.SaveChanges();

                            if (personalDetails.contacts != null)
                            {
                                AddAssociatePersonalContacts(personalDetails, emp.EmployeeId);
                            }
                        }
                        new Common().CreateHRMSRepository(personalDetails.empCode);
                        AddKRARole(personalDetails.empCode, personalDetails.empID, personalDetails.KRARoleId); //Adding KRA Role to Associate
                    }
                    else
                    {
                        throw new AssociatePortalException("Employee code already exists.");

                    }
                    //Log.Write("Adding New Personal details", Log.Severity.Warning, parms);
                }

                return emp.EmployeeId;
            }
            catch
            {
                throw;
            }
            finally
            {
                hrmsEntities = null;
                emp = null;
            }
        }


        private int AddKRARole(string empCode, int empID, int? KRARoleId)
        {
            int rowsAffected = 0;
            try
            {

                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = apEntities.Database.SqlQuery<int>
                               ("[usp_AddKRARole]  @EmployeeCode, @EmployeeId, @KRARoleId",
                                   new object[] {
                                        new SqlParameter ("EmployeeCode", empCode),
                                        new SqlParameter ("EmployeeId", empID),
                                        new SqlParameter ("KRARoleId", KRARoleId)

                                   }
                                   ).SingleOrDefault();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return rowsAffected;
        }


        #endregion

        #region UpdatePersonalDetails
        /// <summary>
        /// Method to update personal details.
        /// </summary>
        /// <param name="personalDetails"></param>
        /// <returns></returns>
        public bool UpdatePersonalDetails(PersonalDetails personalDetails)
        {
            bool retValue = false;

            try
            {
                parms = stringBuilder.AppendFormat("[PersonalDetails:{0}]", personalDetails).ToString();

                using (APEntities hrmsEntities = new APEntities())
                {
                    EmployeeType employeeType = hrmsEntities.EmployeeTypes.Where(i => i.EmployeeTypeCode == personalDetails.employmentType).FirstOrDefault();
                    Employee emp = hrmsEntities.Employees.First(x => x.EmployeeId == personalDetails.empID);

                    emp.DateofBirth = Commons.GetDateTimeInIST(personalDetails.dob);
                    emp.FirstName = personalDetails.firstName;
                    emp.MiddleName = personalDetails.middleName;
                    emp.LastName = personalDetails.lastName;
                    emp.TelephoneNo = personalDetails.EncryptedPhoneNumber;
                    emp.MobileNo = personalDetails.EncryptedMobileNo;
                    emp.PersonalEmailAddress = personalDetails.personalEmail;
                    emp.Gender = personalDetails.gender;
                    emp.MaritalStatus = personalDetails.maritalStatus;
                    emp.BloogGroup = personalDetails.bloodGroup;
                    emp.Nationality = personalDetails.nationality;
                    emp.PANNumber = personalDetails.EncryptedPanNumber;
                    emp.AadharNumber = personalDetails.EncryptedAadharNumber;
                    emp.PFNumber = personalDetails.EncryptedPFNumber;
                    emp.UANNumber = personalDetails.EncryptedUANNumber;
                    emp.BGVInitiatedDate = personalDetails.bgvStartDate;
                    emp.BGVCompletionDate = personalDetails.bgvCompletedDate;
                    emp.PassportNumber = personalDetails.EncryptedPassportNumber;
                    emp.PassportDateValidUpto = personalDetails.passportValidDate;
                    emp.PassportIssuingOffice = personalDetails.passportIssuingOffice;
                    emp.BGVStatusId = personalDetails.bgvStatusID;
                    emp.BGVStatus = personalDetails.bgvStatus;
                    emp.GradeId = personalDetails.gradeID;
                    emp.WorkEmailAddress = personalDetails.workEmailID;
                    emp.ModifiedDate = DateTime.Now;
                    emp.ModifiedUser = personalDetails.CurrentUser;
                    emp.SystemInfo = personalDetails.SystemInfo;
                    emp.JoinDate = Commons.GetDateTimeInIST(personalDetails.doj);
                    emp.CareerBreak = (personalDetails.CareerBreak) * 30;
                    emp.EmploymentStartDate = Commons.GetDateTimeInIST(personalDetails.EmplStartDate);
                    emp.ReportingManager = personalDetails.ReportingManagerId;
                    emp.DepartmentId = personalDetails.deptID;
                    emp.Designation = personalDetails.designationID;
                    emp.HRAdvisor = personalDetails.hrAdvisor;
                    emp.CompetencyGroup = personalDetails.technologyID;
                    emp.EmployeeType = employeeType;
                    hrmsEntities.Entry(emp).State = EntityState.Modified;
                    retValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                    Contact contact = hrmsEntities.Contacts.FirstOrDefault(x => x.EmployeeId == personalDetails.empID && (x.AddressType == "CurrentAddress" || x.AddressType == "Current"));
                    if (contact != null)
                    {
                        if (personalDetails.contacts != null)
                        {
                            if (personalDetails.contacts.currentAddress1 != null || personalDetails.contacts.currentAddress2 != null
                                || personalDetails.contacts.currentAddCity != null || personalDetails.contacts.currentAddCountry != null
                                || personalDetails.contacts.currentAddState != null || personalDetails.contacts.currentAddZip != null)
                            {
                                contact.AddressLine1 = personalDetails.contacts.currentAddress1;
                                contact.AddressLine2 = personalDetails.contacts.currentAddress2;
                                contact.City = personalDetails.contacts.currentAddCity;
                                contact.Country = personalDetails.contacts.currentAddCountry;
                                contact.State = personalDetails.contacts.currentAddState;
                                contact.PostalCode = personalDetails.contacts.currentAddZip;
                                contact.ModifiedUser = personalDetails.CurrentUser;
                                contact.IsActive = true;
                                contact.ModifiedDate = DateTime.Now;
                                contact.SystemInfo = personalDetails.SystemInfo;
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                        }
                        else if (personalDetails.contacts != null)
                        {
                            contact = new Contact();
                            contact.EmployeeId = emp.EmployeeId;
                            contact.AddressType = "CurrentAddress";
                            contact.AddressLine1 = personalDetails.contacts.currentAddress1;
                            contact.AddressLine2 = personalDetails.contacts.currentAddress2;
                            contact.City = personalDetails.contacts.currentAddCity;
                            contact.Country = personalDetails.contacts.currentAddCountry;
                            contact.State = personalDetails.contacts.currentAddState;
                            contact.PostalCode = personalDetails.contacts.currentAddZip;
                            contact.IsActive = true;
                            contact.CreatedUser = personalDetails.CurrentUser;
                            contact.CreatedDate = DateTime.Now;
                            contact.SystemInfo = personalDetails.SystemInfo;
                            hrmsEntities.Contacts.Add(contact);
                            retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                    }
                    else if (personalDetails.contacts != null)
                    {
                        contact = new Contact();
                        contact.EmployeeId = emp.EmployeeId;
                        contact.AddressType = "CurrentAddress";
                        contact.AddressLine1 = personalDetails.contacts.currentAddress1;
                        contact.AddressLine2 = personalDetails.contacts.currentAddress2;
                        contact.City = personalDetails.contacts.currentAddCity;
                        contact.Country = personalDetails.contacts.currentAddCountry;
                        contact.State = personalDetails.contacts.currentAddState;
                        contact.PostalCode = personalDetails.contacts.currentAddZip;
                        contact.IsActive = true;
                        contact.CreatedUser = personalDetails.CurrentUser;
                        contact.CreatedDate = DateTime.Now;
                        contact.SystemInfo = personalDetails.SystemInfo;
                        hrmsEntities.Contacts.Add(contact);
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }

                    Contact contactOne = hrmsEntities.Contacts.FirstOrDefault(x => x.EmployeeId == personalDetails.empID && (x.AddressType == "PermanentAddress" || x.AddressType == "Permanent"));
                    if (contactOne != null)
                    {
                        if (personalDetails.contacts != null)
                        {
                            if (personalDetails.contacts.permanentAddress1 != null || personalDetails.contacts.permanentAddress2 != null
                                || personalDetails.contacts.permanentAddCity != null || personalDetails.contacts.permanentAddCountry != null
                                || personalDetails.contacts.permanentAddState != null || personalDetails.contacts.permanentAddZip != null)
                            {
                                contactOne.AddressLine1 = personalDetails.contacts.permanentAddress1;
                                contactOne.AddressLine2 = personalDetails.contacts.permanentAddress2;
                                contactOne.City = personalDetails.contacts.permanentAddCity;
                                contactOne.Country = personalDetails.contacts.permanentAddCountry;
                                contactOne.State = personalDetails.contacts.permanentAddState;
                                contactOne.IsActive = true;
                                contactOne.PostalCode = personalDetails.contacts.permanentAddZip;
                                contactOne.ModifiedUser = personalDetails.CurrentUser;
                                contactOne.ModifiedDate = DateTime.Now;
                                contactOne.SystemInfo = personalDetails.SystemInfo;
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                        }
                        else if (personalDetails.contacts != null)
                        {
                            contactOne = new Contact();
                            contactOne.EmployeeId = emp.EmployeeId;
                            contactOne.AddressType = "PermanentAddress";
                            contactOne.AddressLine1 = personalDetails.contacts.permanentAddress1;
                            contactOne.AddressLine2 = personalDetails.contacts.permanentAddress2;
                            contactOne.City = personalDetails.contacts.permanentAddCity;
                            contactOne.Country = personalDetails.contacts.permanentAddCountry;
                            contactOne.State = personalDetails.contacts.permanentAddState;
                            contactOne.PostalCode = personalDetails.contacts.permanentAddZip;
                            contactOne.SystemInfo = personalDetails.SystemInfo;
                            contactOne.CreatedDate = DateTime.Now;
                            contactOne.IsActive = true;
                            contactOne.CreatedUser = personalDetails.CurrentUser;
                            hrmsEntities.Contacts.Add(contactOne);
                            retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                    }
                    else if (personalDetails.contacts != null)
                    {
                        contactOne = new Contact();
                        contactOne.EmployeeId = emp.EmployeeId;
                        contactOne.AddressType = "PermanentAddress";
                        contactOne.AddressLine1 = personalDetails.contacts.permanentAddress1;
                        contactOne.AddressLine2 = personalDetails.contacts.permanentAddress2;
                        contactOne.City = personalDetails.contacts.permanentAddCity;
                        contactOne.Country = personalDetails.contacts.permanentAddCountry;
                        contactOne.State = personalDetails.contacts.permanentAddState;
                        contactOne.PostalCode = personalDetails.contacts.permanentAddZip;
                        contactOne.SystemInfo = personalDetails.SystemInfo;
                        contactOne.CreatedDate = DateTime.Now;
                        contactOne.IsActive = true;
                        contactOne.CreatedUser = personalDetails.CurrentUser;
                        hrmsEntities.Contacts.Add(contactOne);
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                }
                //Log.Write("Updating existing Personal details", Log.Severity.Warning, parms);
                UpdateKRARole(personalDetails.empID, personalDetails.KRARoleId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }

            return retValue;
        }

        private int UpdateKRARole(int employeeId, int? KRARoleId)
        {
            int rowsAffected = 0;
            try
            {

                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {

                        rowsAffected = apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateKRARole] @EmployeeId, @KRARoleId",
                                   new object[] {

                                        new SqlParameter ("EmployeeId", employeeId),
                                        new SqlParameter ("KRARoleId", KRARoleId)

                                   }
                                   ).SingleOrDefault();
                        trans.Commit();
                    }
                }
            }
            catch
            {

            }
            return rowsAffected;
        }

        #endregion

        #region GetPersonalDetailsByID
        /// <summary>
        /// Method to Get PersonalDetails By ID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public PersonalDetails GetPersonalDetailsByID(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var dob = (from e in hrmsEntities.Employees
                               where e.EmployeeId == empID
                               select new
                               {
                                   e.DateofBirth,
                                   e.JoinDate
                               }).FirstOrDefault();

                    string birth = dob.DateofBirth != null ? Convert.ToDateTime(dob.DateofBirth).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : null;
                    //string joindate = dob.JoinDate != null ? Convert.ToDateTime(dob.JoinDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : null;

                    parms = stringBuilder.AppendFormat("[EmployeeId:{0}]", empID).ToString();
                    var getPersonalDetails = (from e in hrmsEntities.Employees
                                              join d in hrmsEntities.Departments on e.DepartmentId equals d.DepartmentId into g1
                                              from dep in g1.DefaultIfEmpty()
                                              join emptype in hrmsEntities.EmployeeTypes on e.EmployeeTypeId equals emptype.EmployeeTypeId into g2
                                              from emptypes in g2.DefaultIfEmpty()
                                              join des in hrmsEntities.Designations on e.Designation equals des.DesignationId into g3
                                              from designation in g3.DefaultIfEmpty()
                                              join grade in hrmsEntities.Grades on e.GradeId equals grade.GradeId
                                              where e.EmployeeId == empID
                                              select new PersonalDetails
                                              {
                                                  empID = e.EmployeeId,
                                                  empCode = e.EmployeeCode,
                                                  empName = e.FirstName + " " + e.LastName,
                                                  firstName = e.FirstName,
                                                  lastName = e.LastName,
                                                  middleName = e.MiddleName,
                                                  EncryptedPhoneNumber = e.TelephoneNo,
                                                  EncryptedMobileNo = e.MobileNo,
                                                  bgvStatusID = e.BGVStatusId,
                                                  bgvStatus = e.BGVStatus,
                                                  workEmailID = e.WorkEmailAddress,
                                                  personalEmail = e.PersonalEmailAddress,
                                                  dob = e.DateofBirth,
                                                  Birthdate = birth,
                                                  gender = e.Gender,
                                                  doj = e.JoinDate,
                                                  maritalStatus = e.MaritalStatus,
                                                  bloodGroup = e.BloogGroup,
                                                  nationality = e.Nationality,
                                                  EncryptedPanNumber = e.PANNumber,
                                                  EncryptedAadharNumber = e.AadharNumber,
                                                  EncryptedPFNumber = e.PFNumber,
                                                  EncryptedPassportNumber = e.PassportNumber,
                                                  EncryptedUANNumber = e.UANNumber,
                                                  passportValidDate = e.PassportDateValidUpto,
                                                  passportIssuingOffice = e.PassportIssuingOffice,
                                                  bgvStartDate = e.BGVInitiatedDate,
                                                  bgvCompletedDate = e.BGVCompletionDate,
                                                  Experience = e.Experience.ToString(),
                                                  EmploymentStartDate = e.EmploymentStartDate,
                                                  EmplStartDate = e.EmploymentStartDate,
                                                  CareerBreak = (e.CareerBreak) / 30,
                                                  joiningDate = dob.JoinDate,
                                                  hrAdvisor = e.HRAdvisor,
                                                  designation = designation.DesignationName,
                                                  designationID = designation.DesignationId,
                                                  deptID = e.DepartmentId,
                                                  technologyID = e.CompetencyGroup,
                                                  department = dep.DepartmentCode,
                                                  gradeID = e.GradeId,
                                                  GradeName = grade.GradeName,
                                                  employmentType = emptypes.EmployeeType1,
                                                  ReportingManagerId = e.ReportingManager == null ? 0 : e.ReportingManager.Value,
                                                  contactDetails = (from c in hrmsEntities.Contacts
                                                                    where c.EmployeeId == empID && c.AddressType == "CurrentAddress"
                                                                    select new ContactDetails
                                                                    {
                                                                        ID = c.ID,
                                                                        currentAddress1 = c.AddressLine1,
                                                                        currentAddress2 = c.AddressLine2,
                                                                        currentAddCity = c.City,
                                                                        currentAddCountry = c.Country,
                                                                        currentAddState = c.State,
                                                                        currentAddZip = c.PostalCode,
                                                                        addressType = c.AddressType,
                                                                        permanentAddress1 = "",
                                                                        permanentAddress2 = "",
                                                                        permanentAddCity = "",
                                                                        permanentAddCountry = "",
                                                                        permanentAddState = "",
                                                                        permanentAddZip = "",
                                                                    }).Union(from c in hrmsEntities.Contacts
                                                                             where c.EmployeeId == empID && c.AddressType == "PermanentAddress"
                                                                             select new ContactDetails
                                                                             {
                                                                                 ID = c.ID,
                                                                                 currentAddress1 = "",
                                                                                 currentAddress2 = "",
                                                                                 currentAddCity = "",
                                                                                 currentAddCountry = "",
                                                                                 currentAddState = "",
                                                                                 currentAddZip = "",
                                                                                 addressType = c.AddressType,
                                                                                 permanentAddress1 = c.AddressLine1,
                                                                                 permanentAddress2 = c.AddressLine2,
                                                                                 permanentAddCity = c.City,
                                                                                 permanentAddCountry = c.Country,
                                                                                 permanentAddState = c.State,
                                                                                 permanentAddZip = c.PostalCode,
                                                                             })

                                              }).FirstOrDefault();

                    //if (getPersonalDetails == null)
                    //    Log.Write("Getting Personal details by EmployeeId", Log.Severity.Warning, parms);

                    getPersonalDetails.KRARoleId = GetKRARole(empID);

                    return getPersonalDetails;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
        }


        private int GetKRARole(int empID)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var kraRole = hrmsEntity.Database.SqlQuery<int>
                            ("[usp_GetKRARoleByEmployeeID] @EmployeeId",
                                new object[] {
                                        new SqlParameter ("EmployeeId", empID)
                                }
                                ).SingleOrDefault();
                    return kraRole;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get KRA Role");
            }
        }

        #endregion

        #region Get AssociateDetails By Search
        /// <summary>
        /// Get AssociateDetails By Search
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssociateDetailsBySearch(SearchFilter searchFilter)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                int statusID = hrmsEntities.Status.Where(e => "Approved".Equals(e.StatusCode)).Select(e => e.StatusId).FirstOrDefault();

                var result = (from e in hrmsEntities.Employees
                              join dep in hrmsEntities.Departments on e.DepartmentId equals dep.DepartmentId into employee
                              from emp in employee.DefaultIfEmpty()
                              where e.IsActive == true && e.StatusId == statusID
                              select new AssociateDetails()
                              {
                                  empID = e.EmployeeId,
                                  empCode = e.EmployeeCode,
                                  empName = e.FirstName + " " + e.LastName,
                                  bgvStatus = e.BGVStatus,
                                  EncryptedMobileNumber = e.MobileNo == null ? "" : e.MobileNo,
                                  WorkEmail = e.WorkEmailAddress,
                                  PersonalEmail = e.PersonalEmailAddress,
                                  Department = emp.Description
                              }).OrderByDescending(i => i.empID).ToList();

                if (searchFilter.SearchData == null)
                    searchFilter.SearchData = "";

                if (searchFilter != null && searchFilter.SearchType == "name")
                    result = result.Where(i => i.empName.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

                if (searchFilter != null && searchFilter.SearchType == "empCode")
                    result = result.Where(i => i.empCode.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

                if (searchFilter != null && searchFilter.SearchType == "mob")
                    result = result.Where(i => i.MobileNo.IndexOf(searchFilter.SearchData, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

                return result;
            }
        }
        #endregion

        #region UpdateAssociateStatus
        /// <summary>
        /// UpdateAssociateStatus
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public bool UpdateAssociateStatus(int employeeId)
        {
            bool returnValue = false;

            try
            {
                string pending = resourceManager.GetString("StatusPending");
                using (APEntities hrmEntities = new APEntities())
                {
                    int statusId = hrmEntities.Status.AsNoTracking().Where(s => s.StatusCode.ToLower().Equals(pending.ToLower())).FirstOrDefault().StatusId;
                    Employee employee = hrmEntities.Employees.Where(p => p.EmployeeId == employeeId).FirstOrDefault();

                    if (employee != null && statusId > 0)
                    {
                        employee.StatusId = statusId;
                        employee.SystemInfo = Commons.GetClientIPAddress();
                        employee.ModifiedUser = HttpContext.Current.User.Identity.Name;
                        employee.ModifiedDate = DateTime.Now;
                        returnValue = hrmEntities.SaveChanges() > 0 ? true : false;
                    }
                }

                //Send notification email to HRM for approval
                using (var hrmEntities = new APEntities())
                {
                    EmailNotificationConfiguration emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                              ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.NotificationType.EPC)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.EPC)),
                               }
                              ).FirstOrDefault();

                    if (emailNotificationConfig != null)
                    {
                        AssociateDetails associateDetails = hrmEntities.Database.SqlQuery<AssociateDetails>
                                     ("[usp_GetAssociatePendingApproval] @EmployeeID",
                                       new object[]  {
                                    new SqlParameter ("EmployeeID", employeeId),
                                      }
                                     ).FirstOrDefault();

                        if (associateDetails != null)
                        {
                            NotificationDetail notificationDetail = new NotificationDetail();
                            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

                            emailContent = emailContent.Replace("{AssociateName}", associateDetails.empName)
                                                       .Replace("{Designation}", associateDetails.Designation)
                                                       .Replace("{Department}", associateDetails.Department)
                                                       .Replace("{ReportingManager}", associateDetails.ReportingManager)
                                                       .Replace("{Technology}", associateDetails.Technology);

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                                throw new AssociatePortalException("Email From cannot be blank");
                            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                                throw new AssociatePortalException("Email To cannot be blank");
                            notificationDetail.ToEmail = emailNotificationConfig.EmailTo;

                            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                            notificationDetail.Subject = emailNotificationConfig.EmailSubject + " " + associateDetails.empCode;
                            notificationDetail.EmailBody = emailContent.ToString();
                            NotificationManager.SendEmail(notificationDetail);
                            returnValue = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Error while updating assoicate status.");
            }

            return returnValue;
        }
        #endregion

        #region Get AssociateDetails
        /// <summary>
        /// Get AssociateDetails 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssociateDetails> GetAssociateDetailsByEmpID(int empID, string roleName)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                var result = (IEnumerable<AssociateDetails>)null;

                if (roleName == Enumeration.ApplicationRoles.Lead.ToString())
                {
                    result = (from aa in hrmsEntities.AssociateAllocations
                              join p in hrmsEntities.Projects on aa.ProjectId equals p.ProjectId
                              join projectmgr in hrmsEntities.ProjectManagers on p.ProjectId equals projectmgr.ProjectID
                              join e in hrmsEntities.Employees on aa.EmployeeId equals e.EmployeeId
                              join u in hrmsEntities.Users on e.UserId equals u.UserId into g1
                              from usr in g1.DefaultIfEmpty()
                              join e1 in hrmsEntities.Employees on projectmgr.ReportingManagerID equals e1.EmployeeId into g2
                              from emp in g2.DefaultIfEmpty()
                              join e2 in hrmsEntities.Employees on projectmgr.ProgramManagerID equals e2.EmployeeId into g3
                              from employee in g3.DefaultIfEmpty()
                              where aa.ReportingManagerId == empID && aa.IsActive == true && p.IsActive == true && e.IsActive == true
                              select new AssociateDetails
                              {
                                  empID = aa.EmployeeId.Value,
                                  empCode = e.EmployeeCode,
                                  empName = e.FirstName + " " + e.LastName,
                                  bgvStatus = e.BGVStatus,
                                  EncryptedMobileNumber = e.MobileNo == null ? "" : e.MobileNo,
                                  ProjectId = p.ProjectId,
                                  ProjectName = p.ProjectName,
                                  LeadId = projectmgr.ReportingManagerID,
                                  ManagerId = projectmgr.ProgramManagerID,
                                  ManagerFirstName = employee.FirstName,
                                  ManagerLastName = employee.LastName,
                                  LeadFirstName = emp.FirstName,
                                  LeadLastName = emp.LastName,
                                  EmailAddress = usr.EmailAddress
                              }).OrderByDescending(i => i.empID).ToList();

                    result.Select(e => { e.ManagerName = e.ManagerFirstName + " " + e.ManagerLastName; e.LeadName = e.LeadFirstName + " " + e.LeadLastName; return e; }).ToList();
                }
                else if (roleName == Enumeration.ApplicationRoles.Manager.ToString())
                {
                    result = (from aa in hrmsEntities.AssociateAllocations
                              join p in hrmsEntities.Projects on aa.ProjectId equals p.ProjectId
                              join projectmgr in hrmsEntities.ProjectManagers on p.ProjectId equals projectmgr.ProjectID
                              join e in hrmsEntities.Employees on aa.EmployeeId equals e.EmployeeId
                              join u in hrmsEntities.Users on e.UserId equals u.UserId into g1
                              from usr in g1.DefaultIfEmpty()
                              join e1 in hrmsEntities.Employees on projectmgr.ReportingManagerID equals e1.EmployeeId into g2
                              from emp in g2.DefaultIfEmpty()
                              join e2 in hrmsEntities.Employees on projectmgr.ProgramManagerID equals e2.EmployeeId into g3
                              from employee in g3.DefaultIfEmpty()
                              where projectmgr.ReportingManagerID == empID && aa.IsActive == true && p.IsActive == true && e.IsActive == true
                              select new AssociateDetails
                              {
                                  empID = aa.EmployeeId.Value,
                                  empCode = e.EmployeeCode,
                                  empName = e.FirstName + " " + e.LastName,
                                  bgvStatus = e.BGVStatus,
                                  EncryptedMobileNumber = e.MobileNo == null ? "" : e.MobileNo,
                                  ProjectId = p.ProjectId,
                                  ProjectName = p.ProjectName,
                                  LeadId = projectmgr.ReportingManagerID,
                                  ManagerId = projectmgr.ProgramManagerID,
                                  ManagerFirstName = employee.FirstName,
                                  ManagerLastName = employee.LastName,
                                  LeadFirstName = emp.FirstName,
                                  LeadLastName = emp.LastName,
                                  EmailAddress = usr.EmailAddress
                              }).OrderByDescending(i => i.empID).ToList();

                    result.Select(e => { e.ManagerName = e.ManagerFirstName + " " + e.ManagerLastName; e.LeadName = e.LeadFirstName + " " + e.LeadLastName; return e; }).ToList();
                }
                else if (roleName == Enumeration.DepartmentHead)
                {
                    result = (from aa in hrmsEntities.AssociateAllocations
                              join p in hrmsEntities.Projects on aa.ProjectId equals p.ProjectId
                              join projectmgr in hrmsEntities.ProjectManagers on p.ProjectId equals projectmgr.ProjectID
                              join e in hrmsEntities.Employees on aa.EmployeeId equals e.EmployeeId
                              join u in hrmsEntities.Users on e.UserId equals u.UserId into g1
                              from usr in g1.DefaultIfEmpty()
                              join e1 in hrmsEntities.Employees on projectmgr.ReportingManagerID equals e1.EmployeeId into g2
                              from emp in g2.DefaultIfEmpty()
                              join e2 in hrmsEntities.Employees on projectmgr.ProgramManagerID equals e2.EmployeeId into g3
                              from employee in g3.DefaultIfEmpty()
                              where aa.IsActive == true && p.IsActive == true && e.IsActive == true
                              select new AssociateDetails
                              {
                                  empID = aa.EmployeeId.Value,
                                  empCode = e.EmployeeCode,
                                  empName = e.FirstName + " " + e.LastName,
                                  bgvStatus = e.BGVStatus,
                                  EncryptedMobileNumber = e.MobileNo == null ? "" : e.MobileNo,
                                  ProjectId = p.ProjectId,
                                  ProjectName = p.ProjectName,
                                  LeadId = projectmgr.ReportingManagerID,
                                  ManagerId = projectmgr.ProgramManagerID,
                                  ManagerFirstName = employee.FirstName,
                                  ManagerLastName = employee.LastName,
                                  LeadFirstName = emp.FirstName,
                                  LeadLastName = emp.LastName,
                                  EmailAddress = usr.EmailAddress
                              }).OrderByDescending(i => i.empID).ToList();

                    result.Select(e => { e.ManagerName = e.ManagerFirstName + " " + e.ManagerLastName; e.LeadName = e.LeadFirstName + " " + e.LeadLastName; return e; }).ToList();

                }
                return result;
            }
        }
        #endregion

        #region UpdateAssociateStatus
        /// <summary>
        /// UpdateAssociateStatus
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public bool UpdateAssociateStatus(int employeeId, int statusId, string reason = null)
        {
            bool returnValue = false;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    Employee employee = hrmEntities.Employees.Where(p => p.EmployeeId == employeeId).FirstOrDefault();

                    if (employee != null && statusId > 0)
                    {
                        employee.Remarks = reason;
                        employee.StatusId = statusId;
                        employee.SystemInfo = Commons.GetClientIPAddress();
                        employee.ModifiedUser = HttpContext.Current.User.Identity.Name;
                        employee.ModifiedDate = DateTime.Now;
                        returnValue = hrmEntities.SaveChanges() > 0 ? true : false;
                    }

                    EmailNotificationConfiguration emailNotificationConfig = new EmailNotificationConfiguration();
                    AssociateDetails associateDetails = new AssociateDetails();

                    if (reason == null)
                    {
                        //Send notification email to HRM for approval
                        emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                                    ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                      new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.EPCNotificationStatus.Approved)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.EPC)),
                                     }
                                    ).FirstOrDefault();

                        associateDetails = hrmEntities.Database.SqlQuery<AssociateDetails>
                                     ("[usp_GetAssociateApproval] @EmployeeID",
                                       new object[]  {
                                    new SqlParameter ("EmployeeID", employeeId),
                                      }
                                     ).FirstOrDefault();
                    }
                    else
                    {
                        //Send notification email to HRM for  Rejection
                        emailNotificationConfig = hrmEntities.Database.SqlQuery<EmailNotificationConfiguration>
                                    ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                      new object[]  {
                                    new SqlParameter ("NotificationTypeID", Convert.ToInt32(Enumeration.EPCNotificationStatus.Rejected)),
                                    new SqlParameter ("CategoryId", Convert.ToInt32(Enumeration.CategoryMaster.EPC)),
                                     }
                                    ).FirstOrDefault();

                        associateDetails = hrmEntities.Database.SqlQuery<AssociateDetails>
                                     ("[usp_GetAssociateReject] @EmployeeID",
                                       new object[]  {
                                    new SqlParameter ("EmployeeID", employeeId),
                                      }
                                     ).FirstOrDefault();
                    }
                    if (emailNotificationConfig != null)
                    {
                        if (associateDetails != null)
                        {
                            NotificationDetail notificationDetail = new NotificationDetail();
                            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(emailNotificationConfig.EmailContent));

                            emailContent = emailContent.Replace("{AssociateName}", associateDetails.empName)
                                                       .Replace("{Designation}", associateDetails.Designation)
                                                       .Replace("{Department}", associateDetails.Department);

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailFrom))
                                throw new AssociatePortalException("Email From cannot be blank");
                            notificationDetail.FromEmail = emailNotificationConfig.EmailFrom;

                            if (string.IsNullOrEmpty(emailNotificationConfig.EmailTo))
                                throw new AssociatePortalException("Email To cannot be blank");
                            notificationDetail.ToEmail = emailNotificationConfig.EmailTo;

                            notificationDetail.CcEmail = emailNotificationConfig.EmailCC;

                            notificationDetail.Subject = emailNotificationConfig.EmailSubject + " " + associateDetails.empCode;

                            notificationDetail.EmailBody = emailContent.ToString();
                            NotificationManager.SendEmail(notificationDetail);
                            returnValue = true;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return returnValue;
        }
        #endregion

        #endregion




    }
}
