﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Resources;
using AP.DomainEntities;
using AP.DataStorage;
using AP.Utility;
using Quartz;
using System.Web;

namespace AP.API
{
    public class ITNotificationJob : IJob
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region AddJob
        /// <summary>
        /// Add job to the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <param name="interval"></param>
        /// 
        public bool AddJob(string jobId, string groupId)
        {
            bool returnValue = false;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    string itNotificationCode = resourceManager.GetString("ITNotification");
                    //NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == itNotificationCode).FirstOrDefault();

                    //if (notificationConfiguration.SLA == 0) throw new AssociatePortalException("Admin Notification's SLA can not be zero.");

                   // returnValue = NotificationScheduler.AddJob<ITNotificationJob>(jobId, groupId, notificationConfiguration.SLA.GetValueOrDefault());

                    if (returnValue)
                    {
                        string statusStarted = resourceManager.GetString("Started");
                        var jobExist = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId && j.Status == statusStarted).FirstOrDefault();

                        // Insert record to the JobDetail table 
                        //
                        //if (jobExist == null)
                        //{
                        //    JobDetail jobDetail = new JobDetail() { JobCode = jobId, GroupID = groupId, Status = statusStarted, JobInterval = notificationConfiguration.SLA };
                        //    jobDetail = hrmEntities.JobDetails.Add(jobDetail);
                        //}

                        string itNotificationTypeId = resourceManager.GetString("ITDepartmentNotificationTypeID");
                        string statusPendingID = resourceManager.GetString("StatusPendingID");
                        var notType = Convert.ToInt32(itNotificationTypeId);
                        var pending = Convert.ToInt32(statusPendingID);
                        var notificationExist = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == notType && n.StatusId == pending).FirstOrDefault();

                        // Insert record in to Notification table  
                        //
                        if (notificationExist == null)
                        {
                            Notification notification = new Notification() { EmployeeCode = jobId, NotificationTypeID = Convert.ToInt32(itNotificationTypeId), StatusId = Convert.ToInt32(statusPendingID) };
                            notification = hrmEntities.Notifications.Add(notification);
                        }

                        //Insert the task details into ITTask table
                        //
                        IEnumerable itTasks = GetITTasks();

                        foreach (ITTaskMaster task in itTasks)
                        {
                            ITTask itTask = new ITTask() { EmployeeCode = jobId, TaskID = task.ITTaskID, StatusId = Convert.ToInt32(statusPendingID) };
                            itTask = hrmEntities.ITTasks.Add(itTask);
                        }

                        hrmEntities.SaveChanges();
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save details.");
            }

            return returnValue;
        }
        #endregion

        #region RemoveJob
        /// <summary>
        /// Remove job from the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// 
        public bool RemoveJob(string jobId, string groupId)
        {
            bool isDelete = false;

            try
            {
                isDelete = NotificationScheduler.RemoveJob(jobId, groupId);

                using (APEntities hrmEntities = new APEntities())
                {
                    ///  Delete Job from JobDetail table ///

                    JobDetail jobToBeDeleted = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId).FirstOrDefault();

                    if (jobToBeDeleted != null)
                    {
                        hrmEntities.JobDetails.Remove(jobToBeDeleted);
                    }

                    int itNotificationType = Convert.ToInt32(resourceManager.GetString("ITDepartmentNotificationTypeID"));
                    Notification notification = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == itNotificationType).FirstOrDefault();

                    if (notification != null)
                    {
                        string statusApprovedID = resourceManager.GetString("StatusApprovedID");
                        notification.StatusId = Convert.ToInt32(statusApprovedID);
                    }

                    hrmEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }

            return isDelete;
        }
        #endregion

        #region Execute Job
        /// <summary>
        /// Execution of the job for sending email
        /// </summary>
        /// <param name="context"></param>
        /// 
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                JobDataMap jobDataMap = context.JobDetail.JobDataMap;
                string associateId = jobDataMap.GetString("AssociateId");
                string name = jobDataMap.GetString("AssociateName");
                string designation = jobDataMap.GetString("Designation");
                string department = jobDataMap.GetString("Department");
                string mobileNo = jobDataMap.GetString("Mobile");

                using (APEntities hrmEntities = new APEntities())
                {
                    string itNotificationCode = resourceManager.GetString("ITNotification");
                    //NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == itNotificationCode).FirstOrDefault();

                    //if (notificationConfiguration != null && name != null && department != null)
                    //{
                    //    string htmlContent = notificationConfiguration.emailContent;
                    //    htmlContent = htmlContent.Replace("@AssociateName", name).Replace("@Designation", designation).Replace("@Department", department).Replace("@Mobile", mobileNo);
                    //    Email email = new Email();
                    //    int notificationConfigID = new BaseEmail().BuildEmailObject(email, notificationConfiguration.emailTo, notificationConfiguration.emailFrom, notificationConfiguration.emailCC, notificationConfiguration.emailSubject + associateId, htmlContent, Enumeration.NotificationStatus.IT.ToString());
                    //    new BaseEmail().SendEmail(email, notificationConfigID);
                    //}
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region GetItNotifications
        /// <summary>
        /// Method to get IT notification lists
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetItNotifications()
        {

            try
            {
                int notificationType = Convert.ToInt32(resourceManager.GetString("ITDepartmentNotificationTypeID"));
                int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));

                using (APEntities apEntities = new APEntities())
                {
                    var itNotifications = (from n in apEntities.Notifications
                                           join nm in apEntities.NotificationTypeMasters on n.NotificationTypeID equals nm.NotificationTypeID
                                           join e in apEntities.Employees on n.EmployeeCode equals e.EmployeeCode
                                           join i in apEntities.ITTasks on n.EmployeeCode equals i.EmployeeCode
                                           join itm in apEntities.ITTaskMasters on i.TaskID equals itm.ITTaskID
                                           where (i.StatusId == pendingStatusID && nm.NotificationTypeID == notificationType)
                                           select new NotificationData
                                           {
                                               notificationTypeId = n.NotificationTypeID,
                                               notificationCode = nm.NotificationCode,
                                               notificationDesc = nm.NotificationDesc,
                                               employeeCode = n.EmployeeCode,
                                               firstName = e.FirstName,
                                               lastName = e.LastName,
                                               statusId = i.StatusId,
                                               taskName = itm.TaskName,
                                               taskId = i.TaskID
                                           });

                    var Notification = itNotifications.Distinct().ToList();

                    if (Notification.Count > 0)
                        return Notification;
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region CountItNotifications
        /// <summary>
        /// Method to get number of IT notifications
        /// </summary>
        /// <returns></returns>
        /// 
        public int CountItNotifications()
        {

            try
            {
                using (APEntities apEntities = new APEntities())
                {

                    int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
                    var itTasksCount = (from it in apEntities.ITTasks
                                        where it.StatusId == pendingStatusID
                                        select new { it.EmployeeCode }).Distinct().Count();
                    return itTasksCount;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region UpdateItNotifications
        /// <summary>
        /// Method to update status of IT notification
        /// </summary>
        /// <param name="notificationData"></param>
        /// <returns></returns>
        /// 
        public bool UpdateItNotifications(string employeeCode, int taskId)
        {
            bool isUpdated = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    ITTask itTask = hrmsEntities.ITTasks.Where(e => e.EmployeeCode == employeeCode && e.TaskID == taskId).ToList().FirstOrDefault();

                    int statusApprovedID = Convert.ToInt32(resourceManager.GetString("StatusApprovedID"));
                    itTask.StatusId = statusApprovedID;
                    hrmsEntities.Entry(itTask).State = System.Data.Entity.EntityState.Modified;
                    isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;

                    //Remove the IT notification job if there is no more pending task for this employee
                    //
                    int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
                    ITTask pendingITTask = hrmsEntities.ITTasks.Where(e => e.EmployeeCode == employeeCode && e.StatusId == pendingStatusID).ToList().FirstOrDefault();

                    if (pendingITTask == null)
                    {
                        RemoveJob(employeeCode, resourceManager.GetString("ITNotification"));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }

            return isUpdated;
        }
        #endregion

        #region GetITTasks
        /// <summary>
        /// Method to get IT tasks lists
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ITTaskMaster> GetITTasks()
        {

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    IEnumerable<ITTaskMaster> getITTasks = (from e in hrmsEntities.ITTaskMasters
                                                            select e).ToList();
                    return getITTasks;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region GetNotificationType
        /// <summary>
        /// Method to get notification type
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetNotificationType()
        {
            List<NotificationData> notificationType = null;

            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    notificationType = (from NotificationTypes in apEntities.NotificationTypeMasters
                                        join NotificationConfigurationFromEmail in apEntities.NotificationConfigurationFromEmails on NotificationTypes.NotificationTypeID equals NotificationConfigurationFromEmail.NotificationTypeID
                                        select new NotificationData
                                        {
                                            notificationCode = NotificationTypes.NotificationCode,
                                            notificationDesc = NotificationTypes.NotificationDesc,
                                            NotificationConfigurationFromMail = NotificationConfigurationFromEmail.emailFrom,
                                            IsActive = NotificationConfigurationFromEmail.IsActive
                                        }).Where(e => e.IsActive == true).OrderBy(n => n.notificationCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }

            return notificationType;
        }
        #endregion

        #region GetAllEmailIds
        ///
        /// <summary>
        /// Method to get all email ids
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAllEmailIds()
        {
            List<UserCredentials> lstADUsers = new List<UserCredentials>();

            try
            {
                string domainPath = "LDAP://DC=senecaglobal,DC=net";
                DirectoryEntry searchRoot = new DirectoryEntry(domainPath);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person))";
                search.PropertiesToLoad.Add("samaccountname");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("usergroup");
                search.PropertiesToLoad.Add("displayname");
                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();

                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                                 result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname"))
                        {
                            UserCredentials objSurveyUsers = new UserCredentials();
                            objSurveyUsers.EmailAddress = (String)result.Properties["mail"][0];
                            objSurveyUsers.UserName = (String)result.Properties["displayname"][0];
                            lstADUsers.Add(objSurveyUsers);
                        }
                    }
                }
                else
                {
                    using (APEntities apEntities = new APEntities())
                    {
                        var usr = apEntities.Users.Select(i =>
                                        new
                                        {
                                            EmailAddress = i.EmailAddress,
                                            UserName = i.UserName
                                        }).ToList();
                        foreach (var u in usr)
                        {
                            UserCredentials objSurveyUsers = new UserCredentials();
                            objSurveyUsers.EmailAddress = u.EmailAddress;
                            objSurveyUsers.UserName = u.UserName;
                            lstADUsers.Add(objSurveyUsers);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }

            return lstADUsers;
        }
        #endregion

        #region SaveEmailNotificationConfiguration
        /// <summary>
        /// Method to save email notification configuration
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public string SaveEmailNotificationConfiguration(EmailNotificationConfiguration details)
        {
            try
            {
                string result = string.Empty;
                int dbContextSave = 0;
                int sLA = (int)details.SLA;
                int sLAinMinutes = GetSLA(sLA);

                using (APEntities hrmsEntities = new APEntities())
                {
                    NotificationConfiguration notificationConfigurationIfExists = hrmsEntities.NotificationConfigurations.FirstOrDefault(i => i.notificationCode == details.notificationCode && i.IsActive == true);//&& j.GroupID == groupId).FirstOrDefault();
                    NotificationConfiguration notificationConfigurationForNewEntry = new NotificationConfiguration();

                    if (notificationConfigurationIfExists != null)
                    {
                        notificationConfigurationIfExists.notificationCode = details.notificationCode;
                        notificationConfigurationIfExists.notificationDesc = details.notificationDesc;
                        notificationConfigurationIfExists.emailFrom = details.emailFrom;
                        notificationConfigurationIfExists.emailTo = details.emailTo;
                        notificationConfigurationIfExists.emailCC = details.emailCC;
                        notificationConfigurationIfExists.emailSubject = details.emailSubject;
                        notificationConfigurationIfExists.emailContent = details.emailContent;
                        notificationConfigurationIfExists.SLA = sLAinMinutes; //details.SLA;
                        notificationConfigurationIfExists.IsActive = true;
                        notificationConfigurationIfExists.ModifiedBy = details.CurrentUser;
                        notificationConfigurationIfExists.StartDate = DateTime.Today;
                        notificationConfigurationIfExists.ModifiedDate = DateTime.Now;
                        notificationConfigurationIfExists.SystemInfo = details.SystemInfo;
                        hrmsEntities.Entry(notificationConfigurationIfExists).State = System.Data.Entity.EntityState.Modified;
                        dbContextSave = hrmsEntities.SaveChanges();
                        result = "Notification configuration has been saved"; //string.Concat("Notification configuration has been saved ", Environment.NewLine, " however a notification with notification code :", notificationConfigurationIfExists.notificationCode, " with SLA : ", notificationConfigurationIfExists.SLA / 60, " hours starting from :  ", notificationConfigurationIfExists.StartDate.ToString().Substring(0, notificationConfigurationIfExists.StartDate.ToString().LastIndexOf(' ')), " has already been configured");                        
                    }
                    else
                    {
                        notificationConfigurationForNewEntry.notificationCode = details.notificationCode;
                        notificationConfigurationForNewEntry.notificationDesc = details.notificationDesc;
                        notificationConfigurationForNewEntry.emailFrom = details.emailFrom;
                        notificationConfigurationForNewEntry.emailTo = details.emailTo;
                        notificationConfigurationForNewEntry.emailCC = details.emailCC;
                        notificationConfigurationForNewEntry.emailSubject = details.emailSubject;
                        notificationConfigurationForNewEntry.emailContent = details.emailContent;
                        notificationConfigurationForNewEntry.SLA = sLAinMinutes; //details.SLA;
                        notificationConfigurationForNewEntry.IsActive = true;
                        notificationConfigurationForNewEntry.CreatedBy = details.CurrentUser;
                        notificationConfigurationForNewEntry.StartDate = DateTime.Today;
                        notificationConfigurationForNewEntry.CreatedDate = DateTime.Now;
                        notificationConfigurationForNewEntry.SystemInfo = details.SystemInfo;
                        hrmsEntities.NotificationConfigurations.Add(notificationConfigurationForNewEntry);
                        dbContextSave = hrmsEntities.SaveChanges();

                        if (dbContextSave > 0)
                            result = "Notification configuration has been saved ";
                    }

                    return result;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save notification.");
            }
        }
        #endregion

        #region GetSLA
        /// <summary>
        /// Method to get SLA
        /// </summary>
        /// <param name="sla"></param>
        /// <returns></returns>
        public int GetSLA(int sla)
        {
            int sLAinMinutes = 0;

            try
            {
                DateTime moment = DateTime.Now;
                DateTime currentHour = new DateTime(moment.Year, moment.Month, moment.Day, moment.Hour, 0, 0);
                DateTime sLAHour = currentHour.AddHours(sla);
                string dayOfCompletion = sLAHour.DayOfWeek.ToString();
                int timeOfCompletion = sLAHour.Hour;

                sLAinMinutes = sla * 60;

                if ((dayOfCompletion.ToString().ToUpper() == "FRIDAY" && timeOfCompletion >= 18) || (dayOfCompletion.ToString().ToUpper() == "SATURDAY") || (dayOfCompletion.ToString().ToUpper() == "SUNDAY"))
                {
                    sLAinMinutes = sLAinMinutes + 2880;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }

            return sLAinMinutes;
        }
        #endregion

        #region GetNotificationConfiguration
        /// <summary>
        /// Get Notification Configuration details based on NotificationType
        /// </summary>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        public object GetNotificationConfiguration(string notificationType)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var details = (from notify in apEntities.NotificationConfigurations
                                   where notify.notificationCode == notificationType && notify.IsActive == true
                                   select new
                                   {
                                       notify.notificationCode,
                                       notify.emailTo,
                                       notify.emailFrom,
                                       notify.emailCC,
                                       notify.emailSubject,
                                       notify.emailContent,
                                       notify.notificationDesc,
                                       notify.SLA
                                   }).FirstOrDefault();
                    return details;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion
    }
}
