﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class KRARole
    {
        #region CreateKRAForRole
        /// <summary>
        /// Create KRA for role
        /// </summary>
        /// <param name="kraSetData"></param>
        /// <returns></returns>
        public async Task<bool> CreateKRAForRole(KRARoleData kraRoleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateKRAForRole] @RoleID, @FinancialYearID, @StatusID, @KRAAspectID, @KRAAspectMetric, @KRAAspectTarget, @DateCreated, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("RoleID", kraRoleData.RoleID),
                                        new SqlParameter ("FinancialYearID", kraRoleData.FinancialYearID),
                                        new SqlParameter ("StatusID", kraRoleData.StatusID),
                                        new SqlParameter ("KRAAspectID", kraRoleData.KRAAspectID),
                                        new SqlParameter ("KRAAspectMetric", kraRoleData.KRAAspectMetric),
                                        new SqlParameter ("KRAAspectTarget", kraRoleData.KRAAspectTarget),
                                        new SqlParameter ("DateCreated", DateTime.UtcNow),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region CloneKRARoleTemplate
        /// <summary>
        /// CloneKRARoleTemplate
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        public async Task<List<KRARoleData>> CloneKRARoleTemplate(KRARoleData kraRoleData)
        {
            List<KRARoleData> lstKRARoleData;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRARoleData = await apEntities.Database.SqlQuery<KRARoleData>
                              ("[usp_CloneKRARoleTemplate] @RoleID, @CloneRoleID, @FinancialYearID, @CloneFinancialYearID, @StatusID, @CreatedUser, @DateCreated, @SystemInfo",
                                new object[] {
                                    new SqlParameter("RoleID", kraRoleData.RoleID),
                                    new SqlParameter("CloneRoleID", kraRoleData.CloneRoleID),
                                    new SqlParameter("FinancialYearID", kraRoleData.FinancialYearID),
                                    new SqlParameter("CloneFinancialYearID", kraRoleData.CloneFinancialYearID),
                                    new SqlParameter("StatusID", kraRoleData.StatusID),
                                    new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("DateCreated", DateTime.UtcNow),
                                    new SqlParameter("SystemInfo",  Commons.GetClientIPAddress())
                                }
                                ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRARoleData;
        }
        #endregion

        #region DeleteKRARoleAspect
        /// <summary>
        /// DeleteKRARoleAspect
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="kraAspectID"></param>
        /// <returns></returns>
        public async Task<bool> DeleteKRARoleAspect(int roleID, int kraAspectID, int financialYearID)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteKRARoleAspect] @RoleID, @KRAAspectID, @FinancialYearID",
                                   new object[] {
                                        new SqlParameter ("RoleID", roleID),
                                        new SqlParameter ("KRAAspectID", kraAspectID),
                                        new SqlParameter ("FinancialYearID", financialYearID),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting kra role aspect.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region DeleteKRARoleMetric
        /// <summary>
        /// DeleteKRARoleMetric
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteKRARoleMetric(int id)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteKRARoleMetric] @ID",
                                   new object[] {
                                        new SqlParameter ("ID", id)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting kra role metric.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region UpdateKRARoleMetric
        /// <summary>
        /// Update a KRA Role Metric
        /// </summary>
        /// <param name="kraRoleData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateKRARoleMetric(KRARoleData kraRoleData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateKRARoleMetric] @ID, @KRAAspectMetric, @KRAAspectTarget, @DateModified, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("ID", kraRoleData.ID),
                                        new SqlParameter ("KRAAspectMetric", kraRoleData.KRAAspectMetric),
                                        new SqlParameter ("KRAAspectTarget", kraRoleData.KRAAspectTarget),
                                        new SqlParameter ("DateModified", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetKRATemplateByRoleID
        /// <summary>
        /// GetKRATemplateByRoleID
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public async Task<List<KRARoleData>> GetKRATemplateByRoleID(int RoleID, int FinancialYearID)
        {
            List<KRARoleData> lstKraTemplate;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKraTemplate = await apEntities.Database.SqlQuery<KRARoleData>
                              ("[usp_GetKRATemplateByRoleID] @RoleID, @FinancialYearID",
                                new object[] {
                                        new SqlParameter ("RoleID", RoleID),
                                        new SqlParameter ("FinancialYearID", FinancialYearID)
                                }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKraTemplate;
        }
        #endregion

        #region GetRoleName
        /// <summary>
        /// Gets the role name using roleId
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public string GetRoleName(int roleId)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    var roleName = (from roleMaster in apEntities.RoleMasters
                                       join sgRole in apEntities.SGRoles on roleMaster.SGRoleID equals sgRole.SGRoleID
                                       join sgRolePrefix in apEntities.SGRolePrefixes on roleMaster.PrefixID equals sgRolePrefix.PrefixID into rolePrefix
                                       from prefix in rolePrefix.DefaultIfEmpty()
                                       join sgRoleSuffix in apEntities.SGRoleSuffixes on roleMaster.SuffixID equals sgRoleSuffix.SuffixID into roleSuffix
                                       from suffix in roleSuffix.DefaultIfEmpty()
                                       where roleMaster.RoleMasterID == roleId
                                       select new
                                       {
                                           RoleName = string.Concat(prefix.PrefixName, sgRole.SGRoleName, suffix.SuffixName)
                                       }).FirstOrDefault();
                    return roleName.RoleName;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetKRARoles
        /// <summary>
        /// GetKRARoles
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<List<KRARoleData>> GetKRARoles()
        {
            List<KRARoleData> getKRARoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    getKRARoles = await apEntities.Database.SqlQuery<KRARoleData>
                              ("[usp_GetKRARoles] ",
                                new object[] {
                                       
                                }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getKRARoles;
        }
        #endregion
        #region GetEmployeesByKRARole
        /// <summary>
        /// Get Employees By KRARole
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<List<KRARoleData>> GetEmployeesByKRARole(int KRARoleId,int financialYearId)
        {
            List<KRARoleData> getEmpByKRARole;
            try
            {
                using (var apEntities = new APEntities())
                {
                    getEmpByKRARole = await apEntities.Database.SqlQuery<KRARoleData>
                              ("[usp_GetEmployeesByKRARoleByFinancialYear] @KRARoleID,@FinancialYearId ",
                                new object[] {
                                     new SqlParameter ("KRARoleID",KRARoleId),
                                     new SqlParameter ("FinancialYearId",financialYearId)
                                }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getEmpByKRARole;
        }
        #endregion

        #region GetDepartmentName
        /// <summary>
        /// Gets the department name using departmentId
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public string GetDepartmentName(int departmentId)
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    string departmentName = apEntities.Departments.Where(department => department.DepartmentId == departmentId)
                        .Select(d => d.Description).FirstOrDefault();

                    return departmentName;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion        
    }
}
