﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static AP.Utility.Enumeration;

namespace AP.API
{
    public class AssociateFeedbackDetails
    {
        #region GetProjectsByUserID
        /// <summary>
        /// To get the projects for that user
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EmployeeFeedback> GetProjectsByUserID(string userName)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    var projects = (from u in hrmsEntities.Users
                                    join emp in hrmsEntities.Employees on u.UserId equals emp.UserId
                                    join aa in hrmsEntities.AssociateAllocations on emp.EmployeeId equals aa.EmployeeId
                                    join p in hrmsEntities.Projects on aa.ProjectId equals p.ProjectId
                                    where u.EmailAddress == userName && u.IsActive == true && emp.IsActive == true
                                    && aa.IsActive == true && p.IsActive == true
                                    select new EmployeeFeedback
                                    {
                                        ProjectId = p.ProjectId,
                                        ProjectName = p.ProjectName,

                                    }).Distinct().OrderBy(i => i.ProjectName).ToList();

                    return projects;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        

        #region GetAssociateFeedbacks
        /// <summary>
        /// To get the associate feedbacks based on empID
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssociateFeedbacks(string userName, int projectID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var empFeedbacks = (from u in hrmsEntities.Users
                                        join emp in hrmsEntities.Employees on u.UserId equals emp.UserId
                                        join af in hrmsEntities.AssociateFeedbacks on emp.EmployeeId equals af.EmpId
                                        join p in hrmsEntities.Projects on af.ProjectId equals p.ProjectId
                                        join skill in hrmsEntities.Skills on af.SkillId equals skill.SkillId
                                        join s in hrmsEntities.Status on af.StatusId equals s.StatusId
                                        join ca in hrmsEntities.CompetencyAreas on af.CompetencyAreaId equals ca.CompetencyAreaId
                                        join pl in hrmsEntities.ProficiencyLevels on af.ProficiencyLevelId equals pl.ProficiencyLevelId
                                        where u.EmailAddress == userName && af.ProjectId == projectID && u.IsActive == true && af.IsActive == true && emp.IsActive == true
                                        && p.IsActive == true && skill.IsActive == true && ca.IsActive == true && pl.IsActive == true
                                        select new
                                        {
                                            ID = af.AssociateFeedbackId,
                                            ProjectId = af.ProjectId,
                                            EmpId = af.EmpId,
                                            LeadId = af.LeadId,
                                            ManagerId = af.ManagerId,
                                            EmpName = string.Empty,
                                            CompetencyAreaId = ca.CompetencyAreaId,
                                            CompetencyArea = ca.CompetencyAreaCode,
                                            Skill = skill.SkillCode,
                                            SkillId = skill.SkillId,
                                            ProficiencyLevel = pl.ProficiencyLevelCode,
                                            EmpFeedback = af.AssociateContribution,
                                            ManagerFeedback = af.ManagerFeedback,
                                            SkillApplied = af.SkillApplied,
                                            FeedbackStatus = s.StatusDescription

                                        }).Distinct().ToList();

                    return empFeedbacks;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetAllAssociateFeedbacks
        /// <summary>
        /// To get the associate feedbacks based on empID
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAllAssociateFeedbacks(int projectID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var allEmpFeedbacks = (from af in hrmsEntities.AssociateFeedbacks
                                           join emp in hrmsEntities.Employees on af.EmpId equals emp.EmployeeId
                                           join s in hrmsEntities.Status on af.StatusId equals s.StatusId
                                           join skill in hrmsEntities.Skills on af.SkillId equals skill.SkillId
                                           join ca in hrmsEntities.CompetencyAreas on af.CompetencyAreaId equals ca.CompetencyAreaId
                                           join pl in hrmsEntities.ProficiencyLevels on af.ProficiencyLevelId equals pl.ProficiencyLevelId
                                           where af.ProjectId == projectID && af.IsActive == true && emp.IsActive == true && skill.IsActive == true
                                           && ca.IsActive == true && pl.IsActive == true
                                           select new EmployeeFeedback
                                           {
                                               ID = af.AssociateFeedbackId,
                                               FirstName = emp.FirstName,
                                               LastName = emp.LastName,
                                               ProjectId = af.ProjectId,
                                               EmpId = af.EmpId,
                                               LeadId = af.LeadId,
                                               ManagerId = af.ManagerId,
                                               CompetencyArea = ca.CompetencyAreaCode,
                                               CompetencyAreaId = ca.CompetencyAreaId,
                                               Skill = skill.SkillCode,
                                               SkillId = skill.SkillId,
                                               ProficiencyLevel = pl.ProficiencyLevelCode,
                                               EmpFeedback = af.AssociateContribution,
                                               ManagerFeedback = af.ManagerFeedback,
                                               SkillApplied = af.SkillApplied,
                                               FeedbackStatus = s.StatusDescription

                                           }).Distinct().ToList();

                    allEmpFeedbacks.Select(e => { e.EmpName = e.FirstName + " " + e.LastName; return e; }).ToList();

                    return allEmpFeedbacks;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region GetAssociateFeedbackByID
        /// <summary>
        /// To get the associate feedbacks based on empID
        /// </summary>
        /// <returns></returns>
        public object GetAssociateFeedbackByID(int associateFeedbackID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var empFeedbacks = (from af in hrmsEntities.AssociateFeedbacks
                                        join skill in hrmsEntities.Skills on af.SkillId equals skill.SkillId
                                        join p in hrmsEntities.Projects on af.ProjectId equals p.ProjectId
                                        join projectmgrs in hrmsEntities.ProjectManagers on p.ProjectId equals projectmgrs.ProjectID
                                        join emp in hrmsEntities.Employees on projectmgrs.ReportingManagerID equals emp.EmployeeId
                                        join employee in hrmsEntities.Employees on af.EmpId equals employee.EmployeeId
                                        join e in hrmsEntities.Employees on projectmgrs.ProgramManagerID equals e.EmployeeId
                                        join ca in hrmsEntities.CompetencyAreas on af.CompetencyAreaId equals ca.CompetencyAreaId
                                        join pl in hrmsEntities.ProficiencyLevels on af.ProficiencyLevelId equals pl.ProficiencyLevelId
                                        where af.AssociateFeedbackId == associateFeedbackID && skill.IsActive == true && p.IsActive == true
                                        && af.IsActive == true && emp.IsActive == true && e.IsActive == true && ca.IsActive == true && pl.IsActive == true
                                        select new EmployeeFeedback
                                        {
                                            CompetencyArea = ca.CompetencyAreaCode,
                                            CompetencyAreaId = ca.CompetencyAreaId,
                                            ProjectName = p.ProjectName,
                                            ProjectId = p.ProjectId,
                                            Skill = skill.SkillCode,
                                            SkillId = skill.SkillId,
                                            EmpId = af.EmpId,
                                            FirstName = employee.FirstName,
                                            LastName = employee.LastName,
                                            ManagerFirstName = e.FirstName,
                                            ManagerLastName = e.LastName,
                                            ManagerId = projectmgrs.ProgramManagerID,
                                            LeadFirstName = emp.FirstName,
                                            LeadLastName = emp.LastName,
                                            LeadId = projectmgrs.ReportingManagerID,
                                            ProficiencyLevel = pl.ProficiencyLevelCode,
                                            ProficiencyLevelId = pl.ProficiencyLevelId,
                                            AssociateContribution = af.AssociateContribution,
                                            ManagerFeedback = af.ManagerFeedback,
                                            SkillApplied = af.SkillApplied

                                        }).FirstOrDefault();

                    empFeedbacks.EmpName = $"{empFeedbacks.FirstName} {empFeedbacks.LastName}";
                    empFeedbacks.ManagerName = $"{empFeedbacks.ManagerFirstName} {empFeedbacks.ManagerLastName}";
                    empFeedbacks.LeadName = $"{empFeedbacks.LeadFirstName} {empFeedbacks.LeadLastName}";


                    return empFeedbacks;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetSkillsByCompAreaId
        /// <summary>
        /// Retrieves the skills by competenctAreaID and projectID from projectskills 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetSkillsByCompAreaId(int competenctAreaID, int projectID)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var SkillList = (from ps in hrmEntities.ProjectSkills
                                     join skill in hrmEntities.Skills on ps.SkillId equals skill.SkillId
                                     where ps.IsActive == true && ps.CompetencyAreaId == competenctAreaID && ps.ProjectId == projectID && skill.IsActive == true
                                     select new
                                     {
                                         SkillId = ps.SkillId,
                                         SkillName = skill.SkillCode
                                     }).Distinct().ToList();

                    return SkillList;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, null);
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region GetAssociatesByProjectID
        /// <summary>
        /// To get associates based on projectID
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetAssociatesByProjectID(int projectID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var projects = (from aa in hrmsEntities.AssociateAllocations
                                    join emp in hrmsEntities.Employees on aa.EmployeeId equals emp.EmployeeId
                                    where aa.ProjectId == projectID && aa.IsActive == true && emp.IsActive == true
                                    select new EmployeeFeedback
                                    {
                                        EmpId = aa.EmployeeId.Value,
                                        FirstName = emp.FirstName,
                                        LastName = emp.LastName

                                    }).Distinct().OrderBy(i => i.FirstName).ToList();

                    projects.Select(e => { e.EmpName = e.FirstName + " " + e.LastName; return e; }).ToList();

                    return projects;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeleteAssociateFeedbackByID
        /// <summary>
        /// to delete associates feedback based on Id
        /// </summary>
        /// <returns></returns>

        public bool DeleteAssociateFeedbackByID(int associateFeedbackID)
        {
            bool returnValue = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    AssociateFeedback af = hrmsEntities.AssociateFeedbacks.Where(i => i.AssociateFeedbackId == associateFeedbackID).FirstOrDefault();

                    if (af != null)
                    {
                        af.IsActive = false;
                        af.ModifiedDate = DateTime.Now;
                        af.ModifiedUser = HttpContext.Current.User.Identity.Name;
                        af.SystemInfo = Commons.GetClientIPAddress();
                        returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }
        #endregion

        #region SaveAssociateFeedback
        /// <summary>
        /// UpdateFamilyDetails
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool SaveAssociateFeedback(EmployeeFeedback associateFeedbackDetails)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    AssociateFeedback af = hrmsEntities.AssociateFeedbacks.Where(i => i.AssociateFeedbackId == associateFeedbackDetails.ID).FirstOrDefault();
                    if (af != null)
                    {
                        af.EmpId = associateFeedbackDetails.EmpId;
                        af.ProjectId = associateFeedbackDetails.ProjectId;
                        af.LeadId = associateFeedbackDetails.LeadId.Value;
                        af.ManagerId = associateFeedbackDetails.ManagerId.Value;
                        af.CompetencyAreaId = associateFeedbackDetails.CompetencyAreaId;
                        af.SkillId = associateFeedbackDetails.SkillId;
                        af.ProficiencyLevelId = associateFeedbackDetails.ProficiencyLevelId;
                        af.AssociateContribution = associateFeedbackDetails.AssociateContribution;
                        af.ManagerFeedback = associateFeedbackDetails.ManagerFeedback;
                        af.SkillApplied = associateFeedbackDetails.SkillApplied;
                        af.StatusId = GetStatusId(StatusCategory.ProjectManagement.ToString(), associateFeedbackDetails.Status);
                        af.FeedbackStatus = associateFeedbackDetails.Status;
                        af.IsActive = true;
                        af.ModifiedUser = associateFeedbackDetails.CurrentUser;
                        af.ModifiedDate = DateTime.Now;
                        af.SystemInfo = associateFeedbackDetails.SystemInfo;
                        hrmsEntities.Entry(af).State = EntityState.Modified;
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                    {
                        af = new AssociateFeedback();
                        af.EmpId = associateFeedbackDetails.EmpId;
                        af.ProjectId = associateFeedbackDetails.ProjectId;
                        af.LeadId = associateFeedbackDetails.LeadId.Value;
                        af.ManagerId = associateFeedbackDetails.ManagerId.Value;
                        af.CompetencyAreaId = associateFeedbackDetails.CompetencyAreaId;
                        af.SkillId = associateFeedbackDetails.SkillId;
                        af.ProficiencyLevelId = associateFeedbackDetails.ProficiencyLevelId;
                        af.AssociateContribution = associateFeedbackDetails.AssociateContribution;
                        af.ManagerFeedback = associateFeedbackDetails.ManagerFeedback;
                        af.SkillApplied = associateFeedbackDetails.SkillApplied;
                        af.StatusId = GetStatusId(StatusCategory.ProjectManagement.ToString(), associateFeedbackDetails.Status);
                        af.FeedbackStatus = associateFeedbackDetails.Status;
                        af.IsActive = true;
                        af.CreatedUser = associateFeedbackDetails.CurrentUser;
                        af.CreatedDate = DateTime.Now;
                        af.SystemInfo = associateFeedbackDetails.SystemInfo;
                        hrmsEntities.AssociateFeedbacks.Add(af);
                        retValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retValue;
        }

        #endregion'
        private int GetStatusId(string category, string statusCode)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var statusId = hrmEntities.Status.Where(s => s.StatusCode.ToLower() == statusCode.ToLower() && s.Category.ToLower() == category.ToLower()).FirstOrDefault().StatusId;
                    if (statusId == null)
                        return 0;
                    else
                        return statusId;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details."); ;
            }
        }

        //#region GetManagerandLeadByProjectID
        ///// <summary>
        ///// To get the lead name by project ID and Employee ID
        ///// </summary>
        ///// <param name="projectID"></param>
        ///// <param name="employeeID"></param>
        ///// <returns></returns>
        //public object GetManagerandLeadByProjectID(int projectID, int employeeID)
        //{
        //    try
        //    {
        //        using (APEntities hrmsEntities = new APEntities())
        //        {
        //            EmployeeFeedback getNames = (from allocation in hrmsEntities.AssociateAllocations
        //                                         join projectmgr in hrmsEntities.ProjectManagers on allocation.ProjectId equals projectmgr.ProjectID
        //                                         join lead in hrmsEntities.Employees on projectmgr.ReportingManagerID equals lead.EmployeeId
        //                                         where allocation.ProjectId == projectID && allocation.EmployeeId == employeeID && allocation.IsActive == true && lead.IsActive == true
        //                                         select new EmployeeFeedback
        //                                         {
        //                                             LeadFirstName = lead.FirstName,
        //                                             LeadLastName = lead.LastName
        //                                         }).FirstOrDefault();

        //            getNames.LeadName = $"{getNames.LeadFirstName} {getNames.LeadLastName}";

        //            return getNames;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion
    }
}

