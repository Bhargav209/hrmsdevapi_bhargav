﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Notification;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace AP.API
{
    public class Report
    {
        #region GetResourceByProject
        /// <summary>
        /// Get Resource By Project
        /// </summary>
        /// <returns></returns>
        public async Task<AllocationDetails> GetResourceByProject(int projectId)
        {
            AllocationDetails allocationDetails = new AllocationDetails();
            allocationDetails.lstBillableResources = new List<ResourceAllocationDetails>();
            allocationDetails.lstNonBillableResources = new List<ResourceAllocationDetails>();
            try
            {
                using (var apEntities = new APEntities())
                {
                    allocationDetails = await GetAllocationCountByProject(allocationDetails, projectId);
                    allocationDetails = await GetBillableResourcesByProject(allocationDetails, projectId);
                    allocationDetails = await GetNonBillableResourcesByProject(allocationDetails, projectId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return allocationDetails;
        }

        private async Task<AllocationDetails> GetAllocationCountByProject(AllocationDetails allocationDetails, int projectId)
        {
            using (var apEntities = new APEntities())
            {
                allocationDetails.AllocationCount = await apEntities.Database.SqlQuery<AllocationCount>
                         ("[usp_GetResourceByProjectForReport] @projectId", new object[] {
                             new SqlParameter ("projectId", projectId)
                         }).FirstOrDefaultAsync();
            }
            return allocationDetails;
        }

        private async Task<AllocationDetails> GetBillableResourcesByProject(AllocationDetails allocationDetails, int projectId)
        {
            using (var apEntities = new APEntities())
            {
                allocationDetails.lstBillableResources = await apEntities.Database.SqlQuery<ResourceAllocationDetails>
                         ("[usp_GetBillableResourcesByProjectForReport] @projectId", new object[] {
                             new SqlParameter ("projectId", projectId)
                         }).ToListAsync();
            }
            return allocationDetails;
        }

        private async Task<AllocationDetails> GetNonBillableResourcesByProject(AllocationDetails allocationDetails, int projectId)
        {
            using (var apEntities = new APEntities())
            {
                allocationDetails.lstNonBillableResources = await apEntities.Database.SqlQuery<ResourceAllocationDetails>
                         ("[usp_GetNonBillableResourcesByProjectForReport] @projectId", new object[] {
                             new SqlParameter ("projectId", projectId)
                         }).ToListAsync();
            }
            return allocationDetails;
        }
        #endregion

        #region GetAssociateSkillsReport
        /// <summary>
        /// Gets Associate Skills Report.
        /// </summary>
        /// <param name="skillSearchFilter"></param>
        /// <returns></returns>
        public async Task<List<SkillSearchData>> GetAssociateSkillsReport(SkillSearchData skilldata)
        {
            List<SkillSearchData> skillSearchEmployees;
            try
            {
                using (var apEntities = new APEntities())
                {
                    skillSearchEmployees = await apEntities.Database.SqlQuery<SkillSearchData>
                               ("[usp_rpt_GetAssociateSkillsReport] @SkillIds,@SkillNames,@IsPrimary,@IsSecondary",
                                 new object[] {
                                     new SqlParameter("SkillIds",string.IsNullOrEmpty(skilldata.SkillIds)?"":skilldata.SkillIds),
                                        new SqlParameter("SkillNames",string.IsNullOrEmpty(skilldata.SkillNames)?"":skilldata.SkillNames),
                                        new SqlParameter("IsPrimary",skilldata.IsPrimary),
                                        new SqlParameter("IsSecondary",skilldata.IsSecondary),
                                    }).ToListAsync();
                    skillSearchEmployees = skillSearchEmployees.OrderBy(p => p.EmployeeName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return skillSearchEmployees;
        }
        #endregion

        #region DomainReportCount
        /// <summary>
        /// Get domain wise report count
        /// </summary>
        /// <returns></returns>
        public async Task<List<DomainDataCount>> DomainReportCount()
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<DomainDataCount>
                               ("[usp_rpt_DomainCountReport]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DomainReport
        /// <summary>
        /// Get domain wise report
        /// </summary>
        /// <param name="domainID"></param>
        /// <returns></returns>
        public async Task<List<DomainData>> DomainReport(int domainID)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<DomainData>
                  ("[usp_rpt_GetDomainReport] @DomainID",
                                 new object[] {
                                    new SqlParameter("DomainID", domainID)
                                    }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region TalentPoolReportCount
        /// <summary>
        /// Get domain wise report count
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<List<TalentPoolDataCount>> TalentPoolReportCount()
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<TalentPoolDataCount>
                               ("[usp_rpt_TalentPoolReportCount]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region TalentPoolReport
        /// <summary>
        /// Get Talent Pool report
        /// </summary>
        /// <param name="projectID"></param>
        /// <returns></returns>
        public async Task<List<TalentPoolReportData>> TalentPoolReport(int projectID)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<TalentPoolReportData>
                  ("[usp_rpt_GetTalentPoolReport] @ProjectID",
                                 new object[] {
                                    new SqlParameter("ProjectID", projectID)
                                    }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region GetProjectHistoryByEmployee
        /// <summary>
        /// Get Project History By Employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<List<TalentPoolReportData>> GetProjectHistoryByEmployee(int employeeId)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    return await apEntities.Database.SqlQuery<TalentPoolReportData>
                  ("[usp_rpt_GetProjectHistoryByEmployee] @EmployeeId",
                                 new object[] {
                                    new SqlParameter("EmployeeId", employeeId)
                                    }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetFinanceReports
        /// <summary>
        /// Gets finance reports
        /// </summary>
        /// <returns></returns>
        public async Task<ReportsFilterData> GetFinanceReports(ReportsFilterData reportsFilterData)
        {
            reportsFilterData.reportsData = new List<ReportsData>();
            try
            {
                using (var apEntities = new APEntities())
                {
                    reportsFilterData.financeReportFilterData.FromDate = Commons.GetDateTimeInIST(reportsFilterData.financeReportFilterData.FromDate);
                    reportsFilterData.financeReportFilterData.ToDate = Commons.GetDateTimeInIST(reportsFilterData.financeReportFilterData.ToDate);

                    //reportsFilterData.TotalCount = await apEntities.Database.SqlQuery<int>
                    //                               ("[usp_rpt_FinanceReportCount] @FromDate, @ToDate, @ProjectID",
                    //                               new object[] {
                    //                                    new SqlParameter("FromDate", reportsFilterData.financeReportFilterData.FromDate),
                    //                                    new SqlParameter("ToDate", reportsFilterData.financeReportFilterData.ToDate),
                    //                                    new SqlParameter("ProjectID", reportsFilterData.financeReportFilterData.ProjectId),
                    //                               }).SingleOrDefaultAsync();

                    //if (reportsFilterData.TotalCount > 0)
                    //{
                        reportsFilterData.reportsData = await apEntities.Database.SqlQuery<ReportsData>
                                                        //("[usp_rpt_FinanceReport] @FromDate, @ToDate, @ProjectID, @RowsPerPage, @PageNumber",
                                                        ("[usp_rpt_FinanceReport] @FromDate, @ToDate, @ProjectID",
                                                        new object[] {
                                                            new SqlParameter("FromDate", reportsFilterData.financeReportFilterData.FromDate),
                                                            new SqlParameter("ToDate", reportsFilterData.financeReportFilterData.ToDate),
                                                            new SqlParameter("ProjectID", reportsFilterData.financeReportFilterData.ProjectId),
                                                            //new SqlParameter("RowsPerPage", reportsFilterData.financeReportFilterData.RowsPerPage),
                                                            //new SqlParameter("PageNumber", reportsFilterData.financeReportFilterData.PageNumber),
                                                        }).ToListAsync();
                    //}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportsFilterData;
        }
        #endregion

        #region GetFinanceReportToFreez
        /// <summary>
        /// Get Finance Report To Freez
        /// </summary>
        /// <returns></returns>
        public bool GetFinanceReportToFreez()
        {
            int rowsAffected = 0;
            try
            {
                List<ReportsData> reportsData = new List<ReportsData>();

                using (var apEntities = new APEntities())
                {
                    DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 1);
                    DateTime endtDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month - 1));

                    //DateTime currentMonthEndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month , DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                    //int currentDate = DateTime.Now.Day;
                    //DateTime nextMonthStartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month + 1, 1);

                    int ProjectId = 0;
                    int rowCounts = 0;

                    //if (currentDate <= currentMonthEndDate.Day && DateTime.Now != nextMonthStartDate)
                    //{
                    rowCounts = apEntities.Database.SqlQuery<int>
                          ("[usp_rpt_CheckFinanceReportExists] @Month, @Year",
                             new object[] {
                                      new SqlParameter("Month", startDate.Month ),
                                      new SqlParameter("Year", startDate.Year),
                             }).SingleOrDefault();
                    if (rowCounts > 0)
                    {
                        throw new AssociatePortalException("Previous Month Data Already Archived");
                    }
                    else
                    {
                        reportsData = apEntities.Database.SqlQuery<ReportsData>
                                          ("[usp_rpt_FinanceReport] @FromDate, @ToDate, @ProjectID",
                                          new object[] {
                                               new SqlParameter("FromDate", startDate ),
                                               new SqlParameter("ToDate", endtDate),
                                               new SqlParameter("ProjectID", ProjectId),
                                              }).ToList();

                        var jsonData = new JavaScriptSerializer().Serialize(reportsData);

                        rowsAffected = apEntities.Database.SqlQuery<int>
                            ("[usp_rpt_FreezeFinanceReport] @Month, @Year, @JsonData,@CreatedUser,@CreatedDate,@SystemInfo",
                            new object[] {
                                 new SqlParameter("Month", startDate.Month ),
                                 new SqlParameter("Year", startDate.Year),
                                 new SqlParameter("JsonData", jsonData),
                                 new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                 new SqlParameter ("CreatedDate", DateTime.Now),
                                 new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                            }).SingleOrDefault();
                    }
                    //}
                    //else
                    //{
                    //    throw new AssociatePortalException("You can't Archive Rmg Data ");
                    //}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (rowsAffected > 0 ? true : false);
        }
        #endregion

        #region GetRmgReportDataByMonthYear
        /// <summary>
        /// Get Rmg Report Data By Month Year
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReportsData>> GetRmgReportDataByMonthYear(int month, int year)
        {
            List<ReportsData> rmgReportsData = new List<ReportsData>();

            try
            {
                using (var apEntities = new APEntities())
                {
                    var reportsData = await apEntities.Database.SqlQuery<string>
                                                    ("[usp_rpt_GetRmgReportDataByMonthYear] @Month, @Year",
                                                    new object[] {
                                                            new SqlParameter("Month", month),
                                                            new SqlParameter("Year",year),
                                                    }).ToListAsync();
                    if (reportsData.Count > 0)
                    {
                        var jsonData = new JavaScriptSerializer().Deserialize(reportsData[0], rmgReportsData.GetType());

                        rmgReportsData = jsonData as List<ReportsData>;
                        return rmgReportsData;
                    }
                    else
                    {
                        throw new AssociatePortalException("No records found for selected month and year");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetUtilizationReports
        /// <summary>
        /// Gets utilization reports
        /// </summary>
        /// <returns></returns>
        public async Task<ReportsFilterData> GetUtilizationReports(ReportsFilterData reportsFilterData)
        {
            reportsFilterData.reportsData = new List<ReportsData>();
            reportsFilterData.utilizationReportFilterData.MinExperience = -1; //if value is -1 then SP ignores it
            reportsFilterData.utilizationReportFilterData.MaxExperience = -1; //if value is -1 then SP ignores it

            if (!string.IsNullOrEmpty(reportsFilterData.utilizationReportFilterData.ExperienceRange))
            {
                string[] experienceRange = reportsFilterData.utilizationReportFilterData.ExperienceRange.Split('-');
                reportsFilterData.utilizationReportFilterData.MinExperience = Convert.ToInt32(experienceRange[0].Trim());
                reportsFilterData.utilizationReportFilterData.MaxExperience = Convert.ToInt32(experienceRange[1].Trim());
            }
            try
            {
                using (var apEntities = new APEntities())
                {
                    reportsFilterData.TotalCount = await apEntities.Database.SqlQuery<int>
                                                   ("[usp_rpt_UtilizationReportCount] @EmployeeID, @ProjectID, @GradeID, @DesignationID, @ClientID, @AllocationPercentageID, @ProgramManagerID, @MinExperience,@MaxExperience, @IsBillable, @IsCritical,@practiceAreaID",
                                                   new object[] {
                                                        new SqlParameter("EmployeeID", reportsFilterData.utilizationReportFilterData.EmployeeId),
                                                        new SqlParameter("ProjectID", reportsFilterData.utilizationReportFilterData.ProjectId),
                                                        new SqlParameter("GradeID", reportsFilterData.utilizationReportFilterData.GradeId),
                                                        new SqlParameter("DesignationID", reportsFilterData.utilizationReportFilterData.DesignationId),
                                                        new SqlParameter("ClientID", reportsFilterData.utilizationReportFilterData.ClientId),
                                                        new SqlParameter("AllocationPercentageID", reportsFilterData.utilizationReportFilterData.AllocationPercentageId),
                                                        new SqlParameter("ProgramManagerID", reportsFilterData.utilizationReportFilterData.ProgramManagerId),
                                                        new SqlParameter("MinExperience", reportsFilterData.utilizationReportFilterData.MinExperience),
                                                        new SqlParameter("MaxExperience", reportsFilterData.utilizationReportFilterData.MaxExperience),
                                                        new SqlParameter("IsBillable", reportsFilterData.utilizationReportFilterData.IsBillable),
                                                        new SqlParameter("IsCritical", reportsFilterData.utilizationReportFilterData.IsCritical),
                                                        new SqlParameter("practiceAreaID", reportsFilterData.utilizationReportFilterData.PracticeAreaId)


                                                   }).SingleOrDefaultAsync();

                    if (reportsFilterData.TotalCount > 0)
                    {
                        reportsFilterData.reportsData = await apEntities.Database.SqlQuery<ReportsData>
                                                    ("[usp_rpt_UtilizationReport] @EmployeeID, @ProjectID, @GradeID, @DesignationID, @ClientID, @AllocationPercentageID, @ProgramManagerID, @MinExperience,@MaxExperience, @IsBillable, @IsCritical, @isExportToExcel,@practiceAreaID",
                                                    new object[] {
                                                        new SqlParameter("EmployeeID", reportsFilterData.utilizationReportFilterData.EmployeeId),
                                                        new SqlParameter("ProjectID", reportsFilterData.utilizationReportFilterData.ProjectId),
                                                        new SqlParameter("GradeID", reportsFilterData.utilizationReportFilterData.GradeId),
                                                        new SqlParameter("DesignationID", reportsFilterData.utilizationReportFilterData.DesignationId),
                                                        new SqlParameter("ClientID", reportsFilterData.utilizationReportFilterData.ClientId),
                                                        new SqlParameter("AllocationPercentageID", reportsFilterData.utilizationReportFilterData.AllocationPercentageId),
                                                        new SqlParameter("ProgramManagerID", reportsFilterData.utilizationReportFilterData.ProgramManagerId),
                                                        new SqlParameter("MinExperience", reportsFilterData.utilizationReportFilterData.MinExperience),
                                                        new SqlParameter("MaxExperience", reportsFilterData.utilizationReportFilterData.MaxExperience),
                                                        new SqlParameter("IsBillable", reportsFilterData.utilizationReportFilterData.IsBillable),
                                                        new SqlParameter("IsCritical", reportsFilterData.utilizationReportFilterData.IsCritical),
                                                       // new SqlParameter("RowsPerPage", reportsFilterData.utilizationReportFilterData.RowsPerPage),
                                                       // new SqlParameter("PageNumber", reportsFilterData.utilizationReportFilterData.PageNumber),
                                                        new SqlParameter("isExportToExcel", reportsFilterData.utilizationReportFilterData.isExportToExcel),
                                                        new SqlParameter("practiceAreaID", reportsFilterData.utilizationReportFilterData.PracticeAreaId)
                                                    }).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportsFilterData;
        }
        #endregion

        #region GetUtilizationReportsByTechnology
        /// <summary>
        /// Gets utilization reports By Technology
        /// </summary>
        /// <returns></returns>
        public async Task<ReportsFilterData> GetUtilizationReportsByTechnology(ReportsFilterData reportsFilterData)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    reportsFilterData.TotalCount = await apEntities.Database.SqlQuery<int>
                                                   ("[usp_rpt_UtilizationReportByTechnologyCount] @practiceAreaID",
                                                   new object[] {
                                                        new SqlParameter("practiceAreaID", reportsFilterData.utilizationReportFilterData.PracticeAreaId)

                                                   }).SingleOrDefaultAsync();

                    if (reportsFilterData.TotalCount > 0)
                    {
                        reportsFilterData.reportsData = await apEntities.Database.SqlQuery<ReportsData>
                                                    ("[usp_rpt_UtilizationReportByTechnology] @isExportToExcel,@practiceAreaID",
                                                    new object[] {
                                                        new SqlParameter("isExportToExcel", reportsFilterData.utilizationReportFilterData.isExportToExcel),
                                                        new SqlParameter("practiceAreaID", reportsFilterData.utilizationReportFilterData.PracticeAreaId)
                                                    }).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportsFilterData;
        }
        #endregion

        #region GetUtilizationReportsByMonth
        /// <summary>
        /// Gets utilization reports By Month
        /// </summary>
        /// <returns></returns>
        public async Task<ReportsFilterData> GetUtilizationReportsByMonth(ReportsFilterData reportsFilterData)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                     
                        reportsFilterData.reportsData = await apEntities.Database.SqlQuery<ReportsData>
                                                ("[usp_GetResourceReportByMonth] @frommonth,@tomonth,@year",
                                                new object[] {
                                                        new SqlParameter("frommonth", reportsFilterData.utilizationReportByMonthFilterData.FromMonth),
                                                        new SqlParameter("tomonth", reportsFilterData.utilizationReportByMonthFilterData.ToMonth),
                                                        new SqlParameter("year", reportsFilterData.utilizationReportByMonthFilterData.Year)
                                                }).ToListAsync();
                    reportsFilterData.TotalCount = reportsFilterData.reportsData.Count();
               
            }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportsFilterData;
        }
        #endregion
    }
}

