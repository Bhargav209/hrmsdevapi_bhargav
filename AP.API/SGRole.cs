﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
   public class SGRole
   {
      #region Get SG Roles
      /// <summary>
      /// Gets SG Roles
      /// </summary>
      /// <returns></returns>
      public async Task<List<SGRoleData>> GetSGRoles()
      {
         List<SGRoleData> lstSGRoles;
         try
         {
            using (var apEntities = new APEntities())
            {
               lstSGRoles = await apEntities.Database.SqlQuery<SGRoleData>
                         ("[usp_GetSGRoles]").ToListAsync();
            }
         }
         catch (Exception ex)
         {
            throw ex;
         }
         return lstSGRoles.OrderBy(o=>o.SGRoleName).ToList();
      }
      #endregion
   }
}
