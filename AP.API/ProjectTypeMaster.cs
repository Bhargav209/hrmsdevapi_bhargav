﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AP.DataStorage;
using AP.Utility;
using AP.DomainEntities;

namespace AP.API
{
    public class ProjectTypeMaster
    {
        #region CreateProjectType
        /// <summary>
        /// Method to add projectType data to database
        /// </summary>
        /// <param name="projectTypeData"></param>
        /// <returns></returns>
        public bool CreateProjectType(ProjectTypeDetails projectTypeData)
        {
            bool projectTypeRetValue = false;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var isExists = (from s in hrmEntities.ProjectTypes
                                    where s.ProjectTypeCode == projectTypeData.ProjectTypeCode
                                    select s).Count();
                    if (isExists == 0)
                    {

                        ProjectType projectType = new ProjectType();
                        projectType.ProjectTypeCode = projectTypeData.ProjectTypeCode;
                        projectType.Description = projectTypeData.Description;
                        projectType.IsActive = true;
                        projectType.CreatedUser = projectTypeData.CurrentUser;
                        projectType.CreatedDate = DateTime.Now;
                        projectType.SystemInfo = projectTypeData.SystemInfo;
                        hrmEntities.ProjectTypes.Add(projectType);
                        projectTypeRetValue = hrmEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("Project type code already exists");
                }
            }
            catch
            {
                throw;
            }

            return projectTypeRetValue;
        }
        #endregion

        #region GetProjectTypes
        /// <summary>
        /// Get projectType Master details from DB
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public IEnumerable<object> GetProjectTypes(bool isActive)
        {

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    var projectTypeMasterDetails = (from a in hrmEntities.ProjectTypes
                                               select new { a.ProjectTypeCode, a.Description, IsActive = a.IsActive == true ? "Yes" : "No", a.ProjectTypeId }).OrderBy(ptype=>ptype.ProjectTypeCode).ToList();
                    if (isActive)
                        projectTypeMasterDetails = projectTypeMasterDetails.Where(i => i.IsActive == "Yes").ToList();

                    if (projectTypeMasterDetails.Count > 0)
                        return projectTypeMasterDetails;

                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateProjectType
        /// <summary>
        /// Method to update projectType data
        /// </summary>
        /// <param name="projectTypeData"></param>
        /// <returns></returns>
        public bool UpdateProjectType(ProjectTypeDetails projectTypeData)
        {
            bool modifiedRetValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var isExists = (from s in hrmsEntities.ProjectTypes
                                    where s.ProjectTypeCode == projectTypeData.ProjectTypeCode && s.ProjectTypeId != projectTypeData.ProjectTypeId
                                    select s).Count();
                    if (isExists == 0)
                    {
                        ProjectType projectType = hrmsEntities.ProjectTypes.FirstOrDefault(i => i.ProjectTypeId == projectTypeData.ProjectTypeId);
                        projectType.ProjectTypeCode = projectTypeData.ProjectTypeCode;
                        projectType.Description = projectTypeData.Description;
                        projectType.IsActive = true;
                        projectType.ModifiedDate = DateTime.Now;
                        projectType.ModifiedUser = projectTypeData.CurrentUser;
                        projectType.SystemInfo = projectTypeData.SystemInfo;
                        hrmsEntities.Entry(projectType).State = System.Data.Entity.EntityState.Modified;
                        modifiedRetValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                    }
                    else
                        throw new AssociatePortalException("ProjectType Code already exists");
                }

            }
            catch
            {
                throw;
            }

            return modifiedRetValue;
        }
        #endregion
    }
}
