﻿using AP.DomainEntities;

namespace AP.API
{
    public interface ILogin
    {
        #region LoginUser
        /// <summary>
        /// LoginUser
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        bool LoginUser(string loginUser, string password);
        #endregion
    }

}
