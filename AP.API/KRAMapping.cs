﻿using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AP.DataStorage;
using AP.DomainEntities;
using System.Web;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Configuration;

namespace AP.API
{
    public class KRAMapping
    {
        #region GetEmployeeDetailsByDepartmentAndRole
        /// <summary>
        /// Get Employee Details By Department and Role
        /// </summary>
        /// <returns>List</returns>
        public List<AssociateProjectData> GetEmployeeDetailsByDepartmentAndRole(int DepartmentID, int RoleMasterID)
        {
            List<AssociateProjectData> employeeList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    employeeList = apEntities.Database.SqlQuery<AssociateProjectData>
                              ("[usp_GetAssociatesByDepartmentAndRole] @DepartmentID,@RoleMasterID",
                             new object[] {
                                        new SqlParameter ("DepartmentID", DepartmentID),
                                        new SqlParameter ("RoleMasterID", RoleMasterID)

                             }).ToList();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Employee details.");
            }
            return employeeList;
        }
        #endregion

        #region GenerateKRAPdfForAllAssociates
        /// <summary>
        /// Generate KRA Pdf For All Associates
        /// </summary>
        public async Task<bool> GenerateKRAPdfForAllAssociates(int overideExisting)
        {
            string employeecode = string.Empty;
            List<KRAPdfData> empDetails = new List<KRAPdfData>() ;
            string filepath = ConfigurationManager.AppSettings["HRMSRepositoryPath"].ToString();

            //GenericType lstFinancialYear = new GenericType(); ;
            FinancialYearData lstFinancialYear = new FinancialYearData();
            try
            {
                using (var apEntities = new APEntities())
                {
                    // Get KRA PDF Configuration Details
                    if(overideExisting == 1)
                    {
                       int rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateIsPDFGenerated]").SingleOrDefaultAsync(); 
                    }
                    KRAPdfConfiguarationData objKRAPdfConfiguarationData = new KRAPdfConfiguarationDetails().GetKRAPdfConfiguarationDetails();
                    if (objKRAPdfConfiguarationData != null)
                    {
                        // Get current financial year
                        lstFinancialYear = await new MasterData().GetCurrentFinancialYear();

                        // Get all the employees with their active role id
                         empDetails = await apEntities.Database.SqlQuery<KRAPdfData>
                              ("[usp_GetAllAssociatesByKRAGroup]",
                               new object[] { }).ToListAsync();

                        //Parallel.ForEach(empDetails, async kRAPdfData =>
                        foreach (KRAPdfData kRAPdfData in empDetails)
                        {

                            employeecode = kRAPdfData.EmployeeCode;
                            AssociateKRAs lstKRAs = await new KRA().GetOrganizationAndCustomKRAsForEmployee(kRAPdfData.EmployeeId, kRAPdfData.KRAGroupID, lstFinancialYear.Id);
                          
                            bool isExists = Directory.Exists(filepath + "/KRA " + lstFinancialYear.Name);

                            if (isExists == false)
                                Directory.CreateDirectory(filepath + "/KRA " + lstFinancialYear.Name);

                           
                            var filePath = filepath + "/KRA " + lstFinancialYear.Name + "/" + kRAPdfData.EmployeeCode + "_" + kRAPdfData.AssociateName + ".pdf";
                            try
                            {
                                using (FileStream memoryStream = new FileStream(filePath, FileMode.Create))
                                {
                                    GenerateKRAPdf(memoryStream, kRAPdfData, objKRAPdfConfiguarationData, lstKRAs);

                                    int rowsAffected = await apEntities.Database.SqlQuery<int>
                                                    ("[usp_UpdateIsPDFGeneratedForEmployee] @EmployeeID", new object[] {
                                        new SqlParameter ("EmployeeID",kRAPdfData.EmployeeId)}).SingleOrDefaultAsync();

                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.KRAPDFWrite("Part of the file not exists for the employee"+ kRAPdfData.EmployeeCode, Log.Severity.Error, "");
                            }

                            // Log every successful transaction

                            //Log.KRAPDFWrite(kRAPdfData.EmployeeCode + " KRA Pdf generated successfully for the financial year -" + lstFinancialYear.Name, Log.Severity.Info, "");
                        }
                        //);
                    }
                    else
                    {
                        new AssociatePortalException("KRA Pdf Configuration details not configured.");
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.KRAPDFWrite("Not able to generate KRA pdf for the employee- " + employeecode + lstFinancialYear.Name, Log.Severity.Info, "");
                throw ex;
            }
            if (empDetails.Count() == 0)
                return false;

            return true;
        }
        #endregion

        #region GenerateKRAPdf
        /// <summary>
        /// Generate KRA Pdf
        /// </summary>
        public void GenerateKRAPdf(FileStream memoryStream, KRAPdfData kRAPdfData, KRAPdfConfiguarationData objKRAPdfConfiguarationData, AssociateKRAs lstKRAs)
        {
            try
            {
                Paragraph para = null;
                Phrase pharse = null;
                PdfPTable table = null;
                PdfPCell cell = null;
                Document document = new Document(PageSize.A4, 60, 60, 40, 80); // PageSize.A4, left, right, top , bottom        
                Font contentFont = new Font(FontFactory.GetFont(BaseFont.HELVETICA, 11, new BaseColor(32, 32, 32)));
                Font tableFont = new Font(FontFactory.GetFont(BaseFont.HELVETICA, 11, new BaseColor(32,32,32)));
                Font contentFontBold = new Font(FontFactory.GetFont(BaseFont.HELVETICA, 11, Font.BOLD));
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                // calling PDFFooter class to Include in document
                writer.PageEvent = new Footer();
                document.Open();

                para = new Paragraph(DateTime.Now.ToString("dd-MMMM-yyyy"), contentFont);
                para.Font = new Font(Font.FontFamily.HELVETICA); 
                
                para.SpacingAfter = 6;
                document.Add(para);

                para = new Paragraph("Dear " + kRAPdfData.AssociateName + ",", contentFont);
                para.SpacingAfter = 8;
                document.Add(para);

                StringReader sr = new StringReader(objKRAPdfConfiguarationData.Section1);
                Paragraph p = new Paragraph(objKRAPdfConfiguarationData.Section1, contentFont);
                p.Alignment = Element.ALIGN_JUSTIFIED;
                document.Add(p);
                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);

                para = new Paragraph();
                para.Font = contentFont;
                para.SpacingAfter = 15;
                document.Add(para);

                table = new PdfPTable(3);

                table.HorizontalAlignment = 0;
                table.TotalWidth = 470f;
                table.LockedWidth = true;
                float[] widths = new float[] { 120f, 300f, 100f };
                table.SetWidths(widths);


                pharse = new Phrase();
                cell = PhraseCell(new Paragraph("KRA Aspect", contentFontBold));
                table.AddCell(cell);

                cell = PhraseCell(new Paragraph("Metric", contentFontBold));
                table.AddCell(cell);

                cell = PhraseCell(new Paragraph("Target", contentFontBold));
                table.AddCell(cell);
                //foreach (var kras in lstKRAs.CustomKRAs)
                //{
                //    cell = PhraseCell(new Phrase(kras.KRAAspectName, contentFont));
                //    table.AddCell(cell);
                //    cell = PhraseCell(new Phrase(kras.KRAAspectMetric, contentFont));
                //    table.AddCell(cell);
                //    cell = PhraseCell(new Phrase(kras.KRAAspectTarget, contentFont));
                //    table.AddCell(cell);
                //}
                string KRAtareget = string.Empty;

                var distinctAspects = lstKRAs.OrganizationKRAs
                          .Select(o => new { o.KRAAspectName })
                          .Distinct();

                // var distinctAspects = lstKRAs.OrganizationKRAs.ToList().Distinct();

                foreach (var aspect in distinctAspects)
                {
                    PdfPCell aspectCell = null;
                    aspectCell = PhraseCell(new Paragraph(aspect.KRAAspectName, tableFont));
                    aspectCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    int count = lstKRAs.OrganizationKRAs.FindAll(k => k.KRAAspectName == aspect.KRAAspectName).Count;
                    aspectCell.Rowspan = count;
                    table.AddCell(aspectCell);
      
                    // inner loop to print 
                    var metricsAndTargets = lstKRAs.OrganizationKRAs.FindAll(k => k.KRAAspectName == aspect.KRAAspectName);
                    foreach(var kra in metricsAndTargets)
                    {
                        cell = PhraseCell(new Paragraph(kra.KRAAspectMetric, tableFont));
                        cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        table.AddCell(cell);

                        if (kra.KRAMeasurementType == "Scale" || kra.KRAMeasurementType == "Number")
                        {
                            string target = kra.KRATargetValue.ToString();
                            int endIndex = 0, value = (target.IndexOf('.')) + 1;
                            endIndex = ((target[value] - '0') > 0) ? 2 : 0;

                            if (kra.KRATargetText != null)
                            {

                                KRAtareget = kra.Operator + kra.KRATargetValue.ToString().Substring(0, kra.KRATargetValue.ToString().IndexOf('.') + endIndex) + " " + kra.KRATargetText + " (" + kra.KRATargetPeriod + ")";
                            }
                            else
                                KRAtareget = kra.Operator + kra.KRATargetValue.ToString().Substring(0, kra.KRATargetValue.ToString().IndexOf('.') + endIndex) + " (" + kra.KRATargetPeriod + ")";

                        }
                        else if (kra.KRAMeasurementType == "Percentage")
                        {
                            if (kra.KRATargetText != null)
                                KRAtareget = kra.Operator + kra.KRATargetValue.ToString().Substring(0, kra.KRATargetValue.ToString().IndexOf('.')) + "% " + kra.KRATargetText + " (" + kra.KRATargetPeriod + ")";
                            else
                                KRAtareget = kra.Operator + kra.KRATargetValue.ToString().Substring(0, kra.KRATargetValue.ToString().IndexOf('.')) + "%  (" + kra.KRATargetPeriod + ")";
                        }
                        cell = PhraseCell(new Paragraph(KRAtareget, tableFont));
                        cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        table.AddCell(cell);

                    }

                }

                document.Add(table);
                para = new Paragraph();
                para.Font = contentFont;
                para.SpacingBefore = 15;
                document.Add(para);
                p = new Paragraph(objKRAPdfConfiguarationData.Section2, contentFont);
                p.Alignment = Element.ALIGN_JUSTIFIED;
                document.Add(p);

                para = new Paragraph("With best wishes,", contentFont);
                para.SpacingBefore = 10;
                para.SpacingAfter = 6;
                document.Add(para);

                string imagePath = ConfigurationManager.AppSettings["DevliveryHeadSign"].ToString();
                Image image = Image.GetInstance(imagePath);
                image.Alignment = Element.ALIGN_LEFT;
                image.ScaleToFit(100f, 100f);
                document.Add(image);

                para = new Paragraph("Yours sincerely,", contentFont);
                para.SpacingAfter = 10;
                document.Add(para);



                para = new Paragraph("Haranath Pinnamaneni", contentFont); //+ " <br/>" + "Head, Technology & Delivery" + "</p>");
                para.Alignment = Element.ALIGN_LEFT;
                document.Add(para);

                para = new Paragraph("Head, Technology & Delivery", contentFont); //+ " <br/>" + "Head, Technology & Delivery" + "</p>");
                para.Alignment = Element.ALIGN_LEFT;
                document.Add(para);



                if (document.IsOpen())
                    document.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region PhraseCell
        /// <summary>
        /// PhraseCell
        /// </summary>
        /// <param name="phrase"></param>
        /// <param name="align"></param>
        /// <returns></returns>
        private static PdfPCell PhraseCell(Phrase phrase, int align = Element.ALIGN_CENTER)
        {
            PdfPCell cell = new PdfPCell(phrase);
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingLeft = 7f;
            cell.PaddingRight = 7f;
            cell.PaddingBottom = 7f;
            cell.PaddingTop = 7f;
            return cell;
        }
        #endregion

        //#region UpdateAssociateRole
        ///// /// <summary>
        ///// Update role in allocation table for delivery or save role for service dept
        ///// </summary>
        ///// <param name="allocation"></param>
        ///// <returns></returns>
        //public async Task<bool> UpdateAssociateRole(AssociateRoleMappingData roleMappingData)
        //{
        //    int rowsAffected = 0;
        //    try
        //    {
        //        using (var apEntities = new APEntities())
        //        {
        //            foreach (var id in roleMappingData.IDs)
        //            {
        //                    using (var trans = apEntities.Database.BeginTransaction())
        //                    {

        //                        rowsAffected = await apEntities.Database.SqlQuery<int>
        //                           ("[usp_InsertUpdateRoleMasterId] @Id, @RoleMasterId, @ProjectId, @DepartmentId, @DateModified, @ModifiedUser, @CreatedDate, @CreatedUser, @SystemInfo",
        //                           new object[] {
        //                                new SqlParameter ("Id", id),
        //                                new SqlParameter ("RoleMasterId", roleMappingData.RoleMasterId),
        //                                new SqlParameter ("ProjectId", (object)roleMappingData.ProjectId??DBNull.Value),
        //                                new SqlParameter ("DepartmentId", roleMappingData.DepartmentId),
        //                                new SqlParameter ("DateModified", DateTime.UtcNow),
        //                                new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
        //                                new SqlParameter ("CreatedDate", DateTime.UtcNow),
        //                                new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
        //                                new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
        //                           }
        //                               ).SingleOrDefaultAsync();

        //                        trans.Commit();
        //                    }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return rowsAffected > 0 ? true : false;
        //}
        //#endregion


        #region UpdateAssociateRole
        /// /// <summary>
        /// Update role in allocation table for delivery or save role for service dept
        /// </summary>
        /// <param name="allocation"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAssociateRole(AssociateRoleMappingData roleMappingData)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    foreach (var id in roleMappingData.IDs)
                    {
                        using (var trans = apEntities.Database.BeginTransaction())
                        {

                            rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_AssociateKRAMappings] @EmployeeID, @KRAGroupID, @FinancialYearID, @CreatedDate, @IsActive",
                               new object[] {
                                        new SqlParameter ("EmployeeID", id),
                                        new SqlParameter ("KRAGroupID", roleMappingData.KRAGroupId),
                                        new SqlParameter ("FinancialYearID", roleMappingData.FinancialYearID),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("IsActive", true),
                               }
                                   ).SingleOrDefaultAsync();

                            trans.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion



       // //GenerateKRAPdfSelectedAllAssociates

        //public async Task<bool> GenerateKRAPdfSelectedAllAssociates(KRAPdfData[] empList )
        // {
        //     string employeecode = string.Empty;
        //     string filepath = ConfigurationManager.AppSettings["HRMSRepositoryPath"].ToString();
        //     FinancialYearData lstFinancialYear = new FinancialYearData();
        //     try
        //     {
        //         KRAPdfConfiguarationData objKRAPdfConfiguarationData = new KRAPdfConfiguarationDetails().GetKRAPdfConfiguarationDetails();
        //         if (objKRAPdfConfiguarationData != null)
        //         {
        //             // Get current financial year
        //             lstFinancialYear = await new MasterData().GetCurrentFinancialYear();

        //             //Parallel.ForEach(empDetails, async kRAPdfData =>
        //             foreach (KRAPdfData kRAPdfData in empList)
        //             {
        //                 employeecode = kRAPdfData.EmployeeCode;
        //                 AssociateKRAs lstKRAs = await new KRA().GetOrganizationAndCustomKRAsForEmployee(kRAPdfData.EmployeeId, kRAPdfData.KRAGroupID, lstFinancialYear.Id);

        //                 bool isExists = Directory.Exists(filepath + "/KRA " + lstFinancialYear.Name);

        //                 if (isExists == false)
        //                     Directory.CreateDirectory(filepath + "/KRA " + lstFinancialYear.Name);


        //                 var filePath = filepath + "/KRA " + lstFinancialYear.Name + "/" + kRAPdfData.EmployeeCode + "_" + kRAPdfData.AssociateName + ".pdf";
        //                 try
        //                 {
        //                     using (FileStream memoryStream = new FileStream(filePath, FileMode.Create))
        //                     {
        //                         GenerateKRAPdf(memoryStream, kRAPdfData, objKRAPdfConfiguarationData, lstKRAs);
        //                     }
        //                 }
        //                 catch (Exception ex)
        //                 {
        //                     //Log.KRAPDFWrite("Part of the file not exists for the employee"+ kRAPdfData.EmployeeCode, Log.Severity.Error, "");
        //                 }

        //                 // Log every successful transaction

        //                 //Log.KRAPDFWrite(kRAPdfData.EmployeeCode + " KRA Pdf generated successfully for the financial year -" + lstFinancialYear.Name, Log.Severity.Info, "");
        //             }
        //             //);
        //         }
        //         else
        //         {
        //             new AssociatePortalException("KRA Pdf Configuration details not configured.");
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         //Log.KRAPDFWrite("Not able to generate KRA pdf for the employee- " + employeecode + lstFinancialYear.Name, Log.Severity.Info, "");
        //         throw ex;
        //     }

        //     return true;
        // }
    }
}

