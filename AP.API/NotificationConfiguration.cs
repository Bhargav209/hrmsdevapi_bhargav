﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class NotificationConfiguration
    {
        #region GetNotificationConfigurationDetailsByNotificationTypeId
        /// <summary>
        /// Gets notification configuration details by notification type id
        /// </summary>
        /// <param name="notificationTypeID"></param>
        /// <returns></returns>
        public async Task<EmailNotificationConfiguration> GetNotificationConfigurationDetailsByNotificationTypeId(int notificationTypeID, int categoryId)
        {
            EmailNotificationConfiguration notificationConfiguration;
            try
            {
                using (var apEntities = new APEntities())
                {
                    notificationConfiguration = await apEntities.Database.SqlQuery<EmailNotificationConfiguration>
                              ("[usp_GetNotificationConfigurationByNotificationTypeID] @NotificationTypeID, @CategoryId",
                                new object[] {
                                    new SqlParameter("NotificationTypeID", notificationTypeID),
                                    new SqlParameter("CategoryId", categoryId)
                                        }).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return notificationConfiguration;
        }
        #endregion

        #region GetEmployeeWorkEmails
        /// <summary> 
        /// Gets list of work emails of associates by search string
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetEmployeeWorkEmails(string searchString)
        {
            List<string> lstEmails;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstEmails = await apEntities.Database.SqlQuery<string>
                              ("[usp_GetEmployeeWorkEmails] @SearchString",
                                new object[] {
                                    new SqlParameter("SearchString", searchString)
                                        }).ToListAsync();
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstEmails;
        }
        #endregion

        #region CreateNotificationConfiguration
        /// <summary>
        /// Create a new notification configuration
        /// </summary>
        /// <param name="notificationConfigurationDetails"></param>
        /// <returns></returns>
        public async Task<bool> CreateNotificationConfiguration(EmailNotificationConfiguration notificationConfigurationDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateNotificationConfiguration] @NotificationTypeID, @CategoryId, @EmailFrom, @EmailTo, @EmailCC, @Subject, @Body, @CreatedUser, @SystemInfo",
                                   new object[] {
                                        new SqlParameter ("NotificationTypeID", notificationConfigurationDetails.NotificationTypeID),
                                        new SqlParameter ("CategoryId", notificationConfigurationDetails.CategoryId),
                                        new SqlParameter ("EmailFrom", notificationConfigurationDetails.EmailFrom),
                                        new SqlParameter ("EmailTo", notificationConfigurationDetails.EmailTo),
                                        new SqlParameter ("EmailCC", notificationConfigurationDetails.EmailCC),
                                        new SqlParameter ("Subject", notificationConfigurationDetails.EmailSubject),
                                        new SqlParameter ("Body", notificationConfigurationDetails.EmailContent),
                                        new SqlParameter ("CreatedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region UpdateNotificationConfiguration
        /// <summary>
        /// Update a notification configuration
        /// </summary>
        /// <param name="notificationConfigurationDetails"></param>
        /// <returns></returns>
        public async Task<bool> UpdateNotificationConfiguration(EmailNotificationConfiguration notificationConfigurationDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateNotificationConfiguration] @NotificationTypeID, @CategoryId, @EmailFrom, @EmailTo, @EmailCC, @Subject, @Body, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("NotificationTypeID", notificationConfigurationDetails.NotificationTypeID),
                                        new SqlParameter ("CategoryId", notificationConfigurationDetails.CategoryId),
                                        new SqlParameter ("EmailFrom", notificationConfigurationDetails.EmailFrom),
                                        new SqlParameter ("EmailTo", notificationConfigurationDetails.EmailTo),
                                        new SqlParameter ("EmailCC", notificationConfigurationDetails.EmailCC),
                                        new SqlParameter ("Subject", notificationConfigurationDetails.EmailSubject),
                                        new SqlParameter ("Body", notificationConfigurationDetails.EmailContent),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected > 0 ? true : false;
        }

        #region GetNotificationTypeByCategory
        /// <summary>
        /// Gets list of notification types by Category
        /// </summary>
        /// <returns></returns>
        public async Task<List<GenericType>> GetNotificationType(int categoryId)
        {
            List<GenericType> lstNotificationType;
            try
            {
                using (var apEntities = new APEntities())
                {

                    lstNotificationType = await apEntities.Database.SqlQuery<GenericType>
                            ("[usp_GetNotificationTypeByCategoryId] @CategoryId",

                             new object[] {
                                        new SqlParameter ("CategoryId", categoryId)
                           }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstNotificationType;
        }
        #endregion

        #endregion
    }
}
