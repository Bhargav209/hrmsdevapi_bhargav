﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using static AP.Utility.Enumeration;

namespace AP.API
{
    public class AssociateEducationDetails
    {
        #region Education Tab Methods        

        #region UpdateEducationDetails
        /// <summary>
        /// Save or update Education Details
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool SaveEducationDetails(UserDetails details)
        {
            bool retValue = false;
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    IEnumerable<object> duplicateEducationDetails = apEntities.EducationDetails.Where(id => id.EmployeeId == details.empID && id.IsActive == true).ToList();
                    foreach (EducationDetail educationDetail in duplicateEducationDetails)
                    {
                        educationDetail.IsActive = false;
                        retValue = apEntities.SaveChanges() > 0 ? true : false;
                    }

                    foreach (EducationDetails ed in details.qualifications)
                    {
                        EducationDetail educationDetails = apEntities.EducationDetails.SingleOrDefault(x => x.ID == ed.ID);

                        if (educationDetails != null)
                        {
                            educationDetails.EducationalQualification = ed.qualificationName;
                            educationDetails.AcadamicCompletedYear = Convert.ToDateTime(ed.completedYear);
                            educationDetails.NameofInstitution = ed.institution;
                            educationDetails.Specialization = ed.specialization;
                            educationDetails.ProgramType = Enum.GetName(typeof(ProgramType), ed.programTypeID);
                            educationDetails.Grade = ed.grade;
                            educationDetails.Marks_ = ed.marks;
                            educationDetails.IsActive = true;
                            educationDetails.ModifiedDate = DateTime.Now;
                            educationDetails.ModifiedUser = details.CurrentUser;
                            educationDetails.SystemInfo = details.SystemInfo;
                            apEntities.Entry(educationDetails).State = System.Data.Entity.EntityState.Modified;
                            retValue = apEntities.SaveChanges() > 0 ? true : false;
                        }
                        else
                        {
                            educationDetails = new EducationDetail();
                            educationDetails.EmployeeId = details.empID;
                            educationDetails.EducationalQualification = ed.qualificationName;
                            educationDetails.AcadamicCompletedYear = Convert.ToDateTime(ed.completedYear);
                            educationDetails.NameofInstitution = ed.institution;
                            educationDetails.Specialization = ed.specialization;
                            educationDetails.ProgramType = Enum.GetName(typeof(ProgramType), ed.programTypeID);
                            educationDetails.Grade = ed.grade;
                            educationDetails.Marks_ = ed.marks;
                            educationDetails.IsActive = true;
                            educationDetails.CreatedDate = DateTime.Now;
                            educationDetails.CreatedUser = details.CurrentUser;
                            educationDetails.SystemInfo = details.SystemInfo;
                            apEntities.EducationDetails.Add(educationDetails);
                            retValue = apEntities.SaveChanges() > 0 ? true : false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to save education details.");
            }

            return retValue;
        }
        #endregion

        #region GetEducationDetailsByID
        /// <summary>
        /// GetEducationDetailsByID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public IEnumerable<object> GetEducationDetailsByID(int empID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {                  

                    var isEmployeeExist = hrmsEntities.EducationDetails
                                          .Where(x => x.EmployeeId == empID )
                                           .Select(x => new { x.EmployeeId}).ToList().Count;
                    if (isEmployeeExist > 0)
                    {
                        var getEducationDetails = (from ed in hrmsEntities.EducationDetails.AsEnumerable()
                                                   where ed.EmployeeId == empID && ed.IsActive == true
                                                   select new EducationDetails
                                                   {
                                                       ID = ed.ID,
                                                       empID = ed.EmployeeId,
                                                       programTypeID = (int)Enum.Parse(typeof(ProgramType), ed.ProgramType),
                                                       programType = ed.ProgramType,
                                                       qualificationName = ed.EducationalQualification,
                                                       yearCompleted = ed.AcadamicCompletedYear,
                                                       institution = ed.NameofInstitution,
                                                       specialization = ed.Specialization,
                                                       grade = ed.Grade,
                                                       marks = ed.Marks_,
                                                       completedYear = string.Format("{0:MM/yyyy}", ed.AcadamicCompletedYear)
                                                   }).ToList();


                        if (getEducationDetails.Count > 0)
                            return getEducationDetails;

                        else
                            return Enumerable.Empty<EducationDetails>().ToList();
                    }
                    else
                    {
                        return Enumerable.Empty<EducationDetails>().ToList();
                    }

                }
            }
            catch(Exception ex)
            {
                throw new AssociatePortalException("Failed to get education details.");
            }
        }
        #endregion        

        #endregion
    }
}
