﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class BayDetails
    {
        /// <summary>
        /// Get Bays List
        /// </summary>
        /// <returns></returns>
       
        public async Task<List<BayInformation>> GetBaysList()
        {
            List<BayInformation> lstBays;

            try
            {
                using (var apEntities = new APEntities())
                {
                    lstBays = await apEntities.Database.SqlQuery<BayInformation>
                              ("[usp_GetBays]").ToListAsync();

                    lstBays.OrderBy(c => c.Name);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstBays;
        }



        /// <summary>
        /// Create Work stations
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CreateWorkStation(WorkStation workStation)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {

                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateBayStaion] @WorkStationId,@BayId, @CreatedUser, @SystemInfo,@IsOccupied",
                                   new object[] {
                                        new SqlParameter ("WorkStationId", workStation.WorkStationId),
                                        new SqlParameter ("BayId", workStation.BayId),
                                        new SqlParameter ("CreatedUser",  HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress()),
                                        new SqlParameter ("IsOccupied", workStation.IsOccupied),
                                   }
                                   ).SingleOrDefaultAsync();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a  Work station");
            }
            return rowsAffected > 0 ? true : false;
        }
    }
}
