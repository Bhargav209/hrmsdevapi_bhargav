﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP.API
{
    public class ErrorLog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorLog"></param>
        /// <returns></returns>
        public async Task<bool> AddErrorLog(ErrorLogDetails errorLog, int type)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_LogError] @FileName, @ErrorMessage, @CreatedDate, @Type",
                                   new object[] {
                                        new SqlParameter ("FileName", errorLog.FileName),
                                        new SqlParameter ("ErrorMessage", errorLog.ErrorMessage),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("Type", type)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while logging error log details");
            }
            return rowsAffected > 0 ? true : false;
        }
    }
}
