﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using AP.DataStorage;
using AP.Utility;

namespace AP.API
{
   public class DesignationDetails
   {
      #region SaveDesignationDetails
      /// <summary>
      /// Saves the designation details.
      /// </summary>
      /// <param name="designationData"></param>
      /// <returns></returns>
      public bool SaveDesignationDetails(DesignationData designationData)
      {
         int assignedDesignationsCount = 0;
         using (APEntities hrmsEntities = new APEntities())
         {
            IQueryable<Designation> designationQuery = hrmsEntities.Designations.Where(
                                                         ur => ur.DesignationCode.Trim() == designationData.DesignationCode.Trim() &&
                                                         ur.IsActive == true
                                                      );
            assignedDesignationsCount = designationQuery.Count();

            if (assignedDesignationsCount > 0)
            {
               throw new AssociatePortalException("Designation Code already exists.");
            }

            int gradeID = (from c in hrmsEntities.Grades
                           where c.GradeCode == designationData.GradeCode
                           select c.GradeId).FirstOrDefault();

            Designation designation = new Designation();
            designation.DesignationCode = designationData.DesignationCode;
            designation.DesignationName = designationData.DesignationName;
            designation.IsActive = true;
            designation.GradeId = gradeID;
            designation.CreatedUser = designationData.CurrentUser;
            designation.CreatedDate = DateTime.Now;
            designation.SystemInfo = designationData.SystemInfo;
            hrmsEntities.Designations.Add(designation);

            if (hrmsEntities.SaveChanges() <= 0)
            {
               throw new AssociatePortalException("Failed to save designation details.");
            }
         }

         return true;
      }
      #endregion

      #region UpdateDesignationDetails
      /// <summary>
      /// Updates designation details
      /// </summary>
      /// <param name="designationData"></param>
      /// <returns></returns>
      public bool UpdateDesignationDetails(DesignationData designationData)
      {

         using (APEntities hrmsEntities = new APEntities())
         {

            int isExists = (from desig in hrmsEntities.Designations
                            where desig.DesignationCode == designationData.DesignationCode && desig.DesignationId != designationData.DesignationId
                            select desig).Count();
            if (isExists > 0)
               throw new AssociatePortalException("Designation code already exist");

            int gradeID = (from c in hrmsEntities.Grades
                           where c.GradeCode == designationData.GradeCode
                           select c.GradeId).FirstOrDefault();

            Designation designation = hrmsEntities.Designations.FirstOrDefault(dsg => dsg.DesignationId == designationData.DesignationId);
            designation.GradeId = gradeID;
            designation.DesignationCode = designationData.DesignationCode;
            designation.DesignationName = designationData.DesignationName;
                designation.IsActive = true;
            designation.ModifiedUser = designationData.CurrentUser;
            designation.ModifiedDate = DateTime.Now;
            designation.SystemInfo = designationData.SystemInfo;
            hrmsEntities.Entry(designation).State = System.Data.Entity.EntityState.Modified;

            if (hrmsEntities.SaveChanges() <= 0)
            {
               throw new AssociatePortalException("Failed to save designation details.");
            }
            return true;
         }
      }

      #endregion

      #region GetDesignationDetails
      /// <summary>
      /// Gets designation details
      /// </summary>
      /// <param name="isActive"></param>
      /// <returns></returns>
      public IEnumerable<object> GetDesignationDetails(bool isActive)
      {
         try
         {
            using (APEntities hrmsEntities = new APEntities())
            {
               //var competencyAreasList = hrmsEntities.Designations.Select(c =>
               //  new { ID = c.DesignationId, Name = c.DesignationCode, c.DesignationId, c.DesignationCode, c.DesignationName, c.IsActive }).OrderBy(d => d.DesignationCode).ToList();

               var competencyAreasList = (from designation in hrmsEntities.Designations
                                          join grade in hrmsEntities.Grades on designation.GradeId equals grade.GradeId
                                          where designation.IsActive == true && grade.IsActive == true
                                          select new
                                          {
                                             ID = designation.DesignationId,
                                             Name = designation.DesignationCode,
                                             designation.DesignationId,
                                             designation.DesignationCode,
                                             designation.DesignationName,
                                             IsActive = designation.IsActive == true ? "Yes" : "No",
                                             grade.GradeId,
                                             grade.GradeCode
                                          }).ToList();

               if (isActive == true)
                  competencyAreasList = competencyAreasList.Where(i => i.IsActive == "Yes").ToList();

                    if (competencyAreasList.Count > 0)
                        return competencyAreasList.OrderBy(d => d.DesignationName);

                    else
                        return Enumerable.Empty<object>().ToList();

            }
         }
         catch
         {
            throw new AssociatePortalException("Failed to get designation details.");
         }
      }
      #endregion
   }
}
