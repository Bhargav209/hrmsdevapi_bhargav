﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Globalization;
using System.Web;

namespace AP.API
{
    public class AssociateFamilyDetails
    {
        #region Family Tab Methods

        #region UpdateFamilyDetails
        /// <summary>
        /// UpdateFamilyDetails
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool UpdateFamilyDetails(UserDetails details)
        {
            bool retValue = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var duplicateFamilyDetails = hrmsEntities.FamilyDetails.Where(d => d.EmployeeId == details.empID).ToList();

                    foreach (FamilyDetail detail in duplicateFamilyDetails)
                    {
                        detail.IsActive = false;
                        hrmsEntities.SaveChanges();
                    }

                    foreach (var relationDetails in details.relationsInfo)
                    {
                        FamilyDetail fd = hrmsEntities.FamilyDetails.SingleOrDefault(x => x.ID == relationDetails.ID && x.EmployeeId == details.empID);
                        if (!Object.ReferenceEquals(fd, null))
                        {
                            fd.Name = relationDetails.EncryptedName;
                            fd.EmployeeId = details.empID;
                            fd.DateofBirth = Commons.GetDateTimeInIST(relationDetails.dob);
                            fd.Relationship = relationDetails.relationship;
                            fd.Occupation = relationDetails.occupation;
                            fd.IsActive = true;
                            fd.ModifiedUser = HttpContext.Current.User.Identity.Name;
                            fd.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntities.Entry(fd).State = EntityState.Modified;
                            retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                        else
                        {
                            fd = new FamilyDetail();
                            fd.Name = relationDetails.EncryptedName;
                            fd.EmployeeId = details.empID;
                            fd.DateofBirth = Commons.GetDateTimeInIST(relationDetails.dob);
                            fd.Relationship = relationDetails.relationship;
                            fd.Occupation = relationDetails.occupation;
                            fd.IsActive = true;
                            fd.CreatedUser = HttpContext.Current.User.Identity.Name;
                            fd.SystemInfo = Commons.GetClientIPAddress();
                            hrmsEntities.FamilyDetails.Add(fd);
                            retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                        }
                    }

                    List<EmergencyContactData> ecd = new List<EmergencyContactData>();
                    if (details.contactDetailsOne != null)
                        ecd.Add(details.contactDetailsOne);
                    if (details.contactDetailsTwo != null)
                        ecd.Add(details.contactDetailsTwo);

                    foreach (EmergencyContactData contactData in ecd)
                    {
                        if (!Object.ReferenceEquals(contactData, null))
                        {
                            EmergencyContactDetail contact = hrmsEntities.EmergencyContactDetails.SingleOrDefault(x => x.EmployeeId == details.empID && x.EmergencyContactId == contactData.ID);
                            if (!Object.ReferenceEquals(contact, null))
                            {
                                contact.ContactName = contactData.contactName;
                                contact.Relationship = contactData.relationship;
                                contact.AddressLine1 = contactData.addressLine1;
                                contact.AddressLine2 = contactData.addressLine2;
                                contact.IsPrimary = contactData.isPrimary;
                                contact.City = contactData.city;
                                contact.State = contactData.state;
                                contact.Country = contactData.country;
                                contact.PostalCode = contactData.zip;
                                contact.TelephoneNo = contactData.EncryptedTelePhoneNo;
                                contact.MobileNo = contactData.EncryptedMobileNo;
                                contact.EmailAddress = contactData.emailAddress;
                                contact.ContactType = (bool)contactData.isPrimary ? "PrimaryContact" : string.Empty;
                                contact.ModifiedDate = DateTime.Now;
                                contact.ModifiedUser = details.CurrentUser;
                                contact.SystemInfo = details.SystemInfo;
                                hrmsEntities.Entry(contact).State = EntityState.Modified;
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                            else
                            {
                                contact = new EmergencyContactDetail();
                                contact.EmployeeId = details.empID;
                                contact.ContactName = contactData.contactName;
                                contact.Relationship = contactData.relationship;
                                contact.AddressLine1 = contactData.addressLine1;
                                contact.AddressLine2 = contactData.addressLine2;
                                contact.IsPrimary = contactData.isPrimary;
                                contact.City = contactData.city;
                                contact.State = contactData.state;
                                contact.Country = contactData.country;
                                contact.PostalCode = contactData.zip;
                                contact.TelephoneNo = contactData.EncryptedTelePhoneNo;
                                contact.MobileNo = contactData.EncryptedMobileNo;
                                contact.EmailAddress = contactData.emailAddress;
                                contact.ContactType = (bool)contactData.isPrimary ? "PrimaryContact" : string.Empty;
                                contact.CreatedDate  = DateTime.Now;
                                contact.CreatedUser = details.CurrentUser;
                                contact.SystemInfo = details.SystemInfo;
                                hrmsEntities.EmergencyContactDetails.Add(contact);
                                retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to save/update family details.");
            }

            return retValue;
        }
        #endregion

        #region GetFamilyDetailsByID
        /// <summary>
        /// Gets Family details by ID
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public UserDetails GetFamilyDetailsByID(int empID)
        {
            UserDetails userDetails = null;

            try
            {
                userDetails = new UserDetails();

                using (APEntities hrmsEntities = new APEntities())
                {
                    var getFamilyDetails = (from fd in hrmsEntities.FamilyDetails.AsEnumerable()
                                            where fd.EmployeeId == empID && fd.IsActive == true
                                            select new RelationDetails
                                            {
                                                ID = fd.ID,
                                                EncryptedName = fd.Name,
                                                empID = fd.EmployeeId,
                                                dob = fd.DateofBirth,
                                                relationship = fd.Relationship,
                                                occupation = fd.Occupation,
                                                birthDate = string.Format("{0:dd/MM/yyyy}", fd.DateofBirth)
                                            }).ToList();

                    var contactDetailsOne = (from ecd in hrmsEntities.EmergencyContactDetails
                                             where ecd.EmployeeId == empID
                                             select new EmergencyContactData
                                             {
                                                 ID = ecd.EmergencyContactId,
                                                 employeeID = ecd.EmployeeId,
                                                 contactName = ecd.ContactName,
                                                 contactType = ecd.ContactType,
                                                 relationship = ecd.Relationship,
                                                 addressLine1 = ecd.AddressLine1,
                                                 addressLine2 = ecd.AddressLine2,
                                                 EncryptedTelePhoneNo = ecd.TelephoneNo,
                                                 EncryptedMobileNo = ecd.MobileNo,
                                                 emailAddress = ecd.EmailAddress,
                                                 city = ecd.City,
                                                 state = ecd.State,
                                                 country = ecd.Country,
                                                 zip = ecd.PostalCode,
                                                 isPrimary = ecd.IsPrimary
                                             }).Take(3).ToList();

                    userDetails.relationsInfo = getFamilyDetails;
                    userDetails.contactDetailsOne = contactDetailsOne.Count >= 1 ? contactDetailsOne[0] : null;
                    userDetails.contactDetailsTwo = contactDetailsOne.Count >= 2 ? contactDetailsOne[1] : null;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get family details.");
            }

            return userDetails;
        }
        #endregion

        #endregion
    }
}
