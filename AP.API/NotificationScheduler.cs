﻿using System;
using System.Linq;
using System.Resources;
using AP.DataStorage;
using AP.Utility;
using Quartz;
using Quartz.Impl;
using System.Text;
using AP.DomainEntities;

namespace AP.API
{
    public static class NotificationScheduler
    {
        public static IScheduler scheduler;

        #region StartScheduler
        /// <summary>
        /// Method to start Quartz Scheduler
        /// </summary>
        /// 
        public static void StartScheduler()
        {

            StdSchedulerFactory stdSchedulerFactory = new StdSchedulerFactory();

            try
            {
                stdSchedulerFactory.Initialize();
                scheduler = stdSchedulerFactory.GetScheduler();

                scheduler.Start();
                StartAllStartedJobs();
                AddJob<ProspectiveAssociateNotificationJob>("PAJ", "Prospective Associate Joining Notification", 0);
            }
            catch (Exception ex)
            {
                Log.Write("Start scheduler error", Log.Severity.Warning, "stdSchedulerFactory =" + stdSchedulerFactory + "scheduler =" + scheduler);
                throw new AssociatePortalException("Failed to start scheduler." + ex.Message);
            }
        }
        #endregion

        #region AddJob
        /// <summary>
        /// Method to schedule jobs
        /// </summary>
        /// <typeparam name="TJob"></typeparam>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        /// 
        public static bool AddJob<TJob>(string jobId, string groupId, int interval) where TJob : IJob
        {
            bool retValue = false;

            try
            {
                if (interval != 0)
                {
                    UserDetails user = new AssociatePersonalDetails().GetAssociateDetailsForNotification(jobId);

                    if (null == user) return retValue;

                    IJobDetail job = JobBuilder.Create<TJob>().WithIdentity(jobId, groupId)
                        .UsingJobData("AssociateId", jobId)
                        .UsingJobData("AssociateName", user.name)
                        .UsingJobData("Designation", user.designation)
                        .UsingJobData("Department", user.department)
                        .UsingJobData("Mobile", user.mobileNo)
                        .Build();

                    if (typeof(TJob) != typeof(ProspectiveAssociateNotificationJob))
                    {
                        ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity(jobId, groupId).ForJob(job)
                               .StartNow()
                               .WithSimpleSchedule(x => x
                                   .WithIntervalInMinutes(interval)
                                   .RepeatForever())
                               .Build();

                        if (!scheduler.CheckExists(job.Key))
                        {
                            scheduler.ScheduleJob(job, trigger);
                            retValue = true;
                        }
                    }
                    else
                    {
                        ITrigger paTrigger = TriggerBuilder.Create()
                       .WithIdentity(jobId, groupId).ForJob(job)
                       //.StartNow() // for demo we need to uncomment this
                       .WithSchedule(CronScheduleBuilder.MonthlyOnDayAndHourAndMinute(1, 9, 00))
                       .Build();

                        if (!scheduler.CheckExists(job.Key))
                        {
                            scheduler.ScheduleJob(job, paTrigger);
                            retValue = true;
                        }
                    }
                }
                else
                {
                    string errMessage = "Notification is not added as it has interval value as 0";
                    string parms = new StringBuilder().AppendFormat("[NotificationScheduler.AddJob : JobID - {0}, GroupID - {1},Interval - {2},Message - {3}]", jobId, groupId, interval, errMessage).ToString();
                    Log.Write("List of not added jobs into the scheduler", Log.Severity.Warning, parms);
                }
            }
            catch
            {
                throw;
            }

            return retValue;
        }
        #endregion

        #region RemoveJob
        /// <summary>
        /// Method to unschedule/ delete job
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// 
        public static bool RemoveJob(string jobId, string groupId)
        {
            try
            {
                return scheduler.DeleteJob(JobKey.Create(jobId, groupId));
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region StartAllStartedJobs
        /// <summary>
        /// Method to start all the jobs which are started on application start
        /// </summary>
        /// 
        public static void StartAllStartedJobs()
        {
            try
            {
                APEntities hrmEntities = new APEntities();
                ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);
                string statusStarted = resourceManager.GetString("Started");

                var jobsToBeStarted = hrmEntities.JobDetails.Where(j => j.Status == statusStarted).ToList();

                foreach (JobDetail j in jobsToBeStarted)
                {
                    string jobId = (j.JobCode).ToString();
                    string groupId = j.GroupID;
                    int interval = j.JobInterval.GetValueOrDefault();

                    if (groupId == resourceManager.GetString("ProfileCreationNotification"))
                    {
                        AddJob<ProfileCreationNotificationJob>(jobId, groupId, interval);
                    }
                    else if (groupId == resourceManager.GetString("ITNotification"))
                    {
                        //AddJob<ITNotificationJob>(jobId, groupId, interval);
                    }
                    else if (groupId == resourceManager.GetString("FinanceNotification"))
                    {
                        AddJob<FinanceNotificationJob>(jobId, groupId, interval);
                    }
                    else if (groupId == resourceManager.GetString("AdminNotification"))
                    {
                        //AddJob<AdminNotificationJob>(jobId, groupId, interval);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("StartAllStartedJobs method", Log.Severity.Warning, ex.Message);
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region AddJob
        /// <summary>
        /// Method to schedule jobs
        /// </summary>
        /// <typeparam name="TJob"></typeparam>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>     
        /// <returns></returns>
        /// 
        //public static bool AddJob<TJob>(string year, string scheduleDate, string employeeCode) where TJob : IJob
        //{
        //    bool retValue = false;

        //    try
        //    {
        //        IJobDetail job = JobBuilder.Create<TJob>().WithIdentity(year, scheduleDate).UsingJobData("CurrentYear", year).UsingJobData("EmployeeCode", employeeCode).Build();

        //        if (typeof(TJob) == typeof(KRA))
        //        {
        //            DateTime mydate = Convert.ToDateTime(scheduleDate);
        //            ITrigger trigger = TriggerBuilder.Create()
        //                   .WithIdentity(year, scheduleDate).ForJob(job)
        //                   .StartAt(mydate)
        //                   .Build();

        //            if (!scheduler.CheckExists(job.Key))
        //            {
        //                scheduler.ScheduleJob(job, trigger);
        //                retValue = true;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return retValue;
        //}
        #endregion
    }
}
