﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;

namespace AP.API
{
    public class ProspectiveAssociateDetails
    {
        #region PA Methods

        #region AddPADetails
        /// <summary>
        /// Method to Add PA Details
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        public bool AddPADetails(UserDetails userDetails)
        {
            bool retValue = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                if (!string.IsNullOrEmpty(userDetails.personalEmail) && userDetails.ID == 0)
                {
                    ProspectiveAssociate associate = hrmsEntities.ProspectiveAssociates.Where(email => email.PersonalEmailAddress == userDetails.personalEmail && email.IsActive == true).FirstOrDefault();

                    if (associate != null)
                    {
                        throw new AssociatePortalException("Email already exists.");
                    }

                }

                int departmentId = hrmsEntities.Departments.Where(d => d.DepartmentCode.Contains("Training")).FirstOrDefault().DepartmentId;
                int practiceAreaId = hrmsEntities.PracticeAreas.Where(p => p.PracticeAreaCode.Contains("Training")).FirstOrDefault().PracticeAreaId;

                ProspectiveAssociate prospectiveAssociate = hrmsEntities.ProspectiveAssociates.Where(n => (n.FirstName == userDetails.firstName && n.LastName == userDetails.lastName && n.MobileNo == userDetails.EncryptedMobileNo && n.IsActive == true)).FirstOrDefault();

                if (prospectiveAssociate == null)
                {
                    prospectiveAssociate = new ProspectiveAssociate();

                    prospectiveAssociate.MobileNo = userDetails.EncryptedMobileNo;
                    prospectiveAssociate.JoinDate = Commons.GetDateTimeInIST(userDetails.dateOfJoining);
                    prospectiveAssociate.FirstName = userDetails.firstName;
                    prospectiveAssociate.MiddleName = userDetails.middleName;
                    prospectiveAssociate.LastName = userDetails.lastName;
                    prospectiveAssociate.Gender = userDetails.gender;
                    prospectiveAssociate.MaritalStatus = userDetails.maritalStatus;
                    prospectiveAssociate.PersonalEmailAddress = userDetails.personalEmail;
                    prospectiveAssociate.JoiningStatusId = userDetails.joiningStatusID;
                    prospectiveAssociate.GradeId = userDetails.gradeID;
                    prospectiveAssociate.DesignationId = userDetails.designationID;
                    prospectiveAssociate.EmploymentType = userDetails.employmentType;
                    prospectiveAssociate.Technology = userDetails.technology;
                    prospectiveAssociate.TechnologyID = (departmentId == userDetails.deptID) ? practiceAreaId  : userDetails.technologyID; //Here TechnologyID refers to PracticeAreaId
                    prospectiveAssociate.DepartmentId = userDetails.deptID;
                    prospectiveAssociate.HRAdvisorName = userDetails.hrAdvisor;
                    prospectiveAssociate.ManagerId = userDetails.managerId;
                    prospectiveAssociate.BGVStatusId = userDetails.bgvStatusID;
                    prospectiveAssociate.IsActive = userDetails.IsActive;
                    prospectiveAssociate.CreatedDate = DateTime.Now;
                    prospectiveAssociate.CreatedUser = userDetails.CurrentUser;
                    prospectiveAssociate.IsActive = true;
                    prospectiveAssociate.SystemInfo = userDetails.SystemInfo;
                    prospectiveAssociate.RecruitedBy = userDetails.RecruitedBy;
                    hrmsEntities.ProspectiveAssociates.Add(prospectiveAssociate);
                    retValue = hrmsEntities.SaveChanges() > 0 ? true : false;

                }

                else
                {
                    throw new AssociatePortalException("User already exists with same name and mobile number.");
                }

            }

            if (retValue == false)
            {
                throw new AssociatePortalException("Failed to save Prospective Associate details");
            }
            return retValue;
        }
        #endregion

        #region GetPADetailsForTheMonth
        /// <summary>
        /// GetPADetailsForTheMonth
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetPADetailsForTheMonth()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    DateTime now = DateTime.Now;
                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);

                    var getAddAssociateDetails = (from pa in hrmsEntities.ProspectiveAssociates
                                                  join d in hrmsEntities.Departments on pa.DepartmentId equals d.DepartmentId
                                                  join des in hrmsEntities.Designations on pa.DesignationId equals des.DesignationId
                                                  where pa.IsActive == true && (pa.JoinDate >= startDate && pa.JoinDate <= endDate)
                                                  select new UserDetails
                                                  {
                                                      ID = pa.ID,
                                                      name = pa.FirstName + " " + pa.LastName,
                                                      firstName = pa.FirstName,
                                                      lastName = pa.LastName,
                                                      middleName = pa.MiddleName,
                                                      gender = pa.Gender,
                                                      maritalStatus = pa.MaritalStatus,
                                                      EncryptedMobileNo = pa.MobileNo,
                                                      personalEmail = pa.PersonalEmailAddress,
                                                      joiningStatusID = pa.JoiningStatusId,
                                                      gradeID = pa.GradeId,
                                                      employmentType = pa.EmploymentType,
                                                      technology = pa.Technology,
                                                      technologyID = pa.TechnologyID,
                                                      designationID = pa.DesignationId,
                                                      designation = des.DesignationName,
                                                      deptID = pa.DepartmentId,
                                                      department = d.Description,
                                                      joiningDate = pa.JoinDate == null ? null : SqlFunctions.DateName("day", pa.JoinDate) + "/" + SqlFunctions.StringConvert((double)pa.JoinDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pa.JoinDate),
                                                      hrAdvisor = pa.HRAdvisorName,
                                                      bgvStatusID = pa.BGVStatusId,
                                                      RecruitedBy = pa.RecruitedBy,
                                                      empID = pa.EmployeeID ?? default(int),
                                                      managerId = pa.ManagerId ?? default(int)
                                                  }).OrderByDescending(i => i.ID).ToList();

                    return getAddAssociateDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Prospective Associate details");
            }
        }
        #endregion

        #region GetPAStatus
        /// <summary>
        /// GetPAStatus
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public int GetPAStatus(int empID)
        {
            int status = 0;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    Employee employee = hrmsEntities.Employees.Where(emp => emp.EmployeeId == empID).FirstOrDefault();
                    if (employee != null)
                    {
                        status = employee.StatusId.GetValueOrDefault();
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Prospective Associate status");
            }

            return status;
        }
        #endregion

        #region GetPADetails
        /// <summary>
        /// GetPADetails
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetPADetails()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAddAssociateDetails = (from pa in hrmsEntities.ProspectiveAssociates
                                                  join d in hrmsEntities.Departments on pa.DepartmentId equals d.DepartmentId into g1
                                                  from dep in g1.DefaultIfEmpty()
                                                  join des in hrmsEntities.Designations on pa.DesignationId equals des.DesignationId into g2
                                                  from des in g2.DefaultIfEmpty()
                                                  join tech in hrmsEntities.PracticeAreas on pa.TechnologyID equals tech.PracticeAreaId into g3
                                                  from practiceArea in g3.DefaultIfEmpty()
                                                  where pa.IsActive == true
                                                  select new UserDetails
                                                  {
                                                      ID = pa.ID,
                                                      name = pa.FirstName + " " + pa.LastName,
                                                      firstName = pa.FirstName,
                                                      lastName = pa.LastName,
                                                      middleName = pa.MiddleName,
                                                      gender = pa.Gender,
                                                      maritalStatus = pa.MaritalStatus,
                                                      EncryptedMobileNo = pa.MobileNo,
                                                      personalEmail = pa.PersonalEmailAddress,
                                                      joiningStatusID = pa.JoiningStatusId,
                                                      gradeID = pa.GradeId,
                                                      employmentType = pa.EmploymentType,
                                                      technology = practiceArea.PracticeAreaCode,
                                                      technologyID = pa.TechnologyID,
                                                      designationID = pa.DesignationId,
                                                      designation = des.DesignationName,
                                                      deptID = pa.DepartmentId,
                                                      department = dep.DepartmentCode,
                                                      joiningDate = pa.JoinDate == null ? null : SqlFunctions.DateName("day", pa.JoinDate) + "/" + SqlFunctions.StringConvert((double)pa.JoinDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pa.JoinDate),
                                                      hrAdvisor = pa.HRAdvisorName,
                                                      bgvStatusID = pa.BGVStatusId,
                                                      RecruitedBy = pa.RecruitedBy,
                                                      empID = pa.EmployeeID ?? default(int),
                                                      managerId = pa.ManagerId ?? default(int)
                                                  }).OrderBy(i => i.joiningDate).ToList();

                    return getAddAssociateDetails;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Prospective Associate details");
            }
        }
        #endregion

        #region GetPADetailsByID
        /// <summary>
        /// GetPADetailsByID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public UserDetails GetPADetailsByID(int ID)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAddAssociateDetailsByID = (from pa in hrmsEntities.ProspectiveAssociates
                                                      join d in hrmsEntities.Departments on pa.DepartmentId equals d.DepartmentId into g1
                                                      from dep in g1.DefaultIfEmpty()
                                                      join des in hrmsEntities.Designations on pa.DesignationId equals des.DesignationId into g2
                                                      from designation in g2.DefaultIfEmpty()
                                                      join grade in hrmsEntities.Grades on pa.GradeId equals grade.GradeId into grades
                                                      from gr in grades.DefaultIfEmpty()
                                                      where pa.ID == ID
                                                      select new UserDetails
                                                      {
                                                          ID = pa.ID,
                                                          name = pa.FirstName + " " + pa.LastName,
                                                          firstName = pa.FirstName,
                                                          lastName = pa.LastName,
                                                          middleName = pa.MiddleName,
                                                          gender = pa.Gender,
                                                          //Birthdate = SqlFunctions.DateName("day", pa.DOB) + "/" + SqlFunctions.StringConvert((double)pa.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pa.DOB),
                                                          maritalStatus = pa.MaritalStatus,
                                                          EncryptedMobileNo = pa.MobileNo,
                                                          personalEmail = pa.PersonalEmailAddress,
                                                          joiningStatusID = pa.JoiningStatusId,
                                                          gradeID = pa.GradeId,
                                                          GradeName = gr.GradeName,
                                                          employmentType = pa.EmploymentType,
                                                          technology = pa.Technology,
                                                          technologyID = pa.TechnologyID, //Here TechnologyID refers to PracticeAreaId
                                                          designationID = pa.DesignationId,
                                                          designation = designation.DesignationName,
                                                          deptID = pa.DepartmentId,
                                                          department = dep.DepartmentCode,
                                                          dateOfJoining = pa.JoinDate,
                                                          joiningDate = pa.JoinDate == null ? null : SqlFunctions.DateName("day", pa.JoinDate) + "/" + SqlFunctions.StringConvert((double)pa.JoinDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pa.JoinDate),
                                                          hrAdvisor = pa.HRAdvisorName,
                                                          bgvStatusID = pa.BGVStatusId,
                                                          RecruitedBy = pa.RecruitedBy,
                                                          empID = pa.EmployeeID ?? default(int),
                                                          managerId = pa.ManagerId ?? default(int),
                                                          empCode = string.Empty,
                                                          dropoutReason = pa.ReasonForDropOut,
                                                          ReportingManagerId = pa.ManagerId == null ? 0 : pa.ManagerId.Value
                                                      }).FirstOrDefault();

                    return getAddAssociateDetailsByID;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Prospective Associate details");
            }
        }
        #endregion

        #region UpdatePAStatus
        /// <summary>
        /// UpdatePAStatus
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public bool UpdatePAStatus(int empID, int statusID)
        {
            bool returnValue = false;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    ProspectiveAssociate prospectiveAssociate = hrmEntities.ProspectiveAssociates.Where(p => p.EmployeeID == empID).FirstOrDefault();

                    if (prospectiveAssociate != null)
                    {
                        prospectiveAssociate.StatusID = statusID;
                        hrmEntities.SaveChanges();

                        //Update Associate as approved and show this employee record in the Associate tab
                        //
                        returnValue = new AssociatePersonalDetails().UpdateAssociateStatus(empID, statusID);
                    }

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to update Prospective Associate status"); ;
            }

            return returnValue;
        }
        #endregion


        #region UpdatePADetails
        /// <summary>
        /// UpdatePADetails
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        public bool UpdatePADetails(UserDetails userDetails)
        {
            bool retValue = false;

            try
            {

                using (APEntities hrmsEntities = new APEntities())
                {

                    if (!string.IsNullOrEmpty(userDetails.personalEmail))
                    {
                        ProspectiveAssociate associate = hrmsEntities.ProspectiveAssociates.Where(email => email.PersonalEmailAddress == userDetails.personalEmail && email.IsActive == true && email.ID != userDetails.ID).FirstOrDefault();

                        if (associate != null && string.IsNullOrEmpty(userDetails.dropoutReason))
                        {
                            throw new AssociatePortalException("Email already exists.");
                        }
                    }
                    ProspectiveAssociate prospectiveAsociate = hrmsEntities.ProspectiveAssociates.Where(n => (n.FirstName == userDetails.firstName && n.LastName == userDetails.lastName && n.MobileNo == userDetails.EncryptedMobileNo && n.IsActive == true && n.ID != userDetails.ID)).FirstOrDefault();

                    if (prospectiveAsociate != null && string.IsNullOrEmpty(userDetails.dropoutReason))
                    {
                        throw new AssociatePortalException("User already exists with same name and mobile number.");
                    }
                    int departmentId = hrmsEntities.Departments.Where(d => d.DepartmentCode.Contains("Training")).FirstOrDefault().DepartmentId;
                    int practiceAreaId = hrmsEntities.PracticeAreas.Where(p => p.PracticeAreaCode.Contains("Training")).FirstOrDefault().PracticeAreaId;

                    ProspectiveAssociate prospectiveAssociate = hrmsEntities.ProspectiveAssociates.Find(userDetails.ID);

                    prospectiveAssociate.MobileNo = userDetails.EncryptedMobileNo;
                    prospectiveAssociate.JoinDate = Commons.GetDateTimeInIST(userDetails.dateOfJoining);
                    prospectiveAssociate.FirstName = userDetails.firstName;
                    prospectiveAssociate.MiddleName = userDetails.middleName;
                    prospectiveAssociate.LastName = userDetails.lastName;
                    prospectiveAssociate.Gender = userDetails.gender;
                    prospectiveAssociate.MaritalStatus = userDetails.maritalStatus;
                    prospectiveAssociate.PersonalEmailAddress = userDetails.personalEmail;
                    prospectiveAssociate.JoiningStatusId = userDetails.joiningStatusID;
                    prospectiveAssociate.GradeId = userDetails.gradeID;
                    prospectiveAssociate.DesignationId = userDetails.designationID;
                    prospectiveAssociate.EmploymentType = userDetails.employmentType;
                    prospectiveAssociate.Technology = userDetails.technology;
                    prospectiveAssociate.TechnologyID = (departmentId == userDetails.deptID) ? practiceAreaId : userDetails.technologyID; //Here TechnologyID refers to PracticeAreaId
                    prospectiveAssociate.DepartmentId = userDetails.deptID;
                    prospectiveAssociate.ManagerId = userDetails.managerId;
                    prospectiveAssociate.HRAdvisorName = userDetails.hrAdvisor;
                    prospectiveAssociate.BGVStatusId = userDetails.bgvStatusID;
                    prospectiveAssociate.ModifiedDate = DateTime.Now;
                    prospectiveAssociate.ModifiedUser = userDetails.CurrentUser;
                    prospectiveAssociate.SystemInfo = userDetails.SystemInfo;
                    prospectiveAssociate.RecruitedBy = userDetails.RecruitedBy;
                    prospectiveAssociate.ReasonForDropOut = userDetails.dropoutReason;
                    prospectiveAssociate.IsActive = string.IsNullOrEmpty(prospectiveAssociate.ReasonForDropOut) ? true : false;
                    hrmsEntities.Entry(prospectiveAssociate).State = EntityState.Modified;
                    retValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                throw;
            }

            return retValue;
        }
        #endregion

        #region DeletePADetails
        /// <summary>
        /// DeletePADetails
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        /// 
        public bool DeletePADetails(int employeeID)
        {
            bool returnValue = false;

            try
            {

                using (APEntities hrmsEntities = new APEntities())
                {
                    ProspectiveAssociate prospectiveAssociate = hrmsEntities.ProspectiveAssociates.Where(p => p.EmployeeID == employeeID).FirstOrDefault();

                    if (prospectiveAssociate != null)
                    {
                        prospectiveAssociate.IsActive = false;
                    }

                    hrmsEntities.SaveChanges();
                    returnValue = true;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to delete Prospective Associate details");
            }

            return returnValue;
        }
        #endregion

        #region DeletePADetailsByID
        /// <summary>
        /// DeletePADetailsByID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public bool DeletePADetailsByID(int empID, string reason)
        {
            bool returnValue = false;

            try
            {

                using (APEntities hrmsEntities = new APEntities())
                {
                    ProspectiveAssociate prospectiveAssociate = hrmsEntities.ProspectiveAssociates.Where(p => p.EmployeeID == empID).FirstOrDefault();

                    if (prospectiveAssociate != null)
                    {
                        prospectiveAssociate.IsActive = false;
                        prospectiveAssociate.ReasonForDropOut = reason;
                    }

                    Employee employee = hrmsEntities.Employees.Where(emp => emp.EmployeeId == empID).FirstOrDefault();

                    if (employee != null)
                    {
                        employee.IsActive = false;
                    }

                    hrmsEntities.Entry(prospectiveAssociate).State = EntityState.Modified;
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to delete prospective associate details");
            }

            return returnValue;
        }
        #endregion

        #region PADetailsSearch
        /// <summary>
        /// PADetailsSearch
        /// </summary>
        /// <param name="searchData"></param>
        /// <returns></returns>
        public IEnumerable<object> PADetailsSearch(SearchFilter searchFilter)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    List<UserDetails> result = GetPADetails() as List<UserDetails>;

                    if (searchFilter.SearchData == null)
                        searchFilter.SearchData = "";

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "name")
                        result = result.Where(i => i.firstName.ToLower().Contains(searchFilter.SearchData.ToLower()) || i.lastName.Contains(searchFilter.SearchData.ToLower())).ToList();

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "tech")
                    {
                        result = result.Where(i => i.technology != null).ToList();
                        result = result.Where(i => i.technology.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();
                    }

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "desg")
                    {
                        result = result.Where(i => i.designation != null).ToList();
                        result = result.Where(i => i.designation.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();
                    }

                    if (searchFilter != null && !string.IsNullOrEmpty(searchFilter.SearchType) && searchFilter.SearchType == "dept")
                        result = result.Where(i => i.department.ToLower().Contains(searchFilter.SearchData.ToLower())).ToList();

                    return result;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get Prospective Associate details"); ;
            }
        }
        #endregion

        #region PagingForPADetails
        /// <summary>
        /// PagingForPADetails
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IEnumerable<object> PagingForPADetails(int pageNumber, int pageSize)
        {
            int pageNo = pageNumber, rowsPerPage = pageSize;
            int skipRows = pageNo * (rowsPerPage - 1);

            using (APEntities hrmsEntities = new APEntities())
            {
                var getAddAssociateDetails = (from pa in hrmsEntities.ProspectiveAssociates
                                              join d in hrmsEntities.Departments on pa.DepartmentId equals d.DepartmentId
                                              join des in hrmsEntities.Designations on pa.DesignationId equals des.DesignationId
                                              select new UserDetails
                                              {
                                                  ID = pa.ID,
                                                  name = pa.FirstName + " " + pa.LastName,
                                                  firstName = pa.FirstName,
                                                  lastName = pa.LastName,
                                                  middleName = pa.MiddleName,
                                                  gender = pa.Gender,
                                                  maritalStatus = pa.MaritalStatus,
                                                  EncryptedMobileNo = pa.MobileNo,
                                                  personalEmail = pa.PersonalEmailAddress,
                                                  joiningStatusID = pa.JoiningStatusId,
                                                  gradeID = pa.GradeId,
                                                  employmentType = pa.EmploymentType,
                                                  technology = pa.Technology,
                                                  designationID = pa.DesignationId,
                                                  designation = des.DesignationName,
                                                  deptID = pa.DepartmentId,
                                                  department = d.DepartmentCode,
                                                  joiningDate = pa.JoinDate == null ? null : SqlFunctions.DateName("day", pa.JoinDate) + "/" + SqlFunctions.StringConvert((double)pa.JoinDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", pa.JoinDate),
                                                  hrAdvisor = pa.HRAdvisorName,
                                                  bgvStatusID = pa.BGVStatusId,
                                                  managerId = pa.ManagerId ?? default(int)
                                              }).OrderBy(o => o.ID).Skip(skipRows).Take(pageSize).ToList();

                return getAddAssociateDetails;
            }
        }
        #endregion

        #region GetJoinedAssociates
        /// <summary>
        /// to get all the joined associates
        /// </summary>
        public async Task<List<AssociateJoiningData>> GetJoinedAssociates()
        {
            List<AssociateJoiningData> lstAssociate;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstAssociate = await apEntities.Database.SqlQuery<AssociateJoiningData>
                              ("[usp_GetJoinedAssociates]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, "");
                throw new AssociatePortalException("Failed to get associates.");
            }
            return lstAssociate;
        }
        #endregion

        #region MasterTablesMethods

        #region GetEmpTypes
        /// <summary>
        /// Get Employee Types
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetEmpTypes()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var empTypes = (from et in hrmsEntities.EmployeeTypes
                                    where et.IsActive == true
                                    select new
                                    {
                                        Code = et.EmployeeTypeCode,
                                        Name = et.EmployeeType1
                                    }).ToList();

                    return empTypes;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details"); ;
            }
        }
        #endregion

        #region GetHRAdvisors
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetHRAdvisors()
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    return (from r in hrmsEntities.Roles
                            join usrrole in hrmsEntities.UserRoles on r.RoleId equals usrrole.RoleId
                            join usr in hrmsEntities.Users on usrrole.UserId equals usr.UserId
                            where r.RoleName == "HRA" && usrrole.IsActive == true
                            select new
                            {
                                ID = usr.UserId,
                                Name = usr.UserName
                            }).ToList();
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details"); ;
            }
        }
        #endregion

        #endregion

        #region GetGradeByDesignation
        /// <summary>
        /// GetGradeByDesignation
        /// </summary>
        /// <returns></returns>
        public async Task<GradeData> GetGradeByDesignation(int designationId)
        {
            GradeData grade;
            try
            {
                using (var apEntities = new APEntities())
                {
                    grade = await apEntities.Database.SqlQuery<GradeData>
                              ("[usp_GetGradeByDesignation] @DesignationId",
                              new object[] {
                                        new SqlParameter ("DesignationId", designationId)
                                            }
                              ).FirstOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return grade;
        }
        #endregion

        #endregion
    }
}
