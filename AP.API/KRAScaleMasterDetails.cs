﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class KRAScaleMasterDetails
    {
        #region GetKRAScaleMasters and Details
        /// <summary>
        /// Gets list of KRA scale masters.
        /// </summary>
        /// <returns></returns>
        public async Task<List<KRAScaleData>> GetKRAScaleMasters()
        {
            List<KRAScaleData> lstKRAScales;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAScales = await apEntities.Database.SqlQuery<KRAScaleData>
                              ("[usp_GetKRAScaleMaster]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAScales;
        }
        public async Task<List<KRAScaleDetails>> GetKRAScaleDetailsByMaster(int scaleMasterId)
        {
            List<KRAScaleDetails> lstKRAScaleDetails;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstKRAScaleDetails = await apEntities.Database.SqlQuery<KRAScaleDetails>
                              ("[usp_GetKRAScaleDetailsByMaster] @ScaleMasterId",
                              new object[] {
                             new SqlParameter ("ScaleMasterId", scaleMasterId)
                              }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstKRAScaleDetails;
        }
        #endregion

        #region CreateScaleMaster
        /// <summary>
        /// Create a kra scale master
        /// </summary>
        /// <param name="kraScaleData"></param>
        /// <returns></returns>
        public async Task<int> CreateScaleMaster(KRAScaleData kraScaleData)
        {
            int rowsAffected = 0;
            int kraScaleMasterId;
            try
            {
                using (var apEntities = new APEntities())
                {
                    if (string.IsNullOrEmpty(kraScaleData.KRAScaleTitle.Trim()))
                        return -13; //Mandatory fields cannot be empty

                    foreach (var scaleDetail in kraScaleData.KRAScaleDetails)
                    {
                        if (string.IsNullOrEmpty(scaleDetail.ScaleDescription.Trim()))
                            return -13; //Mandatory fields cannot be empty
                    }

                    //Created for SQLBulkCopy
                    SqlConnection sConn = (SqlConnection)apEntities.Database.Connection;

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        var DistinctItems = kraScaleData.KRAScaleDetails.GroupBy(scale => scale.ScaleDescription.ToLower().Trim()).Select(description => description.First());

                        if (DistinctItems.Count() != kraScaleData.MaximumScale)
                            return -1; //duplicate scale description.

                        kraScaleMasterId = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateScaleMaster] @MinimumScale, @MaximumScale, @KRAScaleTitle",
                                   new object[] {
                                        new SqlParameter ("MinimumScale", kraScaleData.MinimumScale),
                                        new SqlParameter ("MaximumScale", kraScaleData.MaximumScale),
                                        new SqlParameter ("KRAScaleTitle", kraScaleData.KRAScaleTitle)
                                   }
                                   ).SingleOrDefaultAsync();

                        if (kraScaleMasterId == -1)
                            return -1; //duplicate scale title.

                        //Create datatable to store bulk records
                        DataTable dt = new DataTable("KRAScaleDetails");

                        dt.Columns.Add(new DataColumn("KRAScaleMasterID", typeof(int)));
                        dt.Columns.Add(new DataColumn("KRAScale", typeof(string)));
                        dt.Columns.Add(new DataColumn("ScaleDescription", typeof(string)));


                        //copy data into Datatable
                        foreach (KRAScaleDetails kraScaleDetail in DistinctItems)
                        {
                            DataRow newScaleDetail = dt.NewRow();
                            newScaleDetail["KRAScaleMasterID"] = kraScaleMasterId;
                            newScaleDetail["KRAScale"] = kraScaleDetail.KRAScale;
                            newScaleDetail["ScaleDescription"] = kraScaleDetail.ScaleDescription;
                            dt.Rows.Add(newScaleDetail);
                        }
                        // Bulk copy
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sConn, SqlBulkCopyOptions.Default, (SqlTransaction)trans.UnderlyingTransaction))
                        {
                            bulkCopy.ColumnMappings.Add("KRAScaleMasterID", "KRAScaleMasterID");
                            bulkCopy.ColumnMappings.Add("KRAScale", "KRAScale");
                            bulkCopy.ColumnMappings.Add("ScaleDescription", "ScaleDescription");

                            bulkCopy.DestinationTableName = "KRAScaleDetails";

                            bulkCopy.WriteToServer(dt);
                        }
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
        #endregion

        #region UpdateScaleMaster
        /// <summary>
        /// Update a KRA scale details
        /// </summary>
        /// <param name="kraScaleData"></param>
        /// <returns></returns>
        public async Task<int> UpdateScaleDetails(List<KRAScaleDetails> kraScaleDetails, int MaximumScale)
        {
            try
            {
                using (var apEntities = new APEntities())
                {

                    foreach (var scaleDetail in kraScaleDetails)
                    {
                        if (string.IsNullOrEmpty(scaleDetail.ScaleDescription.Trim()))
                            return -13; //Mandatory fields cannot be empty
                    }

                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        var DistinctItems = kraScaleDetails.GroupBy(scale => scale.ScaleDescription.ToLower().Trim()).Select(description => description.First());

                        if (DistinctItems.Count() != MaximumScale)
                            return -1; //duplicate scale description.

                        DataTable tblScaleDetails = new DataTable();
                        tblScaleDetails.Columns.AddRange(new DataColumn[2]
                        {
                                new DataColumn("Id", typeof(int)),
                                new DataColumn("Description",typeof(string)) }
                        );

                        foreach (KRAScaleDetails scaleDetails in kraScaleDetails)
                        {
                            int id = scaleDetails.KRAScaleDetailId;
                            string name = scaleDetails.ScaleDescription;
                            tblScaleDetails.Rows.Add(id, name);
                        }

                        SqlParameter param = new SqlParameter();
                        param.SqlDbType = SqlDbType.Structured;
                        param.ParameterName = "@tblScaleDetails";
                        param.TypeName = "dbo.ScaleDetailsType";
                        param.Value = tblScaleDetails;
                        apEntities.Database.ExecuteSqlCommand("exec usp_UpdateKRAScaleDetails @tblScaleDetails", param);
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
        #endregion

        #region DeleteScaleMaster
        /// <summary>
        /// Delete scale master by id
        /// </summary>
        /// <param name="kraScaleMasterId"></param>
        /// <returns></returns>
        public async Task<int> DeleteScaleMaster(int kraScaleMasterId)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_DeleteScaleMaster] @KRAScaleMasterId",
                                   new object[] {
                                        new SqlParameter ("KRAScaleMasterId", kraScaleMasterId)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting kra scale master.");
            }
            return rowsAffected;
        }
        #endregion
    }
}
