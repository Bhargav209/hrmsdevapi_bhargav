﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class AssetManagement
    {
        #region CreateCategory
        /// <summary>
        /// Create Category
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateCategory(AssetManagementdetails assetManagementdetail)
        {
            int createAsset;
            try
            {
                using (var apEntities = new APEntities())
                {
                    createAsset = await apEntities.Database.SqlQuery<int>
                             ("[usp_CreateCategory] @CategoryName,@CreatedUser,@CreatedDate,@SystemInfo",
                                  new object[] {
                                    new SqlParameter ("CategoryName", assetManagementdetail.CategoryName.Trim()),
                                    new SqlParameter ("CreatedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                    new SqlParameter ("CreatedDate", DateTime.Now),
                                    new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value)
                                  }).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createAsset;
        }
        #endregion
        #region CreateAsset
        /// <summary>
        /// Create Asset
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateAsset(AssetManagementdetails assetManagementdetail)
        {
            int createAsset;
            try
            {
                using (var apEntities = new APEntities())
                {
                    createAsset = await apEntities.Database.SqlQuery<int>
                             ("[usp_CreateAsset] @AssetName,@CategoryId,@CreatedUser,@CreatedDate,@SystemInfo",
                                  new object[] {
                                    new SqlParameter ("AssetName", assetManagementdetail.AssetName.Trim()),
                                     new SqlParameter ("CategoryId", assetManagementdetail.CategoryId),
                                    new SqlParameter ("CreatedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                    new SqlParameter ("CreatedDate", DateTime.Now),
                                    new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value)
                                  }).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createAsset;
        }
        #endregion
        #region CreateVendor
        /// <summary>
        /// Create Vendor
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateVendor(AssetManagementdetails assetManagementdetail)
        {
            int createVendor;
            try
            {
                using (var apEntities = new APEntities())
                {
                    createVendor = await apEntities.Database.SqlQuery<int>
                             ("[usp_CreateVendor] @VendorName,@VendorPhoneNumber,@VendorAddress,@CreatedUser,@CreatedDate,@SystemInfo",
                                  new object[] {
                                    new SqlParameter ("VendorName", assetManagementdetail.VendorName.Trim()),
                                    new SqlParameter ("VendorPhoneNumber", (object)assetManagementdetail.VendorPhoneNumber ?? DBNull.Value),
                                    new SqlParameter ("VendorAddress", (object)assetManagementdetail.VendorAddress ?? DBNull.Value),
                                    new SqlParameter ("CreatedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                    new SqlParameter ("CreatedDate", DateTime.Now),
                                    new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value)
                                  }).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createVendor;
        }
        #endregion
        #region CreateModel
        /// <summary>
        /// Create Model
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateModel(AssetManagementdetails assetManagementdetail)
        {
            int createModel;
            try
            {
                using (var apEntities = new APEntities())
                {
                    createModel = await apEntities.Database.SqlQuery<int>
                                   ("[usp_CreateModel] @AssetTypeId,@MakeId,@ModelName,@CreatedUser,@CreatedDate,@SystemInfo",
                                   new object[] {
                                    new SqlParameter ("AssetTypeId", assetManagementdetail.AssetTypeId),
                                    new SqlParameter ("MakeId", assetManagementdetail.MakeId),
                                    new SqlParameter ("ModelName", assetManagementdetail.ModelName.Trim()),
                                    new SqlParameter ("CreatedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                    new SqlParameter ("CreatedDate", DateTime.Now),
                                    new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value)
                                   }).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createModel;
        }
        #endregion
        #region CreateMake
        /// <summary>
        /// Create Make
        /// </summary>
        /// <returns></returns>
        public async Task<int> CreateMake(AssetManagementdetails assetManagementdetail)
        {
            int createMake;
            try
            {
                using (var apEntities = new APEntities())
                {
                    createMake = await apEntities.Database.SqlQuery<int>
                              ("[usp_CreateMake] @ManufactureName,@CreatedUser,@CreatedDate,@SystemInfo",
                                   new object[] {
                                    new SqlParameter ("ManufactureName", assetManagementdetail.MakeName.Trim()),
                                    new SqlParameter ("CreatedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                    new SqlParameter ("CreatedDate", DateTime.Now),
                                    new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value)
                                   }).SingleOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createMake;
        }
        #endregion

        #region GetCategoryDetails
        /// <summary>
        /// Get Category Details
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetCategoryDetails()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var assetData = hrmsEntity.Database.SqlQuery<AssetManagementdetails>
                            ("[usp_GetCategoryDetails]",
                                new object[] {

                                }).ToList();
                    return assetData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Category details");
            }
        }
        #endregion
        #region GetAssetDetails
        /// <summary>
        /// Get Asset Details
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetAssetDetails()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var assetData = hrmsEntity.Database.SqlQuery<AssetManagementdetails>
                            ("[usp_GetAssetDetails]",
                                new object[] {
                                       
                                }).ToList();
                    return assetData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Asset details");
            }
        }
        #endregion
        #region GetVendorDetails
        /// <summary>
        /// Get Vendor Details
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetVendorDetails()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var vendorData = hrmsEntity.Database.SqlQuery<AssetManagementdetails>
                            ("[usp_GetVendorDetails] ",
                                new object[] {
                                       
                                }).ToList();
                    return vendorData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Vendor details");
            }
        }
        #endregion
        #region GetModelDetails
        /// <summary>
        /// Get Model Details
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetModelDetails()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var modelData = hrmsEntity.Database.SqlQuery<AssetManagementdetails>
                            ("[usp_GetModelDetails] ",
                                new object[] {
                                       
                                }).ToList();
                    return modelData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get Model details");
            }
        }
        #endregion       
        #region GetMakeDetails
        /// <summary>
        /// Get Make Details
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetMakeDetails()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var manufactureData = hrmsEntity.Database.SqlQuery<AssetManagementdetails>
                            ("[usp_GetMakeDetails] ",
                                new object[] {
                                        
                                }).ToList();
                    return manufactureData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get manufacture details");
            }
        }
        #endregion 

        #region UpdateCategoryDetail
        /// <summary>
        /// Update Category Detail
        /// </summary>
        /// <param name="assetManagementdetail"></param>
        /// <returns></returns>
        public async Task<int> UpdateCategoryDetail(AssetManagementdetails assetManagementdetail)
        {
            int rowsAffected;
            try
            {            
           
            using (var apEntities = new APEntities())
            {
               using (var trans = apEntities.Database.BeginTransaction())
               {
                  rowsAffected = await apEntities.Database.SqlQuery<int>
                         ("[usp_UpdateCategoryDetail] @Id, @CategoryName, @ModifiedDate, @ModifiedUser, @SystemInfo",
                         new object[] {
                                        new SqlParameter ("Id", assetManagementdetail.CategoryId),
                                        new SqlParameter ("CategoryName", assetManagementdetail.CategoryName),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                        new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value),
                             }).SingleOrDefaultAsync();
                  trans.Commit();
               }
            }
         }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region UpdateAssetDetail
        /// <summary>
        /// Update Asset Detail
        /// </summary>
        /// <param name="assetManagementdetail"></param>
        /// <returns></returns>
        public async Task<int> UpdateAssetDetail(AssetManagementdetails assetManagementdetail)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateAssetDetail] @Id,@CategoryId, @AssetName, @ModifiedDate, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("Id", assetManagementdetail.AssetTypeId),
                                        new SqlParameter ("CategoryId", assetManagementdetail.CategoryId),
                                        new SqlParameter ("AssetName", assetManagementdetail.AssetName),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                        new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value),
                                   }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region UpdateVendorDetail
        /// <summary>
        /// Update Vendor Detail
        /// </summary>
        /// <param name="assetManagementdetail"></param>
        /// <returns></returns>
        public async Task<int> UpdateVendorDetail(AssetManagementdetails assetManagementdetail)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateVendorDetail] @Id, @VendorName,@VendorPhoneNumber,@VendorAddress, @ModifiedDate, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("Id", assetManagementdetail.VendorId),
                                        new SqlParameter ("VendorName", assetManagementdetail.VendorName),
                                        new SqlParameter ("VendorPhoneNumber", (object)assetManagementdetail.VendorPhoneNumber ?? DBNull.Value),
                                        new SqlParameter ("VendorAddress", (object)assetManagementdetail.VendorAddress ?? DBNull.Value),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                        new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value),
                                   }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region UpdateModelDetail
        /// <summary>
        /// Update Model Detail
        /// </summary>
        /// <param name="assetManagementdetail"></param>
        /// <returns></returns>
        public async Task<int> UpdateModelDetail(AssetManagementdetails assetManagementdetail)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateModelDetail] @Id,@AssetTypeId,@MakeId, @ModelName, @ModifiedDate, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("Id", assetManagementdetail.ModelId),
                                        new SqlParameter ("AssetTypeId", assetManagementdetail.AssetTypeId),
                                        new SqlParameter ("MakeId", assetManagementdetail.MakeId),
                                        new SqlParameter ("ModelName", assetManagementdetail.ModelName),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                        new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value),
                                   }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
        #region UpdateMakeDetail
        /// <summary>
        /// Update Make Detail
        /// </summary>
        /// <param name="assetManagementdetail"></param>
        /// <returns></returns>
        public async Task<int> UpdateMakeDetail(AssetManagementdetails assetManagementdetail)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_UpdateMakeDetail] @Id, @ManufactureName, @ModifiedDate, @ModifiedUser, @SystemInfo",
                               new object[] {
                                        new SqlParameter ("Id", assetManagementdetail.MakeId),
                                        new SqlParameter ("ManufactureName", assetManagementdetail.MakeName),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", (object)HttpContext.Current.User.Identity.Name ?? DBNull.Value),
                                        new SqlParameter ("SystemInfo", (object)Commons.GetClientIPAddress()?? DBNull.Value),
                                   }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rowsAffected;
        }
        #endregion
    }
}
