﻿using AP.DataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DomainEntities;
using static AP.Utility.Enumeration;
using System.Web;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Data.Entity;
using System.Data.Entity.Core;
using AP.Utility;
using System.Data.SqlClient;

namespace AP.API
{
    public class AssociateResignationDetails
    {
        #region CreateAssociateResignation
        /// <summary>
        /// Save Resignation Details
        /// </summary>
        /// <param name="resignationDetails"></param>
        /// <returns>bool</returns>
        public async Task<bool> CreateAssociateResignation(AssociateResignationData resignationDetails)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_CreateAssociateResignation] @EmployeeID, @ReasonID, @ReasonDescription, @DateOfResignation, @StatusID, @CreatedBy, @CreatedDate",
                                   new object[] {
                                        new SqlParameter ("EmployeeID", resignationDetails.EmployeeId),
                                        new SqlParameter ("ReasonID", resignationDetails.ReasonId),
                                        new SqlParameter ("ReasonDescription", resignationDetails.ReasonDescription),
                                        new SqlParameter ("DateOfResignation", resignationDetails.DOR),
                                        new SqlParameter ("StatusID", new Common().GetStatusId(StatusCategory.AssociateExit.ToString(), AssociateExitStatusCodes.SubmittedForResignation.ToString())),
                                        new SqlParameter ("CreatedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while creating resignation.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region ResignationDashboard
        /// <summary>
        /// Get resignation details on Dashboard
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns>List<AssociateResignationData></returns>
        public async Task<List<AssociateResignationData>> ResignationDashboard(int employeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstResignation = await apEntities.Database.SqlQuery<AssociateResignationData>
                              ("[usp_ResignationDashboard] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", employeeID) }).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get active categories");
            }
            return lstResignation;
        }
        #endregion

        #region ApproveResignation
        /// <summary>
        /// ApproveResignation
        /// </summary>
        /// <param name="resignationApprovalData"></param>
        /// <returns>bool</returns>
        public async Task<bool> ApproveResignation(ResignationApprovalData resignationApprovalData)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                               ("[usp_ResignationApproval] @ResignationID, @ApprovedByID, @ApprovedDate, @Remarks, @ResignationRecommendation, @CreatedBy, @CreatedDate, @LastWorkingDate, @StatusID, @ToDeliveryHead",
                                   new object[] {
                                        new SqlParameter ("ResignationID", resignationApprovalData.ResignationID),
                                        new SqlParameter ("ApprovedByID", resignationApprovalData.ApprovedByID),
                                        new SqlParameter ("ApprovedDate", resignationApprovalData.ApprovedDate),
                                        new SqlParameter ("Remarks", resignationApprovalData.Remarks),
                                        new SqlParameter ("ResignationRecommendation", resignationApprovalData.ResignationRecommendation),
                                        new SqlParameter ("CreatedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("LastWorkingDate", resignationApprovalData.LastWorkingDate),
                                        new SqlParameter ("StatusID", resignationApprovalData.StatusID),
                                        new SqlParameter ("ToDeliveryHead", resignationApprovalData.ToDeliveryHead)
                                   }
                                   ).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while approving resignation.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion

        #region GetResignationDetails
        /// <summary>
        /// Get Resignation Details of an Employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns>object</returns>
        public async Task<AssociateResignationData> GetResignationDetails(int employeeID)
        {
            try
            {
                using (var apEntities = new APEntities())
                {
                    var resignationData = await apEntities.Database.SqlQuery<AssociateResignationData>
                              ("[usp_GetAssociateResignationDetails] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", employeeID)}).FirstOrDefaultAsync();

                    return resignationData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get associate resignations.");
            }

        }
        #endregion

        #region GetAssociatePendingResignationsList
        /// <summary>
        /// Gets list of pending associate resignations.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<AssociateResignationData>> GetAssociatePendingResignationsList(int EmployeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstResignation = await apEntities.Database.SqlQuery<AssociateResignationData>
                              ("[usp_GetAssociatePendingResignationsList] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeID)}
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get pending resignations.");
            }
            return lstResignation;
        }
        #endregion

        #region GetAssociateApprovedResignationsList
        /// <summary>
        /// Gets list of approved associate resignations.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<AssociateResignationData>> GetAssociateApprovedResignationsList(int EmployeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstResignation = await apEntities.Database.SqlQuery<AssociateResignationData>
                              ("[usp_GetApprovedResignationsList] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeID)}
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get approved resignations.");
            }
            return lstResignation;
        }
        #endregion

        #region GetAssociateRejectedResignationsList
        /// <summary>
        /// Gets list of rejected associate resignations.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<AssociateResignationData>> GetAssociateRejectedResignationsList(int EmployeeID)
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstResignation = await apEntities.Database.SqlQuery<AssociateResignationData>
                              ("[usp_GetRejectedResignationsList] @EmployeeID",
                              new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeID)}
                              ).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get rejected resignations.");
            }
            return lstResignation;
        }
        #endregion

        #region GetDeliveryHeadList
        /// <summary>
        /// Gets list of delivery heads.
        /// </summary>
        /// <returns>List</returns>
        public async Task<List<AssociateResignationData>> GetDeliveryHeadList()
        {
            List<AssociateResignationData> lstResignation;
            try
            {
                using (var apEntities = new APEntities())
                {
                    lstResignation = await apEntities.Database.SqlQuery<AssociateResignationData>
                              ("[usp_GetDeliveryHeadList]").ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get delivery heads.");
            }
            return lstResignation;
        }
        #endregion

        private bool UpdateEmployeeRelievingDate(int employeeId, DateTime resignationDate)
        {
            bool status = false;
            try
            {
                int iNoticePeriod = GetNoticePeriod();
                using (APEntities hrmsEntities = new APEntities())
                {
                    IQueryable<Employee> employeeQuery = hrmsEntities.Employees.Where(emp => emp.EmployeeId == employeeId);
                    Employee employee = employeeQuery.FirstOrDefault();
                    DateTime nperiod = resignationDate.AddMonths(iNoticePeriod);
                    employee.RelievingDate = nperiod; //System.Data.Entity.DbFunctions.AddMonths(resignationDate, iNoticePeriod); //SqlFunctions.DateAdd("month", iNoticePeriod, resignationDate);
                    hrmsEntities.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                    status = hrmsEntities.SaveChanges() > 0 ? true : false;
                }

            }
            catch (Exception ex)
            {

            }
            return status;
        }
        private int GetNoticePeriod()
        {
            int noticePeriod = 0;
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<lkValue> query = hrmsEntities.lkValues.Where(lkValue => lkValue.ValueName.ToLower() == "Notice Period".ToLower() && lkValue.IsActive == true);
                noticePeriod = Convert.ToInt32(query.FirstOrDefault().ValueID);
            }
            return noticePeriod;
        }
    }
}
