﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using Quartz;
using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System.Web;

namespace AP.API
{
    public class AdminNotificationJob : IJob
    {

        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region AddJob
        /// <summary>
        /// Add job to the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <param name="interval"></param>
        /// 
        public bool AddJob(string jobId, string groupId)
        {
            bool returnValue = true;

            try
            {
                using (APEntities hrmEntities = new APEntities())
                {

                    // Insert record to the JobDetail table //
                    //
                    string adminNotificationCode = resourceManager.GetString("AdminNotification");
                    NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

                    if (notificationConfiguration.SLA == 0) throw new AssociatePortalException("Admin Notification's SLA can not be zero.");

                    returnValue = NotificationScheduler.AddJob<AdminNotificationJob>(jobId, groupId, notificationConfiguration.SLA.GetValueOrDefault());

                    if (returnValue)
                    {
                        string started = resourceManager.GetString("Started");

                        var jobExist = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId && j.Status == started).FirstOrDefault();

                        // Insert record to the JobDetail table 
                        //
                        if (jobExist == null)
                        {
                            JobDetail jobDetail = new JobDetail() { JobCode = jobId, GroupID = groupId, Status = started, JobInterval = notificationConfiguration.SLA };
                            jobDetail = hrmEntities.JobDetails.Add(jobDetail);
                        }

                        int adminDepartmentNotificationTypeID = Convert.ToInt32(resourceManager.GetString("AdminDepartmentNotificationTypeID"));
                        int statusPendingID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
                        var notType = Convert.ToInt32(adminDepartmentNotificationTypeID);
                        var pending = Convert.ToInt32(statusPendingID);
                        var notificationExist = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == notType && n.StatusId == pending).FirstOrDefault();

                        if (notificationExist == null)
                        {
                            // Insert record in to Notification table  
                            //
                            Notification notification = new Notification() { EmployeeCode = jobId, NotificationTypeID = adminDepartmentNotificationTypeID, StatusId = statusPendingID };
                            notification = hrmEntities.Notifications.Add(notification);
                        }

                        //Insert the task details into AdminTask table
                        //
                        IEnumerable adminTasks = GetAdminTasks();

                        foreach (AdminTaskMaster task in adminTasks)
                        {
                            AdminTask adminTask = new AdminTask() { EmployeeCode = jobId, TaskID = task.AdminTaskID, StatusId = statusPendingID };
                            adminTask = hrmEntities.AdminTasks.Add(adminTask);
                        }

                        hrmEntities.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                throw new AssociatePortalException(ex.Message);
            }

            return returnValue;
        }
        #endregion

        #region RemoveJob
        /// <summary>
        /// Remove job from the scheduler
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// 
        public bool RemoveJob(string jobId, string groupId)
        {
            bool isDelete = false;
            APEntities hrmEntities = null;

            try
            {
                isDelete = NotificationScheduler.RemoveJob(jobId, groupId);

                using (hrmEntities = new APEntities())
                {
                    ///  Delete Job from JobDetail table ///
                    JobDetail jobToBeDeleted = hrmEntities.JobDetails.Where(j => j.JobCode == jobId && j.GroupID == groupId).FirstOrDefault();

                    if (jobToBeDeleted != null)
                    {
                        hrmEntities.JobDetails.Remove(jobToBeDeleted);
                    }

                    int adminNotificationTypeID = Convert.ToInt32(resourceManager.GetString("AdminDepartmentNotificationTypeID"));
                    Notification notification = hrmEntities.Notifications.Where(n => n.EmployeeCode == jobId && n.NotificationTypeID == adminNotificationTypeID).FirstOrDefault();

                    if (notification != null)
                    {
                        int statusApprovedID = Convert.ToInt32(resourceManager.GetString("StatusApprovedID"));
                        notification.StatusId = statusApprovedID;
                    }

                    hrmEntities.SaveChanges();
                }
            }
            catch
            {
                throw;
            }

            return isDelete;
        }
        #endregion

        #region Execute
        /// <summary>
        /// Execution of the job for sending email
        /// </summary>
        /// <param name="context"></param>
        /// 
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (context != null)
                {
                    JobDataMap jobDataMap = context.JobDetail.JobDataMap;
                    string associateId = jobDataMap.GetString("AssociateId");
                    string name = jobDataMap.GetString("AssociateName");
                    string designation = jobDataMap.GetString("Designation");
                    string department = jobDataMap.GetString("Department");
                    string mobileNo = jobDataMap.GetString("Mobile");

                    using (APEntities hrmEntities = new APEntities())
                    {
                        string adminNotificationCode = resourceManager.GetString("AdminNotification");
                        //NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == adminNotificationCode).FirstOrDefault();

                        //if (notificationConfiguration != null && name != null && department != null)
                        //{
                        //    string htmlContent = notificationConfiguration.emailContent;
                        //    htmlContent = htmlContent.Replace("@AssociateName", name).Replace("@Designation", designation).Replace("@Department", department).Replace("@Mobile", mobileNo);
                        //    Email email = new Email();
                        //    int notificationConfigID = new BaseEmail().BuildEmailObject(email, notificationConfiguration.emailTo, notificationConfiguration.emailFrom, notificationConfiguration.emailCC, notificationConfiguration.emailSubject + associateId, htmlContent, Enumeration.NotificationStatus.Admin.ToString());
                        //    new BaseEmail().SendEmail(email, notificationConfigID);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException(ex.Message);
            }
        }
        #endregion

        #region GetAdminNotifications
        /// <summary>
        /// Method to get Admin notification list
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<object> GetAdminNotifications()
        {
            try
            {
                using (APEntities apEntities = new APEntities())
                {
                    string pending = resourceManager.GetString("StatusPending");
                    string adminNotificationCode = resourceManager.GetString("AdminNotification");
                    var statusPending = apEntities.Status.Where(s => s.StatusCode == pending).FirstOrDefault();
                    var adminNotificationType = apEntities.NotificationTypeMasters.Where(n => n.NotificationCode == adminNotificationCode).FirstOrDefault();

                    var adminNotifications = (from n in apEntities.Notifications
                                              join nm in apEntities.NotificationTypeMasters on n.NotificationTypeID equals nm.NotificationTypeID
                                              join e in apEntities.Employees on n.EmployeeCode equals e.EmployeeCode
                                              join a in apEntities.AdminTasks on n.EmployeeCode equals a.EmployeeCode
                                              join am in apEntities.AdminTaskMasters on a.TaskID equals am.AdminTaskID
                                              where (a.StatusId == statusPending.StatusId && nm.NotificationTypeID == adminNotificationType.NotificationTypeID)
                                              select new NotificationData
                                              {
                                                  notificationTypeId = n.NotificationTypeID,
                                                  notificationCode = nm.NotificationCode,
                                                  notificationDesc = nm.NotificationDesc,
                                                  employeeCode = n.EmployeeCode,
                                                  firstName = e.FirstName,
                                                  lastName = e.LastName,
                                                  statusId = a.StatusId,
                                                  taskName = am.TaskName,
                                                  taskId = a.TaskID
                                              });

                    var Notification = adminNotifications.Distinct().ToList();

                    if (Notification.Count > 0)
                        return Notification;
                    else
                        return Enumerable.Empty<object>().ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region CountAdminNotifications
        /// <summary>
        /// Method to get Admin notification count
        /// </summary>
        /// <returns></returns>
        /// 
        public int CountAdminNotifications()
        {
            APEntities apEntities = null;

            try
            {
                using (apEntities = new APEntities())
                {
                    int pendingStatusID = Convert.ToInt32(resourceManager.GetString("StatusPendingID"));
                    var empCount = (from admin in apEntities.AdminTasks
                                    where admin.StatusId == pendingStatusID
                                    select new { admin.EmployeeCode }).Distinct().Count();
                    return empCount;
                }
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region UpdateAdminNotifications
        /// <summary>
        /// Method to update Admin notification status
        /// </summary>
        /// <param name="notificationData"></param>
        /// <returns></returns>
        /// 
        public bool UpdateAdminNotifications(string employeeCode, int taskId)
        {
            bool isUpdated = false;
            APEntities hrmsEntities = null;

            try
            {
                using (hrmsEntities = new APEntities())
                {
                    AdminTask adminTask = hrmsEntities.AdminTasks.Where(e => e.EmployeeCode == employeeCode && e.TaskID == taskId).ToList().FirstOrDefault();

                    string approved = resourceManager.GetString("StatusApproved");
                    var statusApproved = hrmsEntities.Status.Where(s => s.StatusCode == approved).FirstOrDefault();
                    adminTask.StatusId = statusApproved.StatusId;
                    hrmsEntities.Entry(adminTask).State = System.Data.Entity.EntityState.Modified;
                    isUpdated = hrmsEntities.SaveChanges() > 0 ? true : false;

                    //Remove the admin notification job if there is no more pending task for this employee
                    string pending = resourceManager.GetString("StatusPending");
                    var statusPending = hrmsEntities.Status.Where(s => s.StatusCode == pending).FirstOrDefault();
                    AdminTask pendingAdminTask = hrmsEntities.AdminTasks.Where(e => e.EmployeeCode == employeeCode && e.StatusId == statusPending.StatusId).ToList().FirstOrDefault();

                    if (pendingAdminTask == null)
                    {
                        RemoveJob(employeeCode, resourceManager.GetString("AdminNotification"));
                    }

                }
            }
            catch
            {
                isUpdated = false;
                throw;
            }

            return isUpdated;
        }
        #endregion

        #region GetAdminTasks
        /// <summary>
        /// Method to get Admin tasks
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<AdminTaskMaster> GetAdminTasks()
        {
            APEntities hrmsEntities = null;

            try
            {
                using (hrmsEntities = new APEntities())
                {
                    IEnumerable<AdminTaskMaster> getAdminTasks = (from e in hrmsEntities.AdminTaskMasters
                                                                  select e).ToList();
                    return getAdminTasks;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}

