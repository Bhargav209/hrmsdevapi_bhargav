﻿using System.DirectoryServices.AccountManagement;
using AP.DomainEntities;

namespace AP.API
{
    public class ActiveDirectoryLogin : ILogin
    {
        #region LoginUser
        /// <summary>
        /// LoginUser
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool LoginUser(string loginUser, string password)
        {
            bool retValue = false;

            if (loginUser != null && password != null)
            {
                using (PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, "SENECAGLOBAL.NET"))
                {
                    // validate the credentials
                    if (principalContext.ValidateCredentials(loginUser, password))
                    {
                        UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, loginUser);
                        retValue = true;
                    }
                }
            }

            return retValue;
        }
        #endregion
    }
}
