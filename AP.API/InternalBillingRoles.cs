﻿using AP.DataStorage;
using AP.DomainEntities;
using AP.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AP.API
{
    public class InternalBillingRoles
    {
        #region CreateInternalBillingRoles
        /// <summary>
        /// Method to add new internal billing role data to database
        /// </summary>
        /// <param name="internalBillingRoleDetails"></param>
        /// <returns></returns>        
        public async Task<int> CreateInternalBillingRoles(InternalBillingRoleDetails internalBillingRoleDetails)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var transaction = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                                ("[usp_CreateInternalBillingRole] @InternalBillingRoleCode,@InternalBillingRoleName,@IsActive,@CreatedUser,@CreatedDate,@SystemInfo",
                                new object[]
                                {
                                    new SqlParameter("InternalBillingRoleCode",internalBillingRoleDetails.InternalBillingRoleCode),
                                    new SqlParameter("InternalBillingRoleName",internalBillingRoleDetails.InternalBillingRoleName),
                                    new SqlParameter("IsActive",1),
                                    new SqlParameter("CreatedUser",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("CreatedDate",DateTime.UtcNow),
                                    new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())

                                }).SingleOrDefaultAsync();

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while adding a InternalBillingRole");
            }
            return rowsAffected;
        }

        #endregion

        #region UpdateInternal
        /// <summary>
        /// Method to update existing internal billing role data
        /// </summary>
        /// <param name="internalBillingRoleDetails"></param>
        /// <returns></returns>       
        public async Task<int> UpdateInternalBillingRole(InternalBillingRoleDetails internalBillingRoleDetails)
        {
            int rowsAffected = 0;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = await apEntities.Database.SqlQuery<int>
                          ("[usp_UpdateInternalBillingRole] @InternalBillingRoleID, @InternalBillingRoleCode,@InternalBillingRoleName,@ModifiedDate,@ModifiedUser, @SystemInfo",
                              new object[] {
                                        new SqlParameter ("InternalBillingRoleID", internalBillingRoleDetails.InternalBillingRoleId),
                                        new SqlParameter ("InternalBillingRoleCode", internalBillingRoleDetails.InternalBillingRoleCode),
                                        new SqlParameter ("InternalBillingRoleName", internalBillingRoleDetails.InternalBillingRoleName),
                                        new SqlParameter ("ModifiedDate", DateTime.UtcNow),
                                        new SqlParameter ("ModifiedUser", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                              }).SingleOrDefaultAsync();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log.LogError(ex, Utility.Log.Severity.Error, "");
                throw new AssociatePortalException("Error while updating a Internal Billing Role Details");
            }
            return rowsAffected;
        }

        #endregion

        #region GetInternalBillingRoles        

        public async Task<List<InternalBillingRoleDetails>> GetInternalBillingRoles(bool isActive)
        {
            List<InternalBillingRoleDetails> internalBillingRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    internalBillingRoles = await apEntities.Database.SqlQuery<InternalBillingRoleDetails>
                                    ("[usp_GetInternalBillingRole]").ToListAsync();

                    internalBillingRoles = internalBillingRoles.OrderBy(p => p.InternalBillingRoleCode).ToList();                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return internalBillingRoles;

        }

        public async Task<List<InternalBillingRoleDetails>> GetInternalBillingRoles()
        {
            List<InternalBillingRoleDetails> internalBillingRoles;
            try
            {
                using (var apEntities = new APEntities())
                {
                    internalBillingRoles = await apEntities.Database.SqlQuery<InternalBillingRoleDetails>
                                    ("[usp_GetInternalBillingRole]").ToListAsync();

                    internalBillingRoles = internalBillingRoles.OrderBy(p => p.InternalBillingRoleCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return internalBillingRoles;

        }

        #endregion
    }
}
