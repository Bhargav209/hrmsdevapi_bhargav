﻿using AP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AP.DataStorage;
using AP.DomainEntities;
using System.Resources;
using System.Data.Entity;
using System.Web;
using System.Data.SqlClient;
using AP.Notification;
using System.Net;

namespace AP.API
{
    public class Allocation
    {
        ResourceManager resourceManager = new ResourceManager("AP.Utility.ConstantResources", typeof(ConstantResources).Assembly);

        #region Get Approved Talent Requisition List
        /// <summary>
        /// GetApprovedTalentRequisitionList
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TalentRequisitionHistoryData> GetApprovedTalentRequisitionList()
        {
            string parms = "[]";
            int statusId = Convert.ToInt32(Enumeration.TalentRequisitionStatusCodes.Approved);

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAssociateDetails = (from tr in hrmsEntities.TalentRequisitions
                                               join dept in hrmsEntities.Departments on tr.DepartmentId equals dept.DepartmentId
                                               where tr.IsActive == true && tr.StatusId == statusId && dept.DepartmentTypeId == 1 && dept.DepartmentId == 1
                                               select new TalentRequisitionHistoryData
                                               {
                                                   TRCode = tr.TRCode,
                                                   TalentRequisitionId = tr.TRId,
                                                   DepartmentName = tr.Department.Description,
                                                   ProjectName = tr.Project.ProjectName,
                                                   ProjectId = tr.ProjectId,
                                                   NoOfPositions = tr.RequisitionRoleDetails.Sum(rrd => rrd.NoOfBillablePositions) + tr.RequisitionRoleDetails.Sum(rrd => rrd.NoOfNonBillablePositions),
                                               }).ToList().OrderByDescending(i => i.TalentRequisitionId);
                    return getAssociateDetails;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
        }
        #endregion

        #region Get Approved Talent Requisition List
        /// <summary>
        /// GetApprovedTalentRequisitionList
        /// </summary>
        /// <returns></returns>
        public async Task<List<TalentRequisitionHistoryData>> GetTaggedEmployeesForTalentRequisition()
        {
            string parms = "[]";
            List<TalentRequisitionHistoryData> talentRequisitionHistoryList;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {

                    talentRequisitionHistoryList = await hrmsEntities.Database.SqlQuery<TalentRequisitionHistoryData>
                                      ("usp_GetTaggedEmployeesForTalentRequisition",
                                       new object[] { }
                                       ).ToListAsync();


                    return talentRequisitionHistoryList.OrderByDescending(tr => tr.TalentRequisitionId).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
        }
        #endregion

        #region Get Required Skills Detail
        /// <summary>
        /// GetRequiredSkillsDetail
        /// </summary>
        /// <param name = "TRId" ></ param >
        /// < param name="RequisitionRoleId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetRequiredSkillsDetails(int requisitionRoleDetailId)
        {

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var getAssociateDetails = (from rrs in hrmsEntities.RequisitionRoleSkills
                                               join roleSkills in hrmsEntities.RequisitionRoleSkillMappings on rrs.RoleSkillMappingID equals roleSkills.ID
                                               where roleSkills.RequistiionRoleDetailID == requisitionRoleDetailId
                                               select new
                                               {
                                                   CompetencyArea = rrs.CompetencyArea.CompetencyAreaDescription,
                                                   Skill = rrs.Skill.SkillName,
                                                   // ProficiencyLevel = rrs.ProficiencyLevel.ProficiencyLevelDescription,
                                                   //IsPrimary = rrs.IsPrimary
                                               }).ToList();
                    return getAssociateDetails;

                }
            }
            catch (Exception ex)
            {

                throw new AssociatePortalException("Failed to get skill details.");
            }

        }
        #endregion

        #region Allocate Associates
        /// <summary>
        /// AllocateAssociates
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        public bool AllocateAssociates(List<AssociateAllocationDetails> associateAllocationList)
        {
            bool isSaved = false;

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    foreach (AssociateAllocationDetails objAssociateAllocation in associateAllocationList)
                    {
                        isSaved = InsertAssociateAllocationDetails(hrmsEntities, objAssociateAllocation);

                        //Close requisition when there is no remaining positions.
                        //bool closed = CloseTalentRequisitionBasedonAllocatedPositions(objAssociateAllocation.TalentRequisitionId);

                        if (isSaved && objAssociateAllocation.NotifyAll == true)
                        {
                            int allocatedProjects = (from aa in hrmsEntities.AssociateAllocations
                                                     where aa.EmployeeId == objAssociateAllocation.EmployeeId && aa.IsActive == true
                                                     select aa).Count();

                            if (allocatedProjects > 1)
                            {
                                int? primaryreportingMangerId = hrmsEntities.AssociateAllocations.AsNoTracking().Where(i => i.EmployeeId == objAssociateAllocation.EmployeeId && i.IsActive == true && i.IsPrimary == true).Select(r => r.ReportingManagerId).FirstOrDefault();
                                if (primaryreportingMangerId != null)
                                {
                                    int? reportingMangerId = hrmsEntities.AssociateAllocations.AsNoTracking().Where(i => i.EmployeeId == objAssociateAllocation.EmployeeId && i.IsActive == true && i.IsPrimary != true).Select(r => r.ReportingManagerId).FirstOrDefault();
                                    SendAllocationNotification(objAssociateAllocation.EmployeeId, reportingMangerId ?? 0, primaryreportingMangerId ?? 0, String.Format("{0:MMM dd, yyyy}", objAssociateAllocation.EffectiveDate), objAssociateAllocation.ProjectId);
                                }
                                else
                                    SendAllocationNotification(objAssociateAllocation.EmployeeId, objAssociateAllocation.ReportingManagerId ?? 0, objAssociateAllocation.ReportingManagerId ?? 0, String.Format("{0:MMM dd, yyyy}", objAssociateAllocation.EffectiveDate), objAssociateAllocation.ProjectId);
                            }
                            else
                                SendAllocationNotification(objAssociateAllocation.EmployeeId, objAssociateAllocation.ReportingManagerId ?? 0, objAssociateAllocation.ReportingManagerId ?? 0, String.Format("{0:MMM dd, yyyy}", objAssociateAllocation.EffectiveDate), objAssociateAllocation.ProjectId);
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                string parms = "[]";
                Log.LogError(ex, Log.Severity.Error, parms);
                throw;
            }
            return isSaved;
        }
        #endregion

        #region CloseTalentRequisitionBasedonAllocatedPositions
        /// <summary>
        /// Close Talent Requisition Based on Allocated Positions
        /// </summary>
        /// <returns>List</returns>
        public bool CloseTalentRequisitionBasedonAllocatedPositions(int? TalentRequisitionId)
        {
            int rowsAffected;
            try
            {
                using (var apEntities = new APEntities())
                {
                    using (var trans = apEntities.Database.BeginTransaction())
                    {
                        rowsAffected = apEntities.Database.SqlQuery<int>
                                ("[usp_CloseTalentRequisitionBasedonAllocatedPositions] @TalentRequisitionId",
                                   new object[] {
                                        new SqlParameter ("TalentRequisitionId", TalentRequisitionId)
                                   }
                                   ).SingleOrDefault();
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Error while deleting skills.");
            }
            return rowsAffected > 0 ? true : false;
        }
        #endregion


        #region InsertAssociateAllocationDetails
        /// <summary>
        /// InsertAssociateAllocationDetails
        /// </summary>
        /// <param name="hrmsEntities"></param>
        /// <param name="objAssociateAllocation"></param>
        /// <returns></returns>
        private bool InsertAssociateAllocationDetails(APEntities hrmsEntities, AssociateAllocationDetails objAssociateAllocation)
        {
            int rowsAffected;

            using (var dbContext = hrmsEntities.Database.BeginTransaction())
            {
                try
                {
                    AssociateAllocation alreadyAllocated = hrmsEntities.AssociateAllocations.AsNoTracking().Where(aa => aa.EmployeeId == objAssociateAllocation.EmployeeId && aa.ProjectId == objAssociateAllocation.ProjectId && aa.IsActive == true).FirstOrDefault();
                    if (alreadyAllocated != null)
                        throw new AssociatePortalException("-2");

                    rowsAffected = hrmsEntities.Database.SqlQuery<int>
                               ("[usp_AllocateAssociate] @TalentRequisitionId,@ProjectId,@EmployeeId,@RoleMasterId,@EffectiveDate,@AllocationPercentageId,@InternalBillingPercentage,@ClientBillingPercentage,@IsCritical,@IsBillable,@IsPrimary,@ReportingManagerId,@ClientBillingRoleId,@InternalBillingRoleId,@CreatedDate,@CreatedBy,@SystemInfo",
                                  new object[] {
                                        new SqlParameter ("TalentRequisitionId", objAssociateAllocation.TalentRequisitionId),
                                        new SqlParameter ("ProjectId", objAssociateAllocation.ProjectId),
                                        new SqlParameter ("EmployeeId", objAssociateAllocation.EmployeeId),
                                        new SqlParameter ("RoleMasterId", objAssociateAllocation.RoleMasterId),
                                        new SqlParameter ("EffectiveDate", objAssociateAllocation.EffectiveDate),
                                        new SqlParameter ("AllocationPercentageId", objAssociateAllocation.AllocationPercentage),
                                        new SqlParameter ("InternalBillingPercentage", objAssociateAllocation.InternalBillingPercentage),
                                        new SqlParameter ("ClientBillingPercentage", objAssociateAllocation.ClientBillingPercentage),
                                        new SqlParameter ("IsCritical", objAssociateAllocation.isCritical),
                                        new SqlParameter ("IsBillable", objAssociateAllocation.Billable),
                                        new SqlParameter ("IsPrimary", objAssociateAllocation.IsPrimary),
                                        new SqlParameter ("ReportingManagerId", objAssociateAllocation.ReportingManagerId),
                                        new SqlParameter ("ClientBillingRoleId", (object)objAssociateAllocation.ClientBillingRoleId??DBNull.Value),
                                        new SqlParameter ("InternalBillingRoleId", (object)objAssociateAllocation.InternalBillingRoleId??DBNull.Value),
                                        new SqlParameter ("CreatedDate", DateTime.UtcNow),
                                        new SqlParameter ("CreatedBy", HttpContext.Current.User.Identity.Name),
                                        new SqlParameter ("SystemInfo", Commons.GetClientIPAddress())
                                  }
                                  ).SingleOrDefault();
                    dbContext.Commit();
                    return rowsAffected > 0 ? true : false;
                }

                catch (Exception ex)
                {
                    dbContext.Rollback();
                    throw;
                }
            }
        }
        #endregion

        #region IsValidAllocation
        /// <summary>
        /// IsValidAllocation
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="newPercentage"></param>
        /// <returns></returns>
        private bool IsValidAllocation(int? empId, decimal? newPercentage)
        {
            return true;
            //Below code will not get executed as the changes were done to avoid the server side validations while allocating...
            bool isValid = false;
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var allocatedPercentage = (from aa in hrmsEntity.AssociateAllocations
                                               join p in hrmsEntity.Projects on aa.ProjectId equals p.ProjectId
                                               join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                                               where aa.EmployeeId == empId && aa.IsActive == true && p.IsActive == true && pt.IsActive == true && pt.PracticeAreaCode != "Talent Pool"
                                               group aa by new { aa.EmployeeId, pt.PracticeAreaId, pt.PracticeAreaCode } into temp
                                               select new { percentage = temp.Sum(u => u.AllocationPercentage) }).FirstOrDefault();

                    //var allocatedPercentage = hrmsEntity.AssociateAllocations
                    //                            .Where(aa => aa.EmployeeId == empId && aa.IsActive==true && aa.ProjectId!=)
                    //                            .Sum(aa => aa.AllocationPercentage);
                    if (allocatedPercentage != null)
                    {
                        if (allocatedPercentage.percentage.GetValueOrDefault() + newPercentage <= 100)
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        if (newPercentage <= 100)
                        {
                            isValid = true;
                        }

                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }

            return isValid;
        }
        #endregion

        /// <summary>
        /// GetAllocatedPercentage
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="newPercentage"></param>
        /// <returns></returns>
        public object GetAllocatedPercentage(string empCode, decimal? newPercentage)
        {
            dynamic result = null;
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    int empId = hrmsEntity.Employees.Where(i => i.EmployeeCode == empCode && i.IsActive == true).Select(i => i.EmployeeId).FirstOrDefault();
                    result = (from aa in hrmsEntity.AssociateAllocations
                              join p in hrmsEntity.Projects on aa.ProjectId equals p.ProjectId
                              join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                              where aa.EmployeeId == empId && aa.IsActive == true && p.IsActive == true && pt.IsActive == true && pt.PracticeAreaCode != "Talent Pool"
                              group aa by new { aa.EmployeeId, pt.PracticeAreaId, pt.PracticeAreaCode } into temp
                              select new { percentage = temp.Sum(u => u.AllocationPercentage) }).FirstOrDefault();

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get allocation percentage.");
            }
            return result;
        }

        /// <summary>
        /// GetAllocatedPercentage
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public object GetAllocatedPercentage(int? empId)
        {
            dynamic result = null;
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    result = (from aa in hrmsEntity.AssociateAllocations
                              join p in hrmsEntity.Projects on aa.ProjectId equals p.ProjectId
                              join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                              where aa.EmployeeId == empId && aa.IsActive == true && p.IsActive == true && pt.IsActive == true && pt.PracticeAreaCode != "Talent Pool"
                              group aa by new { aa.EmployeeId } into temp
                              select new { percentage = temp.Sum(u => u.AllocationPercentage) }).FirstOrDefault();

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get allocation percentage.");
            }
            return result;
        }

        /// <summary>
        /// GetAllocatedPercentage
        /// </summary>
        /// <param name="empIds"></param>
        /// <returns></returns>
        public List<AssociateAllocationDetails> GetAllocatedPercentage(string empIds)
        {
            List<AssociateAllocationDetails> result = null;
            try
            {
                int[] ids = empIds.Split(',').Select(e => Convert.ToInt32(e)).ToArray();

                using (APEntities hrmsEntity = new APEntities())
                {
                    result = (from aa in hrmsEntity.AssociateAllocations
                              join p in hrmsEntity.Projects on aa.ProjectId equals p.ProjectId
                              join pt in hrmsEntity.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                              where ids.Contains(aa.EmployeeId.Value) && aa.IsActive == true && p.IsActive == true && pt.IsActive == true
                                && pt.PracticeAreaCode != "Talent Pool"
                              group aa by new { aa.EmployeeId } into temp
                              select new AssociateAllocationDetails
                              {
                                  EmployeeId = temp.Key.EmployeeId,
                                  percentage = temp.Sum(u => u.AllocationPercentage)
                              }).ToList();

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get allocation percentage.");
            }

            return result;
        }

        #region CloseTalentRequisition
        /// <summary>
        /// To change the TR status as Closed if all the posittion are filled
        /// </summary>
        /// <param name="tRId"></param>
        public bool CloseTalentRequisition(int? tRId)
        {
            bool returnValue = false;
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var tRDetails = (from tr in hrmsEntities.TalentRequisitions
                                     where tr.TRId == tRId
                                     select tr).FirstOrDefault();
                    int closedStatusId = (from s in hrmsEntities.Status
                                          where s.StatusCode == "Closed"
                                          select s.StatusId).FirstOrDefault();

                    tRDetails.StatusId = closedStatusId;
                    returnValue = hrmsEntities.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to close Talent Requisition.");
            }

            return returnValue;
        }
        #endregion

        //#region SendAllocationNotification
        ///// <summary>
        ///// SendAllocationNotification
        ///// </summary>
        ///// <param name="employeeId"></param>
        ///// <param name="reportingMgrId"></param>
        ///// <param name="hrManagerId"></param>
        //public void SendAllocationNotification(int? employeeId, int reportingMgrId, string hrManagerEmailId, string allocationEffectiveDate, int projectId)
        //{
        //    try
        //    {
        //        using (APEntities hrmEntities = new APEntities())
        //        {
        //            string allocationNotificationCode = resourceManager.GetString("Allocation");
        //            NotificationConfiguration notificationConfiguration = hrmEntities.NotificationConfigurations.Where(j => j.notificationCode == allocationNotificationCode).FirstOrDefault();

        //            if (notificationConfiguration != null)
        //            {
        //                string emailContent = notificationConfiguration.emailContent;
        //                string subject = notificationConfiguration.emailSubject;
        //                var associate = GetEmployeeDetails(employeeId);
        //                var reportingMgr = GetEmployeeDetails(reportingMgrId);
        //                KRA kRA = new KRA();
        //                var competencyLead = kRA.GetAssociateByRole(hrmEntities, resourceManager.GetString("CompetencyLeadRoleCode"));

        //                var itHead = kRA.GetAssociateByRole(hrmEntities, resourceManager.GetString("ITHeadRoleCode"));
        //                string itHeadEmailId = null;
        //                if (itHead != null)
        //                {
        //                    itHeadEmailId = itHead.WorkEmail;
        //                }

        //                var adminHead = kRA.GetAssociateByRole(hrmEntities, resourceManager.GetString("AdminHeadRoleCode"));
        //                string adminHeadEmailId = null;
        //                if (adminHead != null)
        //                {
        //                    adminHeadEmailId = adminHead.WorkEmail;
        //                }

        //                var financeHead = kRA.GetAssociateByRole(hrmEntities, resourceManager.GetString("FinanceHeadRoleCode"));
        //                string financeHeadEmailId = null;
        //                if (financeHead != null)
        //                {
        //                    financeHeadEmailId = financeHead.WorkEmail;
        //                }

        //                var projectDetails = hrmEntities.Projects.Where(p => p.ProjectId == projectId).FirstOrDefault();

        //                if (reportingMgr != null && associate != null && projectDetails != null)
        //                {
        //                    subject = subject.Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails.ProjectName);
        //                    emailContent = emailContent.Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails.ProjectName)
        //                        .Replace("@ReportingManager", reportingMgr.UserName).Replace("@EffectiveDate", allocationEffectiveDate);

        //                    StringBuilder sbToaddress = new StringBuilder();
        //                    sbToaddress.Append(associate.EmailAddress).Append(";");
        //                    if (reportingMgr.EmailAddress != null)
        //                        sbToaddress.Append(reportingMgr.EmailAddress).Append(";");
        //                    if (competencyLead != null)
        //                        sbToaddress.Append(competencyLead.WorkEmail).Append(";");

        //                    if (itHeadEmailId != null)
        //                        sbToaddress.Append(itHeadEmailId).Append(";");

        //                    if (adminHeadEmailId != null)
        //                        sbToaddress.Append(adminHeadEmailId).Append(";");

        //                    if (financeHeadEmailId != null)
        //                        sbToaddress.Append(financeHeadEmailId).Append(";");

        //                    //string toaddress = associate.EmailAddress + ";" + reportingMgr.EmailAddress + ";" + hrManagerEmailId + ";" + competencyLead.WorkEmail + ";" + 
        //                    //    itHeadEmailId + ";" + adminHeadEmailId + ";" + financeHeadEmailId; 
        //                    Email email = new Email();
        //                    int notificationConfigID = new BaseEmail().BuildEmailObject(email, sbToaddress.ToString(), notificationConfiguration.emailFrom, notificationConfiguration.emailCC, subject, emailContent, Enumeration.NotificationStatus.Allocation.ToString());
        //                    new BaseEmail().SendEmail(email, notificationConfigID);
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw new AssociatePortalException("Failed to send notification.");
        //    }
        //}

        public UserCredentials GetEmployeeDetails(int? empId)
        {
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var employeeName = (from e in hrmsEntities.Employees
                                        join usr in hrmsEntities.Users on e.UserId equals usr.UserId
                                        where e.EmployeeId == empId
                                        select new UserCredentials
                                        {
                                            UserName = e.FirstName + " " + e.LastName,
                                            EmailAddress = usr.EmailAddress,
                                            EmployeeCode = e.EmployeeCode,
                                            FirstName = e.FirstName
                                        }).FirstOrDefault();
                    return employeeName;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get employee details.");
            }
        }
        //#endregion

        #region Get matching profiles
        /// <summary>
        /// Get matching profiles
        /// </summary>
        /// <param name="requisitionRoleId"></param>
        /// <returns></returns>
        public List<AssociateAllocationDetails> GetMatchingProfiles(int requisitionRoleDetailId)
        {
            List<AssociateAllocationDetails> profiles = null;

            try
            {

                using (APEntities hrmsEntities = new APEntities())
                {
                    var projectandroleId = (from rd in hrmsEntities.RequisitionRoleDetails
                                            join tr in hrmsEntities.TalentRequisitions on rd.TRId equals tr.TRId
                                            where rd.RequisitionRoleDetailID == requisitionRoleDetailId
                                            select new { tr.ProjectId, rd.RoleMasterId }).FirstOrDefault();

                    profiles = (from es in hrmsEntities.EmployeeSkills
                                join skill in hrmsEntities.RequisitionRoleSkills on es.SkillId equals skill.SkillID
                                join roleSkill in hrmsEntities.RequisitionRoleSkillMappings on skill.RoleSkillMappingID equals roleSkill.ID
                                where es.ProficiencyLevelId >= skill.ProficiencyLevelID
                                && roleSkill.RequistiionRoleDetailID == requisitionRoleDetailId
                                && es.IsActive == true
                                group es by new { es.EmployeeId, es.Employee, roleSkill.RequistiionRoleDetailID } into temp
                                select new AssociateAllocationDetails
                                {
                                    RoleMasterId = projectandroleId.RoleMasterId,
                                    EmployeeId = temp.Key.EmployeeId,
                                    AssociateName = temp.Key.Employee.FirstName + " " + temp.Key.Employee.LastName + "_" + temp.Key.Employee.Grade.GradeCode,
                                    RequisitionRoleDetailID = temp.Key.RequistiionRoleDetailID,
                                    skills = (from t in temp
                                              where t.EmployeeId == temp.Key.EmployeeId
                                              select new EmployeeSkillDetails
                                              {
                                                  skillID = t.SkillId,
                                                  isPrimary = t.IsPrimary,
                                                  proficiencyLevelId = t.ProficiencyLevelId ?? 0

                                              }).ToList()
                                }).ToList();

                    List<int> listAssociate = (from p in profiles select p.EmployeeId ?? 0).Distinct().ToList();

                    //Find allocation
                    var allocatedAssociates = (from aa in hrmsEntities.AssociateAllocations
                                               join p in hrmsEntities.Projects on aa.ProjectId equals p.ProjectId
                                               join pt in hrmsEntities.PracticeAreas on p.PracticeAreaId equals pt.PracticeAreaId
                                               where aa.IsActive == true && p.IsActive == true && pt.IsActive == true
                                               && listAssociate.Contains(aa.EmployeeId ?? 0)
                                               group aa by new { aa.EmployeeId, pt.PracticeAreaId, pt.PracticeAreaCode } into temp
                                               select new
                                               {
                                                   AssociateId = temp.Key.EmployeeId,
                                                   Availability = temp.Key.PracticeAreaCode == "Talent Pool" ? temp.Sum(u => u.AllocationPercentage) : 100 - temp.Sum(u => u.AllocationPercentage)
                                               }).Distinct().ToList();


                    //var allocatedAssociates = (from aa in hrmsEntities.AssociateAllocations
                    //                           where aa.IsActive == true
                    //                           && listAssociate.Contains(aa.EmployeeId ?? 0)
                    //                           group aa by aa.EmployeeId into temp
                    //                           select new
                    //                           {
                    //                               AssociateId = temp.Key,
                    //                               Availability = 100 - temp.Sum(u => u.AllocationPercentage)
                    //                           }).Distinct().ToList();

                    List<EmployeeSkillDetails> targetSkills = (from skill in hrmsEntities.RequisitionRoleSkills
                                                               join roleSkill in hrmsEntities.RequisitionRoleSkillMappings on skill.RoleSkillMappingID equals roleSkill.ID
                                                               where roleSkill.RequistiionRoleDetailID == requisitionRoleDetailId
                                                               select new EmployeeSkillDetails
                                                               {
                                                                   skillID = skill.SkillID,
                                                                   //isPrimary = skill.IsPrimary
                                                               }).Distinct().ToList();

                    //Remove unmatching profiles
                    profiles.RemoveAll(x => x.skills.Count() < Math.Round(targetSkills.Count() / 2.0));

                    List<int?> existingAssociate = (from aa in hrmsEntities.AssociateAllocations
                                                    where aa.IsActive == true &&
                                                    aa.ProjectId == projectandroleId.ProjectId &&
                                                          listAssociate.Contains(aa.EmployeeId ?? 0)
                                                    select aa.EmployeeId).ToList();
                    profiles.RemoveAll(x => existingAssociate.Any(e => e == x.EmployeeId));
                    foreach (AssociateAllocationDetails mf in profiles)
                    {
                        var isPrimary = false;
                        foreach (EmployeeSkillDetails esd in targetSkills.Where(s => s.isPrimary == true && mf.skills.Any(sk => sk.skillID == s.skillID)))
                        {
                            isPrimary = mf.skills.Where(s => s.skillID == esd.skillID).Select(s => s.isPrimary).First() ?? false;
                        }

                        mf.IsFullyMatched = mf.skills.Count >= targetSkills.Count() && isPrimary;
                        mf.Availability = allocatedAssociates.Where(a => a.AssociateId == mf.EmployeeId).Select(a => a.Availability).FirstOrDefault() ?? 100;
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get matching profiles.");
            }
            return profiles;
        }
        #endregion

        #region GetReportingManager
        /// <summary>
        /// Get Reporting Managers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetReportingManager()
        {
            try
            {
                int[] roleIds = new int[3] { (int)Enumeration.Roles.CompetencytLead, (int)Enumeration.Roles.TechnicalLead, (int)Enumeration.Roles.ProjectManager };

                using (APEntities hrmsEntity = new APEntities())
                {
                    var reportingManagers = (from emp in hrmsEntity.Employees
                                             join ur in hrmsEntity.UserRoles on emp.UserId equals ur.UserId
                                             where emp.IsActive == true && ur.IsActive == true
                                             && roleIds.Contains(ur.RoleId ?? 0)
                                             select new
                                             {
                                                 EmployeeId = emp.EmployeeId,
                                                 Name = emp.FirstName + " " + emp.LastName,
                                                 EmployeeName = emp.FirstName + " " + emp.LastName
                                             }).Distinct().ToList();
                    return reportingManagers;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get reporting manager details.");
            }

        }
        #endregion

        public IEnumerable<object> GetTaggedEmployeesByTalentRequisitionId(int talentRequisitionId, int roleId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var taggedEmployees = (from tremp in hrmsEntity.TalentRequisitionEmployeeTags
                                           join emp in hrmsEntity.Employees on tremp.EmployeeID equals emp.EmployeeId
                                           where tremp.TalentRequisitionID == talentRequisitionId
                                           && tremp.RoleMasterID == roleId
                                           select new
                                           {
                                               EmployeeId = tremp.EmployeeID,
                                               EmployeeName = emp.FirstName + " " + emp.LastName
                                           }).Distinct().ToList();
                    return taggedEmployees;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #region Get matched and un-matched skills for Associate
        /// <summary>
        /// Get matched and un-matched skills for Associate
        /// </summary>
        /// <param name="requisitionRoleId"></param>
        ///  <param name="empId"></param>
        /// <returns></returns>
        public IEnumerable<object> GetMatchingskillsforAssociate(int requisitionRoleDetailId, int empId)
        {
            List<SkillsInformation> skilllist = new List<SkillsInformation>();
            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    List<string> targetSkills = (from rrs in hrmsEntities.RequisitionRoleSkills
                                                 join roleSkill in hrmsEntities.RequisitionRoleSkillMappings on rrs.RoleSkillMappingID equals roleSkill.ID
                                                 where roleSkill.RequistiionRoleDetailID == requisitionRoleDetailId
                                                 select rrs.Skill.SkillName).Distinct().ToList<string>();

                    List<string> Matched = (from s in hrmsEntities.EmployeeSkills
                                            where s.EmployeeId == empId && s.IsActive == true
                                            where targetSkills.Contains(s.Skill.SkillName)
                                            select s.Skill.SkillName).Distinct().ToList<string>();

                    IEnumerable<string> Unmatched = targetSkills.Except(Matched);

                    SkillsInformation obj = null;
                    foreach (var tar in targetSkills)
                    {
                        obj = new SkillsInformation();
                        obj.RequestedSkills = tar;
                        skilllist.Add(obj);
                    }
                    foreach (var mac in Matched)
                    {
                        obj = new SkillsInformation();
                        obj.MatchedSkills = mac;
                        skilllist.Add(obj);
                    }
                    foreach (var unmatc in Unmatched)
                    {
                        obj = new SkillsInformation();
                        obj.UnMatchedSkills = unmatc;
                        skilllist.Add(obj);
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
            return skilllist;
        }
        #endregion

        #region Get primary skills of Associate
        /// <summary>
        /// Get primary skills of Associate
        /// </summary>
        /// <param name="associateID"></param>
        ///  <param name=" roleID "></param>
        /// <returns>string message </returns>
        public string CheckPrimaryRoleofAssociate(int? associateID, int? roleID)
        {
            string message = "";

            try
            {
                using (APEntities hrmsEntities = new APEntities())
                {
                    var primaryRoleExistsforrole = (from aa in hrmsEntities.AssociateAllocations
                                                    where aa.EmployeeId == associateID && aa.RoleMasterId == roleID
                                                   && aa.IsPrimary == true && aa.IsActive == true
                                                    select aa).Count();
                    var primaryRoleExists = (from aa in hrmsEntities.AssociateAllocations
                                             where aa.EmployeeId == associateID
                                            && aa.IsPrimary == true && aa.IsActive == true
                                             select aa).Count();
                    if (primaryRoleExistsforrole != 0)
                        message = "Same role is already assigned as primary role";
                    else if (primaryRoleExists != 0)
                        message = "Associate already had a primary role";
                    return message;

                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details.");
            }
        }
        #endregion

        #region SetPrimaryRoleofAssociate
        /// <summary>
        /// SetPrimaryRoleofAssociate
        /// </summary>
        /// <param name="associateAllocationList"></param>
        /// <returns></returns>
        public IEnumerable<object> SetPrimaryRoleofAssociate(List<AssociateAllocationDetails> associateAllocationList)
        {
            List<AssociateAllocationDetails> finalList = new List<AssociateAllocationDetails>();
            AssociateAllocationDetails associateAllocation = null;
            int? associateID, roleID;
            try
            {
                foreach (var s in associateAllocationList)
                {

                    using (APEntities hrmsEntities = new APEntities())
                    {
                        string result = CheckPrimaryRoleofAssociate(s.EmployeeId, s.RoleMasterId);

                        if (result != "")
                            s.IsPrimary = false;
                        else
                            s.IsPrimary = true;

                        associateAllocation = new AssociateAllocationDetails();
                        associateAllocation.AllocationPercentage = s.AllocationPercentage;
                        associateAllocation.InternalBillingPercentage = s.InternalBillingPercentage;
                        associateAllocation.ClientBillingPercentage = s.ClientBillingPercentage;
                        associateAllocation.EffectiveDate = s.EffectiveDate;
                        associateAllocation.EmployeeId = s.EmployeeId;
                        associateAllocation.IsActive = true;
                        //associateAllocation.isCritical = s.isCritical == null ? false : true;
                        //associateAllocation.isCritical = s.BillablePercentage == 0 ? false : true;
                        associateAllocation.ProjectId = s.ProjectId;
                        associateAllocation.RoleMasterId = s.RoleMasterId;
                        associateAllocation.SystemInfo = Commons.GetClientIPAddress();
                        associateAllocation.TalentRequisitionId = s.TalentRequisitionId;
                        associateAllocation.ReportingManagerId = s.ReportingManagerId;
                        associateAllocation.RequisitionRoleDetailID = s.RequisitionRoleDetailID;
                        associateAllocation.IsPrimary = s.IsPrimary;
                    }

                    finalList.Add(associateAllocation);
                }
            }
            catch
            {
                throw;
            }
            return finalList;
        }
        #endregion

        #region ResourceAllocate
        /// <summary>
        /// Code to allocate a associate to project
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        public bool TalentPoolAllocation(int employeeId)
        {
            bool isSaved = false;

            using (APEntities hrmsEntity = new APEntities())
            {
                using (var dbContext = hrmsEntity.Database.BeginTransaction())
                {
                    try
                    {
                        Employee employee = hrmsEntity.Employees.Where(e => e.EmployeeId == employeeId && e.IsActive == true).FirstOrDefault();
                        if (employee != null && employee.CompetencyGroup != null)
                        {
                            var talentPool = (from tp in hrmsEntity.TalentPools
                                              join p in hrmsEntity.Projects on tp.ProjectId equals p.ProjectId
                                              join projectmgrs in hrmsEntity.ProjectManagers on p.ProjectId equals projectmgrs.ProjectID
                                              where tp.PracticeAreaId == employee.CompetencyGroup
                                              select new
                                              {
                                                  ProjectId = p.ProjectId,
                                                  ReportingManagerId = projectmgrs.ReportingManagerID
                                              }).FirstOrDefault();

                            if (talentPool != null)
                            {
                                AllocationPercentage percentage = hrmsEntity.AllocationPercentages.Where(p => p.Percentage == 100).FirstOrDefault();
                                AssociateAllocation existingAllocation = hrmsEntity.AssociateAllocations.Where(e => e.EmployeeId == employeeId && e.IsActive == true).FirstOrDefault();
                                if (object.Equals(existingAllocation, null))
                                {
                                    AssociateAllocation associateAllocation = new AssociateAllocation();
                                    associateAllocation.EmployeeId = employeeId;
                                    // The commented section(TRId,RoleMasterId) needs to be modified when Talent Management module is implemented
                                    // For temporary purpose hard coding the values
                                    associateAllocation.TRId = 1;
                                    associateAllocation.RoleMasterId = 1;
                                    /////////////////////////////////
                                    associateAllocation.ProjectId = talentPool.ProjectId;
                                    associateAllocation.IsActive = true;
                                    associateAllocation.IsBillable = false;
                                    associateAllocation.IsCritical = false;
                                    associateAllocation.IsPrimary = false;
                                    associateAllocation.AllocationPercentage = percentage.AllocationPercentageID;
                                    associateAllocation.InternalBillingPercentage = 0;
                                    associateAllocation.ClientBillingPercentage = 0;
                                    associateAllocation.ReportingManagerId = talentPool.ReportingManagerId;
                                    associateAllocation.EffectiveDate = employee.JoinDate;
                                    associateAllocation.AllocationDate = DateTime.Now;
                                    associateAllocation.CreateDate = DateTime.Now;
                                    associateAllocation.CreatedBy = HttpContext.Current.User.Identity.Name;
                                    associateAllocation.SystemInfo = Commons.GetClientIPAddress();
                                    hrmsEntity.AssociateAllocations.Add(associateAllocation);
                                    isSaved = hrmsEntity.SaveChanges() > 0 ? true : false;
                                    dbContext.Commit();
                                }
                            }
                        }
                        return isSaved;
                    }
                    catch(Exception ex)
                    {
                        dbContext.Rollback();
                        throw new AssociatePortalException("Resource allocation failed."); ;
                    }
                }
            }
        }
        #endregion

        #region VTS methods
        #region GetAllAssociateDetails -This method was written for Visitor Tracking System
        /// <summary>
        /// GetAllAssociateDetails
        /// </summary>
        /// <returns></returns>
        public List<AssociateReport> GetAllAssociateDetails()
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    List<AssociateReport> associateAllocationDetails = (from al in hrmsEntity.AssociateAllocations.AsNoTracking()
                                                                        join e in hrmsEntity.Employees.AsNoTracking() on al.EmployeeId equals e.EmployeeId
                                                                        join p in hrmsEntity.Projects.AsNoTracking() on al.ProjectId equals p.ProjectId
                                                                        join projectmgrs in hrmsEntity.ProjectManagers.AsNoTracking() on p.ProjectId equals projectmgrs.ProjectID
                                                                        join rmgr in hrmsEntity.Employees.AsNoTracking() on al.ReportingManagerId equals rmgr.EmployeeId
                                                                        join pmgr in hrmsEntity.Employees.AsNoTracking() on projectmgrs.ReportingManagerID equals pmgr.EmployeeId
                                                                        where (al.IsActive == true && p.IsActive == true)
                                                                        select new AssociateReport
                                                                        {
                                                                            AssociateCode = e.EmployeeCode,
                                                                            ProjectName = p.ProjectName,
                                                                            AssociateName = e.FirstName + " " + e.LastName,
                                                                            ReportingManager = rmgr.FirstName + " " + rmgr.LastName,
                                                                            ProgramManager = pmgr.FirstName + " " + pmgr.LastName
                                                                        }).OrderBy(i => i.AssociateName).ThenBy(i => i.ProjectName).ToList();

                    return associateAllocationDetails;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAssociateDetailsById -This method was written for Visitor Tracking System
        /// <summary>
        /// GetAssociateDetailsById
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public AssociateReport GetAssociateDetailsById(string empId)
        {
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    AssociateReport associateAllocationDetails = (from al in hrmsEntity.AssociateAllocations.AsNoTracking()
                                                                  join e in hrmsEntity.Employees.AsNoTracking() on al.EmployeeId equals e.EmployeeId
                                                                  join p in hrmsEntity.Projects.AsNoTracking() on al.ProjectId equals p.ProjectId
                                                                  join projectmgrs in hrmsEntity.ProjectManagers.AsNoTracking() on p.ProjectId equals projectmgrs.ProjectID
                                                                  join rmgr in hrmsEntity.Employees.AsNoTracking() on al.ReportingManagerId equals rmgr.EmployeeId
                                                                  join pmgr in hrmsEntity.Employees.AsNoTracking() on projectmgrs.ReportingManagerID equals pmgr.EmployeeId
                                                                  where (e.EmployeeCode.ToUpper().Equals(empId.ToUpper()) && al.IsActive == true && p.IsActive == true
                                                                  && e.IsActive == true && rmgr.IsActive == true && pmgr.IsActive == true)
                                                                  select new AssociateReport
                                                                  {
                                                                      AssociateCode = e.EmployeeCode,
                                                                      ProjectName = p.ProjectName,
                                                                      AssociateName = e.FirstName + " " + e.LastName,
                                                                      ReportingManager = rmgr.FirstName + " " + rmgr.LastName,
                                                                      ProgramManager = pmgr.FirstName + " " + pmgr.LastName
                                                                  }).FirstOrDefault();

                    return associateAllocationDetails;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
        #endregion

        #region GetEmployeeByDeptId
        /// <summary>
        /// this method is used to get employees in a department
        /// </summary>
        /// <returns></returns>        
        public List<AssociateDetails> GetEmployeeByDeptId(int deptId)
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<AssociateDetails> employeeList = hrmsEntities.Employees.Where(dept => dept.DepartmentId == deptId && dept.IsActive == true).AsQueryable()
                                                                            .Select(data => new AssociateDetails
                                                                            {
                                                                                empID = data.EmployeeId,
                                                                                empName = data.FirstName + " " + data.LastName
                                                                            });

                return employeeList.ToList<AssociateDetails>();
            }

        }
        #endregion

        #region GetTalentPoolList
        /// <summary>
        /// this method is used to get talent pool list
        /// </summary>
        /// <returns></returns>        
        public List<PracticeAreaDetails> GetTalentPoolList()
        {
            using (APEntities hrmsEntities = new APEntities())
            {
                IQueryable<PracticeAreaDetails> practiseAreaList = hrmsEntities.PracticeAreas.Where(area => area.IsActive == true).AsQueryable()
                                                                            .Select(data => new PracticeAreaDetails
                                                                            {
                                                                                PracticeAreaId = data.PracticeAreaId,
                                                                                PracticeAreaCode = data.PracticeAreaCode
                                                                            });

                return practiseAreaList.ToList<PracticeAreaDetails>();
            }

        }
        #endregion

        #region AddToTalentPool
        /// <summary>
        /// AddToTalentPool
        /// </summary>
        /// <param name="talentPoolData"></param>
        /// <returns>bool</returns>
        public bool AddToTalentPool(TalentPoolData talentPoolData)
        {
            bool status = false;
            using (APEntities hrmsEntities = new APEntities())
            {
                TalentPool talentpool = new TalentPool()
                {
                    PracticeAreaId = talentPoolData.PracticeAreaId,
                    ProjectId = talentPoolData.ProjectId,
                    IsActive = talentPoolData.IsActive,
                    CreatedBy = HttpContext.Current.User.Identity.Name
                };
                hrmsEntities.TalentPools.Add(talentpool);
                status = hrmsEntities.SaveChanges() > 0 ? true : false;
            }
            return status;
        }
        #endregion

        #region GetEmployeePrimaryAllocationProject
        /// <summary>
        /// Get Employee Primary Allocation Project details
        /// </summary>
        /// <returns>List</returns>
        public List<AssociateAllocationDetails> GetEmployeePrimaryAllocationProject(int EmployeeId)
        {
            List<AssociateAllocationDetails> employeeList;
            try
            {
                using (var apEntities = new APEntities())
                {
                    employeeList = apEntities.Database.SqlQuery<AssociateAllocationDetails>
                              ("[usp_GetEmployeePrimaryAllocationProject] @EmployeeID",
                             new object[] {
                                        new SqlParameter ("EmployeeID", EmployeeId)

                             }).ToList();

                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get project details.");
            }
            return employeeList;
        }
        #endregion


        #region TemporaryAllocationsandReleases

        #region GetEmployeesAllocations
        /// <summary>
        /// Code to get the requested allocations
        /// </summary>
        /// <returns></returns>
        public async Task<List<TagAssociateList>> GetEmployeesForAllocations()
        {
            List<TagAssociateList> returnObject = null;

            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    returnObject = await hrmsEntity.Database.SqlQuery<TagAssociateList>
                            ("[usp_GetEmployeesForAllocations]",
                                new object[] {
                                       
                                }
                                ).ToListAsync();
                }
            }
            catch(Exception ex)
            {
                throw new AssociatePortalException("Failed to get employees.");
            }

            return returnObject.OrderBy(o => o.EmployeeName).ToList();
        }
        #endregion

        #region SendAllocationNotification
        /// <summary>
        /// SendAllocationNotification
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="reportingMgrId"></param>
        /// <param name="hrManagerId"></param>
        public void SendAllocationNotification(int? employeeId, int reportingManagerId, int? primaryReportingManagerId, string allocationEffectiveDate, int projectId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    EmailNotificationConfiguration notificationConfiguration = new KRA().getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.Allocation), Convert.ToInt32(Enumeration.CategoryMaster.TalentRequisition));

                    //Project projectDetails = hrmEntities.Projects.Where(p => p.ProjectId == projectId).FirstOrDefault();
                    List<ProjectData> projectDetails = GetProjectDetailById(projectId);
                    ProjectManager projectManager = hrmEntities.ProjectManagers.Where(pm => pm.ProjectID == projectId && pm.IsActive == true).FirstOrDefault();

                    if (notificationConfiguration != null)
                    {
                        string subject = notificationConfiguration.EmailSubject;
                        var associate = GetEmployeeDetails(employeeId);
                        var programMgr = GetEmployeeDetails(projectManager.ProgramManagerID);
                        var reportingMgr = GetEmployeeDetails(reportingManagerId);
                        var primaryReportingMgr = GetEmployeeDetails(primaryReportingManagerId);
                        var initiatedOn = DateTime.UtcNow;
                        var mailIdOfInitiator = HttpContext.Current.User.Identity.Name;
                        var initiatedBy = (from e in hrmEntities.Employees
                                           join users in hrmEntities.Users on e.UserId equals users.UserId
                                           where users.EmailAddress == mailIdOfInitiator
                             select new UserCredentials
                             {
                                 UserName = e.FirstName + " " + e.LastName,
                                
                             }).FirstOrDefault();

                        if (reportingMgr != null && associate != null && projectDetails != null && primaryReportingMgr != null && initiatedOn != null && initiatedBy != null)
                        {
                            subject = subject.Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails[0].ProjectName).Replace("@EmployeeCode", associate.EmployeeCode);
                            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(notificationConfiguration.EmailContent));



                            NotificationDetail notificationDetail = new NotificationDetail();
                            notificationDetail.FromEmail = notificationConfiguration.EmailFrom;
                            notificationDetail.Subject = subject;

                            StringBuilder sbToaddress = new StringBuilder();
                            sbToaddress.Append(notificationConfiguration.EmailTo).Append(";");
                            notificationDetail.ToEmail = sbToaddress.ToString();

                            StringBuilder sbCcAddress = new StringBuilder();
                            sbCcAddress.Append(notificationConfiguration.EmailCC).Append(";");
                            if (reportingMgr.EmailAddress != null)
                                sbCcAddress.Append(reportingMgr.EmailAddress).Append(";");
                            if (programMgr.EmailAddress != null && employeeId != projectManager.LeadID && projectManager.LeadID != projectManager.ProgramManagerID)
                                sbCcAddress.Append(programMgr.EmailAddress).Append(";");
                            if (associate.EmailAddress != null)
                                sbCcAddress.Append(associate.EmailAddress).Append(";");
                            if (reportingManagerId != primaryReportingManagerId)
                            {
                                if (primaryReportingMgr.EmailAddress != null)
                                {
                                    sbCcAddress.Append(primaryReportingMgr.EmailAddress).Append(";");
                                    emailContent = emailContent.Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails[0].ProjectName).Replace("@FirstName", associate.FirstName)
                                        .Replace("@ReportingManager", primaryReportingMgr.UserName).Replace("@EffectiveDate", allocationEffectiveDate);
                                }
                            }
                            else
                                emailContent = emailContent.Replace("@initiatedBy", initiatedBy.UserName).Replace("@initiatedOn", String.Format("{0:MMM dd, yyyy}", initiatedOn)).Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails[0].ProjectName).Replace("@FirstName", associate.FirstName)
                                    .Replace("@ReportingManager", reportingMgr.UserName).Replace("@EffectiveDate", allocationEffectiveDate);

                            notificationDetail.CcEmail = sbCcAddress.ToString();
                            notificationDetail.EmailBody = emailContent.ToString();
                            NotificationManager.SendEmail(notificationDetail);
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to send notification.");
            }
        }

        #endregion

        #region TemporaryReleaseAssociate
        /// <summary>
        /// Code to release an associate from a project
        /// </summary>
        /// <param name="associateDetails"></param>
        /// <returns></returns>
        public bool TemporaryReleaseAssociate(AssociateAllocationDetails associateDetails)
        {
            int rowsAffected = 0;

            using (APEntities hrmsEntity = new APEntities())
            {
                using (var dbContext = hrmsEntity.Database.BeginTransaction())
                {
                    try
                    {
                        int? reportingMangerId = hrmsEntity.AssociateAllocations.Where(a => a.EmployeeId == associateDetails.EmployeeId && a.ProjectId == associateDetails.ProjectId && a.IsActive == true).FirstOrDefault().ReportingManagerId;
                        if (reportingMangerId > 0)
                        {
                            rowsAffected = hrmsEntity.Database.SqlQuery<int>
                                 ("[usp_ReleaseAssociateTemporary] @ProjectId, @EmployeeId, @TalentPoolProjectId,@ReleaseDate, @TalentRequisitionId,@MakePrimaryProjectId,@IsPrimary, @ModifiedDate, @ModifiedBy, @SystemInfo",
                                   new object[] {
                                    new SqlParameter("ProjectId", associateDetails.ProjectId),
                                    new SqlParameter("EmployeeId", associateDetails.EmployeeId),
                                    new SqlParameter("TalentPoolProjectId",associateDetails.ReleaseProjectId),                                    
                                    //new SqlParameter("ReportingManagerId",associateDetails.ReportingManagerId),
                                    new SqlParameter("ReleaseDate",associateDetails.ReleaseDate),
                                    new SqlParameter("TalentRequisitionId", associateDetails.TalentRequisitionId),
                                    new SqlParameter("MakePrimaryProjectId",associateDetails.MakePrimaryProjectId),
                                    new SqlParameter("IsPrimary",associateDetails.IsPrimary),
                                    new SqlParameter("ModifiedDate", DateTime.UtcNow),
                                    new SqlParameter("ModifiedBy",HttpContext.Current.User.Identity.Name),
                                    new SqlParameter("SystemInfo",  Commons.GetClientIPAddress())
                                   }
                                   ).SingleOrDefault();
                            dbContext.Commit();

                            if (associateDetails.NotifyAll == true)
                                SendReleaseNotification(associateDetails.EmployeeId, String.Format("{0:MMM dd, yyyy}", associateDetails.ReleaseDate), associateDetails.ProjectId, reportingMangerId);
                        }
                        return rowsAffected > 0 ? true : false;
                    }
                    catch
                    {
                        dbContext.Rollback();
                        throw;
                    }
                }
            }

        }
        #endregion       

        #region SendReleaseNotification
        /// <summary>
        /// SendReleaseNotification
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="reportingManagerId"></param>
        /// <param name="allocationEffectiveDate"></param>
        /// <param name="projectId"></param>
        public void SendReleaseNotification(int? employeeId, string allocationEffectiveDate, int? projectId, int? reportingManagerId)
        {
            try
            {
                using (APEntities hrmEntities = new APEntities())
                {
                    EmailNotificationConfiguration notificationConfiguration = new KRA().getEmailTemplate(Convert.ToInt32(Enumeration.NotificationType.Release), Convert.ToInt32(Enumeration.CategoryMaster.TalentRequisition));

                    //Project projectDetails = hrmEntities.Projects.Where(p => p.ProjectId == projectId).FirstOrDefault();
                    List<ProjectData> projectDetails = GetProjectDetailById(projectId);
                    ProjectManager projectManager = hrmEntities.ProjectManagers.Where(pm => pm.ProjectID == projectId && pm.IsActive == true).FirstOrDefault();

                    if (notificationConfiguration != null)
                    {
                        string subject = notificationConfiguration.EmailSubject;
                        var associate = GetEmployeeDetails(employeeId);
                        var reportingMgr = GetEmployeeDetails(reportingManagerId);
                        var programMgr = GetEmployeeDetails(projectManager.ProgramManagerID);
                        var initiatedOn = DateTime.UtcNow;
                        var mailIdOfInitiator = HttpContext.Current.User.Identity.Name;
                        var initiatedBy = (from e in hrmEntities.Employees
                                           join users in hrmEntities.Users on e.UserId equals users.UserId
                                           where users.EmailAddress == mailIdOfInitiator
                                           select new UserCredentials
                                           {
                                               UserName = e.FirstName + " " + e.LastName,

                                           }).FirstOrDefault();

                        if (associate != null && projectDetails != null)
                        {

                            StringBuilder emailContent = new StringBuilder(WebUtility.HtmlDecode(notificationConfiguration.EmailContent));
                            subject = subject.Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails[0].ProjectName).Replace("@EmployeeCode", associate.EmployeeCode);
                            emailContent = emailContent.Replace("@initiatedBy", initiatedBy.UserName).Replace("@initiatedOn", String.Format("{0:MMM dd, yyyy}", initiatedOn)).Replace("@AssociateName", associate.UserName).Replace("@ProjectName", projectDetails[0].ProjectName)
                               .Replace("@EffectiveDate", allocationEffectiveDate);

                            NotificationDetail notificationDetail = new NotificationDetail();
                            notificationDetail.EmailBody = emailContent.ToString();
                            notificationDetail.FromEmail = notificationConfiguration.EmailFrom;
                            notificationDetail.Subject = subject;

                            StringBuilder sbToaddress = new StringBuilder();
                            sbToaddress.Append(notificationConfiguration.EmailTo).Append(";");
                            notificationDetail.ToEmail = sbToaddress.ToString();

                            StringBuilder sbCcAddress = new StringBuilder();
                            sbCcAddress.Append(notificationConfiguration.EmailCC).Append(";");
                            if (reportingMgr.EmailAddress != null)
                                sbCcAddress.Append(reportingMgr.EmailAddress).Append(";");
                            if (programMgr.EmailAddress != null && employeeId != projectManager.LeadID && projectManager.LeadID != projectManager.ProgramManagerID)
                                sbCcAddress.Append(programMgr.EmailAddress).Append(";");
                            if (associate.EmailAddress != null)
                                sbCcAddress.Append(associate.EmailAddress).Append(";");
                            notificationDetail.CcEmail = sbCcAddress.ToString();

                            NotificationManager.SendEmail(notificationDetail);
                        }
                    }
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to send notification.");
            }
        }

        #endregion
        private List<ProjectData> GetProjectDetailById(int? projectId)
        {            
            try
            {
                using (APEntities hrmsEntity = new APEntities())
                {
                    var projectData = hrmsEntity.Database.SqlQuery<ProjectData>
                            ("[usp_GetProjectDetailById] @ProjectId",
                                new object[] {
                                        new SqlParameter ("ProjectId", projectId)
                                }
                                ).ToList();
                    return projectData;
                }
            }
            catch (Exception ex)
            {
                throw new AssociatePortalException("Failed to get project details");
            }
           
        }

        #region GetAssociatesToRelease
        /// <summary>
        /// Retrieves the basic details of associates
        /// </summary>
        /// <returns></returns>
        /// 
        public AssociateNamesProjectsList GetAssociatesToRelease(int employeeId, string roleName)
        {
            try
            {
                string projectType = new ProjectDetails().GetEnumDescription(Enumeration.ProjectTypes.TalentPool);
                using (APEntities hrmsEntity = new APEntities())
                {
                    //decimal percentage = hrmsEntity.AllocationPercentages.Where(p => p.Percentage == 100).FirstOrDefault().AllocationPercentageID;
                    List<AssociateNameandProject> lstAssociateNamesList;

                    lstAssociateNamesList = hrmsEntity.Database.SqlQuery<AssociateNameandProject>
                              ("[usp_GetAssociatesforReleaseAndResourceReports] @EmployeeID, @RoleName",
                              new object[] {
                                    new SqlParameter("EmployeeID", employeeId),
                                    new SqlParameter("RoleName", roleName)})
                              .ToList();

                    AssociateNamesProjectsList associateNamesProjectsList = new AssociateNamesProjectsList();

                    var employeeNames = lstAssociateNamesList.Select(employee => new { Id = employee.Id, Name = employee.Name }).ToList().Distinct();
                    associateNamesProjectsList.EmployeeNames = new List<object>();
                    associateNamesProjectsList.EmployeeNames.AddRange(employeeNames);
                    associateNamesProjectsList.EmployeeProjects = lstAssociateNamesList.Select(project => new AssociateNameandProject
                    {
                        Id = project.Id,
                        ProjectId = project.ProjectId,
                        EffectiveDate = project.EffectiveDate
                    }).ToList();

                    return associateNamesProjectsList;
                }
            }
            catch
            {
                throw new AssociatePortalException("Failed to get details");
            }
        }
        #endregion
        #endregion        
    }
}

