﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using AP.DomainEntities;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using AP.Services.Controllers.Tests;
using System;
using AP.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.UnitTests
{
    [TestClass]
    public class CustomKRAsControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region GetOrganizationAndCustomKRAsForPMDHTest
        /// <summary>
        /// GetOrganizationAndCustomKRAsForPMDHTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv"), TestMethod]

        public void GetOrganizationAndCustomKRAsForPMDHTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeID = 186;// Convert.ToInt32(TestContext.DataRow["EmployeeID"]);
                int FinancialYearID = 1;//Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                string KRAAspectMetric = Convert.ToString(TestContext.DataRow["KRAAspectMetric"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CustomKRAs/GetOrganizationAndCustomKRAsForPMDH?employeeID=" + EmployeeID + "&&financialYearID=" + FinancialYearID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var kraData = result.Result;
                        //deserialize to your class
                        List<OrganizationKRAs> krasList = JsonConvert.DeserializeObject<List<OrganizationKRAs>>(kraData);
                        OrganizationKRAs kras = krasList.Find(kra => kra.KRAAspectMetric.ToLower() == KRAAspectMetric.ToLower());
                        Assert.AreEqual(KRAAspectMetric, kras.KRAAspectMetric);
                        List<CustomKRAs> kraList = JsonConvert.DeserializeObject<List<CustomKRAs>>(kraData);
                        CustomKRAs kras1 = kraList.Find(kra => kra.KRAAspectMetric.ToLower() == KRAAspectMetric.ToLower());
                        Assert.AreEqual(KRAAspectMetric, kras.KRAAspectMetric);
                    }
                }

            };
        }
        #endregion

        #region SaveCustomKRAsTest
        /// <summary>
        /// SaveCustomKRAsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv", "TestInput"), TestMethod]
        public void SaveCustomKRAsTest()
        {
            try
            {
                List<int> lstEmployeeIDs = new List<int>();
                lstEmployeeIDs.Add(186);
                lstEmployeeIDs.Add(237);
                CustomKRAs customKRAs = new CustomKRAs
                {
                    KRAAspectID = Convert.ToInt32(TestContext.DataRow["KRAAspectID"]),
                    KRAAspectMetric = Convert.ToString(TestContext.DataRow["KRAAspectMetric"]),
                    KRAAspectTarget = Convert.ToString(TestContext.DataRow["KRAAspectTarget"]),
                    FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]),
                    EmployeeIDs = lstEmployeeIDs
                };

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/CustomKRAs/SaveCustomKRAs", new StringContent(JsonConvert.SerializeObject(customKRAs).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetProjectsByReportingManagerIDTest
        /// <summary>
        /// GetProjectsByReportingManagerIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv"), TestMethod]

        public void GetProjectsByReportingManagerIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["EmployeeID"]);
                string Name = Convert.ToString(TestContext.DataRow["ProjectName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CustomKRAs/GetProjectsByReportingManagerID?projectManagerID=" + EmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var projectsData = result.Result;
                        //deserialize to your class
                        List<GenericType> projectsList = JsonConvert.DeserializeObject<List<GenericType>>(projectsData);
                        GenericType projects = projectsList.Find(kra => kra.Name.ToLower() == Name.ToLower());
                        Assert.AreEqual(Name, projects.Name);
                    }
                }

            };
        }
        #endregion

        #region GetEmployeesByProjectIDTest
        /// <summary>
        /// GetEmployeesByProjectIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv"), TestMethod]

        public void GetEmployeesByProjectIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int ProjectID = Convert.ToInt32(TestContext.DataRow["ProjectID"]);
                string Name = Convert.ToString(TestContext.DataRow["EmployeeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CustomKRAs/GetEmployeesByProjectID?projectID=" + ProjectID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<GenericType> employeesList = JsonConvert.DeserializeObject<List<GenericType>>(employeeData);
                        GenericType employees = employeesList.Find(employee => employee.Name.ToLower() == Name.ToLower());
                        Assert.AreEqual(Name, employees.Name);
                    }
                }

            };
        }
        #endregion

        #region GetEmployeesForDepartmentTest
        /// <summary>
        /// GetEmployeesForDepartmentTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv"), TestMethod]

        public void GetEmployeesForDepartmentTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["EID"]);
                string Name = Convert.ToString(TestContext.DataRow["Employee"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CustomKRAs/GetEmployeesForDepartment?employeeID=" + EmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<GenericType> employeesList = JsonConvert.DeserializeObject<List<GenericType>>(employeeData);
                        GenericType employees = employeesList.Find(employee => employee.Name.ToLower() == Name.ToLower());
                        Assert.AreEqual(Name, employees.Name);
                    }
                }

            };
        }
        #endregion

        #region EditCustomKRAsTest
        /// <summary>
        /// EditCustomKRAsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv", "TestInput"), TestMethod]
        public void EditCustomKRAsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                CustomKRAs customKRAs = new CustomKRAs();
                customKRAs.CustomKRAID = Convert.ToInt32(TestContext.DataRow["CustomKRAID"]);
                customKRAs.KRAAspectID = Convert.ToInt32(TestContext.DataRow["KRAAspectID"]);
                customKRAs.KRAAspectMetric = TestContext.DataRow["KRAAspectMetric"].ToString();
                customKRAs.KRAAspectTarget = TestContext.DataRow["KRAAspectTarget"].ToString();

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/CustomKRAs/EditCustomKRAs", new StringContent(JsonConvert.SerializeObject(customKRAs).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        //#region DeleteCustomKRATest
        ///// <summary>
        ///// DeleteCustomKRATest
        ///// </summary>
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv", "TestInput"), TestMethod]
        //public void DeleteCustomKRATest()
        //{
        //    int customKRAID = 3;
        //    KRA kra = new KRA();
        //    kra.DeleteCustomKRA(customKRAID);

        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //        //::TODO:: Unable to send parameter ID 
        //        using (HttpResponseMessage response = client.PostAsync(serviceURL + "/CustomKRAs/DeleteCustomKRA", new StringContent(JsonConvert.SerializeObject(kra), Encoding.UTF8, "application/json")).Result)
        //        {
        //            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //        }
        //    };
        //}
        //#endregion

    }
}