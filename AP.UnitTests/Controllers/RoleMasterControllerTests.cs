﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using System;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class RoleMasterControllerTests : BaseControllerTests
    {

        #region CreateUserRoleTest
        /// <summary>
        /// CreateUserRoleTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Roles.csv", "Roles#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Roles.csv"), TestMethod]
        public void CreateUserRoleTest()
        {
            try
            {
                RoleData roleData = new RoleData();
                if (!Convert.IsDBNull(TestContext.DataRow["roleName"]) || !Convert.IsDBNull(TestContext.DataRow["roleDesc"]) || !Convert.IsDBNull(TestContext.DataRow["DepartmentId"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]) || !Convert.IsDBNull(TestContext.DataRow["KeyResponsibilities"]))
                {
                    roleData.RoleName = TestContext.DataRow["roleName"].ToString();
                    roleData.RoleDescription = TestContext.DataRow["roleDesc"].ToString();
                    roleData.KeyResponsibilities = TestContext.DataRow["KeyResponsibilities"].ToString();
                    roleData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                    roleData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                    List<RoleCompetencySkills> RoleSkillsList = new List<RoleCompetencySkills>();
                    RoleSkillsList.Add(new RoleCompetencySkills { CompetencyAreaId = 1, SkillGroupId = 1, SkillId = 1, ProficiencyLevelId = 1, IsPrimary = true});
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/RoleMaster/CreateUserRole/", new StringContent(JsonConvert.SerializeObject(roleData), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };


            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        #region GetRoleMasterDetailsTest
        /// <summary>
        /// GetRoleMasterDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetRoleMasterDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/RoleMaster/GetRoleMasterDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region UpdateRoleMasterDetailsTest
        /// <summary>
        /// UpdateRoleMasterDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Roles.csv", "Roles#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Roles.csv"), TestMethod]
        public void UpdateRoleMasterDetailsTest()
        {
            try
            {
                RoleData roleData = new RoleData();
                if (!Convert.IsDBNull(TestContext.DataRow["roleID"]) || !Convert.IsDBNull(TestContext.DataRow["roleName"]) || !Convert.IsDBNull(TestContext.DataRow["roleDesc"]) || !Convert.IsDBNull(TestContext.DataRow["DepartmentId"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]) || !Convert.IsDBNull(TestContext.DataRow["KeyResponsibilities"]))
                {
                    roleData.RoleId = Convert.ToInt32(TestContext.DataRow["roleID"].ToString());
                    roleData.RoleName = TestContext.DataRow["roleName"].ToString();
                    roleData.RoleDescription = TestContext.DataRow["roleDesc"].ToString();
                    roleData.KeyResponsibilities = TestContext.DataRow["KeyResponsibilities"].ToString();
                    roleData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                    roleData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                    List<RoleCompetencySkills> competencySkill = new List<RoleCompetencySkills>();
                    competencySkill.Add(new RoleCompetencySkills {SkillId =1,CompetencyAreaId=1,ProficiencyLevelId=3,IsPrimary=true });
                    roleData.RoleCompetencySkills = competencySkill;
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/RoleMaster/UpdateRoleMasterDetails", new StringContent(JsonConvert.SerializeObject(roleData), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

    }
}