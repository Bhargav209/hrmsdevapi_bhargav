﻿using System;
using System.Text;
using System.Net.Http;
using System.Net;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Newtonsoft.Json;
using AP.DomainEntities;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class AssociateEducationDetailControllerTests:BaseControllerTests
    {
      
        #region UpdateEducationDetailsTest
        /// <summary>
        /// Update Education Details Test
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeEducationDetails.csv", "EmployeeEducationDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeEducationDetails.csv", "TestInput"), TestMethod]
        public void UpdateEducationDetailsTest()
        {
            UserDetails userDetails = new UserDetails();           

            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);

            List<EducationDetails> educationDetailsList = new List<EducationDetails>();
            EducationDetails educationDetails = new EducationDetails();
           
            educationDetailsList.Add(new EducationDetails { programTypeID = 0, marks = "100%", qualificationName = "10TH", completedYear = "12-12-2009" });
            educationDetailsList.Add(educationDetails);

            userDetails.qualifications = educationDetailsList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateEducationDetail/SaveEducationDetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            };
        }
        #endregion

        #region GetEducationDetailsByIDTest
        /// <summary>
        /// Get Education Details By ID Test
        /// </summary>
        [TestMethod()]
        public void GetEducationDetailsByIDTest()
        {
            int empID = 324;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateEducationDetail/GetEducationDetailsByID?empID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

    }
}