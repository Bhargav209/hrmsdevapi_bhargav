﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using System.Configuration;
using System;
using Newtonsoft.Json;
using System.Text;
using AP.DomainEntities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AP.Services.Controllers.Tests;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class MenuControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region GetParent
        /// <summary>
        /// GetParent
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetParent()
        {
            using (HttpClient client = new HttpClient())
            {
                string Title = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["Title"].ToString()))
                {
                    Title = TestContext.DataRow["Title"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetParent"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var parentData = result.Result;
                        //deserialize to your class
                        List<MenuData> data = JsonConvert.DeserializeObject<List<MenuData>>(parentData);
                        MenuData menuData = data.Find(list => list.Title.ToLower().Trim() == Title.ToLower().Trim());
                        Assert.AreEqual(Title, menuData.Title);
                    }
                }

            };
        }
        #endregion

        #region AddMenuTest
        /// <summary>
        /// AddMenuTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv", "TestInput"), TestMethod]
        public void AddMenuTest()
        {
            try
            {

                MenuData menuData = new MenuData();
                string title = TestContext.DataRow["Title"].ToString();
                if (!string.IsNullOrEmpty(title))
                {
                    menuData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                    menuData.ParentId = Convert.ToInt32(TestContext.DataRow["ParentId"]);
                    menuData.DisplayOrder = Convert.ToInt32(TestContext.DataRow["DisplayOrder"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Menu/AddMenu", new StringContent(JsonConvert.SerializeObject(menuData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var data = result.Result;
                            //deserialize to your class
                            List<MenuData> lstMenuData = JsonConvert.DeserializeObject<List<MenuData>>(data);
                            MenuData menus = lstMenuData.Find(menu => menu.Title.ToLower().Trim() == menuData.Title.ToLower().Trim());
                            Assert.AreEqual(menuData.Title, menus.Title);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateMenuTest
        /// <summary>
        /// UpdateMenuTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv", "TestInput"), TestMethod]
        public void UpdateMenuTest()
        {
            using (HttpClient client = new HttpClient())
            {
                MenuData menuDataForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of menus
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var data = result.Result;
                        List<MenuData> lstMenuData = JsonConvert.DeserializeObject<List<MenuData>>(data);
                        //Update one of the object state                
                        if (lstMenuData.Count > 0)
                        {
                            menuDataForUpdate = lstMenuData[0];
                        }
                        menuDataForUpdate.Title += " Update Test" + rnd.Next(1, 1000).ToString();
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Menu/UpdateMenu", new StringContent(JsonConvert.SerializeObject(menuDataForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var menuData = result.Result;
                        List<MenuData> lstMenuData = JsonConvert.DeserializeObject<List<MenuData>>(menuData);
                        MenuData menus = lstMenuData.Find(menu => menu.Title.ToLower().Trim() == menuDataForUpdate.Title.ToLower().Trim());
                        Assert.AreEqual(menuDataForUpdate.Title.ToLower(), menus.Title.ToLower());
                    }
                }
            };
        }
        #endregion

        #region GetMenusTest
        /// <summary>
        /// GetMenusTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetMenusTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string title = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["Title"].ToString()))
                {
                    title = TestContext.DataRow["Title"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var menuData = result.Result;
                        //deserialize to your class
                        List<MenuData> menuList = JsonConvert.DeserializeObject<List<MenuData>>(menuData);
                        MenuData menu = menuList.Find(data => data.Title.ToLower().Trim() == title.ToLower().Trim());
                        Assert.AreEqual(title, menu.Title);
                    }
                }

            };
        }
        #endregion

        #region GetMenuTitlesTest
        /// <summary>
        /// GetMenuTitlesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetMenuTitlesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Title = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["Title"].ToString()))
                {
                    Title = TestContext.DataRow["Title"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenuTitles"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var titlesData = result.Result;
                        //deserialize to your class
                        List<MenuData> data = JsonConvert.DeserializeObject<List<MenuData>>(titlesData);
                        MenuData menuData = data.Find(list => list.Title.ToLower().Trim() == Title.ToLower().Trim());
                        Assert.AreEqual(Title, menuData.Title);
                    }
                }

            };
        }
        #endregion

        #region GetRolesTest
        /// <summary>
        /// GetRolesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetRolesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Role = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["Role"].ToString()))
                {
                    Role = TestContext.DataRow["Role"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetRoles"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var rolesData = result.Result;
                        //deserialize to your class
                        List<RoleData> roleList = JsonConvert.DeserializeObject<List<RoleData>>(rolesData);
                        RoleData data = roleList.Find(list => list.RoleName.ToLower().Trim() == Role.ToLower().Trim());
                        Assert.AreEqual(Role, data.RoleName);
                    }
                }

            };
        }
        #endregion

        #region AddMenuRolesTest
        /// <summary>
        /// AddMenuRolesTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuRoleData.csv", "MenuRoleData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuRoleData.csv", "TestInput"), TestMethod]
        public void AddMenuRolesTest()
        {
            try
            {

                MenuRoleData menuRoleData = new MenuRoleData();
                string menuId = TestContext.DataRow["menuId"].ToString();
                if (menuId != null)
                {
                    menuRoleData.MenuId = Convert.ToInt32(TestContext.DataRow["MenuId"]);
                    menuRoleData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Menu/AddMenuRoles", new StringContent(JsonConvert.SerializeObject(menuRoleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var data = result.Result;
                            //deserialize to your class
                            List<MenuRoleData> lstMenuData = JsonConvert.DeserializeObject<List<MenuRoleData>>(data);
                            MenuRoleData menus = lstMenuData.Find(menu => menu.MenuId == menuRoleData.MenuId);
                            Assert.AreEqual(menuRoleData.MenuId, menus.MenuId);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateMenuRolesTest
        /// <summary>
        /// UpdateMenuRolesTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuRoleData.csv", "MenuRoleData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuRoleData.csv", "TestInput"), TestMethod]
        public void UpdateMenuRolesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                MenuData menuDataForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of menus
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var data = result.Result;
                        List<MenuData> lstMenuData = JsonConvert.DeserializeObject<List<MenuData>>(data);
                        //Update one of the object state                
                        if (lstMenuData.Count > 0)
                        {
                            menuDataForUpdate = lstMenuData[5];
                        }
                        menuDataForUpdate.Title += " Update Test" + rnd.Next(1, 1000).ToString();
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Menu/UpdateMenu", new StringContent(JsonConvert.SerializeObject(menuDataForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenus"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var menuData = result.Result;
                        List<MenuData> lstMenuData = JsonConvert.DeserializeObject<List<MenuData>>(menuData);
                        MenuData menus = lstMenuData.Find(menu => menu.Title.ToLower().Trim() == menuDataForUpdate.Title.ToLower().Trim());
                        Assert.AreEqual(menuDataForUpdate.Title.ToLower(), menus.Title.ToLower());
                    }
                }
            };
        }
        #endregion

        #region GetMenusRolesTest
        /// <summary>
        /// GetMenusRolesest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuRoleData.csv", "MenuRoleData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuRoleData.csv"), TestMethod]

        public void GetMenusRolesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int MenuId;
                MenuId = Convert.ToInt32((TestContext.DataRow["MenuId"].ToString()));
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Menu/GetMenusRoles"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var menuRolesData = result.Result;
                        //deserialize to your class
                        List<MenuRoleData> menuRolesList = JsonConvert.DeserializeObject<List<MenuRoleData>>(menuRolesData);
                        MenuRoleData menu = menuRolesList.Find(data => data.MenuId == MenuId);
                        Assert.AreEqual(MenuId, menu.MenuId);
                    }
                }

            };
        }
        #endregion
    }
}
