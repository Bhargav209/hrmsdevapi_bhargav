﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Net;
using AP.DomainEntities;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using AP.API;
using System.Collections.Generic;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class KRARoleControllerTest : BaseControllerTests
    {
        #region CreateKRAForRoleTest
        /// <summary>
        /// CreateKRAForRoleTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void CreateKRAForRoleTest()
        {
            try
            {

                KRARoleData roleData = new KRARoleData();
                roleData.RoleID = Convert.ToInt32(TestContext.DataRow["RoleID"]);
                roleData.KRAAspectID = Convert.ToInt32(TestContext.DataRow["KRAAspectID"]);
                roleData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                roleData.KRAAspectMetric = Convert.ToString(TestContext.DataRow["KRAAspectMetric"]);
                roleData.KRAAspectTarget = Convert.ToString(TestContext.DataRow["KRAAspectTarget"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRARole/CreateKRAForRole", new StringContent(JsonConvert.SerializeObject(roleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CloneKRARoleTemplateTest
        /// <summary>
        /// CloneKRARoleTemplateTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv"), TestMethod]
        public void CloneKRARoleTemplateTest()
        {
            try
            {
                KRARoleData kraRoleData = new KRARoleData();
                kraRoleData.RoleID = 2;
                kraRoleData.CloneRoleID = 1;
                kraRoleData.FinancialYearID = 2;
                kraRoleData.CloneFinancialYearID = 1;
                string KRAAspectName = Convert.ToString(TestContext.DataRow["KRAAspectName"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRARole/CloneKRARoleTemplate", new StringContent(JsonConvert.SerializeObject(kraRoleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            } 

            
        }
        #endregion

        #region DeleteKRARoleMetricTest
        /// <summary>
        /// DeleteKRARoleMetricTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void DeleteKRARoleMetricTest()
        {
            int ID = 12;
            KRARole kraRole = new KRARole();
            kraRole.DeleteKRARoleMetric(ID);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRARole/DeleteKRARoleMetric", new StringContent(JsonConvert.SerializeObject(kraRole), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region DeleteKRARoleAspectTest
        /// <summary>
        /// DeleteKRARoleAspectTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void DeleteKRARoleAspectTest()
        {
            int roleId = 2;
            int kraAspectID = 1;
            int financialYearID = 1;
            KRARole kraRole = new KRARole();
            kraRole.DeleteKRARoleAspect(roleId, kraAspectID, financialYearID);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRARole/DeleteKRARoleAspect", new StringContent(JsonConvert.SerializeObject(kraRole), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region UpdateKRAAspectTest
        /// <summary>
        /// UpdateKRAAspectTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void UpdateKRARoleMetricTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                KRARoleData kraRoleData = new KRARoleData();
                kraRoleData.ID = 14;//Convert.ToInt32(TestContext.DataRow["KraAspectId"]);
                kraRoleData.KRAAspectMetric = TestContext.DataRow["KRAAspectMetric"].ToString();
                kraRoleData.KRAAspectTarget = TestContext.DataRow["KRAAspectTarget"].ToString();

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRARole/UpdateKRARoleMetric", new StringContent(JsonConvert.SerializeObject(kraRoleData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region GetKRATemplateByRoleIDTest
        /// <summary>
        /// GetKRATemplateByRoleIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv"), TestMethod]

        public void GetKRATemplateByRoleIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int RoleID = Convert.ToInt32(TestContext.DataRow["RoleID"]);
                int FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                string KRAAspectName = Convert.ToString(TestContext.DataRow["KRAAspectName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRARole/GetKRATemplateByRoleID?RoleID=" + RoleID + "&&FinancialYearID=" + FinancialYearID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var roleData = result.Result;
                        //deserialize to your class
                        List<KRARoleData> Roles = JsonConvert.DeserializeObject<List<KRARoleData>>(roleData);
                        KRARoleData roledata = Roles.Find(role => role.KRAAspectName.ToLower() == KRAAspectName.ToLower());
                        Assert.AreEqual(KRAAspectName, roledata.KRAAspectName);
                    }
                }

            };
        }
        #endregion
    }
}
