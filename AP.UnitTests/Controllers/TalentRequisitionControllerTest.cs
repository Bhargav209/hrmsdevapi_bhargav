﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using System.Configuration;
using System;
using Newtonsoft.Json;
using System.Text;
using AP.DomainEntities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AP.API;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class TalentRequisitionControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region Create Talent requisition Test

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", @"|DataDirectory|\TestInput\AddTalentRequisitionDetails.XML", "TalentRequistion", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AddTalentRequisitionDetails.XML"), TestMethod]
        public void CreateRequisitionTest()
        {
            try
            {
                TalentRequistionData talentRequistionData = new TalentRequistionData();
                if (!Convert.IsDBNull(TestContext.DataRow["DepartmentId"]) || !Convert.IsDBNull(TestContext.DataRow["ProjectTypeId"]) ||
                    !Convert.IsDBNull(TestContext.DataRow["ProjectId"]) || !Convert.IsDBNull(TestContext.DataRow["RequestedDate"]) ||
                    !Convert.IsDBNull(TestContext.DataRow["RequiredDate"]))
                {
                    talentRequistionData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                    talentRequistionData.PracticeAreaId = Convert.ToInt32(TestContext.DataRow["ProjectTypeId"]);
                    talentRequistionData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                    talentRequistionData.RequestedDate = TestContext.DataRow["RequestedDate"].ToString();
                    talentRequistionData.RequiredDate = TestContext.DataRow["RequiredDate"].ToString();
                    talentRequistionData.TargetFulfillmentDate = TestContext.DataRow["TargetFulfillmentDate"].ToString();
                    talentRequistionData.Status = TestContext.DataRow["Status"].ToString();
                }
                var requisitionRoleDetailsData = TestContext.DataRow.GetChildRows("TalentRequistion_RequistiionRoleDetails");
                List<RequisitionRoleDetails> requisitionRoleDetailList = new List<RequisitionRoleDetails>();
                foreach (var requisitionRoleDetail in requisitionRoleDetailsData)
                {
                    RequisitionRoleDetails requisitionRoleDetails = new RequisitionRoleDetails()
                    {
                        RoleMasterId = Convert.ToInt32(requisitionRoleDetail["RoleId"]),
                        NoOfBillablePositions = Convert.ToInt32(requisitionRoleDetail["NoOfBillablePositions"]),
                        NoOfNonBillablePositions = Convert.ToInt32(requisitionRoleDetail["NoOfNonBillablePositions"]),
                        MinimumExperience = Convert.ToInt32(requisitionRoleDetail["MinimumExperience"]),
                        EssentialEducationQualification = Convert.ToString(requisitionRoleDetail["EssentialEducationQualification"]),
                        DesiredEducationQualification = Convert.ToString(requisitionRoleDetail["DesiredEducationQualification"]),
                        KeyResponsibilities = Convert.ToString(requisitionRoleDetail["KeyResponsibilities"]),
                        Expertise = Convert.ToString(requisitionRoleDetail["Expertise"]),
                        RoleDescription = Convert.ToString(requisitionRoleDetail["RoleDescription"]),
                        ProjectSpecificResponsibilities = Convert.ToString(requisitionRoleDetail["ProjectSpecificResponsibilities"])
                    };

                    var requisitionRoleSkillsData = requisitionRoleDetail.GetChildRows("RequistiionRoleDetails_RequisitionRoleSkills");
                    List<RequisitionRoleSkills> requisitionRoleSkillsList = new List<RequisitionRoleSkills>();
                    foreach (var requisitionRoleSkillData in requisitionRoleSkillsData)
                    {
                        RequisitionRoleSkills requisitionRoleSkills = new RequisitionRoleSkills()
                        {
                            CompetencyAreaId = Convert.ToInt32(requisitionRoleSkillData["CompetencyAreaId"]),
                            SkillId = Convert.ToInt32(requisitionRoleSkillData["SkillId"]),
                            IsPrimary = Convert.ToBoolean(requisitionRoleSkillData["IsPrimary"]),
                            ProficiencyLevelId = Convert.ToInt32(requisitionRoleSkillData["ProficiencyLevelId"]),
                            IsDefaultSkill = Convert.ToBoolean(requisitionRoleSkillData["IsDefaultSkill"])
                        };
                        requisitionRoleSkillsList.Add(requisitionRoleSkills);
                    }
                    requisitionRoleDetails.RequisitionRoleSkills = requisitionRoleSkillsList;
                    requisitionRoleDetailList.Add(requisitionRoleDetails);
                }

                talentRequistionData.RequisitionRoleDetails = requisitionRoleDetailList;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddOrUpdateRequisition/", new StringContent(JsonConvert.SerializeObject(talentRequistionData), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };


            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        #region GetAllRequisitionsTest
        /// <summary>
        /// GetAllRequisitionsTest
        /// </summary>
        [TestMethod()]
        public void GetAllRequisitionsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetAllRequisition"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region TagTAPositionsTest
        /// <summary>
        /// GetAllRequisitionsTest
        /// </summary>
        [TestMethod()]
        public void TagTAPositionsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/TagTAPositions?roleDetailId=12&positions=2"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetRequisitionByIdTest
        /// <summary>
        /// GetRequisitionByIdTest
        /// </summary>
        [TestMethod()]
        public void GetRequisitionByIdTest()
        {
            try
            {
                int talentRequistionId = 2;
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetRequisitionById?id=" + talentRequistionId))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetRequisitionViewDetailsByIdTest
        /// <summary>
        /// GetRequisitionViewDetailsByIdTest
        /// </summary>
        [TestMethod()]
        public void GetRequisitionViewDetailsByIdTest()
        {
            try
            {
                int talentRequistionId = 2;
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetRequisitionViewDetailsById?id=" + talentRequistionId))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateRequisitionTest
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", @"|DataDirectory|\TestInput\UpdateTalentRequisitionDetails.XML", "TalentRequistion", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\UpdateTalentRequisitionDetails.XML"), TestMethod]
        public void UpdateRequisitionTest()
        {
            try
            {
                TalentRequistionData talentRequistionData = new TalentRequistionData();
                if (!Convert.IsDBNull(TestContext.DataRow["TRId"]) || !Convert.IsDBNull(TestContext.DataRow["DepartmentId"]) || !Convert.IsDBNull(TestContext.DataRow["ProjectTypeId"]) ||
                    !Convert.IsDBNull(TestContext.DataRow["ProjectId"]) || !Convert.IsDBNull(TestContext.DataRow["RequestedDate"]) ||
                    !Convert.IsDBNull(TestContext.DataRow["RequiredDate"]))
                {
                    talentRequistionData.TalentRequisitionId = Convert.ToInt32(TestContext.DataRow["TRId"]);
                    talentRequistionData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                    talentRequistionData.PracticeAreaId = Convert.ToInt32(TestContext.DataRow["ProjectTypeId"]);
                    talentRequistionData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                    talentRequistionData.RequestedDate = TestContext.DataRow["RequestedDate"].ToString();
                    talentRequistionData.RequiredDate = TestContext.DataRow["RequiredDate"].ToString();
                    talentRequistionData.TargetFulfillmentDate = TestContext.DataRow["TargetFulfillmentDate"].ToString();
                    talentRequistionData.Status = TestContext.DataRow["Status"].ToString();
                    string sNumbers = TestContext.DataRow["DeltedRRdetailsIds"].ToString();
                    if (!string.IsNullOrEmpty(sNumbers))
                    {
                        List<int> numbers = new List<int>(Array.ConvertAll(sNumbers.Split(','), int.Parse));
                        talentRequistionData.DeltedRRdetailsIds = numbers;
                    }



                }
                var requisitionRoleDetailsData = TestContext.DataRow.GetChildRows("TalentRequistion_RequistiionRoleDetails");
                List<RequisitionRoleDetails> requisitionRoleDetailList = new List<RequisitionRoleDetails>();
                foreach (var requisitionRoleDetail in requisitionRoleDetailsData)
                {
                    RequisitionRoleDetails requisitionRoleDetails = new RequisitionRoleDetails()
                    {
                        RequisitionRoleDetailID = Convert.ToInt32(requisitionRoleDetail["RequisitionRoleDetailID"]),
                        RoleMasterId = Convert.ToInt32(requisitionRoleDetail["RoleId"]),
                        NoOfBillablePositions = Convert.ToInt32(requisitionRoleDetail["NoOfBillablePositions"]),
                        NoOfNonBillablePositions = Convert.ToInt32(requisitionRoleDetail["NoOfNonBillablePositions"]),
                        MinimumExperience = Convert.ToInt32(requisitionRoleDetail["YearsOfExperience"]),
                        EssentialEducationQualification = Convert.ToString(requisitionRoleDetail["EssentialEducationQualification"]),
                        DesiredEducationQualification = Convert.ToString(requisitionRoleDetail["DesiredEducationQualification"]),
                        KeyResponsibilities = Convert.ToString(requisitionRoleDetail["KeyResponsibilities"]),
                        Expertise = Convert.ToString(requisitionRoleDetail["Expertise"]),
                        RoleDescription = Convert.ToString(requisitionRoleDetail["RoleDescription"]),
                        ProjectSpecificResponsibilities = Convert.ToString(requisitionRoleDetail["ProjectSpecificResponsibilities"])
                    };

                    string sNumbers = requisitionRoleDetail["DeltedRRSkillIds"].ToString();
                    if (!string.IsNullOrEmpty(sNumbers))
                    {
                        List<int> numbers = new List<int>(Array.ConvertAll(sNumbers.Split(','), int.Parse));
                        requisitionRoleDetails.DeltedRRSkillIds = numbers;
                    }


                    var requisitionRoleSkillsData = requisitionRoleDetail.GetChildRows("RequistiionRoleDetails_RequisitionRoleSkills");
                    List<RequisitionRoleSkills> requisitionRoleSkillsList = new List<RequisitionRoleSkills>();
                    foreach (var requisitionRoleSkillData in requisitionRoleSkillsData)
                    {
                        RequisitionRoleSkills requisitionRoleSkills = new RequisitionRoleSkills()
                        {
                            RRSkillId = Convert.ToInt32(requisitionRoleSkillData["RRSkillId"]),
                            CompetencyAreaId = Convert.ToInt32(requisitionRoleSkillData["CompetencyAreaId"]),
                            SkillId = Convert.ToInt32(requisitionRoleSkillData["SkillId"]),
                            IsPrimary = Convert.ToBoolean(requisitionRoleSkillData["IsPrimary"]),
                            ProficiencyLevelId = Convert.ToInt32(requisitionRoleSkillData["ProficiencyLevelId"]),
                            IsDefaultSkill = Convert.ToBoolean(requisitionRoleSkillData["IsDefaultSkill"])
                        };
                        requisitionRoleSkillsList.Add(requisitionRoleSkills);
                    }
                    requisitionRoleDetails.RequisitionRoleSkills = requisitionRoleSkillsList;
                    requisitionRoleDetailList.Add(requisitionRoleDetails);
                }

                talentRequistionData.RequisitionRoleDetails = requisitionRoleDetailList;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddOrUpdateRequisition/", new StringContent(JsonConvert.SerializeObject(talentRequistionData), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };


            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region GetRequisitionDetailsBySearchTest
        /// <summary>
        /// GetRequisitionDetailsBySearchTest
        /// </summary>
        [TestMethod()]
        public void GetRequisitionDetailsBySearchTest()
        {
            try
            {
                SearchFilter searchFilter = new SearchFilter();
                searchFilter.SearchType = "TRNumber";
                searchFilter.SearchData = "2";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/GetRequisitionDetailsBySearch/", new StringContent(JsonConvert.SerializeObject(searchFilter), Encoding.UTF8, "application/json")).Result)
                    {

                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region ApproveRequisitionTest
        /// <summary>
        /// ApproveRequisitionTest
        /// </summary>
        [TestMethod()]
        public void ApproveRequisitionTest()
        {
            try
            {
                ApproveRequisitionData approveRequisitionData = new ApproveRequisitionData();
                var idList = new List<int>() { 23 };
                approveRequisitionData.TalentRequisitionIds = idList;
                approveRequisitionData.Type = "Approved";
                approveRequisitionData.CurrentUser = "rajesh.arthimalla@senecaglobal.com";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/ApproveRequisition/", new StringContent(JsonConvert.SerializeObject(approveRequisitionData), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region RejectRequisitionTest
        /// <summary>
        /// RejectRequisitionTest
        /// </summary>
        [TestMethod()]
        public void RejectRequisitionTest()
        {
            try
            {
                ApproveRequisitionData approveRequisitionData = new ApproveRequisitionData();
                var idList = new List<int>() { 23 };
                approveRequisitionData.TalentRequisitionIds = idList;
                approveRequisitionData.Type = "Rejected";
                approveRequisitionData.Remarks = "rejected for testing";
                approveRequisitionData.CurrentUser = "rajesh.arthimalla@senecaglobal.com";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/ApproveRequisition/", new StringContent(JsonConvert.SerializeObject(approveRequisitionData), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetBehaviorAreaTest
        /// <summary>
        /// GetBehaviorAreaTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorAreaData.csv", "BehaviorAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorAreaData.csv"), TestMethod]

        public void GetBehaviorAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string behaviorDescription = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["BehaviorAreaDescription"].ToString()))
                {
                    behaviorDescription = TestContext.DataRow["BehaviorAreaDescription"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorAreaData = result.Result;
                        //deserialize to your class
                        List<BehaviorAreas> behaviorData = JsonConvert.DeserializeObject<List<BehaviorAreas>>(behaviorAreaData);
                        BehaviorAreas behaviorArea = behaviorData.Find(bArea => bArea.BehaviorArea.ToLower().Trim() == behaviorDescription.ToLower().Trim());
                        Assert.AreEqual(behaviorDescription, behaviorArea.BehaviorArea);
                    }
                }

            };
        }
        #endregion

        #region GetBehaviorRatingTest
        /// <summary>
        /// GetBehaviorRatingTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorRatingData.csv", "BehaviorRatingData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorRatingData.csv"), TestMethod]

        public void GetBehaviorRatingTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string behaviorRating = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["Rating"].ToString()))
                {
                    behaviorRating = TestContext.DataRow["Rating"].ToString();
                }

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorRating"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorRatingData = result.Result;
                        //deserialize to your class
                        List<BehaviorRatings> ratingData = JsonConvert.DeserializeObject<List<BehaviorRatings>>(behaviorRatingData);
                        BehaviorRatings behaviorArea = ratingData.Find(bRating => bRating.Rating.ToLower().Trim() == behaviorRating.ToLower().Trim());
                        Assert.AreEqual(behaviorRating, behaviorArea.Rating);
                    }
                }

            };
        }
        #endregion

        #region GetLearningAptitudeTest
        /// <summary>
        /// GetLearningAptitudeTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\LearningAptitudeData.csv", "LearningAptitudeData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\LearningAptitudeData.csv"), TestMethod]

        public void GetLearningAptitudeTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string learningAptitude = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["LearningAptitude"].ToString()))
                {
                    learningAptitude = TestContext.DataRow["LearningAptitude"].ToString();
                }

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetLearningAptitude"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var learningAptitudeData = result.Result;
                        //deserialize to your class
                        List<LearningAptitudeData> lstAptitudeData = JsonConvert.DeserializeObject<List<LearningAptitudeData>>(learningAptitudeData);
                        LearningAptitudeData aptitudeData = lstAptitudeData.Find(aptitude => aptitude.LearningAptitude.ToLower().Trim() == learningAptitude.ToLower().Trim());
                        Assert.AreEqual(learningAptitude, aptitudeData.LearningAptitude);
                    }
                }

            };
        }
        #endregion

        #region GetExpertiseAreaTest
        /// <summary>
        /// GetExpertiseAreaTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ExpertiseAreaData.csv", "ExpertiseAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ExpertiseAreaData.csv"), TestMethod]

        public void GetExpertiseAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string sExpertArea = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["ExpertiseArea"].ToString()))
                {
                    sExpertArea = TestContext.DataRow["ExpertiseArea"].ToString();
                }

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExpertiseArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var expertiseAreaData = result.Result;
                        //deserialize to your class
                        List<ExpertiseAreaData> lstExpertiseData = JsonConvert.DeserializeObject<List<ExpertiseAreaData>>(expertiseAreaData);
                        ExpertiseAreaData expertiseArea = lstExpertiseData.Find(expertArea => expertArea.ExpertiseAreaDescription.ToLower().Trim() == sExpertArea.ToLower().Trim());
                        Assert.AreEqual(sExpertArea, expertiseArea.ExpertiseAreaDescription);
                    }
                }

            };
        }
        #endregion

        #region AddBehaviorAreaTest
        /// <summary>
        /// AddBehaviorAreaTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorAreaData.csv", "BehaviorAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorAreaData.csv", "TestInput"), TestMethod]
        public void AddBehaviorAreaTest()
        {
            try
            {

                BehaviorAreas behaviorData = new BehaviorAreas();
                string behaviorDescription = TestContext.DataRow["BehaviorAreaDescription"].ToString() + rnd.Next(1, 100000).ToString();
                if (!string.IsNullOrEmpty(behaviorDescription))
                {
                    behaviorData.BehaviorArea = behaviorDescription.ToString();
                    behaviorData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddBehaviorArea", new StringContent(JsonConvert.SerializeObject(behaviorData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorArea"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var behaviorAreaData = result.Result;
                            //deserialize to your class
                            List<BehaviorAreas> lstBehaviorData = JsonConvert.DeserializeObject<List<BehaviorAreas>>(behaviorAreaData);
                            BehaviorAreas behaviorArea = lstBehaviorData.Find(bArea => bArea.BehaviorArea.ToLower().Trim() == behaviorData.BehaviorArea.ToLower().Trim());
                            Assert.AreEqual(behaviorData.BehaviorArea, behaviorArea.BehaviorArea);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateBehaviorAreaTest
        /// <summary>
        /// UpdateBehaviorAreaTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorAreaData.csv", "BehaviorAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorAreaData.csv", "TestInput"), TestMethod]
        public void UpdateBehaviorAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                BehaviorAreas behaviorAreaForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorAreaData = result.Result;
                        List<BehaviorAreas> lstBehaviorData = JsonConvert.DeserializeObject<List<BehaviorAreas>>(behaviorAreaData);
                        //Update one of the object state                
                        if (lstBehaviorData.Count > 0)
                        {
                            behaviorAreaForUpdate = lstBehaviorData[0];
                        }
                        behaviorAreaForUpdate.BehaviorArea += " Update Test" + rnd.Next(1, 1000).ToString();
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/UpdateBehaviorArea", new StringContent(JsonConvert.SerializeObject(behaviorAreaForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorAreaData = result.Result;
                        List<BehaviorAreas> behaviorData = JsonConvert.DeserializeObject<List<BehaviorAreas>>(behaviorAreaData);
                        BehaviorAreas behaviorArea = behaviorData.Find(bArea => bArea.BehaviorArea.ToLower().Trim() == behaviorAreaForUpdate.BehaviorArea.ToLower().Trim());
                        Assert.AreEqual(behaviorAreaForUpdate.BehaviorArea.ToLower(), behaviorArea.BehaviorArea.ToLower());
                    }
                }
            };
        }
        #endregion

        #region DeleteBehaviorAreaTest
        /// <summary>
        /// DeleteBehaviorAreaTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorAreaData.csv", "BehaviorAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorAreaData.csv", "TestInput"), TestMethod]
        public void DeleteBehaviorAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                BehaviorAreas behaviorAreaForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorAreaData = result.Result;
                        List<BehaviorAreas> lstBehaviorData = JsonConvert.DeserializeObject<List<BehaviorAreas>>(behaviorAreaData);
                        //Update one of the object state                
                        if (lstBehaviorData.Count > 0)
                        {
                            behaviorAreaForUpdate = lstBehaviorData[0];
                            behaviorAreaForUpdate.IsActive = false;
                        }
                    }

                }
                //2. Delete 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/DeleteBehaviorArea?behaviorAreaId=" + behaviorAreaForUpdate.BehaviorAreaId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorAreaData = result.Result;
                        List<BehaviorAreas> behaviorData = JsonConvert.DeserializeObject<List<BehaviorAreas>>(behaviorAreaData);
                        BehaviorAreas behaviorArea = behaviorData.Find(bArea => bArea.BehaviorArea.ToLower().Trim() == behaviorAreaForUpdate.BehaviorArea.ToLower().Trim());
                        Assert.IsNull(behaviorArea);
                    }
                }
            };
        }
        #endregion

        #region AddBehaviorRatingTest
        /// <summary>
        /// SaveBehaviorRatingTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorRatingData.csv", "BehaviorRatingData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorRatingData.csv", "TestInput"), TestMethod]
        public void AddBehaviorRating()
        {
            try
            {
                BehaviorRatings behaviorRatingData = new BehaviorRatings();
                string behaviorRating = TestContext.DataRow["BehaviorRatingDescription"].ToString() + rnd.Next(1, 100000).ToString();
                if (!string.IsNullOrEmpty(behaviorRating))
                {
                    behaviorRatingData.Rating = behaviorRating.ToString();
                    behaviorRatingData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddBehaviorRating", new StringContent(JsonConvert.SerializeObject(behaviorRatingData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorRating"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var RatingData = result.Result;
                            //deserialize to your class
                            List<BehaviorRatings> lstBehaviorRating = JsonConvert.DeserializeObject<List<BehaviorRatings>>(RatingData);
                            BehaviorRatings behaviorData = lstBehaviorRating.Find(bRating => bRating.Rating.ToLower().Trim() == behaviorRatingData.Rating.ToLower().Trim());
                            Assert.AreEqual(behaviorRatingData.Rating, behaviorData.Rating);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateBehaviorRatingTest
        /// <summary>
        /// UpdateBehaviorRatingTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorRatingData.csv", "BehaviorRatingData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorRatingData.csv", "TestInput"), TestMethod]
        public void UpdateBehaviorRatingTest()
        {
            using (HttpClient client = new HttpClient())
            {
                BehaviorRatings behaviorRatingForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorRating"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorRatingData = result.Result;
                        List<BehaviorRatings> lstBehaviorData = JsonConvert.DeserializeObject<List<BehaviorRatings>>(behaviorRatingData);
                        //Update one of the object state                
                        if (lstBehaviorData.Count > 0)
                        {
                            behaviorRatingForUpdate = lstBehaviorData[0];
                        }
                        behaviorRatingForUpdate.Rating += " Update Test" + rnd.Next(1, 1000).ToString();
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/UpdateBehaviorRating", new StringContent(JsonConvert.SerializeObject(behaviorRatingForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorRating"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var RatingData = result.Result;
                        List<BehaviorRatings> behaviorData = JsonConvert.DeserializeObject<List<BehaviorRatings>>(RatingData);
                        BehaviorRatings behaviorRating = behaviorData.Find(brating => brating.Rating.ToLower().Trim() == behaviorRatingForUpdate.Rating.ToLower().Trim());
                        Assert.AreEqual(behaviorRatingForUpdate.Rating.ToLower(), behaviorRating.Rating.ToLower());
                    }
                }
            };
        }
        #endregion

        #region DeleteBehaviorRatingTest
        /// <summary>
        /// DeleteBehaviorRatingTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\BehaviorRatingData.csv", "BehaviorRatingData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\BehaviorRatingData.csv", "TestInput"), TestMethod]
        public void DeleteBehaviorRatingTest()
        {
            using (HttpClient client = new HttpClient())
            {
                BehaviorRatings behaviorratingForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorRating"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorratingData = result.Result;
                        List<BehaviorRatings> lstBehaviorData = JsonConvert.DeserializeObject<List<BehaviorRatings>>(behaviorratingData);
                        //Update one of the object state                
                        if (lstBehaviorData.Count > 0)
                        {
                            behaviorratingForUpdate = lstBehaviorData[0];
                            behaviorratingForUpdate.IsActive = false;
                        }
                    }

                }
                //2. Delete 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/DeleteBehaviorRating?ratingId=" + behaviorratingForUpdate.BehaviorRatingId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetBehaviorRating"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var behaviorRatingData = result.Result;
                        List<BehaviorRatings> behaviorData = JsonConvert.DeserializeObject<List<BehaviorRatings>>(behaviorRatingData);
                        BehaviorRatings behaviorRating = behaviorData.Find(brating => brating.Rating.ToLower().Trim() == behaviorratingForUpdate.Rating.ToLower().Trim());
                        Assert.IsNull(behaviorRating);
                    }
                }
            };

        }
        #endregion

        #region AddLearningAptitudeTest
        /// <summary>
        /// AddLearningAptitudeTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\LearningAptitudeData.csv", "LearningAptitudeData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\LearningAptitudeData.csv", "TestInput"), TestMethod]
        public void AddLearningAptitudeTest()
        {
            try
            {

                LearningAptitudeData learningAptitudeData = new LearningAptitudeData();
                string LearningAptitude = TestContext.DataRow["LearningAptitudeDesc"].ToString() + rnd.Next(1, 100000).ToString();
                if (!string.IsNullOrEmpty(LearningAptitude))
                {
                    learningAptitudeData.LearningAptitude = LearningAptitude.ToString();
                    learningAptitudeData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddLearningAptitude", new StringContent(JsonConvert.SerializeObject(learningAptitudeData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetLearningAptitude"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var aptitudeData = result.Result;
                            //deserialize to your class
                            List<LearningAptitudeData> lstAptitudeData = JsonConvert.DeserializeObject<List<LearningAptitudeData>>(aptitudeData);
                            LearningAptitudeData LearningData = lstAptitudeData.Find(aptitude => aptitude.LearningAptitude.ToLower().Trim() == learningAptitudeData.LearningAptitude.ToLower().Trim());
                            Assert.AreEqual(learningAptitudeData.LearningAptitude, LearningData.LearningAptitude);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateLearningAptitudeTest
        /// <summary>
        /// UpdateLearningAptitudeTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\LearningAptitudeData.csv", "LearningAptitudeData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\LearningAptitudeData.csv", "TestInput"), TestMethod]
        public void UpdateLearningAptitudeTest()
        {
            using (HttpClient client = new HttpClient())
            {
                LearningAptitudeData learningAptitudeForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetLearningAptitude"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var aptitudeData = result.Result;
                        List<LearningAptitudeData> lstLearningData = JsonConvert.DeserializeObject<List<LearningAptitudeData>>(aptitudeData);
                        //Update one of the object state                
                        if (lstLearningData.Count > 0)
                        {
                            learningAptitudeForUpdate = lstLearningData[0];
                        }
                        learningAptitudeForUpdate.LearningAptitude += " Update Test" + rnd.Next(1, 1000).ToString();
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/UpdateLearningAptitude", new StringContent(JsonConvert.SerializeObject(learningAptitudeForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetLearningAptitude"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var learningData = result.Result;
                        List<LearningAptitudeData> learningAptitudeData = JsonConvert.DeserializeObject<List<LearningAptitudeData>>(learningData);
                        LearningAptitudeData learningAptitude = learningAptitudeData.Find(aptitude => aptitude.LearningAptitude.ToLower().Trim() == learningAptitudeForUpdate.LearningAptitude.ToLower().Trim());
                        Assert.AreEqual(learningAptitudeForUpdate.LearningAptitude.ToLower(), learningAptitude.LearningAptitude.ToLower());
                    }
                }
            };
        }
        #endregion

        #region DeleteLearningAptitudeTest
        /// <summary>
        /// DeleteLearningAptitudeTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\LearningAptitudeData.csv", "LearningAptitudeData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\LearningAptitudeData.csv", "TestInput"), TestMethod]
        public void DeleteLearningAptitudeTest()
        {
            using (HttpClient client = new HttpClient())
            {
                LearningAptitudeData learningAptitudeForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetLearningAptitude"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var learningAptitudeData = result.Result;
                        List<LearningAptitudeData> lstaptitudeData = JsonConvert.DeserializeObject<List<LearningAptitudeData>>(learningAptitudeData);
                        //Update one of the object state                
                        if (lstaptitudeData.Count > 0)
                        {
                            learningAptitudeForUpdate = lstaptitudeData[0];
                            learningAptitudeForUpdate.IsActive = false;
                        }
                    }

                }
                //2. Delete 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/DeleteLearningAptitude?learningAptitudeId=" + learningAptitudeForUpdate.LearningAptitudeId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetLearningAptitude"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var aptitudeData = result.Result;
                        List<LearningAptitudeData> learningData = JsonConvert.DeserializeObject<List<LearningAptitudeData>>(aptitudeData);
                        LearningAptitudeData learningAptitude = learningData.Find(aptitude => aptitude.LearningAptitude.ToLower().Trim() == learningAptitudeForUpdate.LearningAptitude.ToLower().Trim());
                        Assert.IsNull(learningAptitude);
                    }
                }
            };
        }
        #endregion

        #region AddExpertiseAreaTest
        /// <summary>
        /// AddExpertiseAreaTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ExpertiseAreaData.csv", "ExpertiseAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ExpertiseAreaData.csv", "TestInput"), TestMethod]
        public void AddExpertiseAreaTest()
        {
            try
            {

                ExpertiseAreaData expertiseAreaData = new ExpertiseAreaData();
                string ExpertiseArea = TestContext.DataRow["ExpertiseAreaDescription"].ToString() + rnd.Next(1, 100000).ToString();
                if (!string.IsNullOrEmpty(ExpertiseArea))
                {
                    expertiseAreaData.ExpertiseAreaDescription = ExpertiseArea.ToString();
                    expertiseAreaData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddExpertiseArea", new StringContent(JsonConvert.SerializeObject(expertiseAreaData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExpertiseArea"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var ExpertiseData = result.Result;
                            //deserialize to your class
                            List<ExpertiseAreaData> lstExpertiseData = JsonConvert.DeserializeObject<List<ExpertiseAreaData>>(ExpertiseData);
                            ExpertiseAreaData areaData = lstExpertiseData.Find(eArea => eArea.ExpertiseAreaDescription.ToLower().Trim() == expertiseAreaData.ExpertiseAreaDescription.ToLower().Trim());
                            Assert.AreEqual(expertiseAreaData.ExpertiseAreaDescription, areaData.ExpertiseAreaDescription);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateExpertiseAreaTest
        /// <summary>
        /// UpdateExpertiseAreaTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ExpertiseAreaData.csv", "ExpertiseAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ExpertiseAreaData.csv", "TestInput"), TestMethod]
        public void UpdateExpertiseAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                ExpertiseAreaData expertiseAreaForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExpertiseArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var expertiseData = result.Result;
                        List<ExpertiseAreaData> lstexpertiseData = JsonConvert.DeserializeObject<List<ExpertiseAreaData>>(expertiseData);
                        //Update one of the object state                
                        if (lstexpertiseData.Count > 0)
                        {
                            expertiseAreaForUpdate = lstexpertiseData[0];
                        }
                        expertiseAreaForUpdate.ExpertiseAreaDescription += " Update Test" + rnd.Next(1, 1000).ToString();
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/UpdateExpertiseArea", new StringContent(JsonConvert.SerializeObject(expertiseAreaForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExpertiseArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var expertiseAreaData = result.Result;
                        List<ExpertiseAreaData> expertiseData = JsonConvert.DeserializeObject<List<ExpertiseAreaData>>(expertiseAreaData);
                        ExpertiseAreaData areaData = expertiseData.Find(eArea => eArea.ExpertiseAreaDescription.ToLower().Trim() == expertiseAreaForUpdate.ExpertiseAreaDescription.ToLower().Trim());
                        Assert.AreEqual(expertiseAreaForUpdate.ExpertiseAreaDescription.ToLower(), areaData.ExpertiseAreaDescription.ToLower());
                    }
                }
            };
        }
        #endregion

        #region DeleteExpertiseAreaTest
        /// <summary>
        /// DeleteExpertiseAreaTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ExpertiseAreaData.csv", "ExpertiseAreaData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ExpertiseAreaData.csv", "TestInput"), TestMethod]
        public void DeleteExpertiseAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                ExpertiseAreaData expertiseAreaForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExpertiseArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var expertiseData = result.Result;
                        List<ExpertiseAreaData> lstexpertiseData = JsonConvert.DeserializeObject<List<ExpertiseAreaData>>(expertiseData);
                        //Update one of the object state                
                        if (lstexpertiseData.Count > 0)
                        {
                            expertiseAreaForUpdate = lstexpertiseData[1];
                            expertiseAreaForUpdate.IsActive = false;
                        }
                    }

                }
                //2. Delete 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/DeleteExpertiseArea?expertiseId=" + expertiseAreaForUpdate.ExpertiseId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExpertiseArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var areaData = result.Result;
                        List<ExpertiseAreaData> expertiseAreaData = JsonConvert.DeserializeObject<List<ExpertiseAreaData>>(areaData);
                        ExpertiseAreaData expertiseArea = expertiseAreaData.Find(expertise => expertise.ExpertiseAreaDescription.ToLower().Trim() == expertiseAreaForUpdate.ExpertiseAreaDescription.ToLower().Trim());
                        Assert.IsNull(expertiseArea);
                    }
                }
            };
        }
        #endregion

        #region AddRequisitionRoleSkillsTest
        /// <summary>
        /// AddRequisitionRoleSkillsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionData.csv", "TalentRequisitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionData.csv", "TestInput"), TestMethod]
        public void AddRequisitionRoleSkillsTest()
        {
            try
            {
                List<RequisitionRoleSkills> requisitionRoleSkillsList = new List<RequisitionRoleSkills>();
                requisitionRoleSkillsList.Add(new RequisitionRoleSkills { TalentRequisitionId = 1, RequistionRoleDetailID = 3, CompetencyAreaId = 2, SkillGroupId = 1, SkillId = 1, ProficiencyLevelId = 1 });
                requisitionRoleSkillsList.Add(new RequisitionRoleSkills { TalentRequisitionId = 1, RequistionRoleDetailID = 3, CompetencyAreaId = 2, SkillGroupId = 1, SkillId = 2, ProficiencyLevelId = 2 });
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/AddRequisitionRoleSkills", new StringContent(JsonConvert.SerializeObject(requisitionRoleSkillsList).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRequisitionRoleSkillsByIDTest
        /// <summary>
        /// GetRequisitionRoleSkillsByIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionData.csv", "TalentRequisitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionData.csv"), TestMethod]

        public void GetRequisitionRoleSkillsByIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int skillID = Convert.ToInt32(TestContext.DataRow["skillID"]);
                int TRID = Convert.ToInt32(TestContext.DataRow["TRID"]);
                int RequistionRoleDetailID = Convert.ToInt32(TestContext.DataRow["RequistionRoleDetailID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetRequisitionRoleSkillsByID?TRID=" + TRID + "&&RequistionRoleDetailID=" + RequistionRoleDetailID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var requisitionRoleData = result.Result;
                        //deserialize to your class
                        List<RequisitionRoleSkills> lstrequisitionRoleData = JsonConvert.DeserializeObject<List<RequisitionRoleSkills>>(requisitionRoleData);
                        RequisitionRoleSkills skillData = lstrequisitionRoleData.Find(skill => skill.SkillId == skillID);
                        Assert.AreEqual(skillID, skillData.SkillId);
                    }
                }

            };
        }
        #endregion

        #region GetRolesByTrIdTest
        /// <summary>
        /// GetRolesByTrIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionData.csv", "TalentRequisitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionData.csv"), TestMethod]

        public void GetRolesByTrIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string roleName = Convert.ToString(TestContext.DataRow["roleName"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TRID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetRolesByTrId?TrId=" + TrId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var RoleData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstRoleData = JsonConvert.DeserializeObject<List<GenericType>>(RoleData);
                        GenericType roleDetails = lstRoleData.Find(role => role.Name.ToLower() == roleName.ToLower());
                        Assert.AreEqual(roleName, roleDetails.Name);
                    }
                }

            };
        }
        #endregion

        #region GetSkillDetailsByTalentRequisitionRoleIdTest
        /// <summary>
        /// GetSkillDetailsByTalentRequisitionRoleIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetSkillDetailsByTalentRequisitionRoleIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string skillName = Convert.ToString(TestContext.DataRow["skillName"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TrId"]);
                int RoleId = Convert.ToInt32(TestContext.DataRow["RoleId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetSkillDetailsByTalentRequisitionRoleId?TalentRequisitionId=" + TrId + "&&RoleId=" + RoleId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var skillsData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstSkillsData = JsonConvert.DeserializeObject<List<GenericType>>(skillsData);
                        GenericType skillData = lstSkillsData.Find(skill => skill.Name.ToLower() == skillName.ToLower());
                        Assert.AreEqual(skillName, skillData.Name);
                    }
                }

            };
        }
        #endregion

        #region GetExperienceByTalentRequisitionIdTest
        /// <summary>
        /// GetExperienceByTalentRequisitionIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionData.csv", "TalentRequisitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionData.csv"), TestMethod]

        public void GetExperienceByTalentRequisitionIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string minimumExperience = Convert.ToString(TestContext.DataRow["minimumExperience"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TrId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetExperienceByTalentRequisitionId?TrId=" + TrId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var ExperienceData = result.Result;
                        //deserialize to your class
                        RequisitionRoleDetails ExperienceDetails = JsonConvert.DeserializeObject<RequisitionRoleDetails>(ExperienceData);
                        Assert.AreEqual(minimumExperience, ExperienceDetails.Experience);
                    }
                }

            };
        }
        #endregion

        //#region GetRequisitionDetailsBySearchTest
        ///// <summary>
        ///// GetRequisitionDetailsBySearchTest
        ///// </summary> 
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        //public async Task<List<SkillSearchDetails>> GetRequisitionDetailsBySearch()
        //{
        //    //using (HttpClient client = new HttpClient())
        //    //{
        //        RequisitionSearchFilter requisition = new RequisitionSearchFilter();

        //    requisition.RoleId = 33;
        //    //Convert.ToInt32(TestContext.DataRow["RoleId"]);
        //    requisition.MinimumExperience = 0;
        //    //Convert.ToInt32(TestContext.DataRow["minimumExperience"]);
        //    requisition.MaximumExperience = 5;
        //        //Convert.ToInt32(TestContext.DataRow["MaximumExperience"]);
        //        List<GenericType> skillType = new List<GenericType>();
        //        skillType.Add(new GenericType { Id = 27 });
        //        //skillType.Add(new GenericType { Id = 6 });
        //        requisition.Skills = skillType;
        //        List<GenericType> proficiencyType = new List<GenericType>();
        //        proficiencyType.Add(new GenericType { Id = 1 });
        //        //proficiencyType.Add(new GenericType { Id = 2 });
        //        requisition.Proficiencies = proficiencyType;

        //        TalentRequisitionDetails talentRequisition = new TalentRequisitionDetails();
        //    return  await  talentRequisition.GetRequisitionDetailsBySearch(requisition);
        //    //};
        //}
        //#endregion

        [TestMethod]
        public void GetRequisitionDetailsBySearch()
        {
            //using (HttpClient client = new HttpClient())
            //{
            RequisitionSearchFilter requisition = new RequisitionSearchFilter();
            
            requisition.RoleId = 33;
            //Convert.ToInt32(TestContext.DataRow["RoleId"]);
            requisition.MinimumExperience = 0;
            //Convert.ToInt32(TestContext.DataRow["minimumExperience"]);
            requisition.MaximumExperience = 5;
            requisition.TalentRequisitionId = 1135;
            //Convert.ToInt32(TestContext.DataRow["MaximumExperience"]);
            List<GenericType> skillType = new List<GenericType>();
            skillType.Add(new GenericType { Id = 27 });
            skillType.Add(new GenericType { Id = 17 });
            requisition.Skills = skillType;
            List<GenericType> proficiencyType = new List<GenericType>();
            proficiencyType.Add(new GenericType { Id = 1 });
            proficiencyType.Add(new GenericType { Id = 2 });
            requisition.Proficiencies = proficiencyType;

            TalentRequisitionDetails talentRequisition = new TalentRequisitionDetails();
            talentRequisition.GetRequisitionDetailsBySearch(requisition);
            //};
        }

        #region SubmitRequisitionForApprovalTest
        /// <summary>
        /// SubmitRequisitionForApprovalTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv", "TestInput"), TestMethod]
        public async Task SubmitRequisitionForApprovalTest()
        {
            try
            {
                List<EmployeeTagDetails> employeeTagDetails = new List<EmployeeTagDetails>();
                employeeTagDetails.Add(new EmployeeTagDetails { EmployeeId = 68, RoleMasterID = 32 });
                employeeTagDetails.Add(new EmployeeTagDetails { EmployeeId = 13, RoleMasterID = 32 });

                List<int> genericType = new List<int>();
                genericType.Add(187);
                genericType.Add(101);

                SubmitRequisitionForApproval SubmitRequisitionForApproval = new SubmitRequisitionForApproval
                {
                    employeeTagDetails = employeeTagDetails,
                    TalentRequisitionID = 985,
                    ApprovedByID = genericType,
                    SubmittedByID = 58,
                };

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/SubmitRequisitionForApproval", new StringContent(JsonConvert.SerializeObject(SubmitRequisitionForApproval).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CreateRequisitionTest
        /// <summary>
        /// CreateRequisitionTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv", "TestInput"), TestMethod]
        public void CreateRequisitionsTest()
        {
            try
            {
                TalentRequistionData talentRequisitionData = new TalentRequistionData
                {
                    DepartmentId = 1,
                    PracticeAreaId = 1,
                    ProjectId = 18
                };
                TalentRequisitionDetails talentRequisition = new TalentRequisitionDetails();
                talentRequisition.CreateRequisition(talentRequisitionData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetTalentRequisitionApproversTest
        /// <summary>
        /// GetTalentRequisitionApproversTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetTalentRequisitionApproversTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetTalentRequisitionApprovers"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var approversData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstApproversData = JsonConvert.DeserializeObject<List<GenericType>>(approversData);
                        GenericType approversDetails = lstApproversData.Find(empId => empId.Id == EmployeeId);
                        Assert.AreEqual(EmployeeId, approversDetails.Id);
                    }
                }

            };
        }
        #endregion

        #region DeleteRequisitionRoleDetailsTest
        /// <summary>
        /// DeleteRequisitionRoleDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv", "TestInput"), TestMethod]
        public void DeleteRequisitionRoleDetailsTest()
        {
            int RequisitionRoleDetailId = Convert.ToInt32(TestContext.DataRow["RequisitionRoleDetailId"]);
            int TalentRequisitionId = Convert.ToInt32(TestContext.DataRow["RId"]);
            int RoleId = 33;//Convert.ToInt32(TestContext.DataRow["Role"]);
            TalentRequisitionDetails talentRequisition = new TalentRequisitionDetails();
            talentRequisition.DeleteRequisitionRoleDetails(RequisitionRoleDetailId, TalentRequisitionId, RoleId);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/DeleteRequisitionRoleDetails", new StringContent(JsonConvert.SerializeObject(RequisitionRoleDetailId), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region GetTaggedEmployeesByTalentRequisitionIdTest
        /// <summary>
        /// GetTaggedEmployeesByTalentRequisitionIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetTaggedEmployeesByTalentRequisitionIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string roleName = Convert.ToString(TestContext.DataRow["RoleName"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TRID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TalentRequisition/GetTaggedEmployeesByTalentRequisitionId?talentRequisitionId=" + TrId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var RoleData = result.Result;
                        //deserialize to your class
                        List<EmployeeTagDetails> lstRoleData = JsonConvert.DeserializeObject<List<EmployeeTagDetails>>(RoleData);
                        EmployeeTagDetails roleDetails = lstRoleData.Find(role => role.RoleName.ToLower() == roleName.ToLower());
                        Assert.AreEqual(roleName, roleDetails.RoleName);
                    }
                }

            };
        }
        #endregion

        #region DeleteTaggedEmployeesTest
        /// <summary>
        /// DeleteTaggedEmployeesTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv", "TestInput"), TestMethod]
        public void DeleteTaggedEmployeesTest()
        {
            List<int> type = new List<int>();
            type.Add(159);
            type.Add(160);

            TalentRequisitionDetails talentRequisition = new TalentRequisitionDetails();
            talentRequisition.DeleteTaggedEmployees(type);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/DeleteTaggedEmployees", new StringContent(JsonConvert.SerializeObject(type), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region RejectTalentRequisitionTest
        /// <summary>
        /// RejectTalentRequisitionTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]
        public void RejectTalentRequisitionTest()
        {
            try
            {

                DeliveryHeadTalentRequisitionDetails talentRequisitionDetails = new DeliveryHeadTalentRequisitionDetails();
                talentRequisitionDetails.TalentRequisitionId = Convert.ToInt32(TestContext.DataRow["TRID"]);
                talentRequisitionDetails.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                talentRequisitionDetails.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                talentRequisitionDetails.Comments = Convert.ToString(TestContext.DataRow["Comments"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TalentRequisition/RejectTalentRequisitionByFinance", new StringContent(JsonConvert.SerializeObject(talentRequisitionDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
