﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using AP.DomainEntities;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class PracticeAreaControllerTests : BaseControllerTests
    {

        #region CreatePracticeAreaTest
        /// <summary>
        /// Test method for adding practice area
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PracticeAreaInfo.csv", "PracticeAreaInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PracticeAreaInfo.csv", "TestInput"), TestMethod]
        public void CreatePracticeAreaTest()
        {
            PracticeAreaDetails practiceAreaDetails = new PracticeAreaDetails();
            if (!Convert.IsDBNull(TestContext.DataRow["PracticeAreaCode"]) || !Convert.IsDBNull(TestContext.DataRow["PracticeAreaDescription"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                practiceAreaDetails.PracticeAreaCode = Convert.ToString(TestContext.DataRow["PracticeAreaCode"]);
                practiceAreaDetails.PracticeAreaDescription = Convert.ToString(TestContext.DataRow["PracticeAreaDescription"]);
                practiceAreaDetails.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            }
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/PracticeArea/CreatePracticeArea", new StringContent(JsonConvert.SerializeObject(practiceAreaDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region UpdatePracticeAreaTest
        /// <summary>
        /// Test method for updating proficiency level
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PracticeAreaInfo.csv", "PracticeAreaInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PracticeAreaInfo.csv", "TestInput"), TestMethod]
        public void UpdatePracticeAreaTest()
        {
            PracticeAreaDetails practiceAreaDetails = new PracticeAreaDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["PracticeAreaId"]) || !Convert.IsDBNull(TestContext.DataRow["PracticeAreaCode"]) || !Convert.IsDBNull(TestContext.DataRow["PracticeAreaDescription"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                practiceAreaDetails.PracticeAreaId = Convert.ToInt32(TestContext.DataRow["PracticeAreaId"]);
                practiceAreaDetails.PracticeAreaCode = Convert.ToString(TestContext.DataRow["PracticeAreaCode"]);
                practiceAreaDetails.PracticeAreaDescription = Convert.ToString(TestContext.DataRow["PracticeAreaDescription"]);
                practiceAreaDetails.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            }

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/PracticeArea/UpdatePracticeArea", new StringContent(JsonConvert.SerializeObject(practiceAreaDetails), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region GetPracticeAreaTest
        /// <summary>
        /// Test method for retrieving proficiency levels
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PracticeAreaInfo.csv", "PracticeAreaInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PracticeAreaInfo.csv", "TestInput"), TestMethod]
        public void GetPracticeAreasTest()
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/PracticeArea/GetPracticeAreas"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion
    }
}
