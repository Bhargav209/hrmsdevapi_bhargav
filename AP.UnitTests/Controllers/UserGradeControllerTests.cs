﻿using System.Text;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using AP.DomainEntities;
using Newtonsoft.Json;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class UserGradeControllerTests:BaseControllerTests
    {
        #region CreateGradeTest
        /// <summary>
        /// CreateGradeTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Grades.csv", "Grades#csv", DataAccessMethod.Sequential),DeploymentItem(@"TestInput\Grades.csv")]
        public void CreateGradeTest()
        {
            GradeData gradeData = new GradeData();
            if (!Convert.IsDBNull(TestContext.DataRow["gradeCode"]) || !Convert.IsDBNull(TestContext.DataRow["gradeName"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
            {
                gradeData.GradeCode = TestContext.DataRow["gradeCode"].ToString();
                gradeData.GradeName = TestContext.DataRow["gradeName"].ToString();
                gradeData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"].ToString());
            }
            using (HttpClient client = new HttpClient())
                {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserGrade/CreateGrade/",new StringContent(JsonConvert.SerializeObject(gradeData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
           
        }
        #endregion

        #region UpdateGradeTest
        /// <summary>
        /// UpdateGradeTest
        /// </summary>
       
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Grades.csv", "Grades#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Grades.csv"), TestMethod]
        public void UpdateGradeTest()
        {
            GradeData gradeData = new GradeData();
            if (!Convert.IsDBNull(TestContext.DataRow["gradeId"]) || !Convert.IsDBNull(TestContext.DataRow["gradeCode"]) || !Convert.IsDBNull(TestContext.DataRow["gradeName"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
            {
                gradeData.GradeId = Convert.ToInt32(TestContext.DataRow["gradeId"]);
                gradeData.GradeCode = TestContext.DataRow["gradeCode"].ToString();
                gradeData.GradeName = TestContext.DataRow["gradeName"].ToString();
                gradeData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"].ToString());
            }
            using (HttpClient client = new HttpClient())
                {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserGrade/UpdateGrade/", new StringContent(JsonConvert.SerializeObject(gradeData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
           
        }
        #endregion

        #region GetGradesDetailsTest
        /// <summary>
        /// GetGradesDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetGradesDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserGrade/GetGradesDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region GetGradesByIdTest
        /// <summary>
        /// GetGradesByIdTest
        /// </summary>
        [TestMethod()]
        public void GetGradesByIdTest()
        {
            try
            {
                int gradeId = 1;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserGrade/GetGradesById?gradeId=" + gradeId))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion
      
    }
}