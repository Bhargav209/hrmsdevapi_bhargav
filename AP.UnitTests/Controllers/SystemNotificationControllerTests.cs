﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using System.Configuration;
using System;
using Newtonsoft.Json;
using System.Text;
using AP.DomainEntities;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class SystemNotificationControllerTests:BaseControllerTests
    {
      

        #region GetNotificationsTest
        /// <summary>
        /// GetNotificationsTest
        /// </summary>
        [TestMethod()]
        public void GetNotificationsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region CountNotificationsTest
        /// <summary>
        /// CountNotificationsTest
        /// </summary>
        [TestMethod()]
        public void CountNotificationsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/CountNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateItNotificationTest
        /// <summary>
        /// UpdateItNotificationTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\UpdateNotification.csv", "UpdateNotification#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\UpdateNotification.csv", "TestInput"), TestMethod]
        public void UpdateItNotificationTest()
        {
            try
            {
                string employeeCode = Convert.ToString(TestContext.DataRow["employeeCode"]);
                int taskId = Convert.ToInt32(TestContext.DataRow["taskId"]);

                string content = "employeeCode=" + employeeCode + "&&taskId=" + taskId + "";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/UpdateItNotification?" + content);
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetFinanceNotificationsTest
        /// <summary>
        /// GetFinanceNotificationsTest
        /// </summary>
        [TestMethod()]
        public void GetFinanceNotificationsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetFinanceNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region CountFinanceNotificationsTest
        /// <summary>
        /// CountFinanceNotificationsTest
        /// </summary>
        [TestMethod()]
        public void CountFinanceNotificationsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/CountFinanceNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateFinanceNotificationTest
        /// <summary>
        /// UpdateFinanceNotificationTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\UpdateNotification.csv", "UpdateNotification#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\UpdateNotification.csv", "TestInput"), TestMethod]
        public void UpdateFinanceNotificationTest()
        {
            try
            {
                string employeeCode = Convert.ToString(TestContext.DataRow["employeeCode"]);
                int taskId = Convert.ToInt32(TestContext.DataRow["taskId"]);

                string content = "employeeCode=" + employeeCode + "&&taskId=" + taskId + "";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/UpdateFinanceNotification?" + content);
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAdminNotificationsTest
        /// <summary>
        /// GetAdminNotificationsTest
        /// </summary>
        [TestMethod()]
        public void GetAdminNotificationsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetAdminNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region CountAdminNotificationsTest
        /// <summary>
        /// CountNotificationsTest
        /// </summary>
        [TestMethod()]
        public void CountAdminNotificationsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/CountAdminNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateAdminNotificationTest
        /// <summary>
        /// UpdateAdminNotificationTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\UpdateNotification.csv", "UpdateNotification#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\UpdateNotification.csv", "TestInput"), TestMethod]
        public void UpdateAdminNotificationTest()
        {
            try
            {
                string employeeCode = Convert.ToString(TestContext.DataRow["employeeCode"]);
                int taskId = Convert.ToInt32(TestContext.DataRow["taskId"]);

                string content = "employeeCode=" + employeeCode + "&&taskId=" + taskId + "";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/UpdateAdminNotification?" + content);
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetHRANotificationCountTest
        /// <summary>
        /// GetHRANotificationCountTest
        /// </summary>
        [TestMethod()]
        public void GetHRANotificationCountTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string content = "notificationTypeId=" + 2;
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetHRANotificationCount?" + content))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetNotificationCountTest
        /// <summary>
        /// GetNotificationCountTest
        /// </summary>
        [TestMethod()]
        public void GetNotificationCountTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string content = "notificationTypeId=" + 1;
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetNotificationCount?" + content))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetNotificationTypeTest
        /// <summary>
        /// GetNotificationTypeTest
        /// </summary>
        [TestMethod()]
        public void GetNotificationTypeTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetNotificationType"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAllEmailIdsTest
        /// <summary>
        /// GetAllEmailIdsTest
        /// </summary>
        [TestMethod()]
        public void GetAllEmailIdsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SystemNotification/GetAllEmailIds"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region SaveNotificationConfigTest
        /// <summary>
        /// SaveNotificationConfigTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmailNotificationConfiguration.csv", "EmailNotificationConfiguration#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmailNotificationConfiguration.csv", "TestInput"), TestMethod]
        public void SaveNotificationConfigTest()
        {
            try
            {
                EmailNotificationConfiguration notificationConfig = new EmailNotificationConfiguration();
                notificationConfig.NotificationTypeID = (int) TestContext.DataRow["notificationCode"];
                notificationConfig.Description = Convert.ToString(TestContext.DataRow["notificationDesc"]);
                notificationConfig.EmailFrom = Convert.ToString(TestContext.DataRow["emailFrom"]);
                notificationConfig.EmailTo = Convert.ToString(TestContext.DataRow["emailTo"]);
                notificationConfig.EmailCC = Convert.ToString(TestContext.DataRow["emailCC"]);
                notificationConfig.EmailSubject = Convert.ToString(TestContext.DataRow["emailSubject"]);
                notificationConfig.EmailContent = Convert.ToString(TestContext.DataRow["emailContent"]);
                notificationConfig.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/SystemNotification/SaveNotificationConfig", new StringContent(JsonConvert.SerializeObject(notificationConfig).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}