﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using System.Collections.Generic;
using Newtonsoft.Json;
using AP.Services.Controllers.Tests;
using System.Net;
using AP.DataStorage;
using System.Text;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class MasterDataControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region GetManagersAndCompetencyLeadsTest
        /// <summary>
        /// GetManagersAndCompetencyLeadsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetManagersAndCompetencyLeadsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["EmployeeID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/MasterData/GetManagersAndCompetencyLeads"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        List<GenericType> associateList = JsonConvert.DeserializeObject<List<GenericType>>(associateData);
                        GenericType names = associateList.Find(name => name.Id == EmployeeID);
                        Assert.AreEqual(EmployeeID, names.Id);
                    }
                }

            };
        }
        #endregion

        #region GetEmployeesByDepartmentAndProjectIDTest
        /// <summary>
        /// GetEmployeesByDepartmentAndProjectIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetEmployeesByDepartmentAndProjectIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int ProjectID = Convert.ToInt32(TestContext.DataRow["Project"]);
                int DepartmentID = Convert.ToInt32(TestContext.DataRow["Department"]);
                string EmployeeName = Convert.ToString(TestContext.DataRow["EmployeeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/MasterData/GetEmployeesByDepartmentAndProjectID?projectId=" + ProjectID + "&&departmentId=" + DepartmentID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        List<GenericType> associateList = JsonConvert.DeserializeObject<List<GenericType>>(associateData);
                        GenericType employee = associateList.Find(name => name.Name.ToLower() == EmployeeName.ToLower());
                        Assert.AreEqual(EmployeeName, employee.Name);
                    }
                }

            };
        }
        #endregion

        #region GetEmployeeNameByEmployeeIdTest
        /// <summary>
        /// GetEmployeeNameByEmployeeIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetEmployeeNameByEmployeeIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["EmployeeID"]);
                string EmployeeName = Convert.ToString(TestContext.DataRow["EmployeeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/MasterData/GetEmployeeNameByEmployeeId?employeeID=" + EmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        string employeeName = JsonConvert.DeserializeObject<string>(associateData);
                        Assert.AreEqual(EmployeeName, employeeName);
                    }
                }

            };
        }
        #endregion

        #region GetAssociatesByDepartmentIdTest
        /// <summary>
        /// GetAssociatesByDepartmentIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetDepartmentDetailsByEmployeeIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int DepartmentID = Convert.ToInt32(TestContext.DataRow["DepartmentID"]);
                string EmployeeName = Convert.ToString(TestContext.DataRow["EmployeeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/MasterData/GetAssociatesByDepartmentId?departmentID=" + DepartmentID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        List<GenericType> associateList = JsonConvert.DeserializeObject<List<GenericType>>(associateData);
                        GenericType names = associateList.Find(name => name.Name.ToLower() == EmployeeName.ToLower());
                        Assert.AreEqual(EmployeeName, names.Name);
                    }
                }

            };
        }
        #endregion

        #region GetDepartmentTypes
        /// <summary>
        /// GetDepartmentTypes
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]
        public void GetDepartmentTypesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Name = Convert.ToString(TestContext.DataRow["DepartmentTypeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/MasterData/GetDepartmentTypes"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var depatmentTypeData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstdepatmentTypeData = JsonConvert.DeserializeObject<List<GenericType>>(depatmentTypeData);
                        GenericType departmentType = lstdepatmentTypeData.Find(dept => dept.Name.ToLower().Trim() == Name.ToLower().Trim());
                        Assert.AreEqual(Name, departmentType.Name);
                    }
                }

            };
        }
        #endregion

        #region GetDepartmentByDepartmentTypeId
        /// <summary>
        /// GetDepartmentByDepartmentTypeId
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MenuData.csv", "MenuData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MenuData.csv"), TestMethod]

        public void GetDepartmentByDepartmentTypeIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int DepartmentTypeID = Convert.ToInt32(TestContext.DataRow["DepartmentTypeID"]);
                string Name = Convert.ToString(TestContext.DataRow["Name"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/MasterData/GetDepartmentByDepartmentTypeId?departmentTypeId=" + DepartmentTypeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var departmentData = result.Result;
                        //deserialize to your class
                        List<GenericType> departmentList = JsonConvert.DeserializeObject<List<GenericType>>(departmentData);
                        GenericType departmentNames = departmentList.Find(dept => dept.Name.ToLower() == Name.ToLower());
                        Assert.AreEqual(Name, departmentNames.Name);
                    }
                }

            };
        }
        #endregion
    }
}
