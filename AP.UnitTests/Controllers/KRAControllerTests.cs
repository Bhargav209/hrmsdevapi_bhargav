﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using AP.DomainEntities;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using AP.Services.Controllers.Tests;
using System;
using AP.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.UnitTests
{
    [TestClass()]
    public class KRAControllerTests : BaseControllerTests
    {
        [TestMethod()]
        public void SaveKRADefinitionTest()
        {

        }

        [TestMethod()]
        public void GetKRAAspectsTest()
        {

        }

        [TestMethod()]
        public void GetKRADefinitionsTest()
        {

        }


        [TestMethod()]
        public void GetKRAElementsTest()
        {

        }

        [TestMethod()]
        public void SaveAssociateKRATest()
        {

        }

        [TestMethod()]
        public void GetAssociateKRATest()
        {

        }

        [TestMethod()]
        public void GetRolesByProjectTest()
        {

        }

        [TestMethod()]
        public void GetAssociatesByRoleTest()
        {

        }

        #region GetAssociateKRADetails
        /// <summary>
        /// Get Associate KRA Details
        /// </summary> 
        [TestMethod]
        public void GetAssociateKRADetails()
        {
            string emailId = "srinivasa.karri@senecaglobal.com";
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetAssociateKRADetails?emailId=" + emailId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            }

        }
        #endregion

        #region KRAAssignmentTest
        /// <summary>
        /// KRA Assignment Test method
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAAssignment.csv", "KRAAssignment#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAAssignment.csv", "TestInput"), TestMethod]
        public void KRAAssignmentTest()
        {
            AssociateKRAData associateKRAData = new AssociateKRAData();
            associateKRAData.Year = Convert.ToString(TestContext.DataRow["Year"]);
            associateKRAData.RoleName = Convert.ToString(TestContext.DataRow["Role"]);
            associateKRAData.EmployeeCode = Convert.ToString(TestContext.DataRow["EmployeeCode"]);
            associateKRAData.EffectiveDate = Convert.ToString(TestContext.DataRow["EffectiveDate"]);
            associateKRAData.EffectiveTime = Convert.ToString(TestContext.DataRow["EffectiveTime"]);
            //associateKRAData.ScheduledDateTime = Convert.ToDateTime(TestContext.DataRow["ScheduledDateTime"]);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/KRAAssignment", new StringContent(JsonConvert.SerializeObject(associateKRAData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion

        #region KRA Reject

        /// <summary>
        /// RejectKras - test method.
        /// </summary>
        [TestMethod]
        public void RejectKrasTest()
        {
            string filePath = "KRAReject.json";
            dynamic data = LoadJson(filePath);
            dynamic departmentData = new { kRAData = data.kRAData, status = data.status };
            StringContent content = new StringContent(JsonConvert.SerializeObject(departmentData).ToString(), Encoding.UTF8, "application/json");

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/RejectKRAs", content).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }

        #endregion

        #region KRA Approvals.
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void SubmitKRAForApprovalTest()
        {
            string filePath = "KRAReviewApproved.json";
            dynamic data = LoadJson(filePath);
            dynamic departmentData = new { kRAData = data.kRAData, status = data.status };
            StringContent content = new StringContent(JsonConvert.SerializeObject(departmentData).ToString(), Encoding.UTF8, "application/json");

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/SubmitKRAForApproval", content).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }

        #endregion

        #region CloneKRARoleTemplateTest
        /// <summary>
        /// CloneKRARoleTemplateTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void CloneKRATemplateForFinancialYearTest()
        {
            try
            {
                KRARoleData kraRoleData = new KRARoleData();
                kraRoleData.FinancialYearID = 2;
                kraRoleData.CloneFinancialYearID = 1;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CloneKRATemplateForFinancialYear", new StringContent(JsonConvert.SerializeObject(kraRoleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region DeleteKRAByFinancialYearAndRoleIdTest
        /// <summary>
        /// DeleteKRAByFinancialYearAndRoleIdTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void DeleteKRARoleByFinancialYearAndRoleIdTest()
        {
            int roleID = 1;
            int financialYearId = 2;
            KRA kra = new KRA();
            kra.DeleteKRARoleByFinancialYearAndRoleId(roleID, financialYearId);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/DeleteKRAByFinancialYearAndRoleId", new StringContent(JsonConvert.SerializeObject(kra), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region SubmitKRAForReviewTest
        /// <summary>
        /// SubmitKRAForReviewTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void SubmitKRAForReviewTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(7);
                kraSetData.DepartmentIDs = deptartment;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/SubmitKRAForHRHeadReview", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region SubmitKRAForApprovalTest
        /// <summary>
        /// SubmitKRAForApprovalTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAWorkFlow.csv", "KRAWorkFlow#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAWorkFlow.csv"), TestMethod]
        public void SubmitKRAForApprovalsTest()
        {
            try
            {
                KRAWorkFlowData kraWorkFlowData = new KRAWorkFlowData();
                kraWorkFlowData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraWorkFlowData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraWorkFlowData.DepartmentID = Convert.ToInt32(TestContext.DataRow["DepartmentID"]);
                List<GenericType> kraGroups = new List<GenericType>();
                kraGroups.Add(new GenericType { Id = 32 });
                kraWorkFlowData.KRAGroupIDs = kraGroups;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/SubmitKRAForApproval", new StringContent(JsonConvert.SerializeObject(kraWorkFlowData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region KRAPendingForReviewByHODTest
        /// <summary>
        /// KRAPendingForReviewByHODTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void KRAPendingForReviewByHODTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int ToEmployeeID = Convert.ToInt32(TestContext.DataRow["ToEmployeeID"]);
                string DepartmentCode = Convert.ToString(TestContext.DataRow["DepartmentCode"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/KRAPendingForReviewByHOD?toEmployeeID=" + ToEmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRASetData> PendingKRA = JsonConvert.DeserializeObject<List<KRASetData>>(KRAData);
                        KRASetData kras = PendingKRA.Find(kra => kra.DepartmentCode.ToLower() == DepartmentCode.ToLower());
                        Assert.AreEqual(DepartmentCode, kras.DepartmentCode);
                    }
                }

            };
        }
        #endregion

        #region KRAPendingForReviewByHRHeadTest
        /// <summary>
        /// KRAPendingForReviewByHRHeadTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void KRAPendingForReviewByHRHeadTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int ToEmployeeID = Convert.ToInt32(TestContext.DataRow["ToEmployeeID"]);
                string FinancialYear = Convert.ToString(TestContext.DataRow["FinancialYear"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/KRAPendingForReviewByHRHead?toEmployeeID=" + ToEmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRASetData> PendingKRA = JsonConvert.DeserializeObject<List<KRASetData>>(KRAData);
                        KRASetData kras = PendingKRA.Find(kra => kra.DepartmentCode.ToLower() == FinancialYear.ToLower());
                        Assert.AreEqual(FinancialYear, kras.FinancialYear);
                    }
                }

            };
        }
        #endregion

        #region GetKRADepartmentStatusTest
        /// <summary>
        /// GetKRADepartmentStatusTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetKRADepartmentStatusTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string DepartmentCode = Convert.ToString(TestContext.DataRow["DepartmentCode"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRADepartmentStatus"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRASetData> PendingKRA = JsonConvert.DeserializeObject<List<KRASetData>>(KRAData);
                        KRASetData kras = PendingKRA.Find(kra => kra.DepartmentCode.ToLower() == DepartmentCode.ToLower());
                        Assert.AreEqual(DepartmentCode, kras.DepartmentCode);
                    }
                }

            };
        }
        #endregion

        #region GetKRADepartmentWorkFlowTest
        /// <summary>
        /// GetKRADepartmentWorkFlowTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetKRADepartmentWorkFlowTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                int DepartmentID = Convert.ToInt32(TestContext.DataRow["DepartmentID"]);
                string StatusCode = Convert.ToString(TestContext.DataRow["StatusCode"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRADepartmentWorkFlow?financialYearID=" + FinancialYearID + "&&departmentID=" + DepartmentID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRASetData> PendingKRA = JsonConvert.DeserializeObject<List<KRASetData>>(KRAData);
                        KRASetData kras = PendingKRA.Find(kra => kra.StatusCode.ToLower() == StatusCode.ToLower());
                        Assert.AreEqual(StatusCode, kras.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region KRAPendingForReviewByMDTest
        /// <summary>
        /// KRAPendingForReviewByMDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void KRAPendingForReviewByMDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int ToEmployeeID = Convert.ToInt32(TestContext.DataRow["ToEmployeeID"]);
                string DepartmentCode = Convert.ToString(TestContext.DataRow["DepartmentCode"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/KRAPendingForReviewByMD?toEmployeeID=" + ToEmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRASetData> PendingKRA = JsonConvert.DeserializeObject<List<KRASetData>>(KRAData);
                        KRASetData kras = PendingKRA.Find(kra => kra.DepartmentCode.ToLower() == DepartmentCode.ToLower());
                        Assert.AreEqual(DepartmentCode, kras.DepartmentCode);
                    }
                }

            };
        }
        #endregion

        #region SubmitKRAForApprovalTest
        /// <summary>
        /// SubmitKRAForApprovalTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAWorkFlow.csv", "KRAWorkFlow#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAWorkFlow.csv"), TestMethod]
        public void SubmitKRAForDHApprovalTest()
        {
            try
            {
                KRAWorkFlowData kraWorkFlowData = new KRAWorkFlowData();
                kraWorkFlowData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraWorkFlowData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraWorkFlowData.DepartmentID = Convert.ToInt32(TestContext.DataRow["DepartmentID"]);
                List<GenericType> kraGroups = new List<GenericType>();
                kraGroups.Add(new GenericType { Id = 31 });
                kraWorkFlowData.KRAGroupIDs = kraGroups;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/ApproveKRA", new StringContent(JsonConvert.SerializeObject(kraWorkFlowData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region ApproveKRAByHRHeadTest
        /// <summary>
        /// ApproveKRAByHRHeadTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void ApproveKRAByHRHeadTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(23);
                kraSetData.DepartmentIDs = deptartment;
                kraSetData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/ApproveKRA", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region RejectKRAByMDTest
        /// <summary>
        /// RejectKRAByMDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void RejectKRAByMDTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(5);
                kraSetData.DepartmentIDs = deptartment;
                kraSetData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/RejectKRAByMD", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region AcceptKRAByDepartmentHeadTest
        /// <summary>
        /// AcceptKRAByDepartmentHeadTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void AcceptKRAByDepartmentHeadTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(25);
                kraSetData.DepartmentIDs = deptartment;
                kraSetData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/ApproveKRA", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region SendBackForHRHeadReviewTest
        /// <summary>
        /// SendBackForHRHeadReviewTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void SendBackForHRHeadReviewTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(25);
                kraSetData.DepartmentIDs = deptartment;
                kraSetData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/SendBackForHRHeadReview", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region SendBackForHRMReviewTest
        /// <summary>
        /// SendBackForHRMReviewTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void SendBackForHRMReviewTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(23);
                kraSetData.DepartmentIDs = deptartment;
                kraSetData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/SendBackForHRMReview", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region SendKRAForModificationTest
        /// <summary>
        /// SendKRAForModificationTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void SendKRAForModificationTest()
        {
            try
            {
                KRASetData kraSetData = new KRASetData();
                kraSetData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                kraSetData.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                kraSetData.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(2);
                kraSetData.DepartmentIDs = deptartment;
                kraSetData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/SendKRAForModification", new StringContent(JsonConvert.SerializeObject(kraSetData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion

        #region GetDepartmentDetailsByEmployeeIDTest
        /// <summary>
        /// GetDepartmentDetailsByEmployeeIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetDepartmentDetailsByEmployeeIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeID = 59;//Convert.ToInt32(TestContext.DataRow["EmployeeID"]);
                string DepartmentCode = "Delivery";//Convert.ToString(TestContext.DataRow["DepartmentCode"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetDepartmentsByDepartmentHeadID?employeeID=" + EmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<GenericType> PendingKRA = JsonConvert.DeserializeObject<List<GenericType>>(KRAData);
                        GenericType kras = PendingKRA.Find(kra => kra.Name.ToLower() == DepartmentCode.ToLower());
                        Assert.AreEqual(DepartmentCode, kras.Name);
                    }
                }

            };
        }
        #endregion

        #region CreateKRAForRoleTest
        /// <summary>
        /// CreateKRAForRoleTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void CreateKRAByHRHeadTest()
        {
            try
            {

                KRARoleData roleData = new KRARoleData();
                roleData.RoleID = Convert.ToInt32(TestContext.DataRow["RoleID"]);
                roleData.KRAAspectID = Convert.ToInt32(TestContext.DataRow["KRAAspectID"]);
                roleData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                roleData.KRAAspectMetric = Convert.ToString(TestContext.DataRow["KRAAspectMetric"]);
                roleData.KRAAspectTarget = Convert.ToString(TestContext.DataRow["KRAAspectTarget"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CreateKRAByHRHead", new StringContent(JsonConvert.SerializeObject(roleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CreateKRAForRoleTest
        /// <summary>
        /// CreateKRAForRoleTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void CreateKRAByDepartmentHeadTest()
        {
            try
            {

                KRARoleData roleData = new KRARoleData();
                roleData.RoleID = Convert.ToInt32(TestContext.DataRow["RoleID"]);
                roleData.KRAAspectID = Convert.ToInt32(TestContext.DataRow["KRAAspectID"]);
                roleData.FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                roleData.KRAAspectMetric = Convert.ToString(TestContext.DataRow["KRAAspectMetric"]);
                roleData.KRAAspectTarget = Convert.ToString(TestContext.DataRow["KRAAspectTarget"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CreateKRAByDepartmentHead", new StringContent(JsonConvert.SerializeObject(roleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetLoggedInUserRolesTest
        /// <summary>
        /// GetLoggedInUserRolesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetLoggedInUserRolesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string RoleName = Convert.ToString(TestContext.DataRow["RoleName"]);
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["EmployeeID"]);
                int FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetLoggedInUserRoles?employeeID=" + EmployeeID + "&&finanicalYearId=" + FinancialYearID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var rolesData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstRolesData = JsonConvert.DeserializeObject<List<GenericType>>(rolesData);
                        GenericType roleData = lstRolesData.Find(role => role.Name.ToLower() == RoleName.ToLower());
                        Assert.AreEqual(RoleName, roleData.Name);
                    }
                }

            };
        }
        #endregion

        #region CreateKRADefinitionTest
        /// <summary>
        /// CreateKRADefinitionTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void CreateKRADefinitionTest()
        {
            try
            {
                KRADefinitionData kraDefinitionData = new KRADefinitionData
                {
                    KRAGroupId = Convert.ToInt32(TestContext.DataRow["KRAGroupId"]),
                    KRAAspectId = Convert.ToInt32(TestContext.DataRow["KRAAspectID"]),
                    FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"])
                };
                List<MetricAndTarget> lstMetric = new List<MetricAndTarget>();

                MetricAndTarget lstMetricAndTarget = new MetricAndTarget();
                lstMetricAndTarget.Metric = Convert.ToString(TestContext.DataRow["Metric"]);
                lstMetricAndTarget.KRAMeasurementTypeId = Convert.ToInt32(TestContext.DataRow["KRAMeasurementTypeID"]);
                lstMetricAndTarget.KRAOperatorId = Convert.ToInt32(TestContext.DataRow["KRAOperatorId"]);
                lstMetricAndTarget.KRAScaleMasterId =  Convert.ToInt32(TestContext.DataRow["KRAScaleMasterId"]);
                lstMetricAndTarget.KRATargetValue =  Convert.ToInt32(TestContext.DataRow["KRATargetValue"]);
                lstMetricAndTarget.KRATargetText =  Convert.ToString(TestContext.DataRow["KRATargetText"]);
                lstMetricAndTarget.KRATargetPeriodId = Convert.ToInt32(TestContext.DataRow["KRATargetPeriodId"]);
                lstMetric.Add(lstMetricAndTarget);
                kraDefinitionData.lstMetricAndTarget = lstMetric;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CreateKRADefinition", new StringContent(JsonConvert.SerializeObject(kraDefinitionData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CreateKRAGroupTest
        /// <summary>
        /// CreateKRAGroupTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void CreateKRAGroupTest()
        {
            try
            {
                KRAGroupData kraGroupData = new KRAGroupData
                {
                    DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]),
                    RoleCategoryId = Convert.ToInt32(TestContext.DataRow["RoleCategoryId"]),
                    ProjectTypeId = Convert.ToInt32(TestContext.DataRow["ProjectTypeId"]),
                    FinancialYearId = Convert.ToInt32(TestContext.DataRow["YearId"]),
                    KRATitle = Convert.ToString(TestContext.DataRow["KRATitle"])
                };

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CreateKRAGroup", new StringContent(JsonConvert.SerializeObject(kraGroupData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeleteKRADefinitionTest
        /// <summary>
        /// DeleteKRADefinitionTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void DeleteKRADefinitionTest()
        {
            int kraDefinitionId = 3;
            string RoleName = "HRM";
            KRA kra = new KRA();
            KRADefinitionData kradefinitionaData = new KRADefinitionData();
            kradefinitionaData.KRAAspectId = 3;
            kradefinitionaData.KRAGroupId = 3;
            kradefinitionaData.FinancialYearId = 3;
            kradefinitionaData.StatusId = 21;
            kradefinitionaData.RoleName = RoleName;
            kra.DeleteKRADefinition(kradefinitionaData);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/DeleteKRADefinition", new StringContent(JsonConvert.SerializeObject(kra), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region GetKRADefinitionByIdTest
        /// <summary>
        /// GetKRADefinitionByIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetKRADefinitionByIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int kraGroupId = Convert.ToInt32(TestContext.DataRow["kraGroupId"]);
                string KRAAspectName = Convert.ToString(TestContext.DataRow["KRAAspectName"]);
                int FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRADefinitionById?kraGroupId=" + kraGroupId + "&&financialYearId=" + FinancialYearID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var kradefinition = result.Result;
                        //deserialize to your class
                        List<KRADefinitionData> kraDefinitionList = JsonConvert.DeserializeObject<List<KRADefinitionData>>(kradefinition);
                        KRADefinitionData kraDefinitions = kraDefinitionList.Find(kra => kra.KRAAspectName.ToLower() == KRAAspectName.ToLower());
                        Assert.AreEqual(KRAAspectName, kraDefinitions.KRAAspectName);
                    }
                }

            };
        }
        #endregion

        #region GetKRAGroupsTest
        /// <summary>
        /// GetKRAGroupsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void GetKRAGroupsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string RoleCategoryName = Convert.ToString(TestContext.DataRow["RoleCategoryName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRAGroups"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAAGroupData = result.Result;
                        //deserialize to your class
                        List<KRAGroupData> lstKRAGroupData = JsonConvert.DeserializeObject<List<KRAGroupData>>(KRAAGroupData);
                        KRAGroupData KRA = lstKRAGroupData.Find(kra => kra.RoleCategoryName.ToLower().Trim() == RoleCategoryName.ToLower().Trim());
                        Assert.AreEqual(RoleCategoryName, KRA.RoleCategoryName);
                    }
                }

            };
        }
        #endregion

        #region GetKRAGroupsByFinancialYearIdTest
        /// <summary>
        /// GetKRAGroupsByFinancialYearIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetKRAGroupsByFinancialYearIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int financialYearId = Convert.ToInt32(TestContext.DataRow["financialYearId"]);
                string RoleCategoryName = Convert.ToString(TestContext.DataRow["RoleCategoryName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRAGroups?financialYearId=" + financialYearId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAAGroupData = result.Result;
                        //deserialize to your class
                        List<KRAGroupData> lstKRAGroupData = JsonConvert.DeserializeObject<List<KRAGroupData>>(KRAAGroupData);
                        KRAGroupData KRA = lstKRAGroupData.Find(kra => kra.RoleCategoryName.ToLower() == RoleCategoryName.ToLower());
                        Assert.AreEqual(RoleCategoryName, KRA.RoleCategoryName);
                    }
                }

            };
        }
        #endregion

        #region UpdateKRADefinitionTest
        /// <summary>
        /// UpdateKRADefinitionTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void UpdateKRADefinitionTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                KRADefinitionData kraDefinitionData = new KRADefinitionData
                {
                    RoleName = TestContext.DataRow["RoleName"].ToString(),
                    StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"])
                };
                List<MetricAndTarget> lstMetric = new List<MetricAndTarget>();

                MetricAndTarget lstMetricAndTarget = new MetricAndTarget();
                lstMetricAndTarget.Metric = Convert.ToString(TestContext.DataRow["Metric"]);
                lstMetricAndTarget.KRAMeasurementTypeId =Convert.ToInt32(TestContext.DataRow["KRAMeasurementTypeID"]);
                lstMetricAndTarget.KRAOperatorId = Convert.ToInt32(TestContext.DataRow["KRAOperatorId"]);
                lstMetricAndTarget.KRAScaleMasterId = Convert.ToInt32(TestContext.DataRow["KRAScaleMasterId"]);
                lstMetricAndTarget.KRATargetValue =Convert.ToInt32(TestContext.DataRow["KRATargetValue"]);
                lstMetricAndTarget.KRATargetText = Convert.ToString(TestContext.DataRow["KRATargetText"]);
                lstMetricAndTarget.KRATargetPeriodId = Convert.ToInt32(TestContext.DataRow["KRATargetPeriodId"]);
                lstMetricAndTarget.KRADefinitionId = Convert.ToInt32(TestContext.DataRow["KRADefinitionId"]);
                lstMetric.Add(lstMetricAndTarget);
                kraDefinitionData.lstMetricAndTarget = lstMetric;

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/UpdateKRADefinition", new StringContent(JsonConvert.SerializeObject(kraDefinitionData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region InitiateKRAsTest
        /// <summary>
        /// InitiateKRAsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void InitiateKRAsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                int FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
                KRA kra = new KRA();
                kra.InitiateKRAs(FinancialYearId);
            };
        }
        #endregion

        #region GetKRAOperatorsTest
        /// <summary>
        /// GetKRAOperatorsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void GetKRAOperatorsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Name = Convert.ToString(TestContext.DataRow["OperatorsName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRAOperators"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAOperatorData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstKRAOperatorData = JsonConvert.DeserializeObject<List<GenericType>>(KRAOperatorData);
                        GenericType KRA = lstKRAOperatorData.Find(kra => kra.Name.ToLower().Trim() == Name.ToLower().Trim());
                        Assert.AreEqual(Name, KRA.Name);
                    }
                }

            };
        }
        #endregion

        #region GetKRAMeasurementTypeTest
        /// <summary>
        /// GetKRAMeasurementTypeTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void GetKRAMeasurementTypeTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Name = Convert.ToString(TestContext.DataRow["MeasurementTypeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRAMeasurementType"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var MeasurementTypeData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstMeasurementTypeData = JsonConvert.DeserializeObject<List<GenericType>>(MeasurementTypeData);
                        GenericType KRA = lstMeasurementTypeData.Find(kra => kra.Name.ToLower().Trim() == Name.ToLower().Trim());
                        Assert.AreEqual(Name, KRA.Name);
                    }
                }

            };
        }
        #endregion

        #region GetKRAScalesTest
        /// <summary>
        /// GetKRAScalesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void GetKRAScalesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int MaximumScale = Convert.ToInt32(TestContext.DataRow["MaximumScale"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRAScales"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRAScaleData> lstKRAData = JsonConvert.DeserializeObject<List<KRAScaleData>>(KRAData);
                        KRAScaleData maximum = lstKRAData.Find(kra => kra.MaximumScale == MaximumScale);
                        Assert.AreEqual(MaximumScale, maximum.MaximumScale);
                    }
                }

            };
        }
        #endregion

        #region GetKRATargetPeriodsTest
        /// <summary>
        /// GetKRATargetPeriodsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void GetKRATargetPeriodsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Name = Convert.ToString(TestContext.DataRow["TargetPeriodsName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRATargetPeriods"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRATargetData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstKRATargetData = JsonConvert.DeserializeObject<List<GenericType>>(KRATargetData);
                        GenericType KRA = lstKRATargetData.Find(kra => kra.Name.ToLower().Trim() == Name.ToLower().Trim());
                        Assert.AreEqual(Name, KRA.Name);
                    }
                }

            };
        }
        #endregion

        #region GetKRAScalesTest
        /// <summary>
        /// GetKRAScalesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void GetKRAScaleValuesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Scale = Convert.ToString(TestContext.DataRow["Scale"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRA/GetKRAScaleValues"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstKRAData = JsonConvert.DeserializeObject<List<GenericType>>(KRAData);
                        GenericType scale = lstKRAData.Find(kra => kra.Name.ToLower().Trim() == Scale.ToLower().Trim());
                        Assert.AreEqual(Scale, scale.Name);
                    }
                }

            };
        }
        #endregion

        #region CloneKRAByFinancialYearIdTest
        /// <summary>
        /// CloneKRAByFinancialYearIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void CloneKRAByFinancialYearIdTest()
        {
            try
            {
                CloneKRAData cloneKRAData = new CloneKRAData();
                cloneKRAData.FromFinancialYearId = Convert.ToInt32(TestContext.DataRow["FromFinancialYearId"]);
                cloneKRAData.ToFinancialYearId =  Convert.ToInt32(TestContext.DataRow["ToFinancialYearId"]);
                cloneKRAData.CloneType = Convert.ToInt32(TestContext.DataRow["CloneType"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CloneKRAs", new StringContent(JsonConvert.SerializeObject(cloneKRAData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CloneKRAByDepartmentIdTest
        /// <summary>
        /// CloneKRAByDepartmentIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void CloneKRAByDepartmentIdTest()
        {
            try
            {
                CloneKRAData cloneKRAData = new CloneKRAData();
                cloneKRAData.FromFinancialYearId = Convert.ToInt32(TestContext.DataRow["FromFinancialYearId"]);
                cloneKRAData.ToFinancialYearId = Convert.ToInt32(TestContext.DataRow["ToFinancialYearId"]);
                cloneKRAData.CloneType = Convert.ToInt32(TestContext.DataRow["CloneType"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(1);
                deptartment.Add(2);
                cloneKRAData.DepartmentIds = deptartment;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CloneKRAs", new StringContent(JsonConvert.SerializeObject(cloneKRAData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CloneKRAByDepartmentAndGroupIdTest
        /// <summary>
        /// CloneKRAByDepartmentAndGroupIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]
        public void CloneKRAByDepartmentAndGroupIdTest()
        {
            try
            {
                CloneKRAData cloneKRAData = new CloneKRAData();
                cloneKRAData.FromFinancialYearId = Convert.ToInt32(TestContext.DataRow["FromFinancialYearId"]);
                cloneKRAData.ToFinancialYearId = Convert.ToInt32(TestContext.DataRow["ToFinancialYearId"]);
                cloneKRAData.CloneType = Convert.ToInt32(TestContext.DataRow["CloneType"]);
                List<int> deptartment = new List<int>();
                deptartment.Add(1);
                cloneKRAData.DepartmentIds = deptartment;
                List<int> kraGroup = new List<int>();
                kraGroup.Add(2);
                cloneKRAData.GroupIds = kraGroup;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CloneKRAs", new StringContent(JsonConvert.SerializeObject(cloneKRAData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeleteKRAGroupTest
        /// <summary>
        /// DeleteKRAGroupTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void DeleteKRAGroupTest()
        {
            KRASubmittedGroup kraSubmittedGroup = new KRASubmittedGroup();
            kraSubmittedGroup.FinancialYearId = 1;
            List<int> KRAGroupIds = new List<int>();
            KRAGroupIds.Add(8);
            kraSubmittedGroup.KRAGroupIds = KRAGroupIds;
            kraSubmittedGroup.RoleName = "fdvfdv";
            KRA kra = new KRA();
            kra.DeleteKRAGroup(kraSubmittedGroup);
        }
        #endregion

        #region AddKRACommentsTest
        /// <summary>
        /// AddKRACommentsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv", "TestInput"), TestMethod]
        public void AddKRACommentsTest()
        {
            try
            {
                KRAWorkFlowData kraWorkFlowData = new KRAWorkFlowData
                {
                    FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]),
                    FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]),
                    Comments = Convert.ToString(TestContext.DataRow["Comments"]),
                    StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"])
                };

                List<GenericType> KRAGroupIDs = new List<GenericType>();

                GenericType lstGroupIDs = new GenericType();
                lstGroupIDs.Id = Convert.ToInt32(TestContext.DataRow["Id"]);
                KRAGroupIDs.Add(lstGroupIDs);
                kraWorkFlowData.KRAGroupIDs = KRAGroupIDs;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/AddKRAComments", new StringContent(JsonConvert.SerializeObject(kraWorkFlowData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CreateKRAMeasurementType
        /// <summary>
        /// CreateKRAMeasurementType
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void CreateKRAMeasurementType()
        {
            try
            {

                KRAMeasurementTypeData KRAMeasurementTypeData = new KRAMeasurementTypeData();
                KRAMeasurementTypeData.KRAMeasurementType = "Date";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/CreateKRAMeasurementType", new StringContent(JsonConvert.SerializeObject(KRAMeasurementTypeData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateKRAMeasurementType
        /// <summary>
        /// UpdateKRAMeasurementType
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void UpdateKRAMeasurementType()
        {
            try
            {

                KRAMeasurementTypeData KRAMeasurementTypeData = new KRAMeasurementTypeData();
                KRAMeasurementTypeData.KRAMeasurementTypeId = 4;
                KRAMeasurementTypeData.KRAMeasurementType = "Date";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/UpdateKRAMeasurementType", new StringContent(JsonConvert.SerializeObject(KRAMeasurementTypeData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeleteKRAMeasurementType
        /// <summary>
        /// DeleteKRAMeasurementType
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRARole.csv", "KRARole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRARole.csv", "TestInput"), TestMethod]
        public void DeleteKRAMeasurementType()
        {
            int KRAMeasurementTypeId = 5;
            KRA KRA = new KRA();
            KRA.DeleteKRAMeasurementType(KRAMeasurementTypeId);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRA/DeleteKRAMeasurementType", new StringContent(JsonConvert.SerializeObject(KRA), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion
    }
}