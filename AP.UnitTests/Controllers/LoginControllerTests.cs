﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using AP.DomainEntities;
using System.Configuration;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class LoginControllerTests
    {
        string serviceURL = ConfigurationManager.AppSettings["ServiceURL"];
        public TestContext TestContext { get; set; }

        #region UserLoginTest
        /// <summary>
        /// UserLoginTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Login.csv", "Login#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Login.csv", "TestInput"), TestMethod]
        public void UserLoginTest()
        {
            Login loginObj = new Login();
            loginObj.userName = Convert.ToString(TestContext.DataRow["userName"]);
            loginObj.password = Convert.ToString(TestContext.DataRow["password"]);

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Login/UserLogin", new StringContent(JsonConvert.SerializeObject(loginObj).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    bool result = Convert.ToBoolean(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(result, true);
                }

            };
        }
        #endregion

        #region GetUserLogoutTest
        /// <summary>
        /// GetUserLogoutTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Login.csv", "Login#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Login.csv", "TestInput"), TestMethod]
        public void GetUserLogoutTest()
        {
            Login loginObj = new Login();
            loginObj.userName = Convert.ToString(TestContext.DataRow["userName"]);
            loginObj.password = Convert.ToString(TestContext.DataRow["password"]);
            string content = "userName=" + loginObj.userName + "&&password=" + loginObj.password;

            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Login/GetUserLogout?" + content);
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion
    }
}
