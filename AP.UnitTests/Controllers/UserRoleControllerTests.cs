﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.Net.Http;
using System.Net;
using System;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class UserRoleControllerTests : BaseControllerTests
    {

        #region GetRoleMasterDetailsTest
        /// <summary>
        /// GetRoleMasterDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetRoleMasterDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetRoleMasterDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region AssignRoleTest
        /// <summary>
        /// AssignRoleTest
        /// </summary>
        [TestMethod()]
        public void AssignRoleTest()
        {
            try
            {
                UserRoleData userRoleData = new UserRoleData();
                if (!Convert.IsDBNull(TestContext.DataRow["userName"]) || !Convert.IsDBNull(TestContext.DataRow["roleID"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
                {
                    userRoleData.UserName = TestContext.DataRow["userName"].ToString();
                    userRoleData.RoleId = Convert.ToInt32(TestContext.DataRow["gradeCode"]);
                    userRoleData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"].ToString());
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserRole/AssignRole/", new StringContent(JsonConvert.SerializeObject(userRoleData).ToString(), Encoding.UTF8, "application /json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region UpdateRoleTest
        /// <summary>
        /// UpdateRoleTest
        /// </summary>
        [TestMethod()]
        public void UpdateRoleTest()
        {
            try
            {
                UserRoleData userRoleData = new UserRoleData();
                if (!Convert.IsDBNull(TestContext.DataRow["userName"]) || !Convert.IsDBNull(TestContext.DataRow["roleID"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
                {
                    userRoleData.UserRolesID = Convert.ToInt32(TestContext.DataRow["ID"]);
                    userRoleData.UserName = TestContext.DataRow["userName"].ToString();
                    userRoleData.RoleId = Convert.ToInt32(TestContext.DataRow["gradeCode"]);
                    userRoleData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"].ToString());
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserRole/AssignRole/", new StringContent(JsonConvert.SerializeObject(userRoleData).ToString(), Encoding.UTF8, "application /json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region GetRolesTest
        /// <summary>
        /// GetRolesTest
        /// </summary>
        [TestMethod()]
        public void GetRolesTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetRoles"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region GetUserRoleDetailsTest
        /// <summary>
        /// GetUserRoleDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetUserRoleDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetUserRoleDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region getUserRoleOnLginTest
        /// <summary>
        /// getUserRoleOnLginTest
        /// </summary>
        [TestMethod()]
        public void getUserRoleOnLginTest()
        {
            try
            {
                string userName = "test";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetUserRoleOnLgin?userName=" + userName))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region MapAssociateIdTest
        /// <summary>
        /// MapAssociateIdTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PADetails.csv", "PADetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PADetails.csv", "TestInput"), TestMethod]
        public void MapAssociateIdTest()
        {
            try
            {
                AssociateDetails associateDetails = new AssociateDetails();
                if (!Convert.IsDBNull(TestContext.DataRow["empID"]) || !Convert.IsDBNull(TestContext.DataRow["userId"]))
                {
                    associateDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
                    associateDetails.UserId = Convert.ToInt32(TestContext.DataRow["userId"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserRole/MapAssociateId/", new StringContent(JsonConvert.SerializeObject(associateDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region GetNamesTest
        /// <summary>
        /// GetNamesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\UserRole.csv", "UserRole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\UserRole.csv"), TestMethod]

        public void GetNamesTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetNames"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region UpdateUserRoleTest
        /// <summary>
        /// UpdateUserRoleTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\UserRole.csv", "UserRole#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\UserRole.csv", "TestInput"), TestMethod]

        public void UpdateUserRoleTest()
        {
            using (HttpClient client = new HttpClient())
            {
                UserRoleData userRoleForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list 
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetUserRoleDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var userroleData = result.Result;
                        List<UserRoleData> lstLearningData = JsonConvert.DeserializeObject<List<UserRoleData>>(userroleData);
                        //Update one of the object state                
                        if (lstLearningData.Count > 0)
                        {
                            userRoleForUpdate = lstLearningData[0];
                        }
                    }

                }
                //2. Update
                userRoleForUpdate.IsActive =  Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                userRoleForUpdate.RoleId = Convert.ToInt32(TestContext.DataRow["Role"]);
                userRoleForUpdate.UserId = Convert.ToInt32(TestContext.DataRow["UserId"]);
                
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserRole/UpdateUserRole/", new StringContent(JsonConvert.SerializeObject(userRoleForUpdate), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserRole/GetUserRoleDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var roleData = result.Result;
                        List<UserRoleData> userData = JsonConvert.DeserializeObject<List<UserRoleData>>(roleData);
                        UserRoleData data = userData.Find(users => users.UserId == userRoleForUpdate.UserId);
                        Assert.AreEqual(userRoleForUpdate.UserId, data.UserId);
                    }
                }
            };
        }

        #endregion

        #region UpdateRoleTest
        /// <summary>
        /// UpdateRoleTest
        /// </summary>
        [TestMethod()]
        public void UpdateEmployeeStatusTest()
        {
            try
            {
                AssociateDetails associateData = new AssociateDetails();
                associateData.empID = Convert.ToInt32(TestContext.DataRow["empId"]);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserRole/UpdateEmployeeStatus/", new StringContent(JsonConvert.SerializeObject(associateData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}