﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.DomainEntities;
using System.Net.Http;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class ProspectiveAssociateControllerTests : BaseControllerTests
    {

        #region AddPADetailsTest
        /// <summary>
        /// AddPADetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PADetails.csv", "PADetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PADetails.csv", "TestInput"), TestMethod]
        public void AddPADetailsTest()
        {
            UserDetails userDetails = new UserDetails();
            userDetails.firstName = Convert.ToString(TestContext.DataRow["firstName"]);
            userDetails.middleName = Convert.ToString(TestContext.DataRow["middleName"]);
            userDetails.lastName = Convert.ToString(TestContext.DataRow["lastName"]);
            userDetails.gender = Convert.ToString(TestContext.DataRow["gender"]);
            userDetails.designationID = Convert.ToInt32(TestContext.DataRow["designationID"]);
            userDetails.technology = Convert.ToString(TestContext.DataRow["technology"]);
            userDetails.deptID = Convert.ToInt32(TestContext.DataRow["deptID"]);
            userDetails.hrAdvisor = Convert.ToString(TestContext.DataRow["hrAdvisor"]);
            userDetails.joiningStatusID = Convert.ToInt32(TestContext.DataRow["joiningStatusID"]);
            userDetails.dateOfJoining = Convert.ToDateTime(TestContext.DataRow["joiningDate"]);
            userDetails.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            userDetails.employmentType = Convert.ToString(TestContext.DataRow["employmentType"]);
            userDetails.maritalStatus = Convert.ToString(TestContext.DataRow["maritalStatus"]);
            userDetails.bgvStatusID = Convert.ToInt32(TestContext.DataRow["bgvStatusID"]);
            userDetails.technologyID = Convert.ToInt32(TestContext.DataRow["technologyID"]);
            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
            userDetails.RecruitedBy = Convert.ToString(TestContext.DataRow["RecruitedBy"]);
            userDetails.managerId = Convert.ToInt32(TestContext.DataRow["ManagerId"]);
            userDetails.personalEmail = Convert.ToString(TestContext.DataRow["PersonalEmailAddress"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProspectiveAssociate/AddPADetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    bool result = Convert.ToBoolean(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(result, true);
                }

            };
        }
        #endregion

        #region UpdatePADetailsTest
        /// <summary>
        /// UpdatePADetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PADetails.csv", "PADetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PADetails.csv", "TestInput"), TestMethod]
        public void UpdatePADetailsTest()
        {
            UserDetails userDetails = new UserDetails();
            userDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            userDetails.firstName = Convert.ToString(TestContext.DataRow["firstName"]);
            userDetails.middleName = Convert.ToString(TestContext.DataRow["middleName"]);
            userDetails.lastName = Convert.ToString(TestContext.DataRow["lastName"]);
            userDetails.gender = Convert.ToString(TestContext.DataRow["gender"]);
            userDetails.designationID = Convert.ToInt32(TestContext.DataRow["designationID"]);
            userDetails.gradeID = Convert.ToInt32(TestContext.DataRow["gradeID"]);
            userDetails.technology = Convert.ToString(TestContext.DataRow["technology"]);
            userDetails.deptID = Convert.ToInt32(TestContext.DataRow["deptID"]);
            userDetails.hrAdvisor = Convert.ToString(TestContext.DataRow["hrAdvisor"]);
            userDetails.joiningStatusID = Convert.ToInt32(TestContext.DataRow["joiningStatusID"]);
            userDetails.dateOfJoining = Convert.ToDateTime(TestContext.DataRow["joiningDate"]);
            userDetails.employmentType = Convert.ToString(TestContext.DataRow["employmentType"]);
            userDetails.maritalStatus = Convert.ToString(TestContext.DataRow["maritalStatus"]);
            userDetails.bgvStatusID = Convert.ToInt32(TestContext.DataRow["bgvStatusID"]);
            userDetails.dob = Convert.ToDateTime(TestContext.DataRow["dob"]);
            userDetails.RecruitedBy = Convert.ToString(TestContext.DataRow["RecruitedBy"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProspectiveAssociate/UpdatePADetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    bool result = Convert.ToBoolean(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(result, true);
                }

            };
        }
        #endregion

        #region GetPADetailsTest
        /// <summary>
        /// GetPADetailsTest
        /// </summary>
        [TestMethod()]
        public void GetPADetailsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/GetPADetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetPADetailsByIDTest
        /// <summary>
        /// GetPADetailsByIDTest
        /// </summary>
        [TestMethod()]
        public void GetPADetailsByIDTest()
        {
            int ID = 9;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/GetPADetailsByID?ID=" + ID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region PagingForPADetailsTest
        /// <summary>
        /// PagingForPADetailsTest
        /// </summary>
        [TestMethod()]
        public void PagingForPADetailsTest()
        {
            int pageNumber = 1;
            int pageSize = 5;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/PagingForPADetails?pageNumber=" + pageNumber + "&&pageSize=" + pageSize))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region DeletePADetailsByID
        /// <summary>
        /// DeletePADetailsByID
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PADetails.csv", "PADetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PADetails.csv", "TestInput"), TestMethod]
        public void DeletePADetailsByID()
        {
            try
            {
                int employeeId = 307;
                int ID = 102;
                string reason = Convert.ToString(TestContext.DataRow["Reason"]);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/DeletePADetailsByID?empID=" + employeeId + "&reason=" + reason))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/GetPADetailsByID?ID=" + ID))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetDesignationByGrade
        /// <summary>
        /// GetDesignationByGrade
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PADetails.csv", "PADetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PADetails.csv"), TestMethod]

        public void GetDesignationByGrade()
        {
            using (HttpClient client = new HttpClient())
            {
                string designationCode = string.Empty;
                int gradeId = 1;
                if (!string.IsNullOrEmpty(TestContext.DataRow["designationCode"].ToString()))
                {
                    designationCode = TestContext.DataRow["designationCode"].ToString();
                    //gradeId = Convert.ToInt32(TestContext.DataRow["gradeId"]);
                }

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/GetDesignationByGrade?gradeId=" + gradeId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var Data = result.Result;
                        //deserialize to your class
                        List<DesignationData> lstdesignationData = JsonConvert.DeserializeObject<List<DesignationData>>(Data);
                        DesignationData designation = lstdesignationData.Find(designationData => designationData.DesignationCode.ToLower().Trim() == designationCode.ToLower().Trim());
                        Assert.AreEqual(designationCode, designation.DesignationCode);
                    }
                }

            };
        }
        #endregion

        #region GetJoinedAssociatesTest
        /// <summary>
        /// GetJoinedAssociatesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\PADetails.csv", "PADetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\PADetails.csv"), TestMethod]

        public void GetJoinedAssociatesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int departmentID  = Convert.ToInt32(TestContext.DataRow["departmentID"]);
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/GetJoinedAssociates"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<AssociateJoiningData> lstEmployeeData = JsonConvert.DeserializeObject<List<AssociateJoiningData>>(employeeData);
                        AssociateJoiningData employeeDetails = lstEmployeeData.Find(employee => employee.DepartmentId == departmentID);
                        Assert.AreEqual(departmentID, employeeDetails.DepartmentId);
                    }
                }

            };
        }
        #endregion
    }
}
