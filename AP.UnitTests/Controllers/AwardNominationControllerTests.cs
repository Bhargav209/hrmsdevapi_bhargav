﻿using AP.DomainEntities;
using AP.Services.Controllers.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class AwardNominationControllerTests : BaseControllerTests
    {
        #region CreateAwardNominationTest
        /// <summary>
        /// CreateAwardNomination
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AwardNominationData.csv", "AwardNominationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AwardNominationData.csv", "TestInput"), TestMethod]
        public void CreateAwardNominationTest()
        {
            try
            {
                AwardNominationData awardNominationData = new AwardNominationData();
                awardNominationData.AwardId = Convert.ToInt32(TestContext.DataRow["AwardId"]);
                awardNominationData.AwardTypeId = Convert.ToInt32(TestContext.DataRow["AwardTypeId"]);
                awardNominationData.ADRCycleId = Convert.ToInt32(TestContext.DataRow["AdRCycleId"]);
                awardNominationData.FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
                awardNominationData.NominationDescription = Convert.ToString(TestContext.DataRow["NominationDescription"]);
                awardNominationData.FromEmployeeId = Convert.ToInt32(TestContext.DataRow["FromEmployeeId"]);
                awardNominationData.NomineeComments = Convert.ToString(TestContext.DataRow["NomineeComments"]);
                awardNominationData.ReviewerComments = Convert.ToString(TestContext.DataRow["ReviewerComments"]);
                awardNominationData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                awardNominationData.ProjectID = Convert.ToInt32(TestContext.DataRow["ProjectId"]);

                List<GenericType> associateNames = new List<GenericType>();
                associateNames.Add(new GenericType() { Id = 107 });
                awardNominationData.AssociateNames = associateNames;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AwardNomination/CreateAwardNomination", new StringContent(JsonConvert.SerializeObject(awardNominationData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetAwardNominationTest
        /// <summary>
        /// GetAwardNominationTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AwardNominationData.csv", "AwardNominationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AwardNominationData.csv", "TestInput"), TestMethod]
        public void GetAwardNominationTest()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    int financialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
                    int fromEmployeeId = Convert.ToInt32(TestContext.DataRow["FromEmployeeId"]);
                    string roleName = "Project Manager";

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AwardNomination/GetAwardNominationList?financialYearId=" + financialYearId + "&&fromEmployeeId=" + fromEmployeeId + "&&roleName=" + roleName))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var NominationData = result.Result;
                            //deserialize to your class
                            List<AwardNominationData> Naminations = JsonConvert.DeserializeObject<List<AwardNominationData>>(NominationData);
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }



                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
        #endregion

        #region UpdateAwardNominationTest
        /// <summary>
        /// UpdateAwardNominationTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AwardNominationData.csv", "AwardNominationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AwardNominationData.csv", "TestInput"), TestMethod]
        public void UpdateAwardNominationTest()
        {
            try
            {
                AwardNominationData awardNominationData = new AwardNominationData();
                awardNominationData.AwardId = Convert.ToInt32(TestContext.DataRow["AwardId"]);
                awardNominationData.AwardTypeId = Convert.ToInt32(TestContext.DataRow["AwardTypeId"]);
                awardNominationData.AwardNominationId = Convert.ToInt32(TestContext.DataRow["AwardNominationId"]);
                awardNominationData.NominationDescription = Convert.ToString(TestContext.DataRow["NominationDescription"]);
                awardNominationData.NomineeComments = Convert.ToString(TestContext.DataRow["NomineeComments"]);
                awardNominationData.ReviewerComments = Convert.ToString(TestContext.DataRow["ReviewerComments"]);
                awardNominationData.FromEmployeeId = Convert.ToInt32(TestContext.DataRow["FromEmployeeId"]);
                awardNominationData.RoleName = Convert.ToString(TestContext.DataRow["RoleName"]);
                List<GenericType> associateNames = new List<GenericType>();
                associateNames.Add(new GenericType() { Id = 107 });
                awardNominationData.AssociateNames = associateNames;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AwardNomination/UpdateAwardNomination", new StringContent(JsonConvert.SerializeObject(awardNominationData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
