﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Text;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class ProjectTrainingMasterControllerTests:BaseControllerTests
    {
        [TestMethod()]
        public void GetTrainingDataTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectTrainingMaster/GetTrainingData"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }
                }
            };
        }

        [TestMethod()]
        public void GetTrainingDataByIdTest()
        {
            int trainingId = 2;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectTrainingMaster/GetTrainingDataById?trainingId="+ trainingId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }
                }
            };
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProjectTrainings.csv", "ProjectTrainings#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProjectTrainings.csv", "TestInput"), TestMethod]
        public void CreateNewTrainingTest()
        {
            ProjectTrainingMasterData newTraining = new ProjectTrainingMasterData();
            newTraining.ProjectTrainingCode = TestContext.DataRow["TrainingCode"].ToString();
            newTraining.ProjectTrainingName = TestContext.DataRow["TrainingName"].ToString();
            newTraining.IsActive =Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            List<int?> projects = new List<int?>();
            if(TestContext.DataRow["ProjectId1"].ToString()!="")
                projects.Add(Convert.ToInt32(TestContext.DataRow["ProjectId1"]));
            if (TestContext.DataRow["ProjectId2"].ToString() != "")
                projects.Add(Convert.ToInt32(TestContext.DataRow["ProjectId2"]));
            if (TestContext.DataRow["ProjectId3"].ToString() != "")
                projects.Add(Convert.ToInt32(TestContext.DataRow["ProjectId3"]));          
            newTraining.Projects = projects;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProjectTrainingMaster/CreateNewTraining/", new StringContent(JsonConvert.SerializeObject(newTraining), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProjectTrainings.csv", "ProjectTrainings#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProjectTrainings.csv", "TestInput"), TestMethod]
        public void UpdateTrainingDetailsTest()
        {
            ProjectTrainingMasterData newTraining = new ProjectTrainingMasterData();
            newTraining.ProjectTrainingCode = TestContext.DataRow["TrainingCode"].ToString();
            newTraining.ProjectTrainingName = TestContext.DataRow["TrainingName"].ToString();
            newTraining.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            List<int?> projects = new List<int?>();
            if (TestContext.DataRow["ProjectId1"].ToString() != "")
                projects.Add(Convert.ToInt32(TestContext.DataRow["ProjectId1"]));
            if (TestContext.DataRow["ProjectId2"].ToString() != "")
                projects.Add(Convert.ToInt32(TestContext.DataRow["ProjectId2"]));
            if (TestContext.DataRow["ProjectId3"].ToString() != "")
                projects.Add(Convert.ToInt32(TestContext.DataRow["ProjectId3"]));
            newTraining.Projects = projects;

            newTraining.ProjectTrainingId = Convert.ToInt32(TestContext.DataRow["TrainingId"]);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProjectTrainingMaster/UpdateTrainingDetails/", new StringContent(JsonConvert.SerializeObject(newTraining), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }                
            };
        }
    }
}