﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.Net;
using System.Net.Http;
using AP.DomainEntities;
using Newtonsoft.Json;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class UserDepartmentControllerTests:BaseControllerTests
    {

        #region CreateDepartmentTest
        /// <summary>
        /// CreateDepartmentTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Departments.csv", "Departments#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Departments.csv", "TestInput"), TestMethod]
        public void CreateDepartmentTest()
        {
            try
            {

                DepartmentData departmentData = new DepartmentData();
                if (!Convert.IsDBNull(TestContext.DataRow["deptCode"]) || !Convert.IsDBNull(TestContext.DataRow["description"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
                {
                    departmentData.DepartmentCode = TestContext.DataRow["deptCode"].ToString();
                    departmentData.Description = TestContext.DataRow["description"].ToString();
                    departmentData.DepartmentHeadId =  Convert.ToInt32(TestContext.DataRow["DepartmentHeadId"]);
                    departmentData.DepartmentTypeId = Convert.ToInt32(TestContext.DataRow["DepartmentTypeId"]);
                }

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserDepartment/CreateDepartment", new StringContent(JsonConvert.SerializeObject(departmentData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetUserDepartmentDetailsTest
        /// <summary>
        /// GetUserDepartmentDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetUserDepartmentDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/UserDepartment/GetUserDepartmentDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region UpdateUserDepartmentDetailsTest
        /// <summary>
        /// UpdateUserDepartmentDetailsTest
        /// </summary>
      
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Departments.csv", "Departments#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Departments.csv"),TestMethod]
        public void UpdateUserDepartmentDetailsTest()
        {
            try
            {
                DepartmentData departmentData = new DepartmentData();
                if (!Convert.IsDBNull(TestContext.DataRow["deptCode"]) || !Convert.IsDBNull(TestContext.DataRow["description"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
                {
                    departmentData.DepartmentId =  Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                    departmentData.DepartmentCode =  TestContext.DataRow["deptCode"].ToString();
                    departmentData.Description =  TestContext.DataRow["description"].ToString();
                    departmentData.DepartmentHeadId = Convert.ToInt32(TestContext.DataRow["DepartmentHeadId"]);
                    departmentData.DepartmentTypeId =  Convert.ToInt32(TestContext.DataRow["DepartmentTypeId"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserDepartment/UpdateUserDepartmentDetails/",new StringContent(JsonConvert.SerializeObject(departmentData).ToString(),Encoding.UTF8,"application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {

                throw;
            }
        }
        #endregion
       
    }
}