﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.Net.Http;
using System.Net;
using System;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class AssociateResignationController : BaseControllerTests
    {
        #region SaveResignationTest
        /// <summary>
        /// SaveResignationTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv", "TestInput"), TestMethod]
        public void SaveResignationTest()
        {
            try
            {
                AssociateResignationData associateResigDetails = new AssociateResignationData();
                if (!Convert.IsDBNull(TestContext.DataRow["EmployeeId"]) || !Convert.IsDBNull(TestContext.DataRow["ReasonId"]))
                {
                    associateResigDetails.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                    associateResigDetails.ReasonId = Convert.ToInt32(TestContext.DataRow["ReasonId"]);
                    associateResigDetails.ReasonDescription = TestContext.DataRow["ReasonDescription"].ToString();
                    associateResigDetails.DOR = Convert.ToDateTime(TestContext.DataRow["DOR"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateResignation/CreateAssociateResignation", new StringContent(JsonConvert.SerializeObject(associateResigDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetResignationDataTest
        /// <summary>
        /// GetResignationDataTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv", "TestInput"), TestMethod]
        public void GetResignationDataTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string EmployeeId = string.Empty;
                if (!Convert.IsDBNull(TestContext.DataRow["EmployeeId"]))
                {
                    EmployeeId = "?employeeID=" + TestContext.DataRow["EmployeeId"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateResignation/GetResignationDetails" + EmployeeId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        AssociateResignationData associateResignationData = JsonConvert.DeserializeObject<AssociateResignationData>(associateData);
                        Assert.AreEqual("Test Reason", associateResignationData.ReasonDescription);

                    }
                }

            };
        }
        #endregion

        #region ApproveResignationTest
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv", "TestInput"), TestMethod]
        public void ApproveResignationTest()
        {
            try
            {
                ResignationApprovalData approveResigData = new ResignationApprovalData();
                approveResigData.ResignationID = Convert.ToInt32(TestContext.DataRow["ResignationID"]);
                approveResigData.ApprovedByID = Convert.ToInt32(TestContext.DataRow["ApprovedBy"]);
                approveResigData.ApprovedDate = Convert.ToDateTime(TestContext.DataRow["ApprovedDate"]);
                approveResigData.Remarks = Convert.ToString(TestContext.DataRow["Remarks"]);
                approveResigData.ResignationRecommendation = Convert.ToString(TestContext.DataRow["ResignationRecommendation"]);
                approveResigData.LastWorkingDate = Convert.ToDateTime(TestContext.DataRow["LastWorkingDate"]);
                //approveResigData.StatusID = Convert.ToInt32(TestContext.DataRow["StatusID"]);
                approveResigData.ToDeliveryHead = Convert.ToInt32(TestContext.DataRow["ToDeliveryHead"]);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateResignation/RejectResignation", new StringContent(JsonConvert.SerializeObject(approveResigData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ResignationDashboardTest
        /// <summary>
        /// GetResignationDataTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv", "TestInput"), TestMethod]
        public void ResignationDashboardTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int employeeID = 144;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateResignation/ResignationDashboard?employeeID=" + employeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        List<AssociateResignationData> associateResignationData = JsonConvert.DeserializeObject<List<AssociateResignationData>>(associateData);
                        Assert.AreEqual("HRMS", associateResignationData[0].ProjectName);
                    }
                }

            };
        }
        #endregion

        #region GetAssociateRejectedResignationsListTest
        /// <summary>
        /// GetAssociateRejectedResignationsListTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv"), TestMethod]

        public void GetAssociateRejectedResignationsListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                AssociateResignationData associateData = new AssociateResignationData();
                associateData.EmployeeName = Convert.ToString(TestContext.DataRow["Name"]);

                int ProgramManagerID = Convert.ToInt32(TestContext.DataRow["ProgramManagerID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateResignation/GetAssociateRejectedResignationsList?EmployeeID=" + ProgramManagerID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var resignationData = result.Result;
                        //deserialize to your class
                        List<AssociateResignationData> resignationDetails = JsonConvert.DeserializeObject<List<AssociateResignationData>>(resignationData);
                        AssociateResignationData resignations = resignationDetails.Find(resignation => resignation.EmployeeName.ToLower().Trim() == associateData.EmployeeName.ToLower().Trim());
                        Assert.AreEqual(associateData.EmployeeName, resignations.EmployeeName);
                    }
                }

            };
        }
        #endregion

        #region GetDeliveryHeadListTest
        /// <summary>
        /// GetDeliveryHeadListTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv"), TestMethod]

        public void GetDeliveryHeadListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                AssociateResignationData associateData = new AssociateResignationData();
                associateData.EmployeeName = Convert.ToString(TestContext.DataRow["EmployeeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateResignation/GetDeliveryHeadsList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var resignationData = result.Result;
                        //deserialize to your class
                        List<AssociateResignationData> resignationDetails = JsonConvert.DeserializeObject<List<AssociateResignationData>>(resignationData);
                        AssociateResignationData resignations = resignationDetails.Find(resignation => resignation.EmployeeName.ToLower().Trim() == associateData.EmployeeName.ToLower().Trim());
                        Assert.AreEqual(associateData.FirstName, resignations.FirstName);
                    }
                }

            };
        }
        #endregion

        #region GetAssociateApprovedResignationsListTest
        /// <summary>
        /// GetAssociateApprovedResignationsListTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv"), TestMethod]

        public void GetAssociateApprovedResignationsListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                AssociateResignationData associateData = new AssociateResignationData();
                associateData.EmployeeName = Convert.ToString(TestContext.DataRow["Employee"]);

                int ProgramManagerID = Convert.ToInt32(TestContext.DataRow["ProgramManagerID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateResignation/GetAssociateApprovedResignationsList?EmployeeID=" + ProgramManagerID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var resignationData = result.Result;
                        //deserialize to your class
                        List<AssociateResignationData> resignationDetails = JsonConvert.DeserializeObject<List<AssociateResignationData>>(resignationData);
                        AssociateResignationData resignations = resignationDetails.Find(resignation => resignation.EmployeeName.ToLower().Trim() == associateData.EmployeeName.ToLower().Trim());
                        Assert.AreEqual(associateData.EmployeeName, resignations.EmployeeName);
                    }
                }

            };
        }
        #endregion

        #region GetAssociatePendingResignationsListTest
        /// <summary>
        /// GetAssociatePendingResignationsListTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateResignationData.csv", "AssociateResignationData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateResignationData.csv"), TestMethod]

        public void GetAssociatePendingResignationsListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                AssociateResignationData associateData = new AssociateResignationData();
                associateData.EmployeeName = Convert.ToString(TestContext.DataRow["Employee"]);

                int ProgramManagerID = Convert.ToInt32(TestContext.DataRow["ProgramManagerID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateResignation/GetAssociatePendingResignationsList?EmployeeID=" + ProgramManagerID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var resignationData = result.Result;
                        //deserialize to your class
                        List<AssociateResignationData> resignationDetails = JsonConvert.DeserializeObject<List<AssociateResignationData>>(resignationData);
                        AssociateResignationData resignations = resignationDetails.Find(resignation => resignation.EmployeeName.ToLower().Trim() == associateData.EmployeeName.ToLower().Trim());
                        Assert.AreEqual(associateData.EmployeeName, resignations.EmployeeName);
                    }
                }

            };
        }
        #endregion

    }
}
