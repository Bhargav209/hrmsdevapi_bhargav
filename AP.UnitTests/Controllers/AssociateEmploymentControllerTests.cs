﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using AP.DomainEntities;
using AP.DataStorage;
using System.Threading.Tasks;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class AssociateEmploymentControllerTests:BaseControllerTests
    {
     
        #region GetEmploymentDetailsByIDTest
        /// <summary>
        /// GetEmploymentDetailsByIDTest
        /// </summary>
        [TestMethod()]
        public void GetEmploymentDetailsByIDTest()
        {
            int empID = 9;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateEmployment/GetEmploymentDetailsByID?empID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }
        #endregion

        #region UpdateEmployementDetailsTest
        /// <summary>
        /// UpdateEmployementDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeEmploymentAndProfessionReferenceDetails.csv", "EmployeeEmploymentAndProfessionReferenceDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeEmploymentAndProfessionReferenceDetails.csv", "TestInput"), TestMethod]
        public void UpdateEmployementDetailsTest()
        {
            UserDetails userDetails = new UserDetails();

            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
           
            List<EmploymentDetails> employmentDetailsList = new List<EmploymentDetails>();
            EmploymentDetails employmentDetails = new EmploymentDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["name"]) || !Convert.IsDBNull(TestContext.DataRow["address"]) || !Convert.IsDBNull(TestContext.DataRow["designation"])
               || !Convert.IsDBNull(TestContext.DataRow["fromYear"]) || !Convert.IsDBNull(TestContext.DataRow["lastDrawnSalary"]) || !Convert.IsDBNull(TestContext.DataRow["fromYear"]) || !Convert.IsDBNull(TestContext.DataRow["toYear"])
                    || !Convert.IsDBNull(TestContext.DataRow["leavingReson"]))
            {
                employmentDetails.name = Convert.ToString(TestContext.DataRow["name"]);
                employmentDetails.address = Convert.ToString(TestContext.DataRow["address"]);
                employmentDetails.designation = Convert.ToString(TestContext.DataRow["designation"]);
                employmentDetails.fromYear = Convert.ToString(TestContext.DataRow["fromYear"]);
                employmentDetails.toYear = Convert.ToString(TestContext.DataRow["toYear"]);
                employmentDetails.leavingReson = Convert.ToString(TestContext.DataRow["leavingReson"]);
                employmentDetailsList.Add(employmentDetails);
            }

            List<ProfRefDetails> profRefDetailsList = new List<ProfRefDetails>();
            ProfRefDetails profRefDetails = new ProfRefDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["ProfessionalReferencename"]) || !Convert.IsDBNull(TestContext.DataRow["ProfessionalReferencedesignation"]) ||
                !Convert.IsDBNull(TestContext.DataRow["companyName"]) || !Convert.IsDBNull(TestContext.DataRow["companyAddress"])
                || !Convert.IsDBNull(TestContext.DataRow["officeEmailAddress"]) || !Convert.IsDBNull(TestContext.DataRow["officeEmailAddress"]))

            {
                profRefDetails.name = Convert.ToString(TestContext.DataRow["ProfessionalReferencename"]);
                profRefDetails.designation = Convert.ToString(TestContext.DataRow["ProfessionalReferencedesignation"]);
                profRefDetails.companyName = Convert.ToString(TestContext.DataRow["companyName"]);
                profRefDetails.companyAddress = Convert.ToString(TestContext.DataRow["companyAddress"]);
                profRefDetails.officeEmailAddress = Convert.ToString(TestContext.DataRow["officeEmailAddress"]);
                profRefDetails.mobileNo = Convert.ToString(TestContext.DataRow["officeEmailAddress"]);
                profRefDetailsList.Add(profRefDetails);
            }

            userDetails.prevEmployerdetails = employmentDetailsList;
            userDetails.profReference = profRefDetailsList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateEmployment/SaveEmployementDetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            };
        }
        #endregion

        #region UpdateEmployementDetailsTest
        /// <summary>
        /// UpdateEmployementDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeEmploymentAndProfessionReferenceDetails.csv", "EmployeeEmploymentAndProfessionReferenceDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeEmploymentAndProfessionReferenceDetails.csv", "TestInput"), TestMethod]
        public void UpdateDesignationToAssociateTest()
        {
            AssociateDesignation designationDetails = new AssociateDesignation();

            designationDetails.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
            designationDetails.DesignationId = Convert.ToInt32(TestContext.DataRow["DesignationId"]);
            designationDetails.GradeId = Convert.ToInt32(TestContext.DataRow["GradeId"]);
            designationDetails.ToDate = Convert.ToDateTime(TestContext.DataRow["ToDate"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateEmployment/UpdateDesignationToAssociate", new StringContent(JsonConvert.SerializeObject(designationDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion

        #region GetDesignationDetailsByIDTest
        /// <summary>
        /// GetDesignationDetailsByIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeEmploymentAndProfessionReferenceDetails.csv", "EmployeeEmploymentAndProfessionReferenceDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeEmploymentAndProfessionReferenceDetails.csv"), TestMethod]

        public void GetDesignationDetailsByIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeId;
                EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateEmployment/GetDesignationDetailsByID?employeeID=" + EmployeeId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var designationData = result.Result;
                        //deserialize to your class
                        List<AssociateDesignationData> associateData = JsonConvert.DeserializeObject<List<AssociateDesignationData>>(designationData);
                        AssociateDesignationData designationDetails = associateData.Find(data => data.EmployeeID == EmployeeId);
                        Assert.AreEqual(EmployeeId, designationDetails.EmployeeID);
                    }
                }

            };
        }
        #endregion
    }
}