﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.DomainEntities;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using AP.DataStorage;
using AP.API;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class ProjectControllerTest : BaseControllerTests
    {


        #region PutProjectTest

        /// <summary>
        /// PutProjectTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv"), TestMethod]
        public void PutProjectTest()
        {
            ProjectData projectData = new ProjectData();
            projectData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            projectData.ProjectCode = Convert.ToString(TestContext.DataRow["ProjectCode"]);
            projectData.ProjectName = Convert.ToString(TestContext.DataRow["ProjectName"]);
            projectData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);

            projectData.ActualStartDate = Convert.ToDateTime(TestContext.DataRow["ActualStartDate"]);
            projectData.ActualEndDate = Convert.ToDateTime(TestContext.DataRow["ActualEndDate"]);
            projectData.PlannedEndDate = Convert.ToDateTime(TestContext.DataRow["PlannedEndDate"]);
            projectData.PlannedStartDate = Convert.ToDateTime(TestContext.DataRow["PlannedStartDate"]);
            projectData.CustomerId = Convert.ToInt32(TestContext.DataRow["CustomerId"]);
            projectData.ProjectTypeId = Convert.ToInt32(TestContext.DataRow["ProjectTypeId"]);

            ProjectManager projectManager = new ProjectManager();
            projectManager.ProgramManagerID = Convert.ToInt32(TestContext.DataRow["ProgramManagerID"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/PutProject", new StringContent(JsonConvert.SerializeObject(projectData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion

        #region PostProjectTest

        /// <summary>
        /// PostProjectTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv"), TestMethod]
        public void PostProjectTest()
        {
            ProjectData projectData = new ProjectData();
            projectData.ProjectCode = Convert.ToString(TestContext.DataRow["ProjectCode"]);
            projectData.ProjectName = Convert.ToString(TestContext.DataRow["ProjectName"]);
            projectData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            projectData.ActualStartDate = Convert.ToDateTime(TestContext.DataRow["ActualStartDate"]);
            projectData.ActualEndDate = Convert.ToDateTime(TestContext.DataRow["ActualEndDate"]);
            projectData.PlannedEndDate = Convert.ToDateTime(TestContext.DataRow["PlannedEndDate"]);
            projectData.PlannedStartDate = Convert.ToDateTime(TestContext.DataRow["PlannedStartDate"]);
            projectData.CustomerId = Convert.ToInt32(TestContext.DataRow["CustomerId"]);
            projectData.ProjectTypeId = Convert.ToInt32(TestContext.DataRow["ProjectTypeId"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/PostProject", new StringContent(JsonConvert.SerializeObject(projectData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion

        #region GetProjectTest
        /// <summary>
        /// GetProjectTest
        /// </summary>
        [TestMethod()]
        public void GetProjectTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string content = "?id=168";
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetProject" + content))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetAssociatesTest
        /// <summary>
        /// GetAssociatesTest
        /// </summary>
        [TestMethod()]
        public void GetAssociatesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetAssociates"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetProjectReleaseTest
        /// <summary>
        /// GetProjectReleaseTest
        /// </summary>
        [TestMethod()]
        public void GetProjectReleaseTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string content = "?employeeId=23";
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetProjectRelease" + content))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetPercentUtilizationTest
        /// <summary>
        /// GetPercentUtilizationTest
        /// </summary>
        [TestMethod()]
        public void GetPercentUtilizationTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string content = "?projectId=23&&employeeId=19";
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetPercentUtilization" + content))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region ReleaseAssociateTest
        /// <summary>
        /// ReleaseAssociateTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Allocation.csv", "Allocation#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Allocation.csv", "TestInput"), TestMethod]
        public void ReleaseAssociateTest()
        {
            AssociateAllocationDetails associateDetails = new AssociateAllocationDetails();
            associateDetails.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            associateDetails.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
            associateDetails.RAId = Convert.ToInt32(TestContext.DataRow["RAId"]);
            associateDetails.ReportingManagerId = Convert.ToInt32(TestContext.DataRow["ReportingManagerId"]);
            associateDetails.ReleaseProjectId = Convert.ToInt32(TestContext.DataRow["ReleaseProjectId"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/ReleaseAssociate", new StringContent(JsonConvert.SerializeObject(associateDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region UpdateReportingManagerTest
        /// <summary>
        /// UpdateReportingManagerTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv", "TestInput"), TestMethod]
        public void UpdateReportingManagerTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int reportingManagerId = Convert.ToInt32(TestContext.DataRow["reportingManagerId"]);               
                int projectId = Convert.ToInt32(TestContext.DataRow["projId"]);
                ProjectData projectDataForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetProjectsList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var data = result.Result;
                        List<ProjectData> lstProjectData = JsonConvert.DeserializeObject<List<ProjectData>>(data);
                        //Update one of the object state                
                        if (lstProjectData.Count > 0)
                        {
                            projectDataForUpdate = lstProjectData[8];
                        }
                    }

                }        
                //2. Update
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/UpdateReportingManagerToProject?reportingManagerId=" + reportingManagerId + "&projectId=" + projectId, new StringContent(JsonConvert.SerializeObject(projectDataForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetProjectsList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var projectData = result.Result;
                        List<ProjectData> lstProjectData = JsonConvert.DeserializeObject<List<ProjectData>>(projectData);
                        ProjectData projects = lstProjectData.Find(project => project.ProjectId == projectDataForUpdate.ProjectId);
                        Assert.AreEqual(projectDataForUpdate.ProjectName, projects.ProjectName);
                    }
                }
            };
        }
        #endregion
        
        #region UpdateReportingManagerToAssociateTest
        /// <summary>
        /// UpdateReportingManagerToAssociateTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv", "TestInput"), TestMethod]
        public void UpdateReportingManagerToAssociateTest()
        {
            try
            {

                ProjectData projectData = new ProjectData();
                projectData.EmployeeId = Convert.ToInt32(TestContext.DataRow["Empid"]);
                projectData.ProjectId = Convert.ToInt32(TestContext.DataRow["projId"]);
                projectData.ReportingManagerId = Convert.ToInt32(TestContext.DataRow["reportingManagerId"]);
                projectData.ManagerId = Convert.ToInt32(TestContext.DataRow["programManagerId"]);
                projectData.LeadId = Convert.ToInt32(TestContext.DataRow["leadId"]);
                ProjectDetails projectDetails = new ProjectDetails();
                projectDetails.UpdateReportingManagerToAssociate(projectData, false);
                //using (HttpClient client = new HttpClient())
                //{
                //    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/UpdateReportingManagerToAssociate", new StringContent(JsonConvert.SerializeObject(projectData), Encoding.UTF8, "application/json")).Result)
                //    {
                //        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                //    }
                //};
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SaveManagersToProjectTest

        /// <summary>
        /// SaveManagersToProjectTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv"), TestMethod]
        public void SaveManagersToProjectTest()
        {
            ProjectData projectData = new ProjectData();
            projectData.ProjectId = 54;//Convert.ToInt32(TestContext.DataRow["projId"]);
            projectData.ReportingManagerId = 212;//Convert.ToInt32(TestContext.DataRow["reportingManagerId"]);
            projectData.ManagerId = 214;//Convert.ToInt32(TestContext.DataRow["programManagerId"]);
            projectData.LeadId = 186;// Convert.ToInt32(TestContext.DataRow["leadId"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/SaveManagersToProject", new StringContent(JsonConvert.SerializeObject(projectData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion

        #region GetEmpListTest
        /// <summary>
        /// GetEmpListTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv"), TestMethod]

        public void GetEmpListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string employeeCode = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["employeeCode"].ToString()))
                {
                    employeeCode = TestContext.DataRow["employeeCode"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetEmpList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<AssociateDetails> employees = JsonConvert.DeserializeObject<List<AssociateDetails>>(employeeData);
                        AssociateDetails employeeDetails = employees.Find(code => code.empCode.ToLower().Trim() == employeeCode.ToLower().Trim());
                        Assert.AreEqual(employeeCode, employeeDetails.empCode);
                    }
                }

            };
        }
        #endregion

        #region GetManagerList
        /// <summary>
        /// GetManagerList
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv"), TestMethod]

        public void GetManagerListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string ManagerName = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["ManagerName"].ToString()))
                {
                    ManagerName = TestContext.DataRow["ManagerName"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetManagerList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var ManagerData = result.Result;
                        //deserialize to your class
                        List<AssociateDetails> managersList = JsonConvert.DeserializeObject<List<AssociateDetails>>(ManagerData);
                        AssociateDetails managerDetails = managersList.Find(name => name.ManagerName.ToLower().Trim() == ManagerName.ToLower().Trim());
                        Assert.AreEqual(ManagerName, managerDetails.ManagerName);
                    }
                }

            };
        }
        #endregion

        #region GetEmployeesTest
        /// <summary>
        /// GetEmployeesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Project.csv", "Project#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Project.csv"), TestMethod]

        public void GetEmployeesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string SearchString = Convert.ToString(TestContext.DataRow["SearchString"]);
                string EmployeeName = Convert.ToString(TestContext.DataRow["EmployeeName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetEmployees?searchString=" + SearchString))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var associateData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstAssociateData = JsonConvert.DeserializeObject<List<GenericType>>(associateData);
                        GenericType associates = lstAssociateData.Find(employee => employee.Name.ToLower().Trim() == EmployeeName.ToLower().Trim());
                        Assert.AreEqual(EmployeeName, associates.Name);
                    }
                }

            };
        }
        #endregion

        #region GetReportingDetailsByProjectIdTest
        /// <summary>
        /// GetReportingDetailsByProjectIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRA.csv", "KRA#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRA.csv"), TestMethod]

        public void GetReportingDetailsByProjectIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int projectId = Convert.ToInt32(TestContext.DataRow["projectId"]);
                string ProgramManagerName = Convert.ToString(TestContext.DataRow["ProgramManagerName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetReportingDetailsByProjectId?projectId=" + projectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var ProjectManagerData = result.Result;
                        //deserialize to your class
                        List<ManagersData> lstManagerData = JsonConvert.DeserializeObject<List<ManagersData>>(ProjectManagerData);
                        ManagersData managers = lstManagerData.Find(manager => manager.ProgramManagerName.ToLower() == ProgramManagerName.ToLower());
                        Assert.AreEqual(ProgramManagerName, managers.ProgramManagerName);
                    }
                }

            };
        }
        #endregion
    }
}
