﻿using System;
using System.Configuration;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.DomainEntities;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class CompetencyAreaControllerTests:BaseControllerTests
    {
        #region CreateCompetencyAreaTest
        /// <summary>
        /// Test method for adding competency area
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CompetencyAreaInfo.csv", "CompetencyAreaInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CompetencyAreaInfo.csv", "TestInput"), TestMethod]
        public void CreateCompetencyAreaTest()
        {
            CompetencyAreaData competencyAreaData = new CompetencyAreaData();
            if (!Convert.IsDBNull(TestContext.DataRow["CompetencyAreaCode"]) || !Convert.IsDBNull(TestContext.DataRow["CompetencyAreaDescription"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                competencyAreaData.CompetencyAreaCode = Convert.ToString(TestContext.DataRow["CompetencyAreaCode"]);
                competencyAreaData.CompetencyAreaDescription = Convert.ToString(TestContext.DataRow["CompetencyAreaDescription"]);
                competencyAreaData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            }
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/CompetencyArea/CreateCompetencyArea", new StringContent(JsonConvert.SerializeObject(competencyAreaData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region UpdateCompetencyAreaTest
        /// <summary>
        /// Test method for updating competency area
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CompetencyAreaInfo.csv", "CompetencyAreaInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CompetencyAreaInfo.csv", "TestInput"), TestMethod]
        public void UpdateCompetencyAreaTest()
        {
            CompetencyAreaData competencyAreaData = new CompetencyAreaData();

            if (!Convert.IsDBNull(TestContext.DataRow["Id"]) || !Convert.IsDBNull(TestContext.DataRow["CompetencyAreaCode"]) || !Convert.IsDBNull(TestContext.DataRow["CompetencyAreaDescription"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                competencyAreaData.CompetencyAreaId = Convert.ToInt32(TestContext.DataRow["Id"]);
                competencyAreaData.CompetencyAreaCode = Convert.ToString(TestContext.DataRow["CompetencyAreaCode"]);
                competencyAreaData.CompetencyAreaDescription = Convert.ToString(TestContext.DataRow["CompetencyAreaDescription"]);
                competencyAreaData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            }

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/CompetencyArea/UpdateCompetencyArea", new StringContent(JsonConvert.SerializeObject(competencyAreaData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region GetCompetencyAreasTest
        /// <summary>
        /// Test method for retrieving competency areas
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CompetencyAreaInfo.csv", "CompetencyAreaInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CompetencyAreaInfo.csv", "TestInput"), TestMethod]
        public void GetCompetencyAreasTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get,serviceURL + "/CompetencyArea/GetCompetencyAreaDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        } 
        #endregion
    }
}
