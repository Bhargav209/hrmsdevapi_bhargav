﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using AP.DomainEntities;
using AP.Services.Controllers.Tests;
using Newtonsoft.Json;
using System.Text;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class CompetencySkillsControllerTests : BaseControllerTests
    {       
        #region GetCompetencyAreaTest
        /// <summary>
        /// Test method to get all CompetencyArea details
        /// </summary>
        [TestMethod()]
        public void GetCompetencyAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CompetencySkills/GetCompetencyArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }

                    //using (HttpClient httpClient = new HttpClient())
                    //{
                    //    using (HttpResponseMessage response = httpClient.PostAsync(serviceURL + "/CompetencyArea/UpdateCompetencyArea", new StringContent(JsonConvert.SerializeObject(new CompetencySkillDetails()), Encoding.UTF8, "application/json")).Result)
                    //    {
                    //        var var = response.Content.ReadAsStringAsync().Result;
                    //        Assert.IsNotNull(var);
                    //    } 
                    //}
                }

            };
        }
        #endregion

        #region GetCompetencySkillsMappedDataTest
        /// <summary>
        /// Test method to get mapped competency skills details
        /// </summary>
        [TestMethod()]
        public void GetCompetencySkillsMappedDataTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CompetencySkills/GetCompetencySkillsMappedData"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region AddUpdateCompetencySkillsTest
        /// <summary>
        /// Test method to map and update roles,competency area with skills
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CompetencySkills.csv", "CompetencySkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CompetencySkills.csv", "TestInput"), TestMethod]
        public void AddUpdateCompetencySkillsTest()
        {
            CompetencySkillDetails cSkillData = new CompetencySkillDetails();

            cSkillData.RoleID = Convert.ToInt32(TestContext.DataRow["RoleID"]);
            cSkillData.CompetencyID = Convert.ToInt32(TestContext.DataRow["CompetencyID"]);
            List<int?> skillList = new List<int?>();
            skillList.Add(Convert.ToInt32(TestContext.DataRow["Skill0"]));
            skillList.Add(Convert.ToInt32(TestContext.DataRow["Skill1"]));
            skillList.Add(Convert.ToInt32(TestContext.DataRow["Skill2"]));
            skillList.Add(Convert.ToInt32(TestContext.DataRow["Skill3"]));
            cSkillData.Skills = skillList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/CompetencySkills/AddUpdateCompetencySkills?" + cSkillData);
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion
    }
}