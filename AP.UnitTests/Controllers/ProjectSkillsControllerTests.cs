﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.DomainEntities;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Collections.Generic;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class ProjectSkillsControllerTests:BaseControllerTests
    {

        #region GetProjectSkillsTest
        /// <summary>
        /// GetProjectSkillsTest
        /// </summary>
        [TestMethod()]
        public void GetProjectSkillsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectSkills/GetProjectSkills"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion
        #region PostProjectSkillsTest

        /// <summary>
        /// PostProjectSkillsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProjectSkills.csv", "ProjectSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProjectSkills.csv"), TestMethod]

        public void PostProjectSkillsTest()
        {        
            ProjectSkillsData projectSkillsData = new ProjectSkillsData();
            projectSkillsData.ProjectSkillId = Convert.ToInt32(TestContext.DataRow["ProjectSkillId"]);
            projectSkillsData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            //List<int> SkillIds = new List<int>();
            //if(TestContext.DataRow["SkillId1"]!=null)
            //SkillIds.Add(Convert.ToInt32(TestContext.DataRow["SkillId1"]));            
            //if (TestContext.DataRow["SkillId3"] != null)
            //    SkillIds.Add(Convert.ToInt32(TestContext.DataRow["SkillId3"]));
            //projectSkillsData.SkillId = SkillIds;
            projectSkillsData.CompetencyAreaId = Convert.ToInt32(TestContext.DataRow["CompetencyAreaId"]);
            projectSkillsData.SkillId = Convert.ToInt32(TestContext.DataRow["SkillId"]);
            projectSkillsData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);           
            projectSkillsData.ProficiencyId = Convert.ToInt32(TestContext.DataRow["ProficiencyId"]);
           
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProjectSkills/PostProjectSkills", new StringContent(JsonConvert.SerializeObject(projectSkillsData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion       
    }
}