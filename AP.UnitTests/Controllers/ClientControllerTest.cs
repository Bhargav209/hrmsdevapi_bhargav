﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using AP.DomainEntities;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class ClientControllerTest : BaseControllerTests
    {
        #region CreateClientTest
        /// <summary>
        /// CreateClientTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ClientData.csv", "ClientData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ClientData.csv", "TestInput"), TestMethod]
        public void CreateClientTest()
        {
            try
            {

                ClientDetails clientsData = new ClientDetails();
                string clientCode = TestContext.DataRow["clientCode"].ToString();
                if (!string.IsNullOrEmpty(clientCode))
                {
                    clientsData.ClientCode = clientCode.ToString();
                    clientsData.ClientName = Convert.ToString(TestContext.DataRow["clientName"]);
                    clientsData.ClientRegisterName = Convert.ToString(TestContext.DataRow["clientRegisterName"]);
                    clientsData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Client/CreateClient", new StringContent(JsonConvert.SerializeObject(clientsData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Client/GetClientsDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var clientData = result.Result;
                            //deserialize to your class
                            List<ClientDetails> lstClientData = JsonConvert.DeserializeObject<List<ClientDetails>>(clientData);
                            ClientDetails clientDetails = lstClientData.Find(clients => clients.ClientCode.ToLower().Trim() == clientsData.ClientCode.ToLower().Trim());
                            Assert.AreEqual(clientsData.ClientCode, clientDetails.ClientCode);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateClientTest
        /// <summary>
        /// UpdateClientTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ClientData.csv", "ClientData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ClientData.csv", "TestInput"), TestMethod]
        public void UpdateClientTest()
        {
            using (HttpClient client = new HttpClient())
            {
                ClientDetails clientsForUpdate = null;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //1. Get list of behaviors
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Client/GetClientsDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var clientData = result.Result;
                        List<ClientDetails> lstclientData = JsonConvert.DeserializeObject<List<ClientDetails>>(clientData);
                        //Update one of the object state                
                        if (lstclientData.Count > 0)
                        {
                            clientsForUpdate = lstclientData[0];
                        }
                        clientsForUpdate.ClientCode += " Update Test";
                    }

                }
                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Client/UpdateClient", new StringContent(JsonConvert.SerializeObject(clientsForUpdate).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Client/GetClientsDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var clientDetails = result.Result;
                        List<ClientDetails> lstClientData = JsonConvert.DeserializeObject<List<ClientDetails>>(clientDetails);
                        ClientDetails clients = lstClientData.Find(code => code.ClientCode.ToLower().Trim() == clientsForUpdate.ClientCode.ToLower().Trim());
                        Assert.AreEqual(clientsForUpdate.ClientCode.ToLower(), clients.ClientCode.ToLower());
                    }
                }
            };
        }
        #endregion

        #region GetClientsDetailsTest
        /// <summary>
        /// GetClientsDetailsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ClientData.csv", "ClientData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ClientData.csv"), TestMethod]

        public void GetClientsDetailsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string clientCode = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["clientCode"].ToString()))
                {
                    clientCode = TestContext.DataRow["clientCode"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Client/GetClientsDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var clientData = result.Result;
                        //deserialize to your class
                        List<ClientDetails> lstclientData = JsonConvert.DeserializeObject<List<ClientDetails>>(clientData);
                        ClientDetails clientDetails = lstclientData.Find(clients => clients.ClientCode.ToLower().Trim() == clientCode.ToLower().Trim());
                        Assert.AreEqual(clientCode, clientDetails.ClientCode);
                    }
                }

            };
        }
        #endregion

        #region GetClientsByIdTest
        /// <summary>
        /// GetClientsByIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ClientData.csv", "ClientData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ClientData.csv"), TestMethod]

        public void GetClientsByIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int clientID = Convert.ToInt32(TestContext.DataRow["clientID"].ToString());
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Client/GetClientsById?clientId=" + clientID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var clientData = result.Result;
                        //deserialize to your class
                        List<ClientDetails> lstclientData = JsonConvert.DeserializeObject<List<ClientDetails>>(clientData);
                        ClientDetails clientDetails = lstclientData.Find(clients => clients.ClientId == clientID);
                        Assert.AreEqual(clientID, clientDetails.ClientId);
                    }
                }

            };
        }
        #endregion
    }
}
