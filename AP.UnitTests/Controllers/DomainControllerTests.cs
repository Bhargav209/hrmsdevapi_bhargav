﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;


namespace AP.UnitTests.Controllers
{
    [TestClass()]
    public class DomainControllerTests: BaseControllerTests
    {
        Random rnd = new Random(1);
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\DomainInfo.csv", "DomainInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\DomainInfo.csv"), TestMethod]
        public void GetDomainsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string domain = Convert.ToString(TestContext.DataRow["DomainName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Domain/GetDomains"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var DomainData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstDomainData = JsonConvert.DeserializeObject<List<GenericType>>(DomainData);
                        GenericType Domian = lstDomainData.Find(d => d.Name.ToLower().Trim() == domain.ToLower().Trim());
                        Assert.AreEqual(domain, Domian.Name);
                    }
                }

            };
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\DomainInfo.csv", "DomainInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\DomainInfo.csv"), TestMethod]
        public void CreateDomainTest()
        {
            try
            {

                DomainMasterData domaindata = new DomainMasterData();
                domaindata.DomainName = TestContext.DataRow["DomainName"].ToString();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Domain/CreateDomain", new StringContent(JsonConvert.SerializeObject(domaindata).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Domain/GetDomains"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var DomainData = result.Result;
                            //deserialize to your class
                            List<GenericType> lstDomainData = JsonConvert.DeserializeObject<List<GenericType>>(DomainData);
                            GenericType Domain = lstDomainData.Find(d => d.Name.ToLower().Trim() == domaindata.DomainName.ToLower().Trim());
                            Assert.AreEqual(domaindata.DomainName, Domain.Name);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\DomainInfo.csv", "DomainInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\DomainInfo.csv"), TestMethod]
        public void UpdateDomainTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                DomainMasterData domaindata = new DomainMasterData();
                domaindata.DomainID = 27;
                domaindata.DomainName = TestContext.DataRow["DomainName"].ToString();

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Domain/UpdateDomain", new StringContent(JsonConvert.SerializeObject(domaindata).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Domain/GetDomains"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var DomainData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstDomainData = JsonConvert.DeserializeObject<List<GenericType>>(DomainData);
                        GenericType Domain = lstDomainData.Find(d => d.Name.ToLower().Trim() == domaindata.DomainName.ToLower().Trim());
                        Assert.AreEqual(domaindata.DomainName, Domain.Name);
                    }
                }
            };
        }
    }
}