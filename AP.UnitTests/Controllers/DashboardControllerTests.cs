﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Configuration;
using System.Net;
using System.Data;
using AP.UnitTests;
using AP.DomainEntities;
using Newtonsoft.Json;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class DashboardControllerTests:BaseControllerTests
    {
    
        #region sendmail
        /// <summary>
        /// For sending mail
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\SendEmail.csv", "SendEmail#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\SendEmail.csv", "TestInput"), TestMethod]
        public void SendEmailTest()
        {
           
            Email email = new Email();
            email.FromEmail = Convert.ToString(TestContext.DataRow["FromEmail"]);
            email.ToEmail = Convert.ToString(TestContext.DataRow["ToEmail"]);
            email.CcEmail = Convert.ToString(TestContext.DataRow["CcEmail"]);
            email.Subject = Convert.ToString(TestContext.DataRow["Subject"]);
            email.EmailBody = Convert.ToString(TestContext.DataRow["EmailBody"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Dashboard/SendEmail", new StringContent(JsonConvert.SerializeObject(email).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
                }

            };
        }
        #endregion

        #region HRHeadDetails notification
        /// <summary>
        /// This is for getting HR head details which will be showing as a notification in the dashboard for HR Head.
        /// </summary>
        [TestMethod()]
        public void GetHRHeadDetailsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Dashboard/GetHRHeadDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region HR Advisor Details notification
        /// <summary>
        /// This is for getting HR advisor details which will be showing as a notification in the dashboard for HR Advisor.
        /// </summary>
        [TestMethod()]
        public void GetHRADetailsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProspectiveAssociate/GetHRAdvisors"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region Utilization Report for delivery head and HR Head
        /// <summary>
        /// This method checks the utilization of associate wrt resource type,gender and project.
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\GetUtilizationReport.csv", "GetUtilizationReport#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\GetUtilizationReport.csv", "TestInput"), TestMethod]
        public void GetUtilizeReportsTest()
        {
         
            SearchFilter searchFilter = new SearchFilter();
            searchFilter.ResourceType = Convert.ToString(TestContext.DataRow["resourcetype"]);
            searchFilter.Gender = Convert.ToInt32(TestContext.DataRow["Gender"]);
            searchFilter.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Dashboard/GetUtilizeReports", new StringContent(JsonConvert.SerializeObject(searchFilter).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            };
        }
        #endregion

        #region TalentRequisitionDeliveryHeadDashboard

        #region GetPendingRequisitionsForApprovalTest
        /// <summary>
        /// GetPendingRequisitionsForApprovalTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetPendingRequisitionsForApprovalTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string DepartmentCode = Convert.ToString(TestContext.DataRow["DepartmentCode"]);
                int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Dashboard/GetPendingRequisitionsForApproval?EmployeeId=" + EmployeeId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var dashboardData = result.Result;
                        //deserialize to your class
                        List<DeliveryHeadTalentRequisitionDetails> lstDashboardData = JsonConvert.DeserializeObject<List<DeliveryHeadTalentRequisitionDetails>>(dashboardData);
                        DeliveryHeadTalentRequisitionDetails dashboardDetails = lstDashboardData.Find(code => code.DepartmentCode.ToLower() == DepartmentCode.ToLower());
                        Assert.AreEqual(DepartmentCode, dashboardDetails.DepartmentCode);
                    }
                }

            };
        }
        #endregion

        #region GetRolesAndPositionsByRequisitionIdTest
        /// <summary>
        /// GetRolesAndPositionsByRequisitionIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetRolesAndPositionsByRequisitionIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string roleName = Convert.ToString(TestContext.DataRow["Role"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TRID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Dashboard/GetRolesAndPositionsByRequisitionId?talentRequisitionId=" + TrId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var requisitionData = result.Result;
                        //deserialize to your class
                        List<DeliveryHeadTalentRequisitionDetails> lstRequisitionData = JsonConvert.DeserializeObject<List<DeliveryHeadTalentRequisitionDetails>>(requisitionData);
                        DeliveryHeadTalentRequisitionDetails requisitionDetails = lstRequisitionData.Find(role => role.RoleName.ToLower() == roleName.ToLower());
                        Assert.AreEqual(roleName, requisitionDetails.RoleName);
                    }
                }

            };
        }
        #endregion

        #region GetTaggedEmployeeByTRAndRoleIdTest
        /// <summary>
        /// GetTaggedEmployeeByTRAndRoleIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetTaggedEmployeeByTRAndRoleIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string ProjectName = Convert.ToString(TestContext.DataRow["Project"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TRID"]);
                int RoleId = Convert.ToInt32(TestContext.DataRow["RoleIds"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Dashboard/GetTaggedEmployeeByTRAndRoleId?talentRequisitionId=" + TrId + "&&roleId=" + RoleId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<DeliveryHeadTalentRequisitionDetails> lstEmployeeData = JsonConvert.DeserializeObject<List<DeliveryHeadTalentRequisitionDetails>>(employeeData);
                        DeliveryHeadTalentRequisitionDetails employeeDetails = lstEmployeeData.Find(emp => emp.ProjectName.ToLower() == ProjectName.ToLower());
                        Assert.AreEqual(ProjectName, employeeDetails.ProjectName);
                    }
                }

            };
        }
        #endregion

        #region GetFinanceHeadListTest
        /// <summary>
        /// GetFinanceHeadList
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetFinanceHeadListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                GenericType genericType = new GenericType();
                genericType.Id = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Dashboard/GetFinanceHeadList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var financeHeadData = result.Result;
                        //deserialize to your class
                        List<GenericType> financeHeadDetails = JsonConvert.DeserializeObject<List<GenericType>>(financeHeadData);
                        GenericType financeHeads = financeHeadDetails.Find(financeHead => financeHead.Id == genericType.Id);
                        Assert.AreEqual(genericType.Id, financeHeads.Id);
                    }
                }

            };
        }
        #endregion

        #region ApproveTalentRequisitionTest
        /// <summary>
        /// ApproveTalentRequisitionTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]
        public void ApproveTalentRequisitionTest()
        {
            try
            {

                DeliveryHeadTalentRequisitionDetails talentRequisitionDetails = new DeliveryHeadTalentRequisitionDetails();
                talentRequisitionDetails.TalentRequisitionId = Convert.ToInt32(TestContext.DataRow["TalentrequisitionId"]);
                talentRequisitionDetails.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                talentRequisitionDetails.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                talentRequisitionDetails.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                talentRequisitionDetails.RequisitionType = Convert.ToInt32(TestContext.DataRow["RequisitionType"]);
                //talentRequisitionDetails.ApprovedByID.Add(new GenericType { Id=187 });

                List<GenericType> type = new List<GenericType>();
                type.Add(new GenericType { Id = 187 });

                talentRequisitionDetails.ApprovedByID = type;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Dashboard/ApproveTalentRequisition", new StringContent(JsonConvert.SerializeObject(talentRequisitionDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ApproveTalentRequisitionByFinanceTest
        /// <summary>
        /// ApproveTalentRequisitionByFinanceTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]
        public void ApproveTalentRequisitionByFinanceTest()
        {
            try
            {

                DeliveryHeadTalentRequisitionDetails talentRequisitionDetails = new DeliveryHeadTalentRequisitionDetails();
                talentRequisitionDetails.TalentRequisitionId = Convert.ToInt32(TestContext.DataRow["TalentrequisitionId"]);
                talentRequisitionDetails.FromEmployeeID = Convert.ToInt32(TestContext.DataRow["FromEmployeeID"]);
                talentRequisitionDetails.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                talentRequisitionDetails.Comments = Convert.ToString(TestContext.DataRow["Comments"]);
                talentRequisitionDetails.RequisitionType = Convert.ToInt32(TestContext.DataRow["RequisitionType"]);

                List<GenericType> type = new List<GenericType>();
                type.Add(new GenericType { Id = 187 });

                talentRequisitionDetails.ApprovedByID = type;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Dashboard/ApproveTalentRequisitionByFinance", new StringContent(JsonConvert.SerializeObject(talentRequisitionDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRequisitionWorkflowByTRIdTest
        /// <summary>
        /// GetRequisitionWorkflowByTRIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\TalentRequisitionSearch.csv", "TalentRequisitionSearch#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\TalentRequisitionSearch.csv"), TestMethod]

        public void GetRequisitionWorkflowByTRIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string Name = Convert.ToString(TestContext.DataRow["Name"]);
                int TrId = Convert.ToInt32(TestContext.DataRow["TRID"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Dashboard/GetRequisitionWorkflowByTRId?talentRequisitionId=" + TrId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<DeliveryHeadTalentRequisitionDetails> lstEmployeeData = JsonConvert.DeserializeObject<List<DeliveryHeadTalentRequisitionDetails>>(employeeData);
                        DeliveryHeadTalentRequisitionDetails employeeDetails = lstEmployeeData.Find(emp => emp.ToEmployeeName.ToLower() == Name.ToLower());
                        Assert.AreEqual(Name, employeeDetails.StatusCode);
                    }
                }

            };
        }
        #endregion



        #endregion
    }
}