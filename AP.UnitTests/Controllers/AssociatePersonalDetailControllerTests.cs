﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.Net.Http;
using System.Net;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace AP.Services.Controllers.Tests
{
    /// <summary>
    /// Summary description for AssociatePersonalDetailControllerTests
    /// </summary>
    [TestClass]
    public class AssociatePersonalDetailControllerTests:BaseControllerTests
    {
        #region AddPersonalDetailsTest
        /// <summary>
        /// AddPersonalDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociatePersonalDeatils.csv", "AssociatePersonalDeatils#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociatePersonalDeatils.csv"), TestMethod]
        public void AddPersonalDetailsTest()
        {
            PersonalDetails personalDetails = new PersonalDetails();

            personalDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            personalDetails.firstName = Convert.ToString(TestContext.DataRow["firstName"]);
            personalDetails.middleName = Convert.ToString(TestContext.DataRow["middleName"]);
            personalDetails.lastName = Convert.ToString(TestContext.DataRow["lastName"]);
            personalDetails.maritalStatus = Convert.ToString(TestContext.DataRow["maritalStatus"]);
            personalDetails.dob = Convert.ToDateTime(TestContext.DataRow["dob"]);
            personalDetails.personalEmail = Convert.ToString(TestContext.DataRow["personalEmail"]);
            personalDetails.workEmailID = Convert.ToString(TestContext.DataRow["workEmailID"]);
            personalDetails.gender = Convert.ToString(TestContext.DataRow["gender"]);
            personalDetails.bloodGroup = Convert.ToString(TestContext.DataRow["bloodGroup"]);
            personalDetails.nationality = Convert.ToString(TestContext.DataRow["nationality"]);
            personalDetails.designation = Convert.ToString(TestContext.DataRow["designation"]);
            personalDetails.gradeID = Convert.ToInt32(TestContext.DataRow["gradeID"]);
            personalDetails.deptID = Convert.ToInt32(TestContext.DataRow["deptID"]);
            personalDetails.doj = Convert.ToDateTime(TestContext.DataRow["JoinDate"]);
            personalDetails.technologyID = Convert.ToInt32(TestContext.DataRow["technologyID"]);
            personalDetails.empCode = Convert.ToString(TestContext.DataRow["empCode"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociatePersonalDetail/AddPersonalDetails", new StringContent(JsonConvert.SerializeObject(personalDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

                personalDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociatePersonalDetail/GetPersonalDetailsByID?empID=" + personalDetails.empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var personalData = result.Result;
                        //deserialize to your class
                        PersonalDetails personalDetailsData = JsonConvert.DeserializeObject<PersonalDetails>(personalData);
                        Assert.AreEqual(personalDetails.firstName, personalDetailsData.firstName);
                    }
                }
            }
        }
        #endregion

        #region UpdatePersonalDetailsTest
        /// <summary>
        /// UpdatePersonalDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociatePersonalDeatils.csv", "AssociatePersonalDeatils#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociatePersonalDeatils.csv"), TestMethod]
        public void UpdatePersonalDetailsTest()
        {
            int empID = 307;
            PersonalDetails personalDetails = new PersonalDetails();

            personalDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            personalDetails.firstName = Convert.ToString(TestContext.DataRow["firstName"]);
            personalDetails.middleName = Convert.ToString(TestContext.DataRow["middleName"]);
            personalDetails.lastName = Convert.ToString(TestContext.DataRow["lastName"]);
            personalDetails.empCode = Convert.ToString(TestContext.DataRow["empCode"]);
            personalDetails.dob = Convert.ToDateTime(TestContext.DataRow["dob"]);
            personalDetails.personalEmail = Convert.ToString(TestContext.DataRow["personalEmail"]);
            personalDetails.gender = Convert.ToString(TestContext.DataRow["gender"]);
            personalDetails.maritalStatus = Convert.ToString(TestContext.DataRow["maritalStatus"]);
            personalDetails.bloodGroup = Convert.ToString(TestContext.DataRow["bloodGroup"]);
            personalDetails.nationality = Convert.ToString(TestContext.DataRow["nationality"]);
            personalDetails.panNumber = Convert.ToString(TestContext.DataRow["panNumber"]);
            personalDetails.aadharNumber = Convert.ToString(TestContext.DataRow["aadharNumber"]);
            personalDetails.workEmailID = Convert.ToString(TestContext.DataRow["workEmailID"]);
            personalDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
            personalDetails.designation = Convert.ToString(TestContext.DataRow["designation"]);
            personalDetails.bgvStartDate = Convert.ToDateTime(TestContext.DataRow["BGVInitiatedDate"]);
            personalDetails.bgvCompletedDate = Convert.ToDateTime(TestContext.DataRow["BGVCompletionDate"]);
            personalDetails.passportValidDate = Convert.ToString(TestContext.DataRow["PassportDateValidUpto"]);
            personalDetails.doj = Convert.ToDateTime(TestContext.DataRow["JoinDate"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociatePersonalDetail/UpdatePersonalDetails", new StringContent(JsonConvert.SerializeObject(personalDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociatePersonalDetail/GetPersonalDetailsByID?empID="+ empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var personalData = result.Result;
                        dynamic data = JObject.Parse(personalData);
                        Assert.AreEqual(personalDetails.personalEmail, data.personalEmail);
                        //deserialize to your class
                        //List<PersonalDetails> lstPersonalData = JsonConvert.DeserializeObject<List<PersonalDetails>>(personalData);
                        //PersonalDetails details = lstPersonalData.Find(data => data.personalEmail.ToLower().Trim() == personalDetails.personalEmail.ToLower().Trim());
                        //Assert.AreEqual(personalDetails.personalEmail, details.personalEmail);
                    }
                }

            }
        }
        #endregion

        #region GetAssociateDetailsTest
        /// <summary>
        /// GetAssociateDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetAssociateDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociatePersonalDetail/GetAssociateDetails"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region GetPersonalDetailsByIDTest
        /// <summary>
        /// GetPersonalDetailsByIDTest
        /// </summary>
        [TestMethod()]
        public void GetPersonalDetailsByIDTest()
        {
            try
            {
                int empID = 3;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociatePersonalDetail/GetPersonalDetailsByID?empID=" + empID))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion
    }
}
