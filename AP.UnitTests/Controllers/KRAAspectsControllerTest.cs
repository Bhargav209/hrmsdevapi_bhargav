﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class NotificationTypeControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region GetExpertiseAreaTest
        /// <summary>
        /// GetExpertiseAreaTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KraAspectInfo.csv", "KraAspectInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KraAspectInfo.csv"), TestMethod]
        public void GetKRAAspectTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string KRAAspect = Convert.ToString(TestContext.DataRow["KRAAspectName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRAAspects/GetKRAAspect"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAAspectData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstKRAAspectData = JsonConvert.DeserializeObject<List<GenericType>>(KRAAspectData);
                        GenericType KRA = lstKRAAspectData.Find(kra => kra.Name.ToLower().Trim() == KRAAspect.ToLower().Trim());
                        Assert.AreEqual(KRAAspect, KRA.Name);
                    }
                }

            };
        }
        #endregion

        //#region CreateKRAAspectTest
        ///// <summary>
        ///// CreateKRAAspectTest
        ///// </summary>
        //[DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KraAspectInfo.csv", "KraAspectInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KraAspectInfo.csv", "TestInput"), TestMethod]
        //public void CreateKRAAspectTest()
        //{
        //    try
        //    {

        //        KRAAspectData kraAspectData = new KRAAspectData();
        //        kraAspectData.KRAAspectName = TestContext.DataRow["KRAAspectName"].ToString();
        //        kraAspectData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);

        //        using (HttpClient client = new HttpClient())
        //        {
        //            client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //            using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRAAspects/CreateKRAAspect", new StringContent(JsonConvert.SerializeObject(kraAspectData).ToString(), Encoding.UTF8, "application/json")).Result)
        //            {
        //                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //            }

        //            using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRAAspects/GetKRAAspect"))
        //            {
        //                using (HttpResponseMessage response = client.SendAsync(request).Result)
        //                {
        //                    Task<string> result = response.Content.ReadAsStringAsync();
        //                    var KRAAspectData = result.Result;
        //                    //deserialize to your class
        //                    List<GenericType> lstKRAAspectData = JsonConvert.DeserializeObject<List<GenericType>>(KRAAspectData);
        //                    GenericType KRA = lstKRAAspectData.Find(kra => kra.Name.ToLower().Trim() == kraAspectData.KRAAspectName.ToLower().Trim());
        //                    Assert.AreEqual(kraAspectData.KRAAspectName, KRA.Name);
        //                }
        //            }
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion

        //#region UpdateKRAAspectTest
        ///// <summary>
        ///// UpdateKRAAspectTest
        ///// </summary>
        //[DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KraAspectInfo.csv", "KraAspectInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KraAspectInfo.csv", "TestInput"), TestMethod]
        //public void UpdateKRAAspectTest()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //        KRAAspectData kraAspectData = new KRAAspectData();
        //        //kraAspectData.KRAAspectID = 27;//Convert.ToInt32(TestContext.DataRow["KraAspectId"]);
        //        kraAspectData.KRAAspectName = TestContext.DataRow["KRAAspectName"].ToString();

        //        //2. Update 
        //        using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRAAspects/UpdateKRAAspect", new StringContent(JsonConvert.SerializeObject(kraAspectData).ToString(), Encoding.UTF8, "application/json")).Result)
        //        {
        //            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //        }
        //        //3. Get & Assert
        //        using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRAAspects/GetKRAAspect"))
        //        {
        //            using (HttpResponseMessage response = client.SendAsync(request).Result)
        //            {
        //                Task<string> result = response.Content.ReadAsStringAsync();
        //                var KRAAspectData = result.Result;
        //                //deserialize to your class
        //                List<GenericType> lstKRAAspectData = JsonConvert.DeserializeObject<List<GenericType>>(KRAAspectData);
        //                GenericType KRA = lstKRAAspectData.Find(kra => kra.Name.ToLower().Trim() == kraAspectData.KRAAspectName.ToLower().Trim());
        //                Assert.AreEqual(kraAspectData.KRAAspectName, KRA.Name);
        //            }
        //        }
        //    };
        //}
        //#endregion

    }
}
