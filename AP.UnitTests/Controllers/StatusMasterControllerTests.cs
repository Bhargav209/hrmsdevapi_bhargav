﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System;
using System.Net;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Text;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class StatusMasterControllerTests:BaseControllerTests
    {

        #region CreateStatusMasterTest
        /// <summary>
        /// CreateStatusMasterTest
        /// </summary>

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Status.csv", "Status#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Status.csv"),TestMethod]
        public void CreateStatusMasterTest()
        {
            try
            {
                StatusData statusData = new StatusData();

                if (!Convert.IsDBNull(TestContext.DataRow["statusCode"]) || !Convert.IsDBNull(TestContext.DataRow["statusDesc"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
                {
                    statusData.StatusCode = TestContext.DataRow["statusCode"].ToString();
                    statusData.StatusDescription= TestContext.DataRow["statusDesc"].ToString();
                    statusData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/StatusMaster/CreateStatusMaster",new StringContent(JsonConvert.SerializeObject(statusData),Encoding.UTF8,"application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetStatusMasterDetailsTest
        /// <summary>
        /// GetStatusMasterDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetStatusMasterDetailsTest()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/StatusMaster/GetStatusMasterDetails?" ))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateStatusMasterDetails
        /// <summary>
        /// UpdateStatusMasterDetailsTest
        /// </summary>
      
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Status.csv", "Status#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Status.csv"),TestMethod]
        public void UpdateStatusMasterDetailsTest()
        {
            try
            {
                StatusData statusData = new StatusData();

                if (!Convert.IsDBNull(TestContext.DataRow["statusID"]) || !Convert.IsDBNull(TestContext.DataRow["statusCode"]) || !Convert.IsDBNull(TestContext.DataRow["statusDesc"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
                {
                    statusData.StatusId = Convert.ToInt16(TestContext.DataRow["statusID"]);
                    statusData.StatusCode = TestContext.DataRow["statusCode"].ToString();
                    statusData.StatusDescription = TestContext.DataRow["statusDesc"].ToString();
                    statusData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                }

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    {
                        using (HttpResponseMessage response = client.PostAsync(serviceURL + "/StatusMaster/UpdateStatusMasterDetails",new StringContent(JsonConvert.SerializeObject(statusData),Encoding.UTF8,"application/json")).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }
                };
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
}