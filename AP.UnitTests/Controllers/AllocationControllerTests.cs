﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using AP.DomainEntities;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using System.Threading.Tasks;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class AllocationControllerTests : BaseControllerTests
    {
        #region GetApprovedTalentRequisitionListTest
        /// <summary>
        /// GetApprovedTalentRequisitionListTest
        /// </summary>
        [TestMethod]
        public void GetApprovedTalentRequisitionListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetApprovedTalentRequisitionListTest"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            }
        }
        #endregion

        #region GetTalentRequisitionByIdTest
        /// <summary>
        /// GetTalentRequisitionByIdTest
        /// </summary> 
        //Data provider, connection string, data table, data access method
        [TestMethod]
        public void GetTalentRequisitionByIdTest()
        {
            int tRId = 4;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetTalentRequisitionById?tRId=" + tRId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            }

        }
        #endregion

        #region GetRequiredSkillsDetailsTest
        /// <summary>
        /// GetRequiredSkillsDetailsTest
        /// </summary>
        [TestMethod]
        public void GetRequiredSkillsDetailsTest()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                int id = 2;

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetRequiredSkillsDetails?requisitionRoleDetailId=" + id))
                {
                    using (HttpResponseMessage response = httpClient.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion

        #region AllocateAssociatesTest
        /// <summary>
        /// AllocateAssociatesTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Allocation.csv", "Allocation#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Allocation.csv"), TestMethod]
        public void AllocateAssociatesTest()
        {
            List<AssociateAllocationDetails> associateAllocationList = new List<AssociateAllocationDetails>();
            AssociateAllocationDetails associateAllocation = new AssociateAllocationDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["AllocationPercentage"]) || !Convert.IsDBNull(TestContext.DataRow["EffectiveDate"])
                || !Convert.IsDBNull(TestContext.DataRow["EmployeeId"]) || !Convert.IsDBNull(TestContext.DataRow["isCritical"])
                || !Convert.IsDBNull(TestContext.DataRow["ProjectId"]) || !Convert.IsDBNull(TestContext.DataRow["RoleId"])
                || !Convert.IsDBNull(TestContext.DataRow["TRId"]) || !Convert.IsDBNull(TestContext.DataRow["ReportingManagerId"])
                || !Convert.IsDBNull(TestContext.DataRow["RequisitionRoleDetailID"]))
            {
                associateAllocation.AllocationPercentage = Convert.ToDecimal(TestContext.DataRow["AllocationPercentage"]);
                associateAllocation.ClientBillingPercentage = Convert.ToDecimal(TestContext.DataRow["BillablePercentage"]);
                associateAllocation.EffectiveDate = Convert.ToDateTime(TestContext.DataRow["EffectiveDate"]);
                associateAllocation.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                associateAllocation.isCritical = Convert.ToBoolean(TestContext.DataRow["isCritical"]);
                associateAllocation.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                associateAllocation.RoleMasterId = Convert.ToInt32(TestContext.DataRow["RoleId"]);
                associateAllocation.TalentRequisitionId = Convert.ToInt32(TestContext.DataRow["TRId"]);
                associateAllocation.ReportingManagerId = Convert.ToInt32(TestContext.DataRow["ReportingManagerId"]);
                associateAllocation.RequisitionRoleDetailID = Convert.ToInt32(TestContext.DataRow["RequisitionRoleDetailID"]);
                associateAllocationList.Add(associateAllocation);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Allocation/AllocateAssociates", new StringContent(JsonConvert.SerializeObject(associateAllocationList).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }

        }
        #endregion

        #region CloseTalentRequisitionTest
        /// <summary>
        /// CloseTalentRequisitionTest
        /// </summary>
        [TestMethod]
        public void CloseTalentRequisitionTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                int id = 4;

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/CloseTalentRequisition?tRId=" + id))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion

        #region GetMatchingProfilesTest
        /// <summary>
        /// GetMatchingProfilesTest
        /// </summary>
        [TestMethod]
        public void GetMatchingProfilesTest()
        {
            int requisitionRoleId = 11;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetMatchingProfiles?requisitionRoleId=" + requisitionRoleId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };

        }
        #endregion

        #region GetAllAssociateDetails
        /// <summary>
        /// GetAllAssociateDetails
        /// </summary>
        [TestMethod()]
        public void GetAllAssociateDetails()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string empId = "N0370";
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetAssociateDetailsById?empId=" + empId))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion

        #region GetEmployeeByDeptIdTest
        /// <summary>
        /// GetEmployeeByDeptIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Allocation.csv", "Allocation#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Allocation.csv"), TestMethod]

        public void GetEmployeeByDeptIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int DepartmentId = Convert.ToInt32(TestContext.DataRow["DeptId"]);
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetEmployeeByDeptId?deptId=" + DepartmentId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<AssociateDetails> lstEmployeeData = JsonConvert.DeserializeObject<List<AssociateDetails>>(employeeData);
                        AssociateDetails associateData = lstEmployeeData.Find(dept => dept.DepartmentId == DepartmentId);
                        Assert.AreEqual(DepartmentId, associateData.DepartmentId);
                    }
                }

            };
        }
        #endregion

        #region GetTalentPoolListTest
        /// <summary>
        /// GetTalentPoolList
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Allocation.csv", "Allocation#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Allocation.csv"), TestMethod]

        public void GetTalentPoolListTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string practiceArea = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["PracticeAreaCode"].ToString()))
                {
                    practiceArea = TestContext.DataRow["PracticeAreaCode"].ToString();
                }

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetTalentPoolList"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var practiceAreaData = result.Result;
                        //deserialize to your class
                        List<PracticeAreaDetails> lstPracticeData = JsonConvert.DeserializeObject<List<PracticeAreaDetails>>(practiceAreaData);
                        PracticeAreaDetails practiceDetails = lstPracticeData.Find(data => data.PracticeAreaCode.ToString().Trim() == practiceArea.ToString().Trim());
                        Assert.AreEqual(practiceArea, practiceDetails.PracticeAreaCode);
                    }
                }

            };
        }
        #endregion

        #region GetProjectsById
        /// <summary>
        /// GetProjectsById
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Allocation.csv", "Allocation#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Allocation.csv"), TestMethod]

        public void GetProjectsById()
        {
            using (HttpClient client = new HttpClient())
            {
                int projectTypeId = Convert.ToInt32(TestContext.DataRow["projectTypeId"]);
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Allocation/GetProjectsById?projectTypeId=" + projectTypeId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var projectData = result.Result;
                        //deserialize to your class
                        List<ProjectData> lstprojectData = JsonConvert.DeserializeObject<List<ProjectData>>(projectData);
                        ProjectData associateDetails = lstprojectData.Find(type => type.ProjectTypeId == projectTypeId);
                        Assert.AreEqual(projectTypeId, associateDetails.ProjectTypeId);
                    }
                }

            };
        }
        #endregion

        #region AddToTalentPoolTest
        /// <summary>
        /// AddToTalentPoolTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Allocation.csv", "Allocation#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Allocation.csv", "TestInput"), TestMethod]
        public void AddToTalentPoolTest()
        {
            try
            {
                TalentPoolData talentPoolData = new TalentPoolData();
                int TalentPoolId = Convert.ToInt32(TestContext.DataRow["TalentPoolId"]);
                int projectId = Convert.ToInt32(TestContext.DataRow["Project"]);
                int practiceAreaId = Convert.ToInt32(TestContext.DataRow["PracticeAreaId"]);

                talentPoolData.ProjectId = projectId;
                talentPoolData.PracticeAreaId = practiceAreaId;
                talentPoolData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Allocation/AddToTalentPool", new StringContent(JsonConvert.SerializeObject(talentPoolData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        
    }
}