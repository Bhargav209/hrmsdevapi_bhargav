﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class FileUploadAndDownloadControllerTests:BaseControllerTests
    {
        #region GetFileUploadedDataTest
        /// <summary>
        /// Get File Uploaded Data Test
        /// </summary>
        [TestMethod()]
        public void GetFileUploadedDataTest()
        {
            int empID = 6;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/FileUploadAndDownload/GetFileUploadedData?empID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region generate pdf report
        /// <summary>
        /// This method is responsible for generating employee wise PDF report for HR manager/ HR Dead.
        /// </summary>
        [TestMethod()]
        public void GeneratePDFReportTest()
        {
            int empId = 14;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/FileUploadAndDownload/GeneratePDFReport?empId=" + empId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region File upload

        /// <summary>
        /// This method is used to upload onboard documents by associate
        /// </summary>
        [TestMethod()]
        public void PostFileTest()
        {
           
        }
        #endregion

        #region DeleteFileUploadedDataTest
        /// <summary>
        /// Delete File Uploaded Data Test
        /// </summary>
        [TestMethod()]
        public void DeleteFileUploadedDataTest()
        {
            int empID = 12, Id = 6;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/FileUploadAndDownload/DeleteFileUploadedData?empID=" + empID + "&&iD=" + Id))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion
    }
}

