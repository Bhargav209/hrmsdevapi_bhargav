﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class CommonControllerTests : BaseControllerTests
    {
        #region GetSkillsbySkillGroupIDTest
        /// <summary>
        /// GetSkillsbySkillGroupIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CommonData.csv", "CommonData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CommonData.csv"), TestMethod]

        public void GetSkillsbySkillGroupIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string skillName = string.Empty;
                int skillGroupID = Convert.ToInt32(TestContext.DataRow["skillGroupID"]);
                if (!string.IsNullOrEmpty(TestContext.DataRow["skillName"].ToString()))
                {
                    skillName = TestContext.DataRow["skillName"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Common/GetSkillsbySkillGroupID?SkillGroupID=" + skillGroupID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var skillData = result.Result;
                        //deserialize to your class
                        List<SkillData> skillsList = JsonConvert.DeserializeObject<List<SkillData>>(skillData);
                        SkillData skills = skillsList.Find(skill => skill.SkillName.ToLower().Trim() == skillName.ToLower().Trim());
                        Assert.AreEqual(skillName, skills.SkillName);
                    }
                }

            };
        }
        #endregion
    }
}
