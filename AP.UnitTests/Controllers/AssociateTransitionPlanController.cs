﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.Net.Http;
using System.Net;
using System;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class AssociateTransitionPlanController : BaseControllerTests
    {
        #region GetTasksById
        /// <summary>
        /// GetTasksById
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionPlanData.csv", "AssociateTransitionPlanData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionPlanData.csv"), TestMethod]

        public void GetTasksById()
        {
            using (HttpClient client = new HttpClient())
            {
                string CategoryId = Convert.ToString(TestContext.DataRow["TransitionPlanCategoryId"]);
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TransitionPlan/GetTasksById?categoryId=" + CategoryId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var categoryData = result.Result;
                        //deserialize to your class
                        List<TransitionPlanDetailsData> categories = JsonConvert.DeserializeObject<List<TransitionPlanDetailsData>>(categoryData);
                        Assert.AreEqual("Configuration Management", categories[4].TaskName);
                    }
                }

            };
        }
        #endregion

        #region GetCategoriesTest
        /// <summary>
        /// GetCategoriesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionPlanData.csv", "AssociateTransitionPlanData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionPlanData.csv"), TestMethod]

        public void GetCategoriesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TransitionPlan/GetCategories"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var categoryData = result.Result;
                        //deserialize to your class
                        List<TransitionPlanDetailsData> planDetailsData = JsonConvert.DeserializeObject<List<TransitionPlanDetailsData>>(categoryData);
                        Assert.AreEqual("Project Management", planDetailsData[0].CategoryName);
                    }
                }

            };
        }
        #endregion
    }
}
