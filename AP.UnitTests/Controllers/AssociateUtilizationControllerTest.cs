﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
using AP.DomainEntities;
using System.Text;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class AssociateUtilizationControllerTest:BaseControllerTests
    {
 

        #region PostAssociateUtilizeTest
        /// <summary>
        /// PostAssociateUtilizeTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateUtilization.csv", "AssociateUtilization#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateUtilization.csv"), TestMethod]
        public void PostAssociateUtilizeTest()
        {
            AssociateUsage associateUtilize = new AssociateUsage();
            associateUtilize.Id = Convert.ToInt32(TestContext.DataRow["Id"]);
            associateUtilize.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            associateUtilize.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
            associateUtilize.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            associateUtilize.RoleMasterId = Convert.ToInt32(TestContext.DataRow["RoleId"]);
            associateUtilize.PercentUtilization = Convert.ToInt32(TestContext.DataRow["PercentUtilization"]);
            associateUtilize.Billable = Convert.ToBoolean(TestContext.DataRow["Billable"]);
            associateUtilize.Critical = Convert.ToBoolean(TestContext.DataRow["Critical"]);
            associateUtilize.ManagerId = Convert.ToInt32(TestContext.DataRow["ManagerId"]);
            associateUtilize.PrimaryManager = Convert.ToBoolean(TestContext.DataRow["PrimaryManager"]);
            associateUtilize.ActualStartDate = Convert.ToDateTime(TestContext.DataRow["ActualStartDate"]);
            associateUtilize.ActualEndDate = Convert.ToDateTime(TestContext.DataRow["ActualEndDate"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateUtilization/PostAssociateUtilize", new StringContent(JsonConvert.SerializeObject(associateUtilize).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    int result = Convert.ToInt32(response.Content.ReadAsStringAsync().Result);
                    Assert.AreNotEqual(result, 0);
                }

            }
        }
        #endregion

        #region GetAssociatesUtilizatonTest
        /// <summary>
        /// GetAssociatesUtilizatonTest
        /// </summary>
        [TestMethod()]
        public void GetAssociatesUtilizatonTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateUtilization/GetAssociatesUtilizaton"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetPercentUtilizationTest
        /// <summary>
        /// GetPercentUtilizationTest
        /// </summary>
        [TestMethod()]
        public void GetPercentUtilizationTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string content = "?projectId=23&&employeeId=10";
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateUtilization/GetPercentUtilization" + content))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion
    }
}
