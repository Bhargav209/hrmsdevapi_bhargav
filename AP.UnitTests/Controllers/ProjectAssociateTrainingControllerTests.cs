﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using AP.DomainEntities;
using System.Net;
using Newtonsoft.Json;
using System;
using AP.Utility;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class ProjectAssociateTrainingControllerTests:BaseControllerTests
    {
        [TestMethod()]
        public void GetAssociateSkillGapByEmpIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int empId = 58;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectAssociateTraining/GetAssociateSkillGapByEmpId?empId="+empId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }
                }
            };
        }

        [TestMethod()]
        public void GetProjectTrainingsByProjectIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int projectId = 18;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectAssociateTraining/GetProjectTrainingsByProjectId?projectId=" + projectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }
                }
            };
        }

        [TestMethod()]
        public void GetAssessedByProjectIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int projectId = 8;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectAssociateTraining/GetAssessedByProjectID?projectId=" + projectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }
                }
            };
        }

        [TestMethod()]
        public void GetProjectAssociateTrainingsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int empId = 58;
                int? projectId = 0;
                int? projectAssociateTrainingId = 0;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectAssociateTraining/GetProjectAssociateTrainings?empId="+empId+"&&projectId=" + projectId+ "&&projectAssociateTrainingId="+ projectAssociateTrainingId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.IsNotNull(var);
                    }
                }
            };
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProjectAssociateTrainings.csv", "ProjectAssociateTrainings#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProjectAssociateTrainings.csv", "TestInput"), TestMethod]
        public void SaveOrUpdateProjectAssociateTrainingsTest()
        {
            List<ProjectAssociateTrainingData> list = new List<ProjectAssociateTrainingData>();
            ProjectAssociateTrainingData pat = new ProjectAssociateTrainingData();
            pat.ProjectAssociateTrainingId = Convert.ToInt32(TestContext.DataRow["ProjectAssociateTrainingId"]);
            pat.AssociateSkillGapId = Convert.ToInt32(TestContext.DataRow["AssociateSkillGapId"]);
            pat.ProjectTrainingId = Convert.ToInt32(TestContext.DataRow["ProjectTrainingId"]);
            pat.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            pat.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
            pat.AssessedById = Convert.ToInt32(TestContext.DataRow["AssessedById"]);
            if(TestContext.DataRow["ProficiencyLevelId"].ToString()!="")
                pat.ProficiencyLevelId = Convert.ToInt32(TestContext.DataRow["ProficiencyLevelId"]);
            pat.FromDate = Commons.GetDateTimeInIST(Convert.ToDateTime(TestContext.DataRow["FromDate"]));
            pat.ToDate = Convert.ToDateTime(TestContext.DataRow["ToDate"]);
            pat.AssessedDate = Convert.ToDateTime(TestContext.DataRow["AssessedDate"]);
            pat.SkillName = TestContext.DataRow["SkillName"].ToString();
            pat.Status = TestContext.DataRow["Status"].ToString();
            pat.IsSkillApplied=Convert.ToBoolean(TestContext.DataRow["IsSkillApplied"]);

            list.Add(pat);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProjectAssociateTraining/SaveOrUpdateProjectAssociateTrainings/", new StringContent(JsonConvert.SerializeObject(list), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
    }
}