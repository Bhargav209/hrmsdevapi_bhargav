﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using AP.DomainEntities;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using AP.Services.Controllers.Tests;
using System;
using AP.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AP.UnitTests
{
    [TestClass()]
    public class KRAScaleMasterControllerTests : BaseControllerTests
    {
        #region GetKRAScaleMasterTest
        /// <summary>
        /// GetKRAScaleMasterTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAScaleMaster.csv", "KRAScaleMaster#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAScaleMaster.csv"), TestMethod]
        public void GetKRAScaleMasterTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int MaximumScale = Convert.ToInt32(TestContext.DataRow["MaximumScale"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRAScaleMaster/GetKRAScaleMasters"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var KRAData = result.Result;
                        //deserialize to your class
                        List<KRAScaleData> lstKRAData = JsonConvert.DeserializeObject<List<KRAScaleData>>(KRAData);
                        KRAScaleData maximum = lstKRAData.Find(kra => kra.MaximumScale == MaximumScale);
                        Assert.AreEqual(MaximumScale, maximum.MaximumScale);
                    }
                }

            };
        }
        #endregion

        #region CreateScaleMasterTest
        /// <summary>
        /// CreateScaleMasterTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAScaleMaster.csv", "KRAScaleMaster#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAScaleMaster.csv", "TestInput"), TestMethod]
        public void CreateScaleMasterTest()
        {
            try
            {

                KRAScaleData kraScaleData = new KRAScaleData();
                kraScaleData.MinimumScale = Convert.ToInt32(TestContext.DataRow["MinimumScale"]);
                kraScaleData.MaximumScale = Convert.ToInt32(TestContext.DataRow["MaximumScale"]);
                kraScaleData.KRAScaleTitle = Convert.ToString(TestContext.DataRow["KRAScaleTitle"]);
                List<KRAScaleDetails> details = new List<KRAScaleDetails>();
                details.Add(new KRAScaleDetails {  KRAScale = 1, ScaleDescription = "good1" });
                details.Add(new KRAScaleDetails { KRAScale = 2, ScaleDescription = "good2" });
                details.Add(new KRAScaleDetails { KRAScale = 3, ScaleDescription = "GOOD1" });
                kraScaleData.KRAScaleDetails = details;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRAScaleMaster/CreateScaleMaster", new StringContent(JsonConvert.SerializeObject(kraScaleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateKRAAspectTest
        /// <summary>
        /// UpdateKRAAspectTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAScaleMaster.csv", "KRAScaleMaster#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAScaleMaster.csv", "TestInput"), TestMethod]
        public void UpdateKRAAspectTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                List<KRAScaleDetails> details = new List<KRAScaleDetails>();
                details.Add(new KRAScaleDetails {KRAScaleDetailId = 20, ScaleDescription = "testingtests" });
                details.Add(new KRAScaleDetails { KRAScaleDetailId = 21, ScaleDescription = "testertests" });

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRAScaleMaster/UpdateScaleMaster", new StringContent(JsonConvert.SerializeObject(details).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region DeleteScaleMasterTest
        /// <summary>
        /// DeleteScaleMasterTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAScaleMaster.csv", "KRAScaleMaster#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAScaleMaster.csv", "TestInput"), TestMethod]
        public void DeleteScaleMasterTest()
        {
            int kraScaleMasterId = 5;
            KRAScaleMasterDetails KRAScaleMasterDetails = new KRAScaleMasterDetails();
            KRAScaleMasterDetails.DeleteScaleMaster(kraScaleMasterId);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/KRAScaleMaster/DeleteScaleMaster", new StringContent(JsonConvert.SerializeObject(KRAScaleMasterDetails), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion
    }
}