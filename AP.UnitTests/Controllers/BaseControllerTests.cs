﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using AP.DomainEntities;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using AP.Utility;
using System.IO;
using System.Reflection;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class BaseControllerTests
    {
        public string serviceURL = ConfigurationManager.AppSettings["ServiceURL"];
        public TestContext TestContext { get; set; }
        public string serviceToken = string.Empty;
        [TestInitialize]
        public void Init()
        {
            if (!string.IsNullOrEmpty(serviceToken))
            {
                return;
            }


            using (HttpClient client = new HttpClient())
            {
                var encryptPassword = Commons.EncryptStringAES("yourPassword");
                var data = "grant_type=password&username=yourUsername&password=" + encryptPassword;
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/UserLogin", new StringContent(data.ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    //var serviceToken = clien
                    //   response.Headers.WwwAuthenticate

                    string result = response.Content.ReadAsStringAsync().Result;
                    dynamic jsonResult = JsonConvert.DeserializeObject(result);

                    serviceToken = Convert.ToString(jsonResult.access_token);

                }

            };

        }
        public static dynamic LoadJson(string filePath)
        {
            dynamic data = null;
            string fullPath = GetDataFileFullPath(filePath);
            using (StreamReader reader = new StreamReader(fullPath))
            {
                string json = reader.ReadToEnd();
                data = JsonConvert.DeserializeObject(json);
            }
            return data;
        }
        private static string GetDataFileFullPath(string filePath)
        {
            var path = string.Format(@"{0}\TestInput\{1}", Assembly.GetExecutingAssembly().GetName().Name, filePath);
            return Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory() + "../../..").ToString(), path);
        }
    }
}