﻿using AP.DomainEntities;
using AP.Services.Controllers.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class ProjectRoleControllerTests : BaseControllerTests
    {
        #region AssignProjectRoleTest
        /// <summary>
        /// AssignProjectRoleTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProjectRoleDetails.csv", "ProjectRoleDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProjectRoleDetails.csv"), TestMethod]
        public void AssignProjectRole()
        {
                string filePath = "ProjectRoleData.json";
                dynamic data = LoadJson(filePath);
                dynamic roleData = new { ProjectRoleData = data.ProjectRoleData, status = data.status };
                StringContent content = new StringContent(JsonConvert.SerializeObject(roleData).ToString(), Encoding.UTF8, "application/json");

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProjectRoleAssignment/AssignProjectRole",content).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
        }
        #endregion

        #region RoleApprovalTest
        /// <summary>
        /// RoleApprovalTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProjectRoleDetails.csv", "ProjectRoleDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProjectRoleDetails.csv"), TestMethod]
        public void RoleApprovalTest()
        {
             string filePath = "ProjectRoleData.json";
                dynamic data = LoadJson(filePath);
                dynamic roleData = new { ProjectRoleData = data.ProjectRoleData, status = data.status };
                StringContent content = new StringContent(JsonConvert.SerializeObject(roleData).ToString(), Encoding.UTF8, "application/json");

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProjectRoleAssignment/RoleApproval", content).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
        }
        #endregion

        #region GetRoleNotifications
        /// <summary>
        /// GetRoleNotifications
        /// </summary>
        [TestMethod()]
        public void GetRoleNotifications()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProjectRoleAssignment/GetRoleNotifications"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    }

                };
            }
            catch
            {

                throw;
            }
        }
        #endregion
    }
}
