﻿using System.Net;
using System.Text;
using System;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using AP.DomainEntities;
using Newtonsoft.Json;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class DesignationControllerTests : BaseControllerTests
    {
        #region SaveDesignationDetailsTest
        /// <summary>
        /// Test method for adding designation details
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\DesignationInfo.csv", "DesignationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\DesignationInfo.csv", "TestInput"), TestMethod]
        public void SaveDesignationDetailsTest()
        {
            DesignationData designationData = new DesignationData();
            if (!Convert.IsDBNull(TestContext.DataRow["DesignationCode"]) || !Convert.IsDBNull(TestContext.DataRow["DesignationName"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                designationData.DesignationCode = Convert.ToString(TestContext.DataRow["DesignationCode"]);
                designationData.DesignationName = Convert.ToString(TestContext.DataRow["DesignationName"]);
                designationData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                designationData.GradeId = Convert.ToInt32(TestContext.DataRow["GradeId"]);
            }
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Designation/SaveDesignationDetails", new StringContent(JsonConvert.SerializeObject(designationData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region UpdateDesignationTest
        /// <summary>
        /// Test method for updating designation details
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\DesignationInfo.csv", "DesignationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\DesignationInfo.csv", "TestInput"), TestMethod]
        public void UpdateDesignationTest()
        {
            DesignationData designationData = new DesignationData();

            if (!Convert.IsDBNull(TestContext.DataRow["DesignationCode"]) || !Convert.IsDBNull(TestContext.DataRow["DesignationName"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                designationData.DesignationId = Convert.ToInt32(TestContext.DataRow["DesignationId"]);
                designationData.DesignationCode = Convert.ToString(TestContext.DataRow["DesignationCode"]);
                designationData.DesignationName = Convert.ToString(TestContext.DataRow["DesignationName"]);
                designationData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                designationData.GradeId = Convert.ToInt32(TestContext.DataRow["GradeId"]);
            }

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Designation/UpdateDesignationDetails", new StringContent(JsonConvert.SerializeObject(designationData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region GetDesignationsTest
        /// <summary>
        /// Test method for retrieving designation details
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\DesignationInfo.csv", "DesignationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\DesignationInfo.csv", "TestInput"), TestMethod]
        public void GetDesignationsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Designation/GetDesignationDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion
    }
}
