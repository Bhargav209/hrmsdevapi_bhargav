﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using AP.DomainEntities;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class AsociateFamilyDetailControllerTests:BaseControllerTests
    {
        /// <summary>
        /// AddFamilyDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Family.csv", "Family#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Family.csv"), TestMethod]
        public void AddFamilyDetailsTest()
        {
            UserDetails userDetails = new UserDetails();
            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);

            List<RelationDetails> relationsInfoList = new List<RelationDetails>();
            RelationDetails relationsInfoOne = new RelationDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["name"]) || !Convert.IsDBNull(TestContext.DataRow["relations"]) || !Convert.IsDBNull(TestContext.DataRow["dob"]) || !Convert.IsDBNull(TestContext.DataRow["occupation"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
            {
                relationsInfoOne.name = Convert.ToString(TestContext.DataRow["name"]);
                relationsInfoOne.relationship = Convert.ToString(TestContext.DataRow["relations"]);
                relationsInfoOne.dob = Convert.ToDateTime(TestContext.DataRow["dob"]);
                relationsInfoOne.occupation = Convert.ToString(TestContext.DataRow["occupation"]);
                relationsInfoOne.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                relationsInfoList.Add(relationsInfoOne);
            }

            List<EmergencyContactData> emergencyContactDataList = new List<EmergencyContactData>();
            EmergencyContactData emergencyContactDataInfo = new EmergencyContactData();

            if (!Convert.IsDBNull(TestContext.DataRow["contactName"]) || !Convert.IsDBNull(TestContext.DataRow["isPrimary"]) || !Convert.IsDBNull(TestContext.DataRow["relationship"]) || !Convert.IsDBNull(TestContext.DataRow["telephoneNo"]) || !Convert.IsDBNull(TestContext.DataRow["mobileNo"])
                || !Convert.IsDBNull(TestContext.DataRow["city"]) || !Convert.IsDBNull(TestContext.DataRow["country"]) || !Convert.IsDBNull(TestContext.DataRow["state"]) || !Convert.IsDBNull(TestContext.DataRow["zip"]) || !Convert.IsDBNull(TestContext.DataRow["addressLine1"]) || !Convert.IsDBNull(TestContext.DataRow["addressLine2"]))
            {
                emergencyContactDataInfo.contactName = Convert.ToString(TestContext.DataRow["contactName"]);
                emergencyContactDataInfo.isPrimary = Convert.ToBoolean(TestContext.DataRow["isPrimary"]);
                emergencyContactDataInfo.relationship = Convert.ToString(TestContext.DataRow["relationship"]);
                emergencyContactDataInfo.telephoneNo = Convert.ToString(TestContext.DataRow["telephoneNo"]);
                emergencyContactDataInfo.mobileNo = Convert.ToString(TestContext.DataRow["mobileNo"]);
                emergencyContactDataInfo.city = Convert.ToString(TestContext.DataRow["city"]);
                emergencyContactDataInfo.country = Convert.ToString(TestContext.DataRow["country"]);
                emergencyContactDataInfo.state = Convert.ToString(TestContext.DataRow["state"]);
                emergencyContactDataInfo.zip = Convert.ToString(TestContext.DataRow["zip"]);
                emergencyContactDataInfo.addressLine1 = Convert.ToString(TestContext.DataRow["addressLine1"]);
                emergencyContactDataInfo.addressLine2 = Convert.ToString(TestContext.DataRow["addressLine2"]);
                emergencyContactDataList.Add(emergencyContactDataInfo);
            }

            userDetails.emergencyContactsInfoInfo = emergencyContactDataList;
            userDetails.relationsInfo = relationsInfoList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AsociateFamilyDetail/AddFamilyDetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            };
        }


        #region GetFamilyDetailsByIDTest
        /// <summary>
        /// GetFamilyDetailsByIDTest
        /// </summary> 
        //Data provider, connection string, data table, data access method
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Family.csv", "Family#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Family.csv"), TestMethod]

        public void GetFamilyDetailsByIDTest()
        {
            int empID = Convert.ToInt32(TestContext.DataRow["empID"]);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AsociateFamilyDetail/GetFamilyDetailsByID?empID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };


        }
        #endregion

        #region UpdateFamilyDetailsTest
        /// <summary>
        /// UpdateFamilyDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Family.csv", "Family#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Family.csv"), TestMethod]
        public void UpdateFamilyDetailsTest()
        {

            UserDetails userDetails = new UserDetails();
            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);

            List<RelationDetails> relationsInfoList = new List<RelationDetails>();
            RelationDetails relationsInfoOne = new RelationDetails();
            if (!Convert.IsDBNull(TestContext.DataRow["name"]) || !Convert.IsDBNull(TestContext.DataRow["relations"]) || !Convert.IsDBNull(TestContext.DataRow["dob"]) || !Convert.IsDBNull(TestContext.DataRow["occupation"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
            {
                relationsInfoOne.name = Convert.ToString(TestContext.DataRow["name"]);
                relationsInfoOne.relationship = Convert.ToString(TestContext.DataRow["relations"]);
                relationsInfoOne.dob = Convert.ToDateTime(TestContext.DataRow["dob"]);
                relationsInfoOne.occupation = Convert.ToString(TestContext.DataRow["occupation"]);
                relationsInfoOne.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                relationsInfoList.Add(relationsInfoOne);
            }

            List<EmergencyContactData> emergencyContactDataList = new List<EmergencyContactData>();
            EmergencyContactData emergencyContactDataInfo = new EmergencyContactData();

            if (!Convert.IsDBNull(TestContext.DataRow["contactName"]) || !Convert.IsDBNull(TestContext.DataRow["isPrimary"]) || !Convert.IsDBNull(TestContext.DataRow["relationship"]) || !Convert.IsDBNull(TestContext.DataRow["telephoneNo"]) || !Convert.IsDBNull(TestContext.DataRow["mobileNo"])
                 || !Convert.IsDBNull(TestContext.DataRow["city"]) || !Convert.IsDBNull(TestContext.DataRow["country"]) || !Convert.IsDBNull(TestContext.DataRow["state"]) || !Convert.IsDBNull(TestContext.DataRow["zip"]) || !Convert.IsDBNull(TestContext.DataRow["addressLine1"]) || !Convert.IsDBNull(TestContext.DataRow["addressLine2"]))
            {
                emergencyContactDataInfo.contactName = Convert.ToString(TestContext.DataRow["contactName"]);
                emergencyContactDataInfo.isPrimary = Convert.ToBoolean(TestContext.DataRow["isPrimary"]);
                emergencyContactDataInfo.relationship = Convert.ToString(TestContext.DataRow["relationship"]);
                emergencyContactDataInfo.telephoneNo = Convert.ToString(TestContext.DataRow["telephoneNo"]);
                emergencyContactDataInfo.mobileNo = Convert.ToString(TestContext.DataRow["mobileNo"]);
                emergencyContactDataInfo.city = Convert.ToString(TestContext.DataRow["city"]);
                emergencyContactDataInfo.country = Convert.ToString(TestContext.DataRow["country"]);
                emergencyContactDataInfo.state = Convert.ToString(TestContext.DataRow["state"]);
                emergencyContactDataInfo.zip = Convert.ToString(TestContext.DataRow["zip"]);
                emergencyContactDataInfo.addressLine1 = Convert.ToString(TestContext.DataRow["addressLine1"]);
                emergencyContactDataInfo.addressLine2 = Convert.ToString(TestContext.DataRow["addressLine2"]);
                emergencyContactDataList.Add(emergencyContactDataInfo);
            }

            userDetails.emergencyContactsInfoInfo = emergencyContactDataList;
            userDetails.relationsInfo = relationsInfoList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AsociateFamilyDetail/UpdateFamilyDetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };

        }
        #endregion
    }
}