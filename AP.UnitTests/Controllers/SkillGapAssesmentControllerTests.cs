﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.DomainEntities;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Collections.Generic;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class SkillGapAssesmentControllerTests:BaseControllerTests
    {
        #region GetRequiredProficiencyLevelTest
        /// <summary>
        /// GetRequiredProficiencyLevelTest
        /// </summary>
      [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\SkillGapAssesment.csv", "SkillGapAssesment#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\SkillGapAssesment.csv"), TestMethod]      
        public void GetRequiredProficiencyLevelTest()
        {                     
            int ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            int SkillId = Convert.ToInt32(TestContext.DataRow["SkillId"]);          
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SkillGapAssesment/GetRequiredProficiencyLevel?projectId=" + ProjectId + "&skillId=" + SkillId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }
        #endregion

        #region GetCurrentProficiencyLevelTest
        /// <summary>
        /// GetCurrentProficiencyLevelTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\SkillGapAssesment.csv", "SkillGapAssesment#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\SkillGapAssesment.csv"), TestMethod]
        public void GetCurrentProficiencyLevelTest()
        {                  
            int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
            int SkillId = Convert.ToInt32(TestContext.DataRow["SkillId"]);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SkillGapAssesment/GetCurrentProficiencyLevel?employeeId=" + EmployeeId + "&skillId=" + SkillId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }
        #endregion

        #region GetStatusCodeByCategoryTest
        /// <summary>
        /// GetStatusCodeByCategoryTest
        /// </summary>
        [TestMethod()]
        public void GetStatusCodeByCategoryTest()
        {
           
            string category = "SkillGapAssesment";
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/SkillGapAssesment/GetStatusCodeByCategory?category=" + category ))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }
        #endregion

        #region PostSkillGapAssesmentTest
        /// <summary>
        /// PostSkillGapAssesmentTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\SkillGapAssesment.csv", "SkillGapAssesment#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\SkillGapAssesment.csv"), TestMethod]

        public void PostSkillGapAssesmentTest()
        {
            SkillGapData skillGapData = new SkillGapData();
            skillGapData.AssociateSkillGapId = Convert.ToInt32(TestContext.DataRow["AssociateSkillGapId"]);
            skillGapData.EmpId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
            skillGapData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
            skillGapData.SkillId = Convert.ToInt32(TestContext.DataRow["SkillId"]);
            skillGapData.CompetencyAreaId = Convert.ToInt32(TestContext.DataRow["CompetencyAreaId"]);
            skillGapData.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
            skillGapData.CurrentProficiencyLevelId = Convert.ToInt32(TestContext.DataRow["CurrentProficiencyLevelId"]);
            skillGapData.RequiredProficiencyLevelId = Convert.ToInt32(TestContext.DataRow["RequiredProficiencyLevelId"]);           
            skillGapData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);           
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/SkillGapAssesment/PostSkillGapAssesment", new StringContent(JsonConvert.SerializeObject(skillGapData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            }
        }
        #endregion       
    }
}