﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net;
using System.Configuration;
using System.Data;
using Newtonsoft.Json;
using AP.DomainEntities;
using System.Threading.Tasks;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class AssociateProfessionalDetailControllerTests : BaseControllerTests
    {

        #region GetProfessionalDetailsByIDTest
        /// <summary>
        /// GetProfessionalDetailsByIDTest
        /// </summary>
        [TestMethod()]
        public void GetProfessionalDetailsByIDTest()
        {
            int empID = 318;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateProfessionalDetail/GetProfessionalDetailsByEmployeeID?employeeID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }
        #endregion

        #region DeleteProfessionalDetailsByID
        /// <summary>
        /// DeleteProfessionalDetailsByID
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeMemberShipAndCertificationDetails.csv", "EmployeeMemberShipAndCertificationDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeMemberShipAndCertificationDetails.csv", "TestInput"), TestMethod]
        public void DeleteProfessionalDetailsByID()
        {
            ProfessionalDetails certificationData = new ProfessionalDetails();
            certificationData.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            int ID = certificationData.ID;
            int programType = Convert.ToInt32(TestContext.DataRow["programType"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID, ProgramType
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateProfessionalDetails/DeleteProfessionalDetailsByID?id=" +ID+ "&programTypeID=" + programType, new StringContent(JsonConvert.SerializeObject(certificationData.ID), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region AddCertificationDetailsTest
        /// <summary>
        /// AddCertificationDetailsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeMemberShipAndCertificationDetails.csv", "EmployeeMemberShipAndCertificationDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeMemberShipAndCertificationDetails.csv", "TestInput"), TestMethod]
        public void AddCertificationDetailsTest()
        {
            try
            {
                ProfessionalDetails professionalDetails = new ProfessionalDetails();
                professionalDetails.EmployeeID = Convert.ToInt32(TestContext.DataRow["employeeID"]);
                professionalDetails.institution = TestContext.DataRow["Institution"].ToString();
                professionalDetails.skillGroupID = Convert.ToInt32(TestContext.DataRow["skillGroupID"]);
                professionalDetails.certificationID = Convert.ToInt32(TestContext.DataRow["certificationID"]);
                professionalDetails.validFrom = TestContext.DataRow["validFrom"].ToString();
                professionalDetails.specialization = TestContext.DataRow["specialization"].ToString();
                professionalDetails.validUpto = TestContext.DataRow["validUpto"].ToString();
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateProfessionalDetail/AddCertificationDetails", new StringContent(JsonConvert.SerializeObject(professionalDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                    int EmployeeID = professionalDetails.EmployeeID;
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateProfessionalDetail/GetProfessionalDetailsByEmployeeID?employeeID=" + EmployeeID))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var proficiencyDetails = result.Result;
                            //deserialize to your class
                            List<CommonProficiency> lstProficiency = JsonConvert.DeserializeObject<List<CommonProficiency>>(proficiencyDetails);
                            CommonProficiency professionalData = lstProficiency.Find(proficiency => proficiency.institution.ToLower().Trim() == professionalDetails.institution.ToLower().Trim());
                            Assert.AreEqual(professionalDetails.institution, professionalData.institution);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddMembershipDetailsTest
        /// <summary>
        /// AddMembershipDetailsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeMemberShipAndCertificationDetails.csv", "EmployeeMemberShipAndCertificationDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeMemberShipAndCertificationDetails.csv", "TestInput"), TestMethod]
        public void AddMembershipDetailsTest()
        {
            try
            {
                ProfessionalDetails professionalDetails = new ProfessionalDetails();
                professionalDetails.EmployeeID = Convert.ToInt32(TestContext.DataRow["employeeID"]);
                professionalDetails.institution = TestContext.DataRow["Institution"].ToString();
                professionalDetails.programTitle = TestContext.DataRow["programTitle"].ToString();
                professionalDetails.validFrom = TestContext.DataRow["validFrom"].ToString();
                professionalDetails.specialization = TestContext.DataRow["specialization"].ToString();
                professionalDetails.validUpto = TestContext.DataRow["validUpto"].ToString();
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateProfessionalDetail/AddMembershipDetails", new StringContent(JsonConvert.SerializeObject(professionalDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                    int EmployeeID = professionalDetails.EmployeeID;
                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateProfessionalDetail/GetProfessionalDetailsByEmployeeID?employeeID=" + EmployeeID))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var proficiencyDetails = result.Result;
                            //deserialize to your class
                            List<CommonProficiency> lstProficiency = JsonConvert.DeserializeObject<List<CommonProficiency>>(proficiencyDetails);
                            CommonProficiency proficiencyData = lstProficiency.Find(proficiency => proficiency.institution.ToLower().Trim() == professionalDetails.institution.ToLower().Trim());
                            Assert.AreEqual(professionalDetails.institution, proficiencyData.institution);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateCertificationDetailsTest
        /// <summary>
        /// UpdateCertificationDetailsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeMemberShipAndCertificationDetails.csv", "EmployeeMemberShipAndCertificationDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeMemberShipAndCertificationDetails.csv", "TestInput"), TestMethod]
        public void UpdateCertificationDetailsTest()
        {
            ProfessionalDetails professionalDetails = new ProfessionalDetails();
            professionalDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            professionalDetails.certificationID = Convert.ToInt32(TestContext.DataRow["certificationID"]);
            professionalDetails.validFrom = TestContext.DataRow["validFrom"].ToString();
            professionalDetails.institution = TestContext.DataRow["Institution"].ToString();
            professionalDetails.skillGroupID = Convert.ToInt32(TestContext.DataRow["skillGroupID"]);
            professionalDetails.specialization = TestContext.DataRow["specialization"].ToString();
            professionalDetails.validUpto = TestContext.DataRow["validUpto"].ToString();
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //2. Update  
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateProfessionalDetail/UpdateCertificationDetails", new StringContent(JsonConvert.SerializeObject(professionalDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //GET
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["employeeID"]);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateProfessionalDetail/GetProfessionalDetailsByEmployeeID?employeeID=" + EmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var proficiencyDetails = result.Result;
                        //deserialize to your class
                        List<CommonProficiency> lstProficiency = JsonConvert.DeserializeObject<List<CommonProficiency>>(proficiencyDetails);
                        CommonProficiency proficiencyData = lstProficiency.Find(proficiency => proficiency.institution.ToLower().Trim() == professionalDetails.institution.ToLower().Trim());
                        Assert.AreEqual(professionalDetails.institution, proficiencyData.institution);
                    }
                }
            };
        }
        #endregion

        #region UpdateMembershipDetailsTest
        /// <summary>
        /// UpdateMembershipDetailsTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeMemberShipAndCertificationDetails.csv", "EmployeeMemberShipAndCertificationDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeMemberShipAndCertificationDetails.csv", "TestInput"), TestMethod]
        public void UpdateMembershipDetailsTest()
        {
            ProfessionalDetails professionalDetails = new ProfessionalDetails();
            professionalDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            professionalDetails.validFrom = TestContext.DataRow["validFrom"].ToString();
            professionalDetails.institution = TestContext.DataRow["Institution"].ToString();
            professionalDetails.specialization = TestContext.DataRow["specialization"].ToString();
            professionalDetails.programTitle = TestContext.DataRow["programTitle"].ToString();
            professionalDetails.validUpto = TestContext.DataRow["validUpto"].ToString();
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //2. Update  
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateProfessionalDetail/UpdateMembershipDetails", new StringContent(JsonConvert.SerializeObject(professionalDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //GET
                int EmployeeID = Convert.ToInt32(TestContext.DataRow["employeeID"]);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateProfessionalDetail/GetProfessionalDetailsByEmployeeID?employeeID=" + EmployeeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var proficiencyDetails = result.Result;
                        //deserialize to your class
                        List<CommonProficiency> lstProficiency = JsonConvert.DeserializeObject<List<CommonProficiency>>(proficiencyDetails);
                        CommonProficiency proficiencyData = lstProficiency.Find(proficiency => proficiency.institution.ToLower().Trim() == professionalDetails.institution.ToLower().Trim());
                        Assert.AreEqual(professionalDetails.institution, proficiencyData.institution);
                    }
                }
            };
        }
        #endregion
    }
}