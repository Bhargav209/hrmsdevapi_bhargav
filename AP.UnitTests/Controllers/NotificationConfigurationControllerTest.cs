﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;
using AP.API;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class NotificationConfigurationControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region GetNotificationConfigurationDetailsByNotificationTypeIdTest
        /// <summary>
        /// GetNotificationConfigurationDetailsByNotificationTypeIdTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationConfigurationInfo.csv", "NotificationConfigurationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationConfigurationInfo.csv"), TestMethod]

        public void GetNotificationConfigurationDetailsByNotificationTypeIdTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int NotificationTypeID = Convert.ToInt32(TestContext.DataRow["NotificationTypeID"]);
                string EmailFrom = Convert.ToString(TestContext.DataRow["EmailFrom"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/NotificationConfiguration/GetNotificationConfigurationDetailsByNotificationTypeId?notificationTypeID=" + NotificationTypeID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var ConfigurationData = result.Result;
                        //deserialize to your class
                        EmailNotificationConfiguration ConfigutarionDetails = JsonConvert.DeserializeObject<EmailNotificationConfiguration>(ConfigurationData);
                        Assert.AreEqual(ConfigutarionDetails.EmailFrom, EmailFrom);
                    }
                }

            };
        }
        #endregion

        #region CreateNotificationConfigurationTest
        /// <summary>
        /// CreateNotificationConfigurationTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationConfigurationInfo.csv", "NotificationConfigurationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationConfigurationInfo.csv", "TestInput"), TestMethod]
        public void CreateNotificationConfigurationTest()
        {
            try
            {

                EmailNotificationConfiguration notificationData = new EmailNotificationConfiguration();
                notificationData.NotificationTypeID = Convert.ToInt32(TestContext.DataRow["NotificationTypeID"]);
                notificationData.CategoryId = Convert.ToInt32(TestContext.DataRow["CategoryId"]);
                notificationData.EmailFrom = TestContext.DataRow["EmailFrom"].ToString();
                notificationData.EmailTo = TestContext.DataRow["EmailTo"].ToString();
                notificationData.EmailCC = TestContext.DataRow["EmailCC"].ToString();
                notificationData.EmailSubject = "Testsubject";
                notificationData.EmailContent= "TestContent";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/NotificationConfiguration/CreateNotificationConfiguration", new StringContent(JsonConvert.SerializeObject(notificationData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                    
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateNotificationConfigurationTest
        /// <summary>
        /// UpdateNotificationConfigurationTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationTypeInfo.csv", "NotificationTypeInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationTypeInfo.csv", "TestInput"), TestMethod]
        public void UpdateNotificationConfigurationTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                EmailNotificationConfiguration notificationData = new EmailNotificationConfiguration();
                notificationData.NotificationTypeID = Convert.ToInt32(TestContext.DataRow["NotificationTypeID"]);
                notificationData.CategoryId = Convert.ToInt32(TestContext.DataRow["CategoryId"]);
                notificationData.EmailFrom = TestContext.DataRow["EmailFrom"].ToString();
                notificationData.EmailTo = TestContext.DataRow["EmailTo"].ToString();
                notificationData.EmailCC = TestContext.DataRow["EmailCC"].ToString();
                notificationData.EmailSubject = TestContext.DataRow["Subject"].ToString();
                notificationData.EmailContent = TestContext.DataRow["Body"].ToString();

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/NotificationConfiguration/UpdateNotificationConfiguration", new StringContent(JsonConvert.SerializeObject(notificationData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region GetEmployeeWorkEmailsTest
        /// <summary>
        /// GetEmployeeWorkEmailsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationConfigurationInfo.csv", "NotificationConfigurationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationConfigurationInfo.csv"), TestMethod]

        public void GetEmployeeWorkEmailsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string SearchString = Convert.ToString(TestContext.DataRow["SearchString"]);
                string EmailFrom = Convert.ToString(TestContext.DataRow["EmailFrom"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/NotificationConfiguration/GetEmployeeWorkEmails?searchString=" + SearchString))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var EmailData = result.Result;
                        //deserialize to your class
                        List<EmailNotificationConfiguration> lstEmailData = JsonConvert.DeserializeObject<List<EmailNotificationConfiguration>>(EmailData);
                        EmailNotificationConfiguration emails = lstEmailData.Find(notification => notification.EmailFrom.ToLower().Trim() == EmailFrom.ToLower().Trim());
                        Assert.AreEqual(EmailFrom, emails.EmailFrom);
                    }
                }

            };
        }
        #endregion

        #region GetNotificationTypeByCategory
        /// <summary>
        /// GetEmployeeWorkEmailsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationConfigurationInfo.csv", "NotificationConfigurationInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationConfigurationInfo.csv"), TestMethod]

        public void GetNotificationTypeByCategoryTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int categoryId = Convert.ToInt32(TestContext.DataRow["CategoryId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/NotificationConfiguration/GetNotificationType?categoryId=" + categoryId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var NotificationType = result.Result;
                        //deserialize to your class
                        List<NotificationType> lstNotificationType = JsonConvert.DeserializeObject<List<NotificationType>>(NotificationType);
                        Assert.AreEqual(lstNotificationType, categoryId);
                    }
                }

            };
        }
        #endregion
    }
}
