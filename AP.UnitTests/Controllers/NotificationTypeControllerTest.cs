﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;
using AP.API;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class KRAAspectsControllerTest : BaseControllerTests
    {
        Random rnd = new Random(1);

        #region GetNotificationTypeTest
        /// <summary>
        /// GetNotificationTypeTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationTypeInfo.csv", "NotificationTypeInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationTypeInfo.csv"), TestMethod]
        public void GetNotificationTypeTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string NotificationType = Convert.ToString(TestContext.DataRow["NotificationType"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/NotificationType/GetNotificationType"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var notificationTypeData = result.Result;
                        //deserialize to your class
                        List<NotificationTypeDetails> lstNotificationTypeData = JsonConvert.DeserializeObject<List<NotificationTypeDetails>>(notificationTypeData);
                        NotificationTypeDetails notifications = lstNotificationTypeData.Find(notification => notification.NotificationType.ToLower().Trim() == NotificationType.ToLower().Trim());
                        Assert.AreEqual(NotificationType, notifications.NotificationType);
                    }
                }

            };
        }
        #endregion

        #region CreateNotificationTypeTest
        /// <summary>
        /// CreateNotificationTypeTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationTypeInfo.csv", "NotificationTypeInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationTypeInfo.csv", "TestInput"), TestMethod]
        public void CreateNotificationTypeTest()
        {
            try
            {

                NotificationTypeDetails notificationData = new NotificationTypeDetails();
                notificationData.NotificationType = TestContext.DataRow["NotificationType"].ToString();
                notificationData.Description = TestContext.DataRow["Description"].ToString();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/NotificationType/CreateNotificationType", new StringContent(JsonConvert.SerializeObject(notificationData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/NotificationType/GetNotificationType"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var notificationTypeData = result.Result;
                            //deserialize to your class
                            List<NotificationTypeDetails> lstNotificationTypeData = JsonConvert.DeserializeObject<List<NotificationTypeDetails>>(notificationTypeData);
                            NotificationTypeDetails notifications = lstNotificationTypeData.Find(notification => notification.NotificationType.ToLower().Trim() == notificationData.NotificationType.ToLower().Trim());
                            Assert.AreEqual(notificationData.NotificationType, notifications.NotificationType);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateNotificationTypeTest
        /// <summary>
        /// UpdateNotificationTypeTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationTypeInfo.csv", "NotificationTypeInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationTypeInfo.csv", "TestInput"), TestMethod]
        public void UpdateNotificationTypeTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                NotificationTypeDetails notificationData = new NotificationTypeDetails();
                notificationData.NotificationTypeID = Convert.ToInt32(TestContext.DataRow["NotificationTypeID"]);
                notificationData.NotificationType = TestContext.DataRow["NotificationType"].ToString();
                notificationData.Description = TestContext.DataRow["Description"].ToString();

                //2. Update 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/NotificationType/UpdateNotificationType", new StringContent(JsonConvert.SerializeObject(notificationData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
                //3. Get & Assert
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/NotificationType/GetNotificationType"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var notificationTypeData = result.Result;
                        //deserialize to your class
                        List<NotificationTypeDetails> lstNotificationTypeData = JsonConvert.DeserializeObject<List<NotificationTypeDetails>>(notificationTypeData);
                        NotificationTypeDetails notifications = lstNotificationTypeData.Find(notification => notification.NotificationType.ToLower().Trim() == notificationData.NotificationType.ToLower().Trim());
                        Assert.AreEqual(notificationData.NotificationType, notifications.NotificationType);
                    }
                }
            };
        }
        #endregion

        #region DeleteNotificationTypeByTypeIdTest
        /// <summary>
        /// DeleteNotificationTypeByTypeIdTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\NotificationTypeInfo.csv", "NotificationTypeInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\NotificationTypeInfo.csv", "TestInput"), TestMethod]
        public void DeleteNotificationTypeByTypeIdTest()
        {
            int NotificationTypeId = 23;
            int categoryId = 1;
            NotificationType notification = new NotificationType();
            notification.DeleteNotificationTypeByTypeId(NotificationTypeId,categoryId);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/NotificationType/DeleteNotificationTypeByTypeId", new StringContent(JsonConvert.SerializeObject(notification), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion
    }
}
