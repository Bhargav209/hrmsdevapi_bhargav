﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using AP.DomainEntities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.Net;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class RoleControllerTest : BaseControllerTests
    {
        #region CreateRoleTest
        /// <summary>
        /// CreateRoleTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Roles.csv", "Roles#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Roles.csv", "TestInput"), TestMethod]
        public void CreateRoleTest()
        {
            try
            {

                RoleData roleData = new RoleData();
                roleData.SGRoleID = Convert.ToInt32(TestContext.DataRow["SGRoleID"]);
                roleData.PrefixID = 2;
                roleData.SuffixID = 2;//Convert.ToInt32(TestContext.DataRow["SuffixID"]);
                roleData.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                roleData.RoleDescription = Convert.ToString(TestContext.DataRow["RoleDescription"]);
                roleData.KeyResponsibilities = Convert.ToString(TestContext.DataRow["KeyResponsibilities"]);
                roleData.EducationQualification = Convert.ToString(TestContext.DataRow["EducationQualification"]);
                //roleData.RoleName = Convert.ToString(TestContext.DataRow["RoleName"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Role/CreateRole", new StringContent(JsonConvert.SerializeObject(roleData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Role/GetRoles"))
                    {
                        using (HttpResponseMessage response = client.SendAsync(request).Result)
                        {
                            Task<string> result = response.Content.ReadAsStringAsync();
                            var RolesData = result.Result;
                            //deserialize to your class
                            List<RoleData> lstRolesData = JsonConvert.DeserializeObject<List<RoleData>>(RolesData);
                            RoleData Role = lstRolesData.Find(roles => roles.RoleName.ToLower().Trim() == roleData.RoleName.ToLower().Trim());
                            Assert.AreEqual(roleData.RoleName, Role.RoleName);
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRolesTest
        /// <summary>
        /// GetRolesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Roles.csv", "Roles#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Roles.csv"), TestMethod]
        public void GetRolesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string RoleName = Convert.ToString(TestContext.DataRow["RoleName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Role/GetRoles"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var RolesData = result.Result;
                        //deserialize to your class
                        List<RoleData> lstRolesData = JsonConvert.DeserializeObject<List<RoleData>>(RolesData);
                        RoleData Role = lstRolesData.Find(roles => roles.RoleName.ToLower().Trim() == RoleName.ToLower().Trim());
                        Assert.AreEqual(RoleName, Role.RoleName);
                    }
                }

            };
        }
        #endregion

        #region GetRoleSuffixAndPrefixTest
        /// <summary>
        /// GetRoleSuffixAndPrefixTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Roles.csv", "Roles#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Roles.csv"), TestMethod]
        public void GetRoleSuffixAndPrefixTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string RoleName = Convert.ToString(TestContext.DataRow["RoleName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Role/GetRoleSuffixAndPrefix"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var RolesData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstRolesData = JsonConvert.DeserializeObject<List<GenericType>>(RolesData);
                        GenericType Role = lstRolesData.Find(roles => roles.Name.ToLower().Trim() == RoleName.ToLower().Trim());
                        Assert.AreEqual(RoleName, Role.Name);
                    }
                }

            };
        }
        #endregion

        #region GetRolesByDepartmentIDTest
        /// <summary>
        /// GetRolesByDepartmentIDTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\Roles.csv", "Roles#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\Roles.csv"), TestMethod]

        public void GetRolesByDepartmentIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int DepartmentID = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);
                string Name = Convert.ToString(TestContext.DataRow["Name"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Role/GetRolesByDepartmentID?DepartmentId=" + DepartmentID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var roleData = result.Result;
                        //deserialize to your class
                        List<GenericType> lstRoleData = JsonConvert.DeserializeObject<List<GenericType>>(roleData);
                        GenericType role = lstRoleData.Find(roles => roles.Name.ToLower() == Name.ToLower());
                        Assert.AreEqual(Name, role.Name);
                    }
                }

            };
        }
        #endregion
    }
}
