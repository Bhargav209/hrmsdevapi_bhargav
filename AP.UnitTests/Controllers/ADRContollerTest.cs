﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class ADRContollerTest : BaseControllerTests
    {
        //#region GetEmployeeKRAByEmployeeIdTest
        ///// <summary>
        ///// GetEmployeeKRAByEmployeeIdTest
        ///// </summary> 
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ADRData.csv", "ADRData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ADRData.csv"), TestMethod]
        //public void GetEmployeeKRAByEmployeeIdTest()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        string KRAAspectName = Convert.ToString(TestContext.DataRow["KRAAspectName"]);
        //        int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
        //        int FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
        //        int ADRCycleID = Convert.ToInt32(TestContext.DataRow["ADRCycleID"]);

        //        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //        using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ADR/GetEmployeeKRAByEmployeeId?EmployeeId=" + EmployeeId + "&&FinancialYearId=" + FinancialYearId + "&&ADRCycleID=" + ADRCycleID))
        //        {
        //            using (HttpResponseMessage response = client.SendAsync(request).Result)
        //            {
        //                Task<string> result = response.Content.ReadAsStringAsync();
        //                var KRAAspectData = result.Result;
        //                //deserialize to your class
        //                List<KRAAspectData> lstKRAAspectData = JsonConvert.DeserializeObject<List<KRAAspectData>>(KRAAspectData);
        //                KRAAspectData KRA = lstKRAAspectData.Find(kra => kra.KRAAspectName.ToLower().Trim() == KRAAspectName.ToLower().Trim());
        //                Assert.AreEqual(KRAAspectName, KRA.KRAAspectName);
        //            }
        //        }

        //    };
        //}
        //#endregion

        //#region GetCommentsByEmployeeIdTest
        ///// <summary>
        ///// GetCommentsByEmployeeIdTest
        ///// </summary> 
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ADRData.csv", "ADRData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ADRData.csv"), TestMethod]
        //public void GetCommentsByEmployeeIdTest()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        string CriticalTasksPerformed = Convert.ToString(TestContext.DataRow["CriticalTasksPerformed"]);
        //        int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
        //        int FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
        //        int ADRCycleID = Convert.ToInt32(TestContext.DataRow["ADRCycleID"]);

        //        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //        using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ADR/GetCommentsByEmployeeId?EmployeeId=" + EmployeeId + "&&FinancialYearId=" + FinancialYearId + "&&ADRCycleID=" + ADRCycleID))
        //        {
        //            using (HttpResponseMessage response = client.SendAsync(request).Result)
        //            {
        //                Task<string> result = response.Content.ReadAsStringAsync();
        //                var KRAAspectData = result.Result;
        //                //deserialize to your class
        //                List<KRAAspectData> lstKRAAspectData = JsonConvert.DeserializeObject<List<KRAAspectData>>(KRAAspectData);
        //                KRAAspectData KRA = lstKRAAspectData.Find(kra => kra.CriticalTasksPerformed.ToLower().Trim() == CriticalTasksPerformed.ToLower().Trim());
        //                Assert.AreEqual(CriticalTasksPerformed, KRA.CriticalTasksPerformed);
        //            }
        //        }

        //    };
        //}
        //#endregion

        //#region GetMetricsAndCommentsByAspectIdTest
        ///// <summary>
        ///// GetMetricsAndCommentsByAspectIdTest
        ///// </summary> 
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ADRData.csv", "ADRData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ADRData.csv"), TestMethod]
        //public void GetMetricsAndCommentsByAspectIdTest()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        string KRAMetric = Convert.ToString(TestContext.DataRow["KRAMetric"]);
        //        int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
        //        int FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
        //        int ADRCycleID = Convert.ToInt32(TestContext.DataRow["ADRCycleID"]);
        //        int KRAAspectId = Convert.ToInt32(TestContext.DataRow["KRAAspectId"]);

        //        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //        using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ADR/GetMetricsAndCommentsByAspectId?EmployeeId=" + EmployeeId + "&&FinancialYearId=" + FinancialYearId + "&&ADRCycleID=" + ADRCycleID + "&&KRAAspectId=" + KRAAspectId))
        //        {
        //            using (HttpResponseMessage response = client.SendAsync(request).Result)
        //            {
        //                Task<string> result = response.Content.ReadAsStringAsync();
        //                var KRAAspectData = result.Result;
        //                //deserialize to your class
        //                List<KRAAspectData> lstKRAAspectData = JsonConvert.DeserializeObject<List<KRAAspectData>>(KRAAspectData);
        //                KRAAspectData KRA = lstKRAAspectData.Find(kra => kra.KRAMetric.ToLower().Trim() == KRAMetric.ToLower().Trim());
        //                Assert.AreEqual(KRAMetric, KRA.KRAMetric);
        //            }
        //        }

        //    };
        //}
        //#endregion

        //#region GetCommentsByAspectAndMetricIdTest
        ///// <summary>
        ///// GetCommentsByAspectAndMetricIdTest
        ///// </summary> 
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ADRData.csv", "ADRData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ADRData.csv"), TestMethod]
        //public void GetCommentsByAspectAndMetricIdTest()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        string CriticalTasksPerformed = Convert.ToString(TestContext.DataRow["CriticalTasksPerformed"]);
        //        int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
        //        int FinancialYearId = Convert.ToInt32(TestContext.DataRow["FinancialYearId"]);
        //        int ADRCycleID = Convert.ToInt32(TestContext.DataRow["ADRCycleID"]);
        //        int KRAAspectId = Convert.ToInt32(TestContext.DataRow["KRAAspectId"]);
        //        int KRAMetricId = Convert.ToInt32(TestContext.DataRow["KRAMetricId"]);

        //        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
        //        using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ADR/GetCommentsByAspectAndMetricId?EmployeeId=" + EmployeeId + "&&FinancialYearId=" + FinancialYearId + "&&ADRCycleID=" + ADRCycleID + "&&KRAAspectId=" + KRAAspectId + "&&KRAMetricId=" + KRAMetricId))
        //        {
        //            using (HttpResponseMessage response = client.SendAsync(request).Result)
        //            {
        //                Task<string> result = response.Content.ReadAsStringAsync();
        //                var KRAAspectData = result.Result;
        //                //deserialize to your class
        //                List<KRAAspectData> lstKRAAspectData = JsonConvert.DeserializeObject<List<KRAAspectData>>(KRAAspectData);
        //                KRAAspectData KRA = lstKRAAspectData.Find(kra => kra.CriticalTasksPerformed.ToLower().Trim() == CriticalTasksPerformed.ToLower().Trim());
        //                Assert.AreEqual(CriticalTasksPerformed, KRA.CriticalTasksPerformed);
        //            }
        //        }

        //    };
        //}
        //#endregion
    }
}
