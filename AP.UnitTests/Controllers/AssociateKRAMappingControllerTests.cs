﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using AP.Services.Controllers.Tests;
using System.Threading.Tasks;
using AP.DomainEntities;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class AssociateKRAMappingControllerTests : BaseControllerTests
    {
        Random rnd = new Random(1);        

        #region GetApprovedKRARolesTest
        /// <summary>
        /// GetApprovedKRARolesTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAMapping.csv", "KRAMapping#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAMapping.csv"), TestMethod]

        public void GetApprovedKRARolesTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int DepartmentID =  Convert.ToInt32(TestContext.DataRow["DepartmentID"]);
                int FinancialYearID = Convert.ToInt32(TestContext.DataRow["FinancialYearID"]);
                string RoleName = Convert.ToString(TestContext.DataRow["RoleName"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateKRAMapping/GetApprovedKRARoles?departmentID=" + DepartmentID + "&&financialYearID=" + FinancialYearID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var roleData = result.Result;
                        //deserialize to your class
                        List<GenericType> associateList = JsonConvert.DeserializeObject<List<GenericType>>(roleData);
                        GenericType names = associateList.Find(name => name.Name.ToLower() == RoleName.ToLower());
                        Assert.AreEqual(RoleName, names.Name);
                    }
                }

            };
        }
        #endregion

        #region GenerateKRAPdfForAllAssociates
        /// <summary>
        /// GenerateKRAPdfForAllAssociates
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\CustomKRAs.csv", "CustomKRAs#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\CustomKRAs.csv"), TestMethod]

        public void GenerateKRAPdfForAllAssociatesTest()
        {
            using (HttpClient client = new HttpClient())
            {                

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateKRAMapping/GenerateKRAPdfForAllAssociates"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var kraData = result.Result;                        
                    }
                }

            };
        }
        #endregion

        #region UpdateAllocationRoleIdTest
        /// <summary>
        /// UpdateAllocationRoleIdTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\KRAMapping.csv", "KRAMapping#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\KRAMapping.csv", "TestInput"), TestMethod]
        public void UpdateAllocationRoleIdTest()
        {
            try
            {
                AssociateRoleMappingData roleMapping = new AssociateRoleMappingData();
                List<int> id = new List<int>();
                id.Add(236);
                roleMapping.IDs = id;
                roleMapping.RoleMasterId = Convert.ToInt32(TestContext.DataRow["RoleMasterId"]);
                roleMapping.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                roleMapping.DepartmentId = Convert.ToInt32(TestContext.DataRow["DepartmentId"]);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateKRAMapping/UpdateAssociateRole", new StringContent(JsonConvert.SerializeObject(roleMapping).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
