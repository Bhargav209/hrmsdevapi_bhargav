﻿using AP.Services.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class AssociateSkillsControllerTests : BaseControllerTests
    {
        #region MasterSkillMethods

        #region CreateSkillsMasterTest
        /// <summary>
        /// CreateSkillsMasterTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv", "TestInput"), TestMethod]
        public void CreateSkillsMasterTest()
        {
            SkillData skillData = new SkillData();

            if (!Convert.IsDBNull(TestContext.DataRow["skillCode"]) || !Convert.IsDBNull(TestContext.DataRow["skillDescription"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]) || !Convert.IsDBNull(TestContext.DataRow["isApproved"]))
            {
                skillData.SkillCode = Convert.ToString(TestContext.DataRow["skillCode"]);
                skillData.SkillName = Convert.ToString(TestContext.DataRow["skillName"]);
                skillData.Name = Convert.ToString(TestContext.DataRow["Name"]);
                skillData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                skillData.IsApproved = Convert.ToBoolean(TestContext.DataRow["isApproved"]);
                skillData.SkillDescription = Convert.ToString(TestContext.DataRow["skillDescription"]);
                skillData.CompetencyAreaID = Convert.ToInt32(TestContext.DataRow["CompetencyAreaId"]);
                skillData.SkillGroupID = Convert.ToInt32(TestContext.DataRow["SkillGroupID"]);
            }
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/CreateSkillsMaster/", new StringContent(JsonConvert.SerializeObject(skillData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region GetSkillsDataTest
        /// <summary>
        /// GetSkillsDataTest
        /// </summary>
        [TestMethod()]
        public void GetSkillsDataTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetSkillsData"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region UpdateSkillMasterDetailsTest
        /// <summary>
        /// UpdateSkillMasterDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv", "TestInput"), TestMethod]
        public void UpdateSkillMasterDetailsTest()
        {
            SkillData skillData = new SkillData();

            if (!Convert.IsDBNull(TestContext.DataRow["skillCode"]) || !Convert.IsDBNull(TestContext.DataRow["skillDescription"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]) || !Convert.IsDBNull(TestContext.DataRow["isApproved"]))
            {
                skillData.SkillId = Convert.ToInt32(TestContext.DataRow["skillID"]);
                skillData.SkillCode = Convert.ToString(TestContext.DataRow["skillCode"]);
                skillData.SkillDescription = Convert.ToString(TestContext.DataRow["skillDescription"]);
                skillData.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                skillData.IsApproved = Convert.ToBoolean(TestContext.DataRow["isApproved"]);
                skillData.SkillDescription = Convert.ToString(TestContext.DataRow["skillDescription"]);
                skillData.SkillGroupID = Convert.ToInt32(TestContext.DataRow["SkillGroupID"]);
                skillData.CompetencyAreaID = Convert.ToInt32(TestContext.DataRow["CompetencyAreaId"]);
                skillData.Name = Convert.ToString(TestContext.DataRow["Name"]);
            }

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/UpdateSkillMasterDetails/", new StringContent(JsonConvert.SerializeObject(skillData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }

        #endregion

        #region GetSkillsTest
        /// <summary>
        /// GetSkillsTest
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv"), TestMethod]

        public void GetSkillsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                string skillName = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["skillName"].ToString()))
                {
                    skillName = TestContext.DataRow["skillName"].ToString();
                }
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetSkills"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var skillData = result.Result;
                        //deserialize to your class
                        List<SkillData> skills = JsonConvert.DeserializeObject<List<SkillData>>(skillData);
                        SkillData skillDetail = skills.Find(skill => skill.SkillName.ToLower().Trim() == skillName.ToLower().Trim());
                        Assert.AreEqual(skillName, skillDetail.SkillName);
                    }
                }

            };
        }
        #endregion

        #endregion

        #region AssociateSkillandProjectTabTestMethods        

        #region AddSkillAndProjectDetailsTest
        /// <summary>
        /// AddSkillAndProjectDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeSkillsAndProjectDetails.csv", "EmployeeSkillsAndProjectDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeSkillsAndProjectDetails.csv", "TestInput"), TestMethod]
        public void AddSkillAndProjectDetailsTest()
        {
            UserDetails userDetails = new UserDetails();

            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);

            List<EmployeeSkillDetails> employeeSkillDetailsList = new List<EmployeeSkillDetails>();
            EmployeeSkillDetails employeeSkillDetails = new EmployeeSkillDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["skillID"]) || !Convert.IsDBNull(TestContext.DataRow["experience"]) || !Convert.IsDBNull(TestContext.DataRow["proficiencyLevel"])
               || !Convert.IsDBNull(TestContext.DataRow["isPrimary"]))
            {
                employeeSkillDetails.skillID = Convert.ToInt32(TestContext.DataRow["skillID"]);
                employeeSkillDetails.experience = Convert.ToInt32(TestContext.DataRow["experience"]);
                employeeSkillDetails.proficiencyLevelId = Convert.ToInt32(TestContext.DataRow["proficiencyLevel"]);
                employeeSkillDetails.isPrimary = Convert.ToBoolean(TestContext.DataRow["isPrimary"]);
                employeeSkillDetailsList.Add(employeeSkillDetails);
            }

            List<EmployeeProjectDetails> employeeProjectDetailsList = new List<EmployeeProjectDetails>();
            EmployeeProjectDetails employeeProjectDetails = new EmployeeProjectDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["organizationName"]) || !Convert.IsDBNull(TestContext.DataRow["projectName"]) || !Convert.IsDBNull(TestContext.DataRow["role"])
               || !Convert.IsDBNull(TestContext.DataRow["duration"]) || !Convert.IsDBNull(TestContext.DataRow["keyAchievement"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
            {

                employeeProjectDetails.organizationName = Convert.ToString(TestContext.DataRow["organizationName"]);
                employeeProjectDetails.projectName = Convert.ToString(TestContext.DataRow["projectName"]);
                employeeProjectDetails.roleName = Convert.ToString(TestContext.DataRow["role"]);
                employeeProjectDetails.duration = Convert.ToInt32(TestContext.DataRow["duration"]);
                employeeProjectDetails.keyAchievement = Convert.ToString(TestContext.DataRow["keyAchievement"]);
                employeeProjectDetails.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                employeeProjectDetailsList.Add(employeeProjectDetails);
            }

            userDetails.skills = employeeSkillDetailsList;
            userDetails.projects = employeeProjectDetailsList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/AddSkillsAndProjects", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            };
        }
        #endregion

        #region GetAssociateSkillsByIDTest
        /// <summary>
        /// GetAssociateSkillsByIDTest
        /// </summary>
        [TestMethod()]
        public void GetAssociateSkillsByIDTest()
        {
            int empID = 6;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetAssociateSkillsByID?empID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetAssociateProjectDetailsByIDTest
        /// <summary>
        /// GetAssociateProjectDetailsByIDTest
        /// </summary>
        [TestMethod()]
        public void GetAssociateProjectDetailsByIDTest()
        {
            int empID = 12;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetAssociateProjectDetailsByID?empID=" + empID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }
        #endregion

        #region UpdateSkillAndProjectDetailsTest
        /// <summary>
        /// UpdateSkillAndProjectDetailsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\EmployeeSkillsAndProjectDetails.csv", "EmployeeSkillsAndProjectDetails#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\EmployeeSkillsAndProjectDetails.csv", "TestInput"), TestMethod]
        public void UpdateSkillAndProjectDetailsTest()
        {
            UserDetails userDetails = new UserDetails();

            userDetails.empID = Convert.ToInt32(TestContext.DataRow["empID"]);

            List<EmployeeSkillDetails> employeeSkillDetailsList = new List<EmployeeSkillDetails>();

            EmployeeSkillDetails employeeSkillDetails = new EmployeeSkillDetails();
            if (!Convert.IsDBNull(TestContext.DataRow["ID"]) || !Convert.IsDBNull(TestContext.DataRow["skillID"]) || !Convert.IsDBNull(TestContext.DataRow["experience"]) || !Convert.IsDBNull(TestContext.DataRow["proficiencyLevel"])
               || !Convert.IsDBNull(TestContext.DataRow["isPrimary"]))
            {
                employeeSkillDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
                employeeSkillDetails.skillID = Convert.ToInt32(TestContext.DataRow["skillID"]);
                employeeSkillDetails.experience = Convert.ToInt32(TestContext.DataRow["experience"]);
                employeeSkillDetails.proficiencyLevelId = Convert.ToInt32(TestContext.DataRow["proficiencyLevel"]);
                employeeSkillDetails.isPrimary = Convert.ToBoolean(TestContext.DataRow["isPrimary"]);
                employeeSkillDetailsList.Add(employeeSkillDetails);
            }

            List<EmployeeProjectDetails> employeeProjectDetailsList = new List<EmployeeProjectDetails>();

            EmployeeProjectDetails employeeProjectDetails = new EmployeeProjectDetails();

            if (!Convert.IsDBNull(TestContext.DataRow["organizationName"]) || !Convert.IsDBNull(TestContext.DataRow["projectName"]) || !Convert.IsDBNull(TestContext.DataRow["role"])
               || !Convert.IsDBNull(TestContext.DataRow["duration"]) || !Convert.IsDBNull(TestContext.DataRow["keyAchievement"]) || !Convert.IsDBNull(TestContext.DataRow["isActive"]))
            {
                employeeProjectDetails.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
                employeeProjectDetails.organizationName = Convert.ToString(TestContext.DataRow["organizationName"]);
                employeeProjectDetails.projectName = Convert.ToString(TestContext.DataRow["projectName"]);
                employeeProjectDetails.roleName = Convert.ToString(TestContext.DataRow["role"]);
                employeeProjectDetails.duration = Convert.ToInt32(TestContext.DataRow["duration"]);
                employeeProjectDetails.keyAchievement = Convert.ToString(TestContext.DataRow["keyAchievement"]);
                employeeProjectDetails.IsActive = Convert.ToBoolean(TestContext.DataRow["isActive"]);
                employeeProjectDetailsList.Add(employeeProjectDetails);
            }

            userDetails.skills = employeeSkillDetailsList;
            userDetails.projects = employeeProjectDetailsList;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/UpdateSkillAndProjectDetails", new StringContent(JsonConvert.SerializeObject(userDetails).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }

            };
        }
        #endregion

        #endregion

        #region new skills related methods
        #region GetCompetencyArea
        /// <summary>
        /// controller test method for competency area fetching
        /// </summary>
        [TestMethod()]
        public void GetCompetencyAreaTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetCompetencyArea"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };
        }

        #endregion

        #region GetCompetencySkills
        /// <summary>
        /// 
        /// </summary>
        [TestMethod()]
        public void GetCompetencySkillsTest()
        {
            int roleID = 12;
            int competencyAreaID = 1;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetCompetencySkills?roleID=" + roleID + "&competencyAreaID=" + competencyAreaID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };

        }
        #endregion

        #region GetProficiencyLevel
        /// <summary>
        /// to get proficiency level for the new skills config page
        /// </summary>
        [TestMethod()]
        public void GetProficiencyLevelTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetProficiencyLevel"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            };

        }
        #endregion
        #endregion

        #region CreateEmployeeSkillsTest
        /// <summary>
        /// CreateEmployeeSkillsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv", "TestInput"), TestMethod]
        public void AddAssociateSkillsTest()
        {
            EmployeeSkillDetails skillData = new EmployeeSkillDetails();
            //skillData.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
            skillData.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
            skillData.CompetencyAreaID = 1;
            skillData.SkillGroupID = Convert.ToInt32(TestContext.DataRow["SkillGroupID"]);
            skillData.skillID = Convert.ToInt32(TestContext.DataRow["skillID"]);
            skillData.proficiencyLevelId = Convert.ToInt32(TestContext.DataRow["proficiencyLevelId"]);
            skillData.LastUsed = Convert.ToInt32(TestContext.DataRow["LastUsed"]);
            skillData.isPrimary = Convert.ToBoolean(TestContext.DataRow["isPrimary"]);
            skillData.experience = Convert.ToInt32(TestContext.DataRow["experience"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/AddAssociateSkills/", new StringContent(JsonConvert.SerializeObject(skillData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region SubmitAssociateSkillsTest
        /// <summary>
        /// CreateEmployeeSkillsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv", "TestInput"), TestMethod]
        public void SubmitAssociateSkillsTest()
        {
            SkillDetails skillData = new SkillDetails();
         //   skillData.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
            skillData.ID = 56;
            skillData.empID = Convert.ToInt32(TestContext.DataRow["empID"]);
            skillData.CompetencyAreaID = 1;
            skillData.SkillGroupID = Convert.ToInt32(TestContext.DataRow["SkillGroupID"]);
            skillData.skillID = Convert.ToInt32(TestContext.DataRow["skillID"]);
            skillData.proficiencyLevelId = Convert.ToInt32(TestContext.DataRow["proficiencyLevelId"]);
            skillData.LastUsed = Convert.ToInt32(TestContext.DataRow["LastUsed"]);
            skillData.isPrimary = Convert.ToBoolean(TestContext.DataRow["isPrimary"]);
            skillData.experience = Convert.ToInt32(TestContext.DataRow["experience"]);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/SubmitAssociateSkills/", new StringContent(JsonConvert.SerializeObject(skillData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion

        #region GetSkillsDataByIDTest
        /// <summary>
        /// GetSkillsDataByIDTest
        /// </summary>
        [TestMethod()]
        public void GetSkillsDataByIDTest()
        {
            using (HttpClient client = new HttpClient())
            {
                int empId = 321;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetAssociateSkills?empID=" + empId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetSkillsDataByID
        /// <summary>
        /// GetSkillsDataByID
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv", "TestInput"), TestMethod]
        public void GetSkillsDataByID()
        {
            using (HttpClient client = new HttpClient())
            {
                int CompetencyAreaId = 1;
                int SkillGroupId = 5;

                string skillName = string.Empty;
                if (!string.IsNullOrEmpty(TestContext.DataRow["skillName"].ToString()))
                {
                    skillName = TestContext.DataRow["skillName"].ToString();
                }

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetSkillsByID?skillGroupID=" + SkillGroupId + "&competencyAreaID=" + CompetencyAreaId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var skillData = result.Result;
                        //deserialize to your class
                        List<EmployeeSkillDetails> lstSkillData = JsonConvert.DeserializeObject<List<EmployeeSkillDetails>>(skillData);
                        EmployeeSkillDetails skillDetails = lstSkillData.Find(skill => skill.SkillName.ToLower().Trim() == skillName.ToLower().Trim());
                        Assert.AreEqual(skillName, skillDetails.SkillName);
                    }
                }

            };
        }
        #endregion

        #region GetDraftApproveStatusSkillsTest
        /// <summary>
        /// GetDraftApproveStatusSkillsTest
        /// </summary>
        [TestMethod()]
        public void GetDraftApproveStatusSkills()
        {
            using (HttpClient client = new HttpClient())
            {
                int empId = 107;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetDraftApproveStatusSkills?empID=" + empId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region GetPendingApprovalStatusSkillsTest
        /// <summary>
        /// GetPendingApprovalStatusSkills
        /// </summary>
        [TestMethod()]
        public void GetPendingApprovalStatusSkills()
        {
            using (HttpClient client = new HttpClient())
            {
                int empId = 107;
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateSkills/GetPendingApprovalStatusSkills?empID=" + empId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }

            };
        }
        #endregion

        #region DeleteEmployeeSkillsTest
        /// <summary>
        /// CreateEmployeeSkillsTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\MasterSkills.csv", "MasterSkills#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\MasterSkills.csv", "TestInput"), TestMethod]
        public void DeleteEmployeeSkillsTest()
        {
            EmployeeSkillDetails skillData = new EmployeeSkillDetails();
            skillData.ID = Convert.ToInt32(TestContext.DataRow["ID"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                //::TODO:: Unable to send parameter ID 
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateSkills/DeleteAssociateSkill", new StringContent(JsonConvert.SerializeObject(skillData.ID), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            };
        }
        #endregion
    }
}