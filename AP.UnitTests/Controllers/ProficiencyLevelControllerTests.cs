﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using AP.DomainEntities;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class ProficiencyLevelControllerTests:BaseControllerTests
    {
    
        #region CreateProficiencyLevelTest
        /// <summary>
        /// Test method for adding proficiency level
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProficiencyLevelInfo.csv", "ProficiencyLevelInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProficiencyLevelInfo.csv", "TestInput"), TestMethod]
        public void CreateProficiencyLevelTest()
        {
            ProficiencyLevelData proficiencyLevelData = new ProficiencyLevelData();
            if (!Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelCode"]) || !Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelDescription"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                proficiencyLevelData.ProficiencyLevelCode = Convert.ToString(TestContext.DataRow["ProficiencyLevelCode"]);
                proficiencyLevelData.ProficiencyLevelDescription = Convert.ToString(TestContext.DataRow["ProficiencyLevelDescription"]);
                proficiencyLevelData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            }
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProficiencyLevel/CreateProficiencyLevel", new StringContent(JsonConvert.SerializeObject(proficiencyLevelData).ToString(), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region UpdateProficiencyLevelTest
        /// <summary>
        /// Test method for updating proficiency level
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProficiencyLevelInfo.csv", "ProficiencyLevelInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProficiencyLevelInfo.csv", "TestInput"), TestMethod]
        public void UpdateProficiencyLevelTest()
        {
            ProficiencyLevelData proficiencyLevelData = new ProficiencyLevelData();

            if (!Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelId"]) || !Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelCode"]) || !Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelDescription"]) || !Convert.IsDBNull(TestContext.DataRow["IsActive"]))
            {
                proficiencyLevelData.ProficiencyLevelId = Convert.ToInt32(TestContext.DataRow["ProficiencyLevelId"]);
                proficiencyLevelData.ProficiencyLevelCode = Convert.ToString(TestContext.DataRow["ProficiencyLevelCode"]);
                proficiencyLevelData.ProficiencyLevelDescription = Convert.ToString(TestContext.DataRow["ProficiencyLevelDescription"]);
                proficiencyLevelData.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
            }

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpResponseMessage response = client.PostAsync(serviceURL + "/ProficiencyLevel/UpdateProficiencyLevel", new StringContent(JsonConvert.SerializeObject(proficiencyLevelData), Encoding.UTF8, "application/json")).Result)
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        #endregion

        #region GetProficiencyLevelTest
        /// <summary>
        /// Test method for retrieving proficiency levels
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ProficiencyLevelInfo.csv", "ProficiencyLevelInfo#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ProficiencyLevelInfo.csv", "TestInput"), TestMethod]
        public void GetProficiencyLevelsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/ProficiencyLevel/GetProficiencyLevels"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion
    }
}
