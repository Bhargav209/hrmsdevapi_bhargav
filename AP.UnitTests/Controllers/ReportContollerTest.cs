﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using System.Net.Http;
using System.Threading.Tasks;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;
using AP.API;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class ReportContollerTest : BaseControllerTests
    {
        #region GetResourceReportByProject
        /// <summary>
        /// Get Resource Report By Project
        /// </summary> 

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ResourceReportByProject.csv", "ResourceReportByProject#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ResourceReportByProject.csv"), TestMethod]
        public void GetResourceByProjectTest()
        {

            using (HttpClient client = new HttpClient())
            {
                int projectId = (int)(TestContext.DataRow["projectId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Reports/GetResourceByProject?projectId=" + projectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var reportData = result.Result;
                        //deserialize to your class
                        AllocationDetails aalocDetails = JsonConvert.DeserializeObject<AllocationDetails>(reportData);
                        Assert.IsTrue(aalocDetails.lstNonBillableResources.Count > 0, "Has some data");

                    }
                }

            };
        }
        #endregion

        #region GetFinanceReports
        /// <summary>
        /// GetFinanceReports
        /// </summary>

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ResourceReportByProject.csv", "ResourceReportByProject#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ResourceReportByProject.csv"), TestMethod]
        public void GetFinanceReports()
        {
            ReportsFilterData reportsFilterData = new ReportsFilterData();
            {
                reportsFilterData.financeReportFilterData.FromDate = Convert.ToDateTime(TestContext.DataRow["FromDate"]);
                reportsFilterData.financeReportFilterData.ToDate = Convert.ToDateTime(TestContext.DataRow["ToDate"]);
                reportsFilterData.financeReportFilterData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                reportsFilterData.financeReportFilterData.RowsPerPage = Convert.ToInt32(TestContext.DataRow["RowsPerPage"]);
                reportsFilterData.financeReportFilterData.PageNumber = Convert.ToInt32(TestContext.DataRow["PageNumber"]);
            };
            new Report().GetFinanceReports(reportsFilterData);
        }

        #endregion

        #region GetUtilizationReports
        /// <summary>
        /// GetUtilizationReports
        /// </summary>

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\ResourceReportByProject.csv", "ResourceReportByProject#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\ResourceReportByProject.csv"), TestMethod]
        public void GetUtilizationReports()
        {
            ReportsFilterData reportsFilterData = new ReportsFilterData();
            {
                reportsFilterData.utilizationReportFilterData.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                reportsFilterData.utilizationReportFilterData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                reportsFilterData.utilizationReportFilterData.GradeId = Convert.ToInt32(TestContext.DataRow["GradeId"]);
                reportsFilterData.utilizationReportFilterData.DesignationId = Convert.ToInt32(TestContext.DataRow["DesignationID"]);
                reportsFilterData.utilizationReportFilterData.ClientId = Convert.ToInt32(TestContext.DataRow["ClientId"]);
                reportsFilterData.utilizationReportFilterData.AllocationPercentageId = Convert.ToInt32(TestContext.DataRow["AllocationPercentageId"]);
                reportsFilterData.utilizationReportFilterData.ProgramManagerId = Convert.ToInt32(TestContext.DataRow["ReportingManagerId"]);
                reportsFilterData.utilizationReportFilterData.MinExperience = Convert.ToInt32(TestContext.DataRow["MinExperience"]);
                reportsFilterData.utilizationReportFilterData.MaxExperience = Convert.ToInt32(TestContext.DataRow["MaxExperience"]);
                reportsFilterData.utilizationReportFilterData.IsBillable = Convert.ToInt32(TestContext.DataRow["IsBillable"]);
                reportsFilterData.utilizationReportFilterData.IsCritical = Convert.ToInt32(TestContext.DataRow["IsCritical"]);
                reportsFilterData.utilizationReportFilterData.RowsPerPage = Convert.ToInt32(TestContext.DataRow["RowsPerPage"]);
                reportsFilterData.utilizationReportFilterData.PageNumber = Convert.ToInt32(TestContext.DataRow["PageNumber"]);
            };
            new Report().GetUtilizationReports(reportsFilterData);
        }

        #endregion


    }
}
