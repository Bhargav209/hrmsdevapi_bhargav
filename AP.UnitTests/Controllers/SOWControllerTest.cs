﻿using AP.Services.Controllers.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers.Tests;
using AP.DomainEntities;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace AP.UnitTests.Controllers
{
    [TestClass]
    public class SOWControllerTest : BaseControllerTests
    {
        #region CreateSOWTest
        /// <summary>
        /// CreateSOWTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\SOWData.csv", "SOWData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\SOWData.csv", "TestInput"), TestMethod]
        public void CreateSOWTest()
        {
            try
            {

                SOWSignData sowSignData = new SOWSignData();
                string SowId = TestContext.DataRow["SOWId"].ToString();
                if (!string.IsNullOrEmpty( SowId))
                {
                    sowSignData.SOWId = SowId;
                    sowSignData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                    sowSignData.SOWSignedDate = Convert.ToDateTime(TestContext.DataRow["SOWSignedDate"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/Project/PostSOW", new StringContent(JsonConvert.SerializeObject(sowSignData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }

                    //using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetClientsDetails"))
                    //{
                    //    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    //    {
                    //        Task<string> result = response.Content.ReadAsStringAsync();
                    //        var clientData = result.Result;
                    //        //deserialize to your class
                    //        List<ClientDetails> lstClientData = JsonConvert.DeserializeObject<List<ClientDetails>>(clientData);
                    //        ClientDetails clientDetails = lstClientData.Find(clients => clients.ClientCode.ToLower().Trim() == sowSignData.ClientCode.ToLower().Trim());
                    //        Assert.AreEqual(sowSignData.ClientCode, clientDetails.ClientCode);
                    //    }
                    //}
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetSOWDetailsById
        /// <summary>
        /// GetSOWDetailsById
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\SOWData.csv", "SOWData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\SOWData.csv"), TestMethod]

        public void GetSOWDetailsById()
        {
            using (HttpClient client = new HttpClient())
            {
                int projectId = Convert.ToInt32(TestContext.DataRow["ProjectId"].ToString());
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/Project/GetSOWDetailsById?projectId=" + projectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var sowSignData = result.Result;
                        //deserialize to your class
                        List<SOWSignData> lstSOWData = JsonConvert.DeserializeObject<List<SOWSignData>>(sowSignData);
                        SOWSignData sowDetails = lstSOWData.Find(project => project.ProjectId == projectId);
                        Assert.AreEqual(projectId, sowDetails.ProjectId);
                    }
                }

            };
        }
        #endregion
    }
}
