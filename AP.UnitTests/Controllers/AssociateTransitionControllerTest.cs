﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.Net.Http;
using System.Net;
using System;
using AP.DomainEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using AP.DataStorage;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class AssociateResignationControllerTest : BaseControllerTests
    {
        #region SaveTransitionPlanTest
        /// <summary>
        /// SaveTransitionPlanTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionData.csv", "AssociateTransitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionData.csv", "TestInput"), TestMethod]
        public void SaveTransitionPlanTest()
        {
            try
            {
                TransitionPlanData planData = new TransitionPlanData();
                if (!Convert.IsDBNull(TestContext.DataRow["EmployeeId"]) || !Convert.IsDBNull(TestContext.DataRow["ProjectId"]))
                {
                    planData.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                    planData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                    planData.RoleMasterId = Convert.ToInt32(TestContext.DataRow["RoleId"]);
                    planData.StartDate = Convert.ToDateTime(TestContext.DataRow["StartDate"]);
                    planData.EndDate = Convert.ToDateTime(TestContext.DataRow["EndDate"]);
                    planData.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TransitionPlan/SaveTransitionPlan", new StringContent(JsonConvert.SerializeObject(planData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region UpdateTransitionPlanTest
        /// <summary>
        /// UpdateTransitionPlanTest
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionData.csv", "AssociateTransitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionData.csv", "TestInput"), TestMethod]
        public void UpdateTransitionPlanTest()
        {
            try
            {
                TransitionPlanData planData = new TransitionPlanData();
                if (!Convert.IsDBNull(TestContext.DataRow["EmployeeId"]) || !Convert.IsDBNull(TestContext.DataRow["ProjectId"]))
                {
                    planData.TransitionId = Convert.ToInt32(TestContext.DataRow["TrasitionId"]);
                    planData.EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                    planData.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                    planData.RoleMasterId = Convert.ToInt32(TestContext.DataRow["RoleId"]);
                    planData.StartDate = Convert.ToDateTime(TestContext.DataRow["StartDate"]);
                    planData.EndDate = Convert.ToDateTime(TestContext.DataRow["EndDate"]);
                    planData.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                }
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TransitionPlan/SaveTransitionPlan", new StringContent(JsonConvert.SerializeObject(planData).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region GetRoleByEmployeeID
        /// <summary>
        /// GetRoleByEmployeeID
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionData.csv", "AssociateTransitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionData.csv"), TestMethod]

        public void GetRoleByEmployeeID()
        {
            using (HttpClient client = new HttpClient())
            {
                int EmployeeId = Convert.ToInt32(TestContext.DataRow["EmployeeId"]);
                int ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TransitionPlan/GetRoleByEmployeeID?employeeId=" + EmployeeId + "&projectId=" + ProjectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var roleData = result.Result.ToString();
                        //deserialize to your class
                        string role = JsonConvert.DeserializeObject<string>(roleData);
                        Assert.AreEqual("Software Engineer", role);

                    }
                }

            };
        }
        #endregion

        #region GetProjectByReportingManager
        /// <summary>
        /// GetProjectByReportingManager
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionData.csv", "AssociateTransitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionData.csv"), TestMethod]

        public void GetProjectByReportingManager()
        {
            using (HttpClient client = new HttpClient())
            {
                int ReportingManagerId = Convert.ToInt32(TestContext.DataRow["ReportingManagerId"]);
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TransitionPlan/GetProjectByReportingManager?reportingManagerId=" + ReportingManagerId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var projectData = result.Result;
                        //deserialize to your class
                        List<ProjectData> project = JsonConvert.DeserializeObject<List<ProjectData>>(projectData);
                        Assert.AreEqual("Uline", project[3].ProjectName);
                    }
                }

            };
        }
        #endregion

        #region GetEmployeeById
        /// <summary>
        /// GetEmployeeById
        /// </summary> 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionData.csv", "AssociateTransitionData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionData.csv"), TestMethod]

        public void GetEmployeeById()
        {
            int ReportingManagerId = Convert.ToInt32(TestContext.DataRow["ReportingManagerId"]);
            int ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/TransitionPlan/GetEmployeeById?ReportingManagerId=" + ReportingManagerId + "&ProjectId=" + ProjectId))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Task<string> result = response.Content.ReadAsStringAsync();
                        var employeeData = result.Result;
                        //deserialize to your class
                        List<AssociateAllocationDetails> associateData = JsonConvert.DeserializeObject<List<AssociateAllocationDetails>>(employeeData);
                        Assert.AreEqual("Aneesh Gadde", associateData[0].AssociateName);
                    }
                }

            };
        }
        #endregion

        #region SaveTransitionPlanDetails
        /// <summary>
        /// SaveTransitionPlanDetails
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionPlanData.csv", "AssociateTransitionPlanData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionPlanData.csv", "TestInput"), TestMethod]
        public void SaveTransitionPlanDetailsTest()
        {
            try
            {
                List<TransitionPlanDetailsData> listTransitionPlan = new List<TransitionPlanDetailsData>();
                if (!Convert.IsDBNull(TestContext.DataRow["CategoryId"]) || !Convert.IsDBNull(TestContext.DataRow["TaskId"]))
                {
                    TransitionPlanDetailsData transitionPlan = new TransitionPlanDetailsData();
                    transitionPlan.TransitionId = Convert.ToInt32(TestContext.DataRow["TransitionId"]);
                    transitionPlan.CategoryId = Convert.ToInt32(TestContext.DataRow["CategoryId"]);
                    transitionPlan.TaskId = Convert.ToInt32(TestContext.DataRow["TaskId"]);
                    transitionPlan.StartDate = Convert.ToDateTime(TestContext.DataRow["StartDate"]);
                    transitionPlan.EndDate = Convert.ToDateTime(TestContext.DataRow["EndDate"]);
                    transitionPlan.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                    transitionPlan.Remarks = Convert.ToString(TestContext.DataRow["Remarks"]);
                    listTransitionPlan.Add(transitionPlan);
                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                        using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TransitionPlan/SaveTransitionPlanDetails", new StringContent(JsonConvert.SerializeObject(listTransitionPlan).ToString(), Encoding.UTF8, "application/json")).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    };
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region UpdateTransitionPlanDetails
        /// <summary>
        /// UpdateTransitionPlanDetails
        /// </summary>
        [DataSource(@"Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateTransitionPlanData.csv", "AssociateTransitionPlanData#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateTransitionPlanData.csv", "TestInput"), TestMethod]
        public void UpdateTransitionPlanDetails()
        {
            try
            {
                List<TransitionPlanDetailsData> listTransitionPlan = new List<TransitionPlanDetailsData>();
                if (!Convert.IsDBNull(TestContext.DataRow["TransitionId"]) || !Convert.IsDBNull(TestContext.DataRow["CategoryId"]))
                {
                    TransitionPlanDetailsData transitionPlan = new TransitionPlanDetailsData();
                    transitionPlan.ID = Convert.ToInt32(TestContext.DataRow["ID"]);
                    transitionPlan.TransitionId = Convert.ToInt32(TestContext.DataRow["TransitionId"]);
                    transitionPlan.CategoryId = Convert.ToInt32(TestContext.DataRow["CategoryId"]);
                    transitionPlan.TaskId = Convert.ToInt32(TestContext.DataRow["TaskId"]);
                    transitionPlan.StartDate = Convert.ToDateTime(TestContext.DataRow["StartDate"]);
                    transitionPlan.EndDate = Convert.ToDateTime(TestContext.DataRow["EndDate"]);
                    transitionPlan.StatusId = Convert.ToInt32(TestContext.DataRow["StatusId"]);
                    transitionPlan.Remarks = Convert.ToString(TestContext.DataRow["Remarks"]);
                    listTransitionPlan.Add(transitionPlan);
                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                        using (HttpResponseMessage response = client.PostAsync(serviceURL + "/TransitionPlan/SaveTransitionPlanDetails", new StringContent(JsonConvert.SerializeObject(listTransitionPlan).ToString(), Encoding.UTF8, "application/json")).Result)
                        {
                            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        }
                    };
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
