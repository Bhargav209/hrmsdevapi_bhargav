﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace AP.Services.Controllers.Tests
{
    [TestClass()]
    public class KRAPdfConfigurationControllerTests: BaseControllerTests
    {
        #region GetKRAPdfConfiguarationDetailsTest
        /// <summary>
        /// GetKRAPdfConfiguarationDetailsTest
        /// </summary>
        [TestMethod()]
        public void GetKRAPdfConfiguarationDetailsTest()
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/KRAPdfConfiguration/GetKRAPdfConfiguarationDetails"))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion
    }
}