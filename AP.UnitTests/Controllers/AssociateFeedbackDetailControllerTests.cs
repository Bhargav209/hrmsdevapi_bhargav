﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.Services.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using AP.DomainEntities;

namespace AP.Services.Controllers.Tests
{
    [TestClass]
    public class AssociateFeedbackDetailControllerTests : BaseControllerTests
    {

        #region GetProjectsByUserIDTest
        /// <summary>
        /// GetProjectsByUserIDTest
        /// </summary>
        [TestMethod()]
        public void GetProjectsByUserIDTest()
        {
            string userName = "priyanka.tripurani@senecaglobal.com";

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateFeedbackDetail/GetProjectsByUserID?userName=" + userName))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }

        }
        #endregion

        #region GetProjectsByUserIDTest
        /// <summary>
        /// GetProjectsByUserIDTest
        /// </summary>
        [TestMethod()]
        public void GetManagerandLeadByProjectIDTest()
        {
            int projectID = 116;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateFeedbackDetail/GetManagerandLeadByProjectID?projectID=" + projectID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }

        }
        #endregion

        #region GetAssociatesByProjectIDTest
        /// <summary>
        /// GetAssociatesByProjectIDTest
        /// </summary>
        [TestMethod()]
        public void GetAssociatesByProjectIDTest()
        {
            int projectID = 116;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, serviceURL + "/AssociateFeedbackDetail/GetAssociatesByProjectID?projectID=" + projectID))
                {
                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                    {
                        var var = response.Content.ReadAsStringAsync().Result;
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }
        }
        #endregion

        #region SaveAssociateFeedbackTest
        /// <summary>
        /// SaveAssociateFeedbackTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateFeedback.csv", "AssociateFeedback#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateFeedback.csv"), TestMethod]
        public void SaveAssociateFeedbackTest()
        {
            List<EmployeeFeedback> employeeFeedbackList = new List<EmployeeFeedback>();
            EmployeeFeedback employeeFeedback = new EmployeeFeedback();

            if (!Convert.IsDBNull(TestContext.DataRow["ProjectId"]) || !Convert.IsDBNull(TestContext.DataRow["LeadId"])
                || !Convert.IsDBNull(TestContext.DataRow["ManagerId"]) || !Convert.IsDBNull(TestContext.DataRow["EmpId"])
                || !Convert.IsDBNull(TestContext.DataRow["CompetencyAreaId"]) || !Convert.IsDBNull(TestContext.DataRow["SkillId"])
                || !Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelId"]) || !Convert.IsDBNull(TestContext.DataRow["SkillApplied"])
                || !Convert.IsDBNull(TestContext.DataRow["StatusId"]))
            {
                employeeFeedback.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                employeeFeedback.LeadId = Convert.ToInt32(TestContext.DataRow["LeadId"]);
                employeeFeedback.ManagerId = Convert.ToInt32(TestContext.DataRow["ManagerId"]);
                employeeFeedback.EmpId = Convert.ToInt32(TestContext.DataRow["EmpId"]);
                employeeFeedback.CompetencyAreaId = Convert.ToInt32(TestContext.DataRow["CompetencyAreaId"]);
                employeeFeedback.SkillId = Convert.ToInt32(TestContext.DataRow["SkillId"]);
                employeeFeedback.ProficiencyLevelId = Convert.ToInt32(TestContext.DataRow["ProficiencyLevelId"]);
                employeeFeedback.AssociateContribution = Convert.ToString(TestContext.DataRow["AssociateContribution"]);
                employeeFeedback.ManagerFeedback = Convert.ToString(TestContext.DataRow["ManagerFeedback"]);
                employeeFeedback.SkillApplied = Convert.ToBoolean(TestContext.DataRow["SkillApplied"]);
                employeeFeedback.Status = Convert.ToString(TestContext.DataRow["StatusCode"]);
                employeeFeedback.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                employeeFeedback.UserName = "priyanka.tripurani@senecaglobal.com";
                employeeFeedbackList.Add(employeeFeedback);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateFeedbackDetail/SaveAssociateFeedback", new StringContent(JsonConvert.SerializeObject(employeeFeedbackList).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }

        }
        #endregion

        #region EditAssociateFeedbackTest
        /// <summary>
        /// SaveAssociateFeedbackTest
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestInput\AssociateFeedback.csv", "AssociateFeedback#csv", DataAccessMethod.Sequential), DeploymentItem(@"TestInput\AssociateFeedback.csv"), TestMethod]
        public void EditAssociateFeedbackTest()
        {
            List<EmployeeFeedback> employeeFeedbackList = new List<EmployeeFeedback>();
            EmployeeFeedback employeeFeedback = new EmployeeFeedback();

            if (!Convert.IsDBNull(TestContext.DataRow["ProjectId"]) || !Convert.IsDBNull(TestContext.DataRow["LeadId"])
                || !Convert.IsDBNull(TestContext.DataRow["ManagerId"]) || !Convert.IsDBNull(TestContext.DataRow["EmpId"])
                || !Convert.IsDBNull(TestContext.DataRow["CompetencyAreaId"]) || !Convert.IsDBNull(TestContext.DataRow["SkillId"])
                || !Convert.IsDBNull(TestContext.DataRow["ProficiencyLevelId"]) || !Convert.IsDBNull(TestContext.DataRow["SkillApplied"])
                || !Convert.IsDBNull(TestContext.DataRow["StatusId"]) || !Convert.IsDBNull(TestContext.DataRow["AssociateFeedbackId"]))
            {

                employeeFeedback.ID = Convert.ToInt32(TestContext.DataRow["AssociateFeedbackId"]);
                employeeFeedback.ProjectId = Convert.ToInt32(TestContext.DataRow["ProjectId"]);
                employeeFeedback.LeadId = Convert.ToInt32(TestContext.DataRow["LeadId"]);
                employeeFeedback.ManagerId = Convert.ToInt32(TestContext.DataRow["ManagerId"]);
                employeeFeedback.EmpId = Convert.ToInt32(TestContext.DataRow["EmpId"]);
                employeeFeedback.CompetencyAreaId = Convert.ToInt32(TestContext.DataRow["CompetencyAreaId"]);
                employeeFeedback.SkillId = Convert.ToInt32(TestContext.DataRow["SkillId"]);
                employeeFeedback.ProficiencyLevelId = Convert.ToInt32(TestContext.DataRow["ProficiencyLevelId"]);
                employeeFeedback.AssociateContribution = Convert.ToString(TestContext.DataRow["AssociateContribution"]);
                employeeFeedback.ManagerFeedback = Convert.ToString(TestContext.DataRow["ManagerFeedback"]);
                employeeFeedback.SkillApplied = Convert.ToBoolean(TestContext.DataRow["SkillApplied"]);
                employeeFeedback.Status = Convert.ToString(TestContext.DataRow["StatusCode"]);
                employeeFeedback.IsActive = Convert.ToBoolean(TestContext.DataRow["IsActive"]);
                employeeFeedbackList.Add(employeeFeedback);

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + serviceToken);
                    using (HttpResponseMessage response = client.PostAsync(serviceURL + "/AssociateFeedbackDetail/SaveAssociateFeedback", new StringContent(JsonConvert.SerializeObject(employeeFeedbackList).ToString(), Encoding.UTF8, "application/json")).Result)
                    {
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    }
                }
            }

        }
        #endregion
    }
}