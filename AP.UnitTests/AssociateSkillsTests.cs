﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using AP.API;
//using AP.DomainEntities;
//using System.Collections.Generic;

//namespace AP.UnitTests
//{
//    [TestClass]
//    public class AssociateSkillsTests
//    {
//        #region AssociateSkillsTestMethods

//        #region CreateSkillsMaster
//        /// <summary>
//        /// CreateSkillsMaster
//        /// </summary>
//        [TestMethod]
//        public void CreateSkillsMaster()
//        {
//            string skillCode = "MSBI";
//            string skillName = "MSBI";
//            bool isActive = true;
//            bool isApproved = true;
//            AssociateSkills assSkills = new AssociateSkills();
//            //bool skillData = assSkills.CreateSkillsMaster(skillCode, skillName, isActive, isApproved);
//            //Assert.IsTrue(skillData, "true");
//        }
//        #endregion

//        #region GetSkillsData
//        /// <summary>
//        /// GetSkillsData
//        /// </summary>
//        [TestMethod]
//        public void GetSkillsData()
//        {
//            AssociateSkills assSkills = new AssociateSkills();
//            var getSkillsData = assSkills.GetSkillsData();
//            Assert.IsNotNull(getSkillsData);
//        }
//        #endregion

//        #region UpdateSkillMasterDetails
//        /// <summary>
//        /// UpdateSkillMasterDetails
//        /// </summary>
//        [TestMethod]
//        public void UpdateSkillMasterDetails()
//        {
//            int skillID = 1;
//            string skillCode = ".Net";
//            string skillName = ".NET";
//            bool isActive = true;
//            bool isApproved = false;
//            AssociateSkills assSkills = new AssociateSkills();
//            //bool updatedSkillData = assSkills.UpdateSkillMasterDetails(skillID, skillCode, skillName, isActive, isApproved);
//            //Assert.IsTrue(updatedSkillData, "true");
//        }
//        #endregion 

//        #endregion

//        [TestMethod]
//        public void AddUpdateCompetencySkillsTest()
//        {
//            CompetencySkills compSkills = new CompetencySkills();
//            CompetencySkillDetails csd = new CompetencySkillDetails();
//            csd.CompetencyID = 1;
//            csd.RoleID = 1;
//            List<int?> Skills = new List<int?>                ();
//            Skills.Add(1);
//            Skills.Add(2);
//            Skills.Add(10);
//            Skills.Add(18);
//            csd.Skills = Skills;
//            Assert.IsTrue(compSkills.AddUpdateCompetencySkills(csd));
//        }
//    }
//}
