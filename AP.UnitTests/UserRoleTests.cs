﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.API;
using AP.DomainEntities;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using AP.DataStorage;

namespace AP.UnitTests
{
    [TestClass]
    public class UserRoleTests
    {
        #region AssignRole
        /// <summary>
        /// AssignRole
        /// </summary>
        //[TestMethod]
        //public void AssignRole()
        //{
        //    string userName = "bhavani.chintamadaka@senecaglobal.net";
        //    int roleID = 1;
        //    bool isActive = true;
        //    UserRoles userRole = new UserRoles();
        //    bool userRoles = userRole.AssignRole(userName, roleID, isActive);
        //    Assert.IsTrue(userRoles, "true");

        //} 
        #endregion

        #region CreateRoleMaster
        /// <summary>
        /// Create Role Master Test Method
        /// </summary>
        //[TestMethod]
        //public void CreateRoleMaster()
        //{
        //    string roleName = "IT";
        //    string roleDesc = "ITDept";
        //    bool isActive = true;
        //    UserRoles userRole = new UserRoles();
        //    bool createRole = userRole.CreateRoleMaster(roleName, roleDesc, isActive);
        //    Assert.IsTrue(createRole, "true");

        //}
        #endregion

        #region GetRoleMasterDetails
        /// <summary>
        /// Get Role Master Details Test Method
        /// </summary>
        [TestMethod]

        public void GetRoleMasterDetails()
        {
            UserRoles userRole = new UserRoles();
            var getUserRole = userRole.GetRoleMasterDetails();
            Assert.IsNotNull(getUserRole);

        }
        #endregion

        #region GetUserRoleOnLgin
        /// <summary>
        /// GetUserRoleOnLgin
        /// </summary>
        [TestMethod]
        public void GetUserRoleOnLgin()

        {
            string roleName = "barakha.sahu@senecaglobal.com";
            UserRoles login = new UserRoles();
            var getUserRole = login.GetUserRoleOnLgin(roleName);
            Assert.IsNotNull(getUserRole);

        }
        #endregion

        //[TestMethod]
        //public async Task CreateSkillGroup()
        //{
        //    SkillGroupDetails skillGroupDetails = new SkillGroupDetails();
        //    skillGroupDetails.SkillGroupName = ".NetCore1";
        //    skillGroupDetails.IsActive = true;
        //    skillGroupDetails.Description = ".NetCore2";
        //    skillGroupDetails.CompetencyAreaId = 2;
        //    skillGroupDetails.CreatedDate = DateTime.UtcNow;

        //    //await new SkillGroup().CreateSkillGroup(skillGroupDetails);
        //    Assert.IsNotNull(skillGroupDetails);
        //}


        //[TestMethod]
        //public async Task UpdateSkillGroup()
        //{
        //    SkillGroupDetails skillGroupDetails = new SkillGroupDetails();
        //    skillGroupDetails.SkillGroupId = 13;
        //    skillGroupDetails.SkillGroupName = "BCE (IC)";
        //    skillGroupDetails.IsActive = true;
        //    skillGroupDetails.Description = "BCE (IC)";
        //    skillGroupDetails.CompetencyAreaId = 22;
        //    //await new SkillGroup().UpdateSkillGroup(skillGroupDetails);
        //    Assert.IsNotNull(skillGroupDetails);
        //}

        //[TestMethod]
        //public async Task DeleteSkillGroup()
        //{
        //    SkillGroupDetails skillGroupDetails = new SkillGroupDetails();
        //    skillGroupDetails.SkillGroupId = 13;
        //    skillGroupDetails.ModifiedUser = "Sa";
        //    skillGroupDetails.SystemInfo = "192.168.2.148";
        //    skillGroupDetails.ModifiedDate = DateTime.UtcNow;

        //    await new SkillGroup().DeleteSkillGroup(skillGroupDetails);
        //    Assert.IsNotNull(skillGroupDetails);
        //}

        //[TestMethod]
        //public async Task GetsSkillGroup()
        //{
        //    await new API.SkillGroup().GetsSkillGroup();

        //}

        //[TestMethod]
        //public async Task GetSkillGroupByCompetencyAreaID()
        //{
        //    await new SkillGroup().GetSkillGroupByCompetencyAreaID(9);

        //}


        //[TestMethod]
        //public void UpdateAssociateStatus()
        //{
        //    new AssociatePersonalDetails().UpdateAssociateStatus(10);
        //}

        [TestMethod]
        public async Task AddErrorLog()
        {
            ErrorLogDetails errorLog = new ErrorLogDetails();
            errorLog.ErrorMessage = "UI Error";
            errorLog.FileName = "Employee.ts";

            await new ErrorLog().AddErrorLog(errorLog, 1);
        }

        [TestMethod]
        public void GetRequisitionDetailsBySearch()
        {
            List<GenericType> skillType = new List<GenericType>();
            skillType.Add(new GenericType { Id = 27 });
            skillType.Add(new GenericType { Id = 17 });
            List<GenericType> proficiencyType = new List<GenericType>();
            proficiencyType.Add(new GenericType { Id = 1 });
            proficiencyType.Add(new GenericType { Id = 2 });
            RequisitionSearchFilter searchFilter = new RequisitionSearchFilter
            {
                MinimumExperience = 0,
                MaximumExperience = 0,
                Skills = skillType,
                Proficiencies = proficiencyType,
                RoleId = 1,
            };
            new TalentRequisitionDetails().GetRequisitionDetailsBySearch(searchFilter);
        }

        //[TestMethod]
        //public void GetKRARoleCategory()
        //{
        //    new KRA().GetKRARoleCategory();
        //}

        //[TestMethod]
        //public void GetKRARoleByCategoryID()
        //{
        //    new KRA().GetKRARoleByCategoryID(1);
        //}       

        [TestMethod]
        public void GetKRAAspect(int departmentId)
        {
            new KRAAspects().GetKRAAspect(departmentId);
        }

        //[TestMethod]
        //public void GetKRASetByFinancialYearID()
        //{
        //    new KRA().GetKRASetByFinancialYearID(1);
        //}

        //[TestMethod]
        //public async Task CloneKRAs()
        //{
        //    await new KRA().CloneKRAs(1, 3);
        //}

        //[TestMethod]
        //public void GetKRASets()
        //{
        //    new KRA().GetKRASets(2, 6, 2);
        //}

        //[TestMethod]
        //public void DeleteKraAspectFromKRASet()
        //{
        //    KRASetData data = new KRASetData();
        //    List<KRASetData> lstdata = new List<KRASetData>();

        //    data.FinancialYearID = 2;
        //    data.KRAAspectID = 1;
        //    lstdata.Add(data);
        //    data.FinancialYearID = 2;
        //    data.KRAAspectID = 2;
        //    lstdata.Add(data);

        //    new KRA().DeleteKraAspectFromKRASet(lstdata);
        //}


        //[TestMethod]
        //public async Task SubmitKRA()
        //{
        //    KRASetData data = new KRASetData();
        //    data.FinancialYearID = 3;
        //    data.StatusID = 21;
        //    data.FromEmployeeID = 142;
        //    data.Comments = "Approved";
        //    await new KRA().SubmitKRA(data);
        //}

        [TestMethod]
        public async Task GetKRAHistory()
        {
            await new KRA().GetKRAHistory();
        }

        [TestMethod]
        public async Task TaskGetKRAWorkFlowHistory()
        {
            await new KRA().GetKRAWorkFlowHistory(2);
        }

        [TestMethod]
        public async Task SetADRCycleActive()
        {
            ADRCycleDetail adrCycleDetail = new ADRCycleDetail();
            adrCycleDetail.ADRCycleID = 3;
            adrCycleDetail.IsActive = true;
            await new ADR().SetADRCycleActive(adrCycleDetail);
        }

        [TestMethod]
        public async Task GetActiveProjects()
        {
            await new ProjectDetails().GetActiveProjects();
        }

        [TestMethod]
        public async Task GetEmployeesByProjectsID()
        {
            await new ProjectDetails().GetEmployeesByProjectID(10);
        }

        //[TestMethod]
        //public async Task GetAssociateKRAs()
        //{
        //    await new KRAMapping().GetAssociateKRAs(365, 1, 1);
        //}
        [TestMethod]
        public void GetKRAWorkFlowPendingWithEmployeeID()
        {
            new KRA().GetKRAWorkFlowPendingWithEmployeeID(1, 2);
        }

        [TestMethod]
        public async Task GenerateKRAPdfForAllAssociatesTest()
        {
            await new KRAMapping().GenerateKRAPdfForAllAssociates(1);
        }

        [TestMethod]
        public void GetManagerandLeadByProjectID()
        {
            new ProjectDetails().GetManagerandLeadByProjectID(18, 378);
        }

        [TestMethod]
        public async Task GetResourceByProject()
        {
            await new Report().GetResourceByProject(6);
        }

        [TestMethod]
        public async Task ViewKRAGroups()
        {
            await new KRA().ViewKRAGroups(1,1);
        }

        [TestMethod]
        public async Task GetKRADefinitionByDepartmentID()
        {
            await new KRA().GetKRADefinitionByDepartmentID(1, 1, 2);
        }

        [TestMethod]
        public async Task GetAssociateSkillsReport()
        {
            SkillSearchData skillSearchFilter = new SkillSearchData();           
            skillSearchFilter.SkillIds = "1";
            skillSearchFilter.SkillNames = ".NET Core 1.0";
            skillSearchFilter.IsPrimary = false;
            skillSearchFilter.IsSecondary = true;
            List<SkillSearchData> associateSkillData = await new Report().GetAssociateSkillsReport(skillSearchFilter);
        }

        [TestMethod]
        public async Task DomianReportCount()
        { 
            List<DomainDataCount> domainDataCount = await new Report().DomainReportCount();
        }

        [TestMethod]
        public async Task TalentPoolReportCount()
        {
            List<TalentPoolDataCount> talentPoolDataCount = await new Report().TalentPoolReportCount();
        }

        [TestMethod]
        public async Task UpdateScaleDetails()
        {
            List<KRAScaleDetails> lstKRAScaleDetails = new List<KRAScaleDetails>();

            KRAScaleDetails kraScaleDetails = new KRAScaleDetails();

            kraScaleDetails.KRAScaleDetailId = 46;
            kraScaleDetails.ScaleDescription = "San1";

            lstKRAScaleDetails.Add(kraScaleDetails);
            kraScaleDetails = new KRAScaleDetails();

            kraScaleDetails.KRAScaleDetailId = 47;
            kraScaleDetails.ScaleDescription = "San2";
            int MaximumScale = 5;

            lstKRAScaleDetails.Add(kraScaleDetails);

            await new KRAScaleMasterDetails().UpdateScaleDetails(lstKRAScaleDetails, MaximumScale);
        }
        [TestMethod]
        public void GetWorkPlaceBehaviorByTrId()
        {
            new TalentRequisitionDetails().GetWorkPlaceBehaviorByTrId(987, 154);

        }


        [TestMethod]
        public async Task GetFinanceReports()
        {
            ReportsFilterData reportsFilterData = new ReportsFilterData();
            {
                reportsFilterData.financeReportFilterData.FromDate = Convert.ToDateTime("10-March-2017");
                reportsFilterData.financeReportFilterData.ToDate = Convert.ToDateTime("10-March-2018");
                reportsFilterData.financeReportFilterData.ProjectId = 0;
                reportsFilterData.financeReportFilterData.RowsPerPage = 10;
                reportsFilterData.financeReportFilterData.PageNumber = 1;
            };
           reportsFilterData = await new Report().GetFinanceReports(reportsFilterData);
        }
    }
}


