﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP.API;
using AP.DomainEntities;


namespace AP.UnitTests
{
    [TestClass]
    public class NotificationTest
    {
        #region TestAddJobPositive
        /// <summary>
        /// TestAddJobPositive
        /// </summary>
        [TestMethod]
        public void TestAddJobPositive()
        {
            NotificationScheduler.StartScheduler();
            ProfileCreationNotificationJob objNotify = new ProfileCreationNotificationJob();
            objNotify.AddJob("N0401", "EPC");
        }
        #endregion

        #region TestAddJobNegetive
        /// <summary>
        ///TestAddJobNegetive 
        /// </summary>
        [TestMethod]
        public void TestAddJobNegetive()
        {
            NotificationScheduler.StartScheduler();
            ProfileCreationNotificationJob objNotify = new ProfileCreationNotificationJob();
            objNotify.AddJob("N0401", "EPC");
            objNotify.AddJob("N0401", "EPC");
        }
        #endregion

        #region ITNotification Test
        /// <summary>
        /// ITNotificationAddJobTest
        /// </summary>
        [TestMethod]
        public void ITNotificationAddJobTest()
        {
            NotificationScheduler.StartScheduler();
            Assert.IsTrue(new ITNotificationJob().AddJob("N0401", "IT"));
        }
        #endregion

        #region ITNotificationRemoveJobTest
        /// <summary>
        /// ITNotificationRemoveJobTest
        /// </summary>
        [TestMethod]
        public void ITNotificationRemoveJobTest()
        {
            NotificationScheduler.StartScheduler();
            ITNotificationJob objNotify = new ITNotificationJob();
            objNotify.AddJob("N0401", "IT");
            Assert.IsTrue(objNotify.RemoveJob("N0401", "IT"));
        }
        #endregion

        #region GetItNotificationsTest
        /// <summary>
        /// GetItNotificationsTest
        /// </summary>
        [TestMethod]
        public void GetItNotificationsTest()
        {
            Assert.IsNotNull(new ITNotificationJob().GetItNotifications());
        }
        #endregion

        #region GetNotificationTypeTest
        /// <summary>
        /// GetNotificationTypeTest
        /// </summary>
        [TestMethod]
        public void GetNotificationTypeTest()
        {
            Assert.IsNotNull(new ITNotificationJob().GetNotificationType());
        }
        #endregion

        #region GetSLATest
        /// <summary>
        /// GetSLATest
        /// </summary>
        [TestMethod]
        public void GetSLATest()
        {
            Assert.IsNotNull(new ITNotificationJob().GetSLA(1200));
        }
        #endregion

        #region SaveEmailNotificationConfigurationTest
        /// <summary>
        /// SaveEmailNotificationConfigurationTest
        /// </summary>
        [TestMethod]
        public void SaveEmailNotificationConfigurationTest()
        {
            EmailNotificationConfiguration details = new EmailNotificationConfiguration();
            details.notificationCode = "IT";
            details.notificationDesc = "IT notification111";
            details.emailFrom = "subhadip.sinha@senecaglobal.com";
            details.emailTo = "someone@senecaglobal.com";
            details.emailCC = "some1@senecaglobal.com";
            details.emailSubject = "IT notify";
            details.emailContent = "IT Body";
            details.SLA = 12;
            details.IsActive = true;
            string NotifyDetails = new ITNotificationJob().SaveEmailNotificationConfiguration(details);
            //Assert.IsTrue(typeof(NotifyDetails), "true");
            Assert.IsNotNull(NotifyDetails);
        }  
        #endregion

    }
}
