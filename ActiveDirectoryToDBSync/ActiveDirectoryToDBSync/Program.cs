﻿using System;
using System.DirectoryServices;
using System.Linq;
using System.Net.NetworkInformation;
using System.Diagnostics;

namespace ActiveDirectoryToDBSync
{
    class Program
    {
        public static void InsertADUsersIntoDB()
        {
            try
            {
                string domainPath = "LDAP://DC=senecaglobal,DC=net";
                DirectoryEntry searchRoot = new DirectoryEntry(domainPath);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person)(!userAccountControl:1.2.840.113556.1.4.803:=2))";
                search.PropertiesToLoad.Add("samaccountname");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("usergroup");
                search.PropertiesToLoad.Add("displayname");
                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();

                HRMS_DEVEntities apEntities = new HRMS_DEVEntities();
                //apEntities.Users.ToList().ForEach(i => i.IsActive = false);
                //apEntities.SaveChanges();

                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                                 result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname"))
                        {
                            string emailaddress = result.Properties["mail"][0].ToString();
                            string displayName = (string)result.Properties["displayname"][0];                            

                            User checkuser = apEntities.Users.Where(e => e.EmailAddress.Trim() == emailaddress.Trim() && e.IsActive == true).FirstOrDefault();
                            if (checkuser == null)
                            {
                                checkuser = new User();
                                checkuser.UserName = displayName;
                                checkuser.EmailAddress = emailaddress;
                                checkuser.IsActive = true;
                                checkuser.CreatedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                                checkuser.CreatedDate = DateTime.Now;
                                checkuser.SystemInfo = GetMACAddress();
                                apEntities.Users.Add(checkuser);
                                apEntities.SaveChanges();
                            }
                            //else
                            //{
                            //    checkuser.IsActive = true;
                            //    apEntities.SaveChanges();
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetMACAddress()
        {
            string sMacAddress = string.Empty;
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

                foreach (NetworkInterface adapter in nics)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sMacAddress;
        }

        public static void pub_WriteErrorLog(string strErrorMsg)
        {
            EventLog m_EventLog = new EventLog("");
            m_EventLog.Source = "ActiveDirectirySyncLog";

            if(strErrorMsg.Contains("Failed"))
                m_EventLog.WriteEntry(strErrorMsg,EventLogEntryType.Error);
            else
                m_EventLog.WriteEntry(strErrorMsg, EventLogEntryType.Information);            
        }

        static void Main(string[] args)
        {
            try
            {
                InsertADUsersIntoDB();
                pub_WriteErrorLog("Active Directory Users in Sync");              
            }
            catch (Exception ex)
            {
                pub_WriteErrorLog("Failed to update Active Directory Users in Sync"+ex.Message);                
                throw;
            }
        }
    }
}
